#region

using System;
using Common;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.Commands;
using PrzesuniecieModule.ListaNosnikow;
using PrzesuniecieModule.ListaTowarow;
using PrzesuniecieModule.ListaTowarow.Search;
using PrzesuniecieModule.NowePrzesuniecie;
using PrzesuniecieModule.RealizacjaMR;
using PrzesuniecieModule.RealizacjaMR.Search;
using PrzesuniecieModule.RealizacjaMR.Umieszczenie;

#endregion

namespace PrzesuniecieModule
{
    public class ModuleInitializer : ModuleInit
    {
        private readonly WorkItem workItem;

        public ModuleInitializer([ServiceDependency] WorkItem workItem)
        {
            this.workItem = workItem;
        }

        private void Initialize()
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                InitializeListaTowarow();
                InitializeNowePrzesuniecie();
                InitializeRealizacjaMR();
                InitializeUmieszczenieMR();
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        private void InitializeListaTowarow()
        {
            if (!workItem.WorkItems.Contains(WorkItemsConstants.LISTA_TOWAROW_WORKITEM))
            {
                var listaTowarowWorkItem = workItem.WorkItems.AddNew<ListaTowarowWorkItem>(WorkItemsConstants.LISTA_TOWAROW_WORKITEM);
                listaTowarowWorkItem.State[StateConstants.CRITERIAS] = string.Empty;

                var listaTowarowSearchViewPresenter = new ListaTowarowSearchViewPresenter(listaTowarowWorkItem.Items.AddNew<ListaTowarowSearchView>());
                listaTowarowWorkItem.Items.Add(listaTowarowSearchViewPresenter, ItemsConstants.LISTA_TOWAROW_SEARCH_PRESENTER);

                var listaTowarowViewPresenter = new ListaTowarowViewPresenter(listaTowarowWorkItem.Items.AddNew<ListaTowarowView>());
                listaTowarowWorkItem.Items.Add(listaTowarowViewPresenter, ItemsConstants.LISTA_TOWAROW_PRESENTER);

                var listaNosnikowPresener = new ListaNosnikowPresenter(listaTowarowWorkItem.Items.AddNew<ListaNosnikowView>());
                listaTowarowWorkItem.Items.Add(listaNosnikowPresener, ItemsConstants.PRZESUNIECIE_LISTA_NOSNIKOW_PRESENTER);
            }
        }

        private void InitializeNowePrzesuniecie()
        {
            if (!workItem.WorkItems.Contains(WorkItemsConstants.NOWE_PRZESUNIECIE_WORKITEM))
            {
                var nowePrzesuniecieWorkItem = workItem.WorkItems.AddNew<NowePrzesuniecieWorkItem>(WorkItemsConstants.NOWE_PRZESUNIECIE_WORKITEM);
                nowePrzesuniecieWorkItem.State[StateConstants.CRITERIAS] = string.Empty;

                var nowePrzesuniecieViewPresenter = new NowePrzesuniecieViewPresenter(nowePrzesuniecieWorkItem.Items.AddNew<NowePrzesuniecieView>());
                nowePrzesuniecieWorkItem.Items.Add(nowePrzesuniecieViewPresenter, ItemsConstants.NOWE_PRZESUNIECIE_PRESENTER);
            }
        }

        private void InitializeRealizacjaMR()
        {
            if (!workItem.WorkItems.Contains(WorkItemsConstants.REALIZACJA_MR_WORKITEM))
            {
                var realizacjaMRWorkItem = workItem.WorkItems.AddNew<RealizacjaMRWorkItem>(WorkItemsConstants.REALIZACJA_MR_WORKITEM);
                realizacjaMRWorkItem.State[StateConstants.CRITERIAS] = string.Empty;

                var realizacjaMRSearchViewPresenter = new RealizacjaMRSearchViewPresenter(realizacjaMRWorkItem.Items.AddNew<RealizacjaMRSearchView>());
                realizacjaMRWorkItem.Items.Add(realizacjaMRSearchViewPresenter, ItemsConstants.REALIZACJA_MR_SEARCH_PRESENTER);

                var realizacjaMRViewPresenter = new RealizacjaMRViewPresenter(realizacjaMRWorkItem.Items.AddNew<RealizacjaMRView>());
                realizacjaMRWorkItem.Items.Add(realizacjaMRViewPresenter, ItemsConstants.REALIZACJA_MR_PRESENTER);
            }
        }

        private void InitializeUmieszczenieMR()
        {
            if (!workItem.WorkItems.Contains(WorkItemsConstants.UMIESZCZENIE_MR_WORKITEM))
            {
                var umieszczenieWorkItem = workItem.WorkItems.AddNew<UmieszczenieWorkItem>(WorkItemsConstants.UMIESZCZENIE_MR_WORKITEM);
                umieszczenieWorkItem.State[StateConstants.CRITERIAS] = string.Empty;

                var umieszczenieViewPresenter = new UmieszczenieViewPresenter(umieszczenieWorkItem.Items.AddNew<UmieszczenieView>());
                umieszczenieWorkItem.Items.Add(umieszczenieViewPresenter, ItemsConstants.UMIESZCZENIE_MR_PRESENTER);

                var listaNosnikowPresener = new ListaNosnikowPresenter(umieszczenieWorkItem.Items.AddNew<ListaNosnikowView>());
                umieszczenieWorkItem.Items.Add(listaNosnikowPresener, ItemsConstants.PRZESUNIECIE_LISTA_NOSNIKOW_PRESENTER);
            }
        }


        [CommandHandler(CommandConstants.PRZESUNIECIA_LISTA_TOWAROW)]
        public void ListaTowarow(object sender, EventArgs e)
        {
            Initialize();
            var mainWorkspace = workItem.Workspaces[WorkspacesConstants.MAIN_WORKSPACE];
            var listaTowarowWorkItem = workItem.WorkItems.Get<ListaTowarowWorkItem>(WorkItemsConstants.LISTA_TOWAROW_WORKITEM);
            listaTowarowWorkItem.Show(mainWorkspace);
        }

        [CommandHandler(CommandConstants.NOWE_PRZESUNIECIE)]
        public void NowePrzesuniecie(object sender, EventArgs e)
        {
            Initialize();
            var mainWorkspace = workItem.Workspaces[WorkspacesConstants.MAIN_WORKSPACE];
            var nowePrzesuniecieWorkItem = workItem.WorkItems.Get<NowePrzesuniecieWorkItem>(WorkItemsConstants.NOWE_PRZESUNIECIE_WORKITEM);
            nowePrzesuniecieWorkItem.Show(mainWorkspace);
        }

        [CommandHandler(CommandConstants.PRZESUNIECIA_REALIZACJA)]
        public void RealizacjaMR(object sender, EventArgs e)
        {
            Initialize();
            var mainWorkspace = workItem.Workspaces[WorkspacesConstants.MAIN_WORKSPACE];
            var realizacjaMRWorkItem = workItem.WorkItems.Get<RealizacjaMRWorkItem>(WorkItemsConstants.REALIZACJA_MR_WORKITEM);
            realizacjaMRWorkItem.Show(mainWorkspace);
        }
    }
}