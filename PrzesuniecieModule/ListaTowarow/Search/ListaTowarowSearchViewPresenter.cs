using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using Common.SearchForm;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Common;

namespace PrzesuniecieModule.ListaTowarow.Search
{
	public class ListaTowarowSearchViewPresenter: SearchFormPresenter
	{
		#region Events

		[EventPublication(EventBrokerConstants.WYSZUKAJ_LISTE_TOWAROW)]
		public event EventHandler WyszukajListeTowarow;

		#endregion
		#region Properties

		public IListaTowarowSearchView View
		{
			get { return m_view as IListaTowarowSearchView; }
		}

		#endregion
		#region Constructors

        public ListaTowarowSearchViewPresenter(IListaTowarowSearchView view)
            : base(view)
		{

		}

		#endregion
		#region Overrided Memebers

		protected override void OnViewSzukaj(object sender, EventArgs e)
		{
			base.OnViewSzukaj(sender, e);
			OnWyszukajListeTowarow();
		}

		protected override void OnViewUstawOstatnieZapytanie(object sender, EventArgs e)
		{
			base.OnViewUstawOstatnieZapytanie(sender, e);
			View.Kryteria = State[StateConstants.CRITERIAS] as KryteriaZapytaniaListyTowarow ?? new KryteriaZapytaniaListyTowarow();
		}

		#endregion
		#region Raising Events

		protected void OnWyszukajListeTowarow()
		{
			State[StateConstants.CRITERIAS] = View.Kryteria;
			if (WyszukajListeTowarow != null)
			{
				WyszukajListeTowarow(this, EventArgs.Empty);
			}
		}

		#endregion
	}
}
