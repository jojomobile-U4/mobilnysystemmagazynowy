using System;
using System.Collections.Generic;
using System.Text;

namespace PrzesuniecieModule.ListaTowarow.Search
{
    public class KryteriaZapytaniaListyTowarow
    {
        #region Private Fields

        private string symbol;
        private string lokalizacja;
        private string abc;

        #endregion
        #region Constructors

		public KryteriaZapytaniaListyTowarow()
		{
				
		}

        public KryteriaZapytaniaListyTowarow(String lokalizacja)
        {
            this.lokalizacja = lokalizacja;
        }

		public KryteriaZapytaniaListyTowarow(string symbol, string lokalizacja, string abc)
		{
			this.symbol = abc;
            this.lokalizacja = lokalizacja;
            this.abc = abc;
		}

        #endregion
        #region Properties

        public string Symbol
        {
            get { return symbol; }
            set { symbol = value; }
        }

        public string Lokalizacja
        {
            get { return lokalizacja; }
            set { lokalizacja = value; }
        }

        public string Abc
        {
            get { return abc; }
            set { abc = value; }
        }

        #endregion

    }
}
