#region

using Common.SearchForm;

#endregion

namespace PrzesuniecieModule.ListaTowarow.Search
{
    public interface IListaTowarowSearchView : ISearchForm
    {
        KryteriaZapytaniaListyTowarow Kryteria { get; set; }
    }
}