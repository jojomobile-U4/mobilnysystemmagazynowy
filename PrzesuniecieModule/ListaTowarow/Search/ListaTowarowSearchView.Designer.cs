using Common.Components;

namespace PrzesuniecieModule.ListaTowarow.Search
{
	partial class ListaTowarowSearchView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbSymbol = new System.Windows.Forms.TextBox();
            this.lbSymbol = new System.Windows.Forms.Label();
            this.tbLokalizacja = new System.Windows.Forms.TextBox();
            this.tbAbc = new System.Windows.Forms.TextBox();
            this.lbLokalizacja = new System.Windows.Forms.Label();
            this.lbAbc = new System.Windows.Forms.Label();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // btnOstatnieZapytanie
            // 
            this.btnOstatnieZapytanie.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Location = new System.Drawing.Point(0, 272);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 1;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "WYSZUKIWANIE POZYCJI MR";
            // 
            // tbSymbol
            // 
            this.tbSymbol.Location = new System.Drawing.Point(73, 21);
            this.tbSymbol.Name = "tbSymbol";
            this.tbSymbol.Size = new System.Drawing.Size(164, 21);
            this.tbSymbol.TabIndex = 35;
            // 
            // lbSymbol
            // 
            this.lbSymbol.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbol.Location = new System.Drawing.Point(3, 21);
            this.lbSymbol.Name = "lbSymbol";
            this.lbSymbol.Size = new System.Drawing.Size(64, 21);
            this.lbSymbol.Text = "Symbol:";
            // 
            // tbLokalizacja
            // 
            this.tbLokalizacja.Location = new System.Drawing.Point(73, 48);
            this.tbLokalizacja.Name = "tbLokalizacja";
            this.tbLokalizacja.Size = new System.Drawing.Size(164, 21);
            this.tbLokalizacja.TabIndex = 37;
            // 
            // tbAbc
            // 
            this.tbAbc.Location = new System.Drawing.Point(73, 75);
            this.tbAbc.Name = "tbAbc";
            this.tbAbc.Size = new System.Drawing.Size(164, 21);
            this.tbAbc.TabIndex = 38;
            // 
            // lbLokalizacja
            // 
            this.lbLokalizacja.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbLokalizacja.Location = new System.Drawing.Point(3, 48);
            this.lbLokalizacja.Name = "lbLokalizacja";
            this.lbLokalizacja.Size = new System.Drawing.Size(64, 21);
            this.lbLokalizacja.Text = "Lokalizacja:";
            // 
            // lbAbc
            // 
            this.lbAbc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbAbc.Location = new System.Drawing.Point(3, 75);
            this.lbAbc.Name = "lbAbc";
            this.lbAbc.Size = new System.Drawing.Size(64, 21);
            this.lbAbc.Text = "ABC:";
            // 
            // ListaTowarowSearchView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lbAbc);
            this.Controls.Add(this.lbLokalizacja);
            this.Controls.Add(this.tbAbc);
            this.Controls.Add(this.tbLokalizacja);
            this.Controls.Add(this.lbTytul);
            this.Controls.Add(this.lbSymbol);
            this.Controls.Add(this.tbSymbol);
            this.Name = "ListaTowarowSearchView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.tbSymbol, 0);
            this.Controls.SetChildIndex(this.lbSymbol, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.tbLokalizacja, 0);
            this.Controls.SetChildIndex(this.tbAbc, 0);
            this.Controls.SetChildIndex(this.lbLokalizacja, 0);
            this.Controls.SetChildIndex(this.lbAbc, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private MSMLabel lbTytul;
		private System.Windows.Forms.TextBox tbSymbol;
		private System.Windows.Forms.Label lbSymbol;
        private System.Windows.Forms.TextBox tbLokalizacja;
        private System.Windows.Forms.TextBox tbAbc;
        private System.Windows.Forms.Label lbLokalizacja;
        private System.Windows.Forms.Label lbAbc;
	}
}
