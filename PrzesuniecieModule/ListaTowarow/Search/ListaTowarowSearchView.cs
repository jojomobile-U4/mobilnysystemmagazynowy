#region

using Common.SearchForm;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace PrzesuniecieModule.ListaTowarow.Search
{
    [SmartPart]
    public partial class ListaTowarowSearchView : SearchForm, IListaTowarowSearchView
    {
        #region Constructors

        public ListaTowarowSearchView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #endregion

        #region IListaTowarowSearchView Members

        public KryteriaZapytaniaListyTowarow Kryteria
        {
            get { return new KryteriaZapytaniaListyTowarow(tbSymbol.Text, tbLokalizacja.Text, tbAbc.Text); }
            set
            {
                if (value != null)
                {
                    tbSymbol.Text = value.Symbol;
                    tbLokalizacja.Text = value.Lokalizacja;
                    tbAbc.Text = value.Abc;
                }
                else
                {
                    tbSymbol.Text = null;
                    tbLokalizacja.Text = null;
                    tbAbc.Text = null;
                }
            }
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);

            tbSymbol.ReadOnly = navigationState;
            tbAbc.ReadOnly = navigationState;
            tbLokalizacja.ReadOnly = navigationState;
        }

        #endregion
    }
}