using System;
using System.Windows.Forms;
using Common;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

namespace PrzesuniecieModule.ListaTowarow
{
    [SmartPart]
    public partial class ListaTowarowView : ViewBase, IListaTowarowView
    {       
        
        #region Constructors

        public ListaTowarowView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #endregion
        #region Public Events

        public event EventHandler Powrot;
        public event EventHandler Szukaj;
        public event EventHandler PrzejdzDoNastepnejPozycji;
        public event EventHandler Poprzedni;
        public event EventHandler Nastepny;
        public event EventHandler Usun;
        public event EventHandler Przesun;

        #endregion
        #region Properties

        public String NazwaTowaru
        {
            set { tbNazwaTowaru.Text = value; }
        }

        public String Jm
        {
            set { tbJM.Text = value; }
        }

        public DataModel.ListaTowarow DataSource
        {
            set
            {
                lsTowary.Items.Clear();
                tbLP.Text = "";
                if (value != null && value.Lista != null && value.Lista.Length > 0)
                {
                    tbLP.Text = value.LiczbaPozycji.ToString();
                    ListViewItem listViewItem;
                    foreach (DataModel.Towar towar in value.Lista)
                    {
                        listViewItem = lsTowary.Items.Add(new ListViewItem(towar.Lp.ToString()));
                        listViewItem.SubItems.Add(towar.Symbol);
                        listViewItem.SubItems.Add(towar.Lokalizacja);
                        listViewItem.SubItems.Add(towar.Ilosc.ToString("0.000"));
                        listViewItem.SubItems.Add(towar.Abc);
                        listViewItem.Tag = towar.Id;
                    }
                }
                lsTowary.Focus();
                if (lsTowary.Items.Count > 0)
                {
                    lsTowary.Items[0].Selected = true;
                }
            }
        }

        public long? IdAktualnegoTowaru
        {
            get
            {
                if ((lsTowary != null) && (lsTowary.SelectedIndices.Count > 0))
                {
                    return lsTowary.Items[lsTowary.SelectedIndices[0]].Tag as long?;
                }
                else
                {
                    return null;
                }
            }
        }

        #endregion
        #region Private Methods

        private void btnSzukaj_Click(object sender, EventArgs e)
        {
            if (Szukaj != null)
            {
                Szukaj(this, EventArgs.Empty);
            }
        }

        private void btnPowrot_Click(object sender, EventArgs e)
        {
            if (Powrot != null)
            {
                Powrot(this, EventArgs.Empty);
            }
        }

        private void lsTowary_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PrzejdzDoNastepnejPozycji != null)
            {
                PrzejdzDoNastepnejPozycji(this, EventArgs.Empty);
            }
        }

        protected void OnPoprzedni(object sender, EventArgs e)
        {
            if (Poprzedni != null)
            {
                Poprzedni(sender, EventArgs.Empty);
            }
        }

        protected void OnNastepny(object sender, EventArgs e)
        {
            if (Nastepny != null)
            {
                Nastepny(sender, EventArgs.Empty);
            }
        }

        private void btnUsun_Click(object sender, EventArgs e)
        {
            if (Usun != null)
            {
                Usun(sender, EventArgs.Empty);
            }
        }

        private void btnPrzesun_Click(object sender, EventArgs e)
        {
            if (Przesun != null)
            {
                Przesun(sender, EventArgs.Empty);
            }
        }

        #endregion
    }
}
