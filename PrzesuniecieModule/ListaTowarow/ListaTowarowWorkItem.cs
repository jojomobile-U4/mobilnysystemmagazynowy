#region

using System;
using System.Collections.Generic;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using PrzesuniecieModule.DataModel;
using PrzesuniecieModule.ListaTowarow.Search;
using PrzesuniecieModule.Services;

#endregion

namespace PrzesuniecieModule.ListaTowarow
{
    public class ListaTowarowWorkItem : WorkItemBase
    {
        public void Show(IWorkspace parentWorkspace)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                var listaTowarowViewPresenter = Items.Get<ListaTowarowViewPresenter>(ItemsConstants.LISTA_TOWAROW_PRESENTER);
                MainWorkspace.Show(listaTowarowViewPresenter.View);
                Activate();
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void Szukaj()
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                var searchPresenter = Items.Get<ListaTowarowSearchViewPresenter>(ItemsConstants.LISTA_TOWAROW_SEARCH_PRESENTER);
                searchPresenter.View.Kryteria = null;
                Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                MainWorkspace.Show(searchPresenter.View);
                Activate();
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        [EventSubscription(EventBrokerConstants.WYSZUKAJ_LISTE_TOWAROW)]
        public void OnSzukajListeTowarow(object sender, EventArgs e)
        {
            FunctionsHelper.SetCursorWait();

            var listaTowarowViewPresenter = Items.Get<ListaTowarowViewPresenter>(ItemsConstants.LISTA_TOWAROW_PRESENTER);

            listaTowarowViewPresenter.ResetujStronicowanie();
            listaTowarowViewPresenter.ZaladujDaneDoWidoku(PobierzListeTowarow(null));
            MainWorkspace.Show(listaTowarowViewPresenter.View);
            Activate();
        }

        public DataModel.ListaTowarow PobierzListeTowarow(int? numerStrony)
        {
            DataModel.ListaTowarow listaTowarow;
            try
            {
                FunctionsHelper.SetCursorWait();
                var kryteria = State[StateConstants.CRITERIAS] as KryteriaZapytaniaListyTowarow;

                listaTowarow = Services.Get<IPrzesuniecieService>(true).PobierzListeTowarow(kryteria, numerStrony);

                FunctionsHelper.SetCursorDefault();
                if (RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), BLAD, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE].ToString());
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
            return listaTowarow;
        }

        public void UsunTowarZListy(long idTowaru)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                Services.Get<IPrzesuniecieService>(true).UsunZListyTowarow(idTowaru);

                if (!RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(StatusOperacji.SUCCESS))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), "UsuniÍcie towaru z listy",
                                   RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE].ToString());
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public long? UtowrzDokumentMR(long idTowaru, long? nosnId)
        {
            long? idDokumentu;
            try
            {
                FunctionsHelper.SetCursorWait();

                idDokumentu = Services.Get<IPrzesuniecieService>(true).UtworzDokumentMRWgIdTowaruDoPrzesuniecia(idTowaru, nosnId);

                FunctionsHelper.SetCursorDefault();
                if (!RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(StatusOperacji.SUCCESS))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), "Nowe przesuniÍcie",
                                   RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE].ToString());
                    return null;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }

            return idDokumentu;
        }

        public bool MoznaJednoznacznieOkreslicNosnik(long? painId, string kodLokalizacji, float? ilosc)
        {
            var moznaOkreslic = Services.Get<IPrzesuniecieService>(true).MoznaJednoznacznieOkreslicNosnik(painId,
                                                                                                    kodLokalizacji,
                                                                                                    ilosc);
            if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, BLAD, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
            }

            return moznaOkreslic;
        }

        public List<Nosnik> PobierzListeDostepnychNosnikow(long? painId, string kodLokalizacji, float? ilosc)
        {
            FunctionsHelper.SetCursorWait();
            var listaNosnikow = Parent.Services.Get<IPrzesuniecieService>(true).PobierzListeDostepnychNosnikow(painId, kodLokalizacji, ilosc);

            if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, BLAD, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
            }
            FunctionsHelper.SetCursorDefault();

            return listaNosnikow;
        }

        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ)]
        public void BarCodeRead(object sender, EventArgs e)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                if (Status == WorkItemStatus.Active &&
                    RootWorkItem.State[StateConstants.BAR_CODE] != null)
                {
                    var kodKreskowy = RootWorkItem.State[StateConstants.BAR_CODE] as string;
                    RootWorkItem.State[StateConstants.BAR_CODE] = null;
                    State[StateConstants.CRITERIAS] = new KryteriaZapytaniaListyTowarow(kodKreskowy);
                    OnSzukajListeTowarow(this, EventArgs.Empty);
                    Activate();
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

    }
}