using System;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using PrzesuniecieModule.DataModel;
using PrzesuniecieModule.ListaNosnikow;

namespace PrzesuniecieModule.ListaTowarow
{
    public class ListaTowarowViewPresenter : PresenterBase
    {
        [EventPublication(EventBrokerConstants.WYSZUKAJ_LISTE_DOKUMENTOW_MR)]
        public event EventHandler WyszukajListeDokumentowMR;

        #region Constants

        private const int NASTEPNA = 1;
        private const int POPRZEDNIA = -1;
        private const int PIERWSZA = 0;

        #endregion
        #region Private Fields

        private DataModel.ListaTowarow listaTowarow;
        private int numerStrony;
        private int zmianaStrony;
        private int startoweLp;
        private bool NalezyPrzesunacTowar;

        #endregion
        #region Constructors

        public ListaTowarowViewPresenter(IListaTowarowView listaTowarowView)
            : base(listaTowarowView)
        {
            ResetujStronicowanie();
            startoweLp = 1;
        }

        #endregion

        public IListaTowarowView View
        {
            get { return m_view as IListaTowarowView; }
        }

        protected ListaTowarowWorkItem MyWorkItem
        {
            get { return WorkItem as ListaTowarowWorkItem; }
        }


        protected override void AttachView()
        {
            View.Powrot += ViewPowrot;
            View.Szukaj += ViewSzukaj;
            View.PrzejdzDoNastepnejPozycji += ViewPrzejdzDoNastepnejPozycji;
            View.Poprzedni += ViewPoprzedni;
            View.Nastepny += ViewNastepny;
            View.Usun += ViewUsun;
            View.Przesun += ViewPrzesun;
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.D1:
                    e.Handled = true;
                    ViewSzukaj(this, EventArgs.Empty);
                    break;
                case Keys.Enter:
                    e.Handled = true;
                    ViewPrzesun(this, EventArgs.Empty);
                    break;
                case Keys.D2:
                    e.Handled = true;
                    ViewUsun(this, EventArgs.Empty);
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    ViewPowrot(this, EventArgs.Empty);
                    break;
                case Keys.Left:
                    e.Handled = true;
                    ViewPoprzedni(this, EventArgs.Empty);
                    break;
                case Keys.Right:
                    e.Handled = true;
                    ViewNastepny(this, EventArgs.Empty);
                    break;
            }
        }

        #region View events handlers

        void ViewPowrot(object sender, EventArgs e)
        {
            MyWorkItem.MainWorkspace.Close(View);
        }

        void ViewSzukaj(object sender, EventArgs e)
        {
            MyWorkItem.Szukaj();
        }

        void ViewPrzejdzDoNastepnejPozycji(object sender, EventArgs e)
        {
            AktualizujWidok();
        }

        void ViewPoprzedni(object sender, EventArgs e)
        {
            if (listaTowarow.Lista != null)
            {
                if (numerStrony > 0)
                {
                    ZmienStrone(POPRZEDNIA);
                }
            }
        }

        void ViewNastepny(object sender, EventArgs e)
        {
            if (listaTowarow != null &&
                listaTowarow.Lista != null &&
                listaTowarow.Lista.Length == MyWorkItem.Configuration.PrzesuniecieWS.WielkoscStrony)
            {
                ZmienStrone(NASTEPNA);
            }
        }

        void ViewUsun(object sender, EventArgs e)
        {
            if (View.IdAktualnegoTowaru != null)
            {
                MyWorkItem.UsunTowarZListy((long)View.IdAktualnegoTowaru);
                ZaladujDaneDoWidoku(MyWorkItem.PobierzListeTowarow(numerStrony));
            }
        }

        void ViewPrzesun(object sender, EventArgs e)
        {
            if (View.IdAktualnegoTowaru != null)
            {
                var moznaOkreslicNosnik = MyWorkItem.MoznaJednoznacznieOkreslicNosnik(PainId(View.IdAktualnegoTowaru),
                                                                       Lokalizacja(View.IdAktualnegoTowaru), Ilosc(View.IdAktualnegoTowaru));

                if (StatusOperacji.ERROR.Equals(MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                {
                    return;
                }

                if (!moznaOkreslicNosnik)
                {
                    var listaNosnikowPresenter =
                        MyWorkItem.Items.Get<ListaNosnikowPresenter>(
                            ItemsConstants.PRZESUNIECIE_LISTA_NOSNIKOW_PRESENTER);
                    if (listaNosnikowPresenter != null)
                    {
                        listaNosnikowPresenter.WyswietlListe(PainId(View.IdAktualnegoTowaru),
                                                             Lokalizacja(View.IdAktualnegoTowaru),
                                                             0);
                        MyWorkItem.MainWorkspace.Show(listaNosnikowPresenter.View);
                        NalezyPrzesunacTowar = true;
                    }
                    return;
                }

                Przesun((long)View.IdAktualnegoTowaru, null);
            }
        }

        [EventSubscription(EventBrokerConstants.WYBRANO_NOSNIK)]
        public void WybranoNosnik(Object sender, EventArgs e)
        {
            if (NalezyPrzesunacTowar)
            {
                NalezyPrzesunacTowar = false;
                Przesun((long)View.IdAktualnegoTowaru, WorkItem.State[StateConstants.IDENTYFIKATOR_NOSNIKA] as long?);
            }
        }

        void Przesun(long idTowaru, long? nosnId)
        {
            var idDokumentu = MyWorkItem.UtowrzDokumentMR(idTowaru, nosnId);
            if (idDokumentu == null) return;
            var kryteria = new RealizacjaMR.Search.KryteriaZapytaniaRealizacjiMR { Id = idDokumentu };
            var realizacjaMRWorkItem
                = MyWorkItem.Parent.WorkItems.Get<RealizacjaMR.RealizacjaMRWorkItem>(WorkItemsConstants.REALIZACJA_MR_WORKITEM);
            realizacjaMRWorkItem.State[StateConstants.CRITERIAS] = kryteria;

            if (WyszukajListeDokumentowMR != null)
            {
                WyszukajListeDokumentowMR(this, EventArgs.Empty);
            }

            MyWorkItem.MainWorkspace.Close(View);
        }

        #endregion

        internal void ResetujStronicowanie()
        {
            numerStrony = PIERWSZA;
            zmianaStrony = NASTEPNA;
            startoweLp = 1;
        }


        public void ZaladujDaneDoWidoku(DataModel.ListaTowarow lista)
        {
            listaTowarow = lista ?? new DataModel.ListaTowarow();
            View.DataSource = listaTowarow;

            AktualizujWidok();
        }

        #region Private Methods

        private void AktualizujWidok()
        {
            if (View.IdAktualnegoTowaru != null)
            {
                View.NazwaTowaru = NazwaTowaru(View.IdAktualnegoTowaru);
                View.Jm = Jm(View.IdAktualnegoTowaru);
            }
            else
            {
                View.NazwaTowaru = "";
                View.Jm = "";
            }
        }

        private String NazwaTowaru(long? id)
        {
            if (listaTowarow != null && listaTowarow.Lista != null)
            {
                foreach (Towar towar in listaTowarow.Lista)
                {
                    if ((towar != null) && (towar.Id == id))
                    {
                        return towar.NazwaTowaru;
                    }
                }
            }
            return null;
        }

        private String Jm(long? id)
        {
            if (listaTowarow != null && listaTowarow.Lista != null)
            {
                foreach (Towar towar in listaTowarow.Lista)
                {
                    if ((towar != null) && (towar.Id == id))
                    {
                        return towar.Jm;
                    }
                }
            }
            return null;
        }

        private float Ilosc(long? id)
        {
            if (listaTowarow != null && listaTowarow.Lista != null)
            {
                foreach (Towar towar in listaTowarow.Lista)
                {
                    if ((towar != null) && (towar.Id == id))
                    {
                        return towar.Ilosc;
                    }
                }
            }
            return 0;
        }

        private long? PainId(long? id)
        {
            if (listaTowarow != null && listaTowarow.Lista != null)
            {
                foreach (Towar towar in listaTowarow.Lista)
                {
                    if ((towar != null) && (towar.Id == id))
                    {
                        return towar.PainId;
                    }
                }
            }
            return 0;
        }

        private string Lokalizacja(long? id)
        {
            if (listaTowarow != null && listaTowarow.Lista != null)
            {
                foreach (Towar towar in listaTowarow.Lista)
                {
                    if ((towar != null) && (towar.Id == id))
                    {
                        return towar.Lokalizacja;
                    }
                }
            }
            return null;
        }

        private void ZmienStrone(int kierunek)
        {
            numerStrony += kierunek;
            zmianaStrony = kierunek;
            startoweLp += kierunek * MyWorkItem.Configuration.PrzesuniecieWS.WielkoscStrony;
            ZaladujDaneDoWidoku(MyWorkItem.PobierzListeTowarow(numerStrony));
        }
        #endregion

    }
}
