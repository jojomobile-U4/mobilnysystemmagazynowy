
using Common.Components;

namespace PrzesuniecieModule.ListaTowarow
{
    partial class ListaTowarowView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu viewMenu;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.viewMenu = new System.Windows.Forms.MainMenu();
            this.lbTytul = new Common.Components.MSMLabel();
            this.lsTowary = new Common.Components.MSMListView();
            this.chLP = new System.Windows.Forms.ColumnHeader();
            this.chSymbol = new System.Windows.Forms.ColumnHeader();
            this.chPobranie = new System.Windows.Forms.ColumnHeader();
            this.chIlosc = new System.Windows.Forms.ColumnHeader();
            this.chABC = new System.Windows.Forms.ColumnHeader();
            this.tbNazwaTowaru = new System.Windows.Forms.TextBox();
            this.lbNazwaTowaru = new System.Windows.Forms.Label();
            this.tbLP = new System.Windows.Forms.TextBox();
            this.lbLP = new System.Windows.Forms.Label();
            this.tbJM = new System.Windows.Forms.TextBox();
            this.lbJM = new System.Windows.Forms.Label();
            this.btnPrzesun = new System.Windows.Forms.Button();
            this.btnUsun = new System.Windows.Forms.Button();
            this.btnPowrot = new System.Windows.Forms.Button();
            this.leftRightControl = new Common.Components.LeftRightControl();
            this.btnSzukaj = new System.Windows.Forms.Button();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnSzukaj);
            this.pnlNavigation.Controls.Add(this.btnPowrot);
            this.pnlNavigation.Controls.Add(this.btnUsun);
            this.pnlNavigation.Controls.Add(this.btnPrzesun);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 253);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 47);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 34;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "PRZESUNI�CIE-LISTA TOWAR�W";
            // 
            // lsTowary
            // 
            this.lsTowary.Columns.Add(this.chLP);
            this.lsTowary.Columns.Add(this.chSymbol);
            this.lsTowary.Columns.Add(this.chPobranie);
            this.lsTowary.Columns.Add(this.chIlosc);
            this.lsTowary.Columns.Add(this.chABC);
            this.lsTowary.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lsTowary.FullRowSelect = true;
            this.lsTowary.Location = new System.Drawing.Point(1, 29);
            this.lsTowary.Name = "lsTowary";
            this.lsTowary.Size = new System.Drawing.Size(237, 164);
            this.lsTowary.TabIndex = 0;
            this.lsTowary.View = System.Windows.Forms.View.Details;
            this.lsTowary.SelectedIndexChanged += new System.EventHandler(this.lsTowary_SelectedIndexChanged);
            // 
            // chLP
            // 
            this.chLP.Text = "LP";
            this.chLP.Width = 20;
            // 
            // chSymbol
            // 
            this.chSymbol.Text = "Indeks";
            this.chSymbol.Width = 70;
            // 
            // chPobranie
            // 
            this.chPobranie.Text = "Pobranie";
            this.chPobranie.Width = 70;
            // 
            // chIlosc
            // 
            this.chIlosc.Text = "Ilo��";
            this.chIlosc.Width = 40;
            // 
            // chABC
            // 
            this.chABC.Text = "ABC";
            this.chABC.Width = 25;
            // 
            // tbNazwaTowaru
            // 
            this.tbNazwaTowaru.Location = new System.Drawing.Point(1, 209);
            this.tbNazwaTowaru.Multiline = true;
            this.tbNazwaTowaru.Name = "tbNazwaTowaru";
            this.tbNazwaTowaru.ReadOnly = true;
            this.tbNazwaTowaru.Size = new System.Drawing.Size(237, 21);
            this.tbNazwaTowaru.TabIndex = 9;
            this.tbNazwaTowaru.TabStop = false;
            // 
            // lbNazwaTowaru
            // 
            this.lbNazwaTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbNazwaTowaru.Location = new System.Drawing.Point(1, 193);
            this.lbNazwaTowaru.Name = "lbNazwaTowaru";
            this.lbNazwaTowaru.Size = new System.Drawing.Size(100, 21);
            this.lbNazwaTowaru.Text = "Nazwa towaru:";
            // 
            // tbLP
            // 
            this.tbLP.Location = new System.Drawing.Point(83, 232);
            this.tbLP.Name = "tbLP";
            this.tbLP.ReadOnly = true;
            this.tbLP.Size = new System.Drawing.Size(34, 21);
            this.tbLP.TabIndex = 14;
            this.tbLP.TabStop = false;
            // 
            // lbLP
            // 
            this.lbLP.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbLP.Location = new System.Drawing.Point(2, 234);
            this.lbLP.Name = "lbLP";
            this.lbLP.Size = new System.Drawing.Size(79, 17);
            this.lbLP.Text = "Liczba pozycji:";
            // 
            // tbJM
            // 
            this.tbJM.Location = new System.Drawing.Point(204, 232);
            this.tbJM.Name = "tbJM";
            this.tbJM.ReadOnly = true;
            this.tbJM.Size = new System.Drawing.Size(34, 21);
            this.tbJM.TabIndex = 13;
            this.tbJM.TabStop = false;
            // 
            // lbJM
            // 
            this.lbJM.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbJM.Location = new System.Drawing.Point(169, 234);
            this.lbJM.Name = "lbJM";
            this.lbJM.Size = new System.Drawing.Size(29, 19);
            this.lbJM.Text = "J.m.:";
            // 
            // btnPrzesun
            // 
            this.btnPrzesun.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPrzesun.Location = new System.Drawing.Point(68, 23);
            this.btnPrzesun.Name = "btnPrzesun";
            this.btnPrzesun.Size = new System.Drawing.Size(85, 20);
            this.btnPrzesun.TabIndex = 0;
            this.btnPrzesun.Text = "&Ret Przesu�";
            this.btnPrzesun.Click += new System.EventHandler(this.btnPrzesun_Click);
            // 
            // btnUsun
            // 
            this.btnUsun.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnUsun.Location = new System.Drawing.Point(3, 23);
            this.btnUsun.Name = "btnUsun";
            this.btnUsun.Size = new System.Drawing.Size(60, 20);
            this.btnUsun.TabIndex = 0;
            this.btnUsun.Text = "&2 Usu�";
            this.btnUsun.Click += new System.EventHandler(this.btnUsun_Click);
            // 
            // btnPowrot
            // 
            this.btnPowrot.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPowrot.Location = new System.Drawing.Point(158, 23);
            this.btnPowrot.Name = "btnPowrot";
            this.btnPowrot.Size = new System.Drawing.Size(78, 20);
            this.btnPowrot.TabIndex = 0;
            this.btnPowrot.Text = "&Esc Powr�t";
            this.btnPowrot.Click += new System.EventHandler(this.btnPowrot_Click);
            // 
            // leftRightControl
            // 
            this.leftRightControl.BackColor = System.Drawing.SystemColors.Desktop;
            this.leftRightControl.Location = new System.Drawing.Point(208, 0);
            this.leftRightControl.Name = "leftRightControl";
            this.leftRightControl.NextEnabled = true;
            this.leftRightControl.PreviousEnabled = true;
            this.leftRightControl.Size = new System.Drawing.Size(32, 16);
            this.leftRightControl.TabIndex = 35;
            this.leftRightControl.TabStop = false;
            this.leftRightControl.Next += new System.EventHandler(this.OnNastepny);
            this.leftRightControl.Previous += new System.EventHandler(this.OnPoprzedni);
            // 
            // btnSzukaj
            // 
            this.btnSzukaj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnSzukaj.Location = new System.Drawing.Point(3, 1);
            this.btnSzukaj.Name = "btnSzukaj";
            this.btnSzukaj.Size = new System.Drawing.Size(60, 20);
            this.btnSzukaj.TabIndex = 0;
            this.btnSzukaj.Text = "&1 Szukaj";
            this.btnSzukaj.Click += new System.EventHandler(this.btnSzukaj_Click);
            // 
            // ListaTowarowView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.leftRightControl);
            this.Controls.Add(this.tbLP);
            this.Controls.Add(this.lbLP);
            this.Controls.Add(this.tbJM);
            this.Controls.Add(this.lbJM);
            this.Controls.Add(this.tbNazwaTowaru);
            this.Controls.Add(this.lbNazwaTowaru);
            this.Controls.Add(this.lsTowary);
            this.Controls.Add(this.lbTytul);
            this.Name = "ListaTowarowView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lsTowary, 0);
            this.Controls.SetChildIndex(this.lbNazwaTowaru, 0);
            this.Controls.SetChildIndex(this.tbNazwaTowaru, 0);
            this.Controls.SetChildIndex(this.lbJM, 0);
            this.Controls.SetChildIndex(this.tbJM, 0);
            this.Controls.SetChildIndex(this.lbLP, 0);
            this.Controls.SetChildIndex(this.tbLP, 0);
            this.Controls.SetChildIndex(this.leftRightControl, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MSMLabel lbTytul;
        private MSMListView lsTowary;
        private System.Windows.Forms.TextBox tbNazwaTowaru;
        private System.Windows.Forms.Label lbNazwaTowaru;
        private System.Windows.Forms.TextBox tbLP;
        private System.Windows.Forms.Label lbLP;
        private System.Windows.Forms.TextBox tbJM;
        private System.Windows.Forms.Label lbJM;
        private System.Windows.Forms.Button btnPrzesun;
        private System.Windows.Forms.Button btnUsun;
        private System.Windows.Forms.ColumnHeader chLP;
        private System.Windows.Forms.ColumnHeader chSymbol;
        private System.Windows.Forms.ColumnHeader chPobranie;
        private System.Windows.Forms.ColumnHeader chIlosc;
        private System.Windows.Forms.ColumnHeader chABC;
        private System.Windows.Forms.Button btnPowrot;
        private LeftRightControl leftRightControl;
        private System.Windows.Forms.Button btnSzukaj;
    }
}
