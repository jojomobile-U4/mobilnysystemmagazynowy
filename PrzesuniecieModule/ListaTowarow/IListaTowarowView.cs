using System;
using Common.Base;
using PrzesuniecieModule.PrzesuniecieProxy;

namespace PrzesuniecieModule.ListaTowarow
{
    public interface IListaTowarowView : IViewBase
    {
        #region Events

        event EventHandler Powrot;
        event EventHandler Szukaj;
        event EventHandler PrzejdzDoNastepnejPozycji;
        event EventHandler Poprzedni;
        event EventHandler Nastepny;
        event EventHandler Usun;
        event EventHandler Przesun;

        #endregion
        #region Properties

        DataModel.ListaTowarow DataSource { set; }
        String NazwaTowaru { set; }
        String Jm { set; }
        long? IdAktualnegoTowaru { get; }

        #endregion
    }
}
