#region

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using Common;
using Common.Base;
using OpenNETCF.Web.Services2;
using PrzesuniecieModule.PrzesuniecieProxy;

#endregion

namespace PrzesuniecieModule.ProxyWrappers
{
    public class WsPrzesuniecieWrap : WsPrzesuniecie, IWSSecurity
    {
        #region Private s

        #endregion

        #region Properties

        public SecurityHeader SecurityHeader { get; set; }

        #endregion

        #region Constructors

        public WsPrzesuniecieWrap(WebServiceConfiguration configuration)
        {
            Url = configuration.Url;
        }

        #endregion

        #region Protected Methods

        protected override WebRequest GetWebRequest(Uri uri)
        {
            var request = (HttpWebRequest)base.GetWebRequest(uri);

            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;

            return request;
        }

        #endregion

        #region Methods

        [SoapDocumentMethod("urn:pobierzListeTowarowDoPrzesuniecia", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeTowarowDoPrzesunieciaResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeTowarowDoPrzesunieciaResponse pobierzListeTowarowDoPrzesuniecia([XmlElement("pobierzListeTowarowDoPrzesuniecia", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeTowarowDoPrzesunieciaWrap pobierzListeTowarowDoPrzesuniecia1)
        {
            var results = Invoke("pobierzListeTowarowDoPrzesuniecia", new object[] { pobierzListeTowarowDoPrzesuniecia1 });
            return ((pobierzListeTowarowDoPrzesunieciaResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:pobierzListeDokumentow", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeDokumentowResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeDokumentowResponse pobierzListeDokumentow([XmlElement("pobierzListeDokumentow", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeDokumentowWrap pobierzListeDokumentow1)
        {
            var results = Invoke("pobierzListeDokumentow", new object[] { pobierzListeDokumentow1 });
            return ((pobierzListeDokumentowResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:utworzDokumentMR", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("utworzDokumentMRResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public utworzDokumentMRResponse utworzDokumentMR([XmlElement("utworzDokumentMR", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] utworzDokumentMRWrap utworzDokumentMR1)
        {
            var results = Invoke("utworzDokumentMR", new object[] { utworzDokumentMR1 });
            return ((utworzDokumentMRResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:zmienIloscWageIZakonczPozycje", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("zmienIloscWageIZakonczPozycjeResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public zmienIloscWageIZakonczPozycjeResponse zmienIloscWageIZakonczPozycje([XmlElement("zmienIloscWageIZakonczPozycje", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] zmienIloscWageIZakonczPozycjeWrap zmienIloscWageIZakonczPozycje1)
        {
            var results = Invoke("zmienIloscWageIZakonczPozycje", new object[] { zmienIloscWageIZakonczPozycje1 });
            return ((zmienIloscWageIZakonczPozycjeResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:maksymalnaIloscDlaLokalizacji", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("maksymalnaIloscDlaLokalizacjiResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public maksymalnaIloscDlaLokalizacjiResponse maksymalnaIloscDlaLokalizacji([XmlElement("maksymalnaIloscDlaLokalizacji", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] maksymalnaIloscDlaLokalizacjiWrap maksymalnaIloscDlaLokalizacji1)
        {
            var results = Invoke("maksymalnaIloscDlaLokalizacji", new object[] { maksymalnaIloscDlaLokalizacji1 });
            return ((maksymalnaIloscDlaLokalizacjiResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:dodajPozycjeUPIZatwierdzMR", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("dodajPozycjeUPIZatwierdzMRResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public dodajPozycjeUPIZatwierdzMRResponse dodajPozycjeUPIZatwierdzMR([XmlElement("dodajPozycjeUPIZatwierdzMR", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] dodajPozycjeUPIZatwierdzMRWrap dodajPozycjeUPIZatwierdzMR1)
        {
            var results = Invoke("dodajPozycjeUPIZatwierdzMR", new object[] { dodajPozycjeUPIZatwierdzMR1 });
            return ((dodajPozycjeUPIZatwierdzMRResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:pobierzListeTowarowDlaLokalizacji", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeTowarowDlaLokalizacjiResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeTowarowDlaLokalizacjiResponse pobierzListeTowarowDlaLokalizacji([XmlElement("pobierzListeTowarowDlaLokalizacji", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeTowarowDlaLokalizacjiWrap pobierzListeTowarowDlaLokalizacji1)
        {
            var results = Invoke("pobierzListeTowarowDlaLokalizacji", new object[] { pobierzListeTowarowDlaLokalizacji1 });
            return ((pobierzListeTowarowDlaLokalizacjiResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:rezerwujDokument", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("rezerwujDokumentResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public rezerwujDokumentResponse rezerwujDokument([XmlElement("rezerwujDokument", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] rezerwujDokument rezerwujDokument1)
        {
            var results = Invoke("rezerwujDokument", new object[] { rezerwujDokument1 });
            return ((rezerwujDokumentResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:lokalizacjaJestWolna", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("lokalizacjaJestWolnaResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public lokalizacjaJestWolnaResponse lokalizacjaJestWolna([XmlElement("lokalizacjaJestWolna", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] lokalizacjaJestWolna lokalizacjaJestWolna1)
        {
            var results = Invoke("lokalizacjaJestWolna", new object[] { lokalizacjaJestWolna1 });
            return ((lokalizacjaJestWolnaResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:utworzDokumentMRWgIdTowaruDoPrzesuniecia", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("utworzDokumentMRWgIdTowaruDoPrzesunieciaResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public utworzDokumentMRWgIdTowaruDoPrzesunieciaResponse utworzDokumentMRWgIdTowaruDoPrzesuniecia(
            [XmlElement("utworzDokumentMRWgIdTowaruDoPrzesuniecia", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] utworzDokumentMRWgIdTowaruDoPrzesuniecia utworzDokumentMRWgIdTowaruDoPrzesuniecia1)
        {
            var results = Invoke("utworzDokumentMRWgIdTowaruDoPrzesuniecia", new object[] { utworzDokumentMRWgIdTowaruDoPrzesuniecia1 });
            return ((utworzDokumentMRWgIdTowaruDoPrzesunieciaResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:zaznaczPrzesuniecieTowaru", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("zaznaczPrzesuniecieTowaruResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public zaznaczPrzesuniecieTowaruResponse zaznaczPrzesuniecieTowaru([XmlElement("zaznaczPrzesuniecieTowaru", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] zaznaczPrzesuniecieTowaru zaznaczPrzesuniecieTowaru1)
        {
            var results = Invoke("zaznaczPrzesuniecieTowaru", new object[] { zaznaczPrzesuniecieTowaru1 });
            return ((zaznaczPrzesuniecieTowaruResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:usunZListyTowarowDoPrzesuniecia", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("usunZListyTowarowDoPrzesunieciaResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public usunZListyTowarowDoPrzesunieciaResponse usunZListyTowarowDoPrzesuniecia([XmlElement("usunZListyTowarowDoPrzesuniecia", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] usunZListyTowarowDoPrzesuniecia usunZListyTowarowDoPrzesuniecia1)
        {
            var results = Invoke("usunZListyTowarowDoPrzesuniecia", new object[] { usunZListyTowarowDoPrzesuniecia1 });
            return ((usunZListyTowarowDoPrzesunieciaResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:pobierzStanGoracy", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzStanGoracyResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzStanGoracyResponse pobierzStanGoracy([XmlElement("pobierzStanGoracy", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzStanGoracyWrap pobierzStanGoracy1)
        {
            var results = Invoke("pobierzStanGoracy", new object[] { pobierzStanGoracy1 });
            return ((pobierzStanGoracyResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:pobierzIloscWLokalizacji", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzIloscWLokalizacjiResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzIloscWLokalizacjiResponse pobierzIloscWLokalizacji([XmlElement("pobierzIloscWLokalizacji", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzIloscWLokalizacjiWrap pobierzIloscWLokalizacji1)
        {
            var results = Invoke("pobierzIloscWLokalizacji", new object[] { pobierzIloscWLokalizacji1 });
            return ((pobierzIloscWLokalizacjiResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:dodajPozycjeUP", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("dodajPozycjeUPResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public dodajPozycjeUPResponse dodajPozycjeUP([XmlElement("dodajPozycjeUP", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] dodajPozycjeUPWrap dodajPozycjeUP1)
        {
            var results = Invoke("dodajPozycjeUP", new object[] { dodajPozycjeUP1 });
            return ((dodajPozycjeUPResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:usunPozycje", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("usunPozycjeResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public usunPozycjeResponse usunPozycje([XmlElement("usunPozycje", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] usunPozycjeWrap usunPozycje1)
        {
            var results = Invoke("usunPozycje", new object[] { usunPozycje1 });
            return ((usunPozycjeResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:pobierzListePozycjiUP", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListePozycjiUPResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListePozycjiUPResponse pobierzListePozycjiUP([XmlElement("pobierzListePozycjiUP", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListePozycjiUPWrap pobierzListePozycjiUP1)
        {
            var results = Invoke("pobierzListePozycjiUP", new object[] { pobierzListePozycjiUP1 });
            return ((pobierzListePozycjiUPResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:zmienPozycje", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("zmienPozycjeResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public zmienPozycjeResponse zmienPozycje([XmlElement("zmienPozycje", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] zmienPozycjeWrap zmienPozycje1)
        {
            var results = Invoke("zmienPozycje", new object[] { zmienPozycje1 });
            return ((zmienPozycjeResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:zakonczPozycjeUP", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("zakonczPozycjeUPResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public zakonczPozycjeUPResponse zakonczPozycjeUP([XmlElement("zakonczPozycjeUP", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] zakonczPozycjeUPWrap zakonczPozycjeUP1)
        {
            var results = Invoke("zakonczPozycjeUP", new object[] { zakonczPozycjeUP1 });
            return ((zakonczPozycjeUPResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:zatwierdzPobranie", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("zatwierdzPobranieResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public zatwierdzPobranieResponse zatwierdzPobranie([XmlElement("zatwierdzPobranie", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] zatwierdzPobranieWrap zatwierdzPobranie1)
        {
            var results = Invoke("zatwierdzPobranie", new object[] { zatwierdzPobranie1 });
            return ((zatwierdzPobranieResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:uruchomRaport", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("uruchomRaportResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public uruchomRaportResponse uruchomRaport([XmlElement("uruchomRaport", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] uruchomRaportWrap uruchomRaport1)
        {
            var results = Invoke("uruchomRaport", new object[] { uruchomRaport1 });
            return ((uruchomRaportResponse)(results[0]));
        }

        [SoapDocumentMethodAttribute("urn:moznaJednoznacznieOkreslicNosnik", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElementAttribute("moznaJednoznacznieOkreslicNosnikResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public moznaJednoznacznieOkreslicNosnikResponse moznaJednoznacznieOkreslicNosnik([XmlElementAttribute("moznaJednoznacznieOkreslicNosnik", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] moznaJednoznacznieOkreslicNosnik moznaJednoznacznieOkreslicNosnik1)
        {
            object[] results = Invoke("moznaJednoznacznieOkreslicNosnik", new object[] {
                        moznaJednoznacznieOkreslicNosnik1});
            return ((moznaJednoznacznieOkreslicNosnikResponse)(results[0]));
        }

        [SoapDocumentMethodAttribute("urn:pobierzListeDostepnychNosnDlaPainWMijs", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElementAttribute("pobierzListeDostepnychNosnDlaPainWMijsResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeDostepnychNosnDlaPainWMijsResponse pobierzListeDostepnychNosnDlaPainWMijs([XmlElementAttribute("pobierzListeDostepnychNosnDlaPainWMijs", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeDostepnychNosnDlaPainWMijs pobierzListeDostepnychNosnDlaPainWMijs1)
        {
            object[] results = Invoke("pobierzListeDostepnychNosnDlaPainWMijs", new object[] {
                        pobierzListeDostepnychNosnDlaPainWMijs1});
            return ((pobierzListeDostepnychNosnDlaPainWMijsResponse)(results[0]));
        }

        #endregion

        #region Nested type: dodajPozycjeUPIZatwierdzMRWrap

        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class dodajPozycjeUPIZatwierdzMRWrap : dodajPozycjeUPIZatwierdzMR
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? idDokumentu
            {
                get { return base.idDokumentu; }
                set { base.idDokumentu = value; }
            }

            /// <remarks/>
            [XmlElement("pozycje", IsNullable = true)]
            public Pozycja[] pozycje
            {
                get { return base.pozycje; }
                set { base.pozycje = value; }
            }
        }

        #endregion

        #region Nested type: dodajPozycjeUPWrap

        /// <remarks/>
        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class dodajPozycjeUPWrap : dodajPozycjeUP
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public Pozycja pozycja
            {
                get { return base.pozycja; }
                set { base.pozycja = value; }
            }
        }

        #endregion

        #region Nested type: maksymalnaIloscDlaLokalizacjiWrap

        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class maksymalnaIloscDlaLokalizacjiWrap : maksymalnaIloscDlaLokalizacji
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string lokalizacja
            {
                get { return base.lokalizacja; }
                set { base.lokalizacja = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string symbol
            {
                get { return base.symbol; }
                set { base.symbol = value; }
            }
        }

        #endregion

        #region Nested type: pobierzIloscWLokalizacjiWrap

        /// <remarks/>
        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class pobierzIloscWLokalizacjiWrap : pobierzIloscWLokalizacji
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? identyfikatorZadania
            {
                get { return base.identyfikatorZadania; }
                set { base.identyfikatorZadania = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? painId
            {
                get { return base.painId; }
                set { base.painId = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string lokalizacja
            {
                get { return base.lokalizacja; }
                set { base.lokalizacja = value; }
            }
        }

        #endregion

        #region Nested type: pobierzListeDokumentowWrap

        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class pobierzListeDokumentowWrap : pobierzListeDokumentow
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? id
            {
                get { return base.id; }
                set { base.id = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? idPozycji
            {
                get { return base.idPozycji; }
                set { base.idPozycji = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string symbol
            {
                get { return base.symbol; }
                set { base.symbol = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string abc
            {
                get { return base.abc; }
                set { base.abc = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string lokalizacja
            {
                get { return base.lokalizacja; }
                set { base.lokalizacja = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string symbolTowaru
            {
                get { return base.symbolTowaru; }
                set { base.symbolTowaru = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string nazwaTowaru
            {
                get { return base.nazwaTowaru; }
                set { base.nazwaTowaru = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string uzytkownik
            {
                get { return base.uzytkownik; }
                set { base.uzytkownik = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public int? numerStrony
            {
                get { return base.numerStrony; }
                set { base.numerStrony = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public int? wielkoscStrony
            {
                get { return base.wielkoscStrony; }
                set { base.wielkoscStrony = value; }
            }
        }

        #endregion

        #region Nested type: pobierzListePozycjiUPWrap

        /// <remarks/>
        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class pobierzListePozycjiUPWrap : pobierzListePozycjiUP
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? idDokumentu
            {
                get { return base.idDokumentu; }
                set { base.idDokumentu = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? idPartii
            {
                get { return base.idPartii; }
                set { base.idPartii = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public int? numerStrony
            {
                get { return base.numerStrony; }
                set { base.numerStrony = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public int? wielkoscStrony
            {
                get { return base.wielkoscStrony; }
                set { base.wielkoscStrony = value; }
            }
        }

        #endregion

        #region Nested type: pobierzListeTowarowDlaLokalizacjiWrap

        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class pobierzListeTowarowDlaLokalizacjiWrap : pobierzListeTowarowDlaLokalizacji
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string symbol
            {
                get { return base.symbol; }
                set { base.symbol = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string lokalizacja
            {
                get { return base.lokalizacja; }
                set { base.lokalizacja = value; }
            }

            [XmlElement(IsNullable = true)]
            public int? numerStrony
            {
                get { return base.numerStrony; }
                set { base.numerStrony = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public int? wielkoscStrony
            {
                get { return base.wielkoscStrony; }
                set { base.wielkoscStrony = value; }
            }
        }

        #endregion

        #region Nested type: pobierzListeTowarowDoPrzesunieciaWrap

        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class pobierzListeTowarowDoPrzesunieciaWrap : pobierzListeTowarowDoPrzesuniecia
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? id
            {
                get { return base.id; }
                set { base.id = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string symbol
            {
                get { return base.symbol; }
                set { base.symbol = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string nazwa
            {
                get { return base.nazwa; }
                set { base.nazwa = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string abc
            {
                get { return base.abc; }
                set { base.abc = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string lokalizacja
            {
                get { return base.lokalizacja; }
                set { base.lokalizacja = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string uzytkownik
            {
                get { return base.uzytkownik; }
                set { base.uzytkownik = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public int? numerStrony
            {
                get { return base.numerStrony; }
                set { base.numerStrony = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public int? wielkoscStrony
            {
                get { return base.wielkoscStrony; }
                set { base.wielkoscStrony = value; }
            }
        }

        #endregion

        #region Nested type: pobierzStanGoracyWrap

        /// <remarks/>
        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class pobierzStanGoracyWrap : pobierzStanGoracy
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? identyfikatorZadania
            {
                get { return base.identyfikatorZadania; }
                set { base.identyfikatorZadania = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? painId
            {
                get { return base.painId; }
                set { base.painId = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string lokalizacja
            {
                get { return base.lokalizacja; }
                set { base.lokalizacja = value; }
            }
        }

        #endregion

        #region Nested type: uruchomRaportWrap

        /// <remarks/>
        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class uruchomRaportWrap : uruchomRaport
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string nazwaFormularza
            {
                get { return base.nazwaFormularza; }
                set { base.nazwaFormularza = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? idDokumentuMR
            {
                get { return base.idDokumentuMR; }
                set { base.idDokumentuMR = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? idPozycji
            {
                get { return base.idPozycji; }
                set { base.idPozycji = value; }
            }
        }

        #endregion

        #region Nested type: usunPozycjeWrap

        /// <remarks/>
        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class usunPozycjeWrap : usunPozycje
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? id
            {
                get { return base.id; }
                set { base.id = value; }
            }
        }

        #endregion

        #region Nested type: utworzDokumentMRWrap

        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class utworzDokumentMRWrap : utworzDokumentMR
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public DateTime? dataProdukcji
            {
                get { return base.dataProdukcji; }
                set { base.dataProdukcji = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public DateTime? dataWaznosci
            {
                get { return base.dataWaznosci; }
                set { base.dataWaznosci = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string nrKolejny
            {
                get { return base.nrKolejny; }
                set { base.nrKolejny = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string nrKz
            {
                get { return base.nrKz; }
                set { base.nrKz = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string nrSerii
            {
                get { return base.nrSerii; }
                set { base.nrSerii = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public int? odmiana
            {
                get { return base.odmiana; }
                set { base.odmiana = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string symbolTowaru
            {
                get { return base.symbolTowaru; }
                set { base.symbolTowaru = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string jednostkaMiary
            {
                get { return base.jednostkaMiary; }
                set { base.jednostkaMiary = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public string lokalizacja
            {
                get { return base.lokalizacja; }
                set { base.lokalizacja = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public float? ilosc
            {
                get { return base.ilosc; }
                set { base.ilosc = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? nosnId
            {
                get { return base.nosnId; }
                set { base.nosnId = value; }
            }

        }

        #endregion

        #region Nested type: zakonczPozycjeUPWrap

        /// <remarks/>
        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class zakonczPozycjeUPWrap : zakonczPozycjeUP
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? idDokumentu
            {
                get { return base.idDokumentu; }
                set { base.idDokumentu = value; }
            }
        }

        #endregion

        #region Nested type: zatwierdzPobranieWrap

        /// <remarks/>
        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class zatwierdzPobranieWrap : zatwierdzPobranie
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? id
            {
                get { return base.id; }
                set { base.id = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public float? ilosc
            {
                get { return base.ilosc; }
                set { base.ilosc = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public float? waga
            {
                get { return base.waga; }
                set { base.waga = value; }
            }
        }

        #endregion

        #region Nested type: zmienIloscWageIZakonczPozycjeWrap

        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class zmienIloscWageIZakonczPozycjeWrap : zmienIloscWageIZakonczPozycje
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public long? id
            {
                get { return base.id; }
                set { base.id = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public float? ilosc
            {
                get { return base.ilosc; }
                set { base.ilosc = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public float? waga
            {
                get { return base.waga; }
                set { base.waga = value; }
            }
        }

        #endregion

        #region Nested type: zmienPozycjeWrap

        /// <remarks/>
        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class zmienPozycjeWrap : zmienPozycje
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public Pozycja pozycja
            {
                get { return base.pozycja; }
                set { base.pozycja = value; }
            }
        }

        #endregion
    }
}