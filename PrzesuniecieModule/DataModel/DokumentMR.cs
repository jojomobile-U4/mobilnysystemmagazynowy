using System.Collections.Generic;

namespace PrzesuniecieModule.DataModel
{
    /// <summary>
    /// Summary description for PrzesuniecieModule.DataModel.DokumentMR
    /// </summary>
    public class DokumentMR
    {
        private string statusDokumentu;
        private string dokument;
        private string symbolTowaru;
        private string pobranie;
        private float ilosc;
        private float? iloscWLokalizacji;
        private float? stanGoracy;
        private float wagaJM;
        private string abc;
        private string jm;
        private long idPartii;
        private long idPobrania;
        private string pozycjaPPZakonczona;
        private long id;
        private float? iloscPozostawionaWLokalizacji;

        public long NosnId
        {
            get;
            set;
        }

        public long IdPartii
        {
            get { return idPartii; }
            set { idPartii = value; }
        }

        public long IdPobrania
        {
            get { return idPobrania; }
            set { idPobrania = value; }
        }

        public long ID
        {
            get { return id; }
            set { id = value; }
        }

        public SortedList<long, Umieszczenie> Umieszczenie;

        public string StatusDokumentu
        {
            get { return statusDokumentu; }
            set { statusDokumentu = value; }
        }

        public string Dokument
        {
            get { return dokument; }
            set { dokument = value; }
        }

        public string PozycjaPPZakonczona
        {
            get { return pozycjaPPZakonczona; }
            set { pozycjaPPZakonczona = value; }
        }

        public string SymbolTowaru
        {
            get { return symbolTowaru; }
            set { symbolTowaru = value; }
        }

        public string Pobranie
        {
            get { return pobranie; }
            set { pobranie = value; }
        }

        public float Ilosc
        {
            get { return ilosc; }
            set { ilosc = value; }
        }

        public float? IloscWLokalizacji
        {
            get { return iloscWLokalizacji; }
            set { iloscWLokalizacji = value; }
        }

        public float? IloscPozostawionaWLokalizacji
        {
            get { return iloscPozostawionaWLokalizacji; }
            set { iloscPozostawionaWLokalizacji = value; }
        }

        public float? StanGoracy
        {
            get { return stanGoracy; }
            set { stanGoracy = value; }
        }

        public float WagaJM
        {
            get { return wagaJM; }
            set { wagaJM = value; }
        }

        public string Abc
        {
            get { return abc; }
            set { abc = value; }
        }

        public string Jm
        {
            get { return jm; }
            set { jm = value; }
        }
    }
}

