﻿
namespace PrzesuniecieModule.DataModel
{
    public class Nosnik
    {
        public Nosnik()
        {
        }

        public Nosnik(long id, string symbol)
        {
            Id = id;
            Symbol = symbol;
        }

        public long Id { get; set; }
        public string Symbol { get; set; }
    }
}
