using System;
using System.Collections.Generic;
using System.Text;

namespace PrzesuniecieModule.DataModel
{
    public class ListaTowarow
    {
        Towar[] lista;
        long? liczbaPozycji;

        public Towar[] Lista
        {
            get { return lista; }
            set { lista = value; }
        }

        public long? LiczbaPozycji
        {
            get { return liczbaPozycji; }
            set { liczbaPozycji = value; }
        }
    }
}
