using System;
using System.Collections.Generic;
using System.Text;

namespace PrzesuniecieModule.DataModel
{
    public class Towar
    {
        private string abc;
        private long id;
        private float ilosc;
        private string jm;
        private string lokalizacja;
        private string nazwaTowaru;
        private int odmiana;
        private float stanGoracy;
        private string symbol;
        private long lp;

        public long? PainId
        {
            get;
            set;
        }

        public string Abc
        {
            get { return abc; }
            set { abc = value; }
        }

        public long Id
        {
            get { return id; }
            set { id = value; }
        }

        public float Ilosc
        {
            get { return ilosc; }
            set { ilosc = value; }
        }

        public string Jm
        {
            get { return jm; }
            set { jm = value; }
        }

        public string Lokalizacja
        {
            get { return lokalizacja; }
            set { lokalizacja = value; }
        }

        public string NazwaTowaru
        {
            get { return nazwaTowaru; }
            set { nazwaTowaru = value; }
        }

        public int Odmiana
        {
            get { return odmiana; }
            set { odmiana = value; }
        }

        public float StanGoracy
        {
            get { return stanGoracy; }
            set { stanGoracy = value; }
        }

        public string Symbol
        {
            get { return symbol; }
            set { symbol = value; }
        }

        public long Lp
        {
            get { return lp; }
            set { lp = value; }
        }

        public long? NosnId
        {
            get;
            set;
        }

    }
}
