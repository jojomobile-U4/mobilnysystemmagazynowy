namespace PrzesuniecieModule.DataModel
{
    /// <summary>
    /// Summary description for PrzesuniecieModule.DataModel.Umieszczenie
    /// </summary>
    public class Umieszczenie
    {
        private string lokalizacja;
        private float ilosc;
        private float? iloscWLokalizacji;
        private float max;
        private long idPartii;
        private long id;
        private string symbolTowaru;
        private long idDokumentu;
        private string jednostkaMiary;
        private float? iloscPoUmieszczeniu;

        public long NosnId
        {
            get;
            set;
        }

        public long Id
        {
            get { return id; }
            set { id = value; }
        }

        public long IdDokumentu
        {
            get { return idDokumentu; }
            set { idDokumentu = value; }
        }

        public long IdPartii
        {
            get { return idPartii; }
            set { idPartii = value; }
        }

        public string Lokalizacja
        {
            get { return lokalizacja; }
            set { lokalizacja = value; }
        }

        public float Ilosc
        {
            get { return ilosc; }
            set { ilosc = value; }
        }

        public float? IloscWLokalizacji
        {
            get { return iloscWLokalizacji; }
            set { iloscWLokalizacji = value; }
        }

        public float? IloscPoUmieszczeniu
        {
            get { return iloscPoUmieszczeniu; }
            set { iloscPoUmieszczeniu = value; }
        }

        public float Max
        {
            get { return max; }
            set { max = value; }
        }

        public string SymbolTowaru
        {
            get { return symbolTowaru; }
            set { symbolTowaru = value; }
        }

        public string JednostkaMiary
        {
            get { return jednostkaMiary; }
            set { jednostkaMiary = value; }
        }
    }
}
