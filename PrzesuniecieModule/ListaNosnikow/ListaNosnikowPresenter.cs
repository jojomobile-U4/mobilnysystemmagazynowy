﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Common;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using PrzesuniecieModule.DataModel;
using PrzesuniecieModule.ListaTowarow;
using PrzesuniecieModule.RealizacjaMR.Umieszczenie;

namespace PrzesuniecieModule.ListaNosnikow
{
    class ListaNosnikowPresenter : PresenterBase
    {
        [EventPublication(EventBrokerConstants.WYBRANO_NOSNIK, PublicationScope.WorkItem)]
        public event EventHandler WybranoNosnik;

        List<Nosnik> ListaNosnikow;
        private string Lokalizacja;

        public ListaNosnikowPresenter(IViewBase view)
            : base(view)
        {
        }

        protected UmieszczenieWorkItem MyWorkItemUmieszczenie
        {
            get
            {
                if (WorkItem.GetType().Name.Equals("UmieszczenieWorkItem"))
                {
                    return WorkItem as UmieszczenieWorkItem;
                }
                return null;
            }
        }


        protected ListaTowarowWorkItem MyWorkItemListaTowarow
        {
            get
            {
                if (WorkItem.GetType().Name.Equals("ListaTowarowWorkItem"))
                {
                    return WorkItem as ListaTowarowWorkItem;
                }
                return null;
            }
        }

        public IListaNosnikowView View
        {
            get { return m_view as IListaNosnikowView; }
        }

        public void WyswietlListe(long? painId, string kodLokalizacji, float? ilosc)
        {
            if (MyWorkItemUmieszczenie != null)
            {
                ListaNosnikow = MyWorkItemUmieszczenie.PobierzListeDostepnychNosnikow(painId, kodLokalizacji, ilosc);
            }
            else if (MyWorkItemListaTowarow != null)
            {
                ListaNosnikow = MyWorkItemListaTowarow.PobierzListeDostepnychNosnikow(painId, kodLokalizacji, ilosc);
            }

            Lokalizacja = kodLokalizacji;
            AktualizujWidok();
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Return:
                    e.Handled = true;
                    Wybierz(this, EventArgs.Empty);
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    Anuluj(this, EventArgs.Empty);
                    break;
            }
        }

        protected override void AttachView()
        {
            View.Wybierz += Wybierz;
            View.Anuluj += Anuluj;

        }

        private void Wybierz(object sender, EventArgs e)
        {
            if (View.ZaznaczonyNosnik == null)
                return;

            WorkItem.State[StateConstants.IDENTYFIKATOR_NOSNIKA] = View.ZaznaczonyNosnik.Id;
            WorkItem.State[StateConstants.LOCALIZATION] = Lokalizacja;
            WorkItemBase.MainWorkspace.Close(View);
            if (WybranoNosnik != null)
                WybranoNosnik(this, EventArgs.Empty);
        }

        private void Anuluj(object sender, EventArgs e)
        {
            WorkItemBase.MainWorkspace.Close(View);
        }

        private void AktualizujWidok()
        {
            View.Lokalizacja = Lokalizacja;
            View.ListaNosnikowDataSource.Clear();
            if (ListaNosnikow != null)
            {
                var i = 0;
                foreach (var nosnik in ListaNosnikow)
                {
                    i++;
                    var item = new ListViewItem(new[] { i.ToString(), nosnik.Symbol }) { Tag = nosnik };

                    View.ListaNosnikowDataSource.Add(item);
                }
            }
        }
    }
}