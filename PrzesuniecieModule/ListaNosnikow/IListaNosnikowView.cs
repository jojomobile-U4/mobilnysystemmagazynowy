﻿using System;
using System.Windows.Forms;
using Common.Base;
using PrzesuniecieModule.DataModel;

namespace PrzesuniecieModule.ListaNosnikow
{
    interface IListaNosnikowView : IViewBase
    {
        event EventHandler Wybierz;
        event EventHandler Anuluj;
        ListView.ListViewItemCollection ListaNosnikowDataSource { get; }
        Nosnik ZaznaczonyNosnik { get; }
        string Lokalizacja { set; }
    }
}