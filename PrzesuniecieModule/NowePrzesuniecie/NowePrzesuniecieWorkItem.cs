#region

using System;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using PrzesuniecieModule.Services;

#endregion

namespace PrzesuniecieModule.NowePrzesuniecie
{
    public class NowePrzesuniecieWorkItem : WorkItemBase
    {
        public void Show(IWorkspace parentWorkspace)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                var nowePrzesuniecieViewPresenter = Items.Get<NowePrzesuniecieViewPresenter>(ItemsConstants.NOWE_PRZESUNIECIE_PRESENTER);
                Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                MainWorkspace.Show(nowePrzesuniecieViewPresenter.View);
                Activate();
            }

            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }


        public DataModel.ListaTowarow PobierzListeTowarow(string lokalizacja)
        {
            DataModel.ListaTowarow listaTowarow;
            try
            {
                FunctionsHelper.SetCursorWait();

                listaTowarow = Services.Get<IPrzesuniecieService>(true).PobierzListeTowarowDlaLokalizacji(lokalizacja);

                FunctionsHelper.SetCursorDefault();
                if (!RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(StatusOperacji.SUCCESS))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), "Nowe przesuniÍcie",
                                   RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE].ToString());
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }


            return listaTowarow;
        }

        public long? UtowrzDokumentMR(string symbolTowaru, string lokalizacja, long? nosnId, string jm, int odmiana, float? ilosc)
        {
            long? idDokumentu;
            try
            {
                FunctionsHelper.SetCursorWait();

                idDokumentu = Services.Get<IPrzesuniecieService>(true).UtworzDokumentMR(symbolTowaru, lokalizacja, nosnId, jm, odmiana, ilosc);

                FunctionsHelper.SetCursorDefault();
                if (!RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(StatusOperacji.SUCCESS))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), "Nowe przesuniÍcie",
                                   RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE].ToString());
                    return null;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }

            return idDokumentu;
        }


        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ)]
        public void BarCodeRead(object sender, EventArgs e)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                if (MainWorkspace.ActiveSmartPart.ToString() == Items.Get<NowePrzesuniecieViewPresenter>(ItemsConstants.NOWE_PRZESUNIECIE_PRESENTER).View.ToString() &&
                    RootWorkItem.State[StateConstants.BAR_CODE] != null)
                {
                    var kodKreskowy = RootWorkItem.State[StateConstants.BAR_CODE] as string;
                    RootWorkItem.State[StateConstants.BAR_CODE] = null;
                    Items.Get<NowePrzesuniecieViewPresenter>(ItemsConstants.NOWE_PRZESUNIECIE_PRESENTER).ZaladujDaneDoWidoku(PobierzListeTowarow(Localization.RemovePrefix(kodKreskowy)));
                    Items.Get<NowePrzesuniecieViewPresenter>(ItemsConstants.NOWE_PRZESUNIECIE_PRESENTER).UstawLokalizacje(Localization.RemovePrefix(kodKreskowy));
                    Activate();
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }
    }
}