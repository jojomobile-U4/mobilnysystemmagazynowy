using System;
using System.Windows.Forms;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using PrzesuniecieModule.DataModel;

namespace PrzesuniecieModule.NowePrzesuniecie
{
    [SmartPart]
    public partial class NowePrzesuniecieView : ViewBase, INowePrzesuniecieView
    {
        #region Public Events

        public event EventHandler Anuluj;
        public event EventHandler WybierzLokalizacje;
        public event EventHandler Realizuj;
        public event EventHandler Szukaj;
        public event EventHandler PrzejdzDoNastepnejPozycji;

        #endregion
        #region Constructors

        public NowePrzesuniecieView()
        {
            InitializeComponent();
        }

        #endregion
        #region Overrided Methods

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);
            tbPobranie.ReadOnly = navigationState;
        }

        #endregion
        #region Properties

        public DataModel.ListaTowarow DataSource
        {
            set
            {
                lsTowary.Items.Clear();
                if (value != null && value.Lista != null && value.Lista.Length > 0)
                {
                    ListViewItem listViewItem;
                    foreach (Towar towar in value.Lista)
                    {
                        listViewItem = lsTowary.Items.Add(new ListViewItem(towar.Lp.ToString()));
                        listViewItem.SubItems.Add(towar.Symbol);
                        listViewItem.SubItems.Add(towar.NazwaTowaru);
                        listViewItem.SubItems.Add(towar.Ilosc.ToString("0.000"));
                        listViewItem.SubItems.Add(towar.Abc);
                        listViewItem.Tag = towar.Lp;
                    }
                }
                lsTowary.Focus();
            }
        }

        public long? LpAktualnegoTowaru
        {
            get
            {
                if ((lsTowary != null) && (lsTowary.SelectedIndices.Count > 0))
                {
                    return lsTowary.Items[lsTowary.SelectedIndices[0]].Tag as long?;
                }
                return null;
            }
        }

        public String Lokalizacja
        {
            get { return tbPobranie.Text; }
            set { tbPobranie.Text = value; }
        }

        public float? StanGoracy
        {
            set { tbStanGoracy.Text = value.ToString(); }
        }

        #endregion
        #region Private Methods

        private void btnAnuluj_Click(object sender, EventArgs e)
        {
            if (Anuluj != null)
            {
                Anuluj(this, EventArgs.Empty);
            }
        }

        private void btnWybierz_Click(object sender, EventArgs e)
        {
            if (WybierzLokalizacje != null)
            {
                WybierzLokalizacje(this, EventArgs.Empty);
            }
        }

        private void btnSzukaj_Click(object sender, EventArgs e)
        {
            if (Szukaj != null)
            {
                Szukaj(this, EventArgs.Empty);
            }
        }

        private void btnRealizuj_Click(object sender, EventArgs e)
        {
            if (Realizuj != null)
            {
                Realizuj(this, EventArgs.Empty);
            }
        }


        private void lsTowary_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PrzejdzDoNastepnejPozycji != null)
            {
                PrzejdzDoNastepnejPozycji(this, EventArgs.Empty);
            }
        }

        #endregion
    }
}
