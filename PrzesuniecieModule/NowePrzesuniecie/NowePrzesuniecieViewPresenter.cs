using System;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using PrzesuniecieModule.DataModel;

namespace PrzesuniecieModule.NowePrzesuniecie
{
    public class NowePrzesuniecieViewPresenter : PresenterBase
    {
        private bool WTrakcieRealizacji = false;

        [EventPublication(EventBrokerConstants.WYSZUKAJ_LISTE_DOKUMENTOW_MR)]
        public event EventHandler WyszukajListeDokumentowMR;

        private DataModel.ListaTowarow listaTowarow;

        public NowePrzesuniecieViewPresenter(INowePrzesuniecieView view)
            : base(view)
        {

        }
        public INowePrzesuniecieView View
        {
            get
            {
                return (m_view as INowePrzesuniecieView);
            }
        }

        public NowePrzesuniecieWorkItem MyWorkItem
        {
            get
            {
                return (WorkItemBase as NowePrzesuniecieWorkItem);
            }
        }

        #region Overrided Methods

        protected override void AttachView()
        {
            View.Anuluj += ViewAnuluj;
            View.WybierzLokalizacje += ViewWybierzLokalizacje;
            View.Realizuj += ViewRealizuj;
            View.Szukaj += ViewSzukaj;
            View.PrzejdzDoNastepnejPozycji += ViewPrzejdzDoNastepnejPozycji;
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    e.Handled = true;
                    ViewRealizuj(this, EventArgs.Empty);
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    ViewAnuluj(View, EventArgs.Empty);
                    break;
                case Keys.Left:
                    e.Handled = true;
                    View.FocusNextControl(false);
                    break;
                case Keys.Right:
                    e.Handled = true;
                    View.FocusNextControl(true);
                    break;
                case Keys.D1:
                    e.Handled = true;
                    View.FocusNextControl(true);
                    ViewWybierzLokalizacje(View, EventArgs.Empty);
                    break;
                case Keys.D2:
                    e.Handled = true;
                    View.FocusNextControl(true);
                    ViewSzukaj(View, EventArgs.Empty);
                    break;
            }
        }

        #endregion
        #region View events handlers

        void ViewAnuluj(object sender, EventArgs e)
        {
            MyWorkItem.MainWorkspace.Close(View);
        }

        void ViewWybierzLokalizacje(object sender, EventArgs e)
        {
            if (View.LpAktualnegoTowaru != null)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = SymbolTowaru(View.LpAktualnegoTowaru);
                MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = SymbolTowaru(View.LpAktualnegoTowaru);

            }
            else
            {
                MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = "";
                MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
            }

            MyWorkItem.RootWorkItem.State[StateConstants.LOKALIZACJA_Z_KARTOTEKI_INDEKSU] = "";
            MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueYes;
            MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueYes;


            MyWorkItem.Activate();
            MyWorkItem.RootWorkItem.Commands[CommandConstants.KARTOTEKA_INDEKSU].Execute();
        }


        void ViewRealizuj(object sender, EventArgs e)
        {
            if (WTrakcieRealizacji == false)
            {
                WTrakcieRealizacji = true;
                {
                    if (View.LpAktualnegoTowaru != null)
                    {
                        var idDokumentu = MyWorkItem.UtowrzDokumentMR(SymbolTowaru(View.LpAktualnegoTowaru),
                                                                      Lokalizacja(View.LpAktualnegoTowaru),
                                                                      NosnId(View.LpAktualnegoTowaru),
                                                                      Jm(View.LpAktualnegoTowaru),
                                                                      (int) Odmiana(View.LpAktualnegoTowaru),
                                                                      StanGoracy(View.LpAktualnegoTowaru));
                        if (idDokumentu != null)
                        {
                            var kryteria = new RealizacjaMR.Search.KryteriaZapytaniaRealizacjiMR {Id = idDokumentu};
                            var realizacjaMRWorkItem
                                =
                                MyWorkItem.Parent.WorkItems.Get<RealizacjaMR.RealizacjaMRWorkItem>(
                                    WorkItemsConstants.REALIZACJA_MR_WORKITEM);
                            realizacjaMRWorkItem.State[StateConstants.CRITERIAS] = kryteria;

                            if (WyszukajListeDokumentowMR != null)
                            {
                                WyszukajListeDokumentowMR(this, EventArgs.Empty);
                            }

                            MyWorkItem.MainWorkspace.Close(View);
                        }
                    }
                }
                WTrakcieRealizacji = false;
            }
        }

        void ViewSzukaj(object sender, EventArgs e)
        {
            ZaladujDaneDoWidoku(MyWorkItem.PobierzListeTowarow(Localization.AddPrefix(View.Lokalizacja)));
        }

        void ViewPrzejdzDoNastepnejPozycji(object sender, EventArgs e)
        {
            View.StanGoracy = StanGoracy(View.LpAktualnegoTowaru);
        }

        #endregion
        #region Public Methods

        public void ZaladujDaneDoWidoku(DataModel.ListaTowarow lista)
        {
            listaTowarow = lista ?? new DataModel.ListaTowarow();

            AktualizujWidok();
        }

        [EventSubscription(EventBrokerConstants.WYBRANO_LOKALIZACJE)]
        public void WybranoLokalizacje(object sender, EventArgs e)
        {
            if (View.Lokalizacja != MyWorkItem.RootWorkItem.State[StateConstants.LOKALIZACJA_Z_KARTOTEKI_INDEKSU].ToString())
            {
                View.Lokalizacja = MyWorkItem.RootWorkItem.State[StateConstants.LOKALIZACJA_Z_KARTOTEKI_INDEKSU].ToString();
                View.DataSource = null;
            }

            MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = "";
            MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
            MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
            MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;

        }

        #endregion
        #region Private Methods
        public void UstawLokalizacje(string lokalizacja)
        {
            View.Lokalizacja = lokalizacja;
        }

        private String SymbolTowaru(long? lp)
        {
            if (listaTowarow != null && listaTowarow.Lista != null)
            {
                foreach (Towar towar in listaTowarow.Lista)
                {
                    if ((towar != null) && (towar.Lp == lp))
                    {
                        return towar.Symbol;
                    }
                }
            }
            return null;
        }

        private String Jm(long? lp)
        {
            if (listaTowarow != null && listaTowarow.Lista != null)
            {
                foreach (Towar towar in listaTowarow.Lista)
                {
                    if ((towar != null) && (towar.Lp == lp))
                    {
                        return towar.Jm;
                    }
                }
            }
            return null;
        }

        private int? Odmiana(long? lp)
        {
            if (listaTowarow != null && listaTowarow.Lista != null)
            {
                foreach (Towar towar in listaTowarow.Lista)
                {
                    if ((towar != null) && (towar.Lp == lp))
                    {
                        return towar.Odmiana;
                    }
                }
            }
            return null;
        }

        private string Lokalizacja(long? lp)
        {
            if (listaTowarow != null && listaTowarow.Lista != null)
            {
                foreach (Towar towar in listaTowarow.Lista)
                {
                    if ((towar != null) && (towar.Lp == lp))
                    {
                        return towar.Lokalizacja;
                    }
                }
            }
            return null;
        }

        private float? StanGoracy(long? lp)
        {
            if (listaTowarow != null && listaTowarow.Lista != null)
            {
                foreach (Towar towar in listaTowarow.Lista)
                {
                    if ((towar != null) && (towar.Lp == lp))
                    {
                        return towar.StanGoracy;
                    }
                }
            }
            return null;
        }

        private long? NosnId(long? lp)
        {
            if (listaTowarow != null && listaTowarow.Lista != null)
            {
                foreach (Towar towar in listaTowarow.Lista)
                {
                    if ((towar != null) && (towar.Lp == lp))
                    {
                        return towar.NosnId;
                    }
                }
            }
            return null;
        }

        private void AktualizujWidok()
        {
            View.DataSource = listaTowarow;
            View.StanGoracy = StanGoracy(View.LpAktualnegoTowaru);
        }

        #endregion
    }
}
