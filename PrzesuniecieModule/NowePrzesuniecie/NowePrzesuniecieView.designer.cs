
using Common.Components;

namespace PrzesuniecieModule.NowePrzesuniecie
{
    partial class NowePrzesuniecieView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu viewMenu;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.viewMenu = new System.Windows.Forms.MainMenu();
            this.btnAnuluj = new System.Windows.Forms.Button();
            this.btnPrzesun = new System.Windows.Forms.Button();
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbPobranie = new System.Windows.Forms.TextBox();
            this.lbPobranie = new System.Windows.Forms.Label();
            this.lsTowary = new Common.Components.MSMListView();
            this.chLP = new System.Windows.Forms.ColumnHeader();
            this.chSymbol = new System.Windows.Forms.ColumnHeader();
            this.chNazwIndeksu = new System.Windows.Forms.ColumnHeader();
            this.chIlosc = new System.Windows.Forms.ColumnHeader();
            this.chABC = new System.Windows.Forms.ColumnHeader();
            this.btnSzukaj = new System.Windows.Forms.Button();
            this.btnWybierz = new System.Windows.Forms.Button();
            this.tbStanGoracy = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnPrzesun);
            this.pnlNavigation.Controls.Add(this.btnAnuluj);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 268);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 32);
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnAnuluj.Location = new System.Drawing.Point(161, 8);
            this.btnAnuluj.Name = "btnAnuluj";
            this.btnAnuluj.Size = new System.Drawing.Size(75, 20);
            this.btnAnuluj.TabIndex = 1;
            this.btnAnuluj.TabStop = false;
            this.btnAnuluj.Text = "&Esc Anuluj";
            this.btnAnuluj.Click += new System.EventHandler(this.btnAnuluj_Click);
            // 
            // btnPrzesun
            // 
            this.btnPrzesun.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPrzesun.Location = new System.Drawing.Point(81, 8);
            this.btnPrzesun.Name = "btnPrzesun";
            this.btnPrzesun.Size = new System.Drawing.Size(75, 20);
            this.btnPrzesun.TabIndex = 0;
            this.btnPrzesun.TabStop = false;
            this.btnPrzesun.Text = "&Ret Przesu�";
            this.btnPrzesun.Click += new System.EventHandler(this.btnRealizuj_Click);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 3;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "PRZESUNI�CIE - NOWE UMIESZCZENIE";
            // 
            // tbPobranie
            // 
            this.tbPobranie.Location = new System.Drawing.Point(58, 20);
            this.tbPobranie.Name = "tbPobranie";
            this.tbPobranie.Size = new System.Drawing.Size(98, 21);
            this.tbPobranie.TabIndex = 0;
            // 
            // lbPobranie
            // 
            this.lbPobranie.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbPobranie.Location = new System.Drawing.Point(3, 22);
            this.lbPobranie.Name = "lbPobranie";
            this.lbPobranie.Size = new System.Drawing.Size(76, 21);
            this.lbPobranie.Text = "Pobranie:";
            // 
            // lsTowary
            // 
            this.lsTowary.Columns.Add(this.chLP);
            this.lsTowary.Columns.Add(this.chSymbol);
            this.lsTowary.Columns.Add(this.chNazwIndeksu);
            this.lsTowary.Columns.Add(this.chIlosc);
            this.lsTowary.Columns.Add(this.chABC);
            this.lsTowary.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lsTowary.FullRowSelect = true;
            this.lsTowary.Location = new System.Drawing.Point(2, 72);
            this.lsTowary.Name = "lsTowary";
            this.lsTowary.Size = new System.Drawing.Size(237, 169);
            this.lsTowary.TabIndex = 2;
            this.lsTowary.View = System.Windows.Forms.View.Details;
            this.lsTowary.SelectedIndexChanged += new System.EventHandler(this.lsTowary_SelectedIndexChanged);
            // 
            // chLP
            // 
            this.chLP.Text = "LP";
            this.chLP.Width = 20;
            // 
            // chSymbol
            // 
            this.chSymbol.Text = "Indeks";
            this.chSymbol.Width = 70;
            // 
            // chNazwIndeksu
            // 
            this.chNazwIndeksu.Text = "Nazwa";
            this.chNazwIndeksu.Width = 65;
            // 
            // chIlosc
            // 
            this.chIlosc.Text = "Ilo��";
            this.chIlosc.Width = 40;
            // 
            // chABC
            // 
            this.chABC.Text = "ABC";
            this.chABC.Width = 25;
            // 
            // btnSzukaj
            // 
            this.btnSzukaj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnSzukaj.Location = new System.Drawing.Point(161, 46);
            this.btnSzukaj.Name = "btnSzukaj";
            this.btnSzukaj.Size = new System.Drawing.Size(75, 20);
            this.btnSzukaj.TabIndex = 0;
            this.btnSzukaj.TabStop = false;
            this.btnSzukaj.Text = "&2 Szukaj";
            this.btnSzukaj.Click += new System.EventHandler(this.btnSzukaj_Click);
            // 
            // btnWybierz
            // 
            this.btnWybierz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnWybierz.Location = new System.Drawing.Point(161, 20);
            this.btnWybierz.Name = "btnWybierz";
            this.btnWybierz.Size = new System.Drawing.Size(75, 20);
            this.btnWybierz.TabIndex = 0;
            this.btnWybierz.TabStop = false;
            this.btnWybierz.Text = "&1 Wybierz";
            this.btnWybierz.Click += new System.EventHandler(this.btnWybierz_Click);
            // 
            // tbStanGoracy
            // 
            this.tbStanGoracy.Location = new System.Drawing.Point(161, 247);
            this.tbStanGoracy.MaxLength = 65536;
            this.tbStanGoracy.Name = "tbStanGoracy";
            this.tbStanGoracy.ReadOnly = true;
            this.tbStanGoracy.Size = new System.Drawing.Size(76, 21);
            this.tbStanGoracy.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(93, 250);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 18);
            this.label1.Text = "Stan gor�cy:";
            // 
            // NowePrzesuniecieView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbStanGoracy);
            this.Controls.Add(this.btnWybierz);
            this.Controls.Add(this.btnSzukaj);
            this.Controls.Add(this.lsTowary);
            this.Controls.Add(this.tbPobranie);
            this.Controls.Add(this.lbPobranie);
            this.Controls.Add(this.lbTytul);
            this.Name = "NowePrzesuniecieView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lbPobranie, 0);
            this.Controls.SetChildIndex(this.tbPobranie, 0);
            this.Controls.SetChildIndex(this.lsTowary, 0);
            this.Controls.SetChildIndex(this.btnSzukaj, 0);
            this.Controls.SetChildIndex(this.btnWybierz, 0);
            this.Controls.SetChildIndex(this.tbStanGoracy, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAnuluj;
        private System.Windows.Forms.Button btnPrzesun;
        private MSMLabel lbTytul;
        private System.Windows.Forms.TextBox tbPobranie;
        private System.Windows.Forms.Label lbPobranie;
        private MSMListView lsTowary;
        private System.Windows.Forms.ColumnHeader chLP;
        private System.Windows.Forms.ColumnHeader chSymbol;
        private System.Windows.Forms.ColumnHeader chNazwIndeksu;
        private System.Windows.Forms.ColumnHeader chIlosc;
        private System.Windows.Forms.ColumnHeader chABC;
        private System.Windows.Forms.Button btnSzukaj;
        private System.Windows.Forms.Button btnWybierz;
        private System.Windows.Forms.TextBox tbStanGoracy;
        private System.Windows.Forms.Label label1;
    }
}
