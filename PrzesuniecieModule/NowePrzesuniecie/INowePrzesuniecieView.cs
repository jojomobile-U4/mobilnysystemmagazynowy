using System;
using Common.Base;

namespace PrzesuniecieModule.NowePrzesuniecie
{
    public interface INowePrzesuniecieView : IViewBase
    {
        event EventHandler Anuluj;
        event EventHandler Szukaj;
        event EventHandler WybierzLokalizacje;
        event EventHandler Realizuj;
        event EventHandler PrzejdzDoNastepnejPozycji;

        DataModel.ListaTowarow DataSource { set; }
        long? LpAktualnegoTowaru { get; }
        String Lokalizacja { get; set; }
        float? StanGoracy { set; }    
    }
}
