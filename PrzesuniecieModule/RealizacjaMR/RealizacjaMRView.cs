using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Common.Base;
using PrzesuniecieModule.DataModel;

namespace PrzesuniecieModule.RealizacjaMR
{
    public partial class RealizacjaMRView : ViewBase, IRealizacjaMRView
    {
        #region Public Events

        public event EventHandler Powrot;
        public event EventHandler Szukaj;
        public event EventHandler Poprzedni;
        public event EventHandler Nastepny;
        public event EventHandler Pobierz;
        public event EventHandler Zakoncz;
        public event EventHandler DodajUmieszczenie;
        public event EventHandler ZmienUmieszczenie;
        public event EventHandler UsunUmieszczenie;
        public event EventHandler ZwalidowanoIlosc;
        public event EventHandler ZmienionoIloscWLokalizacji;

        #endregion
        #region Private delegates

        private delegate void InvokeAsynchUstawWartosc(float? wartosc);
        private delegate void InvokeAsynchUstawWartoscWLok(string lokalizajca, float iloscWLokalizacji);

        #endregion
        #region Private constants

        private const string FORMAT_FLOAT = "0.000";
        private const int INDEX_ILOSC_W_LOK = 2;
        private const string UNKNOWN = "?";

        #endregion
        #region Constructors

        public RealizacjaMRView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #endregion
        #region Public properties

        public DokumentMR Naglowek 
        {
            set {
                if (value != null)
                {
                    tbDokument.Text = value.Dokument;
                    tbStatusDokumentu.Text = value.StatusDokumentu;
                    tbSymbolTowaru.Text = value.SymbolTowaru;
                    tbPobranie.Text = value.Pobranie;
                    tbABC.Text = value.Abc;
                    tbJM.Text = value.Jm;
                    tbIlosc.Text = value.Ilosc.ToString();
                    tbStanGoracy.Text = value.StanGoracy.HasValue ? value.StanGoracy.Value.ToString() : UNKNOWN;
                    tbIloscWLokalizacji.Text = value.IloscWLokalizacji.HasValue ? value.IloscWLokalizacji.Value.ToString() : UNKNOWN;
                    if (value.IloscWLokalizacji.HasValue)
                    {
                            tbPozostawIlosc.Text = ((value.IloscPozostawionaWLokalizacji ?? 0)).ToString();
                    }
                    else
                        tbPozostawIlosc.Text = UNKNOWN;
                }
                else
                {
                    tbDokument.Text = null;
                    tbStatusDokumentu.Text = null;
                    tbSymbolTowaru.Text = null;
                    tbPobranie.Text = null;
                    tbABC.Text = null;
                    tbJM.Text = null;
                    tbIlosc.Text = null;
                    tbStanGoracy.Text = null;
                    tbIloscWLokalizacji.Text = null;
                    tbPozostawIlosc.Text = null;
                }
            }
        }

        public SortedList<long, DataModel.Umieszczenie> Umieszczenia
        {
            set {
                lsUmieszczenia.Items.Clear();
                if (value != null && value.Count > 0)
                {
                    ListViewItem listViewItem;
                    foreach (DataModel.Umieszczenie umieszczenie in value.Values)
                    {
                        listViewItem = lsUmieszczenia.Items.Add(new ListViewItem(umieszczenie.Lokalizacja));
                        listViewItem.SubItems.Add(umieszczenie.Ilosc.ToString("0.000"));
                        listViewItem.SubItems.Add(umieszczenie.IloscPoUmieszczeniu.HasValue ? umieszczenie.IloscPoUmieszczeniu.Value.ToString(FORMAT_FLOAT) : UNKNOWN);
                        listViewItem.Tag = umieszczenie.Id;
                    }
                }
                lsUmieszczenia.Focus();
            }
        }

        public long? IdAktualnegoUmieszczenia
        {
            get
            {
                if ((lsUmieszczenia != null) && (lsUmieszczenia.SelectedIndices.Count > 0))
                {
                    return lsUmieszczenia.Items[lsUmieszczenia.SelectedIndices[0]].Tag as long?;
                }
                else
                {
                    return null;
                }
            }
        }

        public float? Ilosc
        {
            get
            {
                if ((tbIlosc.Text != null) && (tbIlosc.Text != "") && (tbIlosc.Text != UNKNOWN))
                {
                    try
                    {
                        return float.Parse(tbIlosc.Text);
                    }
                    catch (FormatException)
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            set { tbIlosc.Text = value.ToString(); }
        }

        public float? IloscPozostawionaWLok
        {
            set { tbPozostawIlosc.Text = value.HasValue ? value.ToString() : UNKNOWN; }
        }


        public bool PrzyciskDodajEnabled
        {
            get { return btnDodaj.Enabled; }
            set { btnDodaj.Enabled = value; }
        }

        public bool PrzyciskZmienEnabled
        {
            get { return btnZmien.Enabled; }
            set { btnZmien.Enabled = value; }
        }

        public bool PrzyciskUsunEnabled
        {
            get { return btnUsun.Enabled; }
            set { btnUsun.Enabled = value; }
        }

        public bool PrzyciskPobierzEnabled
        {
            get { return btnPobierz.Enabled; }
            set { btnPobierz.Enabled = value; }
        }

        public bool PrzyciskZakonczEnabled
        {
            get { return btnZakoncz.Enabled; }
            set { btnZakoncz.Enabled = value; }
        }

        public bool IloscReadOnly
        {
            get { return tbIlosc.ReadOnly; }
            set { tbIlosc.ReadOnly = value; }
        }

        #endregion
        #region Public methods

        public void AsynchUstawStanGoracy(float? stanGoracy)
        {
            if (InvokeRequired)
            {
                this.Invoke(new InvokeAsynchUstawWartosc(AsynchUstawStanGoracy), stanGoracy);
                return;
            }
            tbStanGoracy.Text = stanGoracy.ToString();
        }

        public void AsynchUstawIloscWLokalizacji(float? ilosc)
        {
            if (InvokeRequired)
            {
                this.Invoke(new InvokeAsynchUstawWartosc(AsynchUstawIloscWLokalizacji), ilosc);
                return;
            }
            tbIloscWLokalizacji.Text = ilosc.ToString();
        }

        public void AsynchUstawIloscWLokUmieszczenia(string lokalizacja, float iloscWLokalizacji)
        {
            if (InvokeRequired)
            {
                this.Invoke(new InvokeAsynchUstawWartoscWLok(AsynchUstawIloscWLokUmieszczenia), lokalizacja, iloscWLokalizacji);
                return;
            }

            foreach (ListViewItem lvi in lsUmieszczenia.Items)
            {
                if (lvi.Text.Equals(lokalizacja))
                {
                    lvi.SubItems[INDEX_ILOSC_W_LOK].Text = iloscWLokalizacji.ToString(FORMAT_FLOAT);
                    break;
                }
            }
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);
        }

        #endregion
        #region Private methods

        private void btnPowrot_Click(object sender, EventArgs e)
        {
            if (Powrot != null)
            {
                Powrot(this, EventArgs.Empty);
            }
        }

        private void btnSzukaj_Click(object sender, EventArgs e)
        {
            if (Szukaj != null)
            {
                Szukaj(this, EventArgs.Empty);
            }
        }

        private void OnNastepny(object sender, EventArgs e)
        {
            if (Nastepny != null)
            {
                Nastepny(this, EventArgs.Empty);
            }
        }

        private void OnPoprzedni(object sender, EventArgs e)
        {
            if (Poprzedni != null)
            {
                Poprzedni(this, EventArgs.Empty);
            }
        }

        private void btnPobierz_Click(object sender, EventArgs e)
        {
            if (Pobierz != null)
            {
                Pobierz(this, EventArgs.Empty);
            }
        }

        private void btnZakoncz_Click(object sender, EventArgs e)
        {
            if (Zakoncz != null)
            {
                Zakoncz(this, EventArgs.Empty);
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (DodajUmieszczenie != null)
            {
                DodajUmieszczenie(this, EventArgs.Empty);
            }
        }

        private void btnZmien_Click(object sender, EventArgs e)
        {
            if (ZmienUmieszczenie != null)
            {
                ZmienUmieszczenie(this, EventArgs.Empty);
            }
        }

        private void btnUsun_Click(object sender, EventArgs e)
        {
            if (UsunUmieszczenie != null)
            {
                UsunUmieszczenie(this, EventArgs.Empty);
            }
        }

        private void tbIlosc_Validated(object sender, EventArgs e)
        {
            if (ZwalidowanoIlosc != null)
            {
                ZwalidowanoIlosc(this, EventArgs.Empty);
            }
        }

        private void tbIloscWLokalizacji_TextChanged(object sender, EventArgs e)
        {
            if (ZmienionoIloscWLokalizacji != null)
            {
                ZmienionoIloscWLokalizacji(this, EventArgs.Empty);
            }
        }

        #endregion
    }
}
