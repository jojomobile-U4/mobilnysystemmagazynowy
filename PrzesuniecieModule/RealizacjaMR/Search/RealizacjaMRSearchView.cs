#region

using Common.SearchForm;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace PrzesuniecieModule.RealizacjaMR.Search
{
    [SmartPart]
    public partial class RealizacjaMRSearchView : SearchForm, IRealizacjaMRSearchView
    {
        #region Constructors

        public RealizacjaMRSearchView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #endregion

        #region IRealizacjaMRSearchView Members

        public KryteriaZapytaniaRealizacjiMR Kryteria
        {
            get { return new KryteriaZapytaniaRealizacjiMR(tbSymbolDok.Text, tbTowar.Text, tbLokalizacja.Text, tbABC.Text); }
            set
            {
                if (value != null)
                {
                    tbSymbolDok.Text = value.SymbolDokumentu;
                    tbTowar.Text = value.Towar;
                    tbLokalizacja.Text = value.Lokalizacja;
                    tbABC.Text = value.Abc;
                }
                else
                {
                    tbSymbolDok.Text = null;
                    tbTowar.Text = null;
                    tbLokalizacja.Text = null;
                    tbABC.Text = null;
                }
            }
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);

            tbSymbolDok.ReadOnly = navigationState;
            tbLokalizacja.ReadOnly = navigationState;
            tbTowar.ReadOnly = navigationState;
            tbABC.ReadOnly = navigationState;
        }

        #endregion
    }
}