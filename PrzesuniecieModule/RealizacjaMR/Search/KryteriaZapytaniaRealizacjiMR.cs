using System;
using System.Collections.Generic;
using System.Text;

namespace PrzesuniecieModule.RealizacjaMR.Search
{
    public class KryteriaZapytaniaRealizacjiMR
    {
        #region Private Fields

        private string symbolDokumentu;
        private string towar;
        private string lokalizacja;
        private string abc;
        private long? id;
        private long? idPozycji;

        #endregion
        #region Constructors

        public KryteriaZapytaniaRealizacjiMR()
        {
        }

        public KryteriaZapytaniaRealizacjiMR(long? idPozycji)
        {
            this.IdPozycji = idPozycji;
        }

        public KryteriaZapytaniaRealizacjiMR(string symbolDokumentu, string towar, string lokalizacja, string abc)
        {
            this.SymbolDokumentu = symbolDokumentu;
            this.Towar = towar;
            this.Lokalizacja = lokalizacja;
            this.Abc = abc;
        }

        #endregion
        #region Properties

        public long? Id
        {
            get { return id; }
            set { id = value; }
        }

        public long? IdPozycji
        {
            get { return idPozycji; }
            set { idPozycji = value; }
        }

        public string SymbolDokumentu
        {
            get { return symbolDokumentu; }
            set { symbolDokumentu = value; }
        }

        public string Towar
        {
            get { return towar; }
            set { towar = value; }
        }

        public string Lokalizacja
        {
            get { return lokalizacja; }
            set { lokalizacja = value; }
        }

        public string Abc
        {
            get { return abc; }
            set { abc = value; }
        }

        #endregion

    }
}
