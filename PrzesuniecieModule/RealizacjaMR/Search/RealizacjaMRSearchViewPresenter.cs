using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using Common.SearchForm;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Common;

namespace PrzesuniecieModule.RealizacjaMR.Search
{
	public class RealizacjaMRSearchViewPresenter: SearchFormPresenter
	{
		#region Events

		[EventPublication(EventBrokerConstants.WYSZUKAJ_LISTE_DOKUMENTOW_MR)]
		public event EventHandler WyszukajListeDokumentowMR;

		#endregion
		#region Properties

		public IRealizacjaMRSearchView View
		{
			get { return m_view as IRealizacjaMRSearchView; }
		}

		#endregion
		#region Constructors

        public RealizacjaMRSearchViewPresenter(IRealizacjaMRSearchView view)
            : base(view)
		{

		}

		#endregion
		#region Overrided Memebers

		protected override void OnViewSzukaj(object sender, EventArgs e)
		{
			base.OnViewSzukaj(sender, e);
			OnWyszukajListeDokumentowMR();
		}

		protected override void OnViewUstawOstatnieZapytanie(object sender, EventArgs e)
		{
			base.OnViewUstawOstatnieZapytanie(sender, e);
			View.Kryteria = State[StateConstants.CRITERIAS] as KryteriaZapytaniaRealizacjiMR ?? new KryteriaZapytaniaRealizacjiMR();
		}

		#endregion
		#region Raising Events

		protected void OnWyszukajListeDokumentowMR()
		{
			State[StateConstants.CRITERIAS] = View.Kryteria;
			if (WyszukajListeDokumentowMR != null)
			{
				WyszukajListeDokumentowMR(this, EventArgs.Empty);
			}
		}

		#endregion
	}
}
