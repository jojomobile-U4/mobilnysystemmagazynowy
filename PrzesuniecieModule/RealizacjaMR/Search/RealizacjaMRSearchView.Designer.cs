using Common.Components;

namespace PrzesuniecieModule.RealizacjaMR.Search
{
	partial class RealizacjaMRSearchView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbSymbolDok = new System.Windows.Forms.TextBox();
            this.lbSymbolDok = new System.Windows.Forms.Label();
            this.tbTowar = new System.Windows.Forms.TextBox();
            this.tbLokalizacja = new System.Windows.Forms.TextBox();
            this.lbTowar = new System.Windows.Forms.Label();
            this.lbLokalizacja = new System.Windows.Forms.Label();
            this.lbAbc = new System.Windows.Forms.Label();
            this.tbABC = new System.Windows.Forms.TextBox();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // btnOstatnieZapytanie
            // 
            this.btnOstatnieZapytanie.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Location = new System.Drawing.Point(0, 272);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 1;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "WYSZUKIWANIE DOKUMENTÓW MR";
            // 
            // tbSymbolDok
            // 
            this.tbSymbolDok.Location = new System.Drawing.Point(73, 21);
            this.tbSymbolDok.Name = "tbSymbolDok";
            this.tbSymbolDok.Size = new System.Drawing.Size(164, 21);
            this.tbSymbolDok.TabIndex = 35;
            // 
            // lbSymbolDok
            // 
            this.lbSymbolDok.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbolDok.Location = new System.Drawing.Point(3, 21);
            this.lbSymbolDok.Name = "lbSymbolDok";
            this.lbSymbolDok.Size = new System.Drawing.Size(70, 21);
            this.lbSymbolDok.Text = "Symbol dok.:";
            // 
            // tbTowar
            // 
            this.tbTowar.Location = new System.Drawing.Point(73, 48);
            this.tbTowar.Name = "tbTowar";
            this.tbTowar.Size = new System.Drawing.Size(164, 21);
            this.tbTowar.TabIndex = 37;
            // 
            // tbLokalizacja
            // 
            this.tbLokalizacja.Location = new System.Drawing.Point(73, 75);
            this.tbLokalizacja.Name = "tbLokalizacja";
            this.tbLokalizacja.Size = new System.Drawing.Size(164, 21);
            this.tbLokalizacja.TabIndex = 38;
            // 
            // lbTowar
            // 
            this.lbTowar.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbTowar.Location = new System.Drawing.Point(3, 48);
            this.lbTowar.Name = "lbTowar";
            this.lbTowar.Size = new System.Drawing.Size(64, 21);
            this.lbTowar.Text = "Towar:";
            // 
            // lbLokalizacja
            // 
            this.lbLokalizacja.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbLokalizacja.Location = new System.Drawing.Point(3, 75);
            this.lbLokalizacja.Name = "lbLokalizacja";
            this.lbLokalizacja.Size = new System.Drawing.Size(64, 21);
            this.lbLokalizacja.Text = "Pobranie:";
            // 
            // lbAbc
            // 
            this.lbAbc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbAbc.Location = new System.Drawing.Point(3, 102);
            this.lbAbc.Name = "lbAbc";
            this.lbAbc.Size = new System.Drawing.Size(64, 21);
            this.lbAbc.Text = "ABC:";
            // 
            // tbABC
            // 
            this.tbABC.Location = new System.Drawing.Point(73, 102);
            this.tbABC.Name = "tbABC";
            this.tbABC.Size = new System.Drawing.Size(164, 21);
            this.tbABC.TabIndex = 41;
            // 
            // RealizacjaMRSearchView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lbAbc);
            this.Controls.Add(this.tbABC);
            this.Controls.Add(this.lbLokalizacja);
            this.Controls.Add(this.lbTowar);
            this.Controls.Add(this.tbLokalizacja);
            this.Controls.Add(this.tbTowar);
            this.Controls.Add(this.lbTytul);
            this.Controls.Add(this.lbSymbolDok);
            this.Controls.Add(this.tbSymbolDok);
            this.Name = "RealizacjaMRSearchView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.tbSymbolDok, 0);
            this.Controls.SetChildIndex(this.lbSymbolDok, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.tbTowar, 0);
            this.Controls.SetChildIndex(this.tbLokalizacja, 0);
            this.Controls.SetChildIndex(this.lbTowar, 0);
            this.Controls.SetChildIndex(this.lbLokalizacja, 0);
            this.Controls.SetChildIndex(this.tbABC, 0);
            this.Controls.SetChildIndex(this.lbAbc, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private MSMLabel lbTytul;
		private System.Windows.Forms.TextBox tbSymbolDok;
		private System.Windows.Forms.Label lbSymbolDok;
        private System.Windows.Forms.TextBox tbTowar;
        private System.Windows.Forms.TextBox tbLokalizacja;
        private System.Windows.Forms.Label lbTowar;
        private System.Windows.Forms.Label lbLokalizacja;
        private System.Windows.Forms.Label lbAbc;
        private System.Windows.Forms.TextBox tbABC;
	}
}
