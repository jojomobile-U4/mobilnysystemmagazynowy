#region

using Common.SearchForm;

#endregion

namespace PrzesuniecieModule.RealizacjaMR.Search
{
    public interface IRealizacjaMRSearchView : ISearchForm
    {
        KryteriaZapytaniaRealizacjiMR Kryteria { get; set; }
    }
}