using System;
using Common.Base;
using PrzesuniecieModule.DataModel;
using PrzesuniecieModule.PrzesuniecieProxy;
using System.Collections.Generic;

namespace PrzesuniecieModule.RealizacjaMR
{
    public interface IRealizacjaMRView : IViewBase
    {

        event EventHandler Powrot;
        event EventHandler Szukaj;
        event EventHandler Nastepny;
        event EventHandler Poprzedni;
        event EventHandler Pobierz;
        event EventHandler Zakoncz;
        event EventHandler DodajUmieszczenie;
        event EventHandler ZmienUmieszczenie;
        event EventHandler UsunUmieszczenie;
        event EventHandler ZwalidowanoIlosc;
        event EventHandler ZmienionoIloscWLokalizacji;

        DokumentMR Naglowek { set; }
        SortedList<long, DataModel.Umieszczenie> Umieszczenia { set; }
        long? IdAktualnegoUmieszczenia { get; }
        float? Ilosc { get; set; }
        float? IloscPozostawionaWLok { set; }
        bool PrzyciskDodajEnabled { get; set; }
        bool PrzyciskZmienEnabled { get; set; }
        bool PrzyciskUsunEnabled { get; set; }
        bool IloscReadOnly { get; set; }
        bool PrzyciskPobierzEnabled { get; set; }
        bool PrzyciskZakonczEnabled { get; set; }

        void AsynchUstawStanGoracy(float? stanGoracy);
        void AsynchUstawIloscWLokalizacji(float? ilosc);
        void AsynchUstawIloscWLokUmieszczenia(string lokalizacja, float iloscWLokalizacji);
    }
}
