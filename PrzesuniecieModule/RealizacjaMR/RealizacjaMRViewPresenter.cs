using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Common;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using PrzesuniecieModule.DataModel;
using Common.DataModel;

namespace PrzesuniecieModule.RealizacjaMR
{
    public class RealizacjaMRViewPresenter : PresenterBase
	{

        #region Constants

        public const string NAZWA_FORMULARZA = "PRZESUNI�CIE - REALIZACJA";
        private const string POMYSLNE_WYWOLANIE_RAPORTU = "Wydruk zosta� wys�any na drukark�.";
        private const int NASTEPNA = 1;
        private const int POPRZEDNIA = -1;
        private const int PIERWSZA = 0;

        #endregion
        #region Private Fields

        private DokumentMR[] listaDokumentowMR;
        private int numerPozycji = 0;
        private int numerStrony;
        private int zmianaStrony;

        #endregion
        #region Constructors

        public RealizacjaMRViewPresenter(IRealizacjaMRView view)
            :base(view)
        {

        }

        #endregion
        #region Public properties

        public IRealizacjaMRView View
        {
            get
            {
                return (m_view as IRealizacjaMRView);
            }
        }

        public RealizacjaMRWorkItem MyWorkItem
        {
            get
            {
                return (WorkItemBase as RealizacjaMRWorkItem);
            }
        }

        #endregion
        #region Protected methods

        protected override void AttachView()
        {
            View.Powrot += ViewPowrot;
            View.Szukaj += ViewSzukaj;
            View.Nastepny += ViewNastepny;
            View.Poprzedni += ViewPoprzedni;
            View.Pobierz += ViewPobierz;
            View.Zakoncz += ViewZakoncz;
            View.DodajUmieszczenie += ViewDodajUmieszczenie;
            View.ZmienUmieszczenie += ViewZmienUmieszczenie;
            View.UsunUmieszczenie += ViewUsunUmieszczenie;
            View.ZwalidowanoIlosc += ViewZwalidowanoIlosc;
            View.ZmienionoIloscWLokalizacji += ViewZmienionoIloscWLokalizacji;

        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    e.Handled = true;
                    ViewPoprzedni(this, EventArgs.Empty);
                    break;
                case Keys.Right:
                    e.Handled = true;
                    ViewNastepny(this, EventArgs.Empty);
                    break;
                case Keys.D1:
                    e.Handled = true;
                    ViewSzukaj(this, EventArgs.Empty);
                    break;
                case Keys.D2:
                    e.Handled = true;
                    ViewPobierz(this, EventArgs.Empty);
                    break;
                case Keys.D3:
                    e.Handled = true;
                    ViewZakoncz(this, EventArgs.Empty);
                    break;
                case Keys.D4:
                    e.Handled = true;
                    ViewDodajUmieszczenie(this, EventArgs.Empty);
                    break;
                case Keys.D5:
                    e.Handled = true;
                    ViewZmienUmieszczenie(this, EventArgs.Empty);
                    break;
                case Keys.D6:
                    e.Handled = true;
                    ViewUsunUmieszczenie(this, EventArgs.Empty);
                    break;
                case Keys.D:
                    e.Handled = true;
                    UruchomRaport();
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    MyWorkItem.AnulujOperacjeAsynchroniczne();
                    ViewPowrot(this, EventArgs.Empty);
                    break;
                case Keys.Up:
                    e.Handled = true;
                    View.FocusNextControl(false);
                    break;
                case Keys.Down:
                    e.Handled = true;
                    View.FocusNextControl(true);
                    break;
            }
        }

        [EventSubscription(EventBrokerConstants.NAVIGATION_STATE_CHANGED)]
        public override void NavigationStateChanged(object sender, EventArgs e)
        {
            
            base.NavigationStateChanged(sender, e);
            AktualizujStanKontrolek();
        }

        internal void ResetujStronicowanie()
        {
            numerStrony = PIERWSZA;
            zmianaStrony = NASTEPNA;
        }

        #endregion
        #region Public methods

        public void ZaladujDaneDoWidoku(DokumentMR[] lista)
        {
            if (lista == null)
            {
                listaDokumentowMR = new DokumentMR[0];
                numerPozycji = 0;
            }
            else
            {
                listaDokumentowMR = lista;
                if (listaDokumentowMR.Length > 0)
                {
                    numerPozycji = 0;
                }
                else
                {
                    listaDokumentowMR = new DokumentMR[0];
                    numerPozycji = 0;
                }
            }

            AktualizujWidok();
        }

        [EventSubscription(EventBrokerConstants.DODANO_LUB_ZMIENIONO_POZYCJE_UMIESZCZENIA)]
        public void OnDodanoPozycjeUmieszczenia(object sender, EventArgs e)
        {
            listaDokumentowMR[numerPozycji].Umieszczenie =
                MyWorkItem.PobierzListePozycjiUP(listaDokumentowMR[numerPozycji].ID, listaDokumentowMR[numerPozycji].IdPartii);
            var dokumenty = new DokumentMR[1];
            dokumenty[0] = listaDokumentowMR[numerPozycji];
            MyWorkItem.PobierzDaneAsynchronicznie(dokumenty);
            AktualizujUmieszczenia();
            MyWorkItem.Activate();
        }


        [EventSubscription(EventBrokerConstants.ODCZYTANO_STAN_GORACY, ThreadOption.Background)]
        public void OnOdczytanoStanGoracy(object sender, EventArgs e)
        {
            try
            {
                //TODO: prawdopodobnie trzeba bedzie blokowac aktualnyDokument, aby uniemozliwic w czasie przetwarzania
                //		zmiane dokumentu lub strony
                var listaOdczytanychStanowGoracych = MyWorkItem.RootWorkItem.State[StateConstants.ASYNC_OP_STANY_RESULTS] as SortedList<long, DaneOperacjiAsync>;
                var kluczeDoUsuniecia = new List<long>();
                if (listaOdczytanychStanowGoracych != null)
                {
                    lock (listaOdczytanychStanowGoracych)
                    {
                        foreach (long key in listaOdczytanychStanowGoracych.Keys)
                        {
                            if ((listaDokumentowMR != null) && (listaDokumentowMR.Length > 0) &&
                                (listaDokumentowMR[numerPozycji].IdPobrania == key))
                            {
                                kluczeDoUsuniecia.Add(key);
                                listaDokumentowMR[numerPozycji].StanGoracy = listaOdczytanychStanowGoracych[key].ZwroconaWartosc;
                                View.AsynchUstawStanGoracy(listaDokumentowMR[numerPozycji].StanGoracy.Value);
                            }
                            else if (listaDokumentowMR != null)
                            {
                                DokumentMR dokumentDoZmiany = ZnajdzDokument(key, true);

                                if (dokumentDoZmiany != null)
                                {
                                    dokumentDoZmiany.StanGoracy = listaOdczytanychStanowGoracych[key].ZwroconaWartosc;
                                    kluczeDoUsuniecia.Add(key);
                                }
                            }
                        }
                    }

                    UsunPozycjeZListy(listaOdczytanychStanowGoracych, kluczeDoUsuniecia);
                }
            }
            catch (Exception)
            {
                MyWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
            }
            MyWorkItem.OnAsyncOperationFinished();
        }

        [EventSubscription(EventBrokerConstants.ODCZYTANO_ILOSC_W_LOK, ThreadOption.Background)]
        public void OnOdczytanoIloscWLok(object sender, EventArgs e)
        {
            try
            {
                //TODO: prawdopodobnie trzeba bedzie blokowac aktualnyDokument, aby uniemozliwic w czasie przetwarzania
                //		zmiane dokumentu lub strony
                var listaOdczytanychIlosciWLok = MyWorkItem.RootWorkItem.State[StateConstants.ASYNC_OP_ILOSC_RESULTS] as SortedList<long, DaneOperacjiAsync>;
                var kluczeDoUsuniecia = new List<long>();
                if (listaOdczytanychIlosciWLok != null)
                {
                    lock (listaOdczytanychIlosciWLok)
                    {
                        foreach (long key in listaOdczytanychIlosciWLok.Keys)
                        {
                            if ((listaDokumentowMR != null) && (listaDokumentowMR.Length > 0) &&
                                (listaDokumentowMR[numerPozycji].IdPobrania == key))
                            {
                                kluczeDoUsuniecia.Add(key);
                                listaDokumentowMR[numerPozycji].IloscWLokalizacji = listaOdczytanychIlosciWLok[key].ZwroconaWartosc;
                                View.AsynchUstawIloscWLokalizacji(listaDokumentowMR[numerPozycji].IloscWLokalizacji.Value);
                            }
                            else if (listaDokumentowMR != null)
                            {
                                DokumentMR dokumentDoZmiany = ZnajdzDokument(key, false);

                                if (dokumentDoZmiany != null)
                                {
                                    ZmienIloscWLokNaPozycjiDokumentu(dokumentDoZmiany, listaOdczytanychIlosciWLok[key], key);
                                    kluczeDoUsuniecia.Add(key);
                                }
                            }
                        }
                    }

                    UsunPozycjeZListy(listaOdczytanychIlosciWLok, kluczeDoUsuniecia);
                }
            }
            catch (Exception)
            {
                MyWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
            }
            MyWorkItem.OnAsyncOperationFinished();
        }

        #endregion
        #region Private methods

        void ViewPowrot(object sender, EventArgs e)
        {
            MyWorkItem.MainWorkspace.Close(View); 
        }

        void ViewSzukaj(object sender, EventArgs e)
        {
            MyWorkItem.Szukaj();
        }

        void ViewNastepny(object sender, EventArgs e)
        {
            if ((listaDokumentowMR != null) && (listaDokumentowMR[numerPozycji] != null))
            {
                View.ForceDataBinding();

                if (numerPozycji < listaDokumentowMR.Length - 1)
                {
                    numerPozycji++;
                    AktualizujWidok();
                }
                else
                {
                    if (listaDokumentowMR.Length == MyWorkItem.Configuration.PrzesuniecieWS.WielkoscStrony)
                    {
                        ZmienStrone(NASTEPNA);
                        numerPozycji = 0;
                    }
                }
            }
        }

        void ViewPoprzedni(object sender, EventArgs e)
        {
            if ((listaDokumentowMR != null) && (listaDokumentowMR[numerPozycji] != null))
            {
                View.ForceDataBinding();

                if (numerPozycji > 0)
                {
                    numerPozycji--;
                    AktualizujWidok();
                }
                else
                {
                    if (numerStrony > 0)
                    {
                        ZmienStrone(POPRZEDNIA);
                        numerPozycji = MyWorkItem.Configuration.PrzesuniecieWS.WielkoscStrony - 1;
                    }
                }
            }
        }

        void ViewPobierz(object sender, EventArgs e)
        {
            var ilosc = (float)0;
            if (View.Ilosc != null)
                ilosc = (float)View.Ilosc;
            if ((numerPozycji >= 0) && (listaDokumentowMR != null) && (listaDokumentowMR.Length > 0))
            {
                if (MyWorkItem.ZmienIZakonczPozycjePP(listaDokumentowMR[numerPozycji].IdPobrania, ilosc, 0))
                {
                    listaDokumentowMR[numerPozycji].PozycjaPPZakonczona = "T";
                    listaDokumentowMR[numerPozycji].Ilosc = ilosc;
                    ZmienStatusDokumentu(listaDokumentowMR[numerPozycji].ID, "R");
                    listaDokumentowMR[numerPozycji].StanGoracy = null;
                    listaDokumentowMR[numerPozycji].IloscWLokalizacji = null;
                    AktualizujWidok();
                    var dokumenty = new DokumentMR[1];
                    dokumenty[0] = listaDokumentowMR[numerPozycji];
                    MyWorkItem.PobierzDaneAsynchronicznie(dokumenty);
                }
            }
        }

        void ViewZakoncz(object sender, EventArgs e)
        {
            if ((numerPozycji >= 0) && (listaDokumentowMR != null) && (listaDokumentowMR.Length > 0)
                && (listaDokumentowMR[numerPozycji].Umieszczenie != null) && (listaDokumentowMR[numerPozycji].Umieszczenie.Count > 0))
            {
                if (MyWorkItem.ZakonczPozycjeUP(listaDokumentowMR[numerPozycji].ID))
                {
                    ZmienStatusDokumentu(listaDokumentowMR[numerPozycji].ID, "Z");
                    ZerujIlosciUmieszczen(listaDokumentowMR[numerPozycji].ID);
                    var dokumenty = new DokumentMR[1];
                    dokumenty[0] = listaDokumentowMR[numerPozycji];
                    MyWorkItem.PobierzDaneAsynchronicznie(dokumenty);
                    AktualizujWidok();
                }
            }
        }

        void ViewDodajUmieszczenie(object sender, EventArgs e)
        {
            var noweUmieszczenie = new DataModel.Umieszczenie
                                       {
                                           IdPartii = listaDokumentowMR[numerPozycji].IdPartii,
                                           SymbolTowaru = listaDokumentowMR[numerPozycji].SymbolTowaru,
                                           JednostkaMiary = listaDokumentowMR[numerPozycji].Jm,
                                           IdDokumentu = listaDokumentowMR[numerPozycji].ID
                                       };
            MyWorkItem.ShowUmieszczenie(noweUmieszczenie, SumaIlosciUmieszczen(), listaDokumentowMR[numerPozycji].Ilosc);
        }

        void ViewZmienUmieszczenie(object sender, EventArgs e)
        {
            if (View.IdAktualnegoUmieszczenia != null)
            {
                MyWorkItem.ShowUmieszczenie(listaDokumentowMR[numerPozycji].Umieszczenie[(long)View.IdAktualnegoUmieszczenia], 
                    SumaIlosciUmieszczen() - listaDokumentowMR[numerPozycji].Umieszczenie[(long)View.IdAktualnegoUmieszczenia].Ilosc,
                    listaDokumentowMR[numerPozycji].Ilosc);
            }
        }

        void ViewUsunUmieszczenie(object sender, EventArgs e)
        {
            if (View.IdAktualnegoUmieszczenia != null)
            {
                MyWorkItem.UsunPozycjeUP((long)View.IdAktualnegoUmieszczenia);
                listaDokumentowMR[numerPozycji].Umieszczenie = 
                    MyWorkItem.PobierzListePozycjiUP(listaDokumentowMR[numerPozycji].ID, listaDokumentowMR[numerPozycji].IdPartii);
                var dokumenty = new DokumentMR[1];
                dokumenty[0] = listaDokumentowMR[numerPozycji];
                MyWorkItem.PobierzDaneAsynchronicznie(dokumenty);
                AktualizujUmieszczenia();
            }
        }

        void ViewZwalidowanoIlosc(object sender, EventArgs e)
        {
            if ((View.Ilosc != null) && (listaDokumentowMR != null) && (listaDokumentowMR.Length > 0) && 
                (listaDokumentowMR[numerPozycji].IloscWLokalizacji != null))
            {
                if (listaDokumentowMR[numerPozycji].PozycjaPPZakonczona.Equals("T"))
                {
                    View.IloscPozostawionaWLok = listaDokumentowMR[numerPozycji].IloscWLokalizacji;
                }
                else
                {
                    View.IloscPozostawionaWLok = listaDokumentowMR[numerPozycji].IloscWLokalizacji - View.Ilosc;
                }
            }            
        }
        void ViewZmienionoIloscWLokalizacji(object sender, EventArgs e)
        {
            ViewZwalidowanoIlosc(sender, e);
        }

        private void AktualizujWidok()
        {
            AktualizujNaglowek();
            AktualizujUmieszczenia();
            AktualizujStanKontrolek();
        }

        private void AktualizujNaglowek()
        {
            if ((listaDokumentowMR != null) && (listaDokumentowMR.Length > numerPozycji))
            {
                if (listaDokumentowMR[numerPozycji].PozycjaPPZakonczona.Equals("T"))
                {
                    listaDokumentowMR[numerPozycji].IloscPozostawionaWLokalizacji = listaDokumentowMR[numerPozycji].IloscWLokalizacji;
                }
                else
                {
                    listaDokumentowMR[numerPozycji].IloscPozostawionaWLokalizacji = listaDokumentowMR[numerPozycji].IloscWLokalizacji -
                        listaDokumentowMR[numerPozycji].Ilosc;
                }
                View.Naglowek = listaDokumentowMR[numerPozycji];
            }
            else
            {
                View.Naglowek = null;
            }
        }

        private void AktualizujUmieszczenia()
        {
            if ((listaDokumentowMR != null) && (listaDokumentowMR.Length > numerPozycji))
            {
                //Wyznaczenie ilo�ci w lokalizacji w umieszczeniach po zako�czeniu dokumentu
                foreach (DataModel.Umieszczenie umieszczenie in listaDokumentowMR[numerPozycji].Umieszczenie.Values)
                {
                    if (listaDokumentowMR[numerPozycji].StatusDokumentu.Equals("Z"))
                    {
                        listaDokumentowMR[numerPozycji].Umieszczenie.Values[listaDokumentowMR[numerPozycji].Umieszczenie.IndexOfValue(umieszczenie)].IloscPoUmieszczeniu
                            = umieszczenie.IloscWLokalizacji;
                    }
                    else
                    {
                        listaDokumentowMR[numerPozycji].Umieszczenie.Values[listaDokumentowMR[numerPozycji].Umieszczenie.IndexOfValue(umieszczenie)].IloscPoUmieszczeniu
                            = umieszczenie.IloscWLokalizacji + umieszczenie.Ilosc;
                    }
                }

                View.Umieszczenia = listaDokumentowMR[numerPozycji].Umieszczenie;
            }
            else
            {
                View.Umieszczenia = null;
            }
        }

        private void AktualizujStanKontrolek()
        {
            View.PrzyciskDodajEnabled = false;
            View.PrzyciskZmienEnabled = false;
            View.PrzyciskUsunEnabled = false;
            View.IloscReadOnly = true;
            View.PrzyciskPobierzEnabled = false;
            View.PrzyciskZakonczEnabled = false;
            if (listaDokumentowMR != null && listaDokumentowMR.Length > 0)
            {
                if ((!listaDokumentowMR[numerPozycji].StatusDokumentu.Equals("Z"))
                    && (listaDokumentowMR[numerPozycji].PozycjaPPZakonczona.Equals("T")))
                {
                    View.PrzyciskDodajEnabled = true;
                    View.PrzyciskZmienEnabled = true;
                    View.PrzyciskUsunEnabled = true;
                    View.PrzyciskZakonczEnabled = true;
                }

                if ((!listaDokumentowMR[numerPozycji].StatusDokumentu.Equals("Z"))
                        && (!listaDokumentowMR[numerPozycji].PozycjaPPZakonczona.Equals("T")))
                {
                    View.IloscReadOnly = false || NavigationState;
                    View.PrzyciskPobierzEnabled = true;
                }
            }
        }

        private void ZmienStatusDokumentu(long idDokumentu, string status)
        {
            if (listaDokumentowMR != null && listaDokumentowMR.Length > 0)
            {
                foreach (DokumentMR dokument in listaDokumentowMR)
                {
                    if ((dokument != null) && (dokument.ID == idDokumentu))
                    {
                        dokument.StatusDokumentu = status;
                    }
                }
            }
        }

        private void ZerujIlosciUmieszczen(long idDokumentu)
        {
            if (listaDokumentowMR != null && listaDokumentowMR.Length > 0)
            {
                foreach (DokumentMR dokument in listaDokumentowMR)
                {
                    if ((dokument != null) && (dokument.ID == idDokumentu))
                    {
                        foreach (DataModel.Umieszczenie umieszczenie in listaDokumentowMR[numerPozycji].Umieszczenie.Values)
                        {
                            umieszczenie.IloscPoUmieszczeniu = null;
                            umieszczenie.IloscWLokalizacji = null;
                        }
                    }
                }
            }
        }

        private void ZmienStrone(int kierunek)
        {
            numerStrony += kierunek;
            zmianaStrony = kierunek;
            ZaladujDaneDoWidoku(MyWorkItem.PobierzListeDokumentowMR(numerStrony));
        }

        private float SumaIlosciUmieszczen()
        {
            var ilosc = (float)0;
            foreach (DataModel.Umieszczenie umieszczenie in listaDokumentowMR[numerPozycji].Umieszczenie.Values)
            {
                ilosc += umieszczenie.Ilosc;
            }
            return ilosc;
        }

        private void UsunPozycjeZListy(SortedList<long, DaneOperacjiAsync> lista, List<long> listaKluczyDoUsuniecia)
        {
            if (listaKluczyDoUsuniecia.Count > 0)
            {
                lock (lista)
                {
                    foreach (long keyToDelete in listaKluczyDoUsuniecia)
                    {
                        if (lista.ContainsKey(keyToDelete))
                        {
                            lista.Remove(keyToDelete);
                        }
                    }
                }
            }
        }

        private DokumentMR ZnajdzDokument(long idPozycji, bool tylkoPozycjePobrania)
        {
            if (listaDokumentowMR == null ||
                listaDokumentowMR.Length == 0)
            {
                return null;
            }

            DokumentMR znalezionyDokument = null;
            if (tylkoPozycjePobrania)
            {
                foreach (DokumentMR dokument in listaDokumentowMR)
                {
                    if (dokument.IdPobrania == idPozycji)
                    {
                        znalezionyDokument = dokument;
                        break;
                    }
                }
            }
            else
            {
                foreach (DokumentMR dokument in listaDokumentowMR)
                {
                    if (dokument.IdPobrania == idPozycji ||
                        dokument.Umieszczenie.ContainsKey(idPozycji))
                    {
                        znalezionyDokument = dokument;
                        break;
                    }
                }
            }
            return znalezionyDokument;
        }


        private void ZmienIloscWLokNaPozycjiDokumentu(DokumentMR dokumentDoZmiany, DaneOperacjiAsync daneOperacjiAsync, long idPozycji)
        {
            if (dokumentDoZmiany.IdPobrania == idPozycji)
            {
                dokumentDoZmiany.IloscWLokalizacji = daneOperacjiAsync.ZwroconaWartosc;
            }
            else
            {
                DataModel.Umieszczenie umieszczenie = dokumentDoZmiany.Umieszczenie[idPozycji];
                umieszczenie.IloscWLokalizacji = daneOperacjiAsync.ZwroconaWartosc;
                if (listaDokumentowMR[numerPozycji] == dokumentDoZmiany)
                {
                    if (listaDokumentowMR[numerPozycji].StatusDokumentu.Equals("Z"))
                    {
                        View.AsynchUstawIloscWLokUmieszczenia(umieszczenie.Lokalizacja, umieszczenie.IloscWLokalizacji.Value);
                    }
                    else
                    {
                        View.AsynchUstawIloscWLokUmieszczenia(umieszczenie.Lokalizacja, umieszczenie.IloscWLokalizacji.Value + umieszczenie.Ilosc); 
                    }
                }
            }
        }

        private void UruchomRaport()
        {
            if ((listaDokumentowMR != null) && (listaDokumentowMR.Length > 0) && (listaDokumentowMR[numerPozycji] != null))
            {
                if (MyWorkItem.UruchomRaport(NAZWA_FORMULARZA,
                    listaDokumentowMR[numerPozycji].ID, listaDokumentowMR[numerPozycji].IdPobrania))
                {
                    MessageBox.Show(POMYSLNE_WYWOLANIE_RAPORTU);
                    MyWorkItem.Activate();
                }
            }
        }

        #endregion
    }
}
