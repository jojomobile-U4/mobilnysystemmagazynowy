#region

using System;
using System.Collections.Generic;
using System.Threading;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using PrzesuniecieModule.DataModel;
using PrzesuniecieModule.RealizacjaMR.Search;
using PrzesuniecieModule.RealizacjaMR.Umieszczenie;
using PrzesuniecieModule.Services;

#endregion

namespace PrzesuniecieModule.RealizacjaMR
{
    public class RealizacjaMRWorkItem : WorkItemBase
    {
        #region Constants

        private const string NOT_ALL_DATA_HAS_BEEN_READ = "Nie wszystkie dane zosta�y poprawnie odczytane.";
        private const string WARNING = "Ostrze�enie";

        #endregion

        #region Event Publications

        [EventPublication(EventBrokerConstants.ASYNC_OPERATIONS_STARTED)]
        public event EventHandler AsyncOperationsStarted;

        [EventPublication(EventBrokerConstants.ASYNC_OPERATOIN_FINISHED)]
        public event EventHandler AsyncOperationFinished;

        #endregion

        #region Zmienne prywatne

        private readonly List<DokumentMR> zaczytaneDokumenty = new List<DokumentMR>();
        private Thread watekZaczytywaniaAsynchronicznego;

        #endregion

        #region Metody publiczne

        public void Show(IWorkspace parentWorkspace)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                var realizacjaMRViewPresenter = Items.Get<RealizacjaMRViewPresenter>(ItemsConstants.REALIZACJA_MR_PRESENTER);
                Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                MainWorkspace.Show(realizacjaMRViewPresenter.View);
                this.Activate();
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void Szukaj()
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                var searchPresenter = Items.Get<RealizacjaMRSearchViewPresenter>(ItemsConstants.REALIZACJA_MR_SEARCH_PRESENTER);
                searchPresenter.View.Kryteria = null;
                Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                MainWorkspace.Show(searchPresenter.View);
                this.Activate();
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        [EventSubscription(EventBrokerConstants.WYSZUKAJ_LISTE_DOKUMENTOW_MR)]
        public void OnSzukajListeDokumentowMR(object sender, EventArgs e)
        {
            FunctionsHelper.SetCursorWait();
            var realizacjaMRViewPresenter = Items.Get<RealizacjaMRViewPresenter>(ItemsConstants.REALIZACJA_MR_PRESENTER);

            realizacjaMRViewPresenter.ResetujStronicowanie();
            realizacjaMRViewPresenter.ZaladujDaneDoWidoku(PobierzListeDokumentowMR(null));
            MainWorkspace.Show(realizacjaMRViewPresenter.View);
            this.Activate();
        }

        public DokumentMR[] PobierzListeDokumentowMR(int? numerStrony)
        {
            DokumentMR[] listaDokumentow = null;
            try
            {
                FunctionsHelper.SetCursorWait();
                var kryteria = State[StateConstants.CRITERIAS] as KryteriaZapytaniaRealizacjiMR;

                listaDokumentow = Services.Get<IPrzesuniecieService>(true).PobierzListeDokumentow(kryteria, numerStrony);

                FunctionsHelper.SetCursorDefault();
                if (RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), BLAD, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE].ToString());
                }
                else
                {
                    InicjalizujListy();
                    AnulujOperacjeAsynchroniczne();

                    lock (zaczytaneDokumenty)
                    {
                    }
                    zaczytaneDokumenty.Clear();
                    zaczytaneDokumenty.AddRange(listaDokumentow);
                    UruchomZaczytywanieAsynchroniczne();
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
            return listaDokumentow;
        }

        public void ShowUmieszczenie(DataModel.Umieszczenie umieszczenie, float sumaIlosciUmieszczen, float iloscPobrania)
        {
            var mainWorkspace = Parent.Workspaces[WorkspacesConstants.MAIN_WORKSPACE];
            var umieszczenieWorkItem = Parent.WorkItems.Get<UmieszczenieWorkItem>(WorkItemsConstants.UMIESZCZENIE_MR_WORKITEM);
            umieszczenieWorkItem.Show(mainWorkspace, umieszczenie, sumaIlosciUmieszczen, iloscPobrania);
        }

        public void UsunPozycjeUP(long id)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                Services.Get<IPrzesuniecieService>(true).UsunPozycjeUP(id);

                FunctionsHelper.SetCursorDefault();
                if (!RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(StatusOperacji.SUCCESS))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), "Umieszczenie",
                                   RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE].ToString());
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public SortedList<long, DataModel.Umieszczenie> PobierzListePozycjiUP(long idDokumentu, long idPartii)
        {
            var umieszczenia = new SortedList<long, DataModel.Umieszczenie>();
            try
            {
                FunctionsHelper.SetCursorWait();

                umieszczenia = Services.Get<IPrzesuniecieService>(true).PobierzListePozycjiUP(idDokumentu, idPartii);

                FunctionsHelper.SetCursorDefault();
                if (!RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(StatusOperacji.SUCCESS))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), "Umieszczenie",
                                   RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE].ToString());
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }

            return umieszczenia;
        }

        public bool ZmienIZakonczPozycjePP(long id, float ilosc, float waga)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                Services.Get<IPrzesuniecieService>(true).ZatwierdzPobranie(id, ilosc, waga);

                if (!RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(StatusOperacji.SUCCESS))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), "Zatwierdzenie pobrania",
                                   RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE].ToString());
                    Activate();
                    return false;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
            return true;
        }

        public bool ZakonczPozycjeUP(long idDokumentu)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                Services.Get<IPrzesuniecieService>(true).ZakonczPozycjeUP(idDokumentu);

                if (!RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(StatusOperacji.SUCCESS))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), "Zatwierdzenie dokumentu",
                                   RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE].ToString());
                    Activate();
                    return false;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
            return true;
        }

        public void PobierzDaneAsynchronicznie(DokumentMR[] dokumenty)
        {
            InicjalizujListy();
            AnulujOperacjeAsynchroniczne();

            lock (zaczytaneDokumenty)
            {
            }
            zaczytaneDokumenty.Clear();
            zaczytaneDokumenty.AddRange(dokumenty);
            UruchomZaczytywanieAsynchroniczne();
        }

        public bool UruchomRaport(string nazwaFormularza, long idDokumentu, long idPozycji)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                Services.Get<IPrzesuniecieService>(true).UruchomRaport(nazwaFormularza, idDokumentu, idPozycji);
                if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, BLAD, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                    return false;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }

            return true;
        }

        #endregion

        #region Metody Protected

        public void OnAsyncOperationFinished()
        {
            if (AsyncOperationFinished != null)
            {
                AsyncOperationFinished(this, EventArgs.Empty);
            }
            var listaIlosciDoPobrania = RootWorkItem.State[StateConstants.ASYNC_OP_ILOSC_REMAINING] as SortedList<long, DaneOperacjiAsync>;
            var listaStanowDoPobrania = RootWorkItem.State[StateConstants.ASYNC_OP_STANY_REMAINING] as SortedList<long, DaneOperacjiAsync>;
            if (listaStanowDoPobrania != null)
                if (listaIlosciDoPobrania != null &&
                    listaStanowDoPobrania.Count == 0 &&
                    listaIlosciDoPobrania.Count == 0)
                {
                    if (StatusOperacji.ERROR.Equals(State[StateConstants.OPERATION_RESULT_STATUS]))
                    {
                        ShowMessageBox(NOT_ALL_DATA_HAS_BEEN_READ, WARNING);
                    }
                }
                else
                {
                    //HACK: jezeli najpierw zostalo wywolane OnAsyncOperationsStarted, potem OnAsyncOperationFinished,
                    //		to na interfejsie nie ma informacji, ze tak naprawde dane sa odbierane asynchronicznie
                    OnAsyncOperationsStarted();
                }
        }

        protected void OnAsyncOperationsStarted()
        {
            if (AsyncOperationsStarted != null)
            {
                AsyncOperationsStarted(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Metody Prywatne

        /// <summary>
        /// Pobiera asynchronicznie dane, ktorych wyliczenie jest zbyt kosztowne czasowo.
        /// </summary>
        /// <param name="listaDokumentow">Lista dokumentow, dla ktorych nalezy pobrac brakujace dane</param>
        private void PobierzPozostaleDaneAsynchronicznie()
        {
            if (zaczytaneDokumenty.Count == 0)
            {
                return;
            }
            State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.SUCCESS;
            OnAsyncOperationsStarted();

            var listaIlosciDoPobrania = RootWorkItem.State[StateConstants.ASYNC_OP_ILOSC_REMAINING] as SortedList<long, DaneOperacjiAsync>;
            var listaStanowDoPobrania = RootWorkItem.State[StateConstants.ASYNC_OP_STANY_REMAINING] as SortedList<long, DaneOperacjiAsync>;
            try
            {
                lock (zaczytaneDokumenty)
                {
                    foreach (var dokument in zaczytaneDokumenty)
                    {
                        if (listaStanowDoPobrania != null)
                            listaStanowDoPobrania.Add(dokument.IdPobrania, new DaneOperacjiAsync(dokument.IdPartii, dokument.Pobranie, dokument.NosnId));

                        if (listaIlosciDoPobrania != null)
                            listaIlosciDoPobrania.Add(dokument.IdPobrania, new DaneOperacjiAsync(dokument.IdPartii, dokument.Pobranie, dokument.NosnId));

                        foreach (var idPozycjiUmieszczenia in dokument.Umieszczenie.Keys)
                        {
                            if (listaIlosciDoPobrania != null)
                                if (!listaIlosciDoPobrania.ContainsKey(idPozycjiUmieszczenia))
                                {
                                    listaIlosciDoPobrania.Add(idPozycjiUmieszczenia, new DaneOperacjiAsync(dokument.IdPartii, dokument.Umieszczenie[idPozycjiUmieszczenia].Lokalizacja, dokument.Umieszczenie[idPozycjiUmieszczenia].NosnId));
                                }
                        }
                    }
                    zaczytaneDokumenty.Clear();
                }

                Services.Get<IPrzesuniecieService>(true).PobierzAsynchronicznieStanyGorace(listaStanowDoPobrania);
                Services.Get<IPrzesuniecieService>(true).PobierzAsynchronicznieIlosciWLok(listaIlosciDoPobrania);
            }
            catch (ThreadAbortException)
            {
                if (listaIlosciDoPobrania != null)
                    lock (listaIlosciDoPobrania)
                    {
                        listaIlosciDoPobrania.Clear();
                    }
                if (listaStanowDoPobrania != null)
                    lock (listaStanowDoPobrania)
                    {
                        listaStanowDoPobrania.Clear();
                    }
                OnAsyncOperationFinished();
            }
            catch (Exception)
            {
                //TODO: log wyjatkow
                State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
            }
        }

        private void InicjalizujListy()
        {
            if (RootWorkItem.State[StateConstants.ASYNC_OP_ILOSC_REMAINING] == null)
            {
                RootWorkItem.State[StateConstants.ASYNC_OP_ILOSC_REMAINING] = new SortedList<long, DaneOperacjiAsync>();
            }
            if (RootWorkItem.State[StateConstants.ASYNC_OP_ILOSC_RESULTS] == null)
            {
                RootWorkItem.State[StateConstants.ASYNC_OP_ILOSC_RESULTS] = new SortedList<long, DaneOperacjiAsync>();
            }
            if (RootWorkItem.State[StateConstants.ASYNC_OP_STANY_REMAINING] == null)
            {
                RootWorkItem.State[StateConstants.ASYNC_OP_STANY_REMAINING] = new SortedList<long, DaneOperacjiAsync>();
            }
            if (RootWorkItem.State[StateConstants.ASYNC_OP_STANY_RESULTS] == null)
            {
                RootWorkItem.State[StateConstants.ASYNC_OP_STANY_RESULTS] = new SortedList<long, DaneOperacjiAsync>();
            }
        }

        public void AnulujOperacjeAsynchroniczne()
        {
            Services.Get<IPrzesuniecieService>(true).AnulujOczekujaceOperacje();
            if (watekZaczytywaniaAsynchronicznego != null)
            {
                watekZaczytywaniaAsynchronicznego.Abort();
                watekZaczytywaniaAsynchronicznego = null;
            }
            OnAsyncOperationFinished();
        }

        private void UruchomZaczytywanieAsynchroniczne()
        {
            watekZaczytywaniaAsynchronicznego = new Thread(PobierzPozostaleDaneAsynchronicznie);
            watekZaczytywaniaAsynchronicznego.Start();
        }

        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ)]
        public void BarCodeRead(object sender, EventArgs e)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                if (MainWorkspace.ActiveSmartPart.ToString() == Items.Get<RealizacjaMRViewPresenter>(ItemsConstants.REALIZACJA_MR_PRESENTER).View.ToString() &&
                    RootWorkItem.State[StateConstants.BAR_CODE] != null)
                {
                    var idPozycji = Konwertuj(Localization.RemovePrefix(RootWorkItem.State[StateConstants.BAR_CODE] as string));
                    if (idPozycji != null)
                    {
                        var kryteria = new KryteriaZapytaniaRealizacjiMR(idPozycji);
                        State[StateConstants.CRITERIAS] = kryteria;
                        OnSzukajListeDokumentowMR(this, EventArgs.Empty);
                    }
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        #endregion
    }
}