#region

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using PrzesuniecieModule.DataModel;
using PrzesuniecieModule.Services;

#endregion

namespace PrzesuniecieModule.RealizacjaMR.Umieszczenie
{
    public class UmieszczenieWorkItem : WorkItemBase
    {
        public void Show(IWorkspace parentWorkspace, DataModel.Umieszczenie umieszczenie, float sumaIlosciUmieszczen, float iloscPobrania)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                var umieszczenieViewPresenter = Items.Get<UmieszczenieViewPresenter>(ItemsConstants.UMIESZCZENIE_MR_PRESENTER);
                umieszczenieViewPresenter.Umieszczenie = umieszczenie;
                umieszczenieViewPresenter.SumaIlosciUmieszczen = sumaIlosciUmieszczen;
                umieszczenieViewPresenter.IloscPobrania = iloscPobrania;
                umieszczenieViewPresenter.UstawIlosc();
                Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                MainWorkspace.Show(umieszczenieViewPresenter.View);
                Activate();
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void DodajPozycjeUP(DataModel.Umieszczenie umieszczenie)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                Services.Get<IPrzesuniecieService>(true).DodajPozycjeUP(umieszczenie);

                FunctionsHelper.SetCursorDefault();
                if (!RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(StatusOperacji.SUCCESS))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), "Umieszczenie",
                                   RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE].ToString());
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void ZmienPozycje(DataModel.Umieszczenie umieszczenie)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                Services.Get<IPrzesuniecieService>(true).ZmienPozycje(umieszczenie);

                FunctionsHelper.SetCursorDefault();
                if (!RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(StatusOperacji.SUCCESS))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), "Umieszczenie",
                                   RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE].ToString());
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public bool MoznaJednoznacznieOkreslicNosnik(long? painId, string kodLokalizacji, float? ilosc)
        {
            bool moznaOkreslic = Services.Get<IPrzesuniecieService>(true).MoznaJednoznacznieOkreslicNosnik(painId,
                                                                                                    kodLokalizacji,
                                                                                                    ilosc);
            if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, BLAD, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
            }

            return moznaOkreslic;
        }

        public List<Nosnik> PobierzListeDostepnychNosnikow(long? painId, string kodLokalizacji, float? ilosc)
        {
            FunctionsHelper.SetCursorWait();
            var listaNosnikow = Services.Get<IPrzesuniecieService>(true).PobierzListeDostepnychNosnikow(painId, kodLokalizacji, ilosc);

            if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, BLAD, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
            }
            FunctionsHelper.SetCursorDefault();

            return listaNosnikow;
        }

        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ)]
        public void WczytajKodKreskowy(object sender, EventArgs e)
        {
            if (MainWorkspace.ActiveSmartPart == Items.Get<UmieszczenieViewPresenter>(ItemsConstants.UMIESZCZENIE_MR_PRESENTER).View &&
                RootWorkItem.State[StateConstants.BAR_CODE] != null)
            {
                var umieszczenieViewPresenter = Items.Get<UmieszczenieViewPresenter>(ItemsConstants.UMIESZCZENIE_MR_PRESENTER);
                umieszczenieViewPresenter.UstawLokalizacje(RootWorkItem.State[StateConstants.BAR_CODE] as string);

                Activate();
            }
        }
    }
}