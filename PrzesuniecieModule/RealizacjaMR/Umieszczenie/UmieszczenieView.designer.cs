
using Common.Components;

namespace PrzesuniecieModule.RealizacjaMR.Umieszczenie
{
    partial class UmieszczenieView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu viewMenu;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.viewMenu = new System.Windows.Forms.MainMenu();
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbUmieszczenie = new System.Windows.Forms.TextBox();
            this.lbUmieszczenie = new System.Windows.Forms.Label();
            this.tbIlosc = new System.Windows.Forms.TextBox();
            this.lbIlosc = new System.Windows.Forms.Label();
            this.tbIloscWLokalizacji = new System.Windows.Forms.TextBox();
            this.lbIloscWLokalizacji = new System.Windows.Forms.Label();
            this.btnAnuluj = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnWybierzLokalizacje = new System.Windows.Forms.Button();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnAnuluj);
            this.pnlNavigation.Controls.Add(this.btnOk);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 52);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 14;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "PRZESUNI�CIE - UMIESZCZENIE";
            // 
            // tbUmieszczenie
            // 
            this.tbUmieszczenie.Location = new System.Drawing.Point(80, 32);
            this.tbUmieszczenie.Name = "tbUmieszczenie";
            this.tbUmieszczenie.ReadOnly = true;
            this.tbUmieszczenie.Size = new System.Drawing.Size(80, 21);
            this.tbUmieszczenie.TabIndex = 4;
            // 
            // lbUmieszczenie
            // 
            this.lbUmieszczenie.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbUmieszczenie.Location = new System.Drawing.Point(7, 32);
            this.lbUmieszczenie.Name = "lbUmieszczenie";
            this.lbUmieszczenie.Size = new System.Drawing.Size(78, 21);
            this.lbUmieszczenie.Text = "Umieszczenie:";
            // 
            // tbIlosc
            // 
            this.tbIlosc.Location = new System.Drawing.Point(80, 59);
            this.tbIlosc.Name = "tbIlosc";
            this.tbIlosc.Size = new System.Drawing.Size(157, 21);
            this.tbIlosc.TabIndex = 7;
            // 
            // lbIlosc
            // 
            this.lbIlosc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbIlosc.Location = new System.Drawing.Point(7, 59);
            this.lbIlosc.Name = "lbIlosc";
            this.lbIlosc.Size = new System.Drawing.Size(62, 21);
            this.lbIlosc.Text = "Ilo��:";
            // 
            // tbIloscWLokalizacji
            // 
            this.tbIloscWLokalizacji.Location = new System.Drawing.Point(80, 84);
            this.tbIloscWLokalizacji.Name = "tbIloscWLokalizacji";
            this.tbIloscWLokalizacji.ReadOnly = true;
            this.tbIloscWLokalizacji.Size = new System.Drawing.Size(157, 21);
            this.tbIloscWLokalizacji.TabIndex = 10;
            // 
            // lbIloscWLokalizacji
            // 
            this.lbIloscWLokalizacji.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbIloscWLokalizacji.Location = new System.Drawing.Point(7, 80);
            this.lbIloscWLokalizacji.Name = "lbIloscWLokalizacji";
            this.lbIloscWLokalizacji.Size = new System.Drawing.Size(78, 30);
            this.lbIloscWLokalizacji.Text = "Ilo�� w \r\nlokalizacji:";
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnAnuluj.Location = new System.Drawing.Point(162, 27);
            this.btnAnuluj.Name = "btnAnuluj";
            this.btnAnuluj.Size = new System.Drawing.Size(75, 20);
            this.btnAnuluj.TabIndex = 13;
            this.btnAnuluj.Text = "&Esc Anuluj";
            this.btnAnuluj.Click += new System.EventHandler(this.btnAnuluj_Click);
            // 
            // btnOk
            // 
            this.btnOk.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnOk.Location = new System.Drawing.Point(98, 27);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(58, 20);
            this.btnOk.TabIndex = 12;
            this.btnOk.Text = "&Ret OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnWybierzLokalizacje
            // 
            this.btnWybierzLokalizacje.Location = new System.Drawing.Point(166, 33);
            this.btnWybierzLokalizacje.Name = "btnWybierzLokalizacje";
            this.btnWybierzLokalizacje.Size = new System.Drawing.Size(71, 20);
            this.btnWybierzLokalizacje.TabIndex = 15;
            this.btnWybierzLokalizacje.Text = "&1 Wybierz";
            this.btnWybierzLokalizacje.Click += new System.EventHandler(this.btnWybierzLokalizacje_Click);
            // 
            // UmieszczenieView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.btnWybierzLokalizacje);
            this.Controls.Add(this.tbIloscWLokalizacji);
            this.Controls.Add(this.lbIloscWLokalizacji);
            this.Controls.Add(this.tbIlosc);
            this.Controls.Add(this.lbIlosc);
            this.Controls.Add(this.tbUmieszczenie);
            this.Controls.Add(this.lbUmieszczenie);
            this.Controls.Add(this.lbTytul);
            this.Name = "UmieszczenieView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lbUmieszczenie, 0);
            this.Controls.SetChildIndex(this.tbUmieszczenie, 0);
            this.Controls.SetChildIndex(this.lbIlosc, 0);
            this.Controls.SetChildIndex(this.tbIlosc, 0);
            this.Controls.SetChildIndex(this.lbIloscWLokalizacji, 0);
            this.Controls.SetChildIndex(this.tbIloscWLokalizacji, 0);
            this.Controls.SetChildIndex(this.btnWybierzLokalizacje, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MSMLabel lbTytul;
        private System.Windows.Forms.TextBox tbUmieszczenie;
        private System.Windows.Forms.Label lbUmieszczenie;
        private System.Windows.Forms.TextBox tbIlosc;
        private System.Windows.Forms.Label lbIlosc;
        private System.Windows.Forms.TextBox tbIloscWLokalizacji;
        private System.Windows.Forms.Label lbIloscWLokalizacji;
        private System.Windows.Forms.Button btnAnuluj;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnWybierzLokalizacje;
    }
}
