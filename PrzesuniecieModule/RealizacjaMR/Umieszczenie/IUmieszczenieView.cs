using System;
using Common.Base;

namespace PrzesuniecieModule.RealizacjaMR.Umieszczenie
{
    public interface IUmieszczenieView : IViewBase
    {
        event EventHandler Anuluj;
        event EventHandler WybierzLokalizacje;
        event EventHandler DodajLubZmien;

        string Lokalizacja { get; set; }
        float? Ilosc { get; set; }
        float? IloscWLokalizacji { get; set; }
    }
}
