using System;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

namespace PrzesuniecieModule.RealizacjaMR.Umieszczenie
{
    [SmartPart]
    public partial class UmieszczenieView : ViewBase, IUmieszczenieView
    {
        public event EventHandler Anuluj;
        public event EventHandler WybierzLokalizacje;
        public event EventHandler DodajLubZmien;

        public UmieszczenieView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);
            tbIlosc.ReadOnly = navigationState;
            tbUmieszczenie.ReadOnly = navigationState;
        }

        public string Lokalizacja
        {
            get { return tbUmieszczenie.Text; }
            set { tbUmieszczenie.Text = value; }
        }

        public float? Ilosc
        {
            get
            {
                if (!string.IsNullOrEmpty(tbIlosc.Text))
                {
                    try
                    {
                        return float.Parse(tbIlosc.Text);
                    }
                    catch (FormatException)
                    {
                        return null;
                    }
                }
                return null;
            }
            set { tbIlosc.Text = value.ToString(); }
        }

        public float? IloscWLokalizacji
        {
            get
            {
                if (!string.IsNullOrEmpty(tbIloscWLokalizacji.Text))
                {
                    try
                    {
                        return float.Parse(tbIloscWLokalizacji.Text);
                    }
                    catch (FormatException)
                    {
                        return null;
                    }

                }
                return null;
            }
            set { tbIloscWLokalizacji.Text = value.ToString(); }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (DodajLubZmien != null)
            {
                DodajLubZmien(this, EventArgs.Empty);
            }
        }

        private void btnAnuluj_Click(object sender, EventArgs e)
        {
            if (Anuluj != null)
            {
                Anuluj(this, EventArgs.Empty);
            }
        }

        private void btnWybierzLokalizacje_Click(object sender, EventArgs e)
        {
            if (WybierzLokalizacje != null)
            {
                WybierzLokalizacje(this, EventArgs.Empty);
            }
        }
    }
}
