using System;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using PrzesuniecieModule.ListaNosnikow;

namespace PrzesuniecieModule.RealizacjaMR.Umieszczenie
{
    public class UmieszczenieViewPresenter : PresenterBase
    {

        [EventPublication(EventBrokerConstants.DODANO_LUB_ZMIENIONO_POZYCJE_UMIESZCZENIA)]
        public event EventHandler DodanoLubZmienionoPozycjeUmieszczenia;

        private DataModel.Umieszczenie umieszczenie;
        private float sumaIlosciUmieszczen;
        private float iloscPobrania;
        private bool NalezyDodacLubZmienicUmieszczenie;
        private bool TrwaDodawanieLubZmianaPozycji;

        #region Konstruktory

        public UmieszczenieViewPresenter(UmieszczenieView umieszczenieView)
            : base(umieszczenieView)
        {
        }

        #endregion
        #region W�asno�ci publiczne

        public UmieszczenieWorkItem MyWorkItem
        {
            get
            {
                return (WorkItemBase as UmieszczenieWorkItem);
            }
        }

        public IUmieszczenieView View
        {
            get
            {
                return (m_view as IUmieszczenieView);
            }
        }

        public DataModel.Umieszczenie Umieszczenie
        {
            get { return umieszczenie; }
            set
            {
                umieszczenie = value;
                AktualizujWidok();
            }
        }

        public float SumaIlosciUmieszczen
        {
            get { return sumaIlosciUmieszczen; }
            set { sumaIlosciUmieszczen = value; }
        }

        public float IloscPobrania
        {
            get { return iloscPobrania; }
            set { iloscPobrania = value; }
        }

        #endregion

        protected override void AttachView()
        {
            View.Anuluj += ViewAnuluj;
            View.WybierzLokalizacje += ViewWybierzLokalizacje;
            View.DodajLubZmien += ViewDodajLubZmien;
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    e.Handled = true;
                    ViewDodajLubZmien(this, EventArgs.Empty);
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    ViewAnuluj(this, EventArgs.Empty);
                    break;
                case Keys.Up:
                    e.Handled = true;
                    View.FocusNextControl(false);
                    break;
                case Keys.Down:
                    e.Handled = true;
                    View.FocusNextControl(true);
                    break;
                case Keys.D1:
                    e.Handled = true;
                    ViewWybierzLokalizacje(View, EventArgs.Empty);
                    break;
            }
        }

        void ViewAnuluj(object sender, EventArgs e)
        {
            MyWorkItem.MainWorkspace.Close(View);
            MyWorkItem.Parent.WorkItems.Get<RealizacjaMRWorkItem>(WorkItemsConstants.REALIZACJA_MR_WORKITEM).Activate();
        }

        void ViewWybierzLokalizacje(object sender, EventArgs e)
        {
            if (umieszczenie != null)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = umieszczenie.SymbolTowaru;
                MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = umieszczenie.SymbolTowaru;
            }
            else
            {
                MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = "";
                MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
            }

            MyWorkItem.RootWorkItem.State[StateConstants.LOKALIZACJA_Z_KARTOTEKI_INDEKSU] = "";
            MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueYes;
            MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueYes;


            MyWorkItem.RootWorkItem.Commands[CommandConstants.KARTOTEKA_INDEKSU].Execute();
        }

        void ViewDodajLubZmien(object sender, EventArgs e)
        {
            if (!TrwaDodawanieLubZmianaPozycji)
            {
                TrwaDodawanieLubZmianaPozycji = true;                

                if (View.Ilosc == null)
                {
                    MessageBox.Show("Ilo�� nie zosta�a uzupe�niona lub zosta� podany nieprawid�owy format liczby");
                    TrwaDodawanieLubZmianaPozycji = false;
                    return;
                }

                if (sumaIlosciUmieszczen + (View.Ilosc) > iloscPobrania)
                {
                    MessageBox.Show("Suma ilo�ci umieszcze� przekracza ilo�� pobra�");
                    TrwaDodawanieLubZmianaPozycji = false;
                    MyWorkItem.Activate();
                }
                else
                {
                    if (umieszczenie != null)
                    {
                        var moznaOkreslicNosnik = MyWorkItem.MoznaJednoznacznieOkreslicNosnik(umieszczenie.IdPartii,
                                                                                              View.Lokalizacja,
                                                                                              View.Ilosc);

                        if (
                            StatusOperacji.ERROR.Equals(
                                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                        {
                            TrwaDodawanieLubZmianaPozycji = false;
                            return;
                        }

                        if (!moznaOkreslicNosnik)
                        {
                            var listaNosnikowPresenter =
                                WorkItem.Items.Get<ListaNosnikowPresenter>(
                                    ItemsConstants.PRZESUNIECIE_LISTA_NOSNIKOW_PRESENTER);
                            listaNosnikowPresenter.WyswietlListe(umieszczenie.IdPartii, View.Lokalizacja, View.Ilosc);
                            MyWorkItem.MainWorkspace.Show(listaNosnikowPresenter.View);
                            NalezyDodacLubZmienicUmieszczenie = true;
                            TrwaDodawanieLubZmianaPozycji = false;
                            return;
                        }

                        DodajLubZmien();
                    }
                }
                TrwaDodawanieLubZmianaPozycji = false;
            }
        }

        [EventSubscription(EventBrokerConstants.WYBRANO_NOSNIK)]
        public void WybranoNosnik(Object sender, EventArgs e)
        {
            if (NalezyDodacLubZmienicUmieszczenie)
            {
                NalezyDodacLubZmienicUmieszczenie = false;
                var nosnId = WorkItem.State[StateConstants.IDENTYFIKATOR_NOSNIKA] as long?;
                if (nosnId != null)
                    umieszczenie.NosnId = (long)nosnId;
                DodajLubZmien();
            }
        }

        private void DodajLubZmien()
        {
            if (umieszczenie.Id > 0)
            {
                umieszczenie.Lokalizacja = Localization.AddPrefix(View.Lokalizacja);
                umieszczenie.Ilosc = (float)View.Ilosc;
                umieszczenie.IloscWLokalizacji = View.IloscWLokalizacji;
                MyWorkItem.ZmienPozycje(umieszczenie);
            }
            else
            {
                umieszczenie.Lokalizacja = Localization.AddPrefix(View.Lokalizacja);
                umieszczenie.Ilosc = (float)View.Ilosc;
                umieszczenie.IloscWLokalizacji = View.IloscWLokalizacji;
                MyWorkItem.DodajPozycjeUP(umieszczenie);
            }

            if (StatusOperacji.SUCCESS.Equals(MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                if (DodanoLubZmienionoPozycjeUmieszczenia != null)
                {
                    DodanoLubZmienionoPozycjeUmieszczenia(this, EventArgs.Empty);
                }
                MyWorkItem.MainWorkspace.Close(View);
                MyWorkItem.Parent.WorkItems.Get<RealizacjaMRWorkItem>(WorkItemsConstants.REALIZACJA_MR_WORKITEM).
                    Activate();
            }
        }

        private void AktualizujWidok()
        {
            if (umieszczenie != null)
            {
                View.Lokalizacja = umieszczenie.Lokalizacja;
                View.Ilosc = umieszczenie.Ilosc;
                View.IloscWLokalizacji = umieszczenie.IloscWLokalizacji;
            }
            else
            {
                View.Lokalizacja = null;
                View.Ilosc = null;
                View.IloscWLokalizacji = null;
            }
        }

        [EventSubscription(EventBrokerConstants.WYBRANO_LOKALIZACJE)]
        public void WybranoLokalizacje(object sender, EventArgs e)
        {
            View.SetNavigationState(true);
            View.Lokalizacja = MyWorkItem.RootWorkItem.State[StateConstants.LOKALIZACJA_Z_KARTOTEKI_INDEKSU].ToString();
            MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = "";
            MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
            MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
            MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;

            MyWorkItem.Activate();
        }

        public void UstawIlosc()
        {
            umieszczenie.Ilosc = iloscPobrania - sumaIlosciUmieszczen;
            AktualizujWidok();
        }

        public void UstawLokalizacje(string lokalizacja)
        {
            umieszczenie.Lokalizacja = lokalizacja;
            AktualizujWidok();
        }

    }
}
