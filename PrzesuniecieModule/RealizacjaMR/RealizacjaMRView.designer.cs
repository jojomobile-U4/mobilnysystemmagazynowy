
using Common.Components;

namespace PrzesuniecieModule.RealizacjaMR
{
    partial class RealizacjaMRView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbDokument = new System.Windows.Forms.TextBox();
            this.lbDokument = new System.Windows.Forms.Label();
            this.tbSymbolTowaru = new System.Windows.Forms.TextBox();
            this.lblSymbolTowaru = new System.Windows.Forms.Label();
            this.tbPobranie = new System.Windows.Forms.TextBox();
            this.lblPobranie = new System.Windows.Forms.Label();
            this.tbIlosc = new System.Windows.Forms.TextBox();
            this.lblIlosc = new System.Windows.Forms.Label();
            this.tbIloscWLokalizacji = new System.Windows.Forms.TextBox();
            this.lblIloscWLokalizacji = new System.Windows.Forms.Label();
            this.tbStanGoracy = new System.Windows.Forms.TextBox();
            this.lblStanGoracy = new System.Windows.Forms.Label();
            this.tbWagaJM = new System.Windows.Forms.TextBox();
            this.lblWagaJM = new System.Windows.Forms.Label();
            this.tbPozostawIlosc = new System.Windows.Forms.TextBox();
            this.lblPozostawIlosc = new System.Windows.Forms.Label();
            this.tbABC = new System.Windows.Forms.TextBox();
            this.lblABC = new System.Windows.Forms.Label();
            this.tbJM = new System.Windows.Forms.TextBox();
            this.lblJM = new System.Windows.Forms.Label();
            this.lsUmieszczenia = new Common.Components.MSMListView();
            this.chUmieszczenie = new System.Windows.Forms.ColumnHeader();
            this.chIlosc = new System.Windows.Forms.ColumnHeader();
            this.chIloscWLokalizacji = new System.Windows.Forms.ColumnHeader();
            this.btnPowrot = new System.Windows.Forms.Button();
            this.btnPobierz = new System.Windows.Forms.Button();
            this.btnZakoncz = new System.Windows.Forms.Button();
            this.btnUsun = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.tbStatusDokumentu = new System.Windows.Forms.TextBox();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.tbWagaLacznie = new System.Windows.Forms.TextBox();
            this.lblWagaLacznie = new System.Windows.Forms.Label();
            this.btnZmien = new System.Windows.Forms.Button();
            this.pnlNavigation2 = new System.Windows.Forms.Panel();
            this.btnSzukaj = new System.Windows.Forms.Button();
            this.leftRightControl1 = new Common.Components.LeftRightControl();
            this.pnlNavigation.SuspendLayout();
            this.pnlNavigation2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnSzukaj);
            this.pnlNavigation.Controls.Add(this.btnPowrot);
            this.pnlNavigation.Controls.Add(this.btnZakoncz);
            this.pnlNavigation.Controls.Add(this.btnPobierz);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 272);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 28);
            // 
            // lbTytul
            // 
            this.lbTytul.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 65;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "PRZESUNI�CIE - REALIZACJA";
            // 
            // tbDokument
            // 
            this.tbDokument.Location = new System.Drawing.Point(56, 19);
            this.tbDokument.Name = "tbDokument";
            this.tbDokument.ReadOnly = true;
            this.tbDokument.Size = new System.Drawing.Size(109, 21);
            this.tbDokument.TabIndex = 5;
            this.tbDokument.TabStop = false;
            // 
            // lbDokument
            // 
            this.lbDokument.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbDokument.Location = new System.Drawing.Point(0, 20);
            this.lbDokument.Name = "lbDokument";
            this.lbDokument.Size = new System.Drawing.Size(64, 21);
            this.lbDokument.Text = "Dokument:";
            // 
            // tbSymbolTowaru
            // 
            this.tbSymbolTowaru.Location = new System.Drawing.Point(56, 42);
            this.tbSymbolTowaru.Name = "tbSymbolTowaru";
            this.tbSymbolTowaru.ReadOnly = true;
            this.tbSymbolTowaru.Size = new System.Drawing.Size(180, 21);
            this.tbSymbolTowaru.TabIndex = 8;
            this.tbSymbolTowaru.TabStop = false;
            // 
            // lblSymbolTowaru
            // 
            this.lblSymbolTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblSymbolTowaru.Location = new System.Drawing.Point(0, 43);
            this.lblSymbolTowaru.Name = "lblSymbolTowaru";
            this.lblSymbolTowaru.Size = new System.Drawing.Size(48, 21);
            this.lblSymbolTowaru.Text = "Indeks:";
            // 
            // tbPobranie
            // 
            this.tbPobranie.Location = new System.Drawing.Point(56, 65);
            this.tbPobranie.Name = "tbPobranie";
            this.tbPobranie.ReadOnly = true;
            this.tbPobranie.Size = new System.Drawing.Size(77, 21);
            this.tbPobranie.TabIndex = 11;
            this.tbPobranie.TabStop = false;
            // 
            // lblPobranie
            // 
            this.lblPobranie.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblPobranie.Location = new System.Drawing.Point(0, 71);
            this.lblPobranie.Name = "lblPobranie";
            this.lblPobranie.Size = new System.Drawing.Size(56, 21);
            this.lblPobranie.Text = "Pobranie:";
            // 
            // tbIlosc
            // 
            this.tbIlosc.Location = new System.Drawing.Point(56, 88);
            this.tbIlosc.Name = "tbIlosc";
            this.tbIlosc.ReadOnly = true;
            this.tbIlosc.Size = new System.Drawing.Size(77, 21);
            this.tbIlosc.TabIndex = 0;
            this.tbIlosc.Validated += new System.EventHandler(this.tbIlosc_Validated);
            // 
            // lblIlosc
            // 
            this.lblIlosc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblIlosc.Location = new System.Drawing.Point(0, 95);
            this.lblIlosc.Name = "lblIlosc";
            this.lblIlosc.Size = new System.Drawing.Size(52, 21);
            this.lblIlosc.Text = "Ilo��:";
            // 
            // tbIloscWLokalizacji
            // 
            this.tbIloscWLokalizacji.Location = new System.Drawing.Point(194, 88);
            this.tbIloscWLokalizacji.Name = "tbIloscWLokalizacji";
            this.tbIloscWLokalizacji.ReadOnly = true;
            this.tbIloscWLokalizacji.Size = new System.Drawing.Size(42, 21);
            this.tbIloscWLokalizacji.TabIndex = 17;
            this.tbIloscWLokalizacji.TabStop = false;
            this.tbIloscWLokalizacji.TextChanged += new System.EventHandler(this.tbIloscWLokalizacji_TextChanged);
            // 
            // lblIloscWLokalizacji
            // 
            this.lblIloscWLokalizacji.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblIloscWLokalizacji.Location = new System.Drawing.Point(134, 96);
            this.lblIloscWLokalizacji.Name = "lblIloscWLokalizacji";
            this.lblIloscWLokalizacji.Size = new System.Drawing.Size(84, 21);
            this.lblIloscWLokalizacji.Text = "Ilo�� w lok.:";
            // 
            // tbStanGoracy
            // 
            this.tbStanGoracy.Location = new System.Drawing.Point(56, 111);
            this.tbStanGoracy.Name = "tbStanGoracy";
            this.tbStanGoracy.ReadOnly = true;
            this.tbStanGoracy.Size = new System.Drawing.Size(77, 21);
            this.tbStanGoracy.TabIndex = 20;
            this.tbStanGoracy.TabStop = false;
            // 
            // lblStanGoracy
            // 
            this.lblStanGoracy.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblStanGoracy.Location = new System.Drawing.Point(0, 111);
            this.lblStanGoracy.Name = "lblStanGoracy";
            this.lblStanGoracy.Size = new System.Drawing.Size(56, 27);
            this.lblStanGoracy.Text = "Stan gor�cy:";
            // 
            // tbWagaJM
            // 
            this.tbWagaJM.Location = new System.Drawing.Point(56, 134);
            this.tbWagaJM.Name = "tbWagaJM";
            this.tbWagaJM.ReadOnly = true;
            this.tbWagaJM.Size = new System.Drawing.Size(77, 21);
            this.tbWagaJM.TabIndex = 1;
            this.tbWagaJM.TabStop = false;
            // 
            // lblWagaJM
            // 
            this.lblWagaJM.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWagaJM.Location = new System.Drawing.Point(0, 141);
            this.lblWagaJM.Name = "lblWagaJM";
            this.lblWagaJM.Size = new System.Drawing.Size(100, 21);
            this.lblWagaJM.Text = "Waga j.m.:";
            // 
            // tbPozostawIlosc
            // 
            this.tbPozostawIlosc.Location = new System.Drawing.Point(194, 65);
            this.tbPozostawIlosc.Name = "tbPozostawIlosc";
            this.tbPozostawIlosc.ReadOnly = true;
            this.tbPozostawIlosc.Size = new System.Drawing.Size(42, 21);
            this.tbPozostawIlosc.TabIndex = 33;
            this.tbPozostawIlosc.TabStop = false;
            // 
            // lblPozostawIlosc
            // 
            this.lblPozostawIlosc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblPozostawIlosc.Location = new System.Drawing.Point(135, 72);
            this.lblPozostawIlosc.Name = "lblPozostawIlosc";
            this.lblPozostawIlosc.Size = new System.Drawing.Size(77, 21);
            this.lblPozostawIlosc.Text = "Poz. ilo��:";
            // 
            // tbABC
            // 
            this.tbABC.Location = new System.Drawing.Point(194, 111);
            this.tbABC.Name = "tbABC";
            this.tbABC.ReadOnly = true;
            this.tbABC.Size = new System.Drawing.Size(42, 21);
            this.tbABC.TabIndex = 36;
            this.tbABC.TabStop = false;
            // 
            // lblABC
            // 
            this.lblABC.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblABC.Location = new System.Drawing.Point(134, 118);
            this.lblABC.Name = "lblABC";
            this.lblABC.Size = new System.Drawing.Size(43, 21);
            this.lblABC.Text = "ABC:";
            // 
            // tbJM
            // 
            this.tbJM.Location = new System.Drawing.Point(194, 134);
            this.tbJM.Name = "tbJM";
            this.tbJM.ReadOnly = true;
            this.tbJM.Size = new System.Drawing.Size(42, 21);
            this.tbJM.TabIndex = 39;
            this.tbJM.TabStop = false;
            // 
            // lblJM
            // 
            this.lblJM.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblJM.Location = new System.Drawing.Point(135, 141);
            this.lblJM.Name = "lblJM";
            this.lblJM.Size = new System.Drawing.Size(40, 21);
            this.lblJM.Text = "J.m.:";
            // 
            // lsUmieszczenia
            // 
            this.lsUmieszczenia.Columns.Add(this.chUmieszczenie);
            this.lsUmieszczenia.Columns.Add(this.chIlosc);
            this.lsUmieszczenia.Columns.Add(this.chIloscWLokalizacji);
            this.lsUmieszczenia.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lsUmieszczenia.FullRowSelect = true;
            this.lsUmieszczenia.Location = new System.Drawing.Point(2, 158);
            this.lsUmieszczenia.Name = "lsUmieszczenia";
            this.lsUmieszczenia.Size = new System.Drawing.Size(180, 89);
            this.lsUmieszczenia.TabIndex = 1;
            this.lsUmieszczenia.View = System.Windows.Forms.View.Details;
            // 
            // chUmieszczenie
            // 
            this.chUmieszczenie.Text = "Umieszczenie";
            this.chUmieszczenie.Width = 70;
            // 
            // chIlosc
            // 
            this.chIlosc.Text = "Ilo��";
            this.chIlosc.Width = 40;
            // 
            // chIloscWLokalizacji
            // 
            this.chIloscWLokalizacji.Text = "Ilo�� w lok.";
            this.chIloscWLokalizacji.Width = 60;
            // 
            // btnPowrot
            // 
            this.btnPowrot.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPowrot.Location = new System.Drawing.Point(165, 3);
            this.btnPowrot.Name = "btnPowrot";
            this.btnPowrot.Size = new System.Drawing.Size(71, 22);
            this.btnPowrot.TabIndex = 42;
            this.btnPowrot.TabStop = false;
            this.btnPowrot.Text = "&Esc Powr�t";
            this.btnPowrot.Click += new System.EventHandler(this.btnPowrot_Click);
            // 
            // btnPobierz
            // 
            this.btnPobierz.Enabled = false;
            this.btnPobierz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPobierz.Location = new System.Drawing.Point(73, 3);
            this.btnPobierz.Name = "btnPobierz";
            this.btnPobierz.Size = new System.Drawing.Size(45, 22);
            this.btnPobierz.TabIndex = 44;
            this.btnPobierz.TabStop = false;
            this.btnPobierz.Text = "&2 Pob.";
            this.btnPobierz.Click += new System.EventHandler(this.btnPobierz_Click);
            // 
            // btnZakoncz
            // 
            this.btnZakoncz.Enabled = false;
            this.btnZakoncz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnZakoncz.Location = new System.Drawing.Point(119, 3);
            this.btnZakoncz.Name = "btnZakoncz";
            this.btnZakoncz.Size = new System.Drawing.Size(45, 22);
            this.btnZakoncz.TabIndex = 45;
            this.btnZakoncz.TabStop = false;
            this.btnZakoncz.Text = "&3 Zak.";
            this.btnZakoncz.Click += new System.EventHandler(this.btnZakoncz_Click);
            // 
            // btnUsun
            // 
            this.btnUsun.Enabled = false;
            this.btnUsun.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnUsun.Location = new System.Drawing.Point(3, 50);
            this.btnUsun.Name = "btnUsun";
            this.btnUsun.Size = new System.Drawing.Size(51, 20);
            this.btnUsun.TabIndex = 46;
            this.btnUsun.TabStop = false;
            this.btnUsun.Text = "&6 Usu�";
            this.btnUsun.Click += new System.EventHandler(this.btnUsun_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblStatus.Location = new System.Drawing.Point(165, 20);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(49, 21);
            this.lblStatus.Text = "Status:";
            // 
            // tbStatusDokumentu
            // 
            this.tbStatusDokumentu.Location = new System.Drawing.Point(208, 19);
            this.tbStatusDokumentu.Name = "tbStatusDokumentu";
            this.tbStatusDokumentu.ReadOnly = true;
            this.tbStatusDokumentu.Size = new System.Drawing.Size(28, 21);
            this.tbStatusDokumentu.TabIndex = 50;
            this.tbStatusDokumentu.TabStop = false;
            // 
            // btnDodaj
            // 
            this.btnDodaj.Enabled = false;
            this.btnDodaj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnDodaj.Location = new System.Drawing.Point(3, 4);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(51, 20);
            this.btnDodaj.TabIndex = 51;
            this.btnDodaj.TabStop = false;
            this.btnDodaj.Text = "&4 Dodaj";
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // tbWagaLacznie
            // 
            this.tbWagaLacznie.Location = new System.Drawing.Point(195, 250);
            this.tbWagaLacznie.Name = "tbWagaLacznie";
            this.tbWagaLacznie.ReadOnly = true;
            this.tbWagaLacznie.Size = new System.Drawing.Size(42, 21);
            this.tbWagaLacznie.TabIndex = 55;
            this.tbWagaLacznie.TabStop = false;
            // 
            // lblWagaLacznie
            // 
            this.lblWagaLacznie.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWagaLacznie.Location = new System.Drawing.Point(118, 252);
            this.lblWagaLacznie.Name = "lblWagaLacznie";
            this.lblWagaLacznie.Size = new System.Drawing.Size(75, 18);
            this.lblWagaLacznie.Text = "Waga ��cznie:";
            // 
            // btnZmien
            // 
            this.btnZmien.Enabled = false;
            this.btnZmien.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnZmien.Location = new System.Drawing.Point(3, 27);
            this.btnZmien.Name = "btnZmien";
            this.btnZmien.Size = new System.Drawing.Size(51, 20);
            this.btnZmien.TabIndex = 70;
            this.btnZmien.TabStop = false;
            this.btnZmien.Text = "&5 Zmie�";
            this.btnZmien.Click += new System.EventHandler(this.btnZmien_Click);
            // 
            // pnlNavigation2
            // 
            this.pnlNavigation2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.pnlNavigation2.Controls.Add(this.btnZmien);
            this.pnlNavigation2.Controls.Add(this.btnDodaj);
            this.pnlNavigation2.Controls.Add(this.btnUsun);
            this.pnlNavigation2.Location = new System.Drawing.Point(181, 154);
            this.pnlNavigation2.Name = "pnlNavigation2";
            this.pnlNavigation2.Size = new System.Drawing.Size(57, 93);
            // 
            // btnSzukaj
            // 
            this.btnSzukaj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnSzukaj.Location = new System.Drawing.Point(1, 3);
            this.btnSzukaj.Name = "btnSzukaj";
            this.btnSzukaj.Size = new System.Drawing.Size(71, 22);
            this.btnSzukaj.TabIndex = 46;
            this.btnSzukaj.TabStop = false;
            this.btnSzukaj.Text = "&1 Szukaj";
            this.btnSzukaj.Click += new System.EventHandler(this.btnSzukaj_Click);
            // 
            // leftRightControl1
            // 
            this.leftRightControl1.BackColor = System.Drawing.SystemColors.Desktop;
            this.leftRightControl1.Location = new System.Drawing.Point(207, 0);
            this.leftRightControl1.Name = "leftRightControl1";
            this.leftRightControl1.NextEnabled = true;
            this.leftRightControl1.PreviousEnabled = true;
            this.leftRightControl1.Size = new System.Drawing.Size(32, 16);
            this.leftRightControl1.TabIndex = 71;
            this.leftRightControl1.TabStop = false;
            this.leftRightControl1.Next += new System.EventHandler(this.OnNastepny);
            this.leftRightControl1.Previous += new System.EventHandler(this.OnPoprzedni);
            // 
            // RealizacjaMRView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.leftRightControl1);
            this.Controls.Add(this.tbWagaLacznie);
            this.Controls.Add(this.tbStatusDokumentu);
            this.Controls.Add(this.lsUmieszczenia);
            this.Controls.Add(this.tbJM);
            this.Controls.Add(this.lblJM);
            this.Controls.Add(this.tbABC);
            this.Controls.Add(this.lblABC);
            this.Controls.Add(this.tbPozostawIlosc);
            this.Controls.Add(this.tbWagaJM);
            this.Controls.Add(this.lblWagaJM);
            this.Controls.Add(this.tbStanGoracy);
            this.Controls.Add(this.lblStanGoracy);
            this.Controls.Add(this.tbIloscWLokalizacji);
            this.Controls.Add(this.lblIloscWLokalizacji);
            this.Controls.Add(this.tbIlosc);
            this.Controls.Add(this.lblIlosc);
            this.Controls.Add(this.tbPobranie);
            this.Controls.Add(this.lblPobranie);
            this.Controls.Add(this.tbSymbolTowaru);
            this.Controls.Add(this.lblSymbolTowaru);
            this.Controls.Add(this.tbDokument);
            this.Controls.Add(this.lbDokument);
            this.Controls.Add(this.lbTytul);
            this.Controls.Add(this.lblPozostawIlosc);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblWagaLacznie);
            this.Controls.Add(this.pnlNavigation2);
            this.Name = "RealizacjaMRView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation2, 0);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lblWagaLacznie, 0);
            this.Controls.SetChildIndex(this.lblStatus, 0);
            this.Controls.SetChildIndex(this.lblPozostawIlosc, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lbDokument, 0);
            this.Controls.SetChildIndex(this.tbDokument, 0);
            this.Controls.SetChildIndex(this.lblSymbolTowaru, 0);
            this.Controls.SetChildIndex(this.tbSymbolTowaru, 0);
            this.Controls.SetChildIndex(this.lblPobranie, 0);
            this.Controls.SetChildIndex(this.tbPobranie, 0);
            this.Controls.SetChildIndex(this.lblIlosc, 0);
            this.Controls.SetChildIndex(this.tbIlosc, 0);
            this.Controls.SetChildIndex(this.lblIloscWLokalizacji, 0);
            this.Controls.SetChildIndex(this.tbIloscWLokalizacji, 0);
            this.Controls.SetChildIndex(this.lblStanGoracy, 0);
            this.Controls.SetChildIndex(this.tbStanGoracy, 0);
            this.Controls.SetChildIndex(this.lblWagaJM, 0);
            this.Controls.SetChildIndex(this.tbWagaJM, 0);
            this.Controls.SetChildIndex(this.tbPozostawIlosc, 0);
            this.Controls.SetChildIndex(this.lblABC, 0);
            this.Controls.SetChildIndex(this.tbABC, 0);
            this.Controls.SetChildIndex(this.lblJM, 0);
            this.Controls.SetChildIndex(this.tbJM, 0);
            this.Controls.SetChildIndex(this.lsUmieszczenia, 0);
            this.Controls.SetChildIndex(this.tbStatusDokumentu, 0);
            this.Controls.SetChildIndex(this.tbWagaLacznie, 0);
            this.Controls.SetChildIndex(this.leftRightControl1, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.pnlNavigation2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MSMLabel lbTytul;
        private System.Windows.Forms.TextBox tbDokument;
        private System.Windows.Forms.Label lbDokument;
        private System.Windows.Forms.TextBox tbSymbolTowaru;
        private System.Windows.Forms.Label lblSymbolTowaru;
        private System.Windows.Forms.TextBox tbPobranie;
        private System.Windows.Forms.Label lblPobranie;
        private System.Windows.Forms.TextBox tbIlosc;
        private System.Windows.Forms.Label lblIlosc;
        private System.Windows.Forms.TextBox tbIloscWLokalizacji;
        private System.Windows.Forms.Label lblIloscWLokalizacji;
        private System.Windows.Forms.TextBox tbStanGoracy;
        private System.Windows.Forms.Label lblStanGoracy;
        private System.Windows.Forms.TextBox tbWagaJM;
        private System.Windows.Forms.Label lblWagaJM;
        private System.Windows.Forms.TextBox tbPozostawIlosc;
        private System.Windows.Forms.Label lblPozostawIlosc;
        private System.Windows.Forms.TextBox tbABC;
        private System.Windows.Forms.Label lblABC;
        private System.Windows.Forms.TextBox tbJM;
        private System.Windows.Forms.Label lblJM;
        private MSMListView lsUmieszczenia;
        private System.Windows.Forms.Button btnPowrot;
        private System.Windows.Forms.Button btnPobierz;
        private System.Windows.Forms.Button btnZakoncz;
        private System.Windows.Forms.ColumnHeader chUmieszczenie;
        private System.Windows.Forms.ColumnHeader chIlosc;
        private System.Windows.Forms.ColumnHeader chIloscWLokalizacji;
        private System.Windows.Forms.Button btnUsun;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.TextBox tbStatusDokumentu;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.TextBox tbWagaLacznie;
        private System.Windows.Forms.Label lblWagaLacznie;
        private System.Windows.Forms.Button btnZmien;
        private System.Windows.Forms.Panel pnlNavigation2;
        private System.Windows.Forms.Button btnSzukaj;
        private LeftRightControl leftRightControl1;
    }
}
