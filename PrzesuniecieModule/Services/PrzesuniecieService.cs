#region

using System;
using System.Collections.Generic;
using System.Threading;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using PrzesuniecieModule.DataModel;
using PrzesuniecieModule.ListaTowarow.Search;
using PrzesuniecieModule.ProxyWrappers;
using PrzesuniecieModule.PrzesuniecieProxy;
using PrzesuniecieModule.RealizacjaMR.Search;
using StatusOperacji = Common.DataModel.StatusOperacji;
using Towar = PrzesuniecieModule.DataModel.Towar;

#endregion

namespace PrzesuniecieModule.Services
{
    [Service(typeof(IPrzesuniecieService))]
    public class PrzesuniecieService : ServiceBase, IPrzesuniecieService
    {
        #region Event Publication

        [EventPublication(EventBrokerConstants.ODCZYTANO_STAN_GORACY)]
        public event EventHandler OdczytanoStanGoracy;

        [EventPublication(EventBrokerConstants.ODCZYTANO_ILOSC_W_LOK)]
        public event EventHandler OdczytanoIloscWLokalizacji;

        #endregion

        #region Private Fields

        private WsPrzesuniecieWrap asyncServiceAgent;

        #endregion

        #region Properties

        protected override WebServiceConfiguration Configuration
        {
            get { return MyWorkItem.Configuration.PrzesuniecieWS; }
        }

        #endregion

        #region Konstruktor

        public PrzesuniecieService([ServiceDependency] WorkItem workItem)
            : base(workItem)
        {
        }

        #endregion

        #region Pobierz liste towar�w do przesuni�cia

        public DataModel.ListaTowarow PobierzListeTowarow(KryteriaZapytaniaListyTowarow kryteria, int? numerStrony)
        {
            var listaTowarow = new DataModel.ListaTowarow();
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param =
                        new WsPrzesuniecieWrap.pobierzListeTowarowDoPrzesunieciaWrap { symbol = kryteria.Symbol, abc = kryteria.Abc, lokalizacja = Localization.AddPrefix(kryteria.Lokalizacja), numerStrony = numerStrony ?? 0, wielkoscStrony = Configuration.WielkoscStrony };


                    var listaTowarowProxy = serviceAgent.pobierzListeTowarowDoPrzesuniecia(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = listaTowarowProxy.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = listaTowarowProxy.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = listaTowarowProxy.statusOperacji.stosWywolan;

                    if ((listaTowarowProxy.statusOperacji.status.Equals(StatusOperacji.SUCCESS)))
                    {
                        if ((listaTowarowProxy.lista != null) && (listaTowarowProxy.lista.Length > 0))
                        {
                            listaTowarow.LiczbaPozycji = listaTowarowProxy.iloscTowarow;
                            listaTowarow.Lista = new Towar[listaTowarowProxy.lista.Length];
                            var i = 0;
                            foreach (var towarProxy in listaTowarowProxy.lista)
                            {
                                var towar = new Towar
                                                {
                                                    Id = towarProxy.id,
                                                    Abc = towarProxy.abc,
                                                    Ilosc = towarProxy.ilosc,
                                                    Jm = towarProxy.jm,
                                                    Lokalizacja = Localization.RemovePrefix(towarProxy.lokalizacja),
                                                    NazwaTowaru = towarProxy.nazwaTowaru,
                                                    Odmiana = towarProxy.odmiana,
                                                    StanGoracy = towarProxy.stanGoracy,
                                                    Symbol = towarProxy.symbol,
                                                    Lp = towarProxy.lp,
                                                    NosnId = towarProxy.nosnId,
                                                    PainId =  towarProxy.painId
                                                };

                                listaTowarow.Lista[i] = towar;
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
            return listaTowarow;
        }

        #endregion

        #region Pobierz liste towar�w dla lokalizacji

        public DataModel.ListaTowarow PobierzListeTowarowDlaLokalizacji(string lokalizacja)
        {
            var listaTowarow = new DataModel.ListaTowarow();
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param =
                        new WsPrzesuniecieWrap.pobierzListeTowarowDlaLokalizacjiWrap { numerStrony = 0, lokalizacja = Localization.AddPrefix(lokalizacja) };

                    PrzesuniecieProxy.ListaTowarow listaTowarowProxy = serviceAgent.pobierzListeTowarowDlaLokalizacji(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = listaTowarowProxy.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = listaTowarowProxy.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = listaTowarowProxy.statusOperacji.stosWywolan;

                    if ((listaTowarowProxy.statusOperacji.status.Equals(StatusOperacji.SUCCESS)))
                    {
                        listaTowarow.LiczbaPozycji = listaTowarowProxy.iloscTowarow;
                        if (listaTowarowProxy.lista != null)
                        {
                            listaTowarow.Lista = new Towar[listaTowarowProxy.lista.Length];
                            var i = 0;
                            foreach (var towarProxy in listaTowarowProxy.lista)
                            {
                                var towar = new Towar();
                                towar.Id = towarProxy.id;
                                towar.Abc = towarProxy.abc;
                                towar.Ilosc = towarProxy.ilosc;
                                towar.Jm = towarProxy.jm;
                                towar.Lokalizacja = Localization.RemovePrefix(towarProxy.lokalizacja);
                                towar.NazwaTowaru = towarProxy.nazwaTowaru;
                                towar.Odmiana = towarProxy.odmiana;
                                towar.StanGoracy = towarProxy.stanGoracy;
                                towar.Symbol = towarProxy.symbol;
                                towar.Lp = towarProxy.lp;
                                towar.NosnId = towarProxy.nosnId;

                                listaTowarow.Lista[i] = towar;
                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }

            return listaTowarow;
        }

        #endregion

        #region Usu� z listy towar�w do przesuni�cia

        /// <summary>
        /// Usuwa towar z listy
        /// </summary>
        /// <param name="iDTowaru">Identyfikator towaru do usuni�cia</param>
        public void UsunZListyTowarow(long iDTowaru)
        {
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new usunZListyTowarowDoPrzesuniecia { id = iDTowaru };

                    var statusOperacji = serviceAgent.usunZListyTowarowDoPrzesuniecia(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = statusOperacji.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        #endregion

        #region Pobierz list� pozycji UP

        /// <summary>
        /// Pobiera list� pozycji UP
        /// </summary>
        public SortedList<long, Umieszczenie> PobierzListePozycjiUP(long idDokumentu, long idPartii)
        {
            var umieszczenia = new SortedList<long, Umieszczenie>();
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new WsPrzesuniecieWrap.pobierzListePozycjiUPWrap
                                    {
                                        idDokumentu = idDokumentu,
                                        idPartii = idPartii,
                                        numerStrony = 0,
                                        wielkoscStrony = null
                                    };

                    var listaPozycji = serviceAgent.pobierzListePozycjiUP(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = listaPozycji.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = listaPozycji.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = listaPozycji.statusOperacji.stosWywolan;

                    if ((listaPozycji.pozycje != null) && (listaPozycji.pozycje.Length > 0))
                    {
                        foreach (var pozycja in listaPozycji.pozycje)
                        {
                            var umieszczenie = new Umieszczenie
                                                   {
                                                       Id = pozycja.id,
                                                       Ilosc = pozycja.ilosc,
                                                       Lokalizacja = Localization.RemovePrefix(pozycja.lokalizacja),
                                                       Max = pozycja.iloscMaksymalnaWLokalizacji,
                                                       IdPartii = pozycja.idPartii,
                                                       SymbolTowaru = pozycja.symbolTowaru,
                                                       IdDokumentu = pozycja.idDokumentu,
                                                       JednostkaMiary = pozycja.jednostkaMiary,
                                                       NosnId = pozycja.nosnId
                                                   };
                            umieszczenia.Add(umieszczenie.Id, umieszczenie);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }

            return umieszczenia;
        }

        #endregion

        #region Dodaj pozycj� UP

        /// <summary>
        /// Dodaje pozycj� UP
        /// </summary>
        public void DodajPozycjeUP(Umieszczenie umieszczenie)
        {
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    var pozycja = new Pozycja();
                    pozycja.symbolTowaru = umieszczenie.SymbolTowaru;
                    pozycja.idDokumentu = umieszczenie.IdDokumentu;
                    pozycja.idPartii = umieszczenie.IdPartii;
                    pozycja.ilosc = umieszczenie.Ilosc;
                    pozycja.lokalizacja = umieszczenie.Lokalizacja;
                    pozycja.jednostkaMiary = umieszczenie.JednostkaMiary;
                    pozycja.nosnId = umieszczenie.NosnId;

                    SecureSoapMessage(serviceAgent);
                    var param = new WsPrzesuniecieWrap.dodajPozycjeUPWrap { pozycja = pozycja };

                    var statusOperacji = serviceAgent.dodajPozycjeUP(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = statusOperacji.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        #endregion

        #region Zmie� pozycj�

        /// <summary>
        /// Zmienia dane pozycji
        /// </summary>
        public void ZmienPozycje(Umieszczenie umieszczenie)
        {
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    var pozycja = new Pozycja();
                    pozycja.idDokumentu = umieszczenie.IdDokumentu;
                    pozycja.id = umieszczenie.Id;
                    pozycja.idPartii = umieszczenie.IdPartii;
                    pozycja.ilosc = umieszczenie.Ilosc;
                    pozycja.lokalizacja = umieszczenie.Lokalizacja;
                    pozycja.jednostkaMiary = umieszczenie.JednostkaMiary;
                    pozycja.nosnId = umieszczenie.NosnId;

                    SecureSoapMessage(serviceAgent);
                    var param = new WsPrzesuniecieWrap.zmienPozycjeWrap();
                    param.pozycja = pozycja;

                    var statusOperacji = serviceAgent.zmienPozycje(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = statusOperacji.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        #endregion

        #region Zako�cz pozycje UP dokumentu

        /// <summary>
        /// Zaka�cza pozycje UP na dokumencie MR
        /// </summary>
        public void ZakonczPozycjeUP(long idDokumentu)
        {
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new WsPrzesuniecieWrap.zakonczPozycjeUPWrap { idDokumentu = idDokumentu };

                    var statusOperacji = serviceAgent.zakonczPozycjeUP(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = statusOperacji.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        #endregion

        #region Usu� pozycj�

        /// <summary>
        /// Usuwa pozycj� UP
        /// </summary>
        public void UsunPozycjeUP(long id)
        {
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new WsPrzesuniecieWrap.usunPozycjeWrap();
                    param.id = id;

                    var statusOperacji = serviceAgent.usunPozycje(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = statusOperacji.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        #endregion

        #region Zaznacz przesuni�cie towaru

        /// <summary>
        /// Oznacza towar z listy statusem Przesuniecie='T'
        /// </summary>
        /// <param name="idTowaru">Identyfikator towaru do zaznaczenia</param>
        public void ZaznaczPrzesuniecieTowaru(long idTowaru)
        {
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new zaznaczPrzesuniecieTowaru { id = idTowaru };

                    var statusOperacji = serviceAgent.zaznaczPrzesuniecieTowaru(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = statusOperacji.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        #endregion

        #region Pobranie listy dokument�w

        public DokumentMR[] PobierzListeDokumentow(KryteriaZapytaniaRealizacjiMR kryteria, int? numerStrony)
        {
            DokumentMR[] lista = null;
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param =
                        new WsPrzesuniecieWrap.pobierzListeDokumentowWrap();
                    param.id = kryteria.Id;
                    param.idPozycji = kryteria.IdPozycji;
                    param.symbolTowaru = kryteria.Towar;
                    param.lokalizacja = Localization.AddPrefix(kryteria.Lokalizacja);
                    param.abc = kryteria.Abc != null ? kryteria.Abc.ToUpper() : null;
                    param.symbol = kryteria.SymbolDokumentu != null ? kryteria.SymbolDokumentu.ToUpper() : null;
                    param.numerStrony = numerStrony ?? 0;
                    param.wielkoscStrony = Configuration.WielkoscStrony;

                    ListaDokumentow listaDokumentow;

                    listaDokumentow = serviceAgent.pobierzListeDokumentow(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = listaDokumentow.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = listaDokumentow.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = listaDokumentow.statusOperacji.stosWywolan;

                    if (listaDokumentow.statusOperacji.status.Equals(StatusOperacji.SUCCESS))
                    {
                        lista = KonwertujNaDokumentMr(listaDokumentow.lista);
                    }
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
            return lista;
        }

        #endregion

        #region Utw�rz dokument MR z nowego przesuni�cia

        public long? UtworzDokumentMR(string symbolTowaru, string lokalizacja, long? nosnId, string jM, int odmiana, float? ilosc)
        {
            var idDokumentu = new Id();

            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new WsPrzesuniecieWrap.utworzDokumentMRWrap { symbolTowaru = symbolTowaru, lokalizacja = Localization.AddPrefix(lokalizacja), jednostkaMiary = jM, odmiana = odmiana, ilosc = ilosc, nosnId = nosnId };

                    idDokumentu = serviceAgent.utworzDokumentMR(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = idDokumentu.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = idDokumentu.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = idDokumentu.statusOperacji.stosWywolan;

                    if ((!idDokumentu.statusOperacji.status.Equals("S")) || (idDokumentu.id == -1))
                    {
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }

            return idDokumentu.id;
        }

        #endregion

        #region Utw�rz dokument MR wed�ug Id towaru do przesuni�cia

        public long? UtworzDokumentMRWgIdTowaruDoPrzesuniecia(long id, long? nosnId)
        {
            var idDokumentu = new Id();

            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new utworzDokumentMRWgIdTowaruDoPrzesuniecia { idTowaru = id, nosnId = nosnId };

                    idDokumentu = serviceAgent.utworzDokumentMRWgIdTowaruDoPrzesuniecia(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = idDokumentu.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = idDokumentu.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = idDokumentu.statusOperacji.stosWywolan;

                    if ((!idDokumentu.statusOperacji.status.Equals("S")) || (idDokumentu.id == -1))
                    {
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }

            return idDokumentu.id;
        }

        #endregion

        #region Zatwierd� pobranie

        public void ZatwierdzPobranie(long id, float ilosc, float wagaJm)
        {
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param =
                        new WsPrzesuniecieWrap.zatwierdzPobranieWrap { id = id, ilosc = ilosc, waga = wagaJm };

                    var statusOperacji = serviceAgent.zatwierdzPobranie(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = statusOperacji.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        #endregion

        #region Lokalizacja jest wolna

        public bool LokalizacjaJestWolna(string lokalizacja)
        {
            var pusta = false;
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new lokalizacjaJestWolna { lokalizacja = Localization.AddPrefix(lokalizacja) };

                    StanLokalizacji stanLokalizacji = serviceAgent.lokalizacjaJestWolna(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = stanLokalizacji.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = stanLokalizacji.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = stanLokalizacji.statusOperacji.stosWywolan;

                    //je�li zako�czy�o si� sukcesem pobieram dane
                    if (stanLokalizacji.statusOperacji.status.Equals(StatusOperacji.SUCCESS))
                    {
                        pusta = stanLokalizacji.jestWolna.ToUpper().StartsWith("T");
                    }
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
            return pusta;
        }

        #endregion

        #region Maksymalna ilo�� dla lokalizacji

        public float MaksymalnaIloscDlaLokalizacji(string symbol, string lokalizacja)
        {
            float max = 0;
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param =
                        new WsPrzesuniecieWrap.maksymalnaIloscDlaLokalizacjiWrap { lokalizacja = Localization.AddPrefix(lokalizacja), symbol = symbol };

                    Ilosc maksymalnaIloscDlaLokalizacji = serviceAgent.maksymalnaIloscDlaLokalizacji(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] =
                        maksymalnaIloscDlaLokalizacji.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = maksymalnaIloscDlaLokalizacji.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] =
                        maksymalnaIloscDlaLokalizacji.statusOperacji.stosWywolan;

                    //je�li zako�czy�o si� sukcesem pobieram dane
                    if (maksymalnaIloscDlaLokalizacji.statusOperacji.status.Equals(StatusOperacji.SUCCESS))
                    {
                        max = maksymalnaIloscDlaLokalizacji.ilosc;
                    }
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
            return max;
        }

        #endregion

        #region Zatwierdzenie dokumentu MR i dodanie pozycji UP

        public void ZatwierdzDokumentMR(long idDokumentu, IList<Umieszczenie> umieszczenia)
        {
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param =
                        new WsPrzesuniecieWrap.dodajPozycjeUPIZatwierdzMRWrap { idDokumentu = idDokumentu, pozycje = KonwertujNaPozycje(umieszczenia) };

                    PrzesuniecieProxy.StatusOperacji statusOperacji = serviceAgent.dodajPozycjeUPIZatwierdzMR(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = statusOperacji.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        #endregion

        #region Rezerwuj dokument

        public void RezerwujDokument(long id)
        {
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new rezerwujDokument { id = id };

                    PrzesuniecieProxy.StatusOperacji statusOperacji = serviceAgent.rezerwujDokument(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = statusOperacji.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        #endregion

        #region Translacja obiekt�w

        private static DokumentMR[] KonwertujNaDokumentMr(Dokument[] listaDokumentow)
        {
            Umieszczenie umieszczenie;
            DokumentMR[] listaDokumentowMr;
            if (listaDokumentow != null)
            {
                listaDokumentowMr = new DokumentMR[listaDokumentow.Length];
                for (var i = 0; i < listaDokumentow.Length; i++)
                {
                    listaDokumentowMr[i] = new DokumentMR();
                    listaDokumentowMr[i].ID = listaDokumentow[i].naglowek.id;
                    listaDokumentowMr[i].Dokument = listaDokumentow[i].naglowek.symbol;
                    listaDokumentowMr[i].StatusDokumentu = listaDokumentow[i].naglowek.status;
                    listaDokumentowMr[i].Umieszczenie = new SortedList<long, Umieszczenie>();
                    for (var j = 0; j < listaDokumentow[i].pozycje.Length; j++)
                    {
                        if (listaDokumentow[i].pozycje[j].rodzaj.ToUpper().Equals("PP"))
                        {
                            listaDokumentowMr[i].IdPobrania = listaDokumentow[i].pozycje[j].id;
                            listaDokumentowMr[i].Ilosc = listaDokumentow[i].pozycje[j].ilosc;
                            listaDokumentowMr[i].Jm = listaDokumentow[i].pozycje[j].jednostkaMiary;
                            listaDokumentowMr[i].Pobranie = Localization.RemovePrefix(listaDokumentow[i].pozycje[j].lokalizacja);
                            listaDokumentowMr[i].WagaJM = listaDokumentow[i].pozycje[j].wagaJm;
                            listaDokumentowMr[i].SymbolTowaru = listaDokumentow[i].pozycje[j].symbolTowaru;
                            listaDokumentowMr[i].IdPartii = listaDokumentow[i].pozycje[j].idPartii;
                            listaDokumentowMr[i].Abc = listaDokumentow[i].pozycje[j].abc;
                            listaDokumentowMr[i].PozycjaPPZakonczona = listaDokumentow[i].pozycje[j].zakonczona;
                            listaDokumentowMr[i].NosnId = listaDokumentow[i].pozycje[j].nosnId;
                        }
                        else
                        {
                            umieszczenie = new Umieszczenie
                                               {
                                                   Id = listaDokumentow[i].pozycje[j].id,
                                                   Ilosc = listaDokumentow[i].pozycje[j].ilosc,
                                                   Lokalizacja =
                                                       Localization.RemovePrefix(
                                                       listaDokumentow[i].pozycje[j].lokalizacja),
                                                   Max = listaDokumentow[i].pozycje[j].iloscMaksymalnaWLokalizacji,
                                                   IdPartii = listaDokumentow[i].pozycje[j].idPartii,
                                                   SymbolTowaru = listaDokumentow[i].pozycje[j].symbolTowaru,
                                                   IdDokumentu = listaDokumentow[i].pozycje[j].idDokumentu,
                                                   JednostkaMiary = listaDokumentow[i].pozycje[j].jednostkaMiary,
                                                   NosnId = listaDokumentow[i].pozycje[j].nosnId
                                               };
                            listaDokumentowMr[i].Umieszczenie.Add(umieszczenie.Id, umieszczenie);
                        }
                    }
                }
            }
            else
                listaDokumentowMr = new DokumentMR[0];
            return listaDokumentowMr;
        }

        private static Pozycja[] KonwertujNaPozycje(IList<Umieszczenie> umieszczenia)
        {
            var pozycje = new Pozycja[umieszczenia.Count];
            for (var i = 0; i < umieszczenia.Count; i++)
            {
                pozycje[i] = new Pozycja
                                 {
                                     id = umieszczenia[i].Id,
                                     idPartii = umieszczenia[i].IdPartii,
                                     ilosc = umieszczenia[i].Ilosc,
                                     iloscWLokalizacji = umieszczenia[i].IloscWLokalizacji.HasValue ? umieszczenia[i].IloscWLokalizacji.Value : 0,
                                     iloscMaksymalnaWLokalizacji = umieszczenia[i].Max,
                                     lokalizacja = Localization.AddPrefix(umieszczenia[i].Lokalizacja)
                                 };
            }
            return pozycje;
        }

        #endregion

        #region Uruchomienie raportu

        public void UruchomRaport(string nazwaFormularza, long idDokumentu, long idPozycji)
        {
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new WsPrzesuniecieWrap.uruchomRaportWrap { nazwaFormularza = nazwaFormularza, idDokumentuMR = idDokumentu, idPozycji = idPozycji };

                    var statusOperacji = serviceAgent.uruchomRaport(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = statusOperacji.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        #endregion

        #region MoznaJednoznacznieOkreslicNosnik

        public bool MoznaJednoznacznieOkreslicNosnik(long? painId, string lokalizacja, float? ilosc)
        {
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new moznaJednoznacznieOkreslicNosnik { painId = painId, lokalizacja = lokalizacja, ilosc = ilosc };

                    var moznaOkreslicNsnik = serviceAgent.moznaJednoznacznieOkreslicNosnik(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = moznaOkreslicNsnik.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = moznaOkreslicNsnik.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = moznaOkreslicNsnik.statusOperacji.stosWywolan;

                    if ((moznaOkreslicNsnik.wartosc != null) && (moznaOkreslicNsnik.wartosc.Equals(BoolConstants.TAK)))
                        return true;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }

            return false;
        }

        #endregion

        #region PobierzListeDostepnychNosnikow

        public List<DataModel.Nosnik> PobierzListeDostepnychNosnikow(long? painId, string kodLokalizacji, float? ilosc)
        {
            try
            {
                using (var serviceAgent = new WsPrzesuniecieWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzListeDostepnychNosnDlaPainWMijs { painId = painId, ilosc = ilosc, lokalizacja = kodLokalizacji };

                    var listaNosnikowWs = serviceAgent.pobierzListeDostepnychNosnDlaPainWMijs(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = listaNosnikowWs.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = listaNosnikowWs.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = listaNosnikowWs.statusOperacji.stosWywolan;

                    var listaNosnikow = new List<DataModel.Nosnik>();

                    if (listaNosnikowWs.lista != null)
                    {
                        foreach (var nosnik in listaNosnikowWs.lista)
                        {
                            listaNosnikow.Add(new DataModel.Nosnik(nosnik.id, nosnik.symbol));
                        }
                    }

                    return listaNosnikow;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }

            return null;
        }

        #endregion

        #region Operacje asynchroniczne

        public void PobierzAsynchronicznieStanyGorace(SortedList<long, DaneOperacjiAsync> lista)
        {
            if (lista == null ||
                lista.Count == 0)
            {
                return;
            }
            try
            {
                var serviceAgent = GetAsyncServiceAgentInstance();
                DaneOperacjiAsync dane;
                var listaParametrow = new List<WsPrzesuniecieWrap.pobierzStanGoracyWrap>();
                WsPrzesuniecieWrap.pobierzStanGoracyWrap param;
                foreach (var key in lista.Keys)
                {
                    param = new WsPrzesuniecieWrap.pobierzStanGoracyWrap { identyfikatorZadania = key };
                    dane = lista[key];
                    param.lokalizacja = Localization.AddPrefix(dane.Lokalizacja);
                    param.painId = dane.PainId;
                    param.nosnId = dane.NosnId;
                    listaParametrow.Add(param);
                }
                foreach (var parametry in listaParametrow)
                {
                    PobranoStanGoracy(serviceAgent.pobierzStanGoracy(parametry).@return);
                }
            }
            catch (ThreadAbortException)
            {
                OnOdczytanoStanGoracy();
            }
            catch (Exception)
            {
                //TODO: log wyjatkow
                MyWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
            }
        }

        public void PobierzAsynchronicznieIlosciWLok(SortedList<long, DaneOperacjiAsync> lista)
        {
            if (lista == null ||
                lista.Count == 0)
            {
                return;
            }
            try
            {
                var serviceAgent = GetAsyncServiceAgentInstance();
                DaneOperacjiAsync dane;
                var listaParametrow = new List<WsPrzesuniecieWrap.pobierzIloscWLokalizacjiWrap>();
                WsPrzesuniecieWrap.pobierzIloscWLokalizacjiWrap param;
                foreach (var key in lista.Keys)
                {
                    param = new WsPrzesuniecieWrap.pobierzIloscWLokalizacjiWrap { identyfikatorZadania = key };
                    dane = lista[key];
                    param.lokalizacja = Localization.AddPrefix(dane.Lokalizacja);
                    param.painId = dane.PainId;
                    param.nosnId = dane.NosnId;
                    listaParametrow.Add(param);
                }
                foreach (var parametry in listaParametrow)
                {
                    PobranoIloscWLok(serviceAgent.pobierzIloscWLokalizacji(parametry).@return);
                }
            }
            catch (ThreadAbortException)
            {
                OnOdczytanoIloscWLok();
            }
            catch (Exception)
            {
                //TODO: log wyjatkow
                MyWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
            }
        }

        public void AnulujOczekujaceOperacje()
        {
            if (asyncServiceAgent != null)
            {
                DisposeAsyncServiceAgent();
            }
        }

        private void PobranoStanGoracy(StanGoracy stanGoracy)
        {
            if (stanGoracy != null)
            {
                try
                {
                    var listaOczekujacych = MyWorkItem.RootWorkItem.State[StateConstants.ASYNC_OP_STANY_REMAINING] as SortedList<long, DaneOperacjiAsync>;

                    //jezeli nie anulowano w miedzyczasie
                    if (listaOczekujacych.ContainsKey(stanGoracy.identyfikatorZadania))
                    {
                        var dane = listaOczekujacych[stanGoracy.identyfikatorZadania];
                        dane.ZwroconaWartosc = stanGoracy.stanGoracy;
                        lock (listaOczekujacych)
                        {
                            //jezeli nie anulowano w miedzyczasie
                            if (listaOczekujacych.ContainsKey(stanGoracy.identyfikatorZadania))
                            {
                                listaOczekujacych.Remove(stanGoracy.identyfikatorZadania);
                            }
                        }
                        var listaWykonanych = MyWorkItem.RootWorkItem.State[StateConstants.ASYNC_OP_STANY_RESULTS] as SortedList<long, DaneOperacjiAsync>;
                        lock (listaWykonanych)
                        {
                            listaWykonanych.Add(stanGoracy.identyfikatorZadania, dane);
                        }
                        OnOdczytanoStanGoracy();
                    }
                    DisposeAsyncServiceAgent();
                }
                catch (Exception)
                {
                    //TODO: log wyjatkow
                    MyWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                }
            }
        }

        private void PobranoIloscWLok(IloscWLokalizacji iloscWLok)
        {
            if (iloscWLok != null)
            {
                try
                {
                    var listaOczekujacych = MyWorkItem.RootWorkItem.State[StateConstants.ASYNC_OP_ILOSC_REMAINING] as SortedList<long, DaneOperacjiAsync>;

                    //jezeli nie anulowano w miedzyczasie
                    if (listaOczekujacych.ContainsKey(iloscWLok.identyfikatorZadania))
                    {
                        var dane = listaOczekujacych[iloscWLok.identyfikatorZadania];
                        dane.ZwroconaWartosc = iloscWLok.ilosc;
                        lock (listaOczekujacych)
                        {
                            //jezeli nie anulowano w miedzyczasie
                            if (listaOczekujacych.ContainsKey(iloscWLok.identyfikatorZadania))
                            {
                                listaOczekujacych.Remove(iloscWLok.identyfikatorZadania);
                            }
                        }
                        var listaWykonanych = MyWorkItem.RootWorkItem.State[StateConstants.ASYNC_OP_ILOSC_RESULTS] as SortedList<long, DaneOperacjiAsync>;
                        lock (listaWykonanych)
                        {
                            listaWykonanych.Add(iloscWLok.identyfikatorZadania, dane);
                        }
                        OnOdczytanoIloscWLok();
                    }
                    DisposeAsyncServiceAgent();
                }
                catch (Exception)
                {
                    //TODO: log wyjatkow
                    MyWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                }
            }
        }

        protected void OnOdczytanoStanGoracy()
        {
            if (OdczytanoStanGoracy != null)
            {
                OdczytanoStanGoracy(this, EventArgs.Empty);
            }
        }

        protected void OnOdczytanoIloscWLok()
        {
            if (OdczytanoIloscWLokalizacji != null)
            {
                OdczytanoIloscWLokalizacji(this, EventArgs.Empty);
            }
        }

        private WsPrzesuniecieWrap GetAsyncServiceAgentInstance()
        {
            if (asyncServiceAgent == null)
            {
                asyncServiceAgent = new WsPrzesuniecieWrap(Configuration);
                SecureSoapMessage(asyncServiceAgent);
            }
            return asyncServiceAgent;
        }

        private void DisposeAsyncServiceAgent()
        {
            var listaOczekujacychStany = MyWorkItem.RootWorkItem.State[StateConstants.ASYNC_OP_ILOSC_REMAINING] as SortedList<long, DaneOperacjiAsync>;
            var listaOczekujacychIlosci = MyWorkItem.RootWorkItem.State[StateConstants.ASYNC_OP_STANY_REMAINING] as SortedList<long, DaneOperacjiAsync>;

            if ((listaOczekujacychIlosci == null || listaOczekujacychIlosci.Count == 0) &&
                (listaOczekujacychStany == null || listaOczekujacychStany.Count == 0))
            {
                asyncServiceAgent.Abort();
                asyncServiceAgent.Dispose();
                asyncServiceAgent = null;
            }
        }

        #endregion
    }
}