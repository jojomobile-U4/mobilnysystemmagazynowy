using System.Collections.Generic;
using PrzesuniecieModule.DataModel;
using PrzesuniecieModule.ListaTowarow.Search;
using PrzesuniecieModule.RealizacjaMR.Search;
using Common.DataModel;

namespace PrzesuniecieModule.Services
{
    public interface IPrzesuniecieService
    {
        void UsunZListyTowarow(long iDTowaru);

        void ZaznaczPrzesuniecieTowaru(long ID);

        void ZatwierdzDokumentMR(long idDokumentu, IList<Umieszczenie> umieszczenia);

        void RezerwujDokument(long id);

        bool LokalizacjaJestWolna(string lokalizacja);

        float MaksymalnaIloscDlaLokalizacji(string symbol, string lokalizacja);

        long? UtworzDokumentMR(string symbolTowaru, string lokalizacja, long? nosnId, string jM, int odmiana, float? ilosc);

        long? UtworzDokumentMRWgIdTowaruDoPrzesuniecia(long id, long? nosnId);

        void UruchomRaport(string nazwaFormularza, long idDokumentu, long idPozycji);

        void ZatwierdzPobranie(long id, float ilosc, float wagaJM);

        SortedList<long, Umieszczenie> PobierzListePozycjiUP(long idDokumentu, long idPartii);
        void DodajPozycjeUP(Umieszczenie umieszczenie);
        void ZmienPozycje(Umieszczenie umieszczenie);
        void ZakonczPozycjeUP(long idDokumentu);
        void UsunPozycjeUP(long id);

        DokumentMR[] PobierzListeDokumentow(KryteriaZapytaniaRealizacjiMR kryteria, int? numerStrony);

        DataModel.ListaTowarow PobierzListeTowarow(KryteriaZapytaniaListyTowarow kryteria, int? numerStrony);
        DataModel.ListaTowarow PobierzListeTowarowDlaLokalizacji(string lokalizacja);

        void PobierzAsynchronicznieStanyGorace(SortedList<long, DaneOperacjiAsync> lista);
        void PobierzAsynchronicznieIlosciWLok(SortedList<long, DaneOperacjiAsync> lista);
        void AnulujOczekujaceOperacje();

        bool MoznaJednoznacznieOkreslicNosnik(long? painId, string lokalizacja, float? ilosc);
        List<Nosnik> PobierzListeDostepnychNosnikow(long? painId, string kodLokalizacji, float? ilosc);

    }
}
