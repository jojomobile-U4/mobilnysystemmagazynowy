#region

using System;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using WydanieSWAModule.DataModel;
using WydanieSWAModule.ListaSWA.KodKreskowyMR;
using WydanieSWAModule.ListaSWA.Search;
using WydanieSWAModule.RealizacjaSWA;
using WydanieSWAModule.Services;

#endregion

namespace WydanieSWAModule.ListaSWA
{
    public class ListaSWAWorkItem : WorkItemBase
    {
        #region Constants

        private const string BRAK_DOKUMENTU = "Nie uda�o si� odnale�� dokumentu, do kt�rego nale�y pozycja o zaczytanym identyfikatorze.";
        private const string DOKUMENT_ZAREZERWOWANY = "Nie mo�na pobra� do edycji dokument�w ze statusem [R]ezerwacja.";
        private const string INFORMACJA = "Informacja";
        private const string WZOR_OPISU_BLEDU = "{0}\nNumer b��du: {1}";
        private const string ZAREZERWOWANY = "R";
        private const string ZATWIERDZONY = "Z";

        #endregion

        #region Private Fields

        private bool m_OdczytKodowMR;

        #endregion

        [EventSubscription(EventBrokerConstants.WYSZUKAJ_DOKUMENTY_SWA)]
        public void OdswiezListeDokumentowSWA(object sender, EventArgs e)
        {
            var listaSWAViewPresenter = Items.Get<ListaSWAViewPresenter>(ItemsConstants.LISTA_SWA_PRESENTER);
            //pobieramy zawsze pierwsza strone

            listaSWAViewPresenter.ResetujStronicowanie();
            listaSWAViewPresenter.ZaladujDaneDoWidoku(PobierzListeSWA(null));
            MainWorkspace.Show(listaSWAViewPresenter.View);
        }

        public ListaNaglowkow PobierzListeSWA(int? numerStrony)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                State[StateConstants.LAST_PAGE] = numerStrony;
                var kryteria = State[StateConstants.CRITERIAS] as KryteriaZapytaniaListySWA;

                var lista = Services.Get<IWydanieSWAService>(true).PobierzListeNaglowkowSWA(kryteria, numerStrony);
                if (lista.StatusOperacji.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(lista.StatusOperacji.Tekst, BLAD, lista.StatusOperacji.StosWywolan);
                }

                return lista;
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void Szukaj()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var searchPresenter = Items.Get<ListaSWASearchViewPresenter>(ItemsConstants.LISTA_SWA_SEARCH_PRESENTER);
                //czyszczenie zapamietanego stanu
                searchPresenter.View.Kryteria = null;

                Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                MainWorkspace.Show(searchPresenter.View);
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void KodKreskowyMR()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                m_OdczytKodowMR = true;
                var kodKreskowyMRPresenter = Items.Get<KodKreskowyMRViewPresenter>(ItemsConstants.LISTA_SWA_KOD_KRESKOWY_MR_PRESENTER);
                kodKreskowyMRPresenter.View.KodKreskowy = string.Empty;

                Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                MainWorkspace.Show(kodKreskowyMRPresenter.View);
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void AnulujKodKreskowyMR()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                m_OdczytKodowMR = false;
                var kodKreskowyMRPresenter = Items.Get<KodKreskowyMRViewPresenter>(ItemsConstants.LISTA_SWA_KOD_KRESKOWY_MR_PRESENTER);
                CloseView(kodKreskowyMRPresenter.View);
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void RezerwujIPobierzDokument(Naglowek naglowek)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                if (naglowek.Status.Equals(ZAREZERWOWANY))
                {
                    ShowMessageBox(DOKUMENT_ZAREZERWOWANY, INFORMACJA);
                }
                else
                {
                    if (RezerwujDokument(naglowek))
                    {
                        RootWorkItem.WorkItems.Get<RealizacjaSWAWorkItem>(WorkItemsConstants.REALIZACJA_SWA_WORKITEM).Show(MainWorkspace, naglowek.Id);
                    }
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void RezerwujIPobierzDokument(string wartoscIdentyfikujacaMR)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var naglowek = PobierzNaglowekSWA(wartoscIdentyfikujacaMR);
                if (naglowek != null)
                {
                    if (RezerwujDokument(naglowek))
                    {
                        RealizujDokument(naglowek, wartoscIdentyfikujacaMR);
                    }
                    else if (m_OdczytKodowMR)
                    {
                        //wyswietlenie wczytanej wartosci
                        Items.Get<KodKreskowyMRViewPresenter>(ItemsConstants.LISTA_SWA_KOD_KRESKOWY_MR_PRESENTER).View.KodKreskowy = wartoscIdentyfikujacaMR;
                    }
                }
                else if (m_OdczytKodowMR)
                {
                    //wyswietlenie wczytanej wartosci
                    Items.Get<KodKreskowyMRViewPresenter>(ItemsConstants.LISTA_SWA_KOD_KRESKOWY_MR_PRESENTER).View.KodKreskowy = wartoscIdentyfikujacaMR;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ)]
        public void BarCodeRead(object sender, EventArgs e)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var listaSwaViewPresenter = Items.Get<ListaSWAViewPresenter>(ItemsConstants.LISTA_SWA_PRESENTER);

                var kodKreskowyMRPresenter = Items.Get<KodKreskowyMRViewPresenter>(ItemsConstants.LISTA_SWA_KOD_KRESKOWY_MR_PRESENTER);
                
                if (RootWorkItem.State[StateConstants.BAR_CODE] != null
                    && RootWorkItem.State[StateConstants.BAR_CODE_TYPE] != null && listaSwaViewPresenter!= null 
                    && (MainWorkspace.ActiveSmartPart == listaSwaViewPresenter.View
                    || MainWorkspace.ActiveSmartPart == kodKreskowyMRPresenter.View))
                {
                    var barCode = RootWorkItem.State[StateConstants.BAR_CODE] as string;
                    RootWorkItem.State[StateConstants.BAR_CODE] = null;
                    RezerwujIPobierzDokument(barCode);
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void Show(IWorkspace parentWorkspace)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var listaSWAViewPresenter = Items.Get<ListaSWAViewPresenter>(ItemsConstants.LISTA_SWA_PRESENTER);

                listaSWAViewPresenter.ZaladujDaneDoWidoku(PobierzListeSWA(null));
                parentWorkspace.Show(listaSWAViewPresenter.View);

                m_OdczytKodowMR = false;
                this.Activate();
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        /// <summary>
        /// Realizacja dokumentu SWA wg id pozycji dokumentu SWA (id dokumentu MR) lub id pozycji dokumentuMR
        /// </summary>
        /// <param name="naglowek"></param>
        /// <param name="idPozycji"></param>
        private void RealizujDokument(Naglowek naglowek, string symbolDokumentuMR)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                if (!m_OdczytKodowMR)
                {
                    //udalo sie zarezerwowac dokument - przejscie do edycji pozycji dokumentu
                    //realizacja dokumentu SWA - wyszukanie dokumentu SWA wg ID pozycji dokumentu MR
                    RootWorkItem.WorkItems.Get<RealizacjaSWAWorkItem>(WorkItemsConstants.REALIZACJA_SWA_WORKITEM).Show(MainWorkspace, naglowek.Id, symbolDokumentuMR);
                }
                else
                {
                    //udalo sie zarezerwowac dokument - przejscie do edycji dokumentu
                    //realizacja dokumentu SWA - wyszukanie dokumentu SWA wg ID dokumentu MR
                    AnulujKodKreskowyMR();
                    RootWorkItem.WorkItems.Get<RealizacjaSWAWorkItem>(WorkItemsConstants.REALIZACJA_SWA_WORKITEM).Show(MainWorkspace, naglowek.Id);
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        private bool RezerwujDokument(Naglowek naglowek)
        {
            //proba zarezerwowania dokumentu
            var statusOperacji = Services.Get<IWydanieSWAService>(true).RezerwujDokument(naglowek.Id);
            //odswiezenie listy - zmiana statusu dokumentow z listy
            OdswiezListeDokumentow();
            if (statusOperacji.Status.Equals(StatusOperacji.ERROR))
            {
                //dokument jest zarezerowowany przez inna osobe lub zakonczony 
                //- zmiana statusu nastapila po pobraniu listy przez klienta
                ShowMessageBox(statusOperacji.Tekst, INFORMACJA);
                return false;
            }
            else
            {
                return true;
            }
        }

        private void OdswiezListeDokumentow()
        {
            var ostatnioLadowanaStrona = (int?) State[StateConstants.LAST_PAGE];
            if (ostatnioLadowanaStrona != null)
            {
                var listaSWAViewPresenter = Items.Get<ListaSWAViewPresenter>(ItemsConstants.LISTA_SWA_PRESENTER);
                listaSWAViewPresenter.ZaladujDaneDoWidoku(PobierzListeSWA(ostatnioLadowanaStrona));
            }
        }

        private Naglowek PobierzNaglowekSWA(string wartoscIdentyfikujacaMR)
        {
            Naglowek naglowek = null;
            long idPozycji;

            if (m_OdczytKodowMR)
            {
/*				long? id = Konwertuj(wartoscIdentyfikujacaMR);
				if (id == null)
				{
					return null;
				}

				long idPozycji = (long)id;
				//wyszukanie dokumentu, do ktorego nalezy pozycja o podanym id
				naglowek = Services.Get<IWydanieSWAService>(true).PobierzNaglowekSWA(idPozycji);*/

                try
                {
                    idPozycji = long.Parse(wartoscIdentyfikujacaMR);
                    naglowek = Services.Get<IWydanieSWAService>(true).PobierzNaglowekSWA(idPozycji);
                }
                catch (FormatException)
                {
                    naglowek = Services.Get<IWydanieSWAService>(true).PobierzNaglowekSWA(wartoscIdentyfikujacaMR);
                }
            }
            else
            {
                naglowek = Services.Get<IWydanieSWAService>(true).PobierzNaglowekSWA(wartoscIdentyfikujacaMR);
            }

            if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                //jezeli wystapil blad
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, BLAD, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
            }
            else if (naglowek == null)
            {
                //nie znaleziono dokumentu zawierajacego pozycje o podanym id
                ShowMessageBox(BRAK_DOKUMENTU, BLAD);
            }

            return naglowek;
        }
    }
}