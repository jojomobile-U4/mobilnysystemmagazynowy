#region

using Common.SearchForm;

#endregion

namespace WydanieSWAModule.ListaSWA.Search
{
    public interface IListaSWASearchView : ISearchForm
    {
        KryteriaZapytaniaListySWA Kryteria { get; set; }
    }
}