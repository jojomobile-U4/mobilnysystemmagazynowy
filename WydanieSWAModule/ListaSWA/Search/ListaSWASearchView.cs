#region

using Common.SearchForm;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace WydanieSWAModule.ListaSWA.Search
{
    [SmartPart]
    public partial class ListaSWASearchView : SearchForm, IListaSWASearchView
    {
        #region Constructors

        public ListaSWASearchView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #endregion

        #region IListaSWASearchView Members

        public KryteriaZapytaniaListySWA Kryteria
        {
            get
            {
                return new KryteriaZapytaniaListySWA(
                    tbDokument.Text,
                    tbStatus.Text,
                    tbData.Value,
                    tbPriorytet.Text,
                    tbSymbolOdbiorcy.Text,
                    tbWskaznik.Text);
            }
            set
            {
                if (value != null)
                {
                    tbDokument.Text = value.SymbolDokumentu;
                    tbStatus.Text = value.Status;
                    tbWskaznik.Text = value.Wskaznik;
                    tbPriorytet.Text = value.Priorytet;
                    tbData.Value = value.Data;
                    tbSymbolOdbiorcy.Text = value.SymbolOdbiorcy;
                }
                else
                {
                    tbDokument.Text =
                        tbStatus.Text =
                        tbWskaznik.Text =
                        tbPriorytet.Text =
                        tbData.Text =
                        tbSymbolOdbiorcy.Text = string.Empty;
                }
            }
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);

            tbData.ReadOnly = navigationState;
            tbDokument.ReadOnly = navigationState;
            tbPriorytet.ReadOnly = navigationState;
            tbStatus.ReadOnly = navigationState;
            tbSymbolOdbiorcy.ReadOnly = navigationState;
            tbWskaznik.ReadOnly = navigationState;
        }

        #endregion
    }
}