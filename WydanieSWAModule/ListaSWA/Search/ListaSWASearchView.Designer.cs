
using Common.Components;

namespace WydanieSWAModule.ListaSWA.Search
{
	partial class ListaSWASearchView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tbStatus = new System.Windows.Forms.TextBox();
			this.lbStatus = new System.Windows.Forms.Label();
			this.tbWskaznik = new System.Windows.Forms.TextBox();
			this.lbWskaznik = new System.Windows.Forms.Label();
			this.tbPriorytet = new System.Windows.Forms.TextBox();
			this.lbPriorytet = new System.Windows.Forms.Label();
			this.tbData = new Common.Components.MSMDate();
			this.lbData = new System.Windows.Forms.Label();
			this.tbSymbolOdbiorcy = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.lbTytul = new Common.Components.MSMLabel();
			this.tbDokument = new System.Windows.Forms.TextBox();
			this.lbSymbol = new System.Windows.Forms.Label();
			this.pnlNavigation.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnAnuluj
			// 
			this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			// 
			// btnOstatnieZapytanie
			// 
			this.btnOstatnieZapytanie.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			// 
			// btnOK
			// 
			this.btnOK.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			// 
			// tbStatus
			// 
			this.tbStatus.Location = new System.Drawing.Point(73, 52);
			this.tbStatus.Name = "tbStatus";
			this.tbStatus.Size = new System.Drawing.Size(163, 21);
			this.tbStatus.TabIndex = 1;
			// 
			// lbStatus
			// 
			this.lbStatus.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
			this.lbStatus.Location = new System.Drawing.Point(3, 54);
			this.lbStatus.Name = "lbStatus";
			this.lbStatus.Size = new System.Drawing.Size(71, 21);
			this.lbStatus.Text = "Status:";
			// 
			// tbWskaznik
			// 
			this.tbWskaznik.Location = new System.Drawing.Point(73, 100);
			this.tbWskaznik.Name = "tbWskaznik";
			this.tbWskaznik.Size = new System.Drawing.Size(163, 21);
			this.tbWskaznik.TabIndex = 3;
			// 
			// lbWskaznik
			// 
			this.lbWskaznik.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
			this.lbWskaznik.Location = new System.Drawing.Point(3, 102);
			this.lbWskaznik.Name = "lbWskaznik";
			this.lbWskaznik.Size = new System.Drawing.Size(78, 21);
			this.lbWskaznik.Text = "Wska�nik:";
			// 
			// tbPriorytet
			// 
			this.tbPriorytet.Location = new System.Drawing.Point(73, 76);
			this.tbPriorytet.Name = "tbPriorytet";
			this.tbPriorytet.Size = new System.Drawing.Size(163, 21);
			this.tbPriorytet.TabIndex = 2;
			// 
			// lbPriorytet
			// 
			this.lbPriorytet.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
			this.lbPriorytet.Location = new System.Drawing.Point(3, 77);
			this.lbPriorytet.Name = "lbPriorytet";
			this.lbPriorytet.Size = new System.Drawing.Size(71, 21);
			this.lbPriorytet.Text = "Priorytet:";
			// 
			// tbData
			// 
			this.tbData.Location = new System.Drawing.Point(73, 148);
			this.tbData.Name = "tbData";
			this.tbData.Size = new System.Drawing.Size(163, 21);
			this.tbData.TabIndex = 5;
			this.tbData.Value = null;
			// 
			// lbData
			// 
			this.lbData.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
			this.lbData.Location = new System.Drawing.Point(3, 150);
			this.lbData.Name = "lbData";
			this.lbData.Size = new System.Drawing.Size(69, 33);
			this.lbData.Text = "Data: (dd.mm.rrrr)";
			// 
			// tbSymbolOdbiorcy
			// 
			this.tbSymbolOdbiorcy.Location = new System.Drawing.Point(73, 124);
			this.tbSymbolOdbiorcy.Name = "tbSymbolOdbiorcy";
			this.tbSymbolOdbiorcy.Size = new System.Drawing.Size(163, 21);
			this.tbSymbolOdbiorcy.TabIndex = 4;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
			this.label1.Location = new System.Drawing.Point(3, 120);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(78, 31);
			this.label1.Text = "Symbol odbiorcy:";
			// 
			// lbTytul
			// 
			this.lbTytul.BackColor = System.Drawing.Color.Empty;
			this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
			this.lbTytul.CenterAlignX = false;
			this.lbTytul.CenterAlignY = true;
			this.lbTytul.ColorText = System.Drawing.Color.White;
			this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
			this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.lbTytul.ForeColor = System.Drawing.Color.Empty;
			this.lbTytul.Location = new System.Drawing.Point(0, 0);
			this.lbTytul.Name = "lbTytul";
			this.lbTytul.RectangleColor = System.Drawing.Color.Black;
			this.lbTytul.Size = new System.Drawing.Size(240, 16);
			this.lbTytul.TabIndex = 10;
			this.lbTytul.TabStop = false;
			this.lbTytul.TextDisplayed = "WYSZUKIWANIE DOKUMENT�W SWA";
			// 
			// tbDokument
			// 
			this.tbDokument.Location = new System.Drawing.Point(73, 28);
			this.tbDokument.Name = "tbDokument";
			this.tbDokument.Size = new System.Drawing.Size(163, 21);
			this.tbDokument.TabIndex = 0;
			// 
			// lbSymbol
			// 
			this.lbSymbol.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
			this.lbSymbol.Location = new System.Drawing.Point(3, 31);
			this.lbSymbol.Name = "lbSymbol";
			this.lbSymbol.Size = new System.Drawing.Size(71, 21);
			this.lbSymbol.Text = "Dokument:";
			// 
			// ListaSWASearchView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.AutoScroll = true;
			this.Controls.Add(this.tbStatus);
			this.Controls.Add(this.lbStatus);
			this.Controls.Add(this.tbWskaznik);
			this.Controls.Add(this.lbWskaznik);
			this.Controls.Add(this.tbPriorytet);
			this.Controls.Add(this.lbPriorytet);
			this.Controls.Add(this.tbData);
			this.Controls.Add(this.lbData);
			this.Controls.Add(this.tbSymbolOdbiorcy);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.lbTytul);
			this.Controls.Add(this.tbDokument);
			this.Controls.Add(this.lbSymbol);
			this.Name = "ListaSWASearchView";
			this.Controls.SetChildIndex(this.pnlNavigation, 0);
			this.Controls.SetChildIndex(this.lbSymbol, 0);
			this.Controls.SetChildIndex(this.tbDokument, 0);
			this.Controls.SetChildIndex(this.lbTytul, 0);
			this.Controls.SetChildIndex(this.label1, 0);
			this.Controls.SetChildIndex(this.tbSymbolOdbiorcy, 0);
			this.Controls.SetChildIndex(this.lbData, 0);
			this.Controls.SetChildIndex(this.tbData, 0);
			this.Controls.SetChildIndex(this.lbPriorytet, 0);
			this.Controls.SetChildIndex(this.tbPriorytet, 0);
			this.Controls.SetChildIndex(this.lbWskaznik, 0);
			this.Controls.SetChildIndex(this.tbWskaznik, 0);
			this.Controls.SetChildIndex(this.lbStatus, 0);
			this.Controls.SetChildIndex(this.tbStatus, 0);
			this.pnlNavigation.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TextBox tbStatus;
		private System.Windows.Forms.Label lbStatus;
		private System.Windows.Forms.TextBox tbWskaznik;
		private System.Windows.Forms.Label lbWskaznik;
		private System.Windows.Forms.TextBox tbPriorytet;
		private System.Windows.Forms.Label lbPriorytet;
		private MSMDate tbData;
		private System.Windows.Forms.Label lbData;
		private System.Windows.Forms.TextBox tbSymbolOdbiorcy;
		private System.Windows.Forms.Label label1;
		private MSMLabel lbTytul;
		private System.Windows.Forms.TextBox tbDokument;
		private System.Windows.Forms.Label lbSymbol;
	}
}
