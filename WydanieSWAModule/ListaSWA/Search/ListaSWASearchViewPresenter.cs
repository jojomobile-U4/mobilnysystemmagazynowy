using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using Common.SearchForm;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Common;

namespace WydanieSWAModule.ListaSWA.Search
{
	public class ListaSWASearchViewPresenter: SearchFormPresenter
	{
		#region Events

		[EventPublication(EventBrokerConstants.WYSZUKAJ_DOKUMENTY_SWA)]
		public event EventHandler WyszukajDokumentySWA;

		#endregion
		#region Properties

		public IListaSWASearchView View
		{
			get { return m_view as IListaSWASearchView; }
		}

		#endregion
		#region Constructors

		public ListaSWASearchViewPresenter(IListaSWASearchView view)
			: base(view)
		{

		}

		#endregion
		#region Overrided Memebers

		protected override void OnViewSzukaj(object sender, EventArgs e)
		{
			base.OnViewSzukaj(sender, e);
			OnWyszukajDokumentySWA();
		}

		protected override void OnViewUstawOstatnieZapytanie(object sender, EventArgs e)
		{
			base.OnViewUstawOstatnieZapytanie(sender, e);
			View.Kryteria = State[StateConstants.CRITERIAS] as KryteriaZapytaniaListySWA ?? new KryteriaZapytaniaListySWA();
		}

		#endregion
		#region Raising Events

		protected void OnWyszukajDokumentySWA()
		{
			State[StateConstants.CRITERIAS] = View.Kryteria;
			if (WyszukajDokumentySWA != null)
			{
				WyszukajDokumentySWA(this, EventArgs.Empty);
			}
		}

		#endregion
	}
}
