using System;
using System.Collections.Generic;
using System.Text;

namespace WydanieSWAModule.ListaSWA.Search
{
	public class KryteriaZapytaniaListySWA
	{
		#region Private Fields

		private string m_SymbolDokumentu;
		private string m_Status;
		private DateTime? m_Data;
		private string m_Priorytet;
		private string m_SymbolOdbiorcy;
		private string m_Wskaznik;

		#endregion
		#region Properties

		public string Wskaznik
		{
			get { return m_Wskaznik; }
			set { m_Wskaznik = value; }
		}

		public string SymbolOdbiorcy
		{
			get { return m_SymbolOdbiorcy; }
			set { m_SymbolOdbiorcy = value; }
		}

		public string Priorytet
		{
			get { return m_Priorytet; }
			set { m_Priorytet = value; }
		}

		public DateTime? Data
		{
			get { return m_Data; }
			set { m_Data = value; }
		}

		public string Status
		{
			get { return m_Status; }
			set { m_Status = value; }
		}
		
		public string SymbolDokumentu
		{
			get { return m_SymbolDokumentu; }
			set { m_SymbolDokumentu = value; }
		}

		#endregion
		#region Constructors

		public KryteriaZapytaniaListySWA()
		{

		}

		public KryteriaZapytaniaListySWA(string symbolDokumentu, string status, DateTime? data, string priorytet, string symbolOdbiorcy, string wskaznik)
			: this()
		{
			this.m_SymbolDokumentu = symbolDokumentu;
			this.m_Status = status;
			this.m_Data = data;
			this.m_Priorytet = priorytet;
			this.m_SymbolOdbiorcy = symbolOdbiorcy;
			this.m_Wskaznik = wskaznik;
		}
		#endregion
	}
}
