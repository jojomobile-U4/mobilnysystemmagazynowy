using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using Common.Base;
using System.Windows.Forms;
using WydanieSWAModule.DataModel;
using WydanieSWAModule.Services;
using Common;

namespace WydanieSWAModule.ListaSWA
{
	public class ListaSWAViewPresenter: PresenterBase
	{
		#region Constants

		private const int NASTEPNA = 1;
		private const int POPRZEDNIA = -1;
		private const int PIERWSZA = 0;

		#endregion
		#region Private Fields

		private ListaNaglowkow listaNaglowkow;
		private Naglowek aktualnyNaglowek;
		private int numerStrony;
		private int zmianaStrony;
		private int startoweLp;
		private bool odswiezanieListyDokumentow;

		#endregion
		#region Properties

		public IListaSWAView View
		{
			get { return m_view as IListaSWAView; }
		}

		protected ListaSWAWorkItem MyWorkItem
		{
			get { return WorkItem as ListaSWAWorkItem; }
		}

		#endregion
		#region Constructors

		public ListaSWAViewPresenter(IListaSWAView view):base(view)
		{
			ResetujStronicowanie();
			startoweLp = 1;
			odswiezanieListyDokumentow = false;
		}

		#endregion
		#region Methods

		public void ZaladujDaneDoWidoku(ListaNaglowkow listaNaglowkow)
		{
			if (listaNaglowkow == null ||
				listaNaglowkow.Lista.Count == 0)
			{
				if (listaNaglowkow == null)
				{
					listaNaglowkow = new ListaNaglowkow();
				}
				else
				{
					this.listaNaglowkow = listaNaglowkow;
				}
				aktualnyNaglowek = null;
			}
			else
			{
				this.listaNaglowkow = listaNaglowkow;
				aktualnyNaglowek = this.listaNaglowkow.Lista[0];
			}
			AktualizujWidok();
		}

		internal void ResetujStronicowanie()
		{
			numerStrony = PIERWSZA;
			zmianaStrony = NASTEPNA;
			startoweLp = 1;
		}

		#endregion
		#region Override Members

		protected override void AttachView()
		{
			View.Nastepny += new EventHandler(View_Nastepny);
			View.Pobierz += new EventHandler(View_Pobierz);
			View.Poprzedni += new EventHandler(View_Poprzedni);
			View.Szukaj += new EventHandler(View_Szukaj);
			View.KodKreskowyMR += new EventHandler(View_KodKreskowyMR);
			View.ZaznaczonyDokumentSWAChanged += new EventHandler(View_ZaznaczonyDokumentSWAChanged);
		}

		protected override void HandleNavigationKey(System.Windows.Forms.KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Left:
					e.Handled = true;
					View_Poprzedni(this, EventArgs.Empty);
					break;
				case Keys.Right:
					e.Handled = true;
					View_Nastepny(this, EventArgs.Empty);
					break;
				case Keys.D1:
					e.Handled = true;
					View_Szukaj(this, EventArgs.Empty);
					break;
				case Keys.Enter:
					e.Handled = true;
					View_Pobierz(this, EventArgs.Empty);
					break;
				case Keys.D2:
					e.Handled = true;
					View_KodKreskowyMR(this, EventArgs.Empty);
					break;
				case Keys.Escape:
					e.Handled = true;
					CloseView();
					break;
			}
		}

		#endregion
		#region Obsluga widoku

		private void View_ZaznaczonyDokumentSWAChanged(object sender, EventArgs e)
		{
			if (!odswiezanieListyDokumentow)
			{
				aktualnyNaglowek = View.ZaznaczonyDokumentSWA;
				View.DataSource = aktualnyNaglowek;
			}
		}

		private void View_Szukaj(object sender, EventArgs e)
		{
			MyWorkItem.Szukaj();
		}

		private void View_Pobierz(object sender, EventArgs e)
		{
			if (aktualnyNaglowek != null)
			{
				MyWorkItem.RezerwujIPobierzDokument(aktualnyNaglowek);
			}
		}

		private void View_Poprzedni(object sender, EventArgs e)
		{
			if (listaNaglowkow != null)
			{
				//TODO: sprawdzic czy jest poprzednia paczka (strona)

				//jezeli numer strony = 0, znaczy ze nie ma wczesniejszych stron
				if (numerStrony > 0)
				{
					ZmienStrone(POPRZEDNIA);
				}
			}
		}

		private void View_Nastepny(object sender, EventArgs e)
		{
			//sprawdzenie czy nie jestesmy na ostatniej stronie
			//jezeli nie ma indeksow, znaczy ze nie ma dalszych stron
			if (listaNaglowkow != null &&
				listaNaglowkow.Lista.Count == MyWorkItem.Configuration.WydanieSWAWS.WielkoscStrony )
			{
				//TODO: sprawdzic czy jest nastepna paczka (strona)
				ZmienStrone(NASTEPNA);
			}
		}
		
		private void View_KodKreskowyMR(object sender, EventArgs e)
		{
			MyWorkItem.KodKreskowyMR();
		}

		private void AktualizujWidok()
		{
			View.DataSource = aktualnyNaglowek;
			View.IloscLp = listaNaglowkow.LiczbaNaglowkow;
			AktualizujDokumentyMR();
		}

		private void AktualizujDokumentyMR()
		{
			odswiezanieListyDokumentow = true;
			View.DokumentySWADataSource.Clear();

			if (listaNaglowkow != null)
			{
				Naglowek naglowek;
				for (int i = 0; i < listaNaglowkow.Lista.Count; i++)
				{
					naglowek = listaNaglowkow.Lista[i];
					View.DokumentySWADataSource.Add(UtworzWpis(startoweLp + i, naglowek));
					View.DokumentySWADataSource[i].Selected = naglowek.Equals(aktualnyNaglowek);
				}
			}
			odswiezanieListyDokumentow = false;
		}

		private System.Windows.Forms.ListViewItem UtworzWpis(int lp, Naglowek naglowek)
		{
			string[] elementy = new string[] { 
			    lp.ToString(),
			    naglowek.SymbolDokumentu,
			    naglowek.Status,
			    naglowek.Priorytet,
				naglowek.DataWystawienia.ToString(Constants.DATE_FORMAT)};
			System.Windows.Forms.ListViewItem wpis = new System.Windows.Forms.ListViewItem(elementy);
			wpis.Tag = naglowek;

			return wpis;
		}

		private void ZmienStrone(int kierunek)
		{
			numerStrony += kierunek;
			zmianaStrony = kierunek;
			startoweLp += kierunek * MyWorkItem.Configuration.WydanieSWAWS.WielkoscStrony;
			ZaladujDaneDoWidoku(MyWorkItem.PobierzListeSWA(numerStrony));
		}

		#endregion
	}
}
