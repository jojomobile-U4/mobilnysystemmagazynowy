using Common.Components;

namespace WydanieSWAModule.ListaSWA
{
	partial class ListaSWAView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.btnPobierz = new System.Windows.Forms.Button();
            this.btnSzukaj = new System.Windows.Forms.Button();
            this.tbIloscLP = new System.Windows.Forms.TextBox();
            this.lbIloscLP = new System.Windows.Forms.Label();
            this.naglowekSWABindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbNazwaOdbiorcy = new System.Windows.Forms.TextBox();
            this.lbNazwaOdbiorcy = new System.Windows.Forms.Label();
            this.tbSymbolOdbiorcy = new System.Windows.Forms.TextBox();
            this.lbSymbolOdbiorcy = new System.Windows.Forms.Label();
            this.tbWskaznik = new System.Windows.Forms.TextBox();
            this.lbWskaznik = new System.Windows.Forms.Label();
            this.lstDokumentySWA = new Common.Components.MSMListView();
            this.chLp = new System.Windows.Forms.ColumnHeader();
            this.chDokument = new System.Windows.Forms.ColumnHeader();
            this.chStatus = new System.Windows.Forms.ColumnHeader();
            this.chPriorytet = new System.Windows.Forms.ColumnHeader();
            this.chData = new System.Windows.Forms.ColumnHeader();
            this.lbTytul = new Common.Components.MSMLabel();
            this.btnKodKreskowyMR = new System.Windows.Forms.Button();
            this.lrcNavigation = new Common.Components.LeftRightControl();
            this.tbPoleWydania = new System.Windows.Forms.TextBox();
            this.lbPoleWydania = new System.Windows.Forms.Label();
            this.pnlNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.naglowekSWABindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnKodKreskowyMR);
            this.pnlNavigation.Controls.Add(this.btnSzukaj);
            this.pnlNavigation.Controls.Add(this.btnPobierz);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 272);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 28);
            // 
            // btnPobierz
            // 
            this.btnPobierz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPobierz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPobierz.Location = new System.Drawing.Point(157, 4);
            this.btnPobierz.Name = "btnPobierz";
            this.btnPobierz.Size = new System.Drawing.Size(80, 20);
            this.btnPobierz.TabIndex = 7;
            this.btnPobierz.Text = "&Ret Pobierz";
            this.btnPobierz.Click += new System.EventHandler(this.OnPobierz);
            // 
            // btnSzukaj
            // 
            this.btnSzukaj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSzukaj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnSzukaj.Location = new System.Drawing.Point(3, 4);
            this.btnSzukaj.Name = "btnSzukaj";
            this.btnSzukaj.Size = new System.Drawing.Size(74, 20);
            this.btnSzukaj.TabIndex = 5;
            this.btnSzukaj.Text = "&1 Szukaj";
            this.btnSzukaj.Click += new System.EventHandler(this.OnSzukaj);
            // 
            // tbIloscLP
            // 
            this.tbIloscLP.Location = new System.Drawing.Point(56, 210);
            this.tbIloscLP.Name = "tbIloscLP";
            this.tbIloscLP.ReadOnly = true;
            this.tbIloscLP.Size = new System.Drawing.Size(48, 21);
            this.tbIloscLP.TabIndex = 3;
            this.tbIloscLP.TabStop = false;
            // 
            // lbIloscLP
            // 
            this.lbIloscLP.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbIloscLP.Location = new System.Drawing.Point(0, 214);
            this.lbIloscLP.Name = "lbIloscLP";
            this.lbIloscLP.Size = new System.Drawing.Size(55, 21);
            this.lbIloscLP.Text = "Ilo�� LP:";
            // 
            // naglowekSWABindingSource
            // 
            this.naglowekSWABindingSource.DataSource = typeof(WydanieSWAModule.DataModel.Naglowek);
            // 
            // tbNazwaOdbiorcy
            // 
            this.tbNazwaOdbiorcy.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.naglowekSWABindingSource, "NazwaOdbiorcy", true));
            this.tbNazwaOdbiorcy.Location = new System.Drawing.Point(56, 187);
            this.tbNazwaOdbiorcy.Name = "tbNazwaOdbiorcy";
            this.tbNazwaOdbiorcy.ReadOnly = true;
            this.tbNazwaOdbiorcy.Size = new System.Drawing.Size(181, 21);
            this.tbNazwaOdbiorcy.TabIndex = 2;
            this.tbNazwaOdbiorcy.TabStop = false;
            // 
            // lbNazwaOdbiorcy
            // 
            this.lbNazwaOdbiorcy.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbNazwaOdbiorcy.Location = new System.Drawing.Point(0, 183);
            this.lbNazwaOdbiorcy.Name = "lbNazwaOdbiorcy";
            this.lbNazwaOdbiorcy.Size = new System.Drawing.Size(56, 32);
            this.lbNazwaOdbiorcy.Text = "Nazwa odbiorcy:";
            // 
            // tbSymbolOdbiorcy
            // 
            this.tbSymbolOdbiorcy.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.naglowekSWABindingSource, "SymbolOdbiorcy", true));
            this.tbSymbolOdbiorcy.Location = new System.Drawing.Point(56, 164);
            this.tbSymbolOdbiorcy.Name = "tbSymbolOdbiorcy";
            this.tbSymbolOdbiorcy.ReadOnly = true;
            this.tbSymbolOdbiorcy.Size = new System.Drawing.Size(181, 21);
            this.tbSymbolOdbiorcy.TabIndex = 1;
            this.tbSymbolOdbiorcy.TabStop = false;
            // 
            // lbSymbolOdbiorcy
            // 
            this.lbSymbolOdbiorcy.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbolOdbiorcy.Location = new System.Drawing.Point(0, 158);
            this.lbSymbolOdbiorcy.Name = "lbSymbolOdbiorcy";
            this.lbSymbolOdbiorcy.Size = new System.Drawing.Size(56, 32);
            this.lbSymbolOdbiorcy.Text = "Symbol odbiorcy:";
            // 
            // tbWskaznik
            // 
            this.tbWskaznik.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.naglowekSWABindingSource, "Wskaznik", true));
            this.tbWskaznik.Location = new System.Drawing.Point(213, 210);
            this.tbWskaznik.Name = "tbWskaznik";
            this.tbWskaznik.ReadOnly = true;
            this.tbWskaznik.Size = new System.Drawing.Size(24, 21);
            this.tbWskaznik.TabIndex = 4;
            this.tbWskaznik.TabStop = false;
            // 
            // lbWskaznik
            // 
            this.lbWskaznik.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbWskaznik.Location = new System.Drawing.Point(154, 214);
            this.lbWskaznik.Name = "lbWskaznik";
            this.lbWskaznik.Size = new System.Drawing.Size(57, 21);
            this.lbWskaznik.Text = "Wska�nik:";
            // 
            // lstDokumentySWA
            // 
            this.lstDokumentySWA.Columns.Add(this.chLp);
            this.lstDokumentySWA.Columns.Add(this.chDokument);
            this.lstDokumentySWA.Columns.Add(this.chStatus);
            this.lstDokumentySWA.Columns.Add(this.chPriorytet);
            this.lstDokumentySWA.Columns.Add(this.chData);
            this.lstDokumentySWA.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lstDokumentySWA.FullRowSelect = true;
            this.lstDokumentySWA.Location = new System.Drawing.Point(0, 16);
            this.lstDokumentySWA.Name = "lstDokumentySWA";
            this.lstDokumentySWA.Size = new System.Drawing.Size(238, 139);
            this.lstDokumentySWA.TabIndex = 0;
            this.lstDokumentySWA.View = System.Windows.Forms.View.Details;
            this.lstDokumentySWA.SelectedIndexChanged += new System.EventHandler(this.OnZaznaczonyDokumentSWAChanged);
            // 
            // chLp
            // 
            this.chLp.Text = "Lp";
            this.chLp.Width = 25;
            // 
            // chDokument
            // 
            this.chDokument.Text = "Dokument";
            this.chDokument.Width = 95;
            // 
            // chStatus
            // 
            this.chStatus.Text = "S";
            this.chStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.chStatus.Width = 25;
            // 
            // chPriorytet
            // 
            this.chPriorytet.Text = "P";
            this.chPriorytet.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.chPriorytet.Width = 25;
            // 
            // chData
            // 
            this.chData.Text = "Data";
            this.chData.Width = 60;
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 14;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "WYDANIE - LISTA SWA";
            // 
            // btnKodKreskowyMR
            // 
            this.btnKodKreskowyMR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnKodKreskowyMR.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnKodKreskowyMR.Location = new System.Drawing.Point(80, 4);
            this.btnKodKreskowyMR.Name = "btnKodKreskowyMR";
            this.btnKodKreskowyMR.Size = new System.Drawing.Size(75, 20);
            this.btnKodKreskowyMR.TabIndex = 6;
            this.btnKodKreskowyMR.Text = "&2 Kod MR";
            this.btnKodKreskowyMR.Click += new System.EventHandler(this.OnKodKreskowyMR);
            // 
            // lrcNavigation
            // 
            this.lrcNavigation.BackColor = System.Drawing.SystemColors.Desktop;
            this.lrcNavigation.Location = new System.Drawing.Point(208, 0);
            this.lrcNavigation.Name = "lrcNavigation";
            this.lrcNavigation.NextEnabled = true;
            this.lrcNavigation.PreviousEnabled = true;
            this.lrcNavigation.Size = new System.Drawing.Size(32, 16);
            this.lrcNavigation.TabIndex = 1;
            this.lrcNavigation.TabStop = false;
            this.lrcNavigation.Next += new System.EventHandler(this.OnNastepny);
            this.lrcNavigation.Previous += new System.EventHandler(this.OnPoprzedni);
            // 
            // tbPoleWydania
            // 
            this.tbPoleWydania.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.naglowekSWABindingSource, "PoleWydania", true));
            this.tbPoleWydania.Location = new System.Drawing.Point(56, 233);
            this.tbPoleWydania.Name = "tbPoleWydania";
            this.tbPoleWydania.ReadOnly = true;
            this.tbPoleWydania.Size = new System.Drawing.Size(181, 21);
            this.tbPoleWydania.TabIndex = 16;
            this.tbPoleWydania.TabStop = false;
            // 
            // lbPoleWydania
            // 
            this.lbPoleWydania.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbPoleWydania.Location = new System.Drawing.Point(0, 235);
            this.lbPoleWydania.Name = "lbPoleWydania";
            this.lbPoleWydania.Size = new System.Drawing.Size(56, 21);
            this.lbPoleWydania.Text = "Pole wyd.:";
            // 
            // ListaSWAView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lbPoleWydania);
            this.Controls.Add(this.tbPoleWydania);
            this.Controls.Add(this.lrcNavigation);
            this.Controls.Add(this.tbIloscLP);
            this.Controls.Add(this.lbIloscLP);
            this.Controls.Add(this.tbNazwaOdbiorcy);
            this.Controls.Add(this.lbNazwaOdbiorcy);
            this.Controls.Add(this.tbSymbolOdbiorcy);
            this.Controls.Add(this.lbSymbolOdbiorcy);
            this.Controls.Add(this.tbWskaznik);
            this.Controls.Add(this.lbWskaznik);
            this.Controls.Add(this.lstDokumentySWA);
            this.Controls.Add(this.lbTytul);
            this.Name = "ListaSWAView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lstDokumentySWA, 0);
            this.Controls.SetChildIndex(this.lbWskaznik, 0);
            this.Controls.SetChildIndex(this.tbWskaznik, 0);
            this.Controls.SetChildIndex(this.lbSymbolOdbiorcy, 0);
            this.Controls.SetChildIndex(this.tbSymbolOdbiorcy, 0);
            this.Controls.SetChildIndex(this.lbNazwaOdbiorcy, 0);
            this.Controls.SetChildIndex(this.tbNazwaOdbiorcy, 0);
            this.Controls.SetChildIndex(this.lbIloscLP, 0);
            this.Controls.SetChildIndex(this.tbIloscLP, 0);
            this.Controls.SetChildIndex(this.lrcNavigation, 0);
            this.Controls.SetChildIndex(this.tbPoleWydania, 0);
            this.Controls.SetChildIndex(this.lbPoleWydania, 0);
            this.pnlNavigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.naglowekSWABindingSource)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

        private System.Windows.Forms.Button btnPobierz;
		private System.Windows.Forms.Button btnSzukaj;
		private System.Windows.Forms.TextBox tbIloscLP;
		private System.Windows.Forms.Label lbIloscLP;
		private System.Windows.Forms.TextBox tbNazwaOdbiorcy;
		private System.Windows.Forms.Label lbNazwaOdbiorcy;
		private System.Windows.Forms.TextBox tbSymbolOdbiorcy;
		private System.Windows.Forms.Label lbSymbolOdbiorcy;
		private System.Windows.Forms.TextBox tbWskaznik;
		private System.Windows.Forms.Label lbWskaznik;
		private Common.Components.MSMListView lstDokumentySWA;
		private System.Windows.Forms.ColumnHeader chLp;
		private System.Windows.Forms.ColumnHeader chDokument;
		private System.Windows.Forms.ColumnHeader chStatus;
		private System.Windows.Forms.ColumnHeader chPriorytet;
		private MSMLabel lbTytul;
		private System.Windows.Forms.ColumnHeader chData;
		private System.Windows.Forms.Button btnKodKreskowyMR;
		private System.Windows.Forms.BindingSource naglowekSWABindingSource;
		private LeftRightControl lrcNavigation;
        private System.Windows.Forms.TextBox tbPoleWydania;
        private System.Windows.Forms.Label lbPoleWydania;
	}
}
