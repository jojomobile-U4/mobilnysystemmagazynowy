using Common.Base;
using System;
using WydanieSWAModule.DataModel;

namespace WydanieSWAModule.ListaSWA
{
	public interface IListaSWAView: IViewBase
	{
		event EventHandler Szukaj;
		event EventHandler Pobierz;
		event EventHandler Nastepny;
		event EventHandler Poprzedni;
		event EventHandler KodKreskowyMR;
		event EventHandler ZaznaczonyDokumentSWAChanged;

		Naglowek DataSource
		{
			set;
		}

		long IloscLp
		{
			set;
		}

		Common.Components.MSMListView.ListViewItemCollection DokumentySWADataSource
		{
			get;
		}

		bool PoprzedniEnabled
		{
			get;
			set;
		}

		bool NastepnyEnabled
		{
			get;
			set;
		}

		Naglowek ZaznaczonyDokumentSWA
		{
			get;
		}
	}
}
