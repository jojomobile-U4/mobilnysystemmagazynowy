using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using WydanieSWAModule.DataModel;

namespace WydanieSWAModule.ListaSWA
{
	[SmartPart]
	public partial class ListaSWAView : ViewBase, IListaSWAView
	{
		#region Private Fields

		private Naglowek m_Empty = new Naglowek();

		#endregion
		#region Constructors

		public ListaSWAView()
		{
			InitializeComponent();
			InitializeFocusedControl();
		}

		#endregion
		#region Protected Methods

		protected void OnSzukaj(object sender, EventArgs e)
		{
			if (Szukaj != null)
			{
				Szukaj(sender, e);
			}
		}

		protected void OnPobierz(object sender, EventArgs e)
		{
			if (Pobierz != null)
			{
				Pobierz(sender, e);
			}
		}

		protected void OnPoprzedni(object sender, EventArgs e)
		{
			if (Poprzedni != null)
			{
				Poprzedni(sender, e);
			}
		}

		protected void OnNastepny(object sender, EventArgs e)
		{
			if (Nastepny != null)
			{
				Nastepny(sender, e);
			}
		}

		protected void OnKodKreskowyMR(object sender, EventArgs e)
		{
			if (KodKreskowyMR != null)
			{
				KodKreskowyMR(sender, e);
			}
		}

		protected void OnZaznaczonyDokumentSWAChanged(object sender, EventArgs e)
		{
			if (ZaznaczonyDokumentSWAChanged != null)
			{
				ZaznaczonyDokumentSWAChanged(sender, e);
			}
		}

		#endregion
		#region IListaMPView Members

		public event EventHandler Szukaj;

		public event EventHandler Pobierz;

		public event EventHandler Nastepny;

		public event EventHandler Poprzedni;

		public event EventHandler KodKreskowyMR;

		public event EventHandler ZaznaczonyDokumentSWAChanged;

		public Naglowek DataSource
		{
			set
			{
				this.naglowekSWABindingSource.DataSource = value ?? m_Empty;
			}
		}

		public long IloscLp
		{
			set { tbIloscLP.Text = value.ToString(); }
		}

		public bool PoprzedniEnabled
		{
			get { return lrcNavigation.PreviousEnabled; }
			set { lrcNavigation.PreviousEnabled = value; }
		}

		public bool NastepnyEnabled
		{
			get { return lrcNavigation.NextEnabled; }
            set { lrcNavigation.NextEnabled = value; }
		}

		public Common.Components.MSMListView.ListViewItemCollection DokumentySWADataSource
		{
			get { return lstDokumentySWA.Items; }
		}

		public Naglowek ZaznaczonyDokumentSWA
		{
			get
			{
				if (lstDokumentySWA.SelectedIndices.Count == 0)
				{
					return null;
				}

				return lstDokumentySWA.Items[lstDokumentySWA.SelectedIndices[0]].Tag as Naglowek;
			}
		}

		#endregion
	}
}
