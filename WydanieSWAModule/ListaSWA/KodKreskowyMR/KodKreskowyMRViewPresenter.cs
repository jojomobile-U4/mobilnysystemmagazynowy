using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using Common.Base;
using System.Windows.Forms;

namespace WydanieSWAModule.ListaSWA.KodKreskowyMR
{
	public class KodKreskowyMRViewPresenter: PresenterBase
	{
		#region Properties

		public IKodKreskowyMRView View
		{
			get { return m_view as IKodKreskowyMRView; }
		}

		protected ListaSWAWorkItem MyWorkItem
		{
			get { return WorkItem as ListaSWAWorkItem; }
		}

		#endregion
		#region Constructors

		public KodKreskowyMRViewPresenter(IKodKreskowyMRView view): base(view)
		{

		}

		#endregion
		#region Override Members

		protected override void AttachView()
		{
			View.Anuluj += new EventHandler(View_Anuluj);
			View.OK += new EventHandler(View_OK);
		}

		protected override void HandleNavigationKey(System.Windows.Forms.KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Enter:
					e.Handled = true;
					View_OK(this, EventArgs.Empty);
					break;
				case Keys.Escape:
					e.Handled = true;
					View_Anuluj(this, EventArgs.Empty);
					break;
			}
		}

		#endregion
		#region Obsluga widoku

		private void View_OK(object sender, EventArgs e)
		{
			MyWorkItem.RezerwujIPobierzDokument(View.KodKreskowy);
		}

		private void View_Anuluj(object sender, EventArgs e)
		{
			MyWorkItem.AnulujKodKreskowyMR();
		}

		#endregion
	}
}
