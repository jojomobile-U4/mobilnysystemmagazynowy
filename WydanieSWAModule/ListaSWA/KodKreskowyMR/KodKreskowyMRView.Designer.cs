
using Common.Components;

namespace WydanieSWAModule.ListaSWA.KodKreskowyMR
{
	partial class KodKreskowyMRView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.btnAnuluj = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbKod = new System.Windows.Forms.TextBox();
            this.lbKod = new System.Windows.Forms.Label();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnAnuluj);
            this.pnlNavigation.Controls.Add(this.btnOK);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 52);
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnAnuluj.Location = new System.Drawing.Point(163, 30);
            this.btnAnuluj.Name = "btnAnuluj";
            this.btnAnuluj.Size = new System.Drawing.Size(75, 20);
            this.btnAnuluj.TabIndex = 2;
            this.btnAnuluj.Text = "&Esc Anuluj";
            this.btnAnuluj.Click += new System.EventHandler(this.OnAnuluj);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnOK.Location = new System.Drawing.Point(84, 30);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 20);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "&Ret OK";
            this.btnOK.Click += new System.EventHandler(this.OnOK);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 2;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "KOD KRESKOWY MR";
            // 
            // tbKod
            // 
            this.tbKod.Location = new System.Drawing.Point(32, 32);
            this.tbKod.Name = "tbKod";
            this.tbKod.Size = new System.Drawing.Size(203, 21);
            this.tbKod.TabIndex = 0;
            // 
            // lbKod
            // 
            this.lbKod.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbKod.Location = new System.Drawing.Point(2, 33);
            this.lbKod.Name = "lbKod";
            this.lbKod.Size = new System.Drawing.Size(30, 21);
            this.lbKod.Text = "Kod:";
            // 
            // KodKreskowyMRView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.tbKod);
            this.Controls.Add(this.lbKod);
            this.Controls.Add(this.lbTytul);
            this.Name = "KodKreskowyMRView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lbKod, 0);
            this.Controls.SetChildIndex(this.tbKod, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnAnuluj;
		private System.Windows.Forms.Button btnOK;
		private MSMLabel lbTytul;
		private System.Windows.Forms.TextBox tbKod;
		private System.Windows.Forms.Label lbKod;
	}
}
