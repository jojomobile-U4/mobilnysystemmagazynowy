using Common.Base;
using System;

namespace WydanieSWAModule.ListaSWA.KodKreskowyMR
{
	public interface IKodKreskowyMRView: IViewBase
	{
		event EventHandler OK;
		event EventHandler Anuluj;

		string KodKreskowy { set; get; }
	}
}
