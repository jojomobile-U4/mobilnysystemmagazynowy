using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

namespace WydanieSWAModule.ListaSWA.KodKreskowyMR
{
	[SmartPart]
	public partial class KodKreskowyMRView : ViewBase, IKodKreskowyMRView
	{
		#region Constructors

		public KodKreskowyMRView()
		{
			InitializeComponent();
			InitializeFocusedControl();
		}

		#endregion
		#region Protected Methods

		private void OnAnuluj(object sender, EventArgs e)
		{
			if (Anuluj != null)
			{
				Anuluj(sender, e);
			}
		}

		private void OnOK(object sender, EventArgs e)
		{
			if (OK != null)
			{
				OK(sender, e);
			}
		}

		#endregion
		#region IKodKreskowyMRView Members

		public event EventHandler OK;

		public event EventHandler Anuluj;

		public string KodKreskowy
		{
			get
			{
				return tbKod.Text;
			}
			set
			{
				tbKod.Text = value;
				tbKod.SelectAll();
			}
		}

		#endregion
		#region Override Members

		public override void SetNavigationState(bool navigationState)
		{
			base.SetNavigationState(navigationState);

			tbKod.ReadOnly = navigationState;
		}

		#endregion
	}
}
