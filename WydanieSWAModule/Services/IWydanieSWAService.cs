#region

using Common.DataModel;
using WydanieSWAModule.DataModel;
using WydanieSWAModule.ListaSWA.Search;

#endregion

namespace WydanieSWAModule.Services
{
    public interface IWydanieSWAService
    {
        ListaNaglowkow PobierzListeNaglowkowSWA(KryteriaZapytaniaListySWA kryteria, int? numerStrony);

        StatusOperacji RezerwujDokument(long idDokumentuSWA);

        Naglowek PobierzNaglowekSWA(long idPozycjiMR);

        Naglowek PobierzNaglowekSWA(string symbolDokumentuMR);

        Dokument PobierzDokumentSWA(long dokumentSWAID, string pozycjaMR, string zatwierdzony, string potwierdzony);

        StatusOperacji PotwierdzDokumentMR(long idDokumentuMR);

        StatusOperacji PotwierdzDokumentSWA(long idDokumentuSWA);

        StatusOperacji ZatwierdzSWA(long idDokumentuSWA);

        ListaOpakowan PobierzOpakowania(int? numerStrony, int wielkoscStrony);

        ListaOpakowan PobierzOpakowaniaDlaSWA(long idDokumentuSWA);

        StatusOperacji UsunOpakowanieDlaSWA(long idOpakowania);

        StatusOperacji PrzypiszOpakowanieDoSWA(long idDokumentuSWA, long idOpakowania, int wysokosc, int szerokosc, int dlugosc, float waga);

        StatusOperacji PrzypiszSpedytora(long idSpedytor, long idDokumentuSWA, string nazwaSpedytora, string symbolSpedytora);

        StatusOperacji UsunSpedytora(long idDokumentuSWA);

        ListaSpedytorow PobierzSpedytorow();

        ListaSpedytorow PobierzSpedytoraDlaDokumentu(long dokumentSWAID);

        StatusOperacji PotwierdzDokumentMRWgPozycji(long idPozycji);

        Identyfikator IdSWAdlaDokumentuMR(long idDokumentuMR);

        StatusOperacji PotwierdzDokumentMRWgSymbolu(string symbol);

        void UruchomRaport(string nazwaFormularza, long idSWA);
    }
}