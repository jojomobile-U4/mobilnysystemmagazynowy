#region

using System;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI;
using WydanieSWAModule.ListaSWA.Search;
using WydanieSWAModule.ProxyWrappers;
using WydanieSWAModule.WydanieSWAProxy;
using Dokument=WydanieSWAModule.DataModel.Dokument;
using Identyfikator=WydanieSWAModule.DataModel.Identyfikator;
using ListaNaglowkow=WydanieSWAModule.DataModel.ListaNaglowkow;
using ListaOpakowan=WydanieSWAModule.DataModel.ListaOpakowan;
using ListaSpedytorow=WydanieSWAModule.DataModel.ListaSpedytorow;
using Naglowek=WydanieSWAModule.DataModel.Naglowek;
using StatusOperacji=Common.DataModel.StatusOperacji;

#endregion

namespace WydanieSWAModule.Services
{
    [Service(typeof (IWydanieSWAService))]
    public class WydanieSWAService : ServiceBase, IWydanieSWAService
    {
        #region Constants

        private const string WSKAZNIK_PORZUCONY = "P";
        private const string WSKAZNIK_REZERWOWANY = "R";

        #endregion

        #region Properties

        protected override WebServiceConfiguration Configuration
        {
            get { return MyWorkItem.Configuration.WydanieSWAWS; }
        }

        #endregion

        #region Constructors

        public WydanieSWAService([ServiceDependency] WorkItem workItem) : base(workItem)
        {
        }

        #endregion

        #region IWydanieSWAService Members

        public ListaNaglowkow PobierzListeNaglowkowSWA(KryteriaZapytaniaListySWA kryteria, int? numerStrony)
        {
            var listaNaglowkow = new ListaNaglowkow();
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzListeNaglowkowWrap
                                    {
                                        dataWystawienia = kryteria.Data,
                                        numerStrony = numerStrony ?? 0,
                                        priorytet = kryteria.Priorytet,
                                        status = kryteria.Status != null ? kryteria.Status.ToUpper() : null,
                                        symbolDokumentu = kryteria.SymbolDokumentu != null ? kryteria.SymbolDokumentu.ToUpper() : null,
                                        symbolOdbiorcy = kryteria.SymbolOdbiorcy,
                                        wielkoscStrony = Configuration.WielkoscStrony
                                    };
                    //wskazniki porzucony i rezerwowany ustawiane tylko jezeli wskaznik jest wypelniony
                    if (!string.IsNullOrEmpty(kryteria.Wskaznik) &&
                        DBBool.IsDBBool(kryteria.Wskaznik.ToUpper()))
                    {
                        param.porzucony = WSKAZNIK_PORZUCONY.Equals(kryteria.Wskaznik.ToUpper()) ? DBBool.TRUE : DBBool.FALSE;
                        param.zarezerwowany = WSKAZNIK_REZERWOWANY.Equals(kryteria.Wskaznik.ToUpper()) ? DBBool.TRUE : DBBool.FALSE;
                    }

                    listaNaglowkow = new ListaNaglowkow(serviceAgent.pobierzListeNaglowkow(param).@return);
                }
            }
            catch (Exception ex)
            {
                //TODO: dodac zapisywanie do logow
                listaNaglowkow.StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, ex.Message);
            }

            return listaNaglowkow;
        }

        public StatusOperacji RezerwujDokument(long idDokumentuSwa)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new rezerwujDokumentSWA {id = idDokumentuSwa};

                    var statusOperacjiDb = serviceAgent.rezerwujDokumentSWA(param).@return;
                    statusOperacji = new StatusOperacji(statusOperacjiDb.status, statusOperacjiDb.tekst, statusOperacjiDb.stosWywolan);
                }
            }
            catch (Exception ex)
            {
                statusOperacji.Status = StatusOperacji.ERROR;
                statusOperacji.Tekst = ex.Message;
            }
            return statusOperacji;
        }

        public Naglowek PobierzNaglowekSWA(long idPozycjiMr)
        {
            try
            {
                WydanieSWAProxy.ListaNaglowkow lista;
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzListeNaglowkowWrap {numerStrony = 0, idPozycjiMR = idPozycjiMr};

                    lista = serviceAgent.pobierzListeNaglowkow(param).@return;
                }
                if (lista != null &&
                    lista.statusOperacji.status.Equals(StatusOperacji.SUCCESS) &&
                    lista.lista.Length > 0)
                {
                    return new Naglowek(lista.lista[0]);
                }
            }
            catch (Exception ex)
            {
                //TODO: dodac zapisywanie do logow
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = ex.Message;
            }
            MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.SUCCESS;
            return null;
        }

        public Naglowek PobierzNaglowekSWA(string symbolDokumentuMr)
        {
            try
            {
                WydanieSWAProxy.ListaNaglowkow lista;
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzListeNaglowkowWrap {numerStrony = 0, symbolPozycji = symbolDokumentuMr};

                    lista = serviceAgent.pobierzListeNaglowkow(param).@return;
                }
                if (lista != null &&
                    lista.statusOperacji.status.Equals(StatusOperacji.SUCCESS) &&
                    lista.lista.Length > 0)
                {
                    return new Naglowek(lista.lista[0]);
                }
            }
            catch (Exception ex)
            {
                //TODO: dodac zapisywanie do logow
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = ex.Message;
            }
            MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.SUCCESS;
            return null;
        }

        #endregion

        #region Pobierz dokument

        public Dokument PobierzDokumentSWA(long dokumentSwaid, string pozycjaMr, string zatwierdzony, string potwierdzony)
        {
            var dokument = new Dokument();
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzDokumentWrap();
                    param.id = dokumentSwaid;
                    param.potwierdzony = potwierdzony != null ? potwierdzony.ToUpper() : null;
                    param.zatwierdzony = zatwierdzony != null ? zatwierdzony.ToUpper() : null;
                    param.symbolDokumentuMR = pozycjaMr != null ? pozycjaMr.ToUpper() : null;

                    dokument = new Dokument(serviceAgent.pobierzDokument(param).@return);
                    //	                workItem.RootWorkItem.State["StatusOperacji"] = dokument.StatusOperacji.Status;
                    //	                workItem.RootWorkItem.State["TekstOperacji"] = dokument.StatusOperacji.Tekst;
                }
            }
            catch (Exception e)
            {
                // problem prawdopodobnie polega na tym, �e workItem jest null
                //	                workItem.RootWorkItem.State["StatusOperacji"] = StatusOperacji.ERROR;
                //	                workItem.RootWorkItem.State["TekstOperacji"] = e.Message;
                dokument.StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, e.Message);
            }
            return dokument;
        }

        #endregion

        #region Potwierdz dokument MR na podstawie ID dokumentu MR

        public StatusOperacji PotwierdzDokumentMR(long idDokumentuMr)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new potwierdzDokumentMR {id = idDokumentuMr};
                    var statusOperacjiWs = serviceAgent.potwierdzDokumentMR(param).@return;

                    statusOperacji = new StatusOperacji(statusOperacjiWs.status, statusOperacjiWs.tekst, statusOperacjiWs.stosWywolan);

                    //	                workItem.RootWorkItem.State["StatusOperacji"] = statusOperacji.status;
                    //	                workItem.RootWorkItem.State["TekstOperacji"] = statusOperacji.tekst;
                }
            }
            catch (Exception e)
            {
                //	                workItem.RootWorkItem.State["StatusOperacji"] = StatusOperacji.ERROR;
                //	                workItem.RootWorkItem.State["TekstOperacji"] = e.Message;
                statusOperacji = new StatusOperacji(StatusOperacji.ERROR, e.Message);
            }
            return statusOperacji;
        }

        #endregion

        #region Potwierdzenie dokumentu MR na podstawie symbolu tego dokumentu

        public StatusOperacji PotwierdzDokumentMRWgSymbolu(string symbol)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new potwierdzDokumentMRWgSymbolu {symbol = symbol};
                    var statusOperacjiWs = serviceAgent.potwierdzDokumentMRWgSymbolu(param).@return;

                    statusOperacji = new StatusOperacji(statusOperacjiWs.status, statusOperacjiWs.tekst, statusOperacjiWs.stosWywolan);
                }
            }
            catch (Exception e)
            {
                statusOperacji = new StatusOperacji(StatusOperacji.ERROR, e.Message);
            }
            return statusOperacji;
        }

        #endregion

        #region Potwierdzenie dokumentu SWA

        public StatusOperacji PotwierdzDokumentSWA(long idDokumentuSwa)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new potwierdzDokumentSWA {id = idDokumentuSwa};

                    var statusOperacjiWs = serviceAgent.potwierdzDokumentSWA(param).@return;

                    statusOperacji = new StatusOperacji(statusOperacjiWs.status, statusOperacjiWs.tekst, statusOperacjiWs.stosWywolan);
                }
            }
            catch (Exception e)
            {
                statusOperacji = new StatusOperacji(StatusOperacji.ERROR, e.Message);
            }

            return statusOperacji;
        }

        #endregion

        #region Zatwierd� dokument SWA

        public StatusOperacji ZatwierdzSWA(long idDokumentuSwa)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new zatwierdzDokumentSWA {id = idDokumentuSwa};

                    var statusOperacjiWs = serviceAgent.zatwierdzDokumentSWA(param).@return;

                    statusOperacji = new StatusOperacji(statusOperacjiWs.status, statusOperacjiWs.tekst, statusOperacjiWs.stosWywolan);
                }
            }

            catch (Exception e)
            {
                statusOperacji = new StatusOperacji(StatusOperacji.ERROR, e.Message);
            }
            return statusOperacji;
        }

        #endregion

        #region Pobierz opakowania

        public ListaOpakowan PobierzOpakowania(int? numerStrony, int wielkoscStrony)
        {
            var listaOpakowan = new ListaOpakowan();
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzListeOpakowanWrap();
                    param.numerStrony = numerStrony;
                    param.wielkoscStrony = wielkoscStrony;
                    listaOpakowan = new ListaOpakowan(serviceAgent.pobierzListeOpakowan(param).@return);
                    //	                workItem.RootWorkItem.State["StatusOperacji"] = listaOpakowan.StatusOperacji.Status;
                    //	                workItem.RootWorkItem.State["TekstOperacji"] = listaOpakowan.StatusOperacji.Tekst;
                }
            }
            catch (Exception e)
            {
                listaOpakowan.StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, e.Message);
                //	                workItem.RootWorkItem.State["StatusOperacji"] = StatusOperacji.ERROR;
                //	                workItem.RootWorkItem.State["TekstOperacji"] = e.Message;
            }
            return listaOpakowan;
        }

        #endregion

        #region Pobierz opakowania dla SWA

        public ListaOpakowan PobierzOpakowaniaDlaSWA(long idDokumentuSWA)
        {
            var listaOpakowan = new ListaOpakowan();
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzListeOpakowanDlaSWA {id = idDokumentuSWA};

                    listaOpakowan = new ListaOpakowan(serviceAgent.pobierzListeOpakowanDlaSWA(param).@return);
                    //	                workItem.RootWorkItem.State["StatusOperacji"] = listaOpakowan.StatusOperacji.Status;
                    //	                workItem.RootWorkItem.State["TekstOperacji"] = listaOpakowan.StatusOperacji.Tekst;
                }
            }
            catch (Exception e)
            {
                listaOpakowan.StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, e.Message);
                //					workItem.RootWorkItem.State["StatusOperacji"] = StatusOperacji.ERROR;
                //	                workItem.RootWorkItem.State["TekstOperacji"] = e.Message;
            }
            return listaOpakowan;
        }

        #endregion

        #region Usuni�cie opakowania dla SWA

        public StatusOperacji UsunOpakowanieDlaSWA(long idOpakowania)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new usunOpakowanie {id = idOpakowania};

                    var statusOperacjiWs = serviceAgent.usunOpakowanie(param).@return;

                    statusOperacji = new StatusOperacji(statusOperacjiWs.status, statusOperacjiWs.tekst, statusOperacjiWs.stosWywolan);
                }
            }
            catch (Exception e)
            {
                statusOperacji = new StatusOperacji(StatusOperacji.ERROR, e.Message);
            }
            return statusOperacji;
        }

        #endregion

        #region Przypisanie opakowania

        public StatusOperacji PrzypiszOpakowanieDoSWA(long idDokumentuSwa, long idOpakowania, int wysokosc, int szerokosc, int dlugosc, float waga)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new przypiszOpakowanieDoSWAWrap {idDokumentu = idDokumentuSwa, idOpakowania = idOpakowania, wysokosc = wysokosc, szerokosc = szerokosc, dlugosc = dlugosc, waga = waga};

                    var statusOperacjiWs = serviceAgent.przypiszOpakowanieDoSWA(param).@return;

                    statusOperacji = new StatusOperacji(statusOperacjiWs.status, statusOperacjiWs.tekst, statusOperacjiWs.stosWywolan);
                }
            }
            catch (Exception e)
            {
                statusOperacji = new StatusOperacji(StatusOperacji.ERROR, e.Message);
            }
            return statusOperacji;
        }

        #endregion

        #region Przepisz spedytora

        public StatusOperacji PrzypiszSpedytora(long idSpedytor, long idDokumentuSwa, string nazwaSpedytora, string symbolSpedytora)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    //TODO: Dodanie parametru nazwy i symbolu spedytora
                    var param = new przypiszSpedytoraDoSWAWrap {idDokumentu = idDokumentuSwa, idSpedytora = idSpedytor, nazwaSpedytora = nazwaSpedytora, symbolSpedytora = symbolSpedytora};

                    var statusOperacjiWs = serviceAgent.przypiszSpedytoraDoSWA(param).@return;

                    statusOperacji = new StatusOperacji(statusOperacjiWs.status, statusOperacjiWs.tekst, statusOperacjiWs.stosWywolan);
                }
            }
            catch (Exception e)
            {
                statusOperacji = new StatusOperacji(StatusOperacji.ERROR, e.Message);
            }
            return statusOperacji;
        }

        #endregion

        #region Usu� spedytora

        public StatusOperacji UsunSpedytora(long idDokumentuSwa)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new usunSpedytoraZSWA {idDokumentu = idDokumentuSwa};

                    var statusOperacjiWs = serviceAgent.usunSpedytoraZSWA(param).@return;

                    statusOperacji = new StatusOperacji(statusOperacjiWs.status, statusOperacjiWs.tekst, statusOperacjiWs.stosWywolan);
                }
            }
            catch (Exception e)
            {
                statusOperacji = new StatusOperacji(StatusOperacji.ERROR, e.Message);
            }
            return statusOperacji;
        }

        #endregion

        #region Pobierz liste spedytor�w

        public ListaSpedytorow PobierzSpedytorow()
        {
            var listaSpedytorow = new ListaSpedytorow(null);
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new PobierzListeSpedytorowWrap {numerStrony = 0};
                    listaSpedytorow = new ListaSpedytorow(serviceAgent.pobierzListeSpedytorow(param).@return);
                }
            }
            catch (Exception e)
            {
                listaSpedytorow.StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, e.Message);
            }
            return listaSpedytorow;
        }

        #endregion

        #region Pobierz spedytora dla dokumentu

        public ListaSpedytorow PobierzSpedytoraDlaDokumentu(long dokumentSwaid)
        {
            var listaSpedytorow = new ListaSpedytorow(null);
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzSpedytoraDlaSWA {id = dokumentSwaid};
                    listaSpedytorow = new ListaSpedytorow(serviceAgent.pobierzSpedytoraDlaSWA(param).@return);
                }
            }
            catch (Exception e)
            {
                listaSpedytorow.StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, e.Message);
            }
            return listaSpedytorow;
        }

        #endregion

        #region Potwierdz Dokument MR Wg Pozycji

        public StatusOperacji PotwierdzDokumentMRWgPozycji(long idPozycji)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new potwierdzDokumentMRWgPozycji {idPozycji = idPozycji};

                    var statusOperacjiWs = serviceAgent.potwierdzDokumentMRWgPozycji(param).@return;

                    statusOperacji = new StatusOperacji(statusOperacjiWs.status, statusOperacjiWs.tekst, statusOperacjiWs.stosWywolan);
                }
            }
            catch (Exception e)
            {
                statusOperacji = new StatusOperacji(StatusOperacji.ERROR, e.Message);
            }
            return statusOperacji;
        }

        #endregion

        #region idSWAdlaDokumentuMR

        public Identyfikator IdSWAdlaDokumentuMR(long idDokumentuMr)
        {
            var identyfikator = new Identyfikator();
            using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
            {
                try
                {
                    var param = new idSWAdlaDokumentuMR {id = idDokumentuMr};

                    identyfikator = new Identyfikator(serviceAgent.idSWAdlaDokumentuMR(param).@return);
                }
                catch (Exception e)
                {
                    identyfikator.StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, e.Message);
                }
            }
            return identyfikator;
        }

        #endregion

        #region Uruchomienie raportu

        public void UruchomRaport(string nazwaFormularza, long idSwa)
        {
            try
            {
                using (var serviceAgent = new WsWydanieSWAWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new uruchomRaportWrap {nazwaFormularza = nazwaFormularza, idSWA = idSwa};

                    var statusOperacji = serviceAgent.uruchomRaport(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = statusOperacji.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        #endregion
    }
}