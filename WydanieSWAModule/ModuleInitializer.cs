#region

using System;
using Common;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.Commands;
using WydanieSWAModule.ListaSWA;
using WydanieSWAModule.ListaSWA.KodKreskowyMR;
using WydanieSWAModule.ListaSWA.Search;
using WydanieSWAModule.RealizacjaSWA;

#endregion

namespace WydanieSWAModule
{
    public class ModuleInitializer : ModuleInit
    {
        public static readonly int WIELKOSC_STRONY = 10;
        private readonly WorkItem workItem;

        public ModuleInitializer([ServiceDependency] WorkItem workItem)
        {
            this.workItem = workItem;
        }

        public override void Load()
        {
            base.Load();
        }

        private void Initialize()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                InitializeListaSWA();
                InitializeRealizacjaSWA();
                if (!workItem.Services.Contains(typeof (IWagiService)))
                {
                    workItem.Services.AddNew(typeof (WagiService), typeof (IWagiService));
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        private void InitializeRealizacjaSWA()
        {
            if (!workItem.WorkItems.Contains(WorkItemsConstants.REALIZACJA_SWA_WORKITEM))
            {
                workItem.WorkItems.AddNew<RealizacjaSWAWorkItem>(WorkItemsConstants.REALIZACJA_SWA_WORKITEM);
            }
        }

        private void InitializeListaSWA()
        {
            if (!workItem.WorkItems.Contains(WorkItemsConstants.LISTA_SWA_WORKITEM))
            {
                var listaSWAWorkItem = workItem.WorkItems.AddNew<ListaSWAWorkItem>(WorkItemsConstants.LISTA_SWA_WORKITEM);
                listaSWAWorkItem.State[StateConstants.CRITERIAS] = new KryteriaZapytaniaListySWA();

                var listaSWAViewPresenter = new ListaSWAViewPresenter(listaSWAWorkItem.Items.AddNew<ListaSWAView>());
                listaSWAWorkItem.Items.Add(listaSWAViewPresenter, ItemsConstants.LISTA_SWA_PRESENTER);

                var listaSWASearchViewPresenter = new ListaSWASearchViewPresenter(listaSWAWorkItem.Items.AddNew<ListaSWASearchView>());
                listaSWAWorkItem.Items.Add(listaSWASearchViewPresenter, ItemsConstants.LISTA_SWA_SEARCH_PRESENTER);

                var kodKreskowyMRPresenter = new KodKreskowyMRViewPresenter(listaSWAWorkItem.Items.AddNew<KodKreskowyMRView>());
                listaSWAWorkItem.Items.Add(kodKreskowyMRPresenter, ItemsConstants.LISTA_SWA_KOD_KRESKOWY_MR_PRESENTER);
            }
        }

        [CommandHandler(CommandConstants.WYDANIE_SWA)]
        public void WydanieSWA(object sender, EventArgs e)
        {
            Initialize();

            var mainWorkspace = workItem.Workspaces[WorkspacesConstants.MAIN_WORKSPACE];
            var listaSWAWorkItem = workItem.WorkItems.Get<ListaSWAWorkItem>(WorkItemsConstants.LISTA_SWA_WORKITEM);
            workItem.RootWorkItem.State[StateConstants.WydanieMRModuleKontekstView] = ContextConstants.WydanieSWA;
            listaSWAWorkItem.Show(mainWorkspace);
        }
    }
}