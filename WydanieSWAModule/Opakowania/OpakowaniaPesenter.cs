using System;
using System.Windows.Forms;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using WydanieSWAModule.Opakowania.PrzepiszOpakowanie;
using WydanieSWAModule.DataModel;

namespace WydanieSWAModule.Opakowania
{
    public class OpakowaniaPresenter : PresenterBase
    {
        public const string NAZWA_FORMULARZA = "WYDANIE - OPAKOWANIE";

        #region Pola prywatne

        private const string POMYSLNE_WYWOLANIE_RAPORTU = "Wydruk zosta� wys�any na drukark�.";
        private int numerWyswietlanejStrony = 0;

        #endregion
        #region Konstruktor

        public OpakowaniaPresenter(IViewBase view) : base(view)
        {
        }

        #endregion
        #region Obs�uga zdarze�

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    e.Handled = true;
                    View_Powrot(this, EventArgs.Empty);
                    break;
                case Keys.D1:
                    e.Handled = true;
                    View.AkcjaPrzepisz();
                    break;
                case Keys.Left:
                    e.Handled = true;
                    View_Poprzedni(this,EventArgs.Empty);
                    break;
                case Keys.Right:
                    e.Handled = true;
                    View_Nastepny(this, EventArgs.Empty);
                    break;
                case Keys.D4:
                    e.Handled = true;
                    View_Spedytor(this, EventArgs.Empty);
                    break;
                case Keys.D3:
                    e.Handled = true;
                    View_Zatwierdz(this, EventArgs.Empty);
                    break;
                case Keys.D2:
                    e.Handled = true;
                    View.AkcjaUsun();
                    break;
                case Keys.D:
                    e.Handled = true;
                    uruchomRaport();
                    break;
                case (Keys.RButton | Keys.MButton | Keys.Back | Keys.ShiftKey | Keys.Space | Keys.F17):
                    e.Handled = true;
                    View.FocusNextControl(true);
                    break;
            }
        }

        protected override void AttachView()
        {
            View.Powrot -= new EventHandler(View_Powrot);
            View.Powrot += new EventHandler(View_Powrot);
            View.Usun -= new EventHandler<DataEventArgs<long>>(View_Usun);
            View.Usun += new EventHandler<DataEventArgs<long>>(View_Usun);
            View.Przepisz -= new EventHandler<PrzepiszOpakowanieEventArgs>(View_Przepisz);
            View.Przepisz += new EventHandler<PrzepiszOpakowanieEventArgs>(View_Przepisz);
            View.Nastepny -= new EventHandler(View_Nastepny);
            View.Nastepny += new EventHandler(View_Nastepny);
            View.Poprzedni -= new EventHandler(View_Poprzedni);
            View.Poprzedni += new EventHandler(View_Poprzedni);
            View.Spedytor -= new EventHandler(View_Spedytor);
            View.Spedytor += new EventHandler(View_Spedytor);
            View.Zatwierdz -= new EventHandler(View_Zatwierdz);
            View.Zatwierdz += new EventHandler(View_Zatwierdz);
        }

        void View_Zatwierdz(object sender, EventArgs e)
        {
            if (View.Status.Equals("P") && myWorkItem.ZatwierdzSWA())
            {
                myWorkItem.CloseView(View);
            }
        }

        void View_Spedytor(object sender, EventArgs e)
        {
            myWorkItem.ShowSpedytor();
        }

        void View_Poprzedni(object sender, EventArgs e)
        {
            if (numerWyswietlanejStrony > 0)
            {
                numerWyswietlanejStrony--;
                myWorkItem.Show(myWorkItem.MainWorkspace,numerWyswietlanejStrony,ModuleInitializer.WIELKOSC_STRONY);
            }            
                
            View.PrzyciskPoprzedniRekord = numerWyswietlanejStrony != 0;
        }

        void View_Nastepny(object sender, EventArgs e)
        {
            if (View.IloscWyswietlanychOpakowan>0)
            {
                View.PrzyciskPoprzedniRekord = true;
                numerWyswietlanejStrony++;
                myWorkItem.Show(myWorkItem.MainWorkspace, numerWyswietlanejStrony, ModuleInitializer.WIELKOSC_STRONY);
            }
        }

        void View_Przepisz(object sender, PrzepiszOpakowanieEventArgs e)
        {
            myWorkItem.ShowPrzepisz(e);
        }

        void View_Usun(object sender, DataEventArgs<long> e)
        {
            if (MessageBox.Show("Czy na pewno chcesz usun�� przepisane opakowanie?","Usuwanie opakowania", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)==DialogResult.Yes)
            {
                myWorkItem.UsunZListyOpakowan(e.Data);                    
            }
        }

        void View_Powrot(object sender, EventArgs e)
        {
            //powr�t wy�ej
            myWorkItem.CloseView(View);
        }

        private void uruchomRaport()
        {
            if (myWorkItem.UruchomRaport(NAZWA_FORMULARZA))
            {
                MessageBox.Show(POMYSLNE_WYWOLANIE_RAPORTU);
                myWorkItem.Activate();
            }
        }

        #endregion
        #region W�asno�ci

        public IOpakowaniaView View
        {
            get { return m_view as IOpakowaniaView; }
            set { m_view = value; }
        }

        public OpakowaniaWorkItem myWorkItem
        {
            get
            {
                return (WorkItemBase as OpakowaniaWorkItem);
            }
        }

        #endregion
        #region Metody publiczne

        public void WyswietlOpakowaniaDlaSWA(Opakowanie[] opakowania)
        {
            numerWyswietlanejStrony = 0;
            View.ListaOpakowanSWA = opakowania;            
        }

        public void WyswietlOpakowania(Opakowanie[] opakowania)
        {
            View.NumerWyswietlanejStrony = numerWyswietlanejStrony;
            View.ListaOpakowan = opakowania;
        }

        #endregion
    }
}

