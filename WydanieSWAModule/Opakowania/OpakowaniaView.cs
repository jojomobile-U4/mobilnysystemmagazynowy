using System;
using System.Windows.Forms;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using WydanieSWAModule.Opakowania.PrzepiszOpakowanie;
using WydanieSWAModule.DataModel;

namespace WydanieSWAModule.Opakowania
{
    [SmartPart]
    public partial class OpakowaniaView : ViewBase, IOpakowaniaView
    {
        #region Pola prywatne

        private int numerWyswietlanejStrony;

        #endregion
        #region Konstruktory

        public OpakowaniaView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #endregion
        #region Implementacja interfejsu IOpakowaniaView

        public event EventHandler Powrot;
        public event EventHandler<PrzepiszOpakowanieEventArgs> Przepisz;
        public event EventHandler<DataEventArgs<long>> Usun;
        public event EventHandler Poprzedni;
        public event EventHandler Nastepny;
        public event EventHandler Spedytor;
        public event EventHandler Zatwierdz;

        public string SymbolDokumentu
        {
            set
            {
                tbDokumentSWA.Text = value;
            }
        }

        public string Status
        {
            set
            {
                btnZatwierdz.Enabled = value.Substring(0,1).ToUpper() == "P";
            }            
            get
            {
                return btnZatwierdz.Enabled?"P":"N";
            }
        }

        public Opakowanie[] ListaOpakowanSWA
        {
            set
            {
                lsPrzypisaneOpakowaniaSWA.Items.Clear();
                int lp = 1;
                ListViewItem listViewItem;                
                if (value != null)
                    foreach (Opakowanie opakowanie in value)
                    {
                        listViewItem = lsPrzypisaneOpakowaniaSWA.Items.Add(new ListViewItem(lp.ToString()));
                        listViewItem.SubItems.Add(opakowanie.Symbol);
                        listViewItem.SubItems.Add(opakowanie.Waga.ToString("0.000"));
                        listViewItem.SubItems.Add(opakowanie.Jm);
                        listViewItem.SubItems.Add(opakowanie.Szerokosc.ToString() + 
                                                  "\\" + opakowanie.Dlugosc.ToString() + "\\" + opakowanie.Wysokosc.ToString());                        
                        listViewItem.Tag = opakowanie.Id;
                        lp++;
                    }
                tbLiczbaPozycji.Text = lsPrzypisaneOpakowaniaSWA.Items.Count.ToString();
                if (lsPrzypisaneOpakowaniaSWA.Items.Count>0)
                    lsPrzypisaneOpakowaniaSWA.Items[0].Selected = true;
            }
        }

        public Opakowanie[] ListaOpakowan
        {
            set 
            {
                lsListaOpakowan.Items.Clear();
                int lp = 1 + ModuleInitializer.WIELKOSC_STRONY * numerWyswietlanejStrony;
                ListViewItem listViewItem;
                if (value != null)
                    foreach (Opakowanie opakowanie in value)
                    {
                        listViewItem = lsListaOpakowan.Items.Add(new ListViewItem(lp.ToString()));
                        listViewItem.SubItems.Add(opakowanie.Symbol);
                        listViewItem.SubItems.Add(opakowanie.Nazwa);
                        listViewItem.SubItems.Add(opakowanie.Jm);                        
                        listViewItem.Tag = opakowanie.Id;
                        lp ++;
                    }
                if (lsListaOpakowan.Items.Count > 0)
                    lsListaOpakowan.Items[0].Selected = true;
            }
        }

        public int NumerWyswietlanejStrony
        {
            set { numerWyswietlanejStrony = value; }
        }

        public bool PrzyciskPoprzedniRekord
        {
            set { pcLeft.Enabled = value; }
        }

        public int IloscWyswietlanychOpakowan
        {
            get { return lsListaOpakowan.Items.Count; }
        }

        #endregion       
        #region Prywatne metody pomocnicze

        /// <summary>
        /// Sprawdza czy zosta� zaznaczony element na li�cie
        /// </summary>
        /// <returns></returns>
        private bool CzyZaznaczonyElementNaLiscieOpakowan()
        {
            return
                lsListaOpakowan.Items.Count > 0 &&
                lsListaOpakowan.SelectedIndices.Count > 0 &&
                lsListaOpakowan.Items[lsListaOpakowan.SelectedIndices[0]].Tag != null;
        }

        /// <summary>
        /// Sprawdza czy zosta� zaznaczony element na li�cie
        /// </summary>
        /// <returns></returns>
        private bool CzyZaznaczonyElementNaLiscieOpakowanDlaSWA()
        {
            return
                lsPrzypisaneOpakowaniaSWA.Items.Count > 0 &&
                lsPrzypisaneOpakowaniaSWA.SelectedIndices.Count > 0 &&
                lsPrzypisaneOpakowaniaSWA.Items[lsPrzypisaneOpakowaniaSWA.SelectedIndices[0]].Tag != null;
        }

        #endregion
        #region Obs�uga zdarze�

        private void btnPowrot_Click(object sender, EventArgs e)
        {
            if (Powrot != null)
            {
                Powrot(this, EventArgs.Empty);
            }
        }
        
        private void btnPrzepisz_Click(object sender, EventArgs e)
        {
            AkcjaPrzepisz();
        }

        public void AkcjaPrzepisz()
        {
            if (CzyZaznaczonyElementNaLiscieOpakowan() && Przepisz!=null)
            {
                PrzepiszOpakowanieEventArgs przepiszOpakowanieEventArgs = new PrzepiszOpakowanieEventArgs();
                przepiszOpakowanieEventArgs.DokumentSWA = tbDokumentSWA.Text;
                przepiszOpakowanieEventArgs.Symbol =
                    lsListaOpakowan.Items[lsListaOpakowan.SelectedIndices[0]].SubItems[1].Text;
                przepiszOpakowanieEventArgs.Nazwa =
                    lsListaOpakowan.Items[lsListaOpakowan.SelectedIndices[0]].SubItems[2].Text;
                przepiszOpakowanieEventArgs.Jm =
                    lsListaOpakowan.Items[lsListaOpakowan.SelectedIndices[0]].SubItems[3].Text;
                przepiszOpakowanieEventArgs.IDOpakowania = long.Parse(lsListaOpakowan.Items[lsListaOpakowan.SelectedIndices[0]].Tag.ToString());                

                Przepisz(this, przepiszOpakowanieEventArgs);
            }
        }

        private void btnUsun_Click(object sender, EventArgs e)
        {
            AkcjaUsun();
        }

        public void AkcjaUsun()
        {
            if (CzyZaznaczonyElementNaLiscieOpakowanDlaSWA() && Usun!= null)
            {
                Usun(this, new DataEventArgs<long>(long.Parse(lsPrzypisaneOpakowaniaSWA.Items[lsPrzypisaneOpakowaniaSWA.SelectedIndices[0]].Tag.ToString())));
            }
        }

        private void btnPoprzedni_Click(object sender, EventArgs e)
        {
            if (Poprzedni != null)
            {
                Poprzedni(this,EventArgs.Empty);
            }
        }

        private void btnNastepny_Click(object sender, EventArgs e)
        {
            if (Nastepny != null)
            {
                Nastepny(this, EventArgs.Empty);
            }
        }

        private void btnSpedytor_Click(object sender, EventArgs e)
        {
            if (Spedytor != null)
            {
                Spedytor(this, EventArgs.Empty);
            }
        }

        private void btnZatwierdz_Click(object sender, EventArgs e)
        {
            if (Zatwierdz != null)
            {
                Zatwierdz(this,EventArgs.Empty);
            }
        }

        #endregion
    }
}

