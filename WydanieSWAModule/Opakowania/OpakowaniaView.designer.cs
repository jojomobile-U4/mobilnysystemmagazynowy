
using Common.Components;

namespace WydanieSWAModule.Opakowania
{
    partial class OpakowaniaView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
		private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OpakowaniaView));
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbDokumentSWA = new System.Windows.Forms.TextBox();
            this.lbDokumentSWA = new System.Windows.Forms.Label();
            this.lsListaOpakowan = new Common.Components.MSMListView();
            this.chLP = new System.Windows.Forms.ColumnHeader();
            this.chSymbol = new System.Windows.Forms.ColumnHeader();
            this.chNazwa = new System.Windows.Forms.ColumnHeader();
            this.chJM = new System.Windows.Forms.ColumnHeader();
            this.lsPrzypisaneOpakowaniaSWA = new Common.Components.MSMListView();
            this.chlp2 = new System.Windows.Forms.ColumnHeader();
            this.chSymbol2 = new System.Windows.Forms.ColumnHeader();
            this.chWaga = new System.Windows.Forms.ColumnHeader();
            this.chJM2 = new System.Windows.Forms.ColumnHeader();
            this.chWymiar = new System.Windows.Forms.ColumnHeader();
            this.tbLiczbaPozycji = new System.Windows.Forms.TextBox();
            this.lblLiczbaPozycji = new System.Windows.Forms.Label();
            this.btnPowrot = new System.Windows.Forms.Button();
            this.btnSpedytor = new System.Windows.Forms.Button();
            this.btnPrzepisz = new System.Windows.Forms.Button();
            this.btnZatwierdz = new System.Windows.Forms.Button();
            this.btnUsun = new System.Windows.Forms.Button();
            this.lblPrzypisaneOpakowania = new System.Windows.Forms.Label();
            this.pcRight = new System.Windows.Forms.PictureBox();
            this.pcLeft = new System.Windows.Forms.PictureBox();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnPowrot);
            this.pnlNavigation.Controls.Add(this.btnPrzepisz);
            this.pnlNavigation.Controls.Add(this.btnSpedytor);
            this.pnlNavigation.Controls.Add(this.btnZatwierdz);
            this.pnlNavigation.Controls.Add(this.btnUsun);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 52);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 9;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "WYDANIE - OPAKOWANIA";
            // 
            // tbDokumentSWA
            // 
            this.tbDokumentSWA.Location = new System.Drawing.Point(102, 23);
            this.tbDokumentSWA.Name = "tbDokumentSWA";
            this.tbDokumentSWA.ReadOnly = true;
            this.tbDokumentSWA.Size = new System.Drawing.Size(135, 21);
            this.tbDokumentSWA.TabIndex = 0;
            this.tbDokumentSWA.TabStop = false;
            // 
            // lbDokumentSWA
            // 
            this.lbDokumentSWA.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbDokumentSWA.Location = new System.Drawing.Point(3, 25);
            this.lbDokumentSWA.Name = "lbDokumentSWA";
            this.lbDokumentSWA.Size = new System.Drawing.Size(100, 21);
            this.lbDokumentSWA.Text = "Dokument SWA :";
            // 
            // lsListaOpakowan
            // 
            this.lsListaOpakowan.Columns.Add(this.chLP);
            this.lsListaOpakowan.Columns.Add(this.chSymbol);
            this.lsListaOpakowan.Columns.Add(this.chNazwa);
            this.lsListaOpakowan.Columns.Add(this.chJM);
            this.lsListaOpakowan.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lsListaOpakowan.FullRowSelect = true;
            this.lsListaOpakowan.Location = new System.Drawing.Point(2, 47);
            this.lsListaOpakowan.Name = "lsListaOpakowan";
            this.lsListaOpakowan.Size = new System.Drawing.Size(234, 80);
            this.lsListaOpakowan.TabIndex = 1;
            this.lsListaOpakowan.View = System.Windows.Forms.View.Details;
            // 
            // chLP
            // 
            this.chLP.Text = "LP";
            this.chLP.Width = 20;
            // 
            // chSymbol
            // 
            this.chSymbol.Text = "Indeks";
            this.chSymbol.Width = 60;
            // 
            // chNazwa
            // 
            this.chNazwa.Text = "Nazwa";
            this.chNazwa.Width = 60;
            // 
            // chJM
            // 
            this.chJM.Text = "J.m.";
            this.chJM.Width = 60;
            // 
            // lsPrzypisaneOpakowaniaSWA
            // 
            this.lsPrzypisaneOpakowaniaSWA.Columns.Add(this.chlp2);
            this.lsPrzypisaneOpakowaniaSWA.Columns.Add(this.chSymbol2);
            this.lsPrzypisaneOpakowaniaSWA.Columns.Add(this.chWaga);
            this.lsPrzypisaneOpakowaniaSWA.Columns.Add(this.chJM2);
            this.lsPrzypisaneOpakowaniaSWA.Columns.Add(this.chWymiar);
            this.lsPrzypisaneOpakowaniaSWA.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lsPrzypisaneOpakowaniaSWA.FullRowSelect = true;
            this.lsPrzypisaneOpakowaniaSWA.Location = new System.Drawing.Point(3, 147);
            this.lsPrzypisaneOpakowaniaSWA.Name = "lsPrzypisaneOpakowaniaSWA";
            this.lsPrzypisaneOpakowaniaSWA.Size = new System.Drawing.Size(234, 80);
            this.lsPrzypisaneOpakowaniaSWA.TabIndex = 2;
            this.lsPrzypisaneOpakowaniaSWA.View = System.Windows.Forms.View.Details;
            // 
            // chlp2
            // 
            this.chlp2.Text = "LP";
            this.chlp2.Width = 20;
            // 
            // chSymbol2
            // 
            this.chSymbol2.Text = "Indeks";
            this.chSymbol2.Width = 60;
            // 
            // chWaga
            // 
            this.chWaga.Text = "Waga";
            this.chWaga.Width = 45;
            // 
            // chJM2
            // 
            this.chJM2.Text = "J.m.";
            this.chJM2.Width = 30;
            // 
            // chWymiar
            // 
            this.chWymiar.Text = "szer./d�./wys.";
            this.chWymiar.Width = 70;
            // 
            // tbLiczbaPozycji
            // 
            this.tbLiczbaPozycji.Location = new System.Drawing.Point(86, 228);
            this.tbLiczbaPozycji.Name = "tbLiczbaPozycji";
            this.tbLiczbaPozycji.ReadOnly = true;
            this.tbLiczbaPozycji.Size = new System.Drawing.Size(53, 21);
            this.tbLiczbaPozycji.TabIndex = 3;
            this.tbLiczbaPozycji.TabStop = false;
            // 
            // lblLiczbaPozycji
            // 
            this.lblLiczbaPozycji.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblLiczbaPozycji.Location = new System.Drawing.Point(3, 230);
            this.lblLiczbaPozycji.Name = "lblLiczbaPozycji";
            this.lblLiczbaPozycji.Size = new System.Drawing.Size(100, 21);
            this.lblLiczbaPozycji.Text = "Liczba pozycji:";
            // 
            // btnPowrot
            // 
            this.btnPowrot.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPowrot.Location = new System.Drawing.Point(161, 28);
            this.btnPowrot.Name = "btnPowrot";
            this.btnPowrot.Size = new System.Drawing.Size(76, 22);
            this.btnPowrot.TabIndex = 51;
            this.btnPowrot.TabStop = false;
            this.btnPowrot.Text = "&Esc Powr�t";
            this.btnPowrot.Click += new System.EventHandler(this.btnPowrot_Click);
            // 
            // btnSpedytor
            // 
            this.btnSpedytor.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnSpedytor.Location = new System.Drawing.Point(76, 28);
            this.btnSpedytor.Name = "btnSpedytor";
            this.btnSpedytor.Size = new System.Drawing.Size(83, 22);
            this.btnSpedytor.TabIndex = 50;
            this.btnSpedytor.TabStop = false;
            this.btnSpedytor.Text = "&4 Spedytor";
            this.btnSpedytor.Click += new System.EventHandler(this.btnSpedytor_Click);
            // 
            // btnPrzepisz
            // 
            this.btnPrzepisz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPrzepisz.Location = new System.Drawing.Point(3, 28);
            this.btnPrzepisz.Name = "btnPrzepisz";
            this.btnPrzepisz.Size = new System.Drawing.Size(71, 22);
            this.btnPrzepisz.TabIndex = 49;
            this.btnPrzepisz.TabStop = false;
            this.btnPrzepisz.Text = "&1 Przepisz";
            this.btnPrzepisz.Click += new System.EventHandler(this.btnPrzepisz_Click);
            // 
            // btnZatwierdz
            // 
            this.btnZatwierdz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnZatwierdz.Location = new System.Drawing.Point(76, 4);
            this.btnZatwierdz.Name = "btnZatwierdz";
            this.btnZatwierdz.Size = new System.Drawing.Size(83, 22);
            this.btnZatwierdz.TabIndex = 53;
            this.btnZatwierdz.TabStop = false;
            this.btnZatwierdz.Text = "&3 Zatwierd�";
            this.btnZatwierdz.Click += new System.EventHandler(this.btnZatwierdz_Click);
            // 
            // btnUsun
            // 
            this.btnUsun.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnUsun.Location = new System.Drawing.Point(3, 4);
            this.btnUsun.Name = "btnUsun";
            this.btnUsun.Size = new System.Drawing.Size(71, 22);
            this.btnUsun.TabIndex = 52;
            this.btnUsun.TabStop = false;
            this.btnUsun.Text = "&2 Usu�";
            this.btnUsun.Click += new System.EventHandler(this.btnUsun_Click);
            // 
            // lblPrzypisaneOpakowania
            // 
            this.lblPrzypisaneOpakowania.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblPrzypisaneOpakowania.Location = new System.Drawing.Point(0, 128);
            this.lblPrzypisaneOpakowania.Name = "lblPrzypisaneOpakowania";
            this.lblPrzypisaneOpakowania.Size = new System.Drawing.Size(240, 16);
            this.lblPrzypisaneOpakowania.Text = "PRZYPISANE OPAKOWANIA";
            this.lblPrzypisaneOpakowania.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pcRight
            // 
            this.pcRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.pcRight.Image = ((System.Drawing.Image)(resources.GetObject("pcRight.Image")));
            this.pcRight.Location = new System.Drawing.Point(221, 0);
            this.pcRight.Name = "pcRight";
            this.pcRight.Size = new System.Drawing.Size(16, 16);
            this.pcRight.Click += new System.EventHandler(this.btnNastepny_Click);
            // 
            // pcLeft
            // 
            this.pcLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.pcLeft.Image = ((System.Drawing.Image)(resources.GetObject("pcLeft.Image")));
            this.pcLeft.Location = new System.Drawing.Point(205, 0);
            this.pcLeft.Name = "pcLeft";
            this.pcLeft.Size = new System.Drawing.Size(16, 16);
            this.pcLeft.Click += new System.EventHandler(this.btnPoprzedni_Click);
            // 
            // OpakowaniaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.pcRight);
            this.Controls.Add(this.pcLeft);
            this.Controls.Add(this.lblPrzypisaneOpakowania);
            this.Controls.Add(this.tbLiczbaPozycji);
            this.Controls.Add(this.lblLiczbaPozycji);
            this.Controls.Add(this.lsPrzypisaneOpakowaniaSWA);
            this.Controls.Add(this.lsListaOpakowan);
            this.Controls.Add(this.tbDokumentSWA);
            this.Controls.Add(this.lbDokumentSWA);
            this.Controls.Add(this.lbTytul);
            this.Name = "OpakowaniaView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lbDokumentSWA, 0);
            this.Controls.SetChildIndex(this.tbDokumentSWA, 0);
            this.Controls.SetChildIndex(this.lsListaOpakowan, 0);
            this.Controls.SetChildIndex(this.lsPrzypisaneOpakowaniaSWA, 0);
            this.Controls.SetChildIndex(this.lblLiczbaPozycji, 0);
            this.Controls.SetChildIndex(this.tbLiczbaPozycji, 0);
            this.Controls.SetChildIndex(this.lblPrzypisaneOpakowania, 0);
            this.Controls.SetChildIndex(this.pcLeft, 0);
            this.Controls.SetChildIndex(this.pcRight, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MSMLabel lbTytul;
        private System.Windows.Forms.TextBox tbDokumentSWA;
        private System.Windows.Forms.Label lbDokumentSWA;
        private Common.Components.MSMListView lsListaOpakowan;
        private System.Windows.Forms.ColumnHeader chLP;
        private System.Windows.Forms.ColumnHeader chSymbol;
        private System.Windows.Forms.ColumnHeader chNazwa;
        private System.Windows.Forms.ColumnHeader chJM;
        private Common.Components.MSMListView lsPrzypisaneOpakowaniaSWA;
        private System.Windows.Forms.ColumnHeader chlp2;
        private System.Windows.Forms.ColumnHeader chSymbol2;
        private System.Windows.Forms.ColumnHeader chWaga;
        private System.Windows.Forms.ColumnHeader chJM2;
        private System.Windows.Forms.ColumnHeader chWymiar;
        private System.Windows.Forms.TextBox tbLiczbaPozycji;
        private System.Windows.Forms.Label lblLiczbaPozycji;
        private System.Windows.Forms.Button btnPowrot;
        private System.Windows.Forms.Button btnSpedytor;
        private System.Windows.Forms.Button btnPrzepisz;
        private System.Windows.Forms.Button btnZatwierdz;
        private System.Windows.Forms.Button btnUsun;
        private System.Windows.Forms.Label lblPrzypisaneOpakowania;
        private System.Windows.Forms.PictureBox pcRight;
        private System.Windows.Forms.PictureBox pcLeft;
    }
}

