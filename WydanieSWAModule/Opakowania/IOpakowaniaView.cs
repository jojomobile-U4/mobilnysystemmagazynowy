using System;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using WydanieSWAModule.Opakowania.PrzepiszOpakowanie;
using WydanieSWAModule.DataModel;

namespace WydanieSWAModule.Opakowania
{
    public interface IOpakowaniaView : IViewBase
    {
        #region Zdarzenia

        event EventHandler Powrot;
        event EventHandler<PrzepiszOpakowanieEventArgs> Przepisz;
        event EventHandler<DataEventArgs<long>> Usun;
        event EventHandler Poprzedni;
        event EventHandler Nastepny;
        event EventHandler Spedytor;
        event EventHandler Zatwierdz;

        #endregion
        #region Metody publiczne

        void AkcjaPrzepisz();

        void AkcjaUsun();

        #endregion
        #region W�asno�ci

        string Status { set; get;}
        
        string SymbolDokumentu {set;}

        Opakowanie[] ListaOpakowanSWA { set; }

        Opakowanie[] ListaOpakowan { set; }

        int NumerWyswietlanejStrony { set; }       

        int IloscWyswietlanychOpakowan { get; }

        bool PrzyciskPoprzedniRekord { set; }

        #endregion
    }
}

