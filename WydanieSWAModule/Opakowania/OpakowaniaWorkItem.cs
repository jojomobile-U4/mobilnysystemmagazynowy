#region

using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using WydanieSWAModule.Opakowania.PrzepiszOpakowanie;
using WydanieSWAModule.RealizacjaSWA;
using WydanieSWAModule.Services;
using WydanieSWAModule.Spedytor;

#endregion

namespace WydanieSWAModule.Opakowania
{
    public class OpakowaniaWorkItem : WorkItemBase
    {
        #region Pola prywatne

        private long iDDokumentuSWA;
        private OpakowaniaPresenter opakowaniaPresenter;
        private OpakowaniaView opakowaniaView;
        private PrzepiszOpakowanieWorkItem przepiszOpakowanieWorkItem;
        private SpedytorWorkItem spedytorWorkItem;
        private string symbolDokumentu;
        private IWydanieSWAService wydanieSWAService;

        #endregion

        #region Metody show

        public void Show(IWorkspace workspace, long iDDokumentuSWA, string symbolDokumentu, string status)
        {
            this.iDDokumentuSWA = iDDokumentuSWA;
            this.symbolDokumentu = symbolDokumentu;

            #region Inicjacja zmiennych View + Presenter

            opakowaniaView = opakowaniaView ?? Items.AddNew<OpakowaniaView>();
            opakowaniaPresenter = opakowaniaPresenter ?? new OpakowaniaPresenter(opakowaniaView);
            Items.Add(opakowaniaPresenter);

            #endregion

            #region Pobranie opakowa� dla SWA

            PobierzOpakowaniaDlaSWA();
            opakowaniaView.SymbolDokumentu = symbolDokumentu;
            opakowaniaView.Status = status;

            #endregion

            Show(workspace, 0, ModuleInitializer.WIELKOSC_STRONY);
        }

        internal void Show(IWorkspace workspace, int numerStrony, int wielkoscStrony)
        {
            FunctionsHelper.SetCursorWait();

            #region Inicjacja zmiennych View + Presenter

            opakowaniaView = opakowaniaView ?? Items.AddNew<OpakowaniaView>();
            opakowaniaPresenter = opakowaniaPresenter ?? new OpakowaniaPresenter(opakowaniaView);
            Items.Add(opakowaniaPresenter);

            #endregion

            FunctionsHelper.SetCursorDefault();
            wydanieSWAService = Services.Get<IWydanieSWAService>(true);
            var listaOpakowan = wydanieSWAService.PobierzOpakowania(numerStrony, wielkoscStrony);

            if (!listaOpakowan.StatusOperacji.Status.Equals("S"))
            {
                ShowMessageBox(listaOpakowan.StatusOperacji.Tekst, "Lista opakowa�", listaOpakowan.StatusOperacji.StosWywolan);
            }
            opakowaniaPresenter.WyswietlOpakowania(listaOpakowan.Lista);

            workspace.Show(opakowaniaView);
            Activate();
            FunctionsHelper.SetCursorDefault();
        }

        #endregion

        #region Metody publiczne

        public void PobierzOpakowaniaDlaSWA()
        {
            FunctionsHelper.SetCursorWait();
            wydanieSWAService = Services.Get<IWydanieSWAService>(true);
            var listaOpakowan = wydanieSWAService.PobierzOpakowaniaDlaSWA(iDDokumentuSWA);
            FunctionsHelper.SetCursorDefault();
            if (!listaOpakowan.StatusOperacji.Status.Equals("S"))
            {
                ShowMessageBox(listaOpakowan.StatusOperacji.Tekst, BLAD, listaOpakowan.StatusOperacji.StosWywolan);
                Activate();
            }
            opakowaniaPresenter.WyswietlOpakowaniaDlaSWA(listaOpakowan.Lista);
        }

        public bool UsunZListyOpakowan(long IdOpakowania)
        {
            wydanieSWAService = Services.Get<IWydanieSWAService>(true);
            var operacji = wydanieSWAService.UsunOpakowanieDlaSWA(IdOpakowania);
            if (operacji.Status.Equals(StatusOperacji.SUCCESS))
            {
                PobierzOpakowaniaDlaSWA();
                return true;
            }
            else
            {
                ShowMessageBox(operacji.Tekst, "Usuni�cie opakowania dla SWA", operacji.StosWywolan);
                Activate();
            }
            return false;
        }

        public void ShowPrzepisz(PrzepiszOpakowanieEventArgs przepiszOpakowanieEventArgs)
        {
            przepiszOpakowanieEventArgs.IDDokumentuSWA = iDDokumentuSWA;
            przepiszOpakowanieWorkItem = przepiszOpakowanieWorkItem ?? WorkItems.AddNew<PrzepiszOpakowanieWorkItem>();
            przepiszOpakowanieWorkItem.Show(MainWorkspace, przepiszOpakowanieEventArgs);
        }

        public void ShowSpedytor()
        {
            spedytorWorkItem = spedytorWorkItem ?? WorkItems.AddNew<SpedytorWorkItem>();
            spedytorWorkItem.Show(MainWorkspace, iDDokumentuSWA, symbolDokumentu);
        }

        public bool ZatwierdzSWA()
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                wydanieSWAService = Services.Get<IWydanieSWAService>(true);
                var operacji = wydanieSWAService.ZatwierdzSWA(iDDokumentuSWA);
                if (operacji.Status.Equals(StatusOperacji.SUCCESS))
                {
                    (Parent as RealizacjaSWAWorkItem).ZatwierdzonoDokument();
                    FunctionsHelper.SetCursorDefault();
                    return true;
                }
                else
                {
                    ShowMessageBox(operacji.Tekst, BLAD, operacji.StosWywolan);
                    Activate();
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
            return false;
        }

        public bool UruchomRaport(string nazwaFormularza)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                Services.Get<IWydanieSWAService>(true).UruchomRaport(nazwaFormularza, iDDokumentuSWA);
                if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, BLAD, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                    return false;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }

            return true;
        }

        #endregion
    }
}