using Common.Components;

namespace WydanieSWAModule.Opakowania.PrzepiszOpakowanie
{
    partial class PrzepiszOpakowanieView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAnuluj = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.tbDokumentSWA = new System.Windows.Forms.TextBox();
            this.lbDokumentSWA = new System.Windows.Forms.Label();
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbDlugosc = new System.Windows.Forms.TextBox();
            this.lblDlugosc = new System.Windows.Forms.Label();
            this.tbJM = new System.Windows.Forms.TextBox();
            this.lblJM = new System.Windows.Forms.Label();
            this.tbNazwaOpakowania = new System.Windows.Forms.TextBox();
            this.lblNazwaOpakowania = new System.Windows.Forms.Label();
            this.tbSymbolOpakowania = new System.Windows.Forms.TextBox();
            this.lblSymbolDokumentu = new System.Windows.Forms.Label();
            this.tbSzerokosc = new System.Windows.Forms.TextBox();
            this.lblSzerokosc = new System.Windows.Forms.Label();
            this.tbWaga = new System.Windows.Forms.TextBox();
            this.lblWaga = new System.Windows.Forms.Label();
            this.tbWysokosc = new System.Windows.Forms.TextBox();
            this.lblWysokosc = new System.Windows.Forms.Label();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnAnuluj);
            this.pnlNavigation.Controls.Add(this.btnOK);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 52);
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnAnuluj.Location = new System.Drawing.Point(152, 24);
            this.btnAnuluj.Name = "btnAnuluj";
            this.btnAnuluj.Size = new System.Drawing.Size(83, 22);
            this.btnAnuluj.TabIndex = 9;
            this.btnAnuluj.Text = "&Esc Anuluj";
            this.btnAnuluj.Click += new System.EventHandler(this.btnAnuluj_Click);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnOK.Location = new System.Drawing.Point(79, 24);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(71, 22);
            this.btnOK.TabIndex = 8;
            this.btnOK.Text = "&Ret OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // tbDokumentSWA
            // 
            this.tbDokumentSWA.Location = new System.Drawing.Point(104, 37);
            this.tbDokumentSWA.Name = "tbDokumentSWA";
            this.tbDokumentSWA.ReadOnly = true;
            this.tbDokumentSWA.Size = new System.Drawing.Size(133, 21);
            this.tbDokumentSWA.TabIndex = 0;
            this.tbDokumentSWA.TabStop = false;
            // 
            // lbDokumentSWA
            // 
            this.lbDokumentSWA.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbDokumentSWA.Location = new System.Drawing.Point(2, 37);
            this.lbDokumentSWA.Name = "lbDokumentSWA";
            this.lbDokumentSWA.Size = new System.Drawing.Size(100, 21);
            this.lbDokumentSWA.Text = "Dokument SWA:";
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 15;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "WYDANIE - PRZEPISANIE OPAKOWANIA";
            // 
            // tbDlugosc
            // 
            this.tbDlugosc.Location = new System.Drawing.Point(104, 162);
            this.tbDlugosc.Name = "tbDlugosc";
            this.tbDlugosc.Size = new System.Drawing.Size(133, 21);
            this.tbDlugosc.TabIndex = 5;
            // 
            // lblDlugosc
            // 
            this.lblDlugosc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblDlugosc.Location = new System.Drawing.Point(1, 162);
            this.lblDlugosc.Name = "lblDlugosc";
            this.lblDlugosc.Size = new System.Drawing.Size(100, 21);
            this.lblDlugosc.Text = "D�ugo��:";
            // 
            // tbJM
            // 
            this.tbJM.Location = new System.Drawing.Point(104, 112);
            this.tbJM.Name = "tbJM";
            this.tbJM.ReadOnly = true;
            this.tbJM.Size = new System.Drawing.Size(133, 21);
            this.tbJM.TabIndex = 3;
            this.tbJM.TabStop = false;
            // 
            // lblJM
            // 
            this.lblJM.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblJM.Location = new System.Drawing.Point(2, 112);
            this.lblJM.Name = "lblJM";
            this.lblJM.Size = new System.Drawing.Size(100, 21);
            this.lblJM.Text = "J.m.:";
            // 
            // tbNazwaOpakowania
            // 
            this.tbNazwaOpakowania.Location = new System.Drawing.Point(104, 87);
            this.tbNazwaOpakowania.Name = "tbNazwaOpakowania";
            this.tbNazwaOpakowania.ReadOnly = true;
            this.tbNazwaOpakowania.Size = new System.Drawing.Size(133, 21);
            this.tbNazwaOpakowania.TabIndex = 2;
            this.tbNazwaOpakowania.TabStop = false;
            // 
            // lblNazwaOpakowania
            // 
            this.lblNazwaOpakowania.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblNazwaOpakowania.Location = new System.Drawing.Point(1, 87);
            this.lblNazwaOpakowania.Name = "lblNazwaOpakowania";
            this.lblNazwaOpakowania.Size = new System.Drawing.Size(110, 21);
            this.lblNazwaOpakowania.Text = "Nazwa opakowania:";
            // 
            // tbSymbolOpakowania
            // 
            this.tbSymbolOpakowania.Location = new System.Drawing.Point(104, 62);
            this.tbSymbolOpakowania.Name = "tbSymbolOpakowania";
            this.tbSymbolOpakowania.ReadOnly = true;
            this.tbSymbolOpakowania.Size = new System.Drawing.Size(133, 21);
            this.tbSymbolOpakowania.TabIndex = 1;
            this.tbSymbolOpakowania.TabStop = false;
            // 
            // lblSymbolDokumentu
            // 
            this.lblSymbolDokumentu.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblSymbolDokumentu.Location = new System.Drawing.Point(0, 62);
            this.lblSymbolDokumentu.Name = "lblSymbolDokumentu";
            this.lblSymbolDokumentu.Size = new System.Drawing.Size(111, 21);
            this.lblSymbolDokumentu.Text = "Indeks opakowania:";
            // 
            // tbSzerokosc
            // 
            this.tbSzerokosc.Location = new System.Drawing.Point(104, 187);
            this.tbSzerokosc.Name = "tbSzerokosc";
            this.tbSzerokosc.Size = new System.Drawing.Size(133, 21);
            this.tbSzerokosc.TabIndex = 6;
            // 
            // lblSzerokosc
            // 
            this.lblSzerokosc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblSzerokosc.Location = new System.Drawing.Point(1, 187);
            this.lblSzerokosc.Name = "lblSzerokosc";
            this.lblSzerokosc.Size = new System.Drawing.Size(100, 21);
            this.lblSzerokosc.Text = "Szeroko��:";
            // 
            // tbWaga
            // 
            this.tbWaga.Location = new System.Drawing.Point(104, 137);
            this.tbWaga.Name = "tbWaga";
            this.tbWaga.Size = new System.Drawing.Size(133, 21);
            this.tbWaga.TabIndex = 4;
            // 
            // lblWaga
            // 
            this.lblWaga.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWaga.Location = new System.Drawing.Point(1, 137);
            this.lblWaga.Name = "lblWaga";
            this.lblWaga.Size = new System.Drawing.Size(100, 21);
            this.lblWaga.Text = "Waga:";
            // 
            // tbWysokosc
            // 
            this.tbWysokosc.Location = new System.Drawing.Point(104, 211);
            this.tbWysokosc.Name = "tbWysokosc";
            this.tbWysokosc.Size = new System.Drawing.Size(133, 21);
            this.tbWysokosc.TabIndex = 7;
            // 
            // lblWysokosc
            // 
            this.lblWysokosc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWysokosc.Location = new System.Drawing.Point(2, 211);
            this.lblWysokosc.Name = "lblWysokosc";
            this.lblWysokosc.Size = new System.Drawing.Size(100, 21);
            this.lblWysokosc.Text = "Wysoko��:";
            // 
            // PrzepiszOpakowanieView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.tbWysokosc);
            this.Controls.Add(this.tbWaga);
            this.Controls.Add(this.tbSzerokosc);
            this.Controls.Add(this.tbSymbolOpakowania);
            this.Controls.Add(this.tbNazwaOpakowania);
            this.Controls.Add(this.tbJM);
            this.Controls.Add(this.tbDlugosc);
            this.Controls.Add(this.tbDokumentSWA);
            this.Controls.Add(this.lblWysokosc);
            this.Controls.Add(this.lblWaga);
            this.Controls.Add(this.lblSzerokosc);
            this.Controls.Add(this.lblSymbolDokumentu);
            this.Controls.Add(this.lblNazwaOpakowania);
            this.Controls.Add(this.lblJM);
            this.Controls.Add(this.lblDlugosc);
            this.Controls.Add(this.lbTytul);
            this.Controls.Add(this.lbDokumentSWA);
            this.Name = "PrzepiszOpakowanieView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbDokumentSWA, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lblDlugosc, 0);
            this.Controls.SetChildIndex(this.lblJM, 0);
            this.Controls.SetChildIndex(this.lblNazwaOpakowania, 0);
            this.Controls.SetChildIndex(this.lblSymbolDokumentu, 0);
            this.Controls.SetChildIndex(this.lblSzerokosc, 0);
            this.Controls.SetChildIndex(this.lblWaga, 0);
            this.Controls.SetChildIndex(this.lblWysokosc, 0);
            this.Controls.SetChildIndex(this.tbDokumentSWA, 0);
            this.Controls.SetChildIndex(this.tbDlugosc, 0);
            this.Controls.SetChildIndex(this.tbJM, 0);
            this.Controls.SetChildIndex(this.tbNazwaOpakowania, 0);
            this.Controls.SetChildIndex(this.tbSymbolOpakowania, 0);
            this.Controls.SetChildIndex(this.tbSzerokosc, 0);
            this.Controls.SetChildIndex(this.tbWaga, 0);
            this.Controls.SetChildIndex(this.tbWysokosc, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAnuluj;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TextBox tbDokumentSWA;
        private System.Windows.Forms.Label lbDokumentSWA;
        private MSMLabel lbTytul;
        private System.Windows.Forms.TextBox tbDlugosc;
        private System.Windows.Forms.Label lblDlugosc;
        private System.Windows.Forms.TextBox tbJM;
        private System.Windows.Forms.Label lblJM;
        private System.Windows.Forms.TextBox tbNazwaOpakowania;
        private System.Windows.Forms.Label lblNazwaOpakowania;
        private System.Windows.Forms.TextBox tbSymbolOpakowania;
        private System.Windows.Forms.Label lblSymbolDokumentu;
        private System.Windows.Forms.TextBox tbSzerokosc;
        private System.Windows.Forms.Label lblSzerokosc;
        private System.Windows.Forms.TextBox tbWaga;
        private System.Windows.Forms.Label lblWaga;
        private System.Windows.Forms.TextBox tbWysokosc;
        private System.Windows.Forms.Label lblWysokosc;
    }
}
