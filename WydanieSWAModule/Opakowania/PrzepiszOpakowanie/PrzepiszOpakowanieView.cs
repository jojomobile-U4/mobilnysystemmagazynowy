#region

using System;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace WydanieSWAModule.Opakowania.PrzepiszOpakowanie
{
    [SmartPart]
    public partial class PrzepiszOpakowanieView : ViewBase, IPrzepiszOpakowanieView
    {
        #region Pola prywatne

        private PrzepiszOpakowanieEventArgs wyswietlaneDane;

        #endregion

        #region Konstruktor

        public PrzepiszOpakowanieView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #endregion

        #region Interfejs IPrzepiszOpakowanie

        public event EventHandler Anuluj;
        public event EventHandler<PrzepiszOpakowanieEventArgs> Ok;

        public PrzepiszOpakowanieEventArgs WyswietlaneDane
        {
            set
            {
                wyswietlaneDane = value;

                #region Przepisanie warto�ci do kontrolek

                tbDlugosc.Text = value.Dlugosc.ToString();
                tbDokumentSWA.Text = value.DokumentSWA;
                tbJM.Text = value.Jm;
                tbNazwaOpakowania.Text = value.Nazwa;
                tbSymbolOpakowania.Text = value.Symbol;
                tbSzerokosc.Text = value.Szerokosc.ToString();
                tbWaga.Text = value.Waga.ToString("0.000");
                tbWysokosc.Text = value.Wysokosc.ToString();

                #endregion
            }
        }

        public void UstawWage(string waga)
        {
            tbWaga.Text = waga;
        }

        #endregion

        #region Obs�uga zdarze�

        public void AkcjaZatwierdz()
        {
            wyswietlaneDane.WprowadzonaDlugosc = tbDlugosc.Text;
            wyswietlaneDane.WprowadzonaSzerokosc = tbSzerokosc.Text;
            wyswietlaneDane.WprowadzonaWaga = tbWaga.Text;
            wyswietlaneDane.WprowadzonaWysokosc = tbWysokosc.Text;

            if (Ok != null)
            {
                Ok(this, wyswietlaneDane);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            AkcjaZatwierdz();
        }

        private void btnAnuluj_Click(object sender, EventArgs e)
        {
            if (Anuluj != null)
            {
                Anuluj(this, EventArgs.Empty);
            }
        }

        #endregion
    }
}