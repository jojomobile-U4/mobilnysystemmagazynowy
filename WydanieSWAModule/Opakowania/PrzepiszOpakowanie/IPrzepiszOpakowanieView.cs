using System;
using Common.Base;

namespace WydanieSWAModule.Opakowania.PrzepiszOpakowanie
{
    public interface IPrzepiszOpakowanieView : IViewBase
    {
        event EventHandler Anuluj;
        event EventHandler<PrzepiszOpakowanieEventArgs> Ok;

        PrzepiszOpakowanieEventArgs WyswietlaneDane { set; }
        void AkcjaZatwierdz();
		void UstawWage(string waga);
    }
}
