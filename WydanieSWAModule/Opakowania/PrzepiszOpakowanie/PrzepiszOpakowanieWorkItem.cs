#region

using System;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using WydanieSWAModule.Services;

#endregion

namespace WydanieSWAModule.Opakowania.PrzepiszOpakowanie
{
    public class PrzepiszOpakowanieWorkItem : WorkItemBase
    {
        private PrzepiszOpakowaniePresenter przepiszOpakowaniePresenter;
        private IPrzepiszOpakowanieView przepiszOpakowanieView;
        private IWydanieSWAService wydanieSWAService;


        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ)]
        public void BarCodeRead(object sender, EventArgs e)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                //PrzepiszOpakowaniePresenter presenter = Items.Get<PrzepiszOpakowaniePresenter>(ItemsConstants.PRZEPISZ_OPAKOWANIE_PRESENTER);
                przepiszOpakowaniePresenter = przepiszOpakowaniePresenter ?? new PrzepiszOpakowaniePresenter(przepiszOpakowanieView);
                if (Status == WorkItemStatus.Active &&
                    RootWorkItem.State[StateConstants.BAR_CODE] != null)
                {
                    var odczyt = RootWorkItem.State[StateConstants.BAR_CODE].ToString();

                    if (odczyt != null && odczyt.IndexOf("WAG", 0) == 0)
                    {
                        FunctionsHelper.SetCursorWait();
                        var masa = Services.Get<IWagiService>(true).pobierzOdczytWagi(odczyt);

                        if (masa.statusOperacji.status.Equals(StatusOperacji.ERROR))
                        {
                            ShowMessageBox(masa.statusOperacji.tekst, BLAD, masa.statusOperacji.stosWywolan);
                            return;
                        }
                        else
                        {
                            Services.Get<IWagiService>(true).przeliczNaKg(ref masa);
                            przepiszOpakowaniePresenter.WczytajMase(masa);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ShowMessageBox(ex.Message, "a");
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }


        public void Show(IWorkspace workspace, PrzepiszOpakowanieEventArgs e)
        {
            FunctionsHelper.SetCursorWait();

            #region Inicjacja zmiennych View + Presenter

            przepiszOpakowanieView = przepiszOpakowanieView ?? Items.AddNew<PrzepiszOpakowanieView>();
            przepiszOpakowaniePresenter = przepiszOpakowaniePresenter ?? new PrzepiszOpakowaniePresenter(przepiszOpakowanieView);
            Items.Add(przepiszOpakowaniePresenter);

            #endregion

            #region Pobranie opakowania dla SWA

            przepiszOpakowanieView.WyswietlaneDane = e;

            #endregion

            Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
            workspace.Show(przepiszOpakowanieView);
            FunctionsHelper.SetCursorDefault();
            Activate();
        }

        public bool PrzepiszOpakowanie(PrzepiszOpakowanieEventArgs e)
        {
            wydanieSWAService = Services.Get<IWydanieSWAService>(true);
            var operacji =
                wydanieSWAService.PrzypiszOpakowanieDoSWA(e.IDDokumentuSWA, e.IDOpakowania, e.Wysokosc, e.Szerokosc,
                                                          e.Dlugosc, e.Waga);
            if (operacji.Status.Equals(StatusOperacji.ERROR))
            {
                ShowMessageBox(operacji.Tekst, "Przypisanie opakowania", operacji.StosWywolan);
                Activate();
                return false;
            }
            else
            {
                (Parent as OpakowaniaWorkItem).PobierzOpakowaniaDlaSWA();
                return true;
            }
        }
    }
}