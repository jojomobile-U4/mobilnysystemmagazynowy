#region

using System;

#endregion

namespace WydanieSWAModule.Opakowania.PrzepiszOpakowanie
{
    public class PrzepiszOpakowanieEventArgs : EventArgs
    {
        public long IDDokumentuSWA { get; set; }

        public string WprowadzonaDlugosc { get; set; }

        public string WprowadzonaSzerokosc { get; set; }

        public string WprowadzonaWaga { get; set; }

        public string WprowadzonaWysokosc { get; set; }

        public string DokumentSWA { get; set; }

        public string Symbol { get; set; }

        public string Nazwa { get; set; }

        public string Jm { get; set; }

        public float Waga { get; set; }

        public int Dlugosc { get; set; }

        public int Wysokosc { get; set; }

        public int Szerokosc { get; set; }

        public long IDOpakowania { get; set; }
    }
}