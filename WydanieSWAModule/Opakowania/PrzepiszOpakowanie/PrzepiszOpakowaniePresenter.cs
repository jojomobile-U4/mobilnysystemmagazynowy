#region

using System;
using System.Windows.Forms;
using Common.Base;
using Common.WagiProxy;

#endregion

namespace WydanieSWAModule.Opakowania.PrzepiszOpakowanie
{
    internal class PrzepiszOpakowaniePresenter : PresenterBase
    {
        #region W�asno�ci

        public IPrzepiszOpakowanieView View
        {
            get { return m_view as IPrzepiszOpakowanieView; }
            set { m_view = value; }
        }

        public PrzepiszOpakowanieWorkItem myWorkItem
        {
            get { return (WorkItemBase as PrzepiszOpakowanieWorkItem); }
        }

        #endregion

        public PrzepiszOpakowaniePresenter(IPrzepiszOpakowanieView view)
            : base(view)
        {
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    e.Handled = true;
                    View_Anuluj(this, EventArgs.Empty);
                    break;
                case Keys.Enter:
                    e.Handled = true;
                    View.AkcjaZatwierdz();
                    break;
            }
        }

        protected override void AttachView()
        {
            View.Anuluj -= View_Anuluj;
            View.Anuluj += View_Anuluj;
            View.Ok -= View_Ok;
            View.Ok += View_Ok;
        }


        private void View_Ok(object sender, PrzepiszOpakowanieEventArgs e)
        {
            try
            {
                #region Walidacja d�ugo�ci

                try
                {
                    e.Dlugosc = int.Parse(e.WprowadzonaDlugosc);
                }
                catch (FormatException)
                {
                    throw new Exception("Wprowadzona warto�� d�ugo�ci nie jest liczb� naturaln�");
                }
                if (e.Dlugosc < 0)
                    throw new Exception("D�ugo�� musi by� liczb� wi�ksz� od 0");

                #endregion

                #region Walidacja szeroko�ci

                try
                {
                    e.Szerokosc = int.Parse(e.WprowadzonaSzerokosc);
                }
                catch (FormatException)
                {
                    throw new Exception("Wprowadzona warto�� szeroko�ci nie jest liczb� naturaln�");
                }
                if (e.Szerokosc < 0)
                    throw new Exception("Szeroko�� musi by� liczb� wi�ksz� od 0");

                #endregion

                #region Walidacja wysoko�ci

                try
                {
                    e.Wysokosc = int.Parse(e.WprowadzonaWysokosc);
                }
                catch (FormatException)
                {
                    throw new Exception("Wprowadzona warto�� wysoko�ci nie jest liczb� naturaln�");
                }
                if (e.Wysokosc < 0)
                    throw new Exception("Wysoko�� musi by� liczb� wi�ksz� od 0");

                #endregion

                #region Walidacja wagi

                try
                {
                    e.Waga = float.Parse(e.WprowadzonaWaga);
                }
                catch (FormatException)
                {
                    throw new Exception("Wprowadzona warto�� wagi nie jest liczb�");
                }
                if (e.Waga < 0)
                    throw new Exception("Waga musi by� liczb� wi�ksz� od 0");

                #endregion

                //Wydaje si�, �e jest OK wi�c jedziemy dalej               
                if (myWorkItem.PrzepiszOpakowanie(e))
                    myWorkItem.CloseView(View);
            }
            catch (Exception err)
            {
                //Wy�wietlam komunikat
                MessageBox.Show(err.Message, "Przepisanie opakowania", MessageBoxButtons.OK, MessageBoxIcon.Exclamation,
                                MessageBoxDefaultButton.Button1);
            }
        }

        private void View_Anuluj(object sender, EventArgs e)
        {
            myWorkItem.CloseView(View);
        }

        public void WczytajMase(Masa masa)
        {
            if (masa != null && masa.wartosc > 0)
            {
                this.View.UstawWage(masa.wartosc.ToString("0.000"));
            }
        }
    }
}