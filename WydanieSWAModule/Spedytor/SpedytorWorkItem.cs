#region

using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using WydanieSWAModule.Services;

#endregion

namespace WydanieSWAModule.Spedytor
{
    public class SpedytorWorkItem : WorkItemBase
    {
        #region Zmienne prywatne

        private long IDDokumentuSWA;
        private SpedytorPresenter spedytorPresenter;
        private ISpedytorView spedytorView;
        private IWydanieSWAService wydanieSWAService;

        #endregion

        #region Metody show

        public void Show(IWorkspace workspace, long iDDokumentuSWA, string symbolDokumentu)
        {
            FunctionsHelper.SetCursorWait();
            IDDokumentuSWA = iDDokumentuSWA;

            #region Inicjacja zmiennych View + Presenter

            spedytorView = spedytorView ?? Items.AddNew<SpedytorView>();
            spedytorPresenter = spedytorPresenter ?? new SpedytorPresenter(spedytorView);
            Items.Add(spedytorPresenter);

            #endregion

            wydanieSWAService = Services.Get<IWydanieSWAService>(true);
            var listaSpedytorow = wydanieSWAService.PobierzSpedytorow();
            FunctionsHelper.SetCursorDefault();
            if (listaSpedytorow.StatusOperacji.Status.Equals(StatusOperacji.ERROR))
            {
                ShowMessageBox(listaSpedytorow.StatusOperacji.Tekst, "B��d pobierania listy spedytor�w", listaSpedytorow.StatusOperacji.StosWywolan);
                Activate();
            }
            FunctionsHelper.SetCursorWait();
            spedytorPresenter.WyswietlSpedytorow(listaSpedytorow.Lista);

            //pobranie z bazy przypisanego do dokumentu SWA spedytora
            var przypisanySpedytor = wydanieSWAService.PobierzSpedytoraDlaDokumentu(IDDokumentuSWA);
            FunctionsHelper.SetCursorDefault();
            if (przypisanySpedytor.StatusOperacji.Status.Equals(StatusOperacji.ERROR))
            {
                ShowMessageBox(przypisanySpedytor.StatusOperacji.Tekst, "B��d pobierania spedytora", przypisanySpedytor.StatusOperacji.StosWywolan);
                Activate();
            }
            //utworzenie obiektu spedytora
            var spedytorDokumentu = new DataModel.Spedytor();
            if (przypisanySpedytor.Lista.Length > 0)
            {
                spedytorDokumentu = przypisanySpedytor.Lista[0];
            }
            spedytorPresenter.WyswietlSpedytoraDlaDokumentu(spedytorDokumentu);

            spedytorView.SymbolDokumentu = symbolDokumentu;

            workspace.Show(spedytorView);
            Activate();
        }

        #endregion

        #region Obs�uga logiki biznesowej

        public bool UsunSpedytora()
        {
            FunctionsHelper.SetCursorWait();
            wydanieSWAService = Services.Get<IWydanieSWAService>(true);
            var operacji = wydanieSWAService.UsunSpedytora(IDDokumentuSWA);
            FunctionsHelper.SetCursorDefault();
            if (operacji.Status.Equals(StatusOperacji.SUCCESS))
                return true;
            else
            {
                ShowMessageBox(operacji.Tekst, "Usuni�cie spedytora", operacji.StosWywolan);
                Activate();
            }
            return false;
        }

        public bool PrzypiszSpedytora(long idSpedytora, string symbolSpedytora, string nazwaSpedytora)
        {
            FunctionsHelper.SetCursorWait();
            wydanieSWAService = Services.Get<IWydanieSWAService>(true);
            var operacji = wydanieSWAService.PrzypiszSpedytora(idSpedytora, IDDokumentuSWA, nazwaSpedytora, symbolSpedytora);
            FunctionsHelper.SetCursorDefault();
            if (operacji.Status.Equals(StatusOperacji.SUCCESS))
                return true;
            else
            {
                ShowMessageBox(operacji.Tekst, BLAD, operacji.StosWywolan);
                Activate();
            }
            return false;
        }

        public bool UruchomRaport(string nazwaFormularza)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                Services.Get<IWydanieSWAService>(true).UruchomRaport(nazwaFormularza, IDDokumentuSWA);
                if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, BLAD, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                    return false;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }

            return true;
        }

        #endregion
    }
}