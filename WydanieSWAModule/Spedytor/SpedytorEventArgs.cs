#region

using System;

#endregion

namespace WydanieSWAModule.Spedytor
{
    public class SpedytorEventArgs : EventArgs
    {
        #region Pola prywatne

        #endregion

        #region W�asno�ci

        public long ID { get; set; }

        public string Nazwa { get; set; }

        public string Symbol { get; set; }

        #endregion
    }
}