#region

using System;
using Common.Base;

#endregion

namespace WydanieSWAModule.Spedytor
{
    public interface ISpedytorView : IViewBase
    {
        #region Zdarzenia

        event EventHandler Usun;
        event EventHandler<SpedytorEventArgs> Przypisz;
        event EventHandler Powrot;

        #endregion

        #region W�asno�ci

        string SymbolDokumentu { set; }
        string Nazwa { set; }
        string Symbol { set; }

        DataModel.Spedytor[] Spedytorzy { set; }

        DataModel.Spedytor SpedytorDokumentu { set; }

        #endregion

        #region Metody

        void PrzypiszSpedytora();

        #endregion
    }
}