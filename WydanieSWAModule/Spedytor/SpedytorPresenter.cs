#region

using System;
using System.Windows.Forms;
using Common.Base;

#endregion

namespace WydanieSWAModule.Spedytor
{
    public class SpedytorPresenter : PresenterBase
    {
        public const string NAZWA_FORMULARZA = "WYDANIE - SPEDYTOR";
        private const string POMYSLNE_WYWOLANIE_RAPORTU = "Wydruk zosta� wys�any na drukark�.";

        public SpedytorPresenter(IViewBase view) : base(view)
        {
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    e.Handled = true;
                    View_Powrot(this, EventArgs.Empty);
                    break;
                case Keys.D1:
                    e.Handled = true;
                    View.PrzypiszSpedytora();
                    break;
                case Keys.D2:
                    e.Handled = true;
                    View_Usun(this, EventArgs.Empty);
                    break;
                case Keys.D:
                    e.Handled = true;
                    uruchomRaport();
                    break;
            }
        }

        protected override void AttachView()
        {
            View.Powrot -= View_Powrot;
            View.Powrot += View_Powrot;
            View.Usun -= View_Usun;
            View.Usun += View_Usun;
            View.Przypisz -= View_Przypisz;
            View.Przypisz += View_Przypisz;
        }

        private void View_Przypisz(object sender, SpedytorEventArgs e)
        {
            if (myWorkItem.PrzypiszSpedytora(e.ID, e.Symbol, e.Nazwa))
            {
                View.Nazwa = e.Nazwa;
                View.Symbol = e.Symbol;
            }
        }

        private void View_Usun(object sender, EventArgs e)
        {
            if (MessageBox.Show("Czy na pewno chcesz usun�� spedytora dla dokumentu?", "Spedytor", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                if (myWorkItem.UsunSpedytora())
                {
                    View.Nazwa = String.Empty;
                    View.Symbol = String.Empty;
                }
            }
        }

        private void View_Powrot(object sender, EventArgs e)
        {
            myWorkItem.CloseView(View);
        }

        private void uruchomRaport()
        {
            if (myWorkItem.UruchomRaport(NAZWA_FORMULARZA))
            {
                MessageBox.Show(POMYSLNE_WYWOLANIE_RAPORTU);
                myWorkItem.Activate();
            }
        }

        public void WyswietlSpedytorow(DataModel.Spedytor[] spedytorzy)
        {
            View.Spedytorzy = spedytorzy;
        }

        public void WyswietlSpedytoraDlaDokumentu(DataModel.Spedytor spedytor)
        {
            if (spedytor != null)
                View.SpedytorDokumentu = spedytor;
        }

        #region W�asno�ci

        public ISpedytorView View
        {
            get { return m_view as ISpedytorView; }
            set { m_view = value; }
        }

        public SpedytorWorkItem myWorkItem
        {
            get { return (WorkItemBase as SpedytorWorkItem); }
        }

        #endregion
    }
}