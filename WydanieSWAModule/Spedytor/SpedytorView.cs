#region

using System;
using System.Windows.Forms;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace WydanieSWAModule.Spedytor
{
    [SmartPart]
    public partial class SpedytorView : ViewBase, ISpedytorView
    {
        #region Konstruktory

        public SpedytorView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #endregion

        #region Implementacja interfejsu

        public event EventHandler Usun;
        public event EventHandler<SpedytorEventArgs> Przypisz;
        public event EventHandler Powrot;

        public string SymbolDokumentu
        {
            set { tbDokumentSWA.Text = value; }
        }

        public string Nazwa
        {
            set { tbNazwa.Text = value; }
        }

        public string Symbol
        {
            set { tbSymbol.Text = value; }
        }

        public DataModel.Spedytor[] Spedytorzy
        {
            set
            {
                if (value != null)
                {
                    lsSpedytorzy.Items.Clear();
                    var lp = 1;
                    ListViewItem listViewItem;
                    foreach (var spedytor in value)
                    {
                        listViewItem = lsSpedytorzy.Items.Add(new ListViewItem(lp.ToString()));
                        listViewItem.SubItems.Add(spedytor.Symbol);
                        listViewItem.SubItems.Add(spedytor.Nazwa);
                        listViewItem.Tag = spedytor.Id;
                        lp++;
                    }
                }
            }
        }

        public DataModel.Spedytor SpedytorDokumentu
        {
            set
            {
                tbNazwa.Text = value.Nazwa;
                tbSymbol.Text = value.Symbol;
            }
        }

        #endregion

        #region Obs�uga zdarze�

        public void PrzypiszSpedytora()
        {
            if (CzyZaznaczonyElementNaLiscie() && Przypisz != null)
            {
                var spedytorEventArgs = new SpedytorEventArgs();
                spedytorEventArgs.ID = long.Parse(lsSpedytorzy.Items[lsSpedytorzy.SelectedIndices[0]].Tag.ToString());
                spedytorEventArgs.Symbol = lsSpedytorzy.Items[lsSpedytorzy.SelectedIndices[0]].SubItems[1].Text;
                spedytorEventArgs.Nazwa = lsSpedytorzy.Items[lsSpedytorzy.SelectedIndices[0]].SubItems[2].Text;
                Przypisz(this, spedytorEventArgs);
            }
        }

        private void btnPrzypisz_Click(object sender, EventArgs e)
        {
            PrzypiszSpedytora();
        }

        private void btnUsun_Click(object sender, EventArgs e)
        {
            Usun(this, EventArgs.Empty);
        }

        private void btnPowrot_Click(object sender, EventArgs e)
        {
            if (Powrot != null)
            {
                Powrot(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Metody pomocnicze

        /// <summary>
        /// Sprawdza czy zosta� zaznaczony element na li�cie
        /// </summary>
        /// <returns></returns>
        private bool CzyZaznaczonyElementNaLiscie()
        {
            return
                lsSpedytorzy.Items.Count > 0 &&
                lsSpedytorzy.SelectedIndices.Count > 0 &&
                lsSpedytorzy.Items[lsSpedytorzy.SelectedIndices[0]].Tag != null;
        }

        #endregion
    }
}