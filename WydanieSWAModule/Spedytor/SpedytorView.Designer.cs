using Common.Components;

namespace WydanieSWAModule.Spedytor
{
    partial class SpedytorView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbDokumentSWA = new System.Windows.Forms.TextBox();
            this.lbDokumentSWA = new System.Windows.Forms.Label();
            this.tbNazwa = new System.Windows.Forms.TextBox();
            this.lblNazwa = new System.Windows.Forms.Label();
            this.tbSymbol = new System.Windows.Forms.TextBox();
            this.lblSymbol = new System.Windows.Forms.Label();
            this.lsSpedytorzy = new Common.Components.MSMListView();
            this.chLP = new System.Windows.Forms.ColumnHeader();
            this.chSymbol = new System.Windows.Forms.ColumnHeader();
            this.chNazwa = new System.Windows.Forms.ColumnHeader();
            this.lblDostepniSpedytorzy = new System.Windows.Forms.Label();
            this.btnPowrot = new System.Windows.Forms.Button();
            this.btnUsun = new System.Windows.Forms.Button();
            this.btnPrzypisz = new System.Windows.Forms.Button();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnPowrot);
            this.pnlNavigation.Controls.Add(this.btnUsun);
            this.pnlNavigation.Controls.Add(this.btnPrzypisz);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 52);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 6;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "WYDANIE - SPEDYTOR";
            // 
            // tbDokumentSWA
            // 
            this.tbDokumentSWA.Location = new System.Drawing.Point(101, 23);
            this.tbDokumentSWA.Name = "tbDokumentSWA";
            this.tbDokumentSWA.ReadOnly = true;
            this.tbDokumentSWA.Size = new System.Drawing.Size(135, 21);
            this.tbDokumentSWA.TabIndex = 0;
            this.tbDokumentSWA.TabStop = false;
            // 
            // lbDokumentSWA
            // 
            this.lbDokumentSWA.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbDokumentSWA.Location = new System.Drawing.Point(1, 25);
            this.lbDokumentSWA.Name = "lbDokumentSWA";
            this.lbDokumentSWA.Size = new System.Drawing.Size(100, 21);
            this.lbDokumentSWA.Text = "Dokument SWA:";
            // 
            // tbNazwa
            // 
            this.tbNazwa.Location = new System.Drawing.Point(101, 70);
            this.tbNazwa.Name = "tbNazwa";
            this.tbNazwa.ReadOnly = true;
            this.tbNazwa.Size = new System.Drawing.Size(135, 21);
            this.tbNazwa.TabIndex = 2;
            this.tbNazwa.TabStop = false;
            // 
            // lblNazwa
            // 
            this.lblNazwa.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblNazwa.Location = new System.Drawing.Point(1, 70);
            this.lblNazwa.Name = "lblNazwa";
            this.lblNazwa.Size = new System.Drawing.Size(100, 21);
            this.lblNazwa.Text = "Nazwa:";
            // 
            // tbSymbol
            // 
            this.tbSymbol.Location = new System.Drawing.Point(101, 47);
            this.tbSymbol.Name = "tbSymbol";
            this.tbSymbol.ReadOnly = true;
            this.tbSymbol.Size = new System.Drawing.Size(135, 21);
            this.tbSymbol.TabIndex = 1;
            this.tbSymbol.TabStop = false;
            // 
            // lblSymbol
            // 
            this.lblSymbol.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblSymbol.Location = new System.Drawing.Point(1, 48);
            this.lblSymbol.Name = "lblSymbol";
            this.lblSymbol.Size = new System.Drawing.Size(100, 21);
            this.lblSymbol.Text = "Symbol:";
            // 
            // lsSpedytorzy
            // 
            this.lsSpedytorzy.Columns.Add(this.chLP);
            this.lsSpedytorzy.Columns.Add(this.chSymbol);
            this.lsSpedytorzy.Columns.Add(this.chNazwa);
            this.lsSpedytorzy.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lsSpedytorzy.FullRowSelect = true;
            this.lsSpedytorzy.Location = new System.Drawing.Point(3, 115);
            this.lsSpedytorzy.Name = "lsSpedytorzy";
            this.lsSpedytorzy.Size = new System.Drawing.Size(233, 149);
            this.lsSpedytorzy.TabIndex = 3;
            this.lsSpedytorzy.View = System.Windows.Forms.View.Details;
            // 
            // chLP
            // 
            this.chLP.Text = "LP";
            this.chLP.Width = 30;
            // 
            // chSymbol
            // 
            this.chSymbol.Text = "Symbol";
            this.chSymbol.Width = 80;
            // 
            // chNazwa
            // 
            this.chNazwa.Text = "Nazwa";
            this.chNazwa.Width = 100;
            // 
            // lblDostepniSpedytorzy
            // 
            this.lblDostepniSpedytorzy.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblDostepniSpedytorzy.Location = new System.Drawing.Point(-1, 96);
            this.lblDostepniSpedytorzy.Name = "lblDostepniSpedytorzy";
            this.lblDostepniSpedytorzy.Size = new System.Drawing.Size(241, 16);
            this.lblDostepniSpedytorzy.Text = "DOST�PNI SPEDYTORZY";
            this.lblDostepniSpedytorzy.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnPowrot
            // 
            this.btnPowrot.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPowrot.Location = new System.Drawing.Point(160, 24);
            this.btnPowrot.Name = "btnPowrot";
            this.btnPowrot.Size = new System.Drawing.Size(76, 22);
            this.btnPowrot.TabIndex = 6;
            this.btnPowrot.TabStop = false;
            this.btnPowrot.Text = "&Esc Powr�t";
            this.btnPowrot.Click += new System.EventHandler(this.btnPowrot_Click);
            // 
            // btnUsun
            // 
            this.btnUsun.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnUsun.Location = new System.Drawing.Point(77, 24);
            this.btnUsun.Name = "btnUsun";
            this.btnUsun.Size = new System.Drawing.Size(80, 22);
            this.btnUsun.TabIndex = 5;
            this.btnUsun.TabStop = false;
            this.btnUsun.Text = "&2 Usu�";
            this.btnUsun.Click += new System.EventHandler(this.btnUsun_Click);
            // 
            // btnPrzypisz
            // 
            this.btnPrzypisz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPrzypisz.Location = new System.Drawing.Point(3, 24);
            this.btnPrzypisz.Name = "btnPrzypisz";
            this.btnPrzypisz.Size = new System.Drawing.Size(71, 22);
            this.btnPrzypisz.TabIndex = 4;
            this.btnPrzypisz.TabStop = false;
            this.btnPrzypisz.Text = "&1 Przypisz";
            this.btnPrzypisz.Click += new System.EventHandler(this.btnPrzypisz_Click);
            // 
            // SpedytorView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lblDostepniSpedytorzy);
            this.Controls.Add(this.lsSpedytorzy);
            this.Controls.Add(this.tbSymbol);
            this.Controls.Add(this.lblSymbol);
            this.Controls.Add(this.tbNazwa);
            this.Controls.Add(this.lblNazwa);
            this.Controls.Add(this.lbTytul);
            this.Controls.Add(this.tbDokumentSWA);
            this.Controls.Add(this.lbDokumentSWA);
            this.Name = "SpedytorView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbDokumentSWA, 0);
            this.Controls.SetChildIndex(this.tbDokumentSWA, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lblNazwa, 0);
            this.Controls.SetChildIndex(this.tbNazwa, 0);
            this.Controls.SetChildIndex(this.lblSymbol, 0);
            this.Controls.SetChildIndex(this.tbSymbol, 0);
            this.Controls.SetChildIndex(this.lsSpedytorzy, 0);
            this.Controls.SetChildIndex(this.lblDostepniSpedytorzy, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MSMLabel lbTytul;
        private System.Windows.Forms.TextBox tbDokumentSWA;
        private System.Windows.Forms.Label lbDokumentSWA;
        private System.Windows.Forms.TextBox tbNazwa;
        private System.Windows.Forms.Label lblNazwa;
        private System.Windows.Forms.TextBox tbSymbol;
        private System.Windows.Forms.Label lblSymbol;
        private MSMListView lsSpedytorzy;
        private System.Windows.Forms.ColumnHeader chLP;
        private System.Windows.Forms.ColumnHeader chSymbol;
        private System.Windows.Forms.ColumnHeader chNazwa;
        private System.Windows.Forms.Label lblDostepniSpedytorzy;
        private System.Windows.Forms.Button btnPowrot;
        private System.Windows.Forms.Button btnUsun;
        private System.Windows.Forms.Button btnPrzypisz;
    }
}
