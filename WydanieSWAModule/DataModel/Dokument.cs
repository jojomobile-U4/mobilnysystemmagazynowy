using System;
using System.Collections.Generic;
using System.Text;
using Common.DataModel;
using Common;

namespace WydanieSWAModule.DataModel
{
	public class Dokument
	{
		#region Private Fields
		
		private int m_LiczbaDokumentowMR;
		private Naglowek m_Naglowek;
		private List<PozycjaDokumentu> m_Pozycje;
		private StatusOperacji m_StatusOperacji;

		#endregion 
		#region Properties

		public StatusOperacji StatusOperacji
		{
			get { return m_StatusOperacji; }
			set { m_StatusOperacji = value; }
		}

		public List<PozycjaDokumentu> Pozycje
		{
			get { return m_Pozycje; }
			set { m_Pozycje = value; }
		}

		public Naglowek Naglowek
		{
			get { return m_Naglowek; }
			set { m_Naglowek = value; }
		}

		public int LiczbaDokumentowMR
		{
			get { return m_LiczbaDokumentowMR; }
			set { m_LiczbaDokumentowMR = value; }
		}

		#endregion
		#region Constructors

		public Dokument()
		{
			m_Naglowek = new Naglowek();
			m_Pozycje = new List<PozycjaDokumentu>();
			m_StatusOperacji = new StatusOperacji(StatusOperacji.SUCCESS);
		}

		public Dokument(WydanieSWAProxy.Dokument dokument):this()
		{
			if (dokument != null)
			{
                m_LiczbaDokumentowMR = dokument.naglowek.iloscPozycji;
				m_Naglowek = new Naglowek(dokument.naglowek);
				if (dokument.pozycje != null)
				{
					for (int i = 0; i < dokument.pozycje.Length; i++)
					{
						m_Pozycje.Add(new PozycjaDokumentu(dokument.pozycje[i]));
					}
				}
				m_StatusOperacji = new StatusOperacji(dokument.statusOperacji.status, dokument.statusOperacji.tekst);
			}
			else
			{
				m_StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, DataModelConstants.NO_DATA);
			}
		}

		#endregion
	}
}
