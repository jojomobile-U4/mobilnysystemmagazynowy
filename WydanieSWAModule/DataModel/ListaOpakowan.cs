using System;
using System.Collections.Generic;
using System.Text;
using Common.DataModel;
using Common;

namespace WydanieSWAModule.DataModel
{
	public class ListaOpakowan
	{
		#region Private Fields

		private Opakowanie[] m_Lista;
		private StatusOperacji m_StatusOperacji;

		#endregion
		#region Properties

		public StatusOperacji StatusOperacji
		{
			get { return m_StatusOperacji; }
			set { m_StatusOperacji = value; }
		}

		public Opakowanie[] Lista
		{
			get { return m_Lista; }
			set { m_Lista = value; }
		}

		#endregion
		#region Constructors

		public ListaOpakowan()
		{
			m_Lista = new Opakowanie[0];
			m_StatusOperacji = new StatusOperacji(StatusOperacji.SUCCESS);
		}

		public ListaOpakowan(WydanieSWAProxy.ListaOpakowan lista)
			: this()
		{
			if (lista != null)
			{
				m_StatusOperacji = new StatusOperacji(lista.statusOperacji.status, lista.statusOperacji.tekst);
				if (m_StatusOperacji.Status.Equals(StatusOperacji.SUCCESS) &&
					lista.lista != null)
				{
					m_Lista = new Opakowanie[lista.lista.Length];
					for (int i = 0; i < m_Lista.Length; i++)
					{
						m_Lista[i] = new Opakowanie(lista.lista[i]);
					}
				}
			}
			else
			{
				m_StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, DataModelConstants.NO_DATA);
			}
		}

		#endregion
	}
}
