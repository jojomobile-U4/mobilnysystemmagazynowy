using System;
using System.Collections.Generic;
using System.Text;

namespace WydanieSWAModule.DataModel
{
	public class Opakowanie
	{
		#region Private Fields
		private int m_Dlugosc;
		private long m_Id;
		private string m_Jm;
		private string m_Nazwa;
		private string m_Symbol;
		private int m_Szerokosc;
		private float m_Waga;
		private int m_Wysokosc;

		#endregion
		#region Properties

		public int Wysokosc
		{
			get { return m_Wysokosc; }
			set { m_Wysokosc = value; }
		}

		public float Waga
		{
			get { return m_Waga; }
			set { m_Waga = value; }
		}

		public int Szerokosc
		{
			get { return m_Szerokosc; }
			set { m_Szerokosc = value; }
		}

		public string Symbol
		{
			get { return m_Symbol; }
			set { m_Symbol = value; }
		}

		public string Nazwa
		{
			get { return m_Nazwa; }
			set { m_Nazwa = value; }
		}

		public string Jm
		{
			get { return m_Jm; }
			set { m_Jm = value; }
		}

		public long Id
		{
			get { return m_Id; }
			set { m_Id = value; }
		}

		public int Dlugosc
		{
			get { return m_Dlugosc; }
			set { m_Dlugosc = value; }
		}

		#endregion
		#region Constructors

		public Opakowanie()
		{

		}

		public Opakowanie(WydanieSWAProxy.Opakowanie opakowanie):this()
		{
			if (opakowanie != null)
			{
				m_Dlugosc = opakowanie.dlugosc;
				m_Id = opakowanie.id;
				m_Jm = opakowanie.jm;
				m_Nazwa = opakowanie.nazwa;
				m_Symbol = opakowanie.symbol;
				m_Szerokosc = opakowanie.szerokosc;
				m_Waga = opakowanie.waga;
				m_Wysokosc = opakowanie.wysokosc;
			}
		}

		#endregion
	}
}
