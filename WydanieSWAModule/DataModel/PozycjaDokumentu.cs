using System;
using System.Collections.Generic;
using System.Text;

namespace WydanieSWAModule.DataModel
{
	public class PozycjaDokumentu
	{
		#region Private Members

		private long m_IdDokumentuMR;
		private string m_Potwierdzony;
		private string m_Strefa;
		private string m_SymbolDokumentuMR;
		private string m_Zatwierdzony;
		private int m_LiczbaPozycjiMR;

		#endregion
		#region Properties

		public int LiczbaPozycjiMR
		{
			get { return m_LiczbaPozycjiMR; }
			set { m_LiczbaPozycjiMR = value; }
		}

		public long IdDokumentuMR
		{
			get { return m_IdDokumentuMR; }
			set { m_IdDokumentuMR = value; }
		}

		public string Zatwierdzony
		{
			get { return m_Zatwierdzony; }
			set { m_Zatwierdzony = value; }
		}

		public string SymbolDokumentuMR
		{
			get { return m_SymbolDokumentuMR; }
			set { m_SymbolDokumentuMR = value; }
		}

		public string Strefa
		{
			get { return m_Strefa; }
			set { m_Strefa = value; }
		}

		public string Potwierdzony
		{
			get { return m_Potwierdzony; }
			set { m_Potwierdzony = value; }
		}

		#endregion
		#region Constructors

		public PozycjaDokumentu()
		{

		}

		public PozycjaDokumentu(WydanieSWAProxy.Pozycja pozycja):this()
		{
			if (pozycja != null)
			{
				m_IdDokumentuMR = pozycja.idDokumentuMR;
				m_Potwierdzony = pozycja.potwierdzony;
				m_Strefa = pozycja.strefa;
				m_SymbolDokumentuMR = pozycja.symbolDokumentuMR;
				m_Zatwierdzony = pozycja.zatwierdzony;
                m_LiczbaPozycjiMR = pozycja.iloscPozycjiDokumentuMR;
			}
		}

		#endregion
	}
}
