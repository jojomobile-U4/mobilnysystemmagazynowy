using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using Common.DataModel;
using Common;

namespace WydanieSWAModule.DataModel
{
	public class ListaNaglowkow
	{
		#region Private Fields

		private long m_LiczbaNaglowkow;
		private List<Naglowek> m_Lista;
		private StatusOperacji m_StatusOperacji;

		#endregion
		#region Properties

		public StatusOperacji StatusOperacji
		{
			get { return m_StatusOperacji; }
			set { m_StatusOperacji = value; }
		}

		public List<Naglowek> Lista
		{
			get { return m_Lista; }
		}

		public long LiczbaNaglowkow
		{
			get { return m_LiczbaNaglowkow; }
			set { m_LiczbaNaglowkow = value; }
		}

		#endregion
		#region Constructors

		public ListaNaglowkow()
		{
			m_Lista = new List<Naglowek>();
			m_StatusOperacji = new StatusOperacji(StatusOperacji.SUCCESS);
		}

		public ListaNaglowkow(WydanieSWAProxy.ListaNaglowkow listaNaglowkow):this()
		{
			if (listaNaglowkow != null)
			{
				m_LiczbaNaglowkow = listaNaglowkow.iloscNaglowkow;
				if (listaNaglowkow.lista != null)
				{
					foreach (WydanieSWAProxy.Naglowek naglowek in listaNaglowkow.lista)
					{
						m_Lista.Add(new Naglowek(naglowek));
					}
				}
				m_StatusOperacji = new StatusOperacji(listaNaglowkow.statusOperacji.status, listaNaglowkow.statusOperacji.tekst);
			}
			else
			{
				m_StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, DataModelConstants.NO_DATA);
			}
		}

		#endregion
	}
}
