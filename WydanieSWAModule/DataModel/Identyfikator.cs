using Common;
using Common.DataModel;

namespace WydanieSWAModule.DataModel
{
    public class Identyfikator
    {
        #region Zmienne prywatne

        private long m_id;
        private StatusOperacji m_StatusOperacji;

        #endregion
        #region W�asno�ci

        public long Id
        {
            get { return m_id; }
            set { m_id = value; }
        }

        public StatusOperacji StatusOperacji
        {
            get { return m_StatusOperacji; }
            set { m_StatusOperacji = value; }
        }

        #endregion
        #region Konstruktory

        public Identyfikator()
        {
            m_StatusOperacji = new StatusOperacji(StatusOperacji.SUCCESS);
        }
        
        public Identyfikator(WydanieSWAProxy.Identyfikator identyfikator):this()
        {
            if (identyfikator != null)
            {
                m_id = identyfikator.id;
                m_StatusOperacji = new StatusOperacji(identyfikator.statusOperacji.status, identyfikator.statusOperacji.tekst);
            }
            else
            {
                m_StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, DataModelConstants.NO_DATA);
            }
        }

        #endregion

    }
}
