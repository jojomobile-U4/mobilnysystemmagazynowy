using System;
using Common.DataModel;

namespace WydanieSWAModule.DataModel
{
    public class Naglowek
    {
        #region Constants

        private const string WSKAZNIK_PORZUCONY = "P";
        private const string WSKAZNIK_REZERWOWANY = "R";

        #endregion
        #region Private Fields

        private DateTime m_DataWystawienia;
        private long m_Id;
        private string m_NazwaOdbiorcy;
        private string m_Wskaznik;
        private string m_Priorytet;
        private string m_Status;
        private string m_SymbolDokumentu;
        private string m_SymbolOdbiorcy;
        private string m_PoleWydania;

        #endregion
        #region Properties

        public string SymbolOdbiorcy
        {
            get { return m_SymbolOdbiorcy; }
            set { m_SymbolOdbiorcy = value; }
        }

        public string SymbolDokumentu
        {
            get { return m_SymbolDokumentu; }
            set { m_SymbolDokumentu = value; }
        }

        public string Status
        {
            get { return m_Status; }
            set { m_Status = value; }
        }

        public string Priorytet
        {
            get { return m_Priorytet; }
            set { m_Priorytet = value; }
        }

        public string Wskaznik
        {
            get { return m_Wskaznik; }
            set { m_Wskaznik = value; }
        }

        public string NazwaOdbiorcy
        {
            get { return m_NazwaOdbiorcy; }
            set { m_NazwaOdbiorcy = value; }
        }

        public long Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        public DateTime DataWystawienia
        {
            get { return m_DataWystawienia; }
            set { m_DataWystawienia = value; }
        }

        public string PoleWydania
        {
            get { return m_PoleWydania; }
            set { m_PoleWydania = value; }
        }

        #endregion
        #region Constructors

        public Naglowek()
        {

        }

        public Naglowek(WydanieSWAProxy.Naglowek naglowek)
            : this()
        {
            if (naglowek != null)
            {
                m_DataWystawienia = naglowek.dataWystawienia;
                m_Id = naglowek.id;
                m_Priorytet = naglowek.priorytet;
                m_Status = naglowek.status;
                m_SymbolDokumentu = naglowek.symbolDokumentu;
                m_Wskaznik = OkreslWskaznik(naglowek);
                m_PoleWydania = naglowek.poleWydania;

                if (naglowek.symbolOdbiorcy != null)
                {
                    m_SymbolOdbiorcy = naglowek.symbolOdbiorcy;
                    m_NazwaOdbiorcy = naglowek.nazwaOdbiorcy;
                }
                else
                {
                    m_SymbolOdbiorcy = naglowek.magaId.ToString();
                    m_NazwaOdbiorcy = naglowek.nazwaMagazynu;
                }
            }
        }

        #endregion
        #region Private Methods

        private string OkreslWskaznik(WydanieSWAModule.WydanieSWAProxy.Naglowek naglowek)
        {
            if (DBBool.IsDBBool(naglowek.zarezerwowany) &&
                DBBool.ToBool(naglowek.zarezerwowany))
            {
                return WSKAZNIK_REZERWOWANY;
            }
            if (DBBool.IsDBBool(naglowek.porzucony) &&
                DBBool.ToBool(naglowek.porzucony))
            {
                return WSKAZNIK_PORZUCONY;
            }

            return string.Empty;
        }

        #endregion
    }
}
