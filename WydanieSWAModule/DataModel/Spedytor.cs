using System;
using System.Collections.Generic;
using System.Text;

namespace WydanieSWAModule.DataModel
{
	public class Spedytor
	{
		#region Private Fields

		private long m_Id;
		private string m_Nazwa;
		private string m_Symbol;	

		#endregion
		#region Properties

		public string Symbol
		{
			get { return m_Symbol; }
			set { m_Symbol = value; }
		}

		public string Nazwa
		{
			get { return m_Nazwa; }
			set { m_Nazwa = value; }
		}

		public long Id
		{
			get { return m_Id; }
			set { m_Id = value; }
		}

		#endregion
		#region Constructors

		public Spedytor()
		{

		}

		public Spedytor(WydanieSWAProxy.Spedytor spedytor):this()
		{
			if (spedytor != null)
			{
				m_Id = spedytor.id;
				m_Nazwa = spedytor.nazwa;
				m_Symbol = spedytor.symbol;
			}
		}

		#endregion
	}
}
