#region

using System;
using Common;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using WydanieMRModule.EdycjaMR;
using WydanieMRModule.ListaMR;
using WydanieMRModule.Services;
using WydanieSWAModule.RealizacjaSWA;

#endregion

namespace WydanieSWAModule.PrzegladMR
{
    public class PrzegladMRWorkItem : WorkItemBase
    {
        #region Events

        public event EventHandler<DataEventArgs<long>> ZatwierdzonoDokumentMR;

        #endregion

        #region Constants

        private const string BRAK_DOKUMENTU = "Nie znaleziono dokumentu MR o podanym identyfikatorze.";

        #endregion

        public void Show(long idDokumentuMR)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                InitializeMRModule();

                var listaDokumentowMR =
                    Services.Get<IWydanieMRService>(true).PobierzListeDokumentowMR(idDokumentuMR, null, null, null, null, null, null);
                if (listaDokumentowMR == null ||
                    listaDokumentowMR.Lista.Count == 0)
                {
                    ShowMessageBox(BRAK_DOKUMENTU, BLAD);
                    RootWorkItem.WorkItems.Get<RealizacjaSWAWorkItem>(WorkItemsConstants.REALIZACJA_SWA_WORKITEM).Activate();
                    return;
                }

                var listaMRWorkItem = RootWorkItem.WorkItems.Get<ListaMRWorkItem>(WorkItemsConstants.LISTA_MR_WORKITEM);

                listaMRWorkItem.AktywnaListaMR = false;

                if (listaDokumentowMR.Lista[0].Naglowek.Zatwierdzony.Equals("T"))
                {
                    listaMRWorkItem.PobierzDokument(listaDokumentowMR.Lista[0].Naglowek);
                }
                else
                {
                    listaMRWorkItem.RezerwujIPobierzDokument(listaDokumentowMR.Lista[0].Naglowek, false);
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        private void InitializeMRModule()
        {
            //TODO : Rozdzielenie modu�u WydanieMRModule od WydanieSWAModule
            // tu nale�y odwo�ywa� si� do modu�u WydanieMRModule za pomoc� mechanizm�w CAB'a

            new WydanieMRModule.ModuleInitializer(RootWorkItem).Initialize();
            var edycjaMRWorkItem = RootWorkItem.WorkItems.Get<EdycjaMRWorkItem>(WorkItemsConstants.EDYCJA_MR_WORKITEM);
            edycjaMRWorkItem.ZatwierdzonoDokumentMr -= OnZatwierdzonoDokumentMR;
            edycjaMRWorkItem.ZatwierdzonoDokumentMr += OnZatwierdzonoDokumentMR;

            if (Services.Get<IWydanieMRService>(true) == null)
            {
                new WydanieMRModule.ModuleInitializer(RootWorkItem).Load();
            }
        }

        protected void OnZatwierdzonoDokumentMR(object sender, DataEventArgs<long> e)
        {
            if (ZatwierdzonoDokumentMR != null)
            {
                ZatwierdzonoDokumentMR(sender, e);
            }
        }
    }
}