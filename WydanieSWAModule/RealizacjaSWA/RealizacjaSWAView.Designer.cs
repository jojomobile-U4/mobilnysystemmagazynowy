
using Common.Components;

namespace WydanieSWAModule.RealizacjaSWA
{
	partial class RealizacjaSWAView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
        private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbDokumentSWA = new System.Windows.Forms.TextBox();
            this.lbDokumentSWA = new System.Windows.Forms.Label();
            this.tbStatus = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lsDokumentyMR = new Common.Components.MSMListView();
            this.chLP = new System.Windows.Forms.ColumnHeader();
            this.chDokument = new System.Windows.Forms.ColumnHeader();
            this.chPot = new System.Windows.Forms.ColumnHeader();
            this.chZat = new System.Windows.Forms.ColumnHeader();
            this.chStrefa = new System.Windows.Forms.ColumnHeader();
            this.btnSzukaj = new System.Windows.Forms.Button();
            this.btnPozycjeMR = new System.Windows.Forms.Button();
            this.btnPotwierdz = new System.Windows.Forms.Button();
            this.btnOpakowania = new System.Windows.Forms.Button();
            this.btnPowrot = new System.Windows.Forms.Button();
            this.lblLiczbaMR = new System.Windows.Forms.Label();
            this.tbLiczbaMR = new System.Windows.Forms.TextBox();
            this.lblLiczbaPozycjiMR = new System.Windows.Forms.Label();
            this.tbLiczbaPozycjiMR = new System.Windows.Forms.TextBox();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnOpakowania);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 52);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 59;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "WYDANIE - REALIZACJA SWA";
            // 
            // tbDokumentSWA
            // 
            this.tbDokumentSWA.Location = new System.Drawing.Point(90, 19);
            this.tbDokumentSWA.Name = "tbDokumentSWA";
            this.tbDokumentSWA.ReadOnly = true;
            this.tbDokumentSWA.Size = new System.Drawing.Size(147, 21);
            this.tbDokumentSWA.TabIndex = 30;
            this.tbDokumentSWA.TabStop = false;
            // 
            // lbDokumentSWA
            // 
            this.lbDokumentSWA.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbDokumentSWA.Location = new System.Drawing.Point(3, 21);
            this.lbDokumentSWA.Name = "lbDokumentSWA";
            this.lbDokumentSWA.Size = new System.Drawing.Size(100, 21);
            this.lbDokumentSWA.Text = "Dokument SWA:";
            // 
            // tbStatus
            // 
            this.tbStatus.Location = new System.Drawing.Point(90, 43);
            this.tbStatus.Name = "tbStatus";
            this.tbStatus.ReadOnly = true;
            this.tbStatus.Size = new System.Drawing.Size(147, 21);
            this.tbStatus.TabIndex = 33;
            this.tbStatus.TabStop = false;
            // 
            // lblStatus
            // 
            this.lblStatus.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblStatus.Location = new System.Drawing.Point(3, 44);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(100, 21);
            this.lblStatus.Text = "Status:";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(0, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(240, 16);
            this.label1.Text = "DOKUMENTY MR";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lsDokumentyMR
            // 
            this.lsDokumentyMR.Columns.Add(this.chLP);
            this.lsDokumentyMR.Columns.Add(this.chDokument);
            this.lsDokumentyMR.Columns.Add(this.chPot);
            this.lsDokumentyMR.Columns.Add(this.chZat);
            this.lsDokumentyMR.Columns.Add(this.chStrefa);
            this.lsDokumentyMR.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lsDokumentyMR.FullRowSelect = true;
            this.lsDokumentyMR.Location = new System.Drawing.Point(3, 88);
            this.lsDokumentyMR.Name = "lsDokumentyMR";
            this.lsDokumentyMR.Size = new System.Drawing.Size(234, 136);
            this.lsDokumentyMR.TabIndex = 39;
            this.lsDokumentyMR.View = System.Windows.Forms.View.Details;
            this.lsDokumentyMR.SelectedIndexChanged += new System.EventHandler(this.lsDokumentyMR_SelectedIndexChanged);
            // 
            // chLP
            // 
            this.chLP.Text = "LP";
            this.chLP.Width = 20;
            // 
            // chDokument
            // 
            this.chDokument.Text = "Dokument";
            this.chDokument.Width = 80;
            // 
            // chPot
            // 
            this.chPot.Text = "Pot.";
            this.chPot.Width = 30;
            // 
            // chZat
            // 
            this.chZat.Text = "Zat.";
            this.chZat.Width = 30;
            // 
            // chStrefa
            // 
            this.chStrefa.Text = "Strefa";
            this.chStrefa.Width = 60;
            // 
            // btnSzukaj
            // 
            this.btnSzukaj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnSzukaj.Location = new System.Drawing.Point(3, 275);
            this.btnSzukaj.Name = "btnSzukaj";
            this.btnSzukaj.Size = new System.Drawing.Size(61, 22);
            this.btnSzukaj.TabIndex = 44;
            this.btnSzukaj.TabStop = false;
            this.btnSzukaj.Text = "&1 Szukaj";
            this.btnSzukaj.Click += new System.EventHandler(this.btnSzukaj_Click);
            // 
            // btnPozycjeMR
            // 
            this.btnPozycjeMR.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPozycjeMR.Location = new System.Drawing.Point(66, 275);
            this.btnPozycjeMR.Name = "btnPozycjeMR";
            this.btnPozycjeMR.Size = new System.Drawing.Size(93, 22);
            this.btnPozycjeMR.TabIndex = 45;
            this.btnPozycjeMR.TabStop = false;
            this.btnPozycjeMR.Text = "&2 Pozycje MR";
            this.btnPozycjeMR.Click += new System.EventHandler(this.btnPozycjeMR_Click);
            // 
            // btnPotwierdz
            // 
            this.btnPotwierdz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPotwierdz.Location = new System.Drawing.Point(3, 251);
            this.btnPotwierdz.Name = "btnPotwierdz";
            this.btnPotwierdz.Size = new System.Drawing.Size(118, 22);
            this.btnPotwierdz.TabIndex = 46;
            this.btnPotwierdz.TabStop = false;
            this.btnPotwierdz.Text = "&3 Potwierdz SWA";
            this.btnPotwierdz.Visible = false;
            this.btnPotwierdz.Click += new System.EventHandler(this.btnPotwierdz_Click);
            // 
            // btnOpakowania
            // 
            this.btnOpakowania.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnOpakowania.Location = new System.Drawing.Point(124, 3);
            this.btnOpakowania.Name = "btnOpakowania";
            this.btnOpakowania.Size = new System.Drawing.Size(113, 22);
            this.btnOpakowania.TabIndex = 47;
            this.btnOpakowania.TabStop = false;
            this.btnOpakowania.Text = "&4 Opakowania";
            this.btnOpakowania.Click += new System.EventHandler(this.btnOpakowania_Click);
            // 
            // btnPowrot
            // 
            this.btnPowrot.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPowrot.Location = new System.Drawing.Point(161, 275);
            this.btnPowrot.Name = "btnPowrot";
            this.btnPowrot.Size = new System.Drawing.Size(76, 22);
            this.btnPowrot.TabIndex = 48;
            this.btnPowrot.TabStop = false;
            this.btnPowrot.Text = "&Esc Powr�t";
            this.btnPowrot.Click += new System.EventHandler(this.btnPowrot_Click);
            // 
            // lblLiczbaMR
            // 
            this.lblLiczbaMR.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblLiczbaMR.Location = new System.Drawing.Point(3, 228);
            this.lblLiczbaMR.Name = "lblLiczbaMR";
            this.lblLiczbaMR.Size = new System.Drawing.Size(100, 21);
            this.lblLiczbaMR.Text = "Liczba MR:";
            // 
            // tbLiczbaMR
            // 
            this.tbLiczbaMR.Location = new System.Drawing.Point(58, 227);
            this.tbLiczbaMR.Name = "tbLiczbaMR";
            this.tbLiczbaMR.ReadOnly = true;
            this.tbLiczbaMR.Size = new System.Drawing.Size(35, 21);
            this.tbLiczbaMR.TabIndex = 51;
            this.tbLiczbaMR.TabStop = false;
            // 
            // lblLiczbaPozycjiMR
            // 
            this.lblLiczbaPozycjiMR.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblLiczbaPozycjiMR.Location = new System.Drawing.Point(109, 228);
            this.lblLiczbaPozycjiMR.Name = "lblLiczbaPozycjiMR";
            this.lblLiczbaPozycjiMR.Size = new System.Drawing.Size(97, 21);
            this.lblLiczbaPozycjiMR.Text = "Liczba pozycji MR:";
            // 
            // tbLiczbaPozycjiMR
            // 
            this.tbLiczbaPozycjiMR.Location = new System.Drawing.Point(203, 226);
            this.tbLiczbaPozycjiMR.Name = "tbLiczbaPozycjiMR";
            this.tbLiczbaPozycjiMR.ReadOnly = true;
            this.tbLiczbaPozycjiMR.Size = new System.Drawing.Size(34, 21);
            this.tbLiczbaPozycjiMR.TabIndex = 54;
            this.tbLiczbaPozycjiMR.TabStop = false;
            // 
            // RealizacjaSWAView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.tbLiczbaPozycjiMR);
            this.Controls.Add(this.lblLiczbaPozycjiMR);
            this.Controls.Add(this.tbLiczbaMR);
            this.Controls.Add(this.lblLiczbaMR);
            this.Controls.Add(this.btnPowrot);
            this.Controls.Add(this.btnPotwierdz);
            this.Controls.Add(this.btnPozycjeMR);
            this.Controls.Add(this.btnSzukaj);
            this.Controls.Add(this.lsDokumentyMR);
            this.Controls.Add(this.tbStatus);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.tbDokumentSWA);
            this.Controls.Add(this.lbDokumentSWA);
            this.Controls.Add(this.lbTytul);
            this.Controls.Add(this.label1);
            this.Name = "RealizacjaSWAView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lbDokumentSWA, 0);
            this.Controls.SetChildIndex(this.tbDokumentSWA, 0);
            this.Controls.SetChildIndex(this.lblStatus, 0);
            this.Controls.SetChildIndex(this.tbStatus, 0);
            this.Controls.SetChildIndex(this.lsDokumentyMR, 0);
            this.Controls.SetChildIndex(this.btnSzukaj, 0);
            this.Controls.SetChildIndex(this.btnPozycjeMR, 0);
            this.Controls.SetChildIndex(this.btnPotwierdz, 0);
            this.Controls.SetChildIndex(this.btnPowrot, 0);
            this.Controls.SetChildIndex(this.lblLiczbaMR, 0);
            this.Controls.SetChildIndex(this.tbLiczbaMR, 0);
            this.Controls.SetChildIndex(this.lblLiczbaPozycjiMR, 0);
            this.Controls.SetChildIndex(this.tbLiczbaPozycjiMR, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

        private MSMLabel lbTytul;
        private System.Windows.Forms.TextBox tbDokumentSWA;
        private System.Windows.Forms.Label lbDokumentSWA;
        private System.Windows.Forms.TextBox tbStatus;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label label1;
        private MSMListView lsDokumentyMR;
        private System.Windows.Forms.ColumnHeader chLP;
        private System.Windows.Forms.ColumnHeader chDokument;
        private System.Windows.Forms.ColumnHeader chPot;
        private System.Windows.Forms.ColumnHeader chZat;
        private System.Windows.Forms.ColumnHeader chStrefa;
        private System.Windows.Forms.Button btnSzukaj;
        private System.Windows.Forms.Button btnPozycjeMR;
        private System.Windows.Forms.Button btnPotwierdz;
        private System.Windows.Forms.Button btnOpakowania;
        private System.Windows.Forms.Button btnPowrot;
        private System.Windows.Forms.Label lblLiczbaMR;
        private System.Windows.Forms.TextBox tbLiczbaMR;
        private System.Windows.Forms.Label lblLiczbaPozycjiMR;
        private System.Windows.Forms.TextBox tbLiczbaPozycjiMR;
	}
}
