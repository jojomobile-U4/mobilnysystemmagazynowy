#region

using System;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using WydanieMRModule.ListaMR;
using WydanieSWAModule.Opakowania;
using WydanieSWAModule.PrzegladMR;
using WydanieSWAModule.RealizacjaSWA.Search;
using WydanieSWAModule.Services;

#endregion

namespace WydanieSWAModule.RealizacjaSWA
{
    public class RealizacjaSWAWorkItem : WorkItemBase
    {
        #region Constants

        private String POTWIERDZENIE_MR = "Potwierdzenie dokumentu MR";

        #endregion

        #region Zmienne prywatne

        private long DokumentSWAID;
        private RealizacjaSWASearchEventArgs kryterium;
        private IWorkspace myworkspace;
        private OpakowaniaWorkItem opakowaniaWorkItem;
        private PrzegladMRWorkItem przegladMRWorkItem;
        private RealizacjaSWASearchView realizacjaSWASearchView;
        private RealizacjaSWASearchViewPresenter realizacjaSWASearchViewPresenter;
        private RealizacjaSWAView realizacjaSWAView;
        private RealizacjaSWAViewPresenter realizacjaSWAViewPresenter;
        private IWydanieSWAService wydanieSWAService;

        #endregion

        #region Metody show

        //wywo�anie przyszlo z formularza wyszukiwania
        public void Show(IWorkspace workspace, RealizacjaSWASearchEventArgs searchEventArgs)
        {
            FunctionsHelper.SetCursorWait();
            kryterium = searchEventArgs;
            myworkspace = workspace;

            realizacjaSWAView = realizacjaSWAView ?? Items.AddNew<RealizacjaSWAView>();
            realizacjaSWAViewPresenter = realizacjaSWAViewPresenter ?? new RealizacjaSWAViewPresenter(realizacjaSWAView);
            Items.Add(realizacjaSWAViewPresenter);

            wydanieSWAService = Services.Get<IWydanieSWAService>(true);
            var dokument = wydanieSWAService.PobierzDokumentSWA(DokumentSWAID, searchEventArgs.Dokument,
                                                                searchEventArgs.Zatwierdzony,
                                                                searchEventArgs.Potwierdzony);

            if (!dokument.StatusOperacji.Status.Equals("S"))
            {
                ShowMessageBox(dokument.StatusOperacji.Tekst, "Wczytywanie dokumentu SWA", dokument.StatusOperacji.StosWywolan);
            }
            realizacjaSWAViewPresenter.WyswietlWyszukane(dokument);

            workspace.Show(realizacjaSWAView);
            FunctionsHelper.SetCursorDefault();
            Activate();
        }

        public void Show(IWorkspace workspace, long dokumentSWAID)
        {
            Show(workspace, dokumentSWAID, String.Empty);
        }

        public void Show(IWorkspace workspace, long dokumentSWAID, string symbolDokumentuMR)
        {
            DokumentSWAID = dokumentSWAID;
            myworkspace = workspace;

            var eventArgs = new RealizacjaSWASearchEventArgs();
            eventArgs.Dokument = String.Empty;
            eventArgs.Potwierdzony = String.Empty;
            eventArgs.Zatwierdzony = String.Empty;

            Show(workspace, eventArgs);

            if (symbolDokumentuMR == String.Empty)
                realizacjaSWAView.ZaznaczDokumentOPodanymSymbolu(symbolDokumentuMR);
        }

        #endregion

        #region Metody publiczne

        public void ShowSzukaj(IWorkspace workspace)
        {
            realizacjaSWASearchView = realizacjaSWASearchView ?? Items.AddNew<RealizacjaSWASearchView>();
            realizacjaSWASearchViewPresenter = realizacjaSWASearchViewPresenter ?? new RealizacjaSWASearchViewPresenter(realizacjaSWASearchView);
            Items.Add(realizacjaSWASearchViewPresenter);

            var kryterium = new RealizacjaSWASearchEventArgs();
            kryterium.Potwierdzony = String.Empty;
            kryterium.Zatwierdzony = String.Empty;
            realizacjaSWASearchView.Kryteria = kryterium;

            Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();

            workspace.Show(realizacjaSWASearchView);
            Activate();
        }

        public void ShowOpakowania(IWorkspace workspace, string status, string symbolDokumentu)
        {
            opakowaniaWorkItem = opakowaniaWorkItem ?? WorkItems.AddNew<OpakowaniaWorkItem>();
            opakowaniaWorkItem.Show(workspace, DokumentSWAID, symbolDokumentu, status);
        }

        public bool PotwierdzSWA()
        {
            FunctionsHelper.SetCursorWait();
            wydanieSWAService = Services.Get<IWydanieSWAService>(true);
            var operacji = wydanieSWAService.PotwierdzDokumentSWA(DokumentSWAID);
            FunctionsHelper.SetCursorDefault();
            if (operacji.Status.Equals(StatusOperacji.SUCCESS))
                return true;
            else
            {
                ShowMessageBox(operacji.Tekst, "B��d potwierdzania dokumentu SWA", operacji.StosWywolan);
                Activate();
            }
            return false;
        }

        public void PotwierdzMR(long idDokumentuMR)
        {
            FunctionsHelper.SetCursorWait();
            wydanieSWAService = Services.Get<IWydanieSWAService>(true);
            var statusOperacji = wydanieSWAService.PotwierdzDokumentMR(idDokumentuMR);
            FunctionsHelper.SetCursorDefault();
            if (!statusOperacji.Status.Equals("S"))
            {
                ShowMessageBox(statusOperacji.Tekst, "Potwierdzenie dokumentu MR", statusOperacji.StosWywolan);
                Activate();
            }
            else
            {
                Show(myworkspace, kryterium);
            }
        }

        public void ShowPrzegladMR(long idDokumentuMR)
        {
            RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiEducjaMr] = ContextViewConstants.RealizacjaSWA;
            przegladMRWorkItem = przegladMRWorkItem ?? WorkItems.AddNew<PrzegladMRWorkItem>();
            przegladMRWorkItem.Show(idDokumentuMR);

            przegladMRWorkItem.ZatwierdzonoDokumentMR -= przegladMRWorkItem_ZatwierdzonoDokumentMR;
            przegladMRWorkItem.ZatwierdzonoDokumentMR += przegladMRWorkItem_ZatwierdzonoDokumentMR;
        }

        private void przegladMRWorkItem_ZatwierdzonoDokumentMR(object sender, DataEventArgs<long> e)
        {
            realizacjaSWAView.ZatwierdzDokumentMR(e.Data);

            Show(myworkspace, kryterium);
        }

        public void ZatwierdzonoDokument()
        {
            realizacjaSWAView.StatusDokumentu = "Z";
        }

        public void PotwierdzDokumentMRWgPozycji(long idPozycji)
        {
            FunctionsHelper.SetCursorWait();
            wydanieSWAService = Services.Get<IWydanieSWAService>(true);

            try
            {
                var statusOperacji = wydanieSWAService.PotwierdzDokumentMRWgPozycji(idPozycji);
                FunctionsHelper.SetCursorDefault();
                if (!statusOperacji.Status.Equals("S"))
                {
                    ShowMessageBox(statusOperacji.Tekst, POTWIERDZENIE_MR, statusOperacji.StosWywolan);
                    Activate();
                }
                else
                {
                    Show(myworkspace, kryterium);
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
                Activate();
            }
        }

        public void PotwierdzDokumentMRWgSymbolu(String symbol)
        {
            FunctionsHelper.SetCursorWait();
            wydanieSWAService = Services.Get<IWydanieSWAService>(true);

            try
            {
                var statusOperacji = wydanieSWAService.PotwierdzDokumentMRWgSymbolu(symbol);
                FunctionsHelper.SetCursorDefault();
                if (!statusOperacji.Status.Equals("S"))
                {
                    ShowMessageBox(statusOperacji.Tekst, POTWIERDZENIE_MR, statusOperacji.StosWywolan);
                    Activate();
                }
                else
                {
                    Show(myworkspace, kryterium);
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
                Activate();
            }
        }


        [EventSubscription(EventBrokerConstants.ZATWIERDZONO_DOKUMENT_MR)]
        public void AktywujWorkItem(object sender, EventArgs e)
        {
            var listaMRWorkItem = RootWorkItem.WorkItems.Get<ListaMRWorkItem>(WorkItemsConstants.LISTA_MR_WORKITEM);

            if (!listaMRWorkItem.AktywnaListaMR)
            {
                this.Activate();
            }
        }


        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ)]
        public void WczytajKodKreskowy(object sender, EventArgs e)
        {
            if (RootWorkItem.State[StateConstants.BAR_CODE] != null && realizacjaSWAViewPresenter!=null && MainWorkspace.ActiveSmartPart == realizacjaSWAViewPresenter.View)
            {
                var barCode = RootWorkItem.State[StateConstants.BAR_CODE] as string;
                var idPozycji = KonwertujTextNaLongBezOstrzezenia(barCode);

                if (idPozycji != null)
                {
                    PotwierdzDokumentMRWgPozycji((long) idPozycji);
                }
                else
                {
                    PotwierdzDokumentMRWgSymbolu(barCode);
                }
            }
        }

        #endregion
    }
}