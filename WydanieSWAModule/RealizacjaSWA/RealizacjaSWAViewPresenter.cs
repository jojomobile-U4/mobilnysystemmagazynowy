#region

using System;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.DataModel;
using WydanieMRModule.ListaMR;
using WydanieSWAModule.DataModel;
using WydanieSWAModule.ListaSWA;

#endregion

namespace WydanieSWAModule.RealizacjaSWA
{
    public class RealizacjaSWAViewPresenter : PresenterBase
    {
        #region Constants

        private const string STATUS_GOTOWY = "G";
        private const string STATUS_OTWARTY = "O";
        private const string STATUS_POTWIERDZONY = "P";
        private const string STATUS_ZATWIERDZONY = "Z";

        #endregion

        #region Private Fields

        private Dokument m_Dokument;

        #endregion

        #region Konstruktory

        public RealizacjaSWAViewPresenter(IRealizacjaSWAView view)
            : base(view)
        {
            m_Dokument = new Dokument();
        }

        #endregion

        #region W�asno�ci

        public IRealizacjaSWAView View
        {
            get { return m_view as IRealizacjaSWAView; }
            set { m_view = value; }
        }

        public RealizacjaSWAWorkItem myWorkItem
        {
            get { return (WorkItemBase as RealizacjaSWAWorkItem); }
        }

        private bool MozliwaEdycjaOpakowan
        {
            //pod warunkiem ze status potwierdzony jest przed zatwierdzonym i nadawany automat. przez baze
            get { return STATUS_POTWIERDZONY.Equals(View.StatusDokumentu); }
            //get
            //{
            //    if ( m_Dokument != null )
            //    {
            //        foreach (PozycjaDokumentu pozycja in m_Dokument.Pozycje)
            //        {
            //            if (!DBBool.ToBool(pozycja.Potwierdzony))
            //            {
            //                return false;
            //            }
            //        }
            //        return true;
            //    }
            //    return false;
            //}
        }

        private bool MozliwePotwierdzenieSWA
        {
            get
            {
                return STATUS_GOTOWY.Equals(View.StatusDokumentu) &&
                       PotwierdzoneWszystkiePozycje;
            }
        }

        private bool PotwierdzoneWszystkiePozycje
        {
            get
            {
                if (m_Dokument != null)
                {
                    foreach (var pozycja in m_Dokument.Pozycje)
                    {
                        if (!DBBool.ToBool(pozycja.Potwierdzony))
                        {
                            return false;
                        }
                    }
                    return true;
                }
                return false;
            }
        }

        #endregion

        #region Metody overwrite: podpi�cie zdarze� + nawigacja

        protected override void AttachView()
        {
            View.Powrot -= View_Powrot;
            View.Powrot += View_Powrot;
            View.Szukaj -= View_Szukaj;
            View.Szukaj += View_Szukaj;
            View.PotwierdzDokumentSWA -= View_PotwierdzDokumentSWA;
            View.PotwierdzDokumentSWA += View_PotwierdzDokumentSWA;
            View.PrzegladMR -= View_PrzegladMR;
            View.PrzegladMR += View_PrzegladMR;
            View.Opakowania -= View_Opakowania;
            View.Opakowania += View_Opakowania;
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    e.Handled = true;
                    View_Powrot(this, EventArgs.Empty);

                    myWorkItem.RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiEducjaMr] = ContextViewConstants.None;

                    var listaMRWorkItem = myWorkItem.RootWorkItem.WorkItems.Get<ListaMRWorkItem>(WorkItemsConstants.LISTA_MR_WORKITEM);
                    if (listaMRWorkItem!= null)
                        listaMRWorkItem.AktywnaListaMR = true;

                    break;
                case Keys.D1:
                    e.Handled = true;
                    View_Szukaj(this, EventArgs.Empty);
                    break;
                case Keys.D3:
                    if (MozliwePotwierdzenieSWA)
                    {
                        e.Handled = true;
                        View_PotwierdzDokumentSWA(this, EventArgs.Empty);
                    }
                    break;
/*	            case Keys.Enter:
	                e.Handled = true;
	                View_PotwierdzDokumentMR(this, EventArgs.Empty);
	                break;*/
                case Keys.D4:
                    if (MozliwaEdycjaOpakowan)
                    {
                        e.Handled = true;
                        View_Opakowania(this, EventArgs.Empty);
                    }
                    break;
                case Keys.D2:
                    e.Handled = true;
                    View_PrzegladMR(this, EventArgs.Empty);
                    break;
            }
        }

        #endregion

        #region Obs�uga zdarze�

        private void View_PotwierdzDokumentMR(object sender, EventArgs e)
        {
            if (View.CzyPotwierdzony)
            {
                var l = View.IDDokumentMR;
                if (l == null)
                {
                    MessageBox.Show("Zaznacz dokument, kt�ry chcesz potwierdzi�!", "Potwierdzenie dokumentu MR",
                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                    myWorkItem.Activate();
                }
                else
                    myWorkItem.PotwierdzMR((long) l);
            }
            else
            {
                MessageBox.Show("Nie zaznaczono dokumentu albo wybrany dokument MR nie jest zatwierdzony",
                                "Potwierdzenie dokumentu MR",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                myWorkItem.Activate();
            }
        }


        private void View_Opakowania(object sender, EventArgs e)
        {
            if (MozliwaEdycjaOpakowan)
            {
                myWorkItem.ShowOpakowania(myWorkItem.MainWorkspace, View.StatusDokumentu, View.Symboldokumentu);
            }
        }

        private void View_PrzegladMR(object sender, EventArgs e)
        {
            if (View.IDDokumentMR != null)
                myWorkItem.ShowPrzegladMR((long) View.IDDokumentMR);
        }

        private void View_PotwierdzDokumentSWA(object sender, EventArgs e)
        {
            //potwierdzenie tylko mo�liwe dla dokument�w ze statusem r�nym od P i Z
            //if (!View.StatusDokumentu.Equals("P") && !View.StatusDokumentu.Equals("Z") && myWorkItem.PotwierdzSWA())
            if (MozliwePotwierdzenieSWA && myWorkItem.PotwierdzSWA())
                View.StatusDokumentu = "P";
        }

        private void View_Szukaj(object sender, EventArgs e)
        {
            myWorkItem.ShowSzukaj(myWorkItem.MainWorkspace);
        }

        private void View_Powrot(object sender, EventArgs e)
        {
            myWorkItem.RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiEducjaMr] = ContextViewConstants.None;
            myWorkItem.CloseView(View);
            myWorkItem.RootWorkItem.WorkItems.Get<ListaSWAWorkItem>(WorkItemsConstants.LISTA_SWA_WORKITEM).OdswiezListeDokumentowSWA(this, EventArgs.Empty);
        }

        public void WyswietlWyszukane(Dokument dokument)
        {
            m_Dokument = dokument ?? new Dokument();
            View.WyswietlDokumentSWA = dokument;
            View.OpakowaniaEnabled = MozliwaEdycjaOpakowan;
        }

        #endregion
    }
}