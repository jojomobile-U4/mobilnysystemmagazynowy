#region

using System;
using Common;
using Common.SearchForm;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;

#endregion

namespace WydanieSWAModule.RealizacjaSWA.Search
{
    public class RealizacjaSWASearchViewPresenter : SearchFormPresenter
    {
        private RealizacjaSWASearchEventArgs kryterium;

        public RealizacjaSWASearchViewPresenter(IRealizacjaSWASearchView realizacjaSWASearchView)
            : base(realizacjaSWASearchView)
        {
            kryterium = new RealizacjaSWASearchEventArgs();
            kryterium.Potwierdzony = String.Empty;
            kryterium.Zatwierdzony = String.Empty;
        }

        public IRealizacjaSWASearchView View
        {
            get { return m_view as IRealizacjaSWASearchView; }
        }

        public RealizacjaSWAWorkItem myWorkItem
        {
            get { return (WorkItemBase as RealizacjaSWAWorkItem); }
        }


        protected override void OnViewSzukaj(object sender, EventArgs e)
        {
            kryterium = View.Kryteria;
            myWorkItem.CloseView(View);

            var searchEventArgs = new RealizacjaSWASearchEventArgs();
            searchEventArgs.Dokument = kryterium.Dokument;
            searchEventArgs.Potwierdzony = kryterium.Potwierdzony.Length != 0
                                               ? kryterium.Potwierdzony.Substring(0, 1)
                                               : String.Empty;
            searchEventArgs.Zatwierdzony = kryterium.Zatwierdzony.Length != 0
                                               ? kryterium.Zatwierdzony.Substring(0, 1)
                                               : String.Empty;

            myWorkItem.Show(myWorkItem.MainWorkspace, searchEventArgs);
        }

        protected override void OnViewUstawOstatnieZapytanie(object sender, EventArgs e)
        {
            View.Kryteria = kryterium;
        }

        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ)]
        public void WczytajKodKreskowy(object sender, EventArgs e)
        {
            if (myWorkItem.Status == WorkItemStatus.Active)
            {
                View.KodKreskowy = myWorkItem.RootWorkItem.State["BarCode"] as string;
            }
        }
    }
}