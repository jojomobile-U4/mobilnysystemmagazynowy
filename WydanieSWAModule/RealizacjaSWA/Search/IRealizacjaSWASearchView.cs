#region

using Common.SearchForm;

#endregion

namespace WydanieSWAModule.RealizacjaSWA.Search
{
    public interface IRealizacjaSWASearchView : ISearchForm
    {
        RealizacjaSWASearchEventArgs Kryteria { get; set; }

        string KodKreskowy { set; }
    }
}