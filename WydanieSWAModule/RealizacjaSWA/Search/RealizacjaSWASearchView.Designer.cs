
using Common.Components;

namespace WydanieSWAModule.RealizacjaSWA.Search
{
	partial class RealizacjaSWASearchView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.MainMenu viewMenu;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.viewMenu = new System.Windows.Forms.MainMenu();
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbDokumentSWA = new System.Windows.Forms.TextBox();
            this.lbDokumentSWA = new System.Windows.Forms.Label();
            this.lblPotwierdzony = new System.Windows.Forms.Label();
            this.lblZatwierdzony = new System.Windows.Forms.Label();
            this.lbPotwierdzony = new System.Windows.Forms.ListBox();
            this.lbZatwierdzony = new System.Windows.Forms.ListBox();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // btnOstatnieZapytanie
            // 
            this.btnOstatnieZapytanie.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Location = new System.Drawing.Point(0, 272);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 5;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "SZUKAJ";
            // 
            // tbDokumentSWA
            // 
            this.tbDokumentSWA.Location = new System.Drawing.Point(102, 33);
            this.tbDokumentSWA.Name = "tbDokumentSWA";
            this.tbDokumentSWA.Size = new System.Drawing.Size(135, 21);
            this.tbDokumentSWA.TabIndex = 0;
            // 
            // lbDokumentSWA
            // 
            this.lbDokumentSWA.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbDokumentSWA.Location = new System.Drawing.Point(3, 33);
            this.lbDokumentSWA.Name = "lbDokumentSWA";
            this.lbDokumentSWA.Size = new System.Drawing.Size(100, 21);
            this.lbDokumentSWA.Text = "Dokument SWA:";
            // 
            // lblPotwierdzony
            // 
            this.lblPotwierdzony.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblPotwierdzony.Location = new System.Drawing.Point(3, 57);
            this.lblPotwierdzony.Name = "lblPotwierdzony";
            this.lblPotwierdzony.Size = new System.Drawing.Size(100, 21);
            this.lblPotwierdzony.Text = "Potwierdzony:";
            // 
            // lblZatwierdzony
            // 
            this.lblZatwierdzony.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblZatwierdzony.Location = new System.Drawing.Point(3, 100);
            this.lblZatwierdzony.Name = "lblZatwierdzony";
            this.lblZatwierdzony.Size = new System.Drawing.Size(100, 21);
            this.lblZatwierdzony.Text = "Zatwierdzony:";
            // 
            // lbPotwierdzony
            // 
            this.lbPotwierdzony.DisplayMember = "TAK";
            this.lbPotwierdzony.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbPotwierdzony.Items.Add("");
            this.lbPotwierdzony.Items.Add("TAK");
            this.lbPotwierdzony.Items.Add("NIE");
            this.lbPotwierdzony.Location = new System.Drawing.Point(102, 57);
            this.lbPotwierdzony.Name = "lbPotwierdzony";
            this.lbPotwierdzony.Size = new System.Drawing.Size(100, 41);
            this.lbPotwierdzony.TabIndex = 1;
            this.lbPotwierdzony.ValueMember = "TAK";
            // 
            // lbZatwierdzony
            // 
            this.lbZatwierdzony.DisplayMember = "TAK";
            this.lbZatwierdzony.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbZatwierdzony.Items.Add("");
            this.lbZatwierdzony.Items.Add("TAK");
            this.lbZatwierdzony.Items.Add("NIE");
            this.lbZatwierdzony.Location = new System.Drawing.Point(102, 101);
            this.lbZatwierdzony.Name = "lbZatwierdzony";
            this.lbZatwierdzony.Size = new System.Drawing.Size(100, 41);
            this.lbZatwierdzony.TabIndex = 2;
            this.lbZatwierdzony.ValueMember = "TAK";
            // 
            // RealizacjaSWASearchView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.Controls.Add(this.lbZatwierdzony);
            this.Controls.Add(this.lbPotwierdzony);
            this.Controls.Add(this.lblZatwierdzony);
            this.Controls.Add(this.tbDokumentSWA);
            this.Controls.Add(this.lbDokumentSWA);
            this.Controls.Add(this.lbTytul);
            this.Controls.Add(this.lblPotwierdzony);
            this.Name = "RealizacjaSWASearchView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lblPotwierdzony, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lbDokumentSWA, 0);
            this.Controls.SetChildIndex(this.tbDokumentSWA, 0);
            this.Controls.SetChildIndex(this.lblZatwierdzony, 0);
            this.Controls.SetChildIndex(this.lbPotwierdzony, 0);
            this.Controls.SetChildIndex(this.lbZatwierdzony, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

        private MSMLabel lbTytul;
        private System.Windows.Forms.TextBox tbDokumentSWA;
        private System.Windows.Forms.Label lbDokumentSWA;
        private System.Windows.Forms.Label lblPotwierdzony;
        private System.Windows.Forms.Label lblZatwierdzony;
        private System.Windows.Forms.ListBox lbPotwierdzony;
        private System.Windows.Forms.ListBox lbZatwierdzony;
	}
}
