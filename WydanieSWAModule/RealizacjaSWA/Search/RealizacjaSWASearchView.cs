#region

using Common.SearchForm;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace WydanieSWAModule.RealizacjaSWA.Search
{
    [SmartPart]
    public partial class RealizacjaSWASearchView : SearchForm, IRealizacjaSWASearchView
    {
        public RealizacjaSWASearchView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        // TODO: attach to GUI events, and raise 
        // GUI agnostic events listened by the presenter.
        // These can use EventHandler<TEventArgs> in combination 
        // with DataEventArgs<TData> to avoid proliferation of 
        // delegate types.
        // Raised events must be 'published' to the presenter 
        // through the IRealizacjaSWASearchView interface.

        #region IRealizacjaSWASearchView Members

        public RealizacjaSWASearchEventArgs Kryteria
        {
            get
            {
                var realizacjaSWASearchEventArgs = new RealizacjaSWASearchEventArgs();
                realizacjaSWASearchEventArgs.Dokument = tbDokumentSWA.Text;
                realizacjaSWASearchEventArgs.Potwierdzony = lbPotwierdzony.Text;
                realizacjaSWASearchEventArgs.Zatwierdzony = lbZatwierdzony.Text;
                return realizacjaSWASearchEventArgs;
            }
            set
            {
                tbDokumentSWA.Text = value.Dokument;
                lbPotwierdzony.Text = value.Potwierdzony;
                lbZatwierdzony.Text = value.Zatwierdzony;
            }
        }

        public string KodKreskowy
        {
            set { tbDokumentSWA.Text = value; }
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);
            tbDokumentSWA.ReadOnly = navigationState;
            lbPotwierdzony.Enabled = !navigationState;
            lbZatwierdzony.Enabled = !navigationState;
        }

        #endregion
    }
}