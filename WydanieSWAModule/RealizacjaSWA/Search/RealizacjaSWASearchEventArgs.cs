#region

using System;

#endregion

namespace WydanieSWAModule.RealizacjaSWA.Search
{
    public class RealizacjaSWASearchEventArgs : EventArgs
    {
        #region Zmienne prywatne

        #endregion

        #region W�asno�ci

        public string Dokument { get; set; }

        public string Potwierdzony { get; set; }

        public string Zatwierdzony { get; set; }

        #endregion
    }
}