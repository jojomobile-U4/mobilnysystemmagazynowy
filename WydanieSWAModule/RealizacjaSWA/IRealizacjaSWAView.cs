#region

using System;
using Common.Base;
using WydanieSWAModule.DataModel;

#endregion

namespace WydanieSWAModule.RealizacjaSWA
{
    public interface IRealizacjaSWAView : IViewBase
    {
        #region Zdarzenia

        event EventHandler Powrot;
        event EventHandler Szukaj;
        event EventHandler PotwierdzDokumentSWA;
        event EventHandler PrzegladMR;
        event EventHandler Opakowania;

        #endregion

        #region W�asno�ci

        string StatusDokumentu { set; get; }
        Dokument WyswietlDokumentSWA { set; }
        string Symboldokumentu { get; }
        long? IDDokumentMR { get; }
        bool CzyPotwierdzony { get; }
        bool OpakowaniaEnabled { get; set; }

        #endregion

        #region Metody

        void ZatwierdzDokumentMR(long IDDokumentu);
        void ZaznaczDokumentOPodanymSymbolu(string symbolDokumentuMR);

        #endregion
    }
}