#region

using System;
using System.Windows.Forms;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using WydanieSWAModule.DataModel;

#endregion

namespace WydanieSWAModule.RealizacjaSWA
{
    [SmartPart]
    public partial class RealizacjaSWAView : ViewBase, IRealizacjaSWAView
    {
        #region Wewntrzna klasa pomocnicza

        private class hlpPozycje
        {
            public long IDDokumentuMR;
            public long LiczbaPozycjiMR;
        }

        #endregion

        public RealizacjaSWAView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #region IRealizacjaSWAView Members

        public event EventHandler Powrot;
        public event EventHandler Szukaj;
        public event EventHandler PotwierdzDokumentSWA;
        public event EventHandler PrzegladMR;
        public event EventHandler Opakowania;

        public Dokument WyswietlDokumentSWA
        {
            set { UstawWyswietlaneDane(value); }
        }

        public string Symboldokumentu
        {
            get { return tbDokumentSWA.Text; }
        }

        public string StatusDokumentu
        {
            set
            {
                tbStatus.Text = value;
                UstalZachowanieKontrolek(value);
            }
            get { return tbStatus.Text; }
        }

        public long? IDDokumentMR
        {
            get
            {
                if (CzyZaznaczonyElement())
                    return (lsDokumentyMR.Items[lsDokumentyMR.SelectedIndices[0]].Tag as hlpPozycje).IDDokumentuMR;
                else
                    return null;
            }
        }

        public bool CzyPotwierdzony
        {
            get
            {
                return CzyZaznaczonyElement() && lsDokumentyMR.Items[lsDokumentyMR.SelectedIndices[0]].SubItems[3].Text.
                                                     Substring(0, 1) == "T";
            }
        }

        public void ZatwierdzDokumentMR(long IDDokumentu)
        {
            foreach (ListViewItem viewItem in lsDokumentyMR.Items)
            {
                if (IDDokumentu == (viewItem.Tag as hlpPozycje).IDDokumentuMR)
                    viewItem.SubItems[3].Text = "T";
            }
        }

        public void ZaznaczDokumentOPodanymSymbolu(string symbolDokumentuMR)
        {
            foreach (ListViewItem viewItem in lsDokumentyMR.Items)
            {
                viewItem.Selected = viewItem.SubItems[1].Text.Equals(symbolDokumentuMR);
            }
        }

        public bool OpakowaniaEnabled
        {
            get { return btnOpakowania.Enabled; }
            set { btnOpakowania.Enabled = value; }
        }

        #endregion

        private void btnPowrot_Click(object sender, EventArgs e)
        {
            if (Powrot != null)
            {
                Powrot(this, EventArgs.Empty);
            }
        }


        private void UstawWyswietlaneDane(Dokument value)
        {
            tbDokumentSWA.Text = value != null && value.Naglowek != null
                                     ? value.Naglowek.SymbolDokumentu
                                     : String.Empty;
            tbLiczbaMR.Text = value.LiczbaDokumentowMR.ToString();
            tbStatus.Text = value != null && value.Naglowek != null
                                ? value.Naglowek.Status
                                : String.Empty;

            UstalZachowanieKontrolek(tbStatus.Text);

            #region Umieszczenie listy pozycji

            lsDokumentyMR.Items.Clear();
            var lp = 1;
            ListViewItem listViewItem;
            hlpPozycje hlpPozycje;
            if (value.Pozycje != null)
                foreach (var pozycjaDokumentuSWA in value.Pozycje)
                {
                    listViewItem = lsDokumentyMR.Items.Add(new ListViewItem(lp.ToString()));
                    listViewItem.SubItems.Add(pozycjaDokumentuSWA.SymbolDokumentuMR);
                    listViewItem.SubItems.Add(pozycjaDokumentuSWA.Potwierdzony);
                    listViewItem.SubItems.Add(pozycjaDokumentuSWA.Zatwierdzony);
                    listViewItem.SubItems.Add(pozycjaDokumentuSWA.Strefa);
                    hlpPozycje = new hlpPozycje();
                    //TODO: int => long                    
                    hlpPozycje.IDDokumentuMR = pozycjaDokumentuSWA.IdDokumentuMR;
                    hlpPozycje.LiczbaPozycjiMR = pozycjaDokumentuSWA.LiczbaPozycjiMR;
                    listViewItem.Tag = hlpPozycje;
                    lp++;
                }

            #endregion
        }

        private void UstalZachowanieKontrolek(string status)
        {
            switch (status.ToUpper().Substring(0, 1))
            {
                case "Z":
                    btnOpakowania.Enabled = false;
                    btnPotwierdz.Enabled = false;
                    break;
                case "G":
                    btnOpakowania.Enabled = true;
                    btnPotwierdz.Enabled = true;
                    break;
                case "O":
                    btnOpakowania.Enabled = false;
                    btnPotwierdz.Enabled = false;
                    break;
                case "P":
                    btnOpakowania.Enabled = true;
                    btnPotwierdz.Enabled = false;
                    break;
            }
        }

        private void btnSzukaj_Click(object sender, EventArgs e)
        {
            if (Szukaj != null)
            {
                Szukaj(this, null);
            }
        }

        /// <summary>
        /// Sprawdza czy zosta� zaznaczony element na li�cie
        /// </summary>
        /// <returns></returns>
        private bool CzyZaznaczonyElement()
        {
            return
                lsDokumentyMR.Items.Count > 0 &&
                lsDokumentyMR.SelectedIndices.Count > 0 &&
                lsDokumentyMR.Items[lsDokumentyMR.SelectedIndices[0]].Tag != null;
        }

        private void lsDokumentyMR_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CzyZaznaczonyElement())
            {
                tbLiczbaPozycjiMR.Text =
                    (lsDokumentyMR.Items[lsDokumentyMR.SelectedIndices[0]].Tag as hlpPozycje).LiczbaPozycjiMR.ToString();
            }
            else
            {
                tbLiczbaPozycjiMR.Text = String.Empty;
            }
        }

        private void btnPotwierdz_Click(object sender, EventArgs e)
        {
            if (PotwierdzDokumentSWA != null)
            {
                PotwierdzDokumentSWA(this, EventArgs.Empty);
            }
        }

        private void btnPozycjeMR_Click(object sender, EventArgs e)
        {
            if (PrzegladMR != null)
            {
                PrzegladMR(this, EventArgs.Empty);
            }
        }

        private void btnOpakowania_Click(object sender, EventArgs e)
        {
            if (Opakowania != null)
            {
                Opakowania(this, EventArgs.Empty);
            }
        }
    }
}