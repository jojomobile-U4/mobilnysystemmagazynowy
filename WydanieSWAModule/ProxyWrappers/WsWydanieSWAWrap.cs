#region

using System;
using System.Net;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using Common;
using Common.Base;
using OpenNETCF.Web.Services2;
using WydanieSWAModule.WydanieSWAProxy;

#endregion

namespace WydanieSWAModule.ProxyWrappers
{
    public class WsWydanieSWAWrap : WsWydanieSWA, IWSSecurity
    {
        #region Private s

        #endregion

        #region Properties

        public SecurityHeader SecurityHeader { get; set; }

        #endregion

        #region Constructors

        public WsWydanieSWAWrap(WebServiceConfiguration configuration)
        {
            Url = configuration.Url;
        }

        #endregion

        #region Protected Methods

        protected override WebRequest GetWebRequest(Uri uri)
        {
            var request = (HttpWebRequest) base.GetWebRequest(uri);

            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;

            return request;
        }

        #endregion

        #region Methods

        [SoapDocumentMethod("urn:potwierdzDokumentMR", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("potwierdzDokumentMRResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public potwierdzDokumentMRResponse potwierdzDokumentMR([XmlElement("potwierdzDokumentMR", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] potwierdzDokumentMR potwierdzDokumentMR1)
        {
            var results = Invoke("potwierdzDokumentMR", new object[] {potwierdzDokumentMR1});
            return ((potwierdzDokumentMRResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:potwierdzDokumentMRWgSymbolu", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("potwierdzDokumentMRWgSymboluResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public potwierdzDokumentMRWgSymboluResponse potwierdzDokumentMRWgSymbolu([XmlElement("potwierdzDokumentMRWgSymbolu", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] potwierdzDokumentMRWgSymbolu potwierdzDokumentMRWgSymbolu1)
        {
            var results = Invoke("potwierdzDokumentMRWgSymbolu", new object[] {potwierdzDokumentMRWgSymbolu1});
            return ((potwierdzDokumentMRWgSymboluResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:potwierdzDokumentSWA", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("potwierdzDokumentSWAResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public potwierdzDokumentSWAResponse potwierdzDokumentSWA([XmlElement("potwierdzDokumentSWA", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] potwierdzDokumentSWA potwierdzDokumentSWA1)
        {
            var results = Invoke("potwierdzDokumentSWA", new object[] {potwierdzDokumentSWA1});
            return ((potwierdzDokumentSWAResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:potwierdzDokumentMRWgPozycji", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("potwierdzDokumentMRWgPozycjiResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public potwierdzDokumentMRWgPozycjiResponse potwierdzDokumentMRWgPozycji([XmlElement("potwierdzDokumentMRWgPozycji", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] potwierdzDokumentMRWgPozycji potwierdzDokumentMRWgPozycji1)
        {
            var results = Invoke("potwierdzDokumentMRWgPozycji", new object[] {potwierdzDokumentMRWgPozycji1});
            return ((potwierdzDokumentMRWgPozycjiResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:pobierzListeSpedytorow", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeSpedytorowResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeSpedytorowResponse pobierzListeSpedytorow([XmlElement("pobierzListeSpedytorow", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeSpedytorow pobierzListeSpedytorow1)
        {
            var results = Invoke("pobierzListeSpedytorow", new object[] {pobierzListeSpedytorow1});
            return ((pobierzListeSpedytorowResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:pobierzDokument", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzDokumentResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzDokumentResponse pobierzDokument([XmlElement("pobierzDokument", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzDokumentWrap pobierzDokument1)
        {
            var results = Invoke("pobierzDokument", new object[] {pobierzDokument1});
            return ((pobierzDokumentResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:idSWAdlaDokumentuMR", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("idSWAdlaDokumentuMRResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public idSWAdlaDokumentuMRResponse idSWAdlaDokumentuMR([XmlElement("idSWAdlaDokumentuMR", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] idSWAdlaDokumentuMR idSWAdlaDokumentuMR1)
        {
            var results = Invoke("idSWAdlaDokumentuMR", new object[] {idSWAdlaDokumentuMR1});
            return ((idSWAdlaDokumentuMRResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:przypiszSpedytoraDoSWA", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("przypiszSpedytoraDoSWAResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public przypiszSpedytoraDoSWAResponse przypiszSpedytoraDoSWA([XmlElement("przypiszSpedytoraDoSWA", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] przypiszSpedytoraDoSWAWrap przypiszSpedytoraDoSWA1)
        {
            var results = Invoke("przypiszSpedytoraDoSWA", new object[] {przypiszSpedytoraDoSWA1});
            return ((przypiszSpedytoraDoSWAResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:usunOpakowanie", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("usunOpakowanieResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public usunOpakowanieResponse usunOpakowanie([XmlElement("usunOpakowanie", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] usunOpakowanie usunOpakowanie1)
        {
            var results = Invoke("usunOpakowanie", new object[] {usunOpakowanie1});
            return ((usunOpakowanieResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:usunSpedytoraZSWA", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("usunSpedytoraZSWAResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public usunSpedytoraZSWAResponse usunSpedytoraZSWA([XmlElement("usunSpedytoraZSWA", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] usunSpedytoraZSWA usunSpedytoraZSWA1)
        {
            var results = Invoke("usunSpedytoraZSWA", new object[] {usunSpedytoraZSWA1});
            return ((usunSpedytoraZSWAResponse) (results[0]));
        }

        [SoapDocumentMethodAttribute("urn:pobierzListeNaglowkow", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElementAttribute("pobierzListeNaglowkowResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeNaglowkowResponse pobierzListeNaglowkow([XmlElementAttribute("pobierzListeNaglowkow", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeNaglowkowWrap pobierzListeNaglowkow1)
        {
            var results = Invoke("pobierzListeNaglowkow", new object[] { pobierzListeNaglowkow1 });
            return ((pobierzListeNaglowkowResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:pobierzSpedytoraDlaSWA", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzSpedytoraDlaSWAResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzSpedytoraDlaSWAResponse pobierzSpedytoraDlaSWA([XmlElement("pobierzSpedytoraDlaSWA", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzSpedytoraDlaSWA pobierzSpedytoraDlaSWA1)
        {
            var results = Invoke("pobierzSpedytoraDlaSWA", new object[] {pobierzSpedytoraDlaSWA1});
            return ((pobierzSpedytoraDlaSWAResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:zatwierdzDokumentSWA", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("zatwierdzDokumentSWAResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public zatwierdzDokumentSWAResponse zatwierdzDokumentSWA([XmlElement("zatwierdzDokumentSWA", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] zatwierdzDokumentSWA zatwierdzDokumentSWA1)
        {
            var results = Invoke("zatwierdzDokumentSWA", new object[] {zatwierdzDokumentSWA1});
            return ((zatwierdzDokumentSWAResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:przypiszOpakowanieDoSWA", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("przypiszOpakowanieDoSWAResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public przypiszOpakowanieDoSWAResponse przypiszOpakowanieDoSWA([XmlElement("przypiszOpakowanieDoSWA", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] przypiszOpakowanieDoSWAWrap przypiszOpakowanieDoSWA1)
        {
            var results = Invoke("przypiszOpakowanieDoSWA", new object[] {przypiszOpakowanieDoSWA1});
            return ((przypiszOpakowanieDoSWAResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:rezerwujDokumentSWA", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("rezerwujDokumentSWAResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public rezerwujDokumentSWAResponse rezerwujDokumentSWA([XmlElement("rezerwujDokumentSWA", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] rezerwujDokumentSWA rezerwujDokumentSWA1)
        {
            var results = Invoke("rezerwujDokumentSWA", new object[] {rezerwujDokumentSWA1});
            return ((rezerwujDokumentSWAResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:pobierzListeOpakowan", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeOpakowanResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeOpakowanResponse pobierzListeOpakowan([XmlElement("pobierzListeOpakowan", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeOpakowanWrap pobierzListeOpakowan1)
        {
            var results = Invoke("pobierzListeOpakowan", new object[] {pobierzListeOpakowan1});
            return ((pobierzListeOpakowanResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:pobierzListeOpakowanDlaSWA", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeOpakowanDlaSWAResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeOpakowanDlaSWAResponse pobierzListeOpakowanDlaSWA([XmlElement("pobierzListeOpakowanDlaSWA", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeOpakowanDlaSWA pobierzListeOpakowanDlaSWA1)
        {
            var results = Invoke("pobierzListeOpakowanDlaSWA", new object[] {pobierzListeOpakowanDlaSWA1});
            return ((pobierzListeOpakowanDlaSWAResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:uruchomRaport", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("uruchomRaportResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public uruchomRaportResponse uruchomRaport([XmlElement("uruchomRaport", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] uruchomRaportWrap uruchomRaport1)
        {
            var results = Invoke("uruchomRaport", new object[] {uruchomRaport1});
            return ((uruchomRaportResponse) (results[0]));
        }

        #endregion
    }

    public class pobierzDokumentWrap : pobierzDokument
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? id
        {
            get { return base.id; }
            set { base.id = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string symbolDokumentuMR
        {
            get { return base.symbolDokumentuMR; }
            set { base.symbolDokumentuMR = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string zatwierdzony
        {
            get { return base.zatwierdzony; }
            set { base.zatwierdzony = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string potwierdzony
        {
            get { return base.potwierdzony; }
            set { base.potwierdzony = value; }
        }
    }

    public class pobierzListeNaglowkowWrap : pobierzListeNaglowkow
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idDokumentu
        {
            get { return base.idDokumentu; }
            set { base.idDokumentu = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string symbolDokumentu
        {
            get { return base.symbolDokumentu; }
            set { base.symbolDokumentu = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string status
        {
            get { return base.status; }
            set { base.status = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string zarezerwowany
        {
            get { return base.zarezerwowany; }
            set { base.zarezerwowany = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string porzucony
        {
            get { return base.porzucony; }
            set { base.porzucony = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public DateTime? dataWystawienia
        {
            get { return base.dataWystawienia; }
            set { base.dataWystawienia = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string priorytet
        {
            get { return base.priorytet; }
            set { base.priorytet = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string symbolOdbiorcy
        {
            get { return base.symbolOdbiorcy; }
            set { base.symbolOdbiorcy = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string symbolPozycji
        {
            get { return base.symbolPozycji; }
            set { base.symbolPozycji = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idPozycjiMR
        {
            get { return base.idPozycjiMR; }
            set { base.idPozycjiMR = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? numerStrony
        {
            get { return base.numerStrony; }
            set { base.numerStrony = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? wielkoscStrony
        {
            get { return base.wielkoscStrony; }
            set { base.wielkoscStrony = value; }
        }
    }

    public class pobierzListeOpakowanWrap : pobierzListeOpakowan
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? numerStrony
        {
            get { return base.numerStrony; }
            set { base.numerStrony = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? wielkoscStrony
        {
            get { return base.wielkoscStrony; }
            set { base.wielkoscStrony = value; }
        }
    }

    public class PobierzListeSpedytorowWrap : pobierzListeSpedytorow
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? id
        {
            get { return base.id; }
            set { base.id = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string symbol
        {
            get { return base.symbol; }
            set { base.symbol = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string nazwa
        {
            get { return base.nazwa; }
            set { base.nazwa = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? numerStrony
        {
            get { return base.numerStrony; }
            set { base.numerStrony = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? wielkoscStrony
        {
            get { return base.wielkoscStrony; }
            set { base.wielkoscStrony = value; }
        }
    }

    public class przypiszOpakowanieDoSWAWrap : przypiszOpakowanieDoSWA
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idDokumentu
        {
            get { return base.idDokumentu; }
            set { base.idDokumentu = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idOpakowania
        {
            get { return base.idOpakowania; }
            set { base.idOpakowania = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? wysokosc
        {
            get { return base.wysokosc; }
            set { base.wysokosc = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? szerokosc
        {
            get { return base.szerokosc; }
            set { base.szerokosc = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? dlugosc
        {
            get { return base.dlugosc; }
            set { base.dlugosc = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public float? waga
        {
            get { return base.waga; }
            set { base.waga = value; }
        }
    }

    public class przypiszSpedytoraDoSWAWrap : przypiszSpedytoraDoSWA
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idDokumentu
        {
            get { return base.idDokumentu; }
            set { base.idDokumentu = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idSpedytora
        {
            get { return base.idSpedytora; }
            set { base.idSpedytora = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string symbolSpedytora
        {
            get { return base.symbolSpedytora; }
            set { base.symbolSpedytora = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string nazwaSpedytora
        {
            get { return base.nazwaSpedytora; }
            set { base.nazwaSpedytora = value; }
        }
    }

    public class uruchomRaportWrap : uruchomRaport
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string nazwaFormularza
        {
            get { return base.nazwaFormularza; }
            set { base.nazwaFormularza = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idSWA
        {
            get { return base.idSWA; }
            set { base.idSWA = value; }
        }
    }
}