#region

using System;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.IO;
using Microsoft.Win32;

#endregion

namespace InstallerDLL
{
    [RunInstaller(true)]
    public partial class CustomInstaller : Installer
    {
        #region Constants

        private const string ACTIVESYNC_INSTALL_PATH = @"SOFTWARE\Microsoft\Windows CE Services";
        private const string APP_SUBDIR = @"TETA MSM";
        private const string CEAPPMGR_EXE_FILE = @"CEAPPMGR.EXE";
        private const string CEAPPMGR_INI_FILE = @"MSM.ini";
        private const string CEAPPMGR_PATH = @"SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\CEAPPMGR.EXE";
        private const string INIFILES_SEPARATOR = " ";
        private const string INSTALLED_DIR = "InstalledDir";
        private const string NET_ENU_INI_FILE = @"NETCF2ENU.ini";
        private const string NET_ENU_SUBDIR = @".NET CF 2.0 ENU";
        private const string NET_INI_FILE = @"NETCF2.ini";
        private const string NET_SUBDIR = @".NET CF 2.0";
        private const string SDF_INI_FILE = @"SDF.ini";
        private const string SDF_SUBDIR = @"SDF";
        private const string SYMBOL_INI_FILE = @"SYMBOL.ini";
        private const string SYMBOL_SUBDIR = @"SYMBOL";
        private const string TEMP_FOLDER = @"MSM";
        private const string TEMP_NET_ENU_FOLDER = @"MSM\Prerequisites\.NET CF 2.0 ENU-Strings";
        private const string TEMP_NET_FOLDER = @"MSM\Prerequisites\.NET CF 2.0";
        private const string TEMP_PATH = "TMP";
        private const string TEMP_SDF_FOLDER = @"MSM\Prerequisites\OPENNETCF SDF 2.0";
        private const string TEMP_SYMBOL_FOLDER = @"MSM\Prerequisites\SYMBOL";

        #endregion

        #region Constructors

        public CustomInstaller()
        {
            InitializeComponent();

            this.BeforeInstall += CustomInstaller_BeforeInstall;
            this.BeforeUninstall += CustomInstaller_BeforeUninstall;
        }

        #endregion

        #region Event Subscriptions

        private void CustomInstaller_BeforeUninstall(object sender, InstallEventArgs e)
        {
            RemoveComponent(APP_SUBDIR);
            RemoveComponent(NET_SUBDIR);
            RemoveComponent(NET_ENU_SUBDIR);
            RemoveComponent(SYMBOL_SUBDIR);
            RemoveComponent(SDF_SUBDIR);
        }

        private void CustomInstaller_BeforeInstall(object sender, InstallEventArgs e)
        {
            // Find the location where the application will be installed
            var activeSyncPath = GetActiveSyncPath();
            string iniFiles;
            string iniFile;

            InstallPrereguisites(activeSyncPath, out iniFiles);

            InstallComponent(activeSyncPath, APP_SUBDIR, TEMP_FOLDER, CEAPPMGR_INI_FILE, out iniFile);

            iniFiles += INIFILES_SEPARATOR + iniFile;
            //TODO: dodac formularz z zapytaniem czy instalowac na urzadzeniu mobilnym 
            InstallOnDevice(iniFiles);
        }

        #endregion

        #region Private Methods

        private string GetActiveSyncPath()
        {
            // Get the ActiveSync install directory
            var keyActiveSync =
                Registry.LocalMachine.OpenSubKey(ACTIVESYNC_INSTALL_PATH);
            if (keyActiveSync == null)
            {
                throw new Exception("ActiveSync is not installed.");
            }
            // Build the target directory path under the ActiveSync folder
            var activeSyncPath =
                (string) keyActiveSync.GetValue(INSTALLED_DIR);
            keyActiveSync.Close();
            return activeSyncPath;
        }

        private string GetTemporaryPath(string tempSubFolder)
        {
            return Path.Combine(Environment.GetEnvironmentVariable(TEMP_PATH, EnvironmentVariableTarget.Machine), tempSubFolder);
        }

        private string GetAppMgrPath()
        {
            // Get the path to ceappmgr.exe
            var keyAppMgr =
                Registry.LocalMachine.OpenSubKey(CEAPPMGR_PATH);
            var appMgrPath = (string) keyAppMgr.GetValue(null);
            keyAppMgr.Close();
            return appMgrPath;
        }

        private void InstallPrereguisites(string activeSyncPath, out string iniFiles)
        {
            iniFiles = string.Empty;
            string iniFile;
            InstallComponent(activeSyncPath, NET_SUBDIR, TEMP_NET_FOLDER, NET_INI_FILE, out iniFile);
            iniFiles += iniFile + INIFILES_SEPARATOR;
            InstallComponent(activeSyncPath, NET_ENU_SUBDIR, TEMP_NET_ENU_FOLDER, NET_ENU_INI_FILE, out iniFile);
            iniFiles += iniFile + INIFILES_SEPARATOR;
            InstallComponent(activeSyncPath, SDF_SUBDIR, TEMP_SDF_FOLDER, SDF_INI_FILE, out iniFile);
            iniFiles += iniFile + INIFILES_SEPARATOR;
            InstallComponent(activeSyncPath, SYMBOL_SUBDIR, TEMP_SYMBOL_FOLDER, SYMBOL_INI_FILE, out iniFile);
            iniFiles += iniFile;
        }

        private void InstallComponent(string activeSyncPath, string appSubFolder, string tempFolder, string iniFile, out string appMgrIniFile)
        {
            var installPath = Path.Combine(activeSyncPath, appSubFolder);
            // Create the target directory
            Directory.CreateDirectory(installPath);

            var tempPath = GetTemporaryPath(tempFolder);
            // Copy your application files to the directory
            foreach (var installFile in Directory.GetFiles(tempPath))
            {
                File.Copy(installFile, Path.Combine(installPath,
                                                    Path.GetFileName(installFile)), true);
            }

            appMgrIniFile = "\"" + Path.Combine(installPath, iniFile) + "\"";
        }

        private void RemoveComponent(string appSubFolder)
        {
            // Find where the application is installed
            var installPath = Path.Combine(GetActiveSyncPath(), appSubFolder);
            // Delete the files
            foreach (var appFile in Directory.GetFiles(installPath))
            {
                File.Delete(appFile);
            }
            // Delete the folder
            Directory.Delete(installPath);
        }

        private void InstallOnDevice(string iniFiles)
        {
            // Get the path to ceappmgr.exe
            var appMgrPath = GetAppMgrPath();

            if (!string.IsNullOrEmpty(appMgrPath) &&
                !string.IsNullOrEmpty(iniFiles))
            {
                // Run CeAppMgr.exe to install the files to the device
                Process.Start(appMgrPath, iniFiles);
            }
        }

        #endregion
    }
}