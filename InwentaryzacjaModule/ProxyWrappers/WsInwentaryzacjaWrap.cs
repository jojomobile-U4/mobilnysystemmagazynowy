#region

using System;
using System.Net;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using Common;
using Common.Base;
using InwentaryzacjaModule.WsInwentaryzacjaProxy;
using OpenNETCF.Web.Services2;

#endregion

namespace InwentaryzacjaModule.ProxyWrappers
{
    public class WsInwentaryzacjaWrap : WsInwentaryzacja, IWSSecurity
    {
        #region Private s

        #endregion

        #region Properties

        public SecurityHeader SecurityHeader { get; set; }

        #endregion

        #region Constructors

        public WsInwentaryzacjaWrap(WebServiceConfiguration configuration)
        {
            Url = configuration.Url;
        }

        #endregion

        #region Protected Methods

        protected override WebRequest GetWebRequest(Uri uri)
        {
            var request = (HttpWebRequest) base.GetWebRequest(uri);

            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;

            return request;
        }

        #endregion

        #region Methods

        [SoapDocumentMethod("urn:dodajPozycje", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("dodajPozycjeResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public dodajPozycjeResponse dodajPozycje([XmlElement("dodajPozycje", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] dodajPozycjeWrap dodajPozycje1)
        {
            var results = Invoke("dodajPozycje", new object[] {dodajPozycje1});
            return ((dodajPozycjeResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:pobierzListeNaglowkow", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeNaglowkowResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeNaglowkowResponse pobierzListeNaglowkow([XmlElement("pobierzListeNaglowkow", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeNaglowkowWrap pobierzListeNaglowkow1)
        {
            var results = Invoke("pobierzListeNaglowkow", new object[] {pobierzListeNaglowkow1});
            return ((pobierzListeNaglowkowResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:aktualizujPozycje", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("aktualizujPozycjeResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public aktualizujPozycjeResponse aktualizujPozycje([XmlElement("aktualizujPozycje", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] aktualizujPozycjeWrap aktualizujPozycje1)
        {
            var results = Invoke("aktualizujPozycje", new object[] {aktualizujPozycje1});
            return ((aktualizujPozycjeResponse) (results[0]));
        }

        [SoapDocumentMethodAttribute("urn:pobierzInwentaryzacje", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElementAttribute("pobierzInwentaryzacjeResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzInwentaryzacjeResponse pobierzInwentaryzacje([XmlElementAttribute("pobierzInwentaryzacje", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzInwentaryzacjeWrap pobierzInwentaryzacje1)
        {
            var results = Invoke("pobierzInwentaryzacje", new object[] {pobierzInwentaryzacje1});
            return ((pobierzInwentaryzacjeResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:uruchomRaport", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("uruchomRaportResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public uruchomRaportResponse uruchomRaport([XmlElement("uruchomRaport", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] uruchomRaportWrap uruchomRaport1)
        {
            var results = Invoke("uruchomRaport", new object[] {uruchomRaport1});
            return ((uruchomRaportResponse) (results[0]));
        }

        #endregion
    }

    public class pobierzInwentaryzacjeWrap : pobierzInwentaryzacje
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new long? id
        {
            get { return base.id; }
            set { base.id = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new string lokalizacja
        {
            get { return base.lokalizacja; }
            set { base.lokalizacja = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new string typZakresu
        {
            get { return base.typZakresu; }
            set { base.typZakresu = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new int? lpOd
        {
            get { return base.lpOd; }
            set { base.lpOd = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new int? lpDo
        {
            get { return base.lpDo; }
            set { base.lpDo = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new string adresOd
        {
            get { return base.adresOd; }
            set { base.adresOd = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new string adresDo
        {
            get { return base.adresDo; }
            set { base.adresDo = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new string zWprowadzonaIloscia
        {
            get { return base.zWprowadzonaIloscia; }
            set { base.zWprowadzonaIloscia = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new int? numerStrony
        {
            get { return base.numerStrony; }
            set { base.numerStrony = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new int? wielkoscStrony
        {
            get { return base.wielkoscStrony; }
            set { base.wielkoscStrony = value; }
        }
    }

    public class pobierzListeNaglowkowWrap : pobierzListeNaglowkow
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new int? numerStrony
        {
            get { return base.numerStrony; }
            set { base.numerStrony = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new int? wielkoscStrony
        {
            get { return base.wielkoscStrony; }
            set { base.wielkoscStrony = value; }
        }
    }

    public class dodajPozycjeWrap : dodajPozycje
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new long? idInwentaryzacji
        {
            get { return base.idInwentaryzacji; }
            set { base.idInwentaryzacji = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new string symbolTowaru
        {
            get { return base.symbolTowaru; }
            set { base.symbolTowaru = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new int? odmiana
        {
            get { return base.odmiana; }
            set { base.odmiana = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new DateTime? dataWaznosci
        {
            get { return base.dataWaznosci; }
            set { base.dataWaznosci = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new DateTime? dataProdukcji
        {
            get { return base.dataProdukcji; }
            set { base.dataProdukcji = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new string nrSerii
        {
            get { return base.nrSerii; }
            set { base.nrSerii = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new string nrKolejny
        {
            get { return base.nrKolejny; }
            set { base.nrKolejny = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new string nrKz
        {
            get { return base.nrKz; }
            set { base.nrKz = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new string lokalizacja
        {
            get { return base.lokalizacja; }
            set { base.lokalizacja = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new float? ilosc
        {
            get { return base.ilosc; }
            set { base.ilosc = value; }
        }
    }

    public class uruchomRaportWrap : uruchomRaport
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new string nazwaFormularza
        {
            get { return base.nazwaFormularza; }
            set { base.nazwaFormularza = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new long? idInwentaryzacji
        {
            get { return base.idInwentaryzacji; }
            set { base.idInwentaryzacji = value; }
        }
    }

    public class aktualizujPozycjeWrap : aktualizujPozycje
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public new Pozycja pozycja
        {
            get { return base.pozycja; }
            set { base.pozycja = value; }
        }
    }
}