#region

using System;
using System.Windows.Forms;
using Common;
using Common.Base;
using InwentaryzacjaModule.DataModel;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;

#endregion

namespace InwentaryzacjaModule
{
    public class InwentaryzacjaViewPresenter : PresenterBase
    {
        #region Constants

        private const string ETYKIETA_POKAZ_PUSTE = "&2 Poka� puste";
        private const string ETYKIETA_POKAZ_WSZYSTKIE = "&2 Poka� wszystkie";
        private const int NASTEPNA = 1;
        private const int PIERWSZA = 0;
        private const int POPRZEDNIA = -1;

        #endregion

        #region Private Fields

        private float? aktualnaIloscPrzedZmiana;
        private Pozycja aktualnaPozycja;
        private Inwentaryzacja inwentaryzacja;
        private bool mozliwaZmianaIlosci;
        private int numerStrony;
        private Tryb tryb;
        private int zmianaStrony;

        #endregion

        #region Properties

        public IInwentaryzacjaView View
        {
            get { return m_view as IInwentaryzacjaView; }
        }

        protected InwentaryzacjaWorkItem MyWorkItem
        {
            get { return WorkItem as InwentaryzacjaWorkItem; }
        }

        private bool MozliwaZmianaStrony
        {
            get { return inwentaryzacja != null && inwentaryzacja.Pozycje.Lista.Count >= MyWorkItem.Configuration.InwentaryzacjaWS.WielkoscStrony; }
        }

        private bool TylkoPuste
        {
            get { return tryb == Tryb.Puste; }
        }

        private bool Wszystkie
        {
            get { return tryb == Tryb.Wszystkie; }
        }

        public Tryb TrybWyswietlania
        {
            get { return tryb; }
            set
            {
                tryb = value;
                AktualizujTrybWyswietlania();
            }
        }

        #endregion

        #region Constructors

        public InwentaryzacjaViewPresenter(IInwentaryzacjaView view) : base(view)
        {
            ResetujStronicowanie();
            tryb = Tryb.Wszystkie;
        }

        #endregion

        #region Methods

        public void ZaladujDaneDoWidoku(Inwentaryzacja inwentaryzacja, bool mozliwaZmianaIlosci)
        {
            this.mozliwaZmianaIlosci = mozliwaZmianaIlosci;
            if (inwentaryzacja == null ||
                inwentaryzacja.Pozycje.Lista.Count == 0)
            {
                if (inwentaryzacja == null)
                {
                    this.inwentaryzacja = new Inwentaryzacja();
                }
                else
                {
                    this.inwentaryzacja = inwentaryzacja;
                }
                this.inwentaryzacja.Pozycje = new ListaPozycji();
                aktualnaPozycja = null;
                aktualnaIloscPrzedZmiana = null;
            }
            else
            {
                this.inwentaryzacja = inwentaryzacja;
                if (zmianaStrony == POPRZEDNIA)
                {
                    aktualnaPozycja = this.inwentaryzacja.Pozycje.Lista[inwentaryzacja.Pozycje.Lista.Count - 1];
                    aktualnaIloscPrzedZmiana = this.inwentaryzacja.Pozycje.Lista[inwentaryzacja.Pozycje.Lista.Count - 1].Ilosc;
                }
                else
                {
                    aktualnaPozycja = this.inwentaryzacja.Pozycje.Lista[0];
                    aktualnaIloscPrzedZmiana = this.inwentaryzacja.Pozycje.Lista[0].Ilosc;
                }

                if (numerStrony > 0)
                {
                    //HACK: aktualizacja lp pozycji zgodnie z aktualna strona
                    foreach (var pozycja in inwentaryzacja.Pozycje.Lista)
                    {
                        pozycja.Lp += numerStrony*MyWorkItem.Configuration.InwentaryzacjaWS.WielkoscStrony;
                    }
                }
                if (mozliwaZmianaIlosci)
                {
                    WorkItem.Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                }
            }

            AktualizujWidok();
        }

        internal void ResetujStronicowanie()
        {
            numerStrony = PIERWSZA;
            zmianaStrony = NASTEPNA;
        }

        #endregion

        protected override void AttachView()
        {
            View.Anuluj += View_Anuluj;
            View.Nastepny += View_Nastepny;
            View.NowaPozycja += View_NowaPozycja;
            View.Pokaz += View_Pokaz;
            View.Poprzedni += View_Poprzedni;
        }

        [EventSubscription(EventBrokerConstants.NAVIGATION_STATE_CHANGED)]
        public override void NavigationStateChanged(object sender, EventArgs e)
        {
            if (mozliwaZmianaIlosci)
            {
                base.NavigationStateChanged(sender, e);

                if (NavigationState && (NavigationKeyArgs == null || NavigationKeyArgs.KeyCode != Keys.Escape))
                {
                    AktualizujPozycje();
                }
            }
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    e.Handled = true;
                    View_Anuluj(this, EventArgs.Empty);
                    break;
                case Keys.D1:
                    e.Handled = true;
                    View_NowaPozycja(this, EventArgs.Empty);
                    break;
                case Keys.D2:
                    e.Handled = true;
                    View_Pokaz(this, EventArgs.Empty);
                    break;
                case Keys.Left:
                    e.Handled = true;
                    View_Poprzedni(this, EventArgs.Empty);
                    break;
                case Keys.Right:
                    e.Handled = true;
                    View_Nastepny(this, EventArgs.Empty);
                    break;
                case Keys.Down:
                    e.Handled = true;
                    View.FocusNextControl(true);
                    break;
                case Keys.Up:
                    e.Handled = true;
                    View.FocusNextControl(false);
                    break;
            }
        }

        #region Obsluga widoku

        private void View_NowaPozycja(object sender, EventArgs e)
        {
            MyWorkItem.NowaPozycja();
        }

        private void View_Pokaz(object sender, EventArgs e)
        {
            ZmienTryb();
            AktualizujTrybWyswietlania();

            //usuniecie filtracji po lokalizacji - zawsze po wcisnieciu przycisku Pokaz...
            State[StateConstants.LOCALIZATION] = null;

            ZaladujDaneDoWidoku(MyWorkItem.PobierzInwentaryzacje(inwentaryzacja.Naglowek, TylkoPuste, numerStrony), false);
        }

        private void AktualizujTrybWyswietlania()
        {
            View.EtykietaPokaz = Wszystkie ? ETYKIETA_POKAZ_PUSTE : ETYKIETA_POKAZ_WSZYSTKIE;
        }

        private void ZmienTryb()
        {
            if (tryb == Tryb.Wszystkie)
            {
                tryb = Tryb.Puste;
            }
            else
            {
                tryb = Tryb.Wszystkie;
            }
        }

        private void View_Anuluj(object sender, EventArgs e)
        {
            CloseView();
            MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
            MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
            MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
            MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
        }

        private void View_Poprzedni(object sender, EventArgs e)
        {
            if (aktualnaPozycja != null)
            {
                View.ForceDataBinding();

                var indeksAktualnej = inwentaryzacja.Pozycje.Lista.IndexOf(aktualnaPozycja);
                if (indeksAktualnej > 0)
                {
                    AktualizujPozycje();
                    aktualnaPozycja = inwentaryzacja.Pozycje.Lista[indeksAktualnej - 1];
                    aktualnaIloscPrzedZmiana = inwentaryzacja.Pozycje.Lista[indeksAktualnej - 1].Ilosc;
                    AktualizujWidok();
                }
                else
                {
                    if (MozliwaZmianaStrony)
                    {
                        //TODO: sprawdzic czy jest poprzednia paczka (strona)

                        //jezeli numer strony = 0, znaczy ze nie ma wczesniejszych stron
                        if (numerStrony > 0)
                        {
                            ZmienStrone(POPRZEDNIA);
                        }
                    }
                }
            }
            View.ZaznaczIlosc();
        }

        private void View_Nastepny(object sender, EventArgs e)
        {
            //sprawdzenie czy nie jestesmy na ostatniej stronie
            if (aktualnaPozycja != null)
            {
                View.ForceDataBinding();

                var indeksAktualnej = inwentaryzacja.Pozycje.Lista.IndexOf(aktualnaPozycja);
                if (indeksAktualnej < inwentaryzacja.Pozycje.Lista.Count - 1)
                {
                    AktualizujPozycje();
                    aktualnaPozycja = inwentaryzacja.Pozycje.Lista[indeksAktualnej + 1];
                    aktualnaIloscPrzedZmiana = inwentaryzacja.Pozycje.Lista[indeksAktualnej + 1].Ilosc;
                    AktualizujWidok();
                }
                else
                {
                    //sprawdzenie czy nie jestesmy na ostatniej stronie
                    //jezeli nie ma indeksow, znaczy ze nie ma dalszych stron
                    if (MozliwaZmianaStrony)
                    {
                        //TODO: sprawdzic czy jest nastepna paczka (strona)
                        ZmienStrone(NASTEPNA);
                    }
                }
            }
            View.ZaznaczIlosc();
        }

        private void AktualizujWidok()
        {
            View.DataSource = aktualnaPozycja;
            View.IloscLp = inwentaryzacja.Naglowek.LiczbaPozycji;
            AktualizujDaneInwentaryzacji();

            if (aktualnaPozycja != null)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = aktualnaPozycja.SymbolIndeksu;
                MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = aktualnaPozycja.SymbolIndeksu;
                if (aktualnaPozycja.SymbolIndeksu != null)
                {
                    MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueYes;
                }
                else
                {
                    MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
                }
            }
        }

        private void AktualizujDaneInwentaryzacji()
        {
            View.InwentaryzacjaDataSource.Clear();

            if (inwentaryzacja != null)
            {
                View.InwentaryzacjaDataSource.Add(UtworzWpis());
            }
        }

        private ListViewItem UtworzWpis()
        {
            var naglowek = inwentaryzacja.Naglowek;
            var elementy = new[]
                               {
                                   naglowek.Symbol,
                                   naglowek.SymbolGM,
                                   naglowek.Magazyn,
                                   naglowek.Data.ToString(Constants.DATE_FORMAT)
                               };
            var wpis = new ListViewItem(elementy);

            return wpis;
        }

        private void ZmienStrone(int kierunek)
        {
            numerStrony += kierunek;
            zmianaStrony = kierunek;
            ZaladujDaneDoWidoku(MyWorkItem.PobierzInwentaryzacje(inwentaryzacja.Naglowek, TylkoPuste, numerStrony), mozliwaZmianaIlosci);
        }

        private void AktualizujPozycje()
        {
            var indeksPozycji = inwentaryzacja.Pozycje.Lista.IndexOf(aktualnaPozycja);
            if ((mozliwaZmianaIlosci) && (aktualnaPozycja != null)
                && (!aktualnaPozycja.Ilosc.Equals(aktualnaIloscPrzedZmiana) || View.Ilosc == null))
            {
                aktualnaPozycja.Ilosc = View.Ilosc;
                if (!MyWorkItem.AktualizujPozycje(aktualnaPozycja))
                {
                    View.Ilosc = aktualnaIloscPrzedZmiana;
                }
            }
            aktualnaIloscPrzedZmiana = View.Ilosc;
        }

        #endregion
    }

    public enum Tryb
    {
        Wszystkie,
        Puste,
        FiltrPoLokalizacji
    }
}