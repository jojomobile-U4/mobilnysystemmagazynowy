#region

using System;
using Common;
using Common.Base;
using Common.DataModel;
using InwentaryzacjaModule.DataModel;
using InwentaryzacjaModule.ListaInwentaryzacji;
using InwentaryzacjaModule.NowaPozycja;
using InwentaryzacjaModule.Services;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;

#endregion

namespace InwentaryzacjaModule
{
    public class InwentaryzacjaWorkItem : WorkItemBase
    {
        #region Constants

        private const string BRAK_OGRANICZENIA_ZAKRESU = "Warto�ci Od i Do musz� by� wype�nione.";
        private const string INFORMACJA = "Informacja";
        private const string NIEWYPELNINE_POZYCJE = "Nie wszystkie pozycje z wybranego zakresu inwentaryzacji zosta�y wype�nione.";
        private const string POLE_DO = "Do";
        private const string POLE_OD = "Od";
        private const string WARTOSC_OD_WIEKSZA_NIZ_WARTOSC_DO = "Warto�� w polu Od musi by� mniejsza od warto�ci w polu Do.";
        private const string WZORZEC_BLEDU_WARTOSCI = "Warto�� pola {0} musi by� liczb� naturaln�.";
        private const string ZACZYTANA_LOKALIZACJA_POZA_ZAKRESEM = "Zaczytana lokalizacja nie znajduje si� w wybranym zakresie.";
        private const string ZAPIS_ZAKONCZONY_SUKCESEM = "Zapis do bazy zako�czony sukcesem.";

        #endregion

        #region Private Fields

        private Inwentaryzacja m_AktualnaInwentaryzacja;

        #endregion

        #region Properties

        protected Zakres Zakres
        {
            get { return State[StateConstants.RANGE] as Zakres; }
        }

        protected string Lokalizacja
        {
            get { return State[StateConstants.LOCALIZATION] as string; }
            set { State[StateConstants.LOCALIZATION] = value; }
        }

        #endregion

        #region Event Subscriptions

        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ)]
        public void BarCodeRead(object sender, EventArgs e)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                var inwentaryzacjaPresenter = Items.Get<InwentaryzacjaViewPresenter>(ItemsConstants.INWENTARYAZCJA_PRESENTER);
                if (Status == WorkItemStatus.Active &&
                    RootWorkItem.State[StateConstants.BAR_CODE] != null &&
                    m_AktualnaInwentaryzacja != null &&
                    MainWorkspace.ActiveSmartPart == inwentaryzacjaPresenter.View)
                {
                    Lokalizacja = RootWorkItem.State[StateConstants.BAR_CODE] as string;
                    var inwentaryzacja = Services.Get<IInwentaryzacjaService>(true).PobierzInwentaryzacje(m_AktualnaInwentaryzacja.Naglowek.Id, false, Lokalizacja, Zakres, null);
                    if (inwentaryzacja.StatusOperacji.Status.Equals(StatusOperacji.SUCCESS))
                    {
                        if (inwentaryzacja.Pozycje.Lista.Count == 0)
                        {
                            ShowMessageBox(ZACZYTANA_LOKALIZACJA_POZA_ZAKRESEM, INFORMACJA);
                            Lokalizacja = null;
                            return;
                        }
                        inwentaryzacjaPresenter.ResetujStronicowanie();
                        inwentaryzacjaPresenter.TrybWyswietlania = Tryb.FiltrPoLokalizacji;
                        inwentaryzacjaPresenter.ZaladujDaneDoWidoku(inwentaryzacja, true);
                    }
                    Lokalizacja = null;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        #endregion

        #region Show Methods

        public void Show()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var listaInwentaryzacji = Items.Get<ListaInwentaryzacjiViewPresenter>(ItemsConstants.LISTA_INWENTARYZACJI_PRESENTER);
                var listaNaglowkow = Services.Get<IInwentaryzacjaService>(true).PobierzListeNaglowkow();
                if (listaNaglowkow.StatusOperacji.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(listaNaglowkow.StatusOperacji.Tekst, BLAD, listaNaglowkow.StatusOperacji.StosWywolan);
                }
                listaInwentaryzacji.ZaladujDaneDoWidoku(listaNaglowkow);
                MainWorkspace.Show(listaInwentaryzacji.View);
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        #endregion

        #region Methods

        public void Wybierz(Naglowek naglowek)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                if (!PoprawneWartosciZakresu())
                {
                    return;
                }

                var inwentaryzacjaPresenter = Items.Get<InwentaryzacjaViewPresenter>(ItemsConstants.INWENTARYAZCJA_PRESENTER);
                inwentaryzacjaPresenter.ResetujStronicowanie();
                m_AktualnaInwentaryzacja = PobierzInwentaryzacje(naglowek, false, null);
                inwentaryzacjaPresenter.ZaladujDaneDoWidoku(m_AktualnaInwentaryzacja, false);

                MainWorkspace.Show(inwentaryzacjaPresenter.View);
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public bool DodajNowaPozycje(Pozycja pozycja)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                if (pozycja == null ||
                    !PoprawneDanePozycji(pozycja))
                {
                    return false;
                }

                var nowaPozycje = Services.Get<IInwentaryzacjaService>(true).DodajPozycje(pozycja, m_AktualnaInwentaryzacja.Naglowek.Id);
                if (nowaPozycje.StatusOperacji.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(nowaPozycje.StatusOperacji.Tekst, BLAD, nowaPozycje.StatusOperacji.StosWywolan);
                    return false;
                }
                else
                {
                    m_AktualnaInwentaryzacja.Pozycje.Lista.Add(nowaPozycje.Pozycja);
                    //zmieniamy liczbe pozycji, ale po zmianie paczki bedzie tyle ile miesci sie w zadanym zakresie
                    m_AktualnaInwentaryzacja.Naglowek.LiczbaPozycji++;
                    //nadpisanie rzeczywistych lp, aby nie bylo lp/ilosc lp: 4/2
                    nowaPozycje.Pozycja.Lp = m_AktualnaInwentaryzacja.Pozycje.Lista[m_AktualnaInwentaryzacja.Pozycje.Lista.Count - 1].Lp + 1;

                    Items.Get<InwentaryzacjaViewPresenter>(ItemsConstants.INWENTARYAZCJA_PRESENTER).ZaladujDaneDoWidoku(m_AktualnaInwentaryzacja, false);

                    return true;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void NowaPozycja()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var nowaPozycjaPresenter = Items.Get<NowaPozycjaViewPresenter>(ItemsConstants.NOWA_POZYCJA_INWENTARYZACJI_PRESENTER);
                nowaPozycjaPresenter.View.DataSource = new Pozycja();

                Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                MainWorkspace.Show(nowaPozycjaPresenter.View);
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public bool AktualizujPozycje(Pozycja pozycja)
        {
            if (pozycja.Ilosc == null)
            {
                FunctionsHelper.SetCursorDefault();
                ShowMessageBox(NIEPRAWIDLOWY_FORMAT_LICZBY, BLAD);
                return false;
            }

            try
            {
                FunctionsHelper.SetCursorWait();

                var statusOperacji = Services.Get<IInwentaryzacjaService>(true).AktualizujPozycje(pozycja);

                FunctionsHelper.SetCursorDefault();

                if (!statusOperacji.Status.Equals(StatusOperacji.SUCCESS))
                {
                    ShowMessageBox(statusOperacji.Tekst, "Inwentaryzacja", statusOperacji.StosWywolan);
                    return false;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
            return true;
        }

        public Inwentaryzacja PobierzInwentaryzacje(Naglowek naglowek, bool tylkoPuste, int? numerStrony)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                if (naglowek == null)
                {
                    return null;
                }

                var wynik = Services.Get<IInwentaryzacjaService>(true).PobierzInwentaryzacje(naglowek.Id, tylkoPuste, Lokalizacja, Zakres, numerStrony);
                if (wynik.StatusOperacji.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(wynik.StatusOperacji.Tekst, BLAD, wynik.StatusOperacji.StosWywolan);
                }

                return wynik;
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public bool UruchomRaport(string nazwaFormularza, long idInwentaryzacji)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                Services.Get<IInwentaryzacjaService>(true).UruchomRaport(nazwaFormularza, idInwentaryzacji);
                if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, BLAD, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                    return false;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }

            return true;
        }

        #endregion

        #region Private Methods

        private bool PoprawneDanePozycji(Pozycja pozycja)
        {
            if (string.IsNullOrEmpty(pozycja.SymbolIndeksu))
            {
                ShowMessageBox("Symbol indeksu musi by� wype�niony.", BLAD);
                return false;
            }
            if (string.IsNullOrEmpty(pozycja.Lokalizacja))
            {
                ShowMessageBox("Adres lokalizacji musi by� wype�niony.", BLAD);
                return false;
            }
            if (pozycja.Ilosc == null)
            {
                ShowMessageBox("Ilo�� musi by� wype�niona.", BLAD);
                return false;
            }
            return true;
        }

        private bool PoprawneWartosciZakresu()
        {
            var zakres = Zakres;
            if (zakres == null)
            {
                return false;
            }
            if (string.IsNullOrEmpty(zakres.WartoscDo) ||
                string.IsNullOrEmpty(zakres.WartoscOd))
            {
                ShowMessageBox(BRAK_OGRANICZENIA_ZAKRESU, BLAD);
                return false;
            }
            if (zakres.WartoscOd.CompareTo(zakres.WartoscDo) > 1)
            {
                ShowMessageBox(WARTOSC_OD_WIEKSZA_NIZ_WARTOSC_DO, BLAD);
                return false;
            }
            if (zakres.Typ == TypyZakresow.LP)
            {
                if (!JestLiczbaCalkowita(zakres.WartoscOd))
                {
                    ShowMessageBox(string.Format(WZORZEC_BLEDU_WARTOSCI, POLE_OD), BLAD);
                    return false;
                }
                if (!JestLiczbaCalkowita(zakres.WartoscDo))
                {
                    ShowMessageBox(string.Format(WZORZEC_BLEDU_WARTOSCI, POLE_DO), BLAD);
                    return false;
                }
            }
            return true;
        }

        private bool JestLiczbaCalkowita(string tekst)
        {
            try
            {
                int.Parse(tekst);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        #endregion
    }
}