using Common.Components;

namespace InwentaryzacjaModule.ListaInwentaryzacji
{
	partial class ListaInwentaryzacjiView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.btnWyjdz = new System.Windows.Forms.Button();
            this.lstListaInwentaryzacji = new Common.Components.MSMListView();
            this.chSymbol = new System.Windows.Forms.ColumnHeader();
            this.chInwentaryzacja = new System.Windows.Forms.ColumnHeader();
            this.chMagazyn = new System.Windows.Forms.ColumnHeader();
            this.chData = new System.Windows.Forms.ColumnHeader();
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbUwagi = new System.Windows.Forms.TextBox();
            this.lbUwagi = new System.Windows.Forms.Label();
            this.btnWybierz = new System.Windows.Forms.Button();
            this.lbZakres = new System.Windows.Forms.Label();
            this.zakresBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cbZakres = new System.Windows.Forms.ComboBox();
            this.lbWartoscOd = new System.Windows.Forms.Label();
            this.tbOd = new System.Windows.Forms.TextBox();
            this.tbDo = new System.Windows.Forms.TextBox();
            this.lbWartoscDo = new System.Windows.Forms.Label();
            this.pnlNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zakresBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnWybierz);
            this.pnlNavigation.Controls.Add(this.btnWyjdz);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 273);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 27);
            // 
            // btnWyjdz
            // 
            this.btnWyjdz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnWyjdz.Location = new System.Drawing.Point(144, 3);
            this.btnWyjdz.Name = "btnWyjdz";
            this.btnWyjdz.Size = new System.Drawing.Size(92, 20);
            this.btnWyjdz.TabIndex = 5;
            this.btnWyjdz.Text = "&Esc Wyjd�";
            this.btnWyjdz.Click += new System.EventHandler(this.OnWyjdz);
            // 
            // lstListaInwentaryzacji
            // 
            this.lstListaInwentaryzacji.Columns.Add(this.chSymbol);
            this.lstListaInwentaryzacji.Columns.Add(this.chInwentaryzacja);
            this.lstListaInwentaryzacji.Columns.Add(this.chMagazyn);
            this.lstListaInwentaryzacji.Columns.Add(this.chData);
            this.lstListaInwentaryzacji.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lstListaInwentaryzacji.FullRowSelect = true;
            this.lstListaInwentaryzacji.Location = new System.Drawing.Point(3, 20);
            this.lstListaInwentaryzacji.Name = "lstListaInwentaryzacji";
            this.lstListaInwentaryzacji.Size = new System.Drawing.Size(234, 122);
            this.lstListaInwentaryzacji.TabIndex = 0;
            this.lstListaInwentaryzacji.View = System.Windows.Forms.View.Details;
            this.lstListaInwentaryzacji.SelectedIndexChanged += new System.EventHandler(this.OnZaznaczonaInwentaryzacjaChanged);
            // 
            // chSymbol
            // 
            this.chSymbol.Text = "Symbol";
            this.chSymbol.Width = 50;
            // 
            // chInwentaryzacja
            // 
            this.chInwentaryzacja.Text = "Inwentaryzacja";
            this.chInwentaryzacja.Width = 60;
            // 
            // chMagazyn
            // 
            this.chMagazyn.Text = "Magazyn";
            this.chMagazyn.Width = 50;
            // 
            // chData
            // 
            this.chData.Text = "Data";
            this.chData.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chData.Width = 62;
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 7;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "INWENTARYZACJA - LISTA";
            // 
            // tbUwagi
            // 
            this.tbUwagi.Location = new System.Drawing.Point(48, 145);
            this.tbUwagi.Multiline = true;
            this.tbUwagi.Name = "tbUwagi";
            this.tbUwagi.ReadOnly = true;
            this.tbUwagi.Size = new System.Drawing.Size(189, 55);
            this.tbUwagi.TabIndex = 6;
            this.tbUwagi.TabStop = false;
            // 
            // lbUwagi
            // 
            this.lbUwagi.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbUwagi.Location = new System.Drawing.Point(3, 145);
            this.lbUwagi.Name = "lbUwagi";
            this.lbUwagi.Size = new System.Drawing.Size(45, 17);
            this.lbUwagi.Text = "Uwagi:";
            // 
            // btnWybierz
            // 
            this.btnWybierz.Enabled = false;
            this.btnWybierz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnWybierz.Location = new System.Drawing.Point(48, 3);
            this.btnWybierz.Name = "btnWybierz";
            this.btnWybierz.Size = new System.Drawing.Size(92, 20);
            this.btnWybierz.TabIndex = 4;
            this.btnWybierz.Text = "&Ret Wybierz";
            this.btnWybierz.Click += new System.EventHandler(this.OnWybierz);
            // 
            // lbZakres
            // 
            this.lbZakres.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbZakres.Location = new System.Drawing.Point(4, 202);
            this.lbZakres.Name = "lbZakres";
            this.lbZakres.Size = new System.Drawing.Size(45, 21);
            this.lbZakres.Text = "Zakres:";
            // 
            // zakresBindingSource
            // 
            this.zakresBindingSource.DataSource = typeof(InwentaryzacjaModule.DataModel.Zakres);
            // 
            // cbZakres
            // 
            this.cbZakres.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.zakresBindingSource, "Typ", true));
            this.cbZakres.Enabled = false;
            this.cbZakres.Location = new System.Drawing.Point(48, 202);
            this.cbZakres.Name = "cbZakres";
            this.cbZakres.Size = new System.Drawing.Size(189, 22);
            this.cbZakres.TabIndex = 1;
            this.cbZakres.SelectedIndexChanged += new System.EventHandler(this.OnZmienionoZakres);
            // 
            // lbWartoscOd
            // 
            this.lbWartoscOd.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbWartoscOd.Location = new System.Drawing.Point(3, 227);
            this.lbWartoscOd.Name = "lbWartoscOd";
            this.lbWartoscOd.Size = new System.Drawing.Size(45, 21);
            this.lbWartoscOd.Text = "Od:";
            // 
            // tbOd
            // 
            this.tbOd.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zakresBindingSource, "WartoscOd", true));
            this.tbOd.Location = new System.Drawing.Point(48, 226);
            this.tbOd.Name = "tbOd";
            this.tbOd.ReadOnly = true;
            this.tbOd.Size = new System.Drawing.Size(189, 21);
            this.tbOd.TabIndex = 2;
            // 
            // tbDo
            // 
            this.tbDo.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.zakresBindingSource, "WartoscDo", true));
            this.tbDo.Location = new System.Drawing.Point(48, 249);
            this.tbDo.Name = "tbDo";
            this.tbDo.ReadOnly = true;
            this.tbDo.Size = new System.Drawing.Size(189, 21);
            this.tbDo.TabIndex = 3;
            // 
            // lbWartoscDo
            // 
            this.lbWartoscDo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbWartoscDo.Location = new System.Drawing.Point(3, 250);
            this.lbWartoscDo.Name = "lbWartoscDo";
            this.lbWartoscDo.Size = new System.Drawing.Size(45, 21);
            this.lbWartoscDo.Text = "Do:";
            // 
            // ListaInwentaryzacjiView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.tbDo);
            this.Controls.Add(this.lbWartoscDo);
            this.Controls.Add(this.tbOd);
            this.Controls.Add(this.lbWartoscOd);
            this.Controls.Add(this.cbZakres);
            this.Controls.Add(this.lbZakres);
            this.Controls.Add(this.lstListaInwentaryzacji);
            this.Controls.Add(this.lbTytul);
            this.Controls.Add(this.tbUwagi);
            this.Controls.Add(this.lbUwagi);
            this.Name = "ListaInwentaryzacjiView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbUwagi, 0);
            this.Controls.SetChildIndex(this.tbUwagi, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lstListaInwentaryzacji, 0);
            this.Controls.SetChildIndex(this.lbZakres, 0);
            this.Controls.SetChildIndex(this.cbZakres, 0);
            this.Controls.SetChildIndex(this.lbWartoscOd, 0);
            this.Controls.SetChildIndex(this.tbOd, 0);
            this.Controls.SetChildIndex(this.lbWartoscDo, 0);
            this.Controls.SetChildIndex(this.tbDo, 0);
            this.pnlNavigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.zakresBindingSource)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnWyjdz;
		private MSMListView lstListaInwentaryzacji;
		private System.Windows.Forms.ColumnHeader chSymbol;
		private System.Windows.Forms.ColumnHeader chInwentaryzacja;
		private System.Windows.Forms.ColumnHeader chMagazyn;
		private System.Windows.Forms.ColumnHeader chData;
		private MSMLabel lbTytul;
		private System.Windows.Forms.TextBox tbUwagi;
		private System.Windows.Forms.Label lbUwagi;
		private System.Windows.Forms.Button btnWybierz;
		private System.Windows.Forms.Label lbZakres;
		private System.Windows.Forms.ComboBox cbZakres;
		private System.Windows.Forms.Label lbWartoscOd;
		private System.Windows.Forms.TextBox tbOd;
		private System.Windows.Forms.TextBox tbDo;
		private System.Windows.Forms.Label lbWartoscDo;
		private System.Windows.Forms.BindingSource zakresBindingSource;
	}
}
