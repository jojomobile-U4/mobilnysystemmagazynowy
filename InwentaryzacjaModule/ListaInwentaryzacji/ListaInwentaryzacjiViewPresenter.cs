#region

using System;
using System.Windows.Forms;
using Common;
using Common.Base;
using InwentaryzacjaModule.DataModel;

#endregion

namespace InwentaryzacjaModule.ListaInwentaryzacji
{
    public class ListaInwentaryzacjiViewPresenter : PresenterBase
    {
        public const string NAZWA_FORMULARZA = "INWENTARYZACJA - LISTA";
        private const string POMYSLNE_WYWOLANIE_RAPORTU = "Wydruk zosta� wys�any na drukark�.";

        #region Private Fields

        private Naglowek aktualnyNaglowek;
        private ListaNaglowkow listaNaglowkow;
        private bool odswiezanieListyInwentaryzacji;

        #endregion

        #region Properties

        public IListaInwentaryzacjiView View
        {
            get { return m_view as IListaInwentaryzacjiView; }
        }

        protected InwentaryzacjaWorkItem MyWorkItem
        {
            get { return WorkItem as InwentaryzacjaWorkItem; }
        }

        protected string UwagiAktualnegoNaglowka
        {
            get { return aktualnyNaglowek == null ? string.Empty : aktualnyNaglowek.Uwagi; }
        }

        private Zakres AktualnyZakres(TypyZakresow typ)
        {
            var zakres = new Zakres();
            if (aktualnyNaglowek != null)
            {
                zakres.Typ = typ;
                if (typ.Equals(TypyZakresow.LP))
                {
                    zakres.WartoscOd = aktualnyNaglowek.MinLp.ToString();
                    zakres.WartoscDo = aktualnyNaglowek.MaxLp.ToString();
                }
                else
                {
                    zakres.WartoscOd = aktualnyNaglowek.MinLokalizacja;
                    zakres.WartoscDo = aktualnyNaglowek.MaxLokalizacja;
                }
            }
            return zakres;
        }

        #endregion

        #region Constructors

        public ListaInwentaryzacjiViewPresenter(IListaInwentaryzacjiView view) : base(view)
        {
            odswiezanieListyInwentaryzacji = false;
        }

        #endregion

        #region Methods

        public void ZaladujDaneDoWidoku(ListaNaglowkow listaNaglowkow)
        {
            if (listaNaglowkow == null ||
                listaNaglowkow.Lista.Length == 0)
            {
                if (listaNaglowkow == null)
                {
                    listaNaglowkow = new ListaNaglowkow();
                }
                else
                {
                    this.listaNaglowkow = listaNaglowkow;
                }
                listaNaglowkow.Lista = new Naglowek[0];
                aktualnyNaglowek = null;
            }
            else
            {
                this.listaNaglowkow = listaNaglowkow;
                aktualnyNaglowek = this.listaNaglowkow.Lista[0];
            }
            AktualizujWidok();
        }

        #endregion

        protected override void AttachView()
        {
            View.Wybierz += View_Wybierz;
            View.Wyjdz += View_Wyjdz;
            View.ZaznaczonaInwentaryzacjaChanged += View_ZaznaczonaInwentaryzacjaChanged;
            View.ZmienionoZakres += View_ZmienionoZakres;
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Right:
                    e.Handled = true;
                    View.FocusNextControl(true);
                    break;
                case Keys.Left:
                    e.Handled = true;
                    View.FocusNextControl(false);
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    View_Wyjdz(this, EventArgs.Empty);
                    break;
                case Keys.D:
                    e.Handled = true;
                    uruchomRaport();
                    break;
                case Keys.Enter:
                    e.Handled = true;
                    View_Wybierz(this, EventArgs.Empty);
                    break;
            }
        }

        #region Obsluga widoku

        private void View_ZaznaczonaInwentaryzacjaChanged(object sender, EventArgs e)
        {
            if (!odswiezanieListyInwentaryzacji)
            {
                aktualnyNaglowek = View.ZaznaczonaInwentaryzacja;
                View.Uwagi = UwagiAktualnegoNaglowka;
                View.Zakres = AktualnyZakres(View.Zakres.Typ);
                View.WybierzEnabled = aktualnyNaglowek != null;
            }
        }

        private void View_ZmienionoZakres(object sender, EventArgs e)
        {
            View.Zakres = AktualnyZakres(View.Zakres.Typ);
        }


        private void View_Wybierz(object sender, EventArgs e)
        {
            if (aktualnyNaglowek != null)
            {
                View.ForceDataBinding();

                State[StateConstants.RANGE] = View.Zakres;
                MyWorkItem.Wybierz(aktualnyNaglowek);
            }
        }

        private void View_Wyjdz(object sender, EventArgs e)
        {
            CloseView();
        }

        private void AktualizujWidok()
        {
            View.Uwagi = UwagiAktualnegoNaglowka;
            View.WybierzEnabled = aktualnyNaglowek != null;
            AktualizujInwentaryzacje();
            //HACK: reset bindowania zakresu
            // View.Zakres = null;
            View.Zakres = AktualnyZakres(View.Zakres.Typ);
        }

        private void AktualizujInwentaryzacje()
        {
            odswiezanieListyInwentaryzacji = true;
            View.InwentaryzacjeDataSource.Clear();

            if (listaNaglowkow != null)
            {
                Naglowek naglowek;
                for (var i = 0; i < listaNaglowkow.Lista.Length; i++)
                {
                    naglowek = listaNaglowkow.Lista[i];
                    View.InwentaryzacjeDataSource.Add(UtworzWpis(naglowek));
                    View.InwentaryzacjeDataSource[i].Selected = naglowek.Equals(aktualnyNaglowek);
                }
            }
            odswiezanieListyInwentaryzacji = false;
        }

        private ListViewItem UtworzWpis(Naglowek naglowek)
        {
            var elementy = new[]
                               {
                                   naglowek.Symbol,
                                   naglowek.SymbolGM,
                                   naglowek.Magazyn,
                                   naglowek.Data.ToString(Constants.DATE_FORMAT)
                               };
            var wpis = new ListViewItem(elementy);
            wpis.Tag = naglowek;

            return wpis;
        }

        #endregion

        #region Private Methods

        private bool JestLiczba(string tekst)
        {
            try
            {
                int.Parse(tekst);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        private void uruchomRaport()
        {
            if (aktualnyNaglowek != null)
            {
                if (MyWorkItem.UruchomRaport(NAZWA_FORMULARZA, aktualnyNaglowek.Id))
                {
                    MessageBox.Show(POMYSLNE_WYWOLANIE_RAPORTU);
                }
            }
        }

        #endregion
    }
}