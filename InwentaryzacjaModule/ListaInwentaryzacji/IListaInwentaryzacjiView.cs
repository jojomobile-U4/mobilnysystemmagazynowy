#region

using System;
using System.Windows.Forms;
using Common.Base;
using InwentaryzacjaModule.DataModel;

#endregion

namespace InwentaryzacjaModule.ListaInwentaryzacji
{
    public interface IListaInwentaryzacjiView : IViewBase
    {
        string Uwagi { set; }

        Zakres Zakres { get; set; }

        ListView.ListViewItemCollection InwentaryzacjeDataSource { get; }

        Naglowek ZaznaczonaInwentaryzacja { get; }

        bool WybierzEnabled { get; set; }
        event EventHandler Wybierz;
        event EventHandler Wyjdz;
        event EventHandler ZaznaczonaInwentaryzacjaChanged;
        event EventHandler ZmienionoZakres;
    }
}