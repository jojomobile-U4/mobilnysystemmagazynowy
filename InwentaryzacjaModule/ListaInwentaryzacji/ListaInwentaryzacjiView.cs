#region

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Common.Base;
using InwentaryzacjaModule.DataModel;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace InwentaryzacjaModule.ListaInwentaryzacji
{
    [SmartPart]
    public partial class ListaInwentaryzacjiView : ViewBase, IListaInwentaryzacjiView
    {
        #region Private Fields

        private readonly Zakres m_Empty = new Zakres();

        #endregion

        #region Constructors

        public ListaInwentaryzacjiView()
        {
            InitializeComponent();
            InitializeFocusedControl();
            InitializeZakresComboBox();
        }

        #endregion

        #region Protected Methods

        protected void OnZmienionoZakres(object sender, EventArgs e)
        {
            if (ZmienionoZakres != null)
            {
                ZmienionoZakres(sender, e);
            }
        }

        protected void OnWybierz(object sender, EventArgs e)
        {
            if (Wybierz != null)
            {
                Wybierz(sender, e);
            }
        }

        protected void OnWyjdz(object sender, EventArgs e)
        {
            if (Wyjdz != null)
            {
                Wyjdz(sender, e);
            }
        }

        protected void OnZaznaczonaInwentaryzacjaChanged(object sender, EventArgs e)
        {
            if (ZaznaczonaInwentaryzacjaChanged != null)
            {
                ZaznaczonaInwentaryzacjaChanged(sender, e);
            }
        }

        #endregion

        #region IListaInwentaryzacjiView Members

        public event EventHandler Wybierz;

        public event EventHandler Wyjdz;

        public event EventHandler ZaznaczonaInwentaryzacjaChanged;

        public event EventHandler ZmienionoZakres;

        public string Uwagi
        {
            set { tbUwagi.Text = value; }
        }

        public Zakres Zakres
        {
            set { zakresBindingSource.DataSource = value ?? m_Empty; }
            get
            {
                var zakres = new Zakres();
                if (cbZakres.SelectedValue != null)
                    zakres.Typ = (TypyZakresow) cbZakres.SelectedValue;
                zakres.WartoscOd = tbOd.Text;
                zakres.WartoscDo = tbDo.Text;
                return zakres;
            }
        }

        public ListView.ListViewItemCollection InwentaryzacjeDataSource
        {
            get { return lstListaInwentaryzacji.Items; }
        }

        public Naglowek ZaznaczonaInwentaryzacja
        {
            get
            {
                if (lstListaInwentaryzacji.SelectedIndices.Count == 0)
                {
                    return null;
                }

                return lstListaInwentaryzacji.Items[lstListaInwentaryzacji.SelectedIndices[0]].Tag as Naglowek;
            }
        }

        public bool WybierzEnabled
        {
            get { return btnWybierz.Enabled; }
            set { btnWybierz.Enabled = value; }
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);
            cbZakres.Enabled = !navigationState;
            tbDo.ReadOnly = navigationState;
            tbOd.ReadOnly = navigationState;
        }

        #endregion

        #region Private Methods

        private void InitializeZakresComboBox()
        {
            var typyZakresów = new List<TypZakresu> {new TypZakresu(TypyZakresow.LP, "Lp"), new TypZakresu(TypyZakresow.LOKALIZACJA, "Adres")};

            cbZakres.DataSource = typyZakresów;
            cbZakres.DisplayMember = "Opis";
            cbZakres.ValueMember = "Typ";
        }

        #endregion
    }

    internal class TypZakresu
    {
        #region Private Fields

        private readonly string m_Opis;
        private readonly TypyZakresow m_Typ;

        #endregion

        #region Properties

        public string Opis
        {
            get { return m_Opis; }
        }

        public TypyZakresow Typ
        {
            get { return m_Typ; }
        }

        #endregion

        #region Constructors

        public TypZakresu(TypyZakresow typ, string opis)
        {
            m_Opis = opis;
            m_Typ = typ;
        }

        #endregion
    }
}