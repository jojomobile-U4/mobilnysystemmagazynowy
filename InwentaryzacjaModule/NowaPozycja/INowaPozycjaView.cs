#region

using System;
using Common.Base;
using InwentaryzacjaModule.DataModel;

#endregion

namespace InwentaryzacjaModule.NowaPozycja
{
    public interface INowaPozycjaView : IViewBase
    {
        Pozycja DataSource { set; get; }
        event EventHandler Ok;
        event EventHandler Anuluj;
    }
}