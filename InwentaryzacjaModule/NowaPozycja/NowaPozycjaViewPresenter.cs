#region

using System;
using System.Windows.Forms;
using Common.Base;
using InwentaryzacjaModule.DataModel;

#endregion

namespace InwentaryzacjaModule.NowaPozycja
{
    public class NowaPozycjaViewPresenter : PresenterBase
    {
        #region Properties

        public INowaPozycjaView View
        {
            get { return m_view as INowaPozycjaView; }
        }

        protected InwentaryzacjaWorkItem MyWorkItem
        {
            get { return WorkItem as InwentaryzacjaWorkItem; }
        }

        #endregion

        #region Constructors

        public NowaPozycjaViewPresenter(INowaPozycjaView view) : base(view)
        {
            View.DataSource = new Pozycja();
        }

        #endregion

        //#region Private Fields

        //private Pozycja m_Pozycja;

        //#endregion

        protected override void AttachView()
        {
            View.Anuluj += View_Anuluj;
            View.Ok += View_Ok;
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    e.Handled = true;
                    View_Ok(this, EventArgs.Empty);
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    View_Anuluj(this, EventArgs.Empty);
                    break;
                case Keys.Up:
                    e.Handled = true;
                    View.FocusNextControl(false);
                    break;
                case Keys.Down:
                    e.Handled = true;
                    View.FocusNextControl(true);
                    break;
            }
        }

        #region Obsluga widoku

        private void View_Anuluj(object sender, EventArgs e)
        {
            CloseView();
        }

        private void View_Ok(object sender, EventArgs e)
        {
            View.ForceDataBinding();

            if (MyWorkItem.DodajNowaPozycje(View.DataSource))
            {
                CloseView();
            }
        }

        #endregion
    }
}