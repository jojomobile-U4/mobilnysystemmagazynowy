
using Common.Components;
namespace InwentaryzacjaModule.NowaPozycja
{
	partial class NowaPozycjaView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.pozycjaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbLokalizacja = new System.Windows.Forms.TextBox();
            this.lbLokalizacja = new System.Windows.Forms.Label();
            this.tbSymbolTowaru = new System.Windows.Forms.TextBox();
            this.lbSymbolTowaru = new System.Windows.Forms.Label();
            this.btnAnuluj = new System.Windows.Forms.Button();
            this.lbTytul = new Common.Components.MSMLabel();
            this.btnOK = new System.Windows.Forms.Button();
            this.tbIlosc = new System.Windows.Forms.TextBox();
            this.lbIlosc = new System.Windows.Forms.Label();
            this.pnlNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pozycjaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnAnuluj);
            this.pnlNavigation.Controls.Add(this.btnOK);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 272);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 28);
            // 
            // pozycjaBindingSource
            // 
            this.pozycjaBindingSource.DataSource = typeof(InwentaryzacjaModule.DataModel.Pozycja);
            // 
            // tbLokalizacja
            // 
            this.tbLokalizacja.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "Lokalizacja", true));
            this.tbLokalizacja.Location = new System.Drawing.Point(65, 49);
            this.tbLokalizacja.Name = "tbLokalizacja";
            this.tbLokalizacja.Size = new System.Drawing.Size(172, 21);
            this.tbLokalizacja.TabIndex = 1;
            // 
            // lbLokalizacja
            // 
            this.lbLokalizacja.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbLokalizacja.Location = new System.Drawing.Point(3, 53);
            this.lbLokalizacja.Name = "lbLokalizacja";
            this.lbLokalizacja.Size = new System.Drawing.Size(64, 21);
            this.lbLokalizacja.Text = "Lokalizacja:";
            // 
            // tbSymbolTowaru
            // 
            this.tbSymbolTowaru.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "SymbolIndeksu", true));
            this.tbSymbolTowaru.Location = new System.Drawing.Point(65, 25);
            this.tbSymbolTowaru.Name = "tbSymbolTowaru";
            this.tbSymbolTowaru.Size = new System.Drawing.Size(172, 21);
            this.tbSymbolTowaru.TabIndex = 0;
            // 
            // lbSymbolTowaru
            // 
            this.lbSymbolTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbolTowaru.Location = new System.Drawing.Point(3, 28);
            this.lbSymbolTowaru.Name = "lbSymbolTowaru";
            this.lbSymbolTowaru.Size = new System.Drawing.Size(61, 18);
            this.lbSymbolTowaru.Text = "Indeks:";
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnAnuluj.Location = new System.Drawing.Point(162, 4);
            this.btnAnuluj.Name = "btnAnuluj";
            this.btnAnuluj.Size = new System.Drawing.Size(75, 20);
            this.btnAnuluj.TabIndex = 4;
            this.btnAnuluj.Text = "&Esc Anuluj";
            this.btnAnuluj.Click += new System.EventHandler(this.OnAnuluj);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 4;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "INWENTARYZACJA - NOWA POZYCJA";
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnOK.Location = new System.Drawing.Point(83, 4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 20);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "&Ret OK";
            this.btnOK.Click += new System.EventHandler(this.OnOk);
            // 
            // tbIlosc
            // 
            this.tbIlosc.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "Ilosc", true));
            this.tbIlosc.Location = new System.Drawing.Point(65, 73);
            this.tbIlosc.Name = "tbIlosc";
            this.tbIlosc.Size = new System.Drawing.Size(172, 21);
            this.tbIlosc.TabIndex = 2;
            // 
            // lbIlosc
            // 
            this.lbIlosc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbIlosc.Location = new System.Drawing.Point(3, 73);
            this.lbIlosc.Name = "lbIlosc";
            this.lbIlosc.Size = new System.Drawing.Size(61, 21);
            this.lbIlosc.Text = "Ilo��:";
            // 
            // NowaPozycjaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.tbIlosc);
            this.Controls.Add(this.lbIlosc);
            this.Controls.Add(this.lbTytul);
            this.Controls.Add(this.tbLokalizacja);
            this.Controls.Add(this.tbSymbolTowaru);
            this.Controls.Add(this.lbLokalizacja);
            this.Controls.Add(this.lbSymbolTowaru);
            this.Name = "NowaPozycjaView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.lbSymbolTowaru, 0);
            this.Controls.SetChildIndex(this.lbLokalizacja, 0);
            this.Controls.SetChildIndex(this.tbSymbolTowaru, 0);
            this.Controls.SetChildIndex(this.tbLokalizacja, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lbIlosc, 0);
            this.Controls.SetChildIndex(this.tbIlosc, 0);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.pnlNavigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pozycjaBindingSource)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TextBox tbLokalizacja;
		private System.Windows.Forms.Label lbLokalizacja;
		private System.Windows.Forms.TextBox tbSymbolTowaru;
		private System.Windows.Forms.Label lbSymbolTowaru;
		private System.Windows.Forms.Button btnAnuluj;
		private MSMLabel lbTytul;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.TextBox tbIlosc;
		private System.Windows.Forms.Label lbIlosc;
		private System.Windows.Forms.BindingSource pozycjaBindingSource;
	}
}
