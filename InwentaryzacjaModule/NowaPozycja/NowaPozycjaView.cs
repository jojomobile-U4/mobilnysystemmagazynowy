#region

using System;
using Common.Base;
using InwentaryzacjaModule.DataModel;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace InwentaryzacjaModule.NowaPozycja
{
    [SmartPart]
    public partial class NowaPozycjaView : ViewBase, INowaPozycjaView
    {
        public NowaPozycjaView()
        {
            InitializeComponent();
            InitializeFocusedControl();

            //DataEditFocusedControl = tbSymbolTowaru;
        }

        #region Protected Methods

        protected void OnOk(object sender, EventArgs e)
        {
            if (Ok != null)
            {
                Ok(sender, e);
            }
        }

        protected void OnAnuluj(object sender, EventArgs e)
        {
            if (Anuluj != null)
            {
                Anuluj(sender, e);
            }
        }

        #endregion

        #region INowaPozycjaView Members

        public event EventHandler Ok;

        public event EventHandler Anuluj;

        public Pozycja DataSource
        {
            set { pozycjaBindingSource.DataSource = value ?? new Pozycja(); }
            get { return pozycjaBindingSource.DataSource as Pozycja; }
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);

            tbIlosc.ReadOnly = navigationState;
            tbLokalizacja.ReadOnly = navigationState;
            tbSymbolTowaru.ReadOnly = navigationState;
        }

        #endregion
    }
}