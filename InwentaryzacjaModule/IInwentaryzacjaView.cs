#region

using System;
using System.Windows.Forms;
using Common.Base;
using InwentaryzacjaModule.DataModel;

#endregion

namespace InwentaryzacjaModule
{
    public interface IInwentaryzacjaView : IViewBase
    {
        Pozycja DataSource { set; }

        long IloscLp { set; }

        ListView.ListViewItemCollection InwentaryzacjaDataSource { get; }

        bool PoprzedniEnabled { get; set; }

        bool NastepnyEnabled { get; set; }

        float? Ilosc { get; set; }

        string EtykietaPokaz { get; set; }
        event EventHandler NowaPozycja;
        event EventHandler Pokaz;
        event EventHandler Anuluj;
        event EventHandler Nastepny;
        event EventHandler Poprzedni;

        void ZaznaczIlosc();
    }
}