
using Common.Components;
namespace InwentaryzacjaModule
{
	partial class InwentaryzacjaView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.pozycjaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbJm = new System.Windows.Forms.TextBox();
            this.lbJm = new System.Windows.Forms.Label();
            this.tbNazwa = new System.Windows.Forms.TextBox();
            this.lbNazwaTowaru = new System.Windows.Forms.Label();
            this.btnAnuluj = new System.Windows.Forms.Button();
            this.lstInwentaryzacja = new Common.Components.MSMListView();
            this.chSymbol = new System.Windows.Forms.ColumnHeader();
            this.chInwentaryzacja = new System.Windows.Forms.ColumnHeader();
            this.chMagazyn = new System.Windows.Forms.ColumnHeader();
            this.chData = new System.Windows.Forms.ColumnHeader();
            this.lbTytul = new Common.Components.MSMLabel();
            this.btnNowaPozycja = new System.Windows.Forms.Button();
            this.tbLiczbaLp = new System.Windows.Forms.TextBox();
            this.btnPokaz = new System.Windows.Forms.Button();
            this.lbPozycje = new System.Windows.Forms.Label();
            this.lbLP = new System.Windows.Forms.Label();
            this.tbLP = new System.Windows.Forms.TextBox();
            this.lbSlash = new System.Windows.Forms.Label();
            this.lbSymbolTowaru = new System.Windows.Forms.Label();
            this.tbSymbolTowaru = new System.Windows.Forms.TextBox();
            this.lbLokalizacja = new System.Windows.Forms.Label();
            this.tbLokalizacja = new System.Windows.Forms.TextBox();
            this.lbIlosc = new System.Windows.Forms.Label();
            this.tbIlosc = new System.Windows.Forms.TextBox();
            this.lrcNavigation = new Common.Components.LeftRightControl();
            this.pnlNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pozycjaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnNowaPozycja);
            this.pnlNavigation.Controls.Add(this.btnAnuluj);
            this.pnlNavigation.Controls.Add(this.btnPokaz);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 52);
            // 
            // pozycjaBindingSource
            // 
            this.pozycjaBindingSource.DataSource = typeof(InwentaryzacjaModule.DataModel.Pozycja);
            // 
            // tbJm
            // 
            this.tbJm.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "Jm", true));
            this.tbJm.Location = new System.Drawing.Point(65, 198);
            this.tbJm.Name = "tbJm";
            this.tbJm.ReadOnly = true;
            this.tbJm.Size = new System.Drawing.Size(68, 21);
            this.tbJm.TabIndex = 1;
            this.tbJm.TabStop = false;
            // 
            // lbJm
            // 
            this.lbJm.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbJm.Location = new System.Drawing.Point(4, 200);
            this.lbJm.Name = "lbJm";
            this.lbJm.Size = new System.Drawing.Size(56, 21);
            this.lbJm.Text = "J.m.:";
            // 
            // tbNazwa
            // 
            this.tbNazwa.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "NazwaIndeksu", true));
            this.tbNazwa.Location = new System.Drawing.Point(65, 150);
            this.tbNazwa.Name = "tbNazwa";
            this.tbNazwa.ReadOnly = true;
            this.tbNazwa.Size = new System.Drawing.Size(172, 21);
            this.tbNazwa.TabIndex = 14;
            this.tbNazwa.TabStop = false;
            // 
            // lbNazwaTowaru
            // 
            this.lbNazwaTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbNazwaTowaru.Location = new System.Drawing.Point(4, 150);
            this.lbNazwaTowaru.Name = "lbNazwaTowaru";
            this.lbNazwaTowaru.Size = new System.Drawing.Size(55, 20);
            this.lbNazwaTowaru.Text = "Nazwa:";
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnAnuluj.Location = new System.Drawing.Point(121, 29);
            this.btnAnuluj.Name = "btnAnuluj";
            this.btnAnuluj.Size = new System.Drawing.Size(115, 20);
            this.btnAnuluj.TabIndex = 6;
            this.btnAnuluj.Text = "&Esc Wyjd�";
            this.btnAnuluj.Click += new System.EventHandler(this.OnWyjdz);
            // 
            // lstInwentaryzacja
            // 
            this.lstInwentaryzacja.Columns.Add(this.chSymbol);
            this.lstInwentaryzacja.Columns.Add(this.chInwentaryzacja);
            this.lstInwentaryzacja.Columns.Add(this.chMagazyn);
            this.lstInwentaryzacja.Columns.Add(this.chData);
            this.lstInwentaryzacja.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lstInwentaryzacja.FullRowSelect = true;
            this.lstInwentaryzacja.Location = new System.Drawing.Point(0, 20);
            this.lstInwentaryzacja.Name = "lstInwentaryzacja";
            this.lstInwentaryzacja.Size = new System.Drawing.Size(240, 40);
            this.lstInwentaryzacja.TabIndex = 9;
            this.lstInwentaryzacja.TabStop = false;
            this.lstInwentaryzacja.View = System.Windows.Forms.View.Details;
            // 
            // chSymbol
            // 
            this.chSymbol.Text = "Symbol";
            this.chSymbol.Width = 50;
            // 
            // chInwentaryzacja
            // 
            this.chInwentaryzacja.Text = "Inwentaryzacja";
            this.chInwentaryzacja.Width = 75;
            // 
            // chMagazyn
            // 
            this.chMagazyn.Text = "Magazyn";
            this.chMagazyn.Width = 50;
            // 
            // chData
            // 
            this.chData.Text = "Data";
            this.chData.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chData.Width = 62;
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 20;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "INWENTARYZACJA";
            // 
            // btnNowaPozycja
            // 
            this.btnNowaPozycja.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnNowaPozycja.Location = new System.Drawing.Point(3, 6);
            this.btnNowaPozycja.Name = "btnNowaPozycja";
            this.btnNowaPozycja.Size = new System.Drawing.Size(115, 20);
            this.btnNowaPozycja.TabIndex = 2;
            this.btnNowaPozycja.Text = "&1 Nowa Pozycja";
            this.btnNowaPozycja.Click += new System.EventHandler(this.OnNowaPozycja);
            // 
            // tbLiczbaLp
            // 
            this.tbLiczbaLp.Location = new System.Drawing.Point(157, 78);
            this.tbLiczbaLp.Name = "tbLiczbaLp";
            this.tbLiczbaLp.ReadOnly = true;
            this.tbLiczbaLp.Size = new System.Drawing.Size(80, 21);
            this.tbLiczbaLp.TabIndex = 11;
            this.tbLiczbaLp.TabStop = false;
            // 
            // btnPokaz
            // 
            this.btnPokaz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPokaz.Location = new System.Drawing.Point(3, 29);
            this.btnPokaz.Name = "btnPokaz";
            this.btnPokaz.Size = new System.Drawing.Size(115, 20);
            this.btnPokaz.TabIndex = 3;
            this.btnPokaz.Text = "&2 Poka� puste";
            this.btnPokaz.Click += new System.EventHandler(this.OnPokaz);
            // 
            // lbPozycje
            // 
            this.lbPozycje.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbPozycje.Location = new System.Drawing.Point(0, 58);
            this.lbPozycje.Name = "lbPozycje";
            this.lbPozycje.Size = new System.Drawing.Size(240, 16);
            this.lbPozycje.Text = "POZYCJE";
            this.lbPozycje.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbLP
            // 
            this.lbLP.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbLP.Location = new System.Drawing.Point(3, 80);
            this.lbLP.Name = "lbLP";
            this.lbLP.Size = new System.Drawing.Size(62, 20);
            this.lbLP.Text = "LP/Ilo�� LP:";
            // 
            // tbLP
            // 
            this.tbLP.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "Lp", true));
            this.tbLP.Location = new System.Drawing.Point(65, 78);
            this.tbLP.Name = "tbLP";
            this.tbLP.ReadOnly = true;
            this.tbLP.Size = new System.Drawing.Size(80, 21);
            this.tbLP.TabIndex = 10;
            this.tbLP.TabStop = false;
            // 
            // lbSlash
            // 
            this.lbSlash.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSlash.Location = new System.Drawing.Point(147, 80);
            this.lbSlash.Name = "lbSlash";
            this.lbSlash.Size = new System.Drawing.Size(10, 18);
            this.lbSlash.Text = "/";
            // 
            // lbSymbolTowaru
            // 
            this.lbSymbolTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbolTowaru.Location = new System.Drawing.Point(4, 129);
            this.lbSymbolTowaru.Name = "lbSymbolTowaru";
            this.lbSymbolTowaru.Size = new System.Drawing.Size(59, 19);
            this.lbSymbolTowaru.Text = "Indeks:";
            // 
            // tbSymbolTowaru
            // 
            this.tbSymbolTowaru.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "SymbolIndeksu", true));
            this.tbSymbolTowaru.Location = new System.Drawing.Point(65, 126);
            this.tbSymbolTowaru.Name = "tbSymbolTowaru";
            this.tbSymbolTowaru.ReadOnly = true;
            this.tbSymbolTowaru.Size = new System.Drawing.Size(172, 21);
            this.tbSymbolTowaru.TabIndex = 13;
            this.tbSymbolTowaru.TabStop = false;
            // 
            // lbLokalizacja
            // 
            this.lbLokalizacja.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbLokalizacja.Location = new System.Drawing.Point(3, 104);
            this.lbLokalizacja.Name = "lbLokalizacja";
            this.lbLokalizacja.Size = new System.Drawing.Size(61, 20);
            this.lbLokalizacja.Text = "Lokalizacja:";
            // 
            // tbLokalizacja
            // 
            this.tbLokalizacja.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "Lokalizacja", true));
            this.tbLokalizacja.Location = new System.Drawing.Point(65, 102);
            this.tbLokalizacja.Name = "tbLokalizacja";
            this.tbLokalizacja.ReadOnly = true;
            this.tbLokalizacja.Size = new System.Drawing.Size(172, 21);
            this.tbLokalizacja.TabIndex = 12;
            this.tbLokalizacja.TabStop = false;
            // 
            // lbIlosc
            // 
            this.lbIlosc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbIlosc.Location = new System.Drawing.Point(5, 175);
            this.lbIlosc.Name = "lbIlosc";
            this.lbIlosc.Size = new System.Drawing.Size(59, 20);
            this.lbIlosc.Text = "Ilo��:";
            // 
            // tbIlosc
            // 
            this.tbIlosc.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "Ilosc", true));
            this.tbIlosc.Location = new System.Drawing.Point(65, 174);
            this.tbIlosc.Name = "tbIlosc";
            this.tbIlosc.ReadOnly = true;
            this.tbIlosc.Size = new System.Drawing.Size(68, 21);
            this.tbIlosc.TabIndex = 0;
            // 
            // lrcNavigation
            // 
            this.lrcNavigation.BackColor = System.Drawing.SystemColors.Desktop;
            this.lrcNavigation.Location = new System.Drawing.Point(208, 0);
            this.lrcNavigation.Name = "lrcNavigation";
            this.lrcNavigation.NextEnabled = true;
            this.lrcNavigation.PreviousEnabled = true;
            this.lrcNavigation.Size = new System.Drawing.Size(32, 16);
            this.lrcNavigation.TabIndex = 1;
            this.lrcNavigation.TabStop = false;
            this.lrcNavigation.Next += new System.EventHandler(this.OnNastepny);
            this.lrcNavigation.Previous += new System.EventHandler(this.OnPoprzedni);
            // 
            // InwentaryzacjaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lrcNavigation);
            this.Controls.Add(this.lbIlosc);
            this.Controls.Add(this.tbIlosc);
            this.Controls.Add(this.lbSymbolTowaru);
            this.Controls.Add(this.tbSymbolTowaru);
            this.Controls.Add(this.lbLokalizacja);
            this.Controls.Add(this.tbLokalizacja);
            this.Controls.Add(this.lbLP);
            this.Controls.Add(this.tbLP);
            this.Controls.Add(this.lbSlash);
            this.Controls.Add(this.lbPozycje);
            this.Controls.Add(this.tbLiczbaLp);
            this.Controls.Add(this.tbJm);
            this.Controls.Add(this.lbJm);
            this.Controls.Add(this.tbNazwa);
            this.Controls.Add(this.lbNazwaTowaru);
            this.Controls.Add(this.lstInwentaryzacja);
            this.Controls.Add(this.lbTytul);
            this.Name = "InwentaryzacjaView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lstInwentaryzacja, 0);
            this.Controls.SetChildIndex(this.lbNazwaTowaru, 0);
            this.Controls.SetChildIndex(this.tbNazwa, 0);
            this.Controls.SetChildIndex(this.lbJm, 0);
            this.Controls.SetChildIndex(this.tbJm, 0);
            this.Controls.SetChildIndex(this.tbLiczbaLp, 0);
            this.Controls.SetChildIndex(this.lbPozycje, 0);
            this.Controls.SetChildIndex(this.lbSlash, 0);
            this.Controls.SetChildIndex(this.tbLP, 0);
            this.Controls.SetChildIndex(this.lbLP, 0);
            this.Controls.SetChildIndex(this.tbLokalizacja, 0);
            this.Controls.SetChildIndex(this.lbLokalizacja, 0);
            this.Controls.SetChildIndex(this.tbSymbolTowaru, 0);
            this.Controls.SetChildIndex(this.lbSymbolTowaru, 0);
            this.Controls.SetChildIndex(this.tbIlosc, 0);
            this.Controls.SetChildIndex(this.lbIlosc, 0);
            this.Controls.SetChildIndex(this.lrcNavigation, 0);
            this.pnlNavigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pozycjaBindingSource)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TextBox tbJm;
		private System.Windows.Forms.Label lbJm;
		private System.Windows.Forms.TextBox tbNazwa;
		private System.Windows.Forms.Label lbNazwaTowaru;
		private System.Windows.Forms.Button btnAnuluj;
		private MSMListView lstInwentaryzacja;
		private System.Windows.Forms.ColumnHeader chSymbol;
		private System.Windows.Forms.ColumnHeader chInwentaryzacja;
		private System.Windows.Forms.ColumnHeader chMagazyn;
		private System.Windows.Forms.ColumnHeader chData;
		private MSMLabel lbTytul;
		private System.Windows.Forms.Button btnNowaPozycja;
        private System.Windows.Forms.TextBox tbLiczbaLp;
		private System.Windows.Forms.Button btnPokaz;
		private System.Windows.Forms.Label lbPozycje;
		private System.Windows.Forms.Label lbLP;
		private System.Windows.Forms.TextBox tbLP;
		private System.Windows.Forms.Label lbSlash;
		private System.Windows.Forms.Label lbSymbolTowaru;
		private System.Windows.Forms.TextBox tbSymbolTowaru;
		private System.Windows.Forms.Label lbLokalizacja;
		private System.Windows.Forms.TextBox tbLokalizacja;
		private System.Windows.Forms.Label lbIlosc;
		private System.Windows.Forms.TextBox tbIlosc;
		private System.Windows.Forms.BindingSource pozycjaBindingSource;
		private LeftRightControl lrcNavigation;
	}
}
