#region

using Common.DataModel;

#endregion

namespace InwentaryzacjaModule.DataModel
{
    public class Inwentaryzacja
    {
        #region Private Fields

        private ListaPozycji m_Pozycje;

        #endregion

        #region Properties

        public ListaPozycji Pozycje
        {
            get { return m_Pozycje; }
            set { m_Pozycje = value; }
        }

        public Naglowek Naglowek { get; set; }

        public StatusOperacji StatusOperacji { get; set; }

        public WsInwentaryzacjaProxy.Pozycja[] ZmodyfikowanePozycje
        {
            get
            {
                var listaZmodyfikowanych = new ListaPozycji();
                foreach (var pozycja in m_Pozycje.Lista)
                {
                    if (pozycja.Zmodyfikowana)
                    {
                        listaZmodyfikowanych.Lista.Add(pozycja);
                    }
                }

                return listaZmodyfikowanych.ToWSTablicaPozycji();
            }
        }

        #endregion

        #region Constructors

        public Inwentaryzacja()
        {
            Naglowek = new Naglowek();
            m_Pozycje = new ListaPozycji();
            StatusOperacji = new StatusOperacji(StatusOperacji.SUCCESS, string.Empty);
        }

        public Inwentaryzacja(WsInwentaryzacjaProxy.Inwentaryzacja inwentaryzacja) : this()
        {
            if (inwentaryzacja == null)
            {
                StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, "Brak danych");
            }
            else
            {
                Naglowek = new Naglowek(inwentaryzacja.naglowek);
                m_Pozycje = new ListaPozycji(inwentaryzacja.pozycje);
                StatusOperacji = new StatusOperacji(inwentaryzacja.statusOperacji.status, inwentaryzacja.statusOperacji.tekst);
            }
        }

        #endregion
    }
}