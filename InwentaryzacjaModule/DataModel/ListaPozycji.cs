#region

using System.Collections.Generic;

#endregion

namespace InwentaryzacjaModule.DataModel
{
    public class ListaPozycji //: IBindingList
    {
        #region Private Fields

        private List<Pozycja> m_Lista;

        #endregion

        #region Properties

        public List<Pozycja> Lista
        {
            get { return m_Lista; }
            set { m_Lista = value; }
        }

        #endregion

        #region Constructors

        public ListaPozycji()
        {
            m_Lista = new List<Pozycja>();
        }

        public ListaPozycji(WsInwentaryzacjaProxy.Pozycja[] lista) : this()
        {
            if (lista != null)
            {
                for (var i = 0; i < lista.Length; i++)
                {
                    m_Lista.Add(new Pozycja(lista[i]));
                    //nadpisanie rzeczywistych lp, aby nie bylo lp/ilosc lp: 4/2
                    m_Lista[i].Lp = i + 1;
                }
            }
        }

        #endregion

        #region Conversion

        public WsInwentaryzacjaProxy.Pozycja[] ToWSTablicaPozycji()
        {
            if (m_Lista.Count == 0)
            {
                return null;
            }

            var lista = new List<WsInwentaryzacjaProxy.Pozycja>();
            foreach (var pozycja in m_Lista)
            {
                lista.Add(pozycja.ToWSPozycja());
            }

            return lista.ToArray();
        }

        #endregion
    }
}