#region

using Common.DataModel;

#endregion

namespace InwentaryzacjaModule.DataModel
{
    public class NowaPozycja
    {
        #region Private Fields

        private StatusOperacji m_StatusOperacji;

        #endregion

        #region Properties

        public StatusOperacji StatusOperacji
        {
            get { return m_StatusOperacji; }
            set { m_StatusOperacji = value; }
        }

        public Pozycja Pozycja { get; set; }

        #endregion

        #region Constructors

        public NowaPozycja()
        {
            Pozycja = new Pozycja();
            m_StatusOperacji = new StatusOperacji(StatusOperacji.SUCCESS, string.Empty);
        }

        public NowaPozycja(WsInwentaryzacjaProxy.NowaPozycja nowaPozycja) : this()
        {
            if (nowaPozycja == null)
            {
                m_StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, "Brak danych");
            }
            else
            {
                m_StatusOperacji.Status = nowaPozycja.statusOperacji.status;
                m_StatusOperacji.Tekst = nowaPozycja.statusOperacji.tekst;
                Pozycja = new Pozycja(nowaPozycja.pozycja);
            }
        }

        #endregion
    }
}