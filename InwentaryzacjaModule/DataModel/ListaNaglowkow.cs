#region

using Common.DataModel;

#endregion

namespace InwentaryzacjaModule.DataModel
{
    public class ListaNaglowkow
    {
        #region Private Fields

        private Naglowek[] m_lista;

        #endregion

        #region Properties

        public Naglowek[] Lista
        {
            get { return m_lista; }
            set { m_lista = value; }
        }

        public StatusOperacji StatusOperacji { get; set; }

        #endregion

        #region Constructors

        public ListaNaglowkow()
        {
            m_lista = new Naglowek[0];
            StatusOperacji = new StatusOperacji(StatusOperacji.SUCCESS, string.Empty);
        }

        public ListaNaglowkow(WsInwentaryzacjaProxy.ListaNaglowkow lista)
        {
            if (lista == null)
            {
                m_lista = new Naglowek[0];
                StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, "Brak danych");
            }
            else
            {
                if (lista.lista != null)
                {
                    m_lista = new Naglowek[lista.lista.Length];
                    for (var i = 0; i < m_lista.Length; i++)
                    {
                        m_lista[i] = new Naglowek(lista.lista[i]);
                    }
                }
                else
                {
                    m_lista = new Naglowek[0];
                }
                StatusOperacji = new StatusOperacji(lista.statusOperacji.status, lista.statusOperacji.tekst);
            }
        }

        #endregion
    }
}