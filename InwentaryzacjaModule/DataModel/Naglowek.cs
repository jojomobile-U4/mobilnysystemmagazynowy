#region

using System;

#endregion

namespace InwentaryzacjaModule.DataModel
{
    public class Naglowek
    {
        #region Private Fields

        #endregion

        #region Properties

        public long LiczbaPozycji { get; set; }

        public string Uwagi { get; set; }

        public DateTime Data { get; set; }

        public string Magazyn { get; set; }

        public string SymbolGM { get; set; }

        public string Symbol { get; set; }

        public long Id { get; set; }

        public int MinLp { get; set; }

        public int MaxLp { get; set; }

        public string MinLokalizacja { get; set; }

        public string MaxLokalizacja { get; set; }

        #endregion

        #region Constructors

        public Naglowek()
        {
        }

        public Naglowek(WsInwentaryzacjaProxy.Naglowek naglowek)
        {
            if (naglowek != null)
            {
                Data = naglowek.dataWystawienia;
                Id = naglowek.id;
                //TODO: zamiast maga_id zwracac nazwe lub kod magazynu
                Magazyn = naglowek.magazyn.ToString();
                Symbol = naglowek.symbol;
                SymbolGM = naglowek.kodInwentaryzacji;
                Uwagi = naglowek.uwagi;
                LiczbaPozycji = naglowek.iloscPozycji;
                MinLp = naglowek.minLp;
                MaxLp = naglowek.maxLp;
                MinLokalizacja = naglowek.minLokalizacja;
                MaxLokalizacja = naglowek.maxLokalizacja;
            }
        }

        #endregion

        //#region Conversion

        //public WsInwentaryzacjaProxy.Naglowek ToWSNaglowek()
        //{
        //    WsInwentaryzacjaProxy.Naglowek wynik = new InwentaryzacjaModule.WsInwentaryzacjaProxy.Naglowek();
        //    wynik.data = m_Data;
        //    wynik.iD = m_Id;
        //    wynik.magazyn = m_Magazyn;
        //    wynik.symbol = m_Symbol;
        //    wynik.symbolGM = m_SymbolGM;
        //    wynik.uwagi = m_Uwagi;

        //    return wynik;
        //}

        //#endregion
    }
}