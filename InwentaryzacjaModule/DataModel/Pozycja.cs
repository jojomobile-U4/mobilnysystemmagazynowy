#region

using Common.DataModel;

#endregion

namespace InwentaryzacjaModule.DataModel
{
    public class Pozycja
    {
        #region Private Fields

        private long m_Id;
        private float? m_Ilosc;
        private float? m_IloscPoczatkowa;
        private string m_Jm;
        private string m_Lokalizacja;
        private int m_Lp;
        private string m_NazwaIndeksu;
        private string m_SymbolIndeksu;

        #endregion

        #region Properties

        public int Lp
        {
            get { return m_Lp; }
            set { m_Lp = value; }
        }

        public float? Ilosc
        {
            get { return m_Ilosc; }
            set { m_Ilosc = value; }
        }

        public string Lokalizacja
        {
            get { return m_Lokalizacja; }
            set { m_Lokalizacja = value; }
        }

        public string Jm
        {
            get { return m_Jm; }
            set { m_Jm = value; }
        }

        public string NazwaIndeksu
        {
            get { return m_NazwaIndeksu; }
            set { m_NazwaIndeksu = value; }
        }

        public string SymbolIndeksu
        {
            get { return m_SymbolIndeksu; }
            set { m_SymbolIndeksu = value; }
        }

        public long Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        public bool Zmodyfikowana
        {
            get
            {
                if (m_Ilosc.HasValue)
                {
                    if (m_IloscPoczatkowa.HasValue)
                    {
                        return m_Ilosc.Value != m_IloscPoczatkowa.Value;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    if (m_IloscPoczatkowa.HasValue)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }

        #endregion

        #region Constructors

        public Pozycja()
        {
        }

        public Pozycja(WsInwentaryzacjaProxy.Pozycja pozycja)
        {
            if (pozycja != null)
            {
                m_Id = pozycja.id;
                m_Ilosc = pozycja.ilosc;
                m_IloscPoczatkowa = pozycja.ilosc;
                m_Jm = pozycja.jednostkaMiary;
                m_Lokalizacja = Localization.RemovePrefix(pozycja.kodLokalizacji);
                m_Lp = pozycja.lp;
                m_NazwaIndeksu = pozycja.nazwaTowaru;
                m_SymbolIndeksu = pozycja.symbolTowaru;
            }
        }

        #endregion

        #region Conversion

        public WsInwentaryzacjaProxy.Pozycja ToWSPozycja()
        {
            var wynik = new WsInwentaryzacjaProxy.Pozycja();
            wynik.kodLokalizacji = Localization.AddPrefix(m_Lokalizacja);
            //TODO: to trzeba zmienic, w WS musi byc float?
            wynik.ilosc = m_Ilosc != null ? (float) m_Ilosc : 0;
            wynik.jednostkaMiary = m_Jm;
            wynik.lp = m_Lp;
            wynik.nazwaTowaru = m_NazwaIndeksu;
            wynik.symbolTowaru = m_SymbolIndeksu;
            wynik.id = m_Id;

            return wynik;
        }

        #endregion
    }
}