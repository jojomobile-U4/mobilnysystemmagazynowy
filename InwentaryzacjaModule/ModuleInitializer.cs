#region

using System;
using Common;
using InwentaryzacjaModule.DataModel;
using InwentaryzacjaModule.ListaInwentaryzacji;
using InwentaryzacjaModule.NowaPozycja;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.Commands;

#endregion

namespace InwentaryzacjaModule
{
    public class ModuleInitializer : ModuleInit
    {
        private readonly WorkItem workItem;

        public ModuleInitializer([ServiceDependency] WorkItem workItem)
        {
            this.workItem = workItem;
        }

        public override void Load()
        {
            base.Load();
        }

        private void Initialize()
        {
            try
            {
                FunctionsHelper.SetCursorWait();


                if (!workItem.WorkItems.Contains(WorkItemsConstants.INWENTARYZACJA_WORKITEM))
                {
                    var inwentaryzacjaWorkItem = workItem.WorkItems.AddNew<InwentaryzacjaWorkItem>(WorkItemsConstants.INWENTARYZACJA_WORKITEM);

                    var zakres = new Zakres();
                    zakres.Typ = TypyZakresow.LP;
                    inwentaryzacjaWorkItem.State[StateConstants.RANGE] = zakres;

                    var listaPresenter = new ListaInwentaryzacjiViewPresenter(inwentaryzacjaWorkItem.Items.AddNew<ListaInwentaryzacjiView>());
                    inwentaryzacjaWorkItem.Items.Add(listaPresenter, ItemsConstants.LISTA_INWENTARYZACJI_PRESENTER);

                    var nowaPozycjaPresenter = new NowaPozycjaViewPresenter(inwentaryzacjaWorkItem.Items.AddNew<NowaPozycjaView>());
                    inwentaryzacjaWorkItem.Items.Add(nowaPozycjaPresenter, ItemsConstants.NOWA_POZYCJA_INWENTARYZACJI_PRESENTER);

                    var inwentaryzacjaPresenter = new InwentaryzacjaViewPresenter(inwentaryzacjaWorkItem.Items.AddNew<InwentaryzacjaView>());
                    inwentaryzacjaWorkItem.Items.Add(inwentaryzacjaPresenter, ItemsConstants.INWENTARYAZCJA_PRESENTER);
                }

                workItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        [CommandHandler(CommandConstants.INWENTARYZACJA)]
        public void Inwentaryzacja(object sender, EventArgs e)
        {
            Initialize();
            var inwentaryzacjaWorkItem = workItem.WorkItems.Get<InwentaryzacjaWorkItem>(WorkItemsConstants.INWENTARYZACJA_WORKITEM);
            inwentaryzacjaWorkItem.Show();
        }
    }
}