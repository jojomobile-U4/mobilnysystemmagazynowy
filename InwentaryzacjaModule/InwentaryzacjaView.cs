#region

using System;
using System.Windows.Forms;
using Common.Base;
using InwentaryzacjaModule.DataModel;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace InwentaryzacjaModule
{
    [SmartPart]
    public partial class InwentaryzacjaView : ViewBase, IInwentaryzacjaView
    {
        #region Private Fields

        private readonly Pozycja m_Empty;

        #endregion

        #region Constructors

        public InwentaryzacjaView()
        {
            InitializeComponent();
            InitializeFocusedControl();

            m_Empty = new Pozycja();
        }

        #endregion

        #region Protected Methods

        protected void OnNowaPozycja(object sender, EventArgs e)
        {
            if (NowaPozycja != null)
            {
                NowaPozycja(sender, e);
            }
        }

        protected void OnPokaz(object sender, EventArgs e)
        {
            if (Pokaz != null)
            {
                Pokaz(sender, e);
            }
        }

        protected void OnPoprzedni(object sender, EventArgs e)
        {
            if (Poprzedni != null)
            {
                Poprzedni(sender, e);
            }
        }

        protected void OnNastepny(object sender, EventArgs e)
        {
            if (Nastepny != null)
            {
                Nastepny(sender, e);
            }
        }

        protected void OnWyjdz(object sender, EventArgs e)
        {
            if (Anuluj != null)
            {
                Anuluj(sender, e);
            }
        }

        #endregion

        #region IInwentaryzacjaView Members

        public event EventHandler NowaPozycja;

        public event EventHandler Pokaz;

        public event EventHandler Anuluj;

        public event EventHandler Nastepny;

        public event EventHandler Poprzedni;

        public Pozycja DataSource
        {
            set { pozycjaBindingSource.DataSource = value ?? m_Empty; }
        }

        public long IloscLp
        {
            set { tbLiczbaLp.Text = value.ToString(); }
        }

        public ListView.ListViewItemCollection InwentaryzacjaDataSource
        {
            get { return lstInwentaryzacja.Items; }
        }

        public bool PoprzedniEnabled
        {
            get { return lrcNavigation.PreviousEnabled; }
            set { lrcNavigation.PreviousEnabled = value; }
        }

        public bool NastepnyEnabled
        {
            get { return lrcNavigation.NextEnabled; }
            set { lrcNavigation.NextEnabled = value; }
        }

        public string EtykietaPokaz
        {
            get { return btnPokaz.Text; }
            set { btnPokaz.Text = value; }
        }

        public float? Ilosc
        {
            get
            {
                try
                {
                    return float.Parse(tbIlosc.Text);
                }
                catch (FormatException)
                {
                    return null;
                }
            }
            set { tbIlosc.Text = value.ToString(); }
        }

        public void ZaznaczIlosc()
        {
            tbIlosc.SelectAll();
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);

            tbIlosc.ReadOnly = navigationState;
        }

        #endregion
    }
}