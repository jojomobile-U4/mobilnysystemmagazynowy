#region

using Common.DataModel;
using InwentaryzacjaModule.DataModel;

#endregion

namespace InwentaryzacjaModule.Services
{
    public interface IInwentaryzacjaService
    {
        ListaNaglowkow PobierzListeNaglowkow();
        Inwentaryzacja PobierzInwentaryzacje(long idInwentaryzacji, bool tylkoPuste, string lokalizacja, Zakres zakres, int? numerStrony);
        DataModel.NowaPozycja DodajPozycje(Pozycja pozycja, long idInwentaryzacji);
        StatusOperacji AktualizujPozycje(Pozycja pozycja);
        void UruchomRaport(string nazwaFormularza, long idInwentaryzacji);
        //TypyZakresow PobierzTypyZakresow();
    }
}