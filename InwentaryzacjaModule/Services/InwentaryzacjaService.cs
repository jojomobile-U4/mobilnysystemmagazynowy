#region

using System;
using Common;
using Common.Base;
using Common.DataModel;
using InwentaryzacjaModule.DataModel;
using InwentaryzacjaModule.ProxyWrappers;
using Microsoft.Practices.Mobile.CompositeUI;

#endregion

namespace InwentaryzacjaModule.Services
{
    [Service(typeof (IInwentaryzacjaService))]
    public class InwentaryzacjaService : ServiceBase, IInwentaryzacjaService
    {
        #region Properties

        protected override WebServiceConfiguration Configuration
        {
            get { return MyWorkItem.Configuration.InwentaryzacjaWS; }
        }

        #endregion

        #region Constructors

        public InwentaryzacjaService([ServiceDependency] WorkItem workItem) : base(workItem)
        {
        }

        #endregion

        #region IInwentaryzacjaService Members

        public ListaNaglowkow PobierzListeNaglowkow()
        {
            var listaNaglowkow = new ListaNaglowkow();
            try
            {
                using (var serviceAgent = new WsInwentaryzacjaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzListeNaglowkowWrap();
                    param.numerStrony = 0;
                    listaNaglowkow = new ListaNaglowkow(serviceAgent.pobierzListeNaglowkow(param).@return);
                }
            }
            catch (Exception ex)
            {
                //TODO: dodac zapisywanie do logow
                listaNaglowkow.StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, ex.Message);
            }
            return listaNaglowkow;
        }

        public Inwentaryzacja PobierzInwentaryzacje(long idInwentaryzacji, bool tylkoPuste, string lokalizacja, Zakres zakres, int? numerStrony)
        {
            var inwentaryzacja = new Inwentaryzacja();
            try
            {
                using (var serviceAgent = new WsInwentaryzacjaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzInwentaryzacjeWrap();
                    param.id = idInwentaryzacji;
                    param.lokalizacja = Localization.AddPrefix(lokalizacja);
                    param.numerStrony = numerStrony ?? 0;

                    if (tylkoPuste)
                    {
                        param.zWprowadzonaIloscia = DBBool.FALSE;
                    }

                    param.wielkoscStrony = Configuration.WielkoscStrony;
                    if (zakres != null)
                    {
                        if (TypyZakresow.LP.Equals(zakres.Typ))
                        {
                            param.lpOd = string.IsNullOrEmpty(zakres.WartoscOd) ? null : (int?) int.Parse(zakres.WartoscOd);
                            param.lpDo = string.IsNullOrEmpty(zakres.WartoscDo) ? null : (int?) int.Parse(zakres.WartoscDo);
                        }
                        else
                        {
                            param.adresOd = Localization.AddPrefix(zakres.WartoscOd);
                            param.adresDo = Localization.AddPrefix(zakres.WartoscDo);
                        }
                        param.typZakresu = zakres.Typ.ToString();
                    }
                    inwentaryzacja = new Inwentaryzacja(serviceAgent.pobierzInwentaryzacje(param).@return);
                }
            }
            catch (Exception ex)
            {
                //TODO: dodac zapisywanie do logow
                inwentaryzacja.StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, ex.Message);
            }
            return inwentaryzacja;
        }

        public DataModel.NowaPozycja DodajPozycje(Pozycja pozycja, long idInwentaryzacji)
        {
            if (pozycja == null)
            {
                return new DataModel.NowaPozycja(null);
            }

            var nowaPozycja = new DataModel.NowaPozycja();
            try
            {
                using (var serviceAgent = new WsInwentaryzacjaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new dodajPozycjeWrap();
                    param.lokalizacja = pozycja.Lokalizacja;
                    param.ilosc = pozycja.Ilosc;
                    param.symbolTowaru = pozycja.SymbolIndeksu;
                    param.idInwentaryzacji = idInwentaryzacji;

                    nowaPozycja = new DataModel.NowaPozycja(serviceAgent.dodajPozycje(param).@return);
                }
            }
            catch (Exception ex)
            {
                //TODO: dodac zapisywanie do logow
                nowaPozycja.StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, ex.Message);
            }

            return nowaPozycja;
        }

        public StatusOperacji AktualizujPozycje(Pozycja pozycja)
        {
            var statusOperacji = new StatusOperacji(StatusOperacji.SUCCESS);
            try
            {
                using (var serviceAgent = new WsInwentaryzacjaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new aktualizujPozycjeWrap();
                    param.pozycja = pozycja.ToWSPozycja();

                    var statusOperacjiWs = serviceAgent.aktualizujPozycje(param).@return;
                    statusOperacji = new StatusOperacji(statusOperacjiWs.status, statusOperacjiWs.tekst, statusOperacjiWs.stosWywolan);
                }
            }
            catch (Exception ex)
            {
                //TODO: dodac zapisywanie do logow
                statusOperacji = new StatusOperacji(StatusOperacji.ERROR, ex.Message);
            }
            return statusOperacji;
        }


        public void UruchomRaport(string nazwaFormularza, long idInwentaryzacji)
        {
            try
            {
                using (var serviceAgent = new WsInwentaryzacjaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new uruchomRaportWrap();
                    param.nazwaFormularza = nazwaFormularza;
                    param.idInwentaryzacji = idInwentaryzacji;


                    var statusOperacji = serviceAgent.uruchomRaport(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = statusOperacji.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        #endregion

        //public TypyZakresow PobierzTypyZakresow()
        //{
        //    using (WsInwentaryzacjaProxy.WsInwentaryzacja serviceAgent = new InwentaryzacjaModule.WsInwentaryzacjaProxy.WsInwentaryzacja())
        //    {
        //        TypyZakresow typy = new TypyZakresow();
        //        try
        //        {
        //            typy = new TypyZakresow(serviceAgent.TypyZakresow().@return);
        //        }
        //        catch (Exception ex)
        //        {
        //            typy.StatusOperacji.Status = StatusOperacji.ERROR;
        //            typy.StatusOperacji.Tekst = ex.Message;
        //        }
        //        return typy;
        //    }
        //}
    }
}