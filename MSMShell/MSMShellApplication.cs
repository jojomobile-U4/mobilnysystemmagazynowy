#region

using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;
using Common;
using Microsoft.Practices.Mobile.CompositeUI.WinForms;
using Microsoft.Win32;
using MobilnySystemMagazynowy.Administration;
using MobilnySystemMagazynowy.Administration.Login;
using WydanieMRModule.WyborStrefy;

#endregion

namespace MobilnySystemMagazynowy
{
    internal class MSMShellApplication : FormShellApplication<MSMWorkItem, MSMShellForm>
    {
        #region Constants

        private const string APPLICATION_LOG_FILE = "App.log";
        private const string DATE_FORMAT = "dd-MM-yyyy hh:mm:ss";
        private const string DEFAULT_APPLICATION_FOLDER = @"\Program Files\Mobilny System Magazynowania";
        private const string DEFAULT_CONFIG_FILE_NAME = "config.xml";
        private const string NEW_LINE = "\n";
        private const string REGISTRY_KEY = @"\Software\TETA\MSM";
        private const string REGISTRY_VALUE_APPLICATION_FOLDER = "InstallDir";
        private const string REGISTRY_VALUE_CONFIG_FILE_NAME = "ConfigFileName";
        private const string SECOND_APP_INFO = "Aplikacja jest ju� uruchomiona.";

        #endregion

        #region Private Fields

        private static FileStream lockFile;
        private string applicationFolder = DEFAULT_APPLICATION_FOLDER;
        private string configFileName = DEFAULT_CONFIG_FILE_NAME;
        private Configuration configuration;
        private SplashScreen.SplashScreen splashScreen;

        #endregion

        #region Methods

        public void Uruchom()
        {
            try
            {
                OdczytajRejestr();
                splashScreen = new SplashScreen.SplashScreen();
                splashScreen.Show();
                FunctionsHelper.SetCursorWait();

                if (LockLogFile())
                {
                    OdczytajKonfiguracje();
                    //There isn't another instance. Show our form.
                    Run();
                }
                else
                {
                    MessageBox.Show(SECOND_APP_INFO);
                }
            }
            catch (Exception e)
            {
                HandleException(e);
                if (Shell != null)
                {
                    Shell.Close();
                }
                Application.Exit();
            }
            finally
            {
                UnlockLogFile();
            }
        }

        #endregion

        #region Private Methods

        private void UnlockLogFile()
        {
            if (lockFile != null)
            {
                lockFile.Close();
            }
        }

        private bool LockLogFile()
        {
            var lockFileName = Path.Combine(applicationFolder, APPLICATION_LOG_FILE);

            try
            {
                lockFile = File.Open(lockFileName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);

                return true;
            }
            catch (IOException)
            {
                return false;
            }
        }

        private void InitializeAdministration()
        {
            var administrationWorkItem = RootWorkItem.WorkItems.AddNew<AdministrationWorkItem>(WorkItemsConstants.ADMINISTRATION_WORKITEM);

            var loginPresenter = new LoginViewPresenter(administrationWorkItem.Items.AddNew<LoginView>());
            administrationWorkItem.Items.Add(loginPresenter, ItemsConstants.LOGIN_PRESENTER);

            var roleSelectionPresenter = new RoleSelectionViewPresenter(administrationWorkItem.Items.AddNew<RoleSelectionView>());
            administrationWorkItem.Items.Add(roleSelectionPresenter, ItemsConstants.ROLE_SELECTION_PRESENTER);
        }

        private void OdczytajRejestr()
        {
            applicationFolder = Registry.GetValue(Registry.LocalMachine.Name + REGISTRY_KEY, REGISTRY_VALUE_APPLICATION_FOLDER, DEFAULT_APPLICATION_FOLDER) as string ?? DEFAULT_APPLICATION_FOLDER;
            configFileName = Registry.GetValue(Registry.LocalMachine.Name + REGISTRY_KEY, REGISTRY_VALUE_CONFIG_FILE_NAME, DEFAULT_CONFIG_FILE_NAME) as string ?? DEFAULT_CONFIG_FILE_NAME;
        }

        private void OdczytajKonfiguracje()
        {
            var fullPath = Path.Combine(applicationFolder, configFileName);
            if (File.Exists(fullPath))
            {
                try
                {
                    var file = new FileStream(fullPath, FileMode.Open);
                    var xmlSerializer = new XmlSerializer(typeof (Configuration));
                    configuration = xmlSerializer.Deserialize(file) as Configuration;
                    file.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("B��d odczytu pliku konfiguracyjnego.", "B��d");
                    configuration = Common.Configuration.StandardConfiguration;
                }
            }
            else
            {
                configuration = Common.Configuration.StandardConfiguration;
            }

            Constants.LOCALIZATION_PREFIX = configuration.PrefiksLokalizacji;
        }

        #endregion

        #region Override Methods

        protected override void AfterShellCreated()
        {
            base.AfterShellCreated();

            //HACK: aby Common.DataModel.Localization wiedzialo o aktualnym prefiksie
            Constants.LOCALIZATION_PREFIX = configuration.PrefiksLokalizacji;

            RootWorkItem.State[StateConstants.CONFIGURATION] = configuration;
            InitializeAdministration();
        }

        protected override void Start()
        {
            base.Start();
            FunctionsHelper.SetCursorDefault();
            splashScreen.Close();
        }

        #endregion

        #region Unhandled Exception

        private void HandleException(Exception ex)
        {
            var message = string.Empty;
            using (var fStream = new FileStream(Path.Combine(applicationFolder, "ExceptionLOG.txt"), FileMode.Append))
            {
                var fileWriter = new StreamWriter(fStream);

                fileWriter.WriteLine();
                fileWriter.WriteLine(DateTime.Now.ToString(DATE_FORMAT));
                if (ex != null)
                {
                    message = BuildExceptionString(ex);
                    fileWriter.Write(message);
                }
                else
                {
                    message = "An Exception has occured, unable to get details";
                    fileWriter.Write(message);
                }
                fileWriter.Flush();
            }

            MessageBox.Show(message);
            //Application.Exit();
        }

        private string BuildExceptionString(Exception exception)
        {
            var errMessage = string.Empty;

            errMessage += exception.Message + NEW_LINE + exception.StackTrace;

            while (exception.InnerException != null)
            {
                errMessage += BuildInnerExceptionString(exception.InnerException);
                exception = exception.InnerException;
            }

            return errMessage;
        }

        private string BuildInnerExceptionString(Exception innerException)
        {
            var errMessage = string.Empty;

            errMessage += NEW_LINE + " InnerException ";
            errMessage += NEW_LINE + innerException.Message + NEW_LINE + innerException.StackTrace;

            return errMessage;
        }

        #endregion

        #region Application's Main Entry

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        private static void Main()
        {
            new MSMShellApplication().Uruchom();
        }

        #endregion
    }
}