#region

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

#endregion

namespace MobilnySystemMagazynowy.AdministracjaProxy
{
    /// <remarks/>
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [WebServiceBinding(Name = "WsAdministracjaSOAP11Binding", Namespace = "http://mobilnySystemMagazynowy.teta.com")]
    public class WsAdministracja : SoapHttpClientProtocol
    {
        /// <remarks/>
        public WsAdministracja()
        {
            this.Url = "http://SERVICES:8080/axis2/services/WsAdministracja";
        }

        /// <remarks/>
        [SoapDocumentMethod("urn:zaloguj", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("zalogujResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        public zalogujResponse zaloguj([XmlElement("zaloguj", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] zaloguj zaloguj1)
        {
            var results = this.Invoke("zaloguj", new object[]
                                                     {
                                                         zaloguj1
                                                     });
            return ((zalogujResponse) (results[0]));
        }

        /// <remarks/>
        public IAsyncResult Beginzaloguj(zaloguj zaloguj1, AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("zaloguj", new object[]
                                                   {
                                                       zaloguj1
                                                   }, callback, asyncState);
        }

        /// <remarks/>
        public zalogujResponse Endzaloguj(IAsyncResult asyncResult)
        {
            var results = this.EndInvoke(asyncResult);
            return ((zalogujResponse) (results[0]));
        }

        /// <remarks/>
        [SoapDocumentMethod("urn:pobierzListeRol", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeRolResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        public pobierzListeRolResponse pobierzListeRol([XmlElement("pobierzListeRol", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeRol pobierzListeRol1)
        {
            var results = this.Invoke("pobierzListeRol", new object[]
                                                             {
                                                                 pobierzListeRol1
                                                             });
            return ((pobierzListeRolResponse) (results[0]));
        }

        /// <remarks/>
        public IAsyncResult BeginpobierzListeRol(pobierzListeRol pobierzListeRol1, AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("pobierzListeRol", new object[]
                                                           {
                                                               pobierzListeRol1
                                                           }, callback, asyncState);
        }

        /// <remarks/>
        public pobierzListeRolResponse EndpobierzListeRol(IAsyncResult asyncResult)
        {
            var results = this.EndInvoke(asyncResult);
            return ((pobierzListeRolResponse) (results[0]));
        }

        /// <remarks/>
        [SoapDocumentMethod("urn:pobierzListeFunkcjonalnosci", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeFunkcjonalnosciResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        public pobierzListeFunkcjonalnosciResponse pobierzListeFunkcjonalnosci([XmlElement("pobierzListeFunkcjonalnosci", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeFunkcjonalnosci pobierzListeFunkcjonalnosci1)
        {
            var results = this.Invoke("pobierzListeFunkcjonalnosci", new object[]
                                                                         {
                                                                             pobierzListeFunkcjonalnosci1
                                                                         });
            return ((pobierzListeFunkcjonalnosciResponse) (results[0]));
        }

        /// <remarks/>
        public IAsyncResult BeginpobierzListeFunkcjonalnosci(pobierzListeFunkcjonalnosci pobierzListeFunkcjonalnosci1, AsyncCallback callback, object asyncState)
        {
            return this.BeginInvoke("pobierzListeFunkcjonalnosci", new object[]
                                                                       {
                                                                           pobierzListeFunkcjonalnosci1
                                                                       }, callback, asyncState);
        }

        /// <remarks/>
        public pobierzListeFunkcjonalnosciResponse EndpobierzListeFunkcjonalnosci(IAsyncResult asyncResult)
        {
            var results = this.EndInvoke(asyncResult);
            return ((pobierzListeFunkcjonalnosciResponse) (results[0]));
        }
    }

    /// <remarks/>
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
    public class zaloguj
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)] public string haslo;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public string login;
    }

    /// <remarks/>
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://dto.administracja.mobilnySystemMagazynowy.teta.com/xsd")]
    public class ListaFunkcjonalnosci
    {
        /// <remarks/>
        public long iloscFunkcjonalnosci;

        /// <remarks/>
        [XmlElement("lista", IsNullable = true)] public Funkcjonalnosc[] lista;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public StatusOperacji statusOperacji;
    }

    /// <remarks/>
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://dto.administracja.mobilnySystemMagazynowy.teta.com/xsd")]
    public class Funkcjonalnosc
    {
        /// <remarks/>
        public long id;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public string nazwa;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public string opis;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public StatusOperacji statusOperacji;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public string system;
    }

    /// <remarks/>
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://dto.administracja.mobilnySystemMagazynowy.teta.com/xsd")]
    public class StatusOperacji
    {
        /// <remarks/>
        public int numer;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public string status;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public string stosWywolan;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public string tekst;
    }

    /// <remarks/>
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://dto.administracja.mobilnySystemMagazynowy.teta.com/xsd")]
    public class Rola
    {
        /// <remarks/>
        public long id;

        /// <remarks/>
        [XmlElement("listaFunkcjonalnosci", IsNullable = true)] public Funkcjonalnosc[] listaFunkcjonalnosci;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public string nazwa;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public string opis;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public StatusOperacji statusOperacji;
    }

    /// <remarks/>
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://dto.administracja.mobilnySystemMagazynowy.teta.com/xsd")]
    public class ListaRol
    {
        /// <remarks/>
        public long iloscRol;

        /// <remarks/>
        [XmlElement("lista", IsNullable = true)] public Rola[] lista;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public StatusOperacji statusOperacji;
    }

    /// <remarks/>
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(Namespace = "http://dto.administracja.mobilnySystemMagazynowy.teta.com/xsd")]
    public class StanZalogowania
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)] public string jestZalogowany;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public StatusOperacji statusOperacji;
    }

    /// <remarks/>
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
    public class zalogujResponse
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)] public StanZalogowania @return;
    }

    /// <remarks/>
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
    public class pobierzListeRol
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)] public string nazwaRoli;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public string nazwaUzytkownika;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public int? numerStrony;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public int? wielkoscStrony;
    }

    /// <remarks/>
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
    public class pobierzListeRolResponse
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)] public ListaRol @return;
    }

    /// <remarks/>
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
    public class pobierzListeFunkcjonalnosci
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)] public string nazwaFunkcjonalnosci;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public string nazwaRoli;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public int? numerStrony;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public string system;

        /// <remarks/>
        [XmlElement(IsNullable = true)] public int? wielkoscStrony;
    }

    /// <remarks/>
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true, Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
    public class pobierzListeFunkcjonalnosciResponse
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)] public ListaFunkcjonalnosci @return;
    }
}