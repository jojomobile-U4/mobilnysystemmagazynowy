#region

using System;
using System.Drawing;
using System.Windows.Forms;
using Common;
using Common.Images;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.Commands;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.Services;
using Microsoft.Practices.Mobile.ObjectBuilder;

#endregion

namespace MobilnySystemMagazynowy
{
    public partial class MSMShellForm : Form
    {
        private readonly MSMWorkItem workItem;
        private IWorkItemTypeCatalogService workItemTypeCatalog;

        public MSMShellForm()
        {
            InitializeComponent();
            //wlaczenie odczytu zegara
            tClock.Enabled = true;
            pbAsyncDownload.Image = ImageHelper.PobieranieDanych;
        }

        [InjectionConstructor]
        public MSMShellForm(WorkItem workItem, IWorkItemTypeCatalogService workItemTypeCatalog)
            : this()
        {
            this.workItem = workItem as MSMWorkItem;
            this.workItem.NetConnectionStatusChanged += workItem_NetConnectionStatusChanged;
            this.workItem.NavigationStateChanged += workItem_NavigationStateChanged;
            this.workItemTypeCatalog = workItemTypeCatalog;
        }

        private bool IsConnected
        {
            set { pbNetStatus.Image = value ? ImageHelper.Polaczenie : ImageHelper.BrakPolaczenia; }
        }

        private bool IsInNavigationState
        {
            set { pbNavigationState.Image = value ? ImageHelper.TrybNawigacji : ImageHelper.TrybEdycji; }
        }

        private void workItem_NavigationStateChanged(object sender, EventArgs e)
        {
            if (workItem != null)
            {
                IsInNavigationState = (bool) (workItem.RootWorkItem.State[StateConstants.NAVIGATION_STATE] ?? false);
            }
        }

        private void workItem_NetConnectionStatusChanged(object sender, EventArgs e)
        {
            if (workItem != null)
            {
                IsConnected = (bool) (workItem.State[StateConstants.NETWORK_STATUS] ?? false);
            }
        }


        [CommandHandler(CommandConstants.WYJDZ)]
        public void Wyjdz(object sender, EventArgs e)
        {
            //zapytanie o wyjscie
            if (MessageBox.Show("Czy na pewno chcesz wyj�� z aplikacji?", "Wyj�cie", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                this.tClock.Enabled = false;
                this.Close();
            }
            else
            {
                this.Activate();
            }
        }

        [EventSubscription(EventBrokerConstants.USER_HAS_LOGGED_ON, ThreadOption.Background)]
        public void OnUserHasLoggedOn(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new EventHandler(OnUserHasLoggedOn), sender, e);
                return;
            }

            var userName = workItem.State[StateConstants.USER] as string;
            if (userName != null)
            {
                lblUser.Text = userName;
            }
        }

        [CommandHandler(CommandConstants.WYLOGUJ)]
        public void OnUserHasLoggedOff(object sender, EventArgs e)
        {
            lblUser.Text = string.Empty;
        }

        private void MSMShellForm_Activated(object sender, EventArgs e)
        {
            if (workItem != null)
            {
                workItem.StartReadBarCode();
            }
        }

        private void MSMShellForm_Deactivate(object sender, EventArgs e)
        {
            if (workItem != null)
            {
                workItem.StopReadBarCode();
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            // obluga nawigacji lub zmiana trybu
            if (!workItem.Navigate(e))
            {
                base.OnKeyDown(e);

                // jezeli tryb zmienil sie na not navigationState, trzeba zasymulowac ponowne nacisniecie klawisza Shift,
                // aby nacisniecie klawisza numerycznego zwrocilo numer
                workItem.PostNavigate();
            }
        }

        private void tClock_Tick(object sender, EventArgs e)
        {
            lblClock.Text = DateTime.Now.ToShortTimeString();
        }

        [EventSubscription(EventBrokerConstants.ASYNC_OPERATIONS_STARTED, ThreadOption.Background)]
        public void OnAsyncOperationsStarted(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new EventHandler(OnAsyncOperationsStarted), sender, e);
                return;
            }

            if (!pbAsyncDownload.Visible)
            {
                lblUser.Size = new Size(lblClock.Left - pbAsyncDownload.Right, lblUser.Height);
                lblUser.Location = new Point(pbAsyncDownload.Left + pbAsyncDownload.Width, lblUser.Top);
                pbAsyncDownload.Visible = true;
            }
        }

        [EventSubscription(EventBrokerConstants.ASYNC_OPERATOIN_FINISHED, ThreadOption.Background)]
        public void OnAsyncOperationFinished(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new EventHandler(OnAsyncOperationFinished), sender, e);
                return;
            }

            if (workItem.AllAsyncOperationsFinished)
            {
                lblUser.Size = new Size(lblClock.Left - pbAsyncDownload.Left, lblUser.Height);
                lblUser.Location = new Point(pbAsyncDownload.Left, lblUser.Top);
                pbAsyncDownload.Visible = false;
            }
        }
    }
}