#region

using Common;
using Common.Images;
using Microsoft.Practices.Mobile.CompositeUI;
using MobilnySystemMagazynowy.Administration.DataModel;
using MobilnySystemMagazynowy.Menu;

#endregion

namespace MenuBuilder
{
    public static class MenuBuilder
    {
        private static Rola Rola(WorkItem workItem)
        {
            if (workItem == null ||
                workItem.State[StateConstants.ROLE] == null)
            {
                return new Rola();
            }
            return workItem.State[StateConstants.ROLE] as Rola;
        }

        public static void LoadMenu(WorkItem workItem)
        {
            var aktualnaRola = Rola(workItem);

            var menuItemKodyKreskowe = new MenuButton
            {
                Text = NavigationConstants.KodyKreskowe,
                Image = ImageHelper.MenuKodyKreskowe
            };
            workItem.UIExtensionSites[UIExtensionConstants.MAIN_MENU].Add(menuItemKodyKreskowe);
            workItem.Commands[CommandConstants.KodyKreskowe].AddInvoker(menuItemKodyKreskowe, "Click");

            //TODO: przerobic! nie moze byc tyle if'ow - zrobione na szybko przed prezentacja
            if (aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) ||
                aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.DOSTAWA_KONTROLA_MP) ||
                aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.DOSTAWA_LISTA_DOKUMENTOW_MP) ||
                aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.DOSTAWA_REALIZACJA_MP))
            {
                var menuItemDostawa = new MenuButton();
                menuItemDostawa.Text = NavigationConstants.DOSTAWA;
                menuItemDostawa.Image = ImageHelper.MenuDostawa;
                workItem.UIExtensionSites[UIExtensionConstants.MAIN_MENU].Add(menuItemDostawa);
                workItem.Commands[CommandConstants.DOSTAWA].AddInvoker(menuItemDostawa, "Click");

                if (aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) ||
                    aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.DOSTAWA_LISTA_DOKUMENTOW_MP))
                {
                    var menuItemDostawaListaMP = new MenuButton();
                    menuItemDostawaListaMP.Text = NavigationConstants.LISTA_MP;
                    menuItemDostawaListaMP.Image = ImageHelper.MenuDostawa;
                    menuItemDostawa.MenuItems.Add(menuItemDostawaListaMP);
                    workItem.Commands[CommandConstants.LISTA_MP].AddInvoker(menuItemDostawaListaMP, "Click");
                }

                if (aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) ||
                    aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.DOSTAWA_REALIZACJA_MP))
                {
                    var menuItemDostawaRealizacjaMP = new MenuButton();
                    menuItemDostawaRealizacjaMP.Text = NavigationConstants.REALIZACJA_MP;
                    menuItemDostawaRealizacjaMP.Image = ImageHelper.MenuDostawa;
                    menuItemDostawa.MenuItems.Add(menuItemDostawaRealizacjaMP);
                    workItem.Commands[CommandConstants.REALIZACJA_MP].AddInvoker(menuItemDostawaRealizacjaMP, "Click");
                }
            }

            #region Przesuniecia

            if (aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) ||
                aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.PRZESUNIECIE_LISTA) ||
                aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.PRZESUNIECIE_REALIZACJA_MR))
            {
                var menuItemPrzesuniecia = new MenuButton();
                menuItemPrzesuniecia.Text = NavigationConstants.PRZESUNIECIA;
                menuItemPrzesuniecia.Image = ImageHelper.MenuPrzesuniecie;
                workItem.UIExtensionSites[UIExtensionConstants.MAIN_MENU].Add(menuItemPrzesuniecia);
                workItem.Commands[CommandConstants.PRZESUNIECIA].AddInvoker(menuItemPrzesuniecia, "Click");

                if (aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) ||
                    aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.PRZESUNIECIE_LISTA) ||
                    aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.PRZESUNIECIE_REALIZACJA_MR))
                {
                    var menuItemPrzesunieciaListaTowarow = new MenuButton();
                    menuItemPrzesunieciaListaTowarow.Text = NavigationConstants.PRZESUNIECIA_LISTA_TOWAROW;
                    menuItemPrzesunieciaListaTowarow.Image = ImageHelper.MenuPrzesuniecie;
                    menuItemPrzesuniecia.MenuItems.Add(menuItemPrzesunieciaListaTowarow);
                    workItem.Commands[CommandConstants.PRZESUNIECIA_LISTA_TOWAROW].AddInvoker(menuItemPrzesunieciaListaTowarow, "Click");
                }

                if (aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) ||
                    aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.PRZESUNIECIE_REALIZACJA_MR))
                {
                    var menuItemPrzesunieciaRealizacjaMR = new MenuButton();
                    menuItemPrzesunieciaRealizacjaMR.Text = NavigationConstants.PRZESUNIECIA_REALIZACJA_MP;
                    menuItemPrzesunieciaRealizacjaMR.Image = ImageHelper.MenuPrzesuniecie;
                    menuItemPrzesuniecia.MenuItems.Add(menuItemPrzesunieciaRealizacjaMR);
                    workItem.Commands[CommandConstants.PRZESUNIECIA_REALIZACJA].AddInvoker(menuItemPrzesunieciaRealizacjaMR, "Click");

                    var menuItemNowePrzesuniecie = new MenuButton();
                    menuItemNowePrzesuniecie.Text = NavigationConstants.NOWE_PRZESUNIECIE;
                    menuItemNowePrzesuniecie.Image = ImageHelper.MenuPrzesuniecie;
                    menuItemPrzesuniecia.MenuItems.Add(menuItemNowePrzesuniecie);
                    workItem.Commands[CommandConstants.NOWE_PRZESUNIECIE].AddInvoker(menuItemNowePrzesuniecie, "Click");
                }
            }

            #endregion

            #region WydanieMR
            //org #Albi
            //if (aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) ||
            //    aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WYDANIE_MR_LISTA))
            //{
            //    var menuItemWydanieMR = new MenuButton();
            //    menuItemWydanieMR.Text = NavigationConstants.WydanieMR;
            //    menuItemWydanieMR.Image = ImageHelper.MenuWydanieMr;
            //    workItem.UIExtensionSites[UIExtensionConstants.MAIN_MENU].Add(menuItemWydanieMR);
            //    workItem.Commands[CommandConstants.WydanieMR].AddInvoker(menuItemWydanieMR, "Click");
            //}

            if (aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) ||
                aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WYDANIE_MR_LISTA))
            {
                var menuItemWydanieMR = new MenuButton();
                menuItemWydanieMR.Text = NavigationConstants.WYDANIE_MR;
                menuItemWydanieMR.Image = ImageHelper.MenuWydanieMr;
                workItem.UIExtensionSites[UIExtensionConstants.MAIN_MENU].Add(menuItemWydanieMR);
                workItem.Commands[CommandConstants.WydanieMR].AddInvoker(menuItemWydanieMR, "Click");

                if (aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) ||
                    aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WYDANIE_MR_LISTA))
                {
                    var menuItemWydanieMRRealizacja = new MenuButton { Text = NavigationConstants.WydanieMRRealizacja, Image = ImageHelper.MenuWydanieMr };
                    menuItemWydanieMR.MenuItems.Add(menuItemWydanieMRRealizacja);
                    workItem.Commands[CommandConstants.WydanieMRRealizacja].AddInvoker(menuItemWydanieMRRealizacja, "Click");
                }

                if (aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) ||
                    aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WYDANIE_MR_LISTA))
                {
                    var menuItemWydanieMRRealizacjaZbiorcza = new MenuButton { Text = NavigationConstants.WydanieMRRealizacjaZbiorcza, Image = ImageHelper.MenuWydanieMr };
                    menuItemWydanieMR.MenuItems.Add(menuItemWydanieMRRealizacjaZbiorcza);
                    workItem.Commands[CommandConstants.WydanieMRRealizacjaZbiorcza].AddInvoker(menuItemWydanieMRRealizacjaZbiorcza, "Click");
                }

                if (aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) ||
                    aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WYDANIE_MR_LISTA))
                {
                    var menuItemWydanieMRPrzejecie = new MenuButton { Text = NavigationConstants.WydanieMRPrzejecie, Image = ImageHelper.MenuWydanieMr };
                    menuItemWydanieMR.MenuItems.Add(menuItemWydanieMRPrzejecie);
                    workItem.Commands[CommandConstants.WydanieMRPrzejecie].AddInvoker(menuItemWydanieMRPrzejecie, "Click");
                }
            }

            #endregion

            if (aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) ||
                aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WYDANIE_SWA_LISTA))
            {
                var menuItemWydanieSWA = new MenuButton();
                menuItemWydanieSWA.Text = NavigationConstants.WYDANIE_SWA;
                menuItemWydanieSWA.Image = ImageHelper.MenuWydanieSwa;
                workItem.UIExtensionSites[UIExtensionConstants.MAIN_MENU].Add(menuItemWydanieSWA);
                workItem.Commands[CommandConstants.WYDANIE_SWA].AddInvoker(menuItemWydanieSWA, "Click");
            }

            if (aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) ||
                aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.INDEKSY))
            {
                var menuItemKartotekaIndeksu = new MenuButton();
                menuItemKartotekaIndeksu.Text = NavigationConstants.KARTOTEKA_INDEKSU;
                menuItemKartotekaIndeksu.Image = ImageHelper.MenuIndeksy;
                workItem.UIExtensionSites[UIExtensionConstants.MAIN_MENU].Add(menuItemKartotekaIndeksu);
                workItem.Commands[CommandConstants.KARTOTEKA_INDEKSU].AddInvoker(menuItemKartotekaIndeksu, "Click");
            }

            if (aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) ||
                aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.MIEJSCA))
            {
                var menuItemKartotekaMiejsca = new MenuButton();
                menuItemKartotekaMiejsca.Text = NavigationConstants.KARTOTEKA_MIEJSCA;
                menuItemKartotekaMiejsca.Image = ImageHelper.MenuMiejsca;
                workItem.UIExtensionSites[UIExtensionConstants.MAIN_MENU].Add(menuItemKartotekaMiejsca);
                workItem.Commands[CommandConstants.KARTOTEKA_MIEJSCA].AddInvoker(menuItemKartotekaMiejsca, "Click");
            }

            if (aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) ||
                aktualnaRola.Uprawnienia.Contains(FunctionalityConstants.INWENTARYZACJA_LISTA))
            {
                var menuItemInwentaryzacja = new MenuButton();
                menuItemInwentaryzacja.Text = NavigationConstants.INWENTARYZACJA;
                menuItemInwentaryzacja.Image = ImageHelper.MenuInwentaryzacja;
                workItem.UIExtensionSites[UIExtensionConstants.MAIN_MENU].Add(menuItemInwentaryzacja);
                workItem.Commands[CommandConstants.INWENTARYZACJA].AddInvoker(menuItemInwentaryzacja, "Click");
            }

            var listaRol = (workItem.State[StateConstants.USER_ROLES] as ListaRol);
            if (listaRol != null &&
                listaRol.WieleRol)
            {
                var menuItemZmienProfil = new MenuButton();
                menuItemZmienProfil.Text = NavigationConstants.ZMIEN_PROFIL;
                menuItemZmienProfil.Image = ImageHelper.MenuZmienProfil;
                workItem.UIExtensionSites[UIExtensionConstants.MAIN_MENU].Add(menuItemZmienProfil);
                workItem.Commands[CommandConstants.ZMIEN_PROFIL].AddInvoker(menuItemZmienProfil, "Click");
            }

            var menuItemWyloguj = new MenuButton();
            menuItemWyloguj.Text = NavigationConstants.WYLOGUJ;
            menuItemWyloguj.Image = ImageHelper.MenuWyloguj;
            workItem.UIExtensionSites[UIExtensionConstants.MAIN_MENU].Add(menuItemWyloguj);
            workItem.Commands[CommandConstants.WYLOGUJ].AddInvoker(menuItemWyloguj, "Click");
        }
    }
}