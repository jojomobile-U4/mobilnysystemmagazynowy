#region

using Microsoft.Practices.Mobile.CompositeUI.Commands;
using MobilnySystemMagazynowy.Menu;

#endregion

namespace MenuBuilder
{
    public class MenuViewCommandAdapter : EventCommandAdapter<MenuView>
    {
        public MenuViewCommandAdapter()
        {
        }

        public MenuViewCommandAdapter(MenuView item, string eventName)
            : base(item, eventName)
        {
        }
    }
}