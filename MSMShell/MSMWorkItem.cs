#region

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Common;
using Common.BarCodeReader;
using Common.Base;
using Common.DataModel;
using MenuBuilder;
using Microsoft.Practices.Mobile.CompositeUI.Commands;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using MobilnySystemMagazynowy.Administration;
using MobilnySystemMagazynowy.Administration.DataModel;
using MobilnySystemMagazynowy.Menu;
using Symbol.WirelessLAN;

#endregion

namespace MobilnySystemMagazynowy
{
    internal class MSMWorkItem : WorkItemBase
    {
        #region Event Publications

        [EventPublication(EventBrokerConstants.BARCODE_HAS_BEEN_READ, PublicationScope.Global)]
        public event EventHandler BarCodeHasBeenRead;

        [EventPublication(EventBrokerConstants.NAVIGATION_KEY_PRESSED)]
        public event EventHandler NavigationKeyPressed;

        public event EventHandler NetConnectionStatusChanged;

        [EventPublication(EventBrokerConstants.NAVIGATION_STATE_CHANGED)]
        public event EventHandler NavigationStateChanged;

        #endregion

        #region Constants

        private const byte VkShift = 0x10;
        const int KeyeventfKeyup = 0x02;
        const int KeyeventfKeydown = 0x00;


        #endregion

        #region Private Fields

        private Radio NetworkConnectionStatus;
        private Rola m_AktualnaRola;
        private bool m_DataEditStateEnabled;
        private MenuView m_MainSmartPart;
        private BarCodeReader m_Reader;
        private bool m_ReaderStarted;

        #endregion

        #region Properties

        private bool NavigationState
        {
            get
            {
                if (RootWorkItem.State[StateConstants.NAVIGATION_STATE] == null)
                {
                    return false;
                }
                return (bool)RootWorkItem.State[StateConstants.NAVIGATION_STATE];
            }
            set
            {
                RootWorkItem.State[StateConstants.NAVIGATION_STATE] = value;
                OnNavigationStateChanged();
            }
        }

        private bool UserLoggedOn
        {
            get { return State[StateConstants.USER] != null; }
        }

        public bool AllAsyncOperationsFinished
        {
            get
            {
                var listaStanow = State[StateConstants.ASYNC_OP_STANY_REMAINING] as SortedList<long, DaneOperacjiAsync>;
                var listaIlosci = State[StateConstants.ASYNC_OP_ILOSC_REMAINING] as SortedList<long, DaneOperacjiAsync>;

                return (listaStanow == null || listaStanow.Count == 0) &&
                       (listaIlosci == null || listaIlosci.Count == 0);
            }
        }

        #endregion

        #region Imported Methods

        [DllImport("coredll", SetLastError = true)]
        private static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

        #endregion

        #region Event Subscriptions

        [CommandHandler(CommandConstants.WYLOGUJ)]
        public void LogOff(object sender, EventArgs e)
        {
            State[StateConstants.USER] = null;
            MainWorkspace.Close(m_MainSmartPart);
            m_MainSmartPart = null;
            RootWorkItem.UIExtensionSites.UnregisterSite(UIExtensionConstants.MAIN_MENU);
            WorkItems.Get<AdministrationWorkItem>(WorkItemsConstants.ADMINISTRATION_WORKITEM).Show();
        }

        [CommandHandler(CommandConstants.ZMIEN_PROFIL)]
        public void ChangeRole(object sender, EventArgs e)
        {
            WorkItems.Get<AdministrationWorkItem>(WorkItemsConstants.ADMINISTRATION_WORKITEM).ZmienRole();
        }

        [CommandHandler(CommandConstants.REQUEST_EDIT_STATE_ACTIVATION)]
        public void OnShowSearchForm(object sender, EventArgs e)
        {
            NavigationState = false;
        }

        [EventSubscription(EventBrokerConstants.USER_HAS_LOGGED_ON)]
        public void OnUserHasLoggedOn(object sender, EventArgs e)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                if (!UserLoggedOn)
                {
                    //Moze lepiej wyjatek?
                    return;
                }
                //zamkniecie jezeli jeszcze nie zamknieto
                if (m_MainSmartPart != null)
                {
                    MainWorkspace.Close(m_MainSmartPart);
                }

                var mapService =
                    RootWorkItem.Services.Get<ICommandAdapterMapService>();
                mapService.Register(typeof(MenuView), typeof(MenuViewCommandAdapter));

                //Przerobic, aby nie tworzyc formatki logowania za kazdym razem, tylko zmieniac jej zawartosc
                m_MainSmartPart = CreateMenu();
                //Ponizsze wywalic (chyba)
                //RootWorkItem.State[StateConstants.MAIN_SMART_PART] = m_MainSmartPart;
                if (RootWorkItem.UIExtensionSites.Contains(UIExtensionConstants.MAIN_MENU))
                {
                    RootWorkItem.UIExtensionSites.UnregisterSite(UIExtensionConstants.MAIN_MENU);
                }

                RootWorkItem.UIExtensionSites.RegisterSite(UIExtensionConstants.MAIN_MENU, m_MainSmartPart);

                MenuBuilder.MenuBuilder.LoadMenu(this);

                m_AktualnaRola = State[StateConstants.ROLE] as Rola;

                MainWorkspace.Show(m_MainSmartPart);
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        #endregion

        #region Methods

        public void StopReadBarCode()
        {
            if (m_Reader != null &&
                m_ReaderStarted)
            {
                m_Reader.StopRead();
                m_ReaderStarted = false;
            }
        }

        public void StartReadBarCode()
        {
            if (m_Reader != null &&
                !m_ReaderStarted)
            {
                m_Reader.StartRead();
                m_ReaderStarted = true;
            }
        }

        public bool Navigate(KeyEventArgs e)
        {
            SetNavigationState(e);
            if (!e.Handled &&
                IsNavigationKey(e.KeyCode))
            {
                PreProcessNavigationKey(e);
                if (!e.Handled)
                {
                    OnNavigationKeyPressed(e);
                }
            }
            return e.Handled;
        }

        public void PostNavigate()
        {
            // jezeli nastapila zmiana trybu z NavigationState do DataEditState nalezy zasymulowac ponowne nacisniecie
            // klawisza Shift, aby pierwszy nacisniety klawisz nie posiadal modyfikatora Shift.
            if (m_DataEditStateEnabled)
            {
                m_DataEditStateEnabled = false;
                SimulateShiftKeyPress();
            }
        }

        #endregion

        #region Protected Methods

        protected override void OnRunStarted()
        {
            base.OnRunStarted();

            WorkItems.Get<AdministrationWorkItem>(WorkItemsConstants.ADMINISTRATION_WORKITEM).Show();

            m_Reader = m_Reader ?? new BarCodeReader();
            m_Reader.Initialize();
            m_ReaderStarted = true;
            m_Reader.OdczytanoDane += ReaderOdczytanoDane;
            Workspaces.Get(WorkspacesConstants.MAIN_WORKSPACE).SmartPartActivated += MsmWorkItemSmartPartActivated;

            NetworkConnectionStatus = new Radio();
            NetworkConnectionStatus.StatusNotify += NetworkConnectionStatusStatusNotify;
            State[StateConstants.NETWORK_STATUS] = NetworkConnectionStatus.Signal.IsAssociated;
            OnNetConnetctionStatusChanged();
            NavigationState = true;
            m_DataEditStateEnabled = false;
            FunctionsHelper.SetCursorDefault();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (m_Reader != null)
                {
                    m_Reader.Terminate();
                }
                if (NetworkConnectionStatus != null)
                {
                    NetworkConnectionStatus.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        protected void OnNetConnetctionStatusChanged()
        {
            if (NetConnectionStatusChanged != null)
            {
                NetConnectionStatusChanged(this, EventArgs.Empty);
            }
        }

        protected void OnNavigationKeyPressed(KeyEventArgs e)
        {
            if (NavigationKeyPressed != null)
            {
                State[StateConstants.NAVIGATION_KEY] = e;
                NavigationKeyPressed(this, EventArgs.Empty);
            }
        }

        protected void OnNavigationStateChanged()
        {
            if (NavigationStateChanged != null)
            {
                NavigationStateChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Private Methods

        private static void SimulateShiftKeyPress()
        {
            keybd_event(VkShift, 0, KeyeventfKeydown, 0);
            keybd_event(VkShift, 0, KeyeventfKeyup, 0);
        }


        private void NetworkConnectionStatusStatusNotify(object sender, EventArgs e)
        {
            var myStatus = NetworkConnectionStatus.GetNextStatus();
            if (myStatus.Change == ChangeType.SIGNAL)
            {
                State[StateConstants.NETWORK_STATUS] = NetworkConnectionStatus.Signal.IsAssociated;
                OnNetConnetctionStatusChanged();
            }
        }


        private static void MsmWorkItemSmartPartActivated(object sender, WorkspaceEventArgs e)
        {
            var controlSmartPart = e.SmartPart as Control;
            if (controlSmartPart != null)
            {
                controlSmartPart.Focus();
            }
        }

        private void ReaderOdczytanoDane(Object sender, EventArgs e)
        {
            RootWorkItem.State[StateConstants.BAR_CODE] = m_Reader.BarCode;
            RootWorkItem.State[StateConstants.BAR_CODE_TYPE] = m_Reader.BarCodeType;

            if (BarCodeHasBeenRead != null)
            {
                BarCodeHasBeenRead(this, null);
            }
        }

        private MenuView CreateMenu()
        {
            var menu = RootWorkItem.Items.AddNew<MenuView>();

            menu.Dock = DockStyle.Fill;
            return menu;
        }

        #endregion

        #region Navigation

        private void PreProcessNavigationKey(KeyEventArgs e)
        {
            if (!UserLoggedOn)
            {
                return;
            }

            switch (e.KeyCode)
            {
                case Keys.I:
                    if (Workspaces[WorkspacesConstants.MAIN_WORKSPACE].ActiveSmartPart != m_MainSmartPart)
                    {
                        if (m_AktualnaRola.Uprawnienia.Contains(FunctionalityConstants.INDEKSY))
                        {
                            e.Handled = true;
                            Commands[CommandConstants.KARTOTEKA_INDEKSU].Execute();
                        }
                    }
                    break;
                case Keys.M:
                    if (Workspaces[WorkspacesConstants.MAIN_WORKSPACE].ActiveSmartPart != m_MainSmartPart)
                    {
                        if (m_AktualnaRola.Uprawnienia.Contains(FunctionalityConstants.MIEJSCA))
                        {
                            e.Handled = true;
                            Commands[CommandConstants.KARTOTEKA_MIEJSCA].Execute();
                        }
                    }
                    break;
                case Keys.K:
                    if (Workspaces[WorkspacesConstants.MAIN_WORKSPACE].ActiveSmartPart != m_MainSmartPart)
                    {
                        if (RootWorkItem.State[StateConstants.BlokadaWywolaniaFormatkiK] == null)
                            RootWorkItem.State[StateConstants.BlokadaWywolaniaFormatkiK] = StateConstants.BoolValueNo;

                        if (!Equals(RootWorkItem.State[StateConstants.BlokadaWywolaniaFormatkiK], StateConstants.BoolValueYes))
                        {
                            RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueYes;
                            e.Handled = true;
                            Commands[CommandConstants.KodyKreskowe].Execute();
                        }
                    }
                    break;
                case Keys.Escape:
                    if (Workspaces[WorkspacesConstants.MAIN_WORKSPACE].ActiveSmartPart == m_MainSmartPart)
                    {
                        e.Handled = true;

                        m_MainSmartPart.ShowUpLevel();
                        //Commands[CommandConstants.WYLOGUJ].Execute();
                    }
                    else
                    {
                        State[StateConstants.NAVIGATION_KEY] = new KeyEventArgs(Keys.Escape);
                        // powrot do formatki poprzedniej powoduje automatyczne przejscie w tryb nawigacji
                        NavigationState = true;
                    }
                    break;
                case Keys.D0:
                case Keys.D1:
                case Keys.D2:
                case Keys.D3:
                case Keys.D4:
                case Keys.D5:
                case Keys.D6:
                case Keys.D7:
                case Keys.D8:
                case Keys.D9:
                case Keys.D:
                    if (Workspaces[WorkspacesConstants.MAIN_WORKSPACE].ActiveSmartPart == m_MainSmartPart)
                    {
                        m_MainSmartPart.HangleNavigation(e);
                    }
                    break;
            }
        }

        private void SetNavigationState(KeyEventArgs e)
        {
            // application state changed only if user has logged on
            if (UserLoggedOn)
            {
                if (NavigationState)
                {
                    // jezeli wcisnieto shift i nie jestesmy na menu
                    if (e.KeyCode == Keys.ShiftKey &&
                        Workspaces[WorkspacesConstants.MAIN_WORKSPACE].ActiveSmartPart != m_MainSmartPart)
                    {
                        //wyczyszczenie flagi pamietajacej o wcisnieciu klawisza shift, po zakonczeniu obslugi nacisniecia
                        // klawisza
                        m_DataEditStateEnabled = true;
                        NavigationState = false;
                        e.Handled = true;
                    }
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    NavigationState = true;
                    e.Handled = true;
                }
            }
        }

        private bool IsNavigationKey(Keys key)
        {
            if (key == Keys.Down ||
                key == Keys.Up ||
                key == Keys.Left ||
                key == Keys.Right ||
                key == Keys.Escape ||
                key == Keys.Enter ||
                //focus next control na opakowaniach - "."
                key == (Keys.RButton | Keys.MButton | Keys.Back | Keys.ShiftKey | Keys.Space | Keys.F17))
            {
                return true;
            }
            if (NavigationState)
            {
                if (IsDecimalKey(key) ||
                    //kartoteka indeksu
                    key == Keys.I ||
                    //kartoteka miejsca
                    key == Keys.M ||
                    //kartoteka kody kreskowe
                    key == Keys.K ||
                    //wydruki
                    key == Keys.D)
                {
                    return true;
                }
            }

            return false;
        }

        private static bool IsDecimalKey(Keys key)
        {
            return key >= Keys.D0 && key <= Keys.D9;
        }

        #endregion
    }
}