
namespace MobilnySystemMagazynowy.Menu
{
	partial class MenuView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.pnlBase = new System.Windows.Forms.Panel();
            this.btnPowrot = new System.Windows.Forms.Button();
            this.pnlPowrot = new System.Windows.Forms.Panel();
            this.pnlPowrot.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBase
            // 
            this.pnlBase.BackColor = System.Drawing.Color.Transparent;
            this.pnlBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBase.Location = new System.Drawing.Point(0, 0);
            this.pnlBase.Name = "pnlBase";
            this.pnlBase.Size = new System.Drawing.Size(240, 277);
            // 
            // btnPowrot
            // 
            this.btnPowrot.Location = new System.Drawing.Point(158, 0);
            this.btnPowrot.Name = "btnPowrot";
            this.btnPowrot.Size = new System.Drawing.Size(79, 20);
            this.btnPowrot.TabIndex = 1;
            this.btnPowrot.TabStop = false;
            this.btnPowrot.Text = "&Esc Powr�t";
            this.btnPowrot.Click += new System.EventHandler(this.m_returnButton_Click);
            // 
            // pnlPowrot
            // 
            this.pnlPowrot.Controls.Add(this.btnPowrot);
            this.pnlPowrot.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlPowrot.Location = new System.Drawing.Point(0, 277);
            this.pnlPowrot.Name = "pnlPowrot";
            this.pnlPowrot.Size = new System.Drawing.Size(240, 23);
            this.pnlPowrot.Visible = false;
            // 
            // MenuView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.pnlBase);
            this.Controls.Add(this.pnlPowrot);
            this.Name = "MenuView";
            this.Size = new System.Drawing.Size(240, 300);
            this.pnlPowrot.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel pnlBase;
		private System.Windows.Forms.Button btnPowrot;
		private System.Windows.Forms.Panel pnlPowrot;
	}
}
