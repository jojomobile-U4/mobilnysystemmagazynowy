#region

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

#endregion

namespace MobilnySystemMagazynowy.Menu
{
    public partial class MenuButton : UserControl
    {
        #region Constants

        private const string ACCELERATOR = "&";
        private const int MARGINES_X = 6;
        private const int MARGINES_Y = 1;
        private const int TEKST_START_X = 94;
        private const int TEKST_START_Y = 2;

        #endregion

        #region Private Fields

        private readonly List<MenuButton> menuItems;
        private Image image;
        private string skrot;
        private string tekst;
        private string tekstWyrysowywany;

        #endregion

        #region Properties

        public List<MenuButton> MenuItems
        {
            get { return menuItems; }
        }

        public Image Image
        {
            get { return image; }
            set { image = value; }
        }

        public override string Text
        {
            get { return tekst; }
            set
            {
                tekst = value;
                if (!string.IsNullOrEmpty(value))
                {
                    skrot = value.Substring(1, 1);
                    tekstWyrysowywany = value.Substring(2);
                }
            }
        }

        #endregion

        #region Constructors

        public MenuButton()
        {
            InitializeComponent();
            menuItems = new List<MenuButton>();
        }

        #endregion

        #region Methods

        internal void PerformClick()
        {
            OnClick(EventArgs.Empty);
        }

        #endregion

        #region Painting

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            using (var backgroundBrush = new SolidBrush(Color.FromArgb(246, 231, 221)))
            {
                e.Graphics.FillRectangle(backgroundBrush,
                                         new Rectangle(ClientRectangle.X + MARGINES_X,
                                                       ClientRectangle.Y + MARGINES_Y,
                                                       ClientRectangle.Width - 2 * MARGINES_X,
                                                       ClientRectangle.Height - 2 * MARGINES_Y));
            }

            using (var pen = new Pen(Color.FromArgb(211, 192, 175)))
            {
                e.Graphics.DrawRectangle(pen,
                                         new Rectangle(ClientRectangle.X + MARGINES_X,
                                                       ClientRectangle.Y + MARGINES_Y,
                                                       ClientRectangle.Width - 2 * MARGINES_X,
                                                       ClientRectangle.Height - 2 * MARGINES_Y));
            }

            if (image != null)
            {
                e.Graphics.DrawImage(image, MARGINES_X, MARGINES_Y);
            }

            if (!string.IsNullOrEmpty(tekst))
            {
                using (var podkreslona = new Font(Font.Name, Font.Size, Font.Style | FontStyle.Underline))
                {
                    var y = (e.Graphics.MeasureString(tekst, podkreslona).Height/2) + TEKST_START_Y;
                    var x = e.Graphics.MeasureString(skrot, podkreslona).Width + TEKST_START_X;

                    using (var textBrush = new SolidBrush(Color.Black))
                    {
                        e.Graphics.DrawString(skrot, podkreslona, textBrush, TEKST_START_X, y);
                        e.Graphics.DrawString(tekstWyrysowywany, Font, textBrush, x, y);
                    }
                }
            }
        }

        #endregion
    }
}