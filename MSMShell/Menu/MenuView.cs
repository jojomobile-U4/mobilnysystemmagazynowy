#region

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Practices.Mobile.CompositeUI.UIElements;

#endregion

namespace MobilnySystemMagazynowy.Menu
{
    public partial class MenuView : UserControl, IUIElementAdapter //, IMenuView
    {
        #region Constants

        private const string EIGHT = "8";
        private const int FIRST_LEVEL = 0;
        private const string FIVE = "5";
        private const string FOUR = "4";
        private const string NINE = "9";
        private const string ONE = "1";
        private const string RETURN_BUTTON = "&0 Powr�t";
        private const string SEVEN = "7";
        private const string SIX = "6";
        private const string THREE = "3";
        private const string TWO = "2";
        private const string ZERO = "0";

        #endregion

        private readonly SortedList<string, MenuButton> m_buttons;
        private readonly Stack<string[]> m_menu;
        private readonly int m_PnlPowrotHeight;

        public MenuView()
        {
            InitializeComponent();

            m_buttons = new SortedList<string, MenuButton>();
            m_menu = new Stack<string[]>();
            m_PnlPowrotHeight = pnlPowrot.Height;
            pnlBase.Height = Height;
        }

        private int ActualMenuLevel
        {
            get { return m_menu.Count; }
        }

        private string[] CurrentMenu
        {
            get
            {
                var currentMenu = new List<string>();
                var visibleButtons = new SortedList<int, string>();
                foreach (var button in m_buttons.Values)
                {
                    if (button.Visible)
                    {
                        visibleButtons[button.TabIndex] = button.Text;
                    }
                }
                foreach (var key in visibleButtons.Keys)
                {
                    currentMenu.Add(visibleButtons[key]);
                }
                return currentMenu.ToArray();
            }
        }

        #region IUIElementAdapter Members

        public object Add(object uiElement)
        {
            var button = uiElement as MenuButton;
            if (AddButton(button))
            {
                return button;
            }
            return null;
        }

        public void Remove(object uiElement)
        {
            var button = uiElement as MenuButton;
            if (button != null &&
                m_buttons.ContainsValue(button))
            {
                m_buttons.RemoveAt(m_buttons.IndexOfValue(button));
            }
        }

        #endregion

        private void m_returnButton_Click(object sender, EventArgs e)
        {
            ShowUpLevel();
        }

        public bool ShowUpLevel()
        {
            if (ActualMenuLevel == FIRST_LEVEL)
            {
                return false;
            }

            ClearMenu();
            foreach (var button in m_menu.Pop())
            {
                ShowButton(button);
                AddButtonToPanel(m_buttons[button]);
            }

            pnlPowrot.Visible = ActualMenuLevel != FIRST_LEVEL;
            //HACK: ukrywanie panelu
            pnlBase.Height = pnlPowrot.Visible ? Height - m_PnlPowrotHeight : Height;
            pnlBase.Focus();
            return true;
        }

        public bool ShowSubLevel(string menuCommand)
        {
            return ShowSubLevel(m_buttons[menuCommand]);
        }

        public bool ShowSubLevel(MenuButton menuButton)
        {
            if (menuButton == null ||
                menuButton.MenuItems.Count == 0)
            {
                return false;
            }

            m_menu.Push(CurrentMenu);

            ClearMenu();
            foreach (var subButton in menuButton.MenuItems)
            {
                if (m_buttons.ContainsValue(subButton))
                {
                    subButton.Visible = true;
                    AddButtonToPanel(subButton);
                }
                else
                {
                    AddButton(subButton);
                }
            }
            //pokazanie przycisku powrot
            pnlPowrot.Visible = true;
            //HACK: zmniejszanie panelu
            pnlBase.Height = Height - m_PnlPowrotHeight;
            pnlBase.Focus();

            return true;
        }

        public void ClearMenu()
        {
            foreach (Control control in pnlBase.Controls)
            {
                control.Visible = false;
            }
            pnlBase.Controls.Clear();
        }

        public void ShowButton(string buttonText)
        {
            if (m_buttons.ContainsKey(buttonText))
            {
                m_buttons[buttonText].Visible = true;
            }
        }

        private bool AddButton(MenuButton button)
        {
            if (m_buttons.ContainsKey(button.Text))
            {
                return false;
            }
            m_buttons.Add(button.Text, button);
            button.Visible = true;
            button.TabIndex = m_buttons.Count;
            AddButtonToPanel(button);
            button.Click += button_Click;
            return true;
        }

        private void button_Click(object sender, EventArgs e)
        {
            var button = sender as MenuButton;
            ShowSubLevel(button);
        }

        private void AddButtonToPanel(MenuButton button)
        {
            button.Dock = DockStyle.Top;
            //aby zachowac porz�dek od gory do dolu
            var controls = new List<Control>();
            foreach (Control control in pnlBase.Controls)
            {
                controls.Add(control);
            }

            controls.Insert(0, button);

            pnlBase.Controls.Clear();
            foreach (var control in controls)
            {
                pnlBase.Controls.Add(control);
            }
        }

        public void HangleNavigation(KeyEventArgs e)
        {
            var currentMenu = CurrentMenu;

            switch (e.KeyCode)
            {
                case Keys.D0:
                case Keys.NumPad0:
                    e.Handled = true;
                    HandleAccelerator(currentMenu, ZERO);
                    break;
                case Keys.D1:
                case Keys.NumPad1:
                    e.Handled = true;
                    HandleAccelerator(currentMenu, ONE);
                    break;
                case Keys.D2:
                case Keys.NumPad2:
                    e.Handled = true;
                    HandleAccelerator(currentMenu, TWO);
                    break;
                case Keys.D3:
                case Keys.NumPad3:
                    e.Handled = true;
                    HandleAccelerator(currentMenu, THREE);
                    break;
                case Keys.D4:
                case Keys.NumPad4:
                    e.Handled = true;
                    HandleAccelerator(currentMenu, FOUR);
                    break;
                case Keys.D5:
                case Keys.NumPad5:
                    e.Handled = true;
                    HandleAccelerator(currentMenu, FIVE);
                    break;
                case Keys.D6:
                case Keys.NumPad6:
                    e.Handled = true;
                    HandleAccelerator(currentMenu, SIX);
                    break;
                case Keys.D7:
                case Keys.NumPad7:
                    e.Handled = true;
                    HandleAccelerator(currentMenu, SEVEN);
                    break;
                case Keys.D8:
                case Keys.NumPad8:
                    e.Handled = true;
                    HandleAccelerator(currentMenu, EIGHT);
                    break;
                case Keys.D9:
                case Keys.NumPad9:
                    e.Handled = true;
                    HandleAccelerator(currentMenu, NINE);
                    break;
            }

        }

        private void HandleAccelerator(string[] currentMenu, string accelerator)
        {
            foreach (var menuButtonText in currentMenu)
            {
                if (menuButtonText.Substring(1, 1).Equals(accelerator))
                {
                    m_buttons[menuButtonText].PerformClick();
                    break;
                }
            }
        }
    }
}