#region

using System.Collections.Generic;
using MobilnySystemMagazynowy.AdministracjaProxy;

#endregion

namespace MobilnySystemMagazynowy.Administration.DataModel
{
    public class Rola
    {
        #region Private Fields

        private List<string> m_Uprawnienia;

        #endregion

        #region Properties

        public List<string> Uprawnienia
        {
            get { return m_Uprawnienia; }
            set { m_Uprawnienia = value; }
        }

        public string Nazwa { get; set; }

        #endregion

        #region Constructors

        public Rola()
        {
            Nazwa = string.Empty;
            m_Uprawnienia = new List<string>();
        }

        public Rola(AdministracjaProxy.Rola rola) : this()
        {
            Nazwa = rola.nazwa;
            zaladujUprawnienia(rola.listaFunkcjonalnosci);
        }

        #endregion

        #region Private Methods

        private void zaladujUprawnienia(Funkcjonalnosc[] listaFunkcjonalnosci)
        {
            if (listaFunkcjonalnosci == null)
            {
                return;
            }

            m_Uprawnienia.Clear();
            foreach (var funkcjonalnosc in listaFunkcjonalnosci)
            {
                m_Uprawnienia.Add(funkcjonalnosc.nazwa);
            }
        }

        #endregion
    }
}