#region

using Common;
using Common.DataModel;

#endregion

namespace MobilnySystemMagazynowy.Administration.DataModel
{
    public class StanZalogowania
    {
        #region Private Fields

        #endregion

        #region Properties

        public StatusOperacji StatusOperacji { get; set; }

        public bool Zalogowany { get; set; }

        #endregion

        #region Constructors

        public StanZalogowania()
        {
            StatusOperacji = new StatusOperacji(StatusOperacji.SUCCESS);
            Zalogowany = false;
        }

        public StanZalogowania(AdministracjaProxy.StanZalogowania stanZalogowania) : this()
        {
            if (stanZalogowania != null)
            {
                StatusOperacji = new StatusOperacji(stanZalogowania.statusOperacji.status, stanZalogowania.statusOperacji.tekst, stanZalogowania.statusOperacji.stosWywolan);
                Zalogowany = DBBool.ToBool(stanZalogowania.jestZalogowany);
            }
            else
            {
                StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, DataModelConstants.NO_DATA, stanZalogowania.statusOperacji.stosWywolan);
            }
        }

        #endregion
    }
}