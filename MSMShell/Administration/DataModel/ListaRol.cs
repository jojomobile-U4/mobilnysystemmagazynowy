#region

using System.Collections.Generic;
using Common;
using Common.DataModel;

#endregion

namespace MobilnySystemMagazynowy.Administration.DataModel
{
    public class ListaRol
    {
        #region Private Fields

        private List<Rola> m_Role;

        #endregion

        #region Properties

        public StatusOperacji StatusOperacji { get; set; }

        public List<Rola> Role
        {
            get { return m_Role; }
            set { m_Role = value; }
        }

        public bool WieleRol
        {
            get { return m_Role.Count > 1; }
        }

        #endregion

        #region Constructors

        public ListaRol()
        {
            m_Role = new List<Rola>();
            StatusOperacji = new StatusOperacji(StatusOperacji.SUCCESS);
        }

        public ListaRol(AdministracjaProxy.ListaRol role) : this()
        {
            if (role != null && role.lista != null)
            {
                for (var i = 0; i < role.lista.Length; i++)
                {
                    m_Role.Add(new Rola(role.lista[i]));
                }
            }
            else
            {
                StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, DataModelConstants.NO_DATA);
            }
        }

        #endregion
    }
}