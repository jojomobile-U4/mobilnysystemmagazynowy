#region

using System;
using System.Windows.Forms;
using Common;
using Common.Base;

#endregion

namespace MobilnySystemMagazynowy.Administration.Login
{
    public class LoginViewPresenter : PresenterBase
    {
        #region Properties

        public ILoginView View
        {
            get { return m_view as ILoginView; }
        }

        protected AdministrationWorkItem MyWorkItem
        {
            get { return WorkItem as AdministrationWorkItem; }
        }

        #endregion

        #region Constructors

        public LoginViewPresenter(ILoginView view) : base(view)
        {
        }

        #endregion

        #region Obsluga widoku

        private void ViewLogin(object sender, EventArgs e)
        {
            MyWorkItem.Login(View.User, View.Password);
        }

        private void ViewCancel(object sender, EventArgs e)
        {
            MyWorkItem.CloseApplication();
        }

        #endregion

        protected override void AttachView()
        {
            View.Cancel += ViewCancel;
            View.Login += ViewLogin;
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    e.Handled = true;
                    ViewLogin(this, EventArgs.Empty);
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    ViewCancel(this, EventArgs.Empty);
                    break;
                case Keys.Up:
                    e.Handled = true;
                    View.FocusNextControl(false);
                    break;
                case Keys.Down:
                    e.Handled = true;
                    View.FocusNextControl(true);
                    break;
            }
        }
    }
}