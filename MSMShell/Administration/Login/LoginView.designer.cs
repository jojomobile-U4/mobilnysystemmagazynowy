
namespace MobilnySystemMagazynowy.Administration.Login
{
	partial class LoginView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginView));
            this.lbPassword = new System.Windows.Forms.Label();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.tbUser = new System.Windows.Forms.TextBox();
            this.btnAnuluj = new System.Windows.Forms.Button();
            this.lbUser = new System.Windows.Forms.Label();
            this.btnZaloguj = new System.Windows.Forms.Button();
            this.pcLogo = new System.Windows.Forms.PictureBox();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnAnuluj);
            this.pnlNavigation.Controls.Add(this.btnZaloguj);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 166);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 134);
            // 
            // lbPassword
            // 
            this.lbPassword.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbPassword.Location = new System.Drawing.Point(4, 141);
            this.lbPassword.Name = "lbPassword";
            this.lbPassword.Size = new System.Drawing.Size(59, 19);
            this.lbPassword.Text = "Has�o:";
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(74, 139);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(163, 21);
            this.tbPassword.TabIndex = 1;
            // 
            // tbUser
            // 
            this.tbUser.Location = new System.Drawing.Point(74, 116);
            this.tbUser.Name = "tbUser";
            this.tbUser.Size = new System.Drawing.Size(163, 21);
            this.tbUser.TabIndex = 0;
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnAnuluj.Location = new System.Drawing.Point(153, 107);
            this.btnAnuluj.Name = "btnAnuluj";
            this.btnAnuluj.Size = new System.Drawing.Size(80, 20);
            this.btnAnuluj.TabIndex = 3;
            this.btnAnuluj.TabStop = false;
            this.btnAnuluj.Text = "&Esc Zamknij";
            this.btnAnuluj.Click += new System.EventHandler(this.OnCancel);
            // 
            // lbUser
            // 
            this.lbUser.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbUser.Location = new System.Drawing.Point(3, 117);
            this.lbUser.Name = "lbUser";
            this.lbUser.Size = new System.Drawing.Size(76, 20);
            this.lbUser.Text = "U�ytkownik:";
            // 
            // btnZaloguj
            // 
            this.btnZaloguj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnZaloguj.Location = new System.Drawing.Point(71, 107);
            this.btnZaloguj.Name = "btnZaloguj";
            this.btnZaloguj.Size = new System.Drawing.Size(80, 20);
            this.btnZaloguj.TabIndex = 2;
            this.btnZaloguj.TabStop = false;
            this.btnZaloguj.Text = "&Ret zaloguj";
            this.btnZaloguj.Click += new System.EventHandler(this.OnLogin);
            // 
            // pcLogo
            // 
            this.pcLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pcLogo.Image = ((System.Drawing.Image)(resources.GetObject("pcLogo.Image")));
            this.pcLogo.Location = new System.Drawing.Point(0, 0);
            this.pcLogo.Name = "pcLogo";
            this.pcLogo.Size = new System.Drawing.Size(240, 110);
            // 
            // LoginView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.pcLogo);
            this.Controls.Add(this.lbPassword);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.tbUser);
            this.Controls.Add(this.lbUser);
            this.Name = "LoginView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbUser, 0);
            this.Controls.SetChildIndex(this.tbUser, 0);
            this.Controls.SetChildIndex(this.tbPassword, 0);
            this.Controls.SetChildIndex(this.lbPassword, 0);
            this.Controls.SetChildIndex(this.pcLogo, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnZaloguj;
		private System.Windows.Forms.Button btnAnuluj;
		private System.Windows.Forms.Label lbPassword;
		private System.Windows.Forms.TextBox tbPassword;
		private System.Windows.Forms.TextBox tbUser;
        private System.Windows.Forms.Label lbUser;
        private System.Windows.Forms.PictureBox pcLogo;
	}
}
