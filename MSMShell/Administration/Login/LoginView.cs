#region

using System;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace MobilnySystemMagazynowy.Administration.Login
{
    [SmartPart]
    public partial class LoginView : ViewBase, ILoginView
    {
        #region Constructors

        public LoginView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #endregion

        #region Protected Methods

        protected void OnCancel(object sender, EventArgs e)
        {
            if (Cancel != null)
            {
                Cancel(sender, e);
            }
        }

        protected void OnLogin(object sender, EventArgs e)
        {
            if (Login != null)
            {
                Login(sender, e);
            }
        }

        #endregion

        #region ILoginView Members

        public event EventHandler Cancel;

        public event EventHandler Login;

        public string User
        {
            get { return tbUser.Text; }
            set { tbUser.Text = value; }
        }

        public string Password
        {
            get { return tbPassword.Text; }
            set { tbPassword.Text = value; }
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);
            //UWAGA: Tylko w przypadku tej formatki uzycie control.Focus jest dozwolone!
            tbUser.Focus();
        }

        #endregion
    }
}