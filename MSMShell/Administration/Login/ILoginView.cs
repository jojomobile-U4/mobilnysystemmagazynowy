#region

using System;
using Common.Base;

#endregion

namespace MobilnySystemMagazynowy.Administration.Login
{
    public interface ILoginView : IViewBase
    {
        string User { get; set; }
        string Password { get; set; }
        event EventHandler Cancel;
        event EventHandler Login;
    }
}