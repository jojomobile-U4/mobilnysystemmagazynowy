#region

using System;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using MobilnySystemMagazynowy.Administration.DataModel;
using MobilnySystemMagazynowy.Administration.Login;
using MobilnySystemMagazynowy.Administration.Services;
using WydanieMRModule.WyborStrefy;

#endregion

namespace MobilnySystemMagazynowy.Administration
{
    public class AdministrationWorkItem : WorkItemBase
    {
        #region Event Publication

        [EventPublication(EventBrokerConstants.USER_HAS_LOGGED_ON)]
        public event EventHandler UserHasLoggedOn;

        #endregion

        #region Constants

        private const string INVALID_LOGIN_PASSWORD = "Nieprawidłowa nazwa użytkownika lub hasło.";

        #endregion

        #region Properties

        private string User
        {
            get { return RootWorkItem.State[StateConstants.USER] as string; }
            set { RootWorkItem.State[StateConstants.USER] = value; }
        }

        private string Password
        {
            get { return RootWorkItem.State[StateConstants.PASSWORD] as string; }
            set { RootWorkItem.State[StateConstants.PASSWORD] = value; }
        }

        #endregion

        #region Event Subscription

        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ)]
        public void OnBarCodeRead(object sender, EventArgs e)
        {
            if (Status == WorkItemStatus.Active &&
                RootWorkItem.State[StateConstants.BAR_CODE] != null)
            {
                var loginPresenter = Items.Get<LoginViewPresenter>(ItemsConstants.LOGIN_PRESENTER);
                loginPresenter.View.User = RootWorkItem.State[StateConstants.BAR_CODE].ToString();
            }
        }

        #endregion

        #region Show Methods

        public void Show()
        {
            var loginPresenter = Items.Get<LoginViewPresenter>(ItemsConstants.LOGIN_PRESENTER);
            loginPresenter.View.Password = string.Empty;
            var user = RootWorkItem.State[StateConstants.USER];
            loginPresenter.View.User = (user as string) ?? string.Empty;

            MainWorkspace.Show(loginPresenter.View);
        }

        #endregion

        #region Methods

        public void CloseApplication()
        {
            Commands[CommandConstants.WYJDZ].Execute();
        }

        public void Login(string user, string password)
        {
            try
            {
                if (string.IsNullOrEmpty(user))
                {
                    ShowMessageBox(INVALID_LOGIN_PASSWORD, BLAD);
                    return;
                }

                FunctionsHelper.SetCursorWait();

                User = user;
                Password = password;

                if (!Zaloguj())
                {
                    User = null;
                    Password = null;
                }
                else
                {
                    var listaRol = PobierzListeRol();

                    if (listaRol != null)
                    {
                        RootWorkItem.State[StateConstants.USER_ROLES] = listaRol;
                        if (listaRol.WieleRol)
                        {
                            ZmienRole();
                        }
                        else if (listaRol.Role.Count > 0)
                        {
                            RootWorkItem.State[StateConstants.ROLE] = listaRol.Role[0];

                            ZatwierdzWyborRoli();
                        }
                    }
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void ZmienRole()
        {
            var lista = RootWorkItem.State[StateConstants.USER_ROLES] as ListaRol;
            if (lista == null ||
                !lista.WieleRol)
            {
                return;
            }

            var roleSelectionPresenter = Items.Get<RoleSelectionViewPresenter>(ItemsConstants.ROLE_SELECTION_PRESENTER);
            roleSelectionPresenter.ZaladujDaneDoWidoku(lista);
            MainWorkspace.Show(roleSelectionPresenter.View);
        }

        public void ZatwierdzWyborRoli()
        {
            var rola = RootWorkItem.State[StateConstants.ROLE] as Rola;
            if (rola == null)
            {
                return;
            }

            //HACK: ustawianie uprawnien do funkcjonalosci na formatkach listyMP i realizacjiMP
            RootWorkItem.State[StateConstants.DELIVERY_CONTROL] = rola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) || rola.Uprawnienia.Contains(FunctionalityConstants.DOSTAWA_KONTROLA_MP);
            RootWorkItem.State[StateConstants.DELIVERY_REALIZATION] = rola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) || rola.Uprawnienia.Contains(FunctionalityConstants.DOSTAWA_REALIZACJA_MP);
            RootWorkItem.State[StateConstants.ASSORTIMENT_MOVE_RIGHTS] = rola.Uprawnienia.Contains(FunctionalityConstants.WSZYSTKO) || rola.Uprawnienia.Contains(FunctionalityConstants.PRZESUNIECIE_REALIZACJA_MR);

            OnUserHasLoggedOn();
        }

        #endregion

        #region Protected Methods

        protected void OnUserHasLoggedOn()
        {
            if (UserHasLoggedOn != null)
            {
                UserHasLoggedOn(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Private Methods

        private bool Zaloguj()
        {
            var stan =
                Services.Get<IAdministrationService>(true).LogOn(User, Password);
            if (stan.StatusOperacji.Status.Equals(StatusOperacji.ERROR))
            {
                ShowMessageBox(stan.StatusOperacji.Tekst, BLAD, stan.StatusOperacji.StosWywolan);
                return false;
            }
            if (!stan.Zalogowany)
            {
                ShowMessageBox(INVALID_LOGIN_PASSWORD, BLAD, stan.StatusOperacji.StosWywolan);
                return false;
            }

            return true;
        }

        private ListaRol PobierzListeRol()
        {
            var listaRol = Services.Get<IAdministrationService>(true).PobierzRoleUzytkownika(RootWorkItem.State[StateConstants.USER] as string);

            if (listaRol.StatusOperacji.Status.Equals(StatusOperacji.ERROR))
            {
                ShowMessageBox(listaRol.StatusOperacji.Tekst, BLAD, listaRol.StatusOperacji.StosWywolan);
                RootWorkItem.State[StateConstants.USER] = null;
                RootWorkItem.State[StateConstants.PASSWORD] = null;
                return null;
            }

            return listaRol;
        }

        #endregion
    }
}