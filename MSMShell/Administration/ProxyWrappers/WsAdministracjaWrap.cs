#region

using System;
using System.Net;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using Common;
using Common.Base;
using MobilnySystemMagazynowy.AdministracjaProxy;
using OpenNETCF.Web.Services2;

#endregion

namespace MobilnySystemMagazynowy.Administration.ProxyWrappers
{
    public class WsAdministracjaWrap : WsAdministracja, IWSSecurity
    {
        #region Private Fields

        #endregion

        #region Properties

        public SecurityHeader SecurityHeader { get; set; }

        #endregion

        #region Constructors

        public WsAdministracjaWrap(WebServiceConfiguration configuration)
        {
            Url = configuration.Url;
        }

        #endregion

        #region Protected Methods

        protected override WebRequest GetWebRequest(Uri uri)
        {
            var request = (HttpWebRequest) base.GetWebRequest(uri);

            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;

            return request;
        }

        #endregion

        #region Methods

        /// <remarks/>
        [SoapDocumentMethod("urn:pobierzListeFunkcjonalnosci", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeFunkcjonalnosciResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeFunkcjonalnosciResponse pobierzListeFunkcjonalnosci([XmlElement("pobierzListeFunkcjonalnosci", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeFunkcjonalnosciWrap pobierzListeFunkcjonalnosci1)
        {
            var results = Invoke("pobierzListeFunkcjonalnosci", new object[] {pobierzListeFunkcjonalnosci1});
            return ((pobierzListeFunkcjonalnosciResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:pobierzListeRol", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeRolResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeRolResponse pobierzListeRol([XmlElement("pobierzListeRol", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeRolWrap pobierzListeRol1)
        {
            var results = Invoke("pobierzListeRol", new object[] {pobierzListeRol1});
            return ((pobierzListeRolResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:zaloguj", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("zalogujResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public zalogujResponse zaloguj([XmlElement("zaloguj", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] zalogujWrap zaloguj1)
        {
            var results = Invoke("zaloguj", new object[] {zaloguj1});
            return ((zalogujResponse) (results[0]));
        }

        #endregion
    }

    public class pobierzListeFunkcjonalnosciWrap : pobierzListeFunkcjonalnosci
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string nazwaFunkcjonalnosci
        {
            get { return base.nazwaFunkcjonalnosci; }
            set { base.nazwaFunkcjonalnosci = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string system
        {
            get { return base.system; }
            set { base.system = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string nazwaRoli
        {
            get { return base.nazwaRoli; }
            set { base.nazwaRoli = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? numerStrony
        {
            get { return base.numerStrony; }
            set { base.numerStrony = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? wielkoscStrony
        {
            get { return base.wielkoscStrony; }
            set { base.wielkoscStrony = value; }
        }
    }

    public class pobierzListeRolWrap : pobierzListeRol
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string nazwaRoli
        {
            get { return base.nazwaRoli; }
            set { base.nazwaRoli = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string nazwaUzytkownika
        {
            get { return base.nazwaUzytkownika; }
            set { base.nazwaUzytkownika = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? numerStrony
        {
            get { return base.numerStrony; }
            set { base.numerStrony = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? wielkoscStrony
        {
            get { return base.wielkoscStrony; }
            set { base.wielkoscStrony = value; }
        }
    }

    public class zalogujWrap : zaloguj
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string login
        {
            get { return base.login; }
            set { base.login = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string haslo
        {
            get { return base.haslo; }
            set { base.haslo = value; }
        }
    }
}