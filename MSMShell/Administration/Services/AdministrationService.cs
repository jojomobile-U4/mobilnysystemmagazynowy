#region

using System;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI;
using MobilnySystemMagazynowy.Administration.DataModel;
using MobilnySystemMagazynowy.Administration.ProxyWrappers;

#endregion

namespace MobilnySystemMagazynowy.Administration.Services
{
    [Service(typeof (IAdministrationService))]
    public class AdministrationService : ServiceBase, IAdministrationService
    {
        #region Properties

        protected override WebServiceConfiguration Configuration
        {
            get { return MyWorkItem.Configuration.AdministracjaWS; }
        }

        #endregion

        #region Constructors

        public AdministrationService([ServiceDependency] WorkItem workItem)
            : base(workItem)
        {
        }

        #endregion

        #region IAdministrationService Members

        public StanZalogowania LogOn(string user, string password)
        {
            var stan = new StanZalogowania();
            try
            {
                using (var serviceAgent = new WsAdministracjaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new zalogujWrap {login = user, haslo = password};
                    stan = new StanZalogowania(serviceAgent.zaloguj(param).@return);
                }
            }
            catch (Exception ex)
            {
                stan.StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, ex.Message);
            }
            return stan;
        }

        public ListaRol PobierzRoleUzytkownika(string user)
        {
            var lista = new ListaRol();
            try
            {
                using (var serviceAgent = new WsAdministracjaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzListeRolWrap();
                    param.nazwaUzytkownika = user;
                    param.numerStrony = 0;
                    lista = new ListaRol(serviceAgent.pobierzListeRol(param).@return);
                }
            }
            catch (Exception ex)
            {
                lista.StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, ex.Message);
            }
            return lista;
        }

        #endregion
    }
}