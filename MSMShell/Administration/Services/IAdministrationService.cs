#region

using MobilnySystemMagazynowy.Administration.DataModel;

#endregion

namespace MobilnySystemMagazynowy.Administration.Services
{
    public interface IAdministrationService
    {
        StanZalogowania LogOn(string user, string password);
        ListaRol PobierzRoleUzytkownika(string user);
    }
}