#region

using System;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using MobilnySystemMagazynowy.Administration.DataModel;

#endregion

namespace WydanieMRModule.WyborStrefy
{
    [SmartPart]
    public partial class RoleSelectionView : ViewBase, IWyborStrefyView
    {
        #region Constructor

        public RoleSelectionView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #endregion

        #region IWyborStrefyView Members

        public event EventHandler OK;

        public event EventHandler Anuluj;

        public ListaRol DataSource
        {
            set { listaRolBindingSource.DataSource = value; }
        }

        public string WybranaRola
        {
            set { lstRole.SelectedValue = value; }
            get { return lstRole.SelectedValue as string ?? string.Empty; }
        }

        #endregion

        #region Protected Methods

        protected void OnOK(object sender, EventArgs e)
        {
            if (OK != null)
            {
                OK(sender, e);
            }
        }

        protected void OnAnuluj(object sender, EventArgs e)
        {
            if (Anuluj != null)
            {
                Anuluj(sender, e);
            }
        }

        #endregion
    }
}