#region

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Common;
using Common.Base;
using MobilnySystemMagazynowy.Administration;
using MobilnySystemMagazynowy.Administration.DataModel;

#endregion

namespace WydanieMRModule.WyborStrefy
{
    public class RoleSelectionViewPresenter : PresenterBase
    {
        #region Private Fields

        private readonly SortedList<string, Rola> m_ListaRol;

        #endregion

        #region Properties

        public IWyborStrefyView View
        {
            get { return m_view as IWyborStrefyView; }
        }

        protected AdministrationWorkItem MyWorkItem
        {
            get { return WorkItem as AdministrationWorkItem; }
        }

        #endregion

        #region Constructor

        public RoleSelectionViewPresenter(IWyborStrefyView view)
            : base(view)
        {
            m_ListaRol = new SortedList<string, Rola>();
        }

        #endregion

        #region Methods

        public void ZaladujDaneDoWidoku(ListaRol listaRol)
        {
            m_ListaRol.Clear();
            foreach (var rola in listaRol.Role)
            {
                m_ListaRol.Add(rola.Nazwa, rola);
            }

            View.DataSource = listaRol;
            var poprzedniaRola = MyWorkItem.RootWorkItem.State[StateConstants.ROLE] as Rola;
            if (poprzedniaRola != null)
            {
                View.WybranaRola = poprzedniaRola.Nazwa;
            }
            else if (listaRol.Role.Count > 0)
            {
                View.WybranaRola = listaRol.Role[0].Nazwa;
            }
            else
            {
                View.WybranaRola = string.Empty;
            }
        }

        #endregion

        protected override void AttachView()
        {
            View.OK += ViewOK;
            View.Anuluj += ViewAnuluj;
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    e.Handled = true;
                    ViewOK(this, EventArgs.Empty);
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    ViewAnuluj(this, EventArgs.Empty);
                    break;
            }
        }

        #region Obsluga widoku

        private void ViewOK(object sender, EventArgs e)
        {
            var rola = m_ListaRol[View.WybranaRola];
            if (rola != null)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.ROLE] = rola;
                CloseView();
                MyWorkItem.ZatwierdzWyborRoli();
            }
        }

        private void ViewAnuluj(object sender, EventArgs e)
        {
            CloseView();
        }

        #endregion
    }
}