#region

using System;
using Common.Base;
using MobilnySystemMagazynowy.Administration.DataModel;

#endregion

namespace WydanieMRModule.WyborStrefy
{
    public interface IWyborStrefyView : IViewBase
    {
        ListaRol DataSource { set; }

        string WybranaRola { set; get; }
        event EventHandler OK;
        event EventHandler Anuluj;
    }
}