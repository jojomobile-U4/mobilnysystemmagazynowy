using Common.Components;

namespace WydanieMRModule.WyborStrefy
{
	partial class RoleSelectionView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.lbTytul = new Common.Components.MSMLabel();
            this.btnAnuluj = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.listaRolBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lstRole = new System.Windows.Forms.ListBox();
            this.pnlNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listaRolBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnOK);
            this.pnlNavigation.Controls.Add(this.btnAnuluj);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 272);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 28);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 1;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "WYB�R PROFILU";
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnAnuluj.Location = new System.Drawing.Point(153, 6);
            this.btnAnuluj.Name = "btnAnuluj";
            this.btnAnuluj.Size = new System.Drawing.Size(84, 20);
            this.btnAnuluj.TabIndex = 3;
            this.btnAnuluj.TabStop = false;
            this.btnAnuluj.Text = "&Esc Anuluj";
            this.btnAnuluj.Click += new System.EventHandler(this.OnAnuluj);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnOK.Location = new System.Drawing.Point(66, 6);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(84, 20);
            this.btnOK.TabIndex = 2;
            this.btnOK.TabStop = false;
            this.btnOK.Text = "&Ret OK";
            this.btnOK.Click += new System.EventHandler(this.OnOK);
            // 
            // listaRolBindingSource
            // 
            this.listaRolBindingSource.DataMember = "Role";
            this.listaRolBindingSource.DataSource = typeof(MobilnySystemMagazynowy.Administration.DataModel.ListaRol);
            // 
            // lstRole
            // 
            this.lstRole.DataSource = this.listaRolBindingSource;
            this.lstRole.DisplayMember = "Nazwa";
            this.lstRole.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lstRole.Location = new System.Drawing.Point(3, 20);
            this.lstRole.Name = "lstRole";
            this.lstRole.Size = new System.Drawing.Size(234, 249);
            this.lstRole.TabIndex = 0;
            this.lstRole.ValueMember = "Nazwa";
            // 
            // RoleSelectionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lstRole);
            this.Controls.Add(this.lbTytul);
            this.Name = "RoleSelectionView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lstRole, 0);
            this.pnlNavigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listaRolBindingSource)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private MSMLabel lbTytul;
		private System.Windows.Forms.Button btnAnuluj;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.BindingSource listaRolBindingSource;
		private System.Windows.Forms.ListBox lstRole;
	}
}
