namespace MobilnySystemMagazynowy
{
    partial class MSMShellForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MSMShellForm));
			this.mainWorkSpace = new Microsoft.Practices.Mobile.CompositeUI.WinForms.DeckWorkspace();
			this.pnlStatus = new System.Windows.Forms.Panel();
			this.pbAsyncDownload = new System.Windows.Forms.PictureBox();
			this.lblUser = new System.Windows.Forms.Label();
			this.lblClock = new System.Windows.Forms.Label();
			this.pbNavigationState = new System.Windows.Forms.PictureBox();
			this.pbNetStatus = new System.Windows.Forms.PictureBox();
			this.tClock = new System.Windows.Forms.Timer();
			this.pnlStatus.SuspendLayout();
			this.SuspendLayout();
			// 
			// mainWorkSpace
			// 
			this.mainWorkSpace.BackColor = System.Drawing.SystemColors.Control;
			this.mainWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainWorkSpace.Location = new System.Drawing.Point(0, 0);
			this.mainWorkSpace.Name = "mainWorkSpace";
			this.mainWorkSpace.Size = new System.Drawing.Size(240, 300);
			this.mainWorkSpace.TabIndex = 0;
			// 
			// pnlStatus
			// 
			this.pnlStatus.BackColor = System.Drawing.SystemColors.GrayText;
			this.pnlStatus.Controls.Add(this.pbAsyncDownload);
			this.pnlStatus.Controls.Add(this.lblUser);
			this.pnlStatus.Controls.Add(this.lblClock);
			this.pnlStatus.Controls.Add(this.pbNavigationState);
			this.pnlStatus.Controls.Add(this.pbNetStatus);
			this.pnlStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.pnlStatus.Location = new System.Drawing.Point(0, 300);
			this.pnlStatus.Name = "pnlStatus";
			this.pnlStatus.Size = new System.Drawing.Size(240, 20);
			// 
			// pbAsyncDawnload
			// 
			this.pbAsyncDownload.Location = new System.Drawing.Point(40, 2);
			this.pbAsyncDownload.Name = "pbAsyncDawnload";
			this.pbAsyncDownload.Size = new System.Drawing.Size(16, 16);
			this.pbAsyncDownload.Visible = false;
			// 
			// lblUser
			// 
			this.lblUser.Location = new System.Drawing.Point(40, 2);
			this.lblUser.Name = "lblUser";
			this.lblUser.Size = new System.Drawing.Size(160, 16);
			this.lblUser.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblClock
			// 
			this.lblClock.Location = new System.Drawing.Point(200, 2);
			this.lblClock.Name = "lblClock";
			this.lblClock.Size = new System.Drawing.Size(40, 16);
			this.lblClock.Text = "12:45";
			// 
			// pbNavigationState
			// 
			this.pbNavigationState.Location = new System.Drawing.Point(21, 2);
			this.pbNavigationState.Name = "pbNavigationState";
			this.pbNavigationState.Size = new System.Drawing.Size(16, 16);
			// 
			// pbNetStatus
			// 
			this.pbNetStatus.Location = new System.Drawing.Point(2, 2);
			this.pbNetStatus.Name = "pbNetStatus";
			this.pbNetStatus.Size = new System.Drawing.Size(16, 16);
			// 
			// tClock
			// 
			this.tClock.Interval = 1000;
			this.tClock.Tick += new System.EventHandler(this.tClock_Tick);
			// 
			// MSMShellForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(240, 320);
			this.ControlBox = false;
			this.Controls.Add(this.mainWorkSpace);
			this.Controls.Add(this.pnlStatus);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.Location = new System.Drawing.Point(0, 0);
			this.Name = "MSMShellForm";
			this.Text = "Mobilny System Magazynowy";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Deactivate += new System.EventHandler(this.MSMShellForm_Deactivate);
			this.Activated += new System.EventHandler(this.MSMShellForm_Activated);
			this.pnlStatus.ResumeLayout(false);
			this.ResumeLayout(false);

        }        

        #endregion

		private Microsoft.Practices.Mobile.CompositeUI.WinForms.DeckWorkspace mainWorkSpace;
		private System.Windows.Forms.Panel pnlStatus;
		private System.Windows.Forms.PictureBox pbNetStatus;
		private System.Windows.Forms.PictureBox pbNavigationState;
		private System.Windows.Forms.Timer tClock;
		private System.Windows.Forms.Label lblClock;
		private System.Windows.Forms.Label lblUser;
		private System.Windows.Forms.PictureBox pbAsyncDownload;
    }
}

