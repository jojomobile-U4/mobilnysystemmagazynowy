﻿using System;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.DataModel;
using WydanieZbiorczeMRModule.DataModel;

namespace WydanieZbiorczeMRModule.ListaMR.ListaMRKontrahenta
{
    class ListaMRKontrahentaPresenter : PresenterBase
    {
        private const string Reserved = "R";
        private const string Abandoned = "P";
        public const string NazwaFormularza = "WYDANIE - LISTA MR KONR";
        private const string PomyslneWywolanieRaportu = "Wydruk został wysłany na drukarkę.";

        private Naglowek WybranyNaglowek;
        private ListaNaglowkow ListaNaglowkow;

        public ListaMRKontrahentaPresenter(IViewBase view)
            : base(view)
        {
        }

        protected ListaMRWorkItem MyWorkItem
        {
            get
            {
                return WorkItem as ListaMRWorkItem;
            }
        }

        public IListaMRKontrahentaView View
        {
            get { return m_view as IListaMRKontrahentaView; }
        }

        public void WyswietlListe(Naglowek naglowek)
        {
            WybranyNaglowek = naglowek;

            if (!WybranyNaglowek.Status.Equals(Reserved))
                MyWorkItem.ZmienStanZarezerwowania(WybranyNaglowek);

            if (WybranyNaglowek.MagaIdFk != null)
            {
                ListaNaglowkow = MyWorkItem.PobierzListeMRWgMagazynu(WybranyNaglowek.MagaIdFk, 0);
            }
            else
                if (WybranyNaglowek.SymbolOdbiorcy != null)
                {
                    ListaNaglowkow = MyWorkItem.PobierzListeMRWgKontrahenta(WybranyNaglowek.SymbolOdbiorcy, 0);
                }

            AktualizujWidok();
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Return:
                    e.Handled = true;
                    Pobierz(this, EventArgs.Empty);
                    break;
                case Keys.Space:
                    e.Handled = true;
                    Zaznacz(this, EventArgs.Empty);
                    break;
                case Keys.D:
                    e.Handled = true;
                    UruchomRaport();
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    Anuluj(this, EventArgs.Empty);
                    break;
            }
        }

        protected override void AttachView()
        {
            View.Pobierz += Pobierz;
            View.Zaznacz += Zaznacz;
        }

        private void Anuluj(object sender, EventArgs e)
        {
            WorkItemBase.MainWorkspace.Close(View);
            MyWorkItem.OdswiezListeDokumentow();
            MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
            MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
            MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
            MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
        }

        private void Pobierz(object sender, EventArgs e)
        {
            if ((WybranyNaglowek.MagaIdFk != null) && (WybranyNaglowek.MagaIdFk != 0))
            {
                MyWorkItem.PobierzDokumentyUzytkownikaWgMagazynu(WybranyNaglowek.MagaIdFk);
            }
            else
                MyWorkItem.PobierzDokumentyUzytkownikaWgKontrahenta(WybranyNaglowek.SymbolOdbiorcy);
        }

        private void Zaznacz(object sender, EventArgs e)
        {
            var zaznaczonyNaglowek = View.ZaznaczonyNaglowek;
            if (zaznaczonyNaglowek != null)
            {
                var idx = ListaNaglowkow.Lista.IndexOf(zaznaczonyNaglowek);
                MyWorkItem.ZmienStanZarezerwowania(zaznaczonyNaglowek);

                if (StatusOperacji.ERROR.Equals(MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                {
                    return;
                }

                ListaNaglowkow.Lista[idx].Status = ListaNaglowkow.Lista[idx].Status.Equals(Reserved) ? Abandoned : Reserved;

                var indeksZaznaczonegoNaglowka = View.IndeksZaznaczonegoNaglowka;
                AktualizujWidok();
                View.IndeksZaznaczonegoNaglowka = indeksZaznaczonegoNaglowka;
            }
        }

        private void AktualizujWidok()
        {
            View.Symbol = WybranyNaglowek.SymbolOdbiorcy;
            View.Nazwa = WybranyNaglowek.NazwaOdbiorcy;
            View.ListaDokumentowMRDataSource.Clear();
            if (ListaNaglowkow != null)
            {
                View.ListaDokumentowMRDataSource.Add(PrzygotujWiersz(1, PobierzZListy(WybranyNaglowek)));

                var i = 1;
                foreach (var naglowek in ListaNaglowkow.Lista)
                {
                    i++;

                    if (WybranyNaglowek.Id != naglowek.Id)
                        View.ListaDokumentowMRDataSource.Add(PrzygotujWiersz(i, naglowek));
                }
            }
        }

        private static ListViewItem PrzygotujWiersz(int numerWiersza, Naglowek naglowek)
        {
            var status = Constants.NO;
            if (naglowek.Status.Equals(Reserved))
                status = Constants.YES;

            return new ListViewItem(new[] { numerWiersza.ToString(), naglowek.SymbolDokumentu, naglowek.Dostepnosc, status }) { Tag = naglowek };
        }

        private Naglowek PobierzZListy(Naglowek naglowek)
        {
            foreach (var n in ListaNaglowkow.Lista)
            {
                if (n.Id == naglowek.Id)
                    return n;
            }

            return null;
        }

        private void UruchomRaport()
        {
            if (WybranyNaglowek != null && (MyWorkItem.UruchomRaport(NazwaFormularza,
                                                                         WybranyNaglowek.SymbolOdbiorcy)))
            {
                MessageBox.Show(PomyslneWywolanieRaportu);
                MyWorkItem.Activate();
            }
        }

    }
}