﻿using System;
using System.Windows.Forms;
using Common.Base;
using WydanieZbiorczeMRModule.DataModel;

namespace WydanieZbiorczeMRModule.ListaMR.ListaMRKontrahenta
{
    public partial class ListaMRKontrahentaView : ViewBase, IListaMRKontrahentaView
    {
        public event EventHandler Zaznacz;
        public event EventHandler Pobierz;

        private const char SpaceChar = (char)32;

        public ListaMRKontrahentaView()
        {
            InitializeComponent();
        }

        private void OnBtnZaznacz(object sender, EventArgs e)
        {
            if (Zaznacz != null)
                Zaznacz(sender, e);
        }

        private void OnBtnPobierz(object sender, EventArgs e)
        {
            if (Pobierz != null)
                Pobierz(sender, e);
        }

        private void OnLstZaznacz(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == SpaceChar)
            {
                if (Zaznacz != null)
                    Zaznacz(sender, e);
            }
        }

        public string Symbol
        {
            get
            {
                return tbSymbolOdbiorcy.Text;
            }

            set
            {
                tbSymbolOdbiorcy.Text = value;
            }
        }

        public string Nazwa
        {
            get
            {
                return tbNazwaOdbiorcy.Text;
            }

            set
            {
                tbNazwaOdbiorcy.Text = value;
            }
        }

        public ListView.ListViewItemCollection ListaDokumentowMRDataSource
        {
            get
            {
                return lstDokumentyMR.Items;
            }
        }

        public Naglowek ZaznaczonyNaglowek
        {
            get
            {
                if (lstDokumentyMR.SelectedIndices.Count == 0)
                {
                    return null;
                }

                return lstDokumentyMR.Items[lstDokumentyMR.SelectedIndices[0]].Tag as Naglowek;
            }
        }

        public int IndeksZaznaczonegoNaglowka
        {
            get
            {
                return lstDokumentyMR.SelectedIndices[0];
            }
            set
            {
                if (lstDokumentyMR.Items.Count >= value)
                {
                    lstDokumentyMR.Items[value].Selected = true;
                    lstDokumentyMR.Items[value].Focused = true;
                }
            }
        }
    }
}