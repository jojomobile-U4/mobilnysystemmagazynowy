﻿namespace WydanieZbiorczeMRModule.ListaMR.ListaMRKontrahenta
{
    partial class ListaMRKontrahentaView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnZaznacz = new System.Windows.Forms.Button();
            this.btnPobierz = new System.Windows.Forms.Button();
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbNazwaOdbiorcy = new System.Windows.Forms.TextBox();
            this.lbNazwaOdbiorcy = new System.Windows.Forms.Label();
            this.tbSymbolOdbiorcy = new System.Windows.Forms.TextBox();
            this.lbSymbolOdbiorcy = new System.Windows.Forms.Label();
            this.lstDokumentyMR = new Common.Components.MSMListView();
            this.chLp = new System.Windows.Forms.ColumnHeader();
            this.chDokument = new System.Windows.Forms.ColumnHeader();
            this.chDostepnosc = new System.Windows.Forms.ColumnHeader();
            this.chDoRealizacji = new System.Windows.Forms.ColumnHeader();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnZaznacz);
            this.pnlNavigation.Controls.Add(this.btnPobierz);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 255);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 25);
            // 
            // btnZaznacz
            // 
            this.btnZaznacz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnZaznacz.Location = new System.Drawing.Point(73, 2);
            this.btnZaznacz.Name = "btnZaznacz";
            this.btnZaznacz.Size = new System.Drawing.Size(80, 20);
            this.btnZaznacz.TabIndex = 0;
            this.btnZaznacz.TabStop = false;
            this.btnZaznacz.Text = "&Spc Zaznacz";
            this.btnZaznacz.Click += new System.EventHandler(this.OnBtnZaznacz);
            // 
            // btnPobierz
            // 
            this.btnPobierz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPobierz.Location = new System.Drawing.Point(157, 2);
            this.btnPobierz.Name = "btnPobierz";
            this.btnPobierz.Size = new System.Drawing.Size(80, 20);
            this.btnPobierz.TabIndex = 1;
            this.btnPobierz.TabStop = false;
            this.btnPobierz.Text = "&Ret Pobierz";
            this.btnPobierz.Click += new System.EventHandler(this.OnBtnPobierz);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 1;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "WYD. ZBIORCZE - LISTA MR KONTRAHENTA";
            // 
            // tbNazwaOdbiorcy
            // 
            this.tbNazwaOdbiorcy.Location = new System.Drawing.Point(54, 55);
            this.tbNazwaOdbiorcy.Name = "tbNazwaOdbiorcy";
            this.tbNazwaOdbiorcy.ReadOnly = true;
            this.tbNazwaOdbiorcy.Size = new System.Drawing.Size(183, 21);
            this.tbNazwaOdbiorcy.TabIndex = 8;
            this.tbNazwaOdbiorcy.TabStop = false;
            // 
            // lbNazwaOdbiorcy
            // 
            this.lbNazwaOdbiorcy.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbNazwaOdbiorcy.Location = new System.Drawing.Point(2, 56);
            this.lbNazwaOdbiorcy.Name = "lbNazwaOdbiorcy";
            this.lbNazwaOdbiorcy.Size = new System.Drawing.Size(51, 16);
            this.lbNazwaOdbiorcy.Text = "Nazwa:";
            // 
            // tbSymbolOdbiorcy
            // 
            this.tbSymbolOdbiorcy.Location = new System.Drawing.Point(55, 28);
            this.tbSymbolOdbiorcy.Name = "tbSymbolOdbiorcy";
            this.tbSymbolOdbiorcy.ReadOnly = true;
            this.tbSymbolOdbiorcy.Size = new System.Drawing.Size(183, 21);
            this.tbSymbolOdbiorcy.TabIndex = 7;
            this.tbSymbolOdbiorcy.TabStop = false;
            // 
            // lbSymbolOdbiorcy
            // 
            this.lbSymbolOdbiorcy.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbolOdbiorcy.Location = new System.Drawing.Point(3, 28);
            this.lbSymbolOdbiorcy.Name = "lbSymbolOdbiorcy";
            this.lbSymbolOdbiorcy.Size = new System.Drawing.Size(50, 15);
            this.lbSymbolOdbiorcy.Text = "Symbol:";
            // 
            // lstDokumentyMR
            // 
            this.lstDokumentyMR.Columns.Add(this.chLp);
            this.lstDokumentyMR.Columns.Add(this.chDokument);
            this.lstDokumentyMR.Columns.Add(this.chDostepnosc);
            this.lstDokumentyMR.Columns.Add(this.chDoRealizacji);
            this.lstDokumentyMR.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lstDokumentyMR.FullRowSelect = true;
            this.lstDokumentyMR.Location = new System.Drawing.Point(3, 88);
            this.lstDokumentyMR.Name = "lstDokumentyMR";
            this.lstDokumentyMR.Size = new System.Drawing.Size(234, 163);
            this.lstDokumentyMR.TabIndex = 11;
            this.lstDokumentyMR.View = System.Windows.Forms.View.Details;
            this.lstDokumentyMR.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnLstZaznacz);
            // 
            // chLp
            // 
            this.chLp.Text = "Lp";
            this.chLp.Width = 25;
            // 
            // chDokument
            // 
            this.chDokument.Text = "Dokument";
            this.chDokument.Width = 95;
            // 
            // chDostepnosc
            // 
            this.chDostepnosc.Text = "Dost.";
            this.chDostepnosc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.chDostepnosc.Width = 50;
            // 
            // chDoRealizacji
            // 
            this.chDoRealizacji.Text = "Do realizacji";
            this.chDoRealizacji.Width = 60;
            // 
            // ListaMRKontrahentaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lstDokumentyMR);
            this.Controls.Add(this.tbNazwaOdbiorcy);
            this.Controls.Add(this.lbNazwaOdbiorcy);
            this.Controls.Add(this.tbSymbolOdbiorcy);
            this.Controls.Add(this.lbSymbolOdbiorcy);
            this.Controls.Add(this.lbTytul);
            this.Name = "ListaMRKontrahentaView";
            this.Size = new System.Drawing.Size(240, 280);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbSymbolOdbiorcy, 0);
            this.Controls.SetChildIndex(this.tbSymbolOdbiorcy, 0);
            this.Controls.SetChildIndex(this.lbNazwaOdbiorcy, 0);
            this.Controls.SetChildIndex(this.tbNazwaOdbiorcy, 0);
            this.Controls.SetChildIndex(this.lstDokumentyMR, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnZaznacz;
        private System.Windows.Forms.Button btnPobierz;
        private Common.Components.MSMLabel lbTytul;
        private System.Windows.Forms.TextBox tbNazwaOdbiorcy;
        private System.Windows.Forms.Label lbNazwaOdbiorcy;
        private System.Windows.Forms.TextBox tbSymbolOdbiorcy;
        private System.Windows.Forms.Label lbSymbolOdbiorcy;
        private Common.Components.MSMListView lstDokumentyMR;
        private System.Windows.Forms.ColumnHeader chLp;
        private System.Windows.Forms.ColumnHeader chDokument;
        private System.Windows.Forms.ColumnHeader chDostepnosc;
        private System.Windows.Forms.ColumnHeader chDoRealizacji;
    }
}