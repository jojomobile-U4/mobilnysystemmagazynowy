﻿using System;
using System.Windows.Forms;
using Common.Base;
using WydanieZbiorczeMRModule.DataModel;

namespace WydanieZbiorczeMRModule.ListaMR.ListaMRKontrahenta
{
    interface IListaMRKontrahentaView : IViewBase
    {
        event EventHandler Zaznacz;
        event EventHandler Pobierz;

        string Symbol { get; set; }
        string Nazwa { get; set; }
        Naglowek ZaznaczonyNaglowek { get; }
        int IndeksZaznaczonegoNaglowka { get; set; }
        ListView.ListViewItemCollection ListaDokumentowMRDataSource { get; }
    }
}