using Common.Base;
using System;
using WydanieZbiorczeMRModule.DataModel;

namespace WydanieZbiorczeMRModule.ListaMR
{
	public interface IListaMRView: IViewBase
	{
		event EventHandler Szukaj;
		event EventHandler Pobierz;
		event EventHandler Nastepny;
		event EventHandler Poprzedni;
		event EventHandler ZaznaczonyDokumentMRChanged;

		Naglowek DataSource
		{
			set;
		}

		long? IloscLp
		{
			set;
		}

		Common.Components.MSMListView.ListViewItemCollection DokumentyMRDataSource
		{
			get;
		}

		bool PoprzedniEnabled
		{
			get;
			set;
		}

		bool NastepnyEnabled
		{
			get;
			set;
		}

		Naglowek ZaznaczonyDokumentMR
		{
			get;
		}

        void AsynchUstawIlosc(long? ilosc);

        void UstawAktywnaListe();
	}
}
