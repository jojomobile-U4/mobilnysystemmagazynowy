using System;
using Common.Base;
using System.Windows.Forms;
using WydanieZbiorczeMRModule.ListaMR.ListaMRKontrahenta;
using Common;
using WydanieZbiorczeMRModule.DataModel;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Common.DataModel;

namespace WydanieZbiorczeMRModule.ListaMR
{
    public class ListaMRViewPresenter : PresenterBase
    {
        #region Constants

        private const int NASTEPNA = 1;
        private const int POPRZEDNIA = -1;
        private const int PIERWSZA = 0;

        #endregion
        #region Private Fields

        private ListaNaglowkow listaNaglowkow;
        private Naglowek aktualnyNaglowek;
        private int numerStrony;
        private int zmianaStrony;
        private int startoweLp;
        private bool odswiezanieListyDokumentow;

        #endregion
        #region Properties

        public IListaMRView View
        {
            get { return m_view as IListaMRView; }
        }

        protected ListaMRWorkItem MyWorkItem
        {
            get { return WorkItem as ListaMRWorkItem; }
        }

        #endregion
        #region Constructors

        public ListaMRViewPresenter(IListaMRView view)
            : base(view)
        {
            ResetujStronicowanie();
            startoweLp = 1;
            odswiezanieListyDokumentow = false;
        }

        #endregion
        #region Methods

        public void ZaladujDaneDoWidoku(ListaNaglowkow listaNaglowkow)
        {
            if (listaNaglowkow == null ||
                listaNaglowkow.Lista.Count == 0)
            {
                if (listaNaglowkow == null)
                {
                    this.listaNaglowkow = new ListaNaglowkow();
                }
                else
                {
                    this.listaNaglowkow = listaNaglowkow;
                }

                aktualnyNaglowek = null;
            }
            else
            {
                this.listaNaglowkow = listaNaglowkow;
                aktualnyNaglowek = this.listaNaglowkow.Lista[0];
            }
            AktualizujWidok();
            View.UstawAktywnaListe();
        }

        internal void ResetujStronicowanie()
        {
            numerStrony = PIERWSZA;
            zmianaStrony = NASTEPNA;
            startoweLp = 1;
        }

        #endregion
        #region Override Members

        protected override void AttachView()
        {
            View.Nastepny += ViewNastepny;
            View.Pobierz += ViewPobierz;
            View.Poprzedni += ViewPoprzedni;
            View.Szukaj += ViewSzukaj;
            View.ZaznaczonyDokumentMRChanged += ViewZaznaczonyDokumentMRChanged;
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    e.Handled = true;
                    ViewPoprzedni(this, EventArgs.Empty);
                    break;
                case Keys.Right:
                    e.Handled = true;
                    ViewNastepny(this, EventArgs.Empty);
                    break;
                case Keys.D1:
                    e.Handled = true;
                    ViewSzukaj(this, EventArgs.Empty);
                    break;
                case Keys.Enter:
                    e.Handled = true;
                    ViewPobierz(this, EventArgs.Empty);
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    MyWorkItem.AnulujOperacjeAsynchroniczne();
                    CloseView();
                    break;
            }
        }

        #endregion
        #region Obsluga widoku

        private void ViewZaznaczonyDokumentMRChanged(object sender, EventArgs e)
        {
            //jezeli zmiana zaznaczonego dokumentu nie zostala spowodowana wyczyszczeniem listy dokumentow 
            //(podczas przetwarzania metody AktualizujDokumentyMR)
            if (!odswiezanieListyDokumentow)
            {
                aktualnyNaglowek = View.ZaznaczonyDokumentMR;
                View.DataSource = aktualnyNaglowek;
            }
        }

        private void ViewSzukaj(object sender, EventArgs e)
        {
            MyWorkItem.Szukaj();
            View.UstawAktywnaListe();
        }

        private void ViewPobierz(object sender, EventArgs e)
        {
            var listaMRKontrahentaPresenter = WorkItem.Items.Get<ListaMRKontrahentaPresenter>(ItemsConstants.RealizacjaZbiorczaMrKontrahentaPresenter);
            listaMRKontrahentaPresenter.WyswietlListe(aktualnyNaglowek);
            MyWorkItem.MainWorkspace.Show(listaMRKontrahentaPresenter.View);
        }

        private void ViewPoprzedni(object sender, EventArgs e)
        {
            if (listaNaglowkow != null)
            {
                //TODO: sprawdzic czy jest poprzednia paczka (strona)

                //jezeli numer strony = 0, znaczy ze nie ma wczesniejszych stron
                if (numerStrony > 0)
                {
                    ZmienStrone(POPRZEDNIA);
                }
            }
            View.UstawAktywnaListe();
        }

        private void ViewNastepny(object sender, EventArgs e)
        {
            //sprawdzenie czy nie jestesmy na ostatniej stronie
            //jezeli nie ma indeksow, znaczy ze nie ma dalszych stron
            if (listaNaglowkow != null &&
                listaNaglowkow.Lista != null &&
                listaNaglowkow.Lista.Count == MyWorkItem.Configuration.WydanieMRWS.WielkoscStrony)
            {
                //TODO: sprawdzic czy jest nastepna paczka (strona)
                ZmienStrone(NASTEPNA);
            }
            View.UstawAktywnaListe();
        }
        private void AktualizujWidok()
        {
            View.DataSource = aktualnyNaglowek;

            View.IloscLp = listaNaglowkow.IloscNaglowkow;
            AktualizujDokumentyMR();
        }

        private void AktualizujDokumentyMR()
        {
            odswiezanieListyDokumentow = true;

            View.DokumentyMRDataSource.Clear();

            if (listaNaglowkow != null)
            {
                Naglowek naglowek;
                for (int i = 0; i < listaNaglowkow.Lista.Count; i++)
                {
                    naglowek = listaNaglowkow.Lista[i];
                    View.DokumentyMRDataSource.Add(UtworzWpis(startoweLp + i, naglowek));
                    View.DokumentyMRDataSource[i].Selected = naglowek.Equals(aktualnyNaglowek);
                }
            }

            odswiezanieListyDokumentow = false;
        }

        private ListViewItem UtworzWpis(int lp, Naglowek naglowek)
        {
            var elementy = new[] { 
				lp.ToString(),
				naglowek.SymbolDokumentu,
				naglowek.Status,
				naglowek.Dostepnosc,
				naglowek.Priorytet.ToString()};
            var wpis = new ListViewItem(elementy) { Tag = naglowek };

            return wpis;
        }

        private void ZmienStrone(int kierunek)
        {
            numerStrony += kierunek;
            zmianaStrony = kierunek;
            startoweLp += kierunek * MyWorkItem.Configuration.WydanieMRWS.WielkoscStrony;
            ZaladujDaneDoWidoku(MyWorkItem.PobierzListeMR(numerStrony));
        }

        #endregion


        [EventSubscription(EventBrokerConstants.ODCZYTANO_ILOSC, ThreadOption.Background)]
        public void OnOdczytanoIlosc(object sender, EventArgs e)
        {
            try
            {
                var ilosc = (long)MyWorkItem.RootWorkItem.State[StateConstants.ASYNC_OP_ILOSC_RESULT];
                View.AsynchUstawIlosc(ilosc);
            }
            catch (Exception)
            {
                MyWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
            }
            MyWorkItem.OnAsyncOperationFinished();
        }

    }
}
