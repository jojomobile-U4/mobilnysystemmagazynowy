using System;
using Common.SearchForm;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Common;

namespace WydanieZbiorczeMRModule.ListaMR.Search
{
	public class ListaMRSearchViewPresenter: SearchFormPresenter
	{
		#region Events

        [EventPublication(EventBrokerConstants.RealizacjaZbiorczaWyszukajMr)]
		public event EventHandler WyszukajDokumentyMP;

		#endregion
		#region Properties

		public IListaMRSearchView View
		{
			get { return m_view as IListaMRSearchView; }
		}

		#endregion
		#region Constructors

		public ListaMRSearchViewPresenter(IListaMRSearchView view):base(view)
		{

		}

		#endregion
		#region Overrided Memebers

		protected override void OnViewSzukaj(object sender, EventArgs e)
		{
			base.OnViewSzukaj(sender, e);
			OnWyszukajDokumentyMR();
		}

		protected override void OnViewUstawOstatnieZapytanie(object sender, EventArgs e)
		{
			base.OnViewUstawOstatnieZapytanie(sender, e);
			View.Kryteria = State[StateConstants.CRITERIAS] as KryteriaZapytaniaListyMR ?? new KryteriaZapytaniaListyMR();
		}

		#endregion
		#region Raising Events

		protected void OnWyszukajDokumentyMR()
		{
			State[StateConstants.CRITERIAS] = View.Kryteria;
			if (WyszukajDokumentyMP != null)
			{
				WyszukajDokumentyMP(this, EventArgs.Empty);
			}
		}

		#endregion
	}
}
