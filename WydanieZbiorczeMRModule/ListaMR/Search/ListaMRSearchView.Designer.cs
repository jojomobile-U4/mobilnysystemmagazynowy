
using Common.Components;
namespace WydanieZbiorczeMRModule.ListaMR.Search
{
	partial class ListaMRSearchView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbDokument = new System.Windows.Forms.TextBox();
            this.lbSymbol = new System.Windows.Forms.Label();
            this.tbSymbolOdbiorcy = new System.Windows.Forms.TextBox();
            this.lblSymbolOdbiorcy = new System.Windows.Forms.Label();
            this.lbData = new System.Windows.Forms.Label();
            this.tbPriorytet = new System.Windows.Forms.TextBox();
            this.lbPriorytet = new System.Windows.Forms.Label();
            this.tbDostepnosc = new System.Windows.Forms.TextBox();
            this.lbDostepnosc = new System.Windows.Forms.Label();
            this.tbStatus = new System.Windows.Forms.TextBox();
            this.lbStatus = new System.Windows.Forms.Label();
            this.tbData = new Common.Components.MSMDate();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.btnAnuluj.Location = new System.Drawing.Point(165, 12);
            // 
            // btnOstatnieZapytanie
            // 
            this.btnOstatnieZapytanie.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.btnOstatnieZapytanie.Location = new System.Drawing.Point(63, 12);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.btnOK.Location = new System.Drawing.Point(3, 12);
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Location = new System.Drawing.Point(0, 264);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 36);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 7;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "WYSZUKIWANIE DOKUMENTÓW MR";
            // 
            // tbDokument
            // 
            this.tbDokument.Location = new System.Drawing.Point(72, 20);
            this.tbDokument.Name = "tbDokument";
            this.tbDokument.Size = new System.Drawing.Size(165, 21);
            this.tbDokument.TabIndex = 0;
            // 
            // lbSymbol
            // 
            this.lbSymbol.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbol.Location = new System.Drawing.Point(3, 21);
            this.lbSymbol.Name = "lbSymbol";
            this.lbSymbol.Size = new System.Drawing.Size(71, 21);
            this.lbSymbol.Text = "Dokument:";
            // 
            // tbSymbolOdbiorcy
            // 
            this.tbSymbolOdbiorcy.Location = new System.Drawing.Point(72, 116);
            this.tbSymbolOdbiorcy.Name = "tbSymbolOdbiorcy";
            this.tbSymbolOdbiorcy.Size = new System.Drawing.Size(165, 21);
            this.tbSymbolOdbiorcy.TabIndex = 4;
            // 
            // lblSymbolOdbiorcy
            // 
            this.lblSymbolOdbiorcy.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblSymbolOdbiorcy.Location = new System.Drawing.Point(3, 111);
            this.lblSymbolOdbiorcy.Name = "lblSymbolOdbiorcy";
            this.lblSymbolOdbiorcy.Size = new System.Drawing.Size(70, 31);
            this.lblSymbolOdbiorcy.Text = "Symbol odbiorcy:";
            // 
            // lbData
            // 
            this.lbData.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbData.Location = new System.Drawing.Point(3, 141);
            this.lbData.Name = "lbData";
            this.lbData.Size = new System.Drawing.Size(66, 30);
            this.lbData.Text = "Data: (dd.mm.rrrr)";
            // 
            // tbPriorytet
            // 
            this.tbPriorytet.Location = new System.Drawing.Point(72, 92);
            this.tbPriorytet.Name = "tbPriorytet";
            this.tbPriorytet.Size = new System.Drawing.Size(165, 21);
            this.tbPriorytet.TabIndex = 3;
            // 
            // lbPriorytet
            // 
            this.lbPriorytet.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbPriorytet.Location = new System.Drawing.Point(3, 93);
            this.lbPriorytet.Name = "lbPriorytet";
            this.lbPriorytet.Size = new System.Drawing.Size(71, 21);
            this.lbPriorytet.Text = "Priorytet:";
            // 
            // tbDostepnosc
            // 
            this.tbDostepnosc.Location = new System.Drawing.Point(72, 68);
            this.tbDostepnosc.Name = "tbDostepnosc";
            this.tbDostepnosc.Size = new System.Drawing.Size(165, 21);
            this.tbDostepnosc.TabIndex = 2;
            // 
            // lbDostepnosc
            // 
            this.lbDostepnosc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbDostepnosc.Location = new System.Drawing.Point(3, 69);
            this.lbDostepnosc.Name = "lbDostepnosc";
            this.lbDostepnosc.Size = new System.Drawing.Size(78, 21);
            this.lbDostepnosc.Text = "Dostępność:";
            // 
            // tbStatus
            // 
            this.tbStatus.Location = new System.Drawing.Point(72, 44);
            this.tbStatus.Name = "tbStatus";
            this.tbStatus.Size = new System.Drawing.Size(165, 21);
            this.tbStatus.TabIndex = 1;
            // 
            // lbStatus
            // 
            this.lbStatus.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbStatus.Location = new System.Drawing.Point(3, 45);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(71, 21);
            this.lbStatus.Text = "Status:";
            // 
            // tbData
            // 
            this.tbData.Location = new System.Drawing.Point(72, 140);
            this.tbData.Name = "tbData";
            this.tbData.Size = new System.Drawing.Size(165, 21);
            this.tbData.TabIndex = 5;
            this.tbData.Value = null;
            // 
            // ListaMRSearchView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.Controls.Add(this.tbStatus);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.tbDostepnosc);
            this.Controls.Add(this.lbDostepnosc);
            this.Controls.Add(this.tbPriorytet);
            this.Controls.Add(this.lbPriorytet);
            this.Controls.Add(this.tbData);
            this.Controls.Add(this.lbData);
            this.Controls.Add(this.tbSymbolOdbiorcy);
            this.Controls.Add(this.lblSymbolOdbiorcy);
            this.Controls.Add(this.lbTytul);
            this.Controls.Add(this.tbDokument);
            this.Controls.Add(this.lbSymbol);
            this.Name = "ListaMRSearchView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbSymbol, 0);
            this.Controls.SetChildIndex(this.tbDokument, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lblSymbolOdbiorcy, 0);
            this.Controls.SetChildIndex(this.tbSymbolOdbiorcy, 0);
            this.Controls.SetChildIndex(this.lbData, 0);
            this.Controls.SetChildIndex(this.tbData, 0);
            this.Controls.SetChildIndex(this.lbPriorytet, 0);
            this.Controls.SetChildIndex(this.tbPriorytet, 0);
            this.Controls.SetChildIndex(this.lbDostepnosc, 0);
            this.Controls.SetChildIndex(this.tbDostepnosc, 0);
            this.Controls.SetChildIndex(this.lbStatus, 0);
            this.Controls.SetChildIndex(this.tbStatus, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private MSMLabel lbTytul;
		private System.Windows.Forms.TextBox tbDokument;
		private System.Windows.Forms.Label lbSymbol;
		private System.Windows.Forms.TextBox tbSymbolOdbiorcy;
		private System.Windows.Forms.Label lblSymbolOdbiorcy;
		private System.Windows.Forms.Label lbData;
		private System.Windows.Forms.TextBox tbPriorytet;
		private System.Windows.Forms.Label lbPriorytet;
		private System.Windows.Forms.TextBox tbDostepnosc;
		private System.Windows.Forms.Label lbDostepnosc;
		private System.Windows.Forms.TextBox tbStatus;
		private System.Windows.Forms.Label lbStatus;
		private MSMDate tbData;
	}
}
