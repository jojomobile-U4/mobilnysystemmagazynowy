using System;

namespace WydanieZbiorczeMRModule.ListaMR.Search
{
    public class KryteriaZapytaniaListyMR
    {
        #region Private Fields

        private string m_Dokument;
        private string m_Status;
        private string m_Dostepnosc;
        private string m_Priorytet;
        private DateTime? m_Data;
        private string m_SymbolOdbiorcy;
        private string m_Strefa;

        #endregion
        #region Properties

        public string SymbolOdbiorcy
        {
            get { return m_SymbolOdbiorcy; }
            set { m_SymbolOdbiorcy = value; }
        }

        public string SortujWgDostepnosci
        {
            get;
            set;
        }

        public DateTime? Data
        {
            get { return m_Data; }
            set { m_Data = value; }
        }

        public string Priorytet
        {
            get { return m_Priorytet; }
            set { m_Priorytet = value; }
        }

        public string Dostepnosc
        {
            get { return m_Dostepnosc; }
            set { m_Dostepnosc = value; }
        }

        public string Status
        {
            get { return m_Status; }
            set { m_Status = value; }
        }

        public string Dokument
        {
            get { return m_Dokument; }
            set { m_Dokument = value; }
        }

        public string Strefa
        {
            get { return m_Strefa; }
            set { m_Strefa = value; }
        }

        public long? MagaIdFk
        {
            get;
            set;
        }

        #endregion
        #region Constructors

        public KryteriaZapytaniaListyMR()
        {

        }

        public KryteriaZapytaniaListyMR(string strefa)
            : this()
        {
            m_Strefa = strefa;
        }

        public KryteriaZapytaniaListyMR(string strefa, string dokument, string status, string dostepnosc, string priorytet, DateTime? data, string symbolOdbiorcy, long? magaIdFk)
            : this(strefa)
        {
            m_Dokument = dokument;
            m_Status = status;
            m_Dostepnosc = dostepnosc;
            m_Priorytet = priorytet;
            m_Data = data;
            m_SymbolOdbiorcy = symbolOdbiorcy;
            MagaIdFk = magaIdFk;
        }

        #endregion
    }
}
