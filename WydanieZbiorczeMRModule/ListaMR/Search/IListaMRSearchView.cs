#region

using Common.SearchForm;

#endregion

namespace WydanieZbiorczeMRModule.ListaMR.Search
{
    public interface IListaMRSearchView : ISearchForm
    {
        KryteriaZapytaniaListyMR Kryteria { get; set; }
    }
}