#region

using System;
using System.Threading;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using WydanieZbiorczeMRModule.DataModel;
using WydanieZbiorczeMRModule.EdycjaMR;
using WydanieZbiorczeMRModule.ListaMR.Search;
using WydanieZbiorczeMRModule.Services;
using WydanieZbiorczeMRModule.WyborStrefy;

#endregion

namespace WydanieZbiorczeMRModule.ListaMR
{
    public class ListaMRWorkItem : WorkItemBase
    {
        #region Events Publication

        [EventPublication(EventBrokerConstants.RealizacjaZbiorczaOdczytanoStrefe)]
        public event EventHandler ZoneHasBeenRead;

        [EventPublication(EventBrokerConstants.ASYNC_OPERATIONS_STARTED)]
        public event EventHandler AsyncOperationsStarted;

        [EventPublication(EventBrokerConstants.ASYNC_OPERATOIN_FINISHED)]
        public event EventHandler AsyncOperationFinished;

        #endregion

        #region Constants

        private const string BRAK_DOKUMENTU = "Nie uda�o si� odnale�� dokumentu, do kt�rego nale�y pozycja o zaczytanym identyfikatorze.";
        private const string DOKUMENT_ZAKONCZONY = "Nie mo�na pobra� do edycji dokument�w ze statusem [Z]ako�czony.";
        private const string DOKUMENT_ZAREZERWOWANY = "Nie mo�na pobra� do edycji dokument�w ze statusem [R]ezerwacja.";
        private const string DOWOLNA_STREFA = "<DOWOLNA>";
        private const string INFORMACJA = "Informacja";
        private const string PYTANIE = "Pytanie";
        private const string WYSWIETLIC_DOKUMENT_INNEJ_STREFY = "Czy wy�wietli� pozycj� dokumentu nie nale��c� do wybranej wcze�niej strefy?";
        private const string WZOR_OPISU_BLEDU = "{0}\nNumer b��du: {1}";
        private const string ZAKONCZONY = "Z";
        private const string ZAREZERWOWANY = "R";

        #endregion

        #region Private Fields

        private bool m_AktywnaListaMR = true;
        private Thread watekZaczytywaniaAsynchronicznego;

        #endregion

        #region Properties

        public bool AktywnaListaMR
        {
            get { return m_AktywnaListaMR; }
            set { m_AktywnaListaMR = value; }
        }

        #endregion

        #region Protected Methods

        protected void OnZoneHasBeenRead(string kodStrefy)
        {
            RootWorkItem.State[StateConstants.ZONE] = kodStrefy;
            if (ZoneHasBeenRead != null)
            {
                ZoneHasBeenRead(this, EventArgs.Empty);
            }
        }

        #endregion

        [EventSubscription(EventBrokerConstants.RealizacjaZbiorczaWyszukajMr)]
        public void OnSzukajDokumentyMR(object sender, EventArgs e)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                if (!m_AktywnaListaMR)
                {
                    return;
                }

                var listaMRViewPresenter = Items.Get<ListaMRViewPresenter>(ItemsConstants.RealizacjaZbiorczaListaMrPresenter);
                //pobieramy zawsze pierwsza strone

                listaMRViewPresenter.ResetujStronicowanie();
                listaMRViewPresenter.ZaladujDaneDoWidoku(PobierzListeMR(null));
                MainWorkspace.Show(listaMRViewPresenter.View);
                listaMRViewPresenter.View.UstawAktywnaListe();
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public ListaNaglowkow PobierzListeMR(int? numerStrony)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                State[StateConstants.LAST_PAGE] = numerStrony;
                var kryteria = State[StateConstants.CRITERIAS] as KryteriaZapytaniaListyMR;
                kryteria.Strefa = RootWorkItem.State[StateConstants.ZONE] as string;

                var listaNaglowkow = Services.Get<IWydanieMRService>(true).PobierzListeNaglowkowMR(kryteria, numerStrony);
                if (listaNaglowkow.StatusOperacji.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(listaNaglowkow.StatusOperacji.Tekst, BLAD, listaNaglowkow.StatusOperacji.StosWywolan);
                    return null;
                }
                else
                {
                    AnulujOperacjeAsynchroniczne();
                    UruchomZaczytywanieAsynchroniczne();
                }
                return listaNaglowkow;
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public ListaNaglowkow PobierzListeMRWgKontrahenta(string symbolKontrahenta, int? numerStrony)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                State[StateConstants.LAST_PAGE] = numerStrony;
                var kryteria = new KryteriaZapytaniaListyMR
                                   {
                                       Strefa = RootWorkItem.State[StateConstants.ZONE] as string,
                                       SymbolOdbiorcy = symbolKontrahenta,
                                       SortujWgDostepnosci = Constants.YES
                                   };

                var listaNaglowkow = Services.Get<IWydanieMRService>(true).PobierzListeNaglowkowMR(kryteria, numerStrony);
                if (listaNaglowkow.StatusOperacji.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(listaNaglowkow.StatusOperacji.Tekst, BLAD, listaNaglowkow.StatusOperacji.StosWywolan);
                    return null;
                }

                return listaNaglowkow;
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public ListaNaglowkow PobierzListeMRWgMagazynu(long? magaIdFk, int? numerStrony)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                State[StateConstants.LAST_PAGE] = numerStrony;
                var kryteria = new KryteriaZapytaniaListyMR
                {
                    Strefa = RootWorkItem.State[StateConstants.ZONE] as string,
                    MagaIdFk = magaIdFk,
                    SortujWgDostepnosci = Constants.YES
                };

                var listaNaglowkow = Services.Get<IWydanieMRService>(true).PobierzListeNaglowkowMR(kryteria, numerStrony);
                if (listaNaglowkow.StatusOperacji.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(listaNaglowkow.StatusOperacji.Tekst, BLAD, listaNaglowkow.StatusOperacji.StosWywolan);
                    return null;
                }

                return listaNaglowkow;
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void Szukaj()
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                var searchPresenter = Items.Get<ListaMRSearchViewPresenter>(ItemsConstants.RealizacjaZbiorczaListaMrSearchPresenter);
                //czyszczenie zapamietanego stanu
                searchPresenter.View.Kryteria = null;
                Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                MainWorkspace.Show(searchPresenter.View);
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void ZmienStanZarezerwowania(Naglowek naglowek)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                if (naglowek.Status.Equals(ZAKONCZONY))
                {
                    ShowMessageBox(DOKUMENT_ZAKONCZONY, INFORMACJA);
                }
                else
                {
                    Services.Get<IWydanieMRService>(true).ZmienStanZarezerwowania(naglowek.Id);

                    if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                    {
                        ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, BLAD, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                    }
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        private void RezerwujIPobierzDokument(Dokument dokument)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                //proba zarezerwowania dokumentu
                var wynikOperacji = Services.Get<IWydanieMRService>(true).RezerwujDokument(dokument.Naglowek.Id);

                if (wynikOperacji.Status.Equals(StatusOperacji.ERROR))
                {
                    //dokument jest zarezerowowany przez inna osobe lub zakonczony 
                    //- zmiana statusu nastapila po pobraniu listy przez klienta
                    ShowMessageBox(wynikOperacji.Tekst, INFORMACJA); //string.Format(WZOR_OPISU_BLEDU, wynikOperacji.Opis, wynikOperacji.NumerBledu), INFORMACJA);

                    //odswiezenie listy - zmiana statusu dokumentow z listy
                    OdswiezListeDokumentow();
                }
                else
                {
                    //udalo sie zarezerwowac dokument - przejscie do edycji pozycji dokumentu
                    RootWorkItem.WorkItems.Get<EdycjaMRWorkItem>(WorkItemsConstants.RealizacjaZbiorczaListaMrWorkItem).Show(MainWorkspace, dokument);
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void PobierzDokumentyUzytkownikaWgKontrahenta(String symbolKontrahenta)
        {
            RootWorkItem.WorkItems.Get<EdycjaMRWorkItem>(WorkItemsConstants.EdycjaZbiorczaMrWorkItem).Show(MainWorkspace, symbolKontrahenta);
        }

        public void PobierzDokumentyUzytkownikaWgMagazynu(long? magaIdFk)
        {
            RootWorkItem.WorkItems.Get<EdycjaMRWorkItem>(WorkItemsConstants.EdycjaZbiorczaMrWorkItem).Show(MainWorkspace, magaIdFk);
        }


        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ)]
        public void BarCodeRead(object sender, EventArgs e)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var listaMRViewPresenter = Items.Get<ListaMRViewPresenter>(ItemsConstants.RealizacjaZbiorczaListaMrPresenter);

                if (MainWorkspace.ActiveSmartPart.ToString() == listaMRViewPresenter.View.ToString()
                    && (string)RootWorkItem.State[StateConstants.AktywneCzytanieKodu] == StateConstants.BoolValueYes)
                {
                    var wyborStrefyPrezenter = Items.Get<WyborStrefyViewPresenter>(ItemsConstants.RealizacjaZbiorczaWyborStrefyPresenter);

                    var kodKreskowy = RootWorkItem.State[StateConstants.BAR_CODE] as string;
                    if (MainWorkspace.ActiveSmartPart == wyborStrefyPrezenter.View)
                    {

                        if (wyborStrefyPrezenter.ListaSektorow.Contains(kodKreskowy))
                        {
                            OnZoneHasBeenRead(kodKreskowy);
                        }
                    }
                    else
                    {
                        //HACK: wyczyszczenie zaczytanego kodu - nie uruchomi sie obsluga zczytania kodu w EdycjiMRWorkItem
                        RootWorkItem.State[StateConstants.BAR_CODE] = null;
                        try
                        {
                            PobierzDoEdycji(long.Parse(kodKreskowy));
                        }
                        catch (FormatException)
                        {
                            ShowMessageBox(NIEPRAWIDLOWY_FORMAT_ID_POZYCJI, BLAD);
                        }
                    }
                }
            }
            finally
            {
                RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueYes;
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void Show(IWorkspace parentWorkspace)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                var wyborStrefyViewPresenter = Items.Get<WyborStrefyViewPresenter>(ItemsConstants.RealizacjaZbiorczaWyborStrefyPresenter);

                wyborStrefyViewPresenter.ZaladujDaneDoWidoku(PobierzIUzupelnijListeStref());
                parentWorkspace.Show(wyborStrefyViewPresenter.View);
                Activate();
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        private DialogResult ShowQuestionMessageBox(string text, string caption)
        {
            var wynik = MessageBox.Show(text, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
            this.Activate();

            return wynik;
        }

        private ListaSektorow PobierzIUzupelnijListeStref()
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                var wynik = Services.Get<IWydanieMRService>(true).PobierzListeStref();
                if (wynik.StatusOperacji.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(wynik.StatusOperacji.Tekst, BLAD, wynik.StatusOperacji.StosWywolan);
                }

                var pseudoStrefaDowolna = new Sektor();
                pseudoStrefaDowolna.Kod = string.Empty;
                pseudoStrefaDowolna.Nazwa = DOWOLNA_STREFA;

                wynik.Lista.Insert(0, pseudoStrefaDowolna);
                return wynik;
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        private void PobierzDoEdycji(long idPozycji)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                //wyszukanie dokumentu, do ktorego nalezy pozycja o podanym id
                var dokument = Services.Get<IWydanieMRService>(true).PobierzDokumentMR(idPozycji);
                if (dokument == null ||
                    dokument.Pozycje == null)
                {
                    //nie znaleziono dokumentu zawierajacego pozycje o podanym id
                    ShowMessageBox(BRAK_DOKUMENTU, BLAD);
                }
                else if (dokument.Pozycje.Count > 0 &&
                         !NalezyDoWybranejStrefy(dokument.Pozycje[0]))
                {
                    if (ShowQuestionMessageBox(WYSWIETLIC_DOKUMENT_INNEJ_STREFY, PYTANIE) ==
                        DialogResult.Yes)
                    {
                        RezerwujIPobierzDokument(dokument);
                    }
                }
                else
                {
                    RezerwujIPobierzDokument(dokument);
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        private bool NalezyDoWybranejStrefy(Pozycja pozycja)
        {
            var wybranaStrefa = RootWorkItem.State[StateConstants.ZONE] as string;
            if (!wybranaStrefa.Equals(string.Empty) &&
                !pozycja.Sektor.Equals(wybranaStrefa))
            {
                return false;
            }
            return true;
        }

        public void OdswiezListeDokumentow()
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                var ostatnioLadowanaStrona = (int?)State[StateConstants.LAST_PAGE];

                var listaMRViewPresenter = Items.Get<ListaMRViewPresenter>(ItemsConstants.RealizacjaZbiorczaListaMrPresenter);
                listaMRViewPresenter.ZaladujDaneDoWidoku(PobierzListeMR(ostatnioLadowanaStrona));
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public bool UruchomRaport(string nazwaFormularza, string symbolKontrahenta)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                Services.Get<IWydanieMRService>(true).UruchomRaport(nazwaFormularza, symbolKontrahenta, RootWorkItem.State[StateConstants.ZONE] as string);
                if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, BLAD, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                    return false;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }

            return true;
        }

        #region Operacje asynchroniczne

        private void UruchomZaczytywanieAsynchroniczne()
        {
            watekZaczytywaniaAsynchronicznego = new Thread(PobierzPozostaleDaneAsynchronicznie);
            watekZaczytywaniaAsynchronicznego.Start();
        }

        private void PobierzPozostaleDaneAsynchronicznie()
        {
            State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.SUCCESS;
            OnAsyncOperationsStarted();

            var kryteria = State[StateConstants.CRITERIAS] as KryteriaZapytaniaListyMR;
            try
            {
                Services.Get<IWydanieMRService>(true).PobierzAsynchronicznieIlosc(kryteria);
            }
            catch (ThreadAbortException)
            {
                OnAsyncOperationFinished();
            }
            catch (Exception)
            {
                //TODO: log wyjatkow
                State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
            }
        }

        public void AnulujOperacjeAsynchroniczne()
        {
            Services.Get<IWydanieMRService>(true).AnulujOczekujaceOperacje();
            if (watekZaczytywaniaAsynchronicznego != null)
            {
                watekZaczytywaniaAsynchronicznego.Abort();
                watekZaczytywaniaAsynchronicznego = null;
            }
            OnAsyncOperationFinished();
        }

        public void OnAsyncOperationFinished()
        {
            if (AsyncOperationFinished != null)
            {
                AsyncOperationFinished(this, EventArgs.Empty);
            }
        }

        protected void OnAsyncOperationsStarted()
        {
            if (AsyncOperationsStarted != null)
            {
                AsyncOperationsStarted(this, EventArgs.Empty);
            }
        }

        #endregion
    }
}