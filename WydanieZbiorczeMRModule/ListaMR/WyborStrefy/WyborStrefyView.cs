using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using WydanieZbiorczeMRModule.DataModel;

namespace WydanieZbiorczeMRModule.WyborStrefy
{
	[SmartPart]
	public partial class WyborStrefyView : ViewBase, IWyborStrefyView
	{
		#region Constructor

		public WyborStrefyView()
		{
			InitializeComponent();
			InitializeFocusedControl();
		}

		#endregion
		#region IWyborStrefyView Members

		public event EventHandler OK;

		public event EventHandler Anuluj;

		public ListaSektorow DataSource
		{
			set { listaStrefBindingSource.DataSource = value; }
		}

		public string WybranaStrefa
		{
			set { lstStrefy.SelectedValue = value; }
			get { return lstStrefy.SelectedValue.ToString(); }
		}

		#endregion
		#region Protected Methods

		protected void OnOK(object sender, EventArgs e)
		{
			if (OK != null)
			{
				OK(sender, e);
			}
		}

		protected void OnAnuluj(object sender, EventArgs e)
		{
			if (Anuluj != null)
			{
				Anuluj(sender, e);
			}
		}

		#endregion
	}
}
