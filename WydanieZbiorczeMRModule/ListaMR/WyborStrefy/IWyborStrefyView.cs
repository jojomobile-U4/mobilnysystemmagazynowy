using Common.Base;
using System;
using WydanieZbiorczeMRModule.DataModel;

namespace WydanieZbiorczeMRModule.WyborStrefy
{
	public interface IWyborStrefyView: IViewBase
	{
		event EventHandler OK;
		event EventHandler Anuluj;

		ListaSektorow DataSource { set; }

		string WybranaStrefa { set; get; }
	}
}
