using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using WydanieZbiorczeMRModule.DataModel;


namespace WydanieZbiorczeMRModule.ListaMR
{
	[SmartPart]
	public partial class ListaMRView : ViewBase, IListaMRView
	{
		#region Private Fields

		private Naglowek m_Empty = new Naglowek();

		#endregion
        #region Private Delegates

        private delegate void InvokeAsynchUstawWartosc(long? ilosc);

        #endregion
        #region Constructors

        public ListaMRView()
		{
			InitializeComponent();
			InitializeFocusedControl();
		}

		#endregion
		#region Protected Methods

		protected void OnSzukaj(object sender, EventArgs e)
		{
			if (Szukaj != null)
			{
				Szukaj(sender, e);
			}
		}

		protected void OnPobierz(object sender, EventArgs e)
		{
			if (Pobierz != null)
			{
				Pobierz(sender, e);
			}
		}

		protected void OnPoprzedni(object sender, EventArgs e)
		{
			if (Poprzedni != null)
			{
				Poprzedni(sender, e);
			}
		}

		protected void OnNastepny(object sender, EventArgs e)
		{
			if (Nastepny != null)
			{
				Nastepny(sender, e);
			}
		}

		protected void OnZaznaczonyDokumentMRChanged(object sender, EventArgs e)
		{
			if (ZaznaczonyDokumentMRChanged != null)
			{
				ZaznaczonyDokumentMRChanged(sender, e);
			}
		}        public void UstawAktywnaListe()
        {
            SelectNextControl(tbData, false, true, true, true);
        }

		#endregion
		#region IListaMPView Members

		public event EventHandler Szukaj;

		public event EventHandler Pobierz;

		public event EventHandler Nastepny;

		public event EventHandler Poprzedni;

		public event EventHandler ZaznaczonyDokumentMRChanged;

		public Naglowek DataSource
		{
			set
			{
				this.naglowekBindingSource.DataSource = value ?? m_Empty;
			}
		}

		public long? IloscLp
		{
            set
            {
                if (value != null)
                {
                    tbIloscLP.Text = value.ToString();
                }
                else
                {
                    tbIloscLP.Text = "?";
                }
            }
		}

		public bool PoprzedniEnabled
		{
			get { return lrcNavigation.PreviousEnabled; }
			set { lrcNavigation.PreviousEnabled = value; }
		}

		public bool NastepnyEnabled
		{
			get { return lrcNavigation.NextEnabled; }
			set { lrcNavigation.NextEnabled = value; }
		}

		public Common.Components.MSMListView.ListViewItemCollection DokumentyMRDataSource
		{
			get { return lstDokumentyMR.Items; }
		}

		public Naglowek ZaznaczonyDokumentMR
		{
			get
			{
				if (lstDokumentyMR.SelectedIndices.Count == 0)
				{
					return null;
				}

				return lstDokumentyMR.Items[lstDokumentyMR.SelectedIndices[0]].Tag as Naglowek;
			}
		}

        public void AsynchUstawIlosc(long? ilosc)
        {
            if (InvokeRequired)
            {
                this.Invoke(new InvokeAsynchUstawWartosc(AsynchUstawIlosc), ilosc);
                return;
            }
            tbIloscLP.Text = ilosc.ToString();
        }

		#endregion
	}
}
