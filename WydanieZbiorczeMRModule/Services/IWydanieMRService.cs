using System.Collections.Generic;
using WydanieZbiorczeMRModule.ListaMR.Search;
using WydanieZbiorczeMRModule.WydanieMRProxy;
using Dokument = WydanieZbiorczeMRModule.DataModel.Dokument;
using ListaDokumentow = WydanieZbiorczeMRModule.DataModel.ListaDokumentow;
using ListaNaglowkow = WydanieZbiorczeMRModule.DataModel.ListaNaglowkow;
using ListaSektorow = WydanieZbiorczeMRModule.DataModel.ListaSektorow;
using Pozycja = WydanieZbiorczeMRModule.DataModel.Pozycja;
using StatusOperacji = Common.DataModel.StatusOperacji;

namespace WydanieZbiorczeMRModule.Services
{
    public interface IWydanieMRService
    {
        ListaSektorow PobierzListeStref();

        ListaNaglowkow PobierzListeNaglowkowMR(
            KryteriaZapytaniaListyMR kryteria,
            int? numerStrony);

        ListaDokumentow PobierzListeDokumentowMR(
            long? idDokumentuMR, long? pozycjaDokumentu_ID, string lokalizacja, string rodzajDokumentu, string rodzajPozycji, string zakonczona, int? numerStrony);

        StatusOperacji ZakonczPozycje(long id);

        StatusOperacji UsunPozycje(long id);

        StatusOperacji RezerwujDokument(long idDokumentuMR);

        StatusOperacji ZmienStanZarezerwowania(long idDokumentuMR);

        StatusOperacji ZatwierdzDokument(long idDokumentuMR);

        Dokument PobierzDokumentMR(long idPozycji);

        Pozycja PobierzPozycjeMR(long id);

        StatusOperacji PorzucDokument(long idDokumentu);

        void PobierzAsynchronicznieIlosc(KryteriaZapytaniaListyMR kryteria);
        void AnulujOczekujaceOperacje();

        void UruchomRaport(string nazwaFormularza, long idDokumentu, long idPozycji, string symbolStrefy);

        void UruchomRaport(string nazwaFormularza, string symbolKontrahenta, string symbolStrefy);

        StatusOperacji AktaulizujPozycje(Pozycja pozycja);

        StatusOperacji AktaulizujPozycje(Pozycja pozycja, float iloscOrg);

        JednostkaMiary PodstawowaJednostkaMiaryDlaIndeksu(string indeks);

        DataModel.Pozycje PobierzListePozycjiWgUzytkownika(string rodzajDokumentu, string rodzajPozycji, string zakonczona, string symbolKontrahenta, int? numerStrony);

        DataModel.Pozycje PobierzListePozycjiWgMagazynu(string rodzajDokumentu, string rodzajPozycji, string zakonczona, long? magaIdFk, int? numerStrony);

        long IloscNiezakonczonychPozycji(long idDokumentu);

    }
}
