#region

using System;
using System.Windows.Forms;
using Common;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.Commands;
using WydanieZbiorczeMRModule.EdycjaMR;
using WydanieZbiorczeMRModule.EdycjaMR.Search;
using WydanieZbiorczeMRModule.ListaMR;
using WydanieZbiorczeMRModule.ListaMR.ListaMRKontrahenta;
using WydanieZbiorczeMRModule.ListaMR.Search;
using WydanieZbiorczeMRModule.WyborStrefy;

#endregion

namespace WydanieZbiorczeMRModule
{
    public class ModuleInitializer : ModuleInit
    {
        private readonly WorkItem MyWorkItem;

        public ModuleInitializer([ServiceDependency] WorkItem workItem)
        {
            MyWorkItem = workItem;
        }

        public void Initialize()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                InitializeListaMR();
                InitializeEdycjaMR();
                InitializeWagi();
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        private void InitializeEdycjaMR()
        {
            if (!MyWorkItem.WorkItems.Contains(WorkItemsConstants.EdycjaZbiorczaMrWorkItem))
            {
                var edycjaMRWorkItem = MyWorkItem.WorkItems.AddNew<EdycjaMRWorkItem>(WorkItemsConstants.EdycjaZbiorczaMrWorkItem);
                edycjaMRWorkItem.State[StateConstants.CRITERIAS] = string.Empty;

                var edycjaMRSearchViewPresenter = new EdycjaMRSearchViewPresenter(edycjaMRWorkItem.Items.AddNew<EdycjaMRSearchView>());
                edycjaMRWorkItem.Items.Add(edycjaMRSearchViewPresenter, ItemsConstants.EdycjaZbiorczaMrSearchPresenter);

                var edycjaMRViewPresenter = new EdycjaMRViewPresenter(edycjaMRWorkItem.Items.AddNew<EdycjaMRView>());
                edycjaMRWorkItem.Items.Add(edycjaMRViewPresenter, ItemsConstants.EdycjaZborczaMrPresenter);
            }
        }

        private void InitializeListaMR()
        {
            if (!MyWorkItem.WorkItems.Contains(WorkItemsConstants.RealizacjaZbiorczaListaMrWorkItem))
            {
                var listaMRWorkItem = MyWorkItem.WorkItems.AddNew<ListaMRWorkItem>(WorkItemsConstants.RealizacjaZbiorczaListaMrWorkItem);
                listaMRWorkItem.State[StateConstants.CRITERIAS] = new KryteriaZapytaniaListyMR();
                listaMRWorkItem.RootWorkItem.State[StateConstants.ZONE] = string.Empty;

                var searchPresenter = new ListaMRSearchViewPresenter(listaMRWorkItem.Items.AddNew<ListaMRSearchView>());
                listaMRWorkItem.Items.Add(searchPresenter, ItemsConstants.RealizacjaZbiorczaListaMrSearchPresenter);

                var listaMRViewPresenter = new ListaMRViewPresenter(listaMRWorkItem.Items.AddNew<ListaMRView>());
                listaMRWorkItem.Items.Add(listaMRViewPresenter, ItemsConstants.RealizacjaZbiorczaListaMrPresenter);

                var wyborStrefyViewPresenter = new WyborStrefyViewPresenter(listaMRWorkItem.Items.AddNew<WyborStrefyView>());
                listaMRWorkItem.Items.Add(wyborStrefyViewPresenter, ItemsConstants.RealizacjaZbiorczaWyborStrefyPresenter);

                var listaMRKontrahentaPresener = new ListaMRKontrahentaPresenter(listaMRWorkItem.Items.AddNew<ListaMRKontrahentaView>());
                listaMRWorkItem.Items.Add(listaMRKontrahentaPresener, ItemsConstants.RealizacjaZbiorczaMrKontrahentaPresenter);

            }
        }

        private void InitializeWagi()
        {
            if (!MyWorkItem.Services.Contains(typeof(IWagiService)))
            {
                MyWorkItem.Services.AddNew(typeof(WagiService), typeof(IWagiService));
            }
        }

        [CommandHandler(CommandConstants.WydanieMRRealizacjaZbiorcza)]
        public void WydanieMR(object sender, EventArgs e)
        {
            Initialize();

            var mainWorkspace = MyWorkItem.Workspaces[WorkspacesConstants.MAIN_WORKSPACE];
            var listaMRWorkItem = MyWorkItem.WorkItems.Get<ListaMRWorkItem>(WorkItemsConstants.RealizacjaZbiorczaListaMrWorkItem);
            listaMRWorkItem.RootWorkItem.State[StateConstants.PrzejecieSkanIdLinii] = ModuleBooleanConstants.No;
            listaMRWorkItem.RootWorkItem.State[StateConstants.PrzejecieSkanRodzaj] = ModuleSkanRodzajConstants.Lokalizacja;
            listaMRWorkItem.Show(mainWorkspace);
        }
    }
}