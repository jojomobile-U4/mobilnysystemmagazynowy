using System;
using System.Collections.Generic;
using System.Text;

namespace WydanieZbiorczeMRModule.DataModel
{
    public class Naglowek
    {
        #region Private Fields

        private DateTime? m_DataDokumentu;
        private string m_Dostepnosc;
        private long m_Id;
        private long m_IloscNiezakonczonychPozycji;
        private long m_IloscPozycji;
        private string m_NazwaOdbiorcy;
        private int m_Priorytet;
        private string m_Status;
        private string m_SymbolDokumentu;
        private string m_SymbolOdbiorcy;
        private string m_PoleWydania;
        private string m_zatwierdzony;

        #endregion
        #region Properties

        public string SymbolOdbiorcy
        {
            get { return m_SymbolOdbiorcy; }
            set { m_SymbolOdbiorcy = value; }
        }

        public string SymbolDokumentu
        {
            get { return m_SymbolDokumentu; }
            set { m_SymbolDokumentu = value; }
        }

        public string Status
        {
            get { return m_Status; }
            set { m_Status = value; }
        }

        public int Priorytet
        {
            get { return m_Priorytet; }
            set { m_Priorytet = value; }
        }

        public string NazwaOdbiorcy
        {
            get { return m_NazwaOdbiorcy; }
            set { m_NazwaOdbiorcy = value; }
        }

        public long IloscPozycji
        {
            get { return m_IloscPozycji; }
            set { m_IloscPozycji = value; }
        }

        public long IloscNiezakonczonychPozycji
        {
            get { return m_IloscNiezakonczonychPozycji; }
            set { m_IloscNiezakonczonychPozycji = value; }
        }

        public long Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        public string Dostepnosc
        {
            get { return m_Dostepnosc; }
            set { m_Dostepnosc = value; }
        }

        public string PoleWydania
        {
            get { return m_PoleWydania; }
            set { m_PoleWydania = value; }
        }

        public DateTime? DataDokumentu
        {
            get { return m_DataDokumentu; }
            set { m_DataDokumentu = value; }
        }

        public string Zatwierdzony
        {
            get { return m_zatwierdzony; }
            set { m_zatwierdzony = value; }
        }

        public long? MagaIdFk { get; set; }

        #endregion
        #region Constructors

        public Naglowek()
        {
            m_Dostepnosc = string.Empty;
            m_NazwaOdbiorcy = string.Empty;
            m_Status = string.Empty;
            m_SymbolDokumentu = string.Empty;
            m_SymbolOdbiorcy = string.Empty;
            m_PoleWydania = string.Empty;
            m_zatwierdzony = string.Empty;
        }

        public Naglowek(WydanieMRProxy.Naglowek naglowek)
            : this()
        {
            if (naglowek != null)
            {
                m_DataDokumentu = naglowek.dataDokumentu;
                m_Dostepnosc = naglowek.dostepnosc;
                m_Id = naglowek.id;
                m_IloscNiezakonczonychPozycji = naglowek.iloscNiezakonczonychPozycji;
                m_IloscPozycji = naglowek.iloscPozycji;
                m_NazwaOdbiorcy = naglowek.nazwaOdbiorcy;
                m_Priorytet = naglowek.priorytet;
                m_Status = naglowek.status;
                m_SymbolDokumentu = naglowek.symbolDokumentu;
                m_SymbolOdbiorcy = naglowek.symbolOdbiorcy;
                m_PoleWydania = naglowek.poleWydania;
                m_zatwierdzony = naglowek.zatwierdzony;

                if (naglowek.magaIdFk != 0)
                    MagaIdFk = naglowek.magaIdFk;

                if ((SymbolOdbiorcy == null) && (naglowek.magaIdFk != 0))
                {
                    SymbolOdbiorcy = naglowek.magaIdFk.ToString();
                    NazwaOdbiorcy = naglowek.magaFkNazwa;
                }
            }
        }

        #endregion
    }
}
