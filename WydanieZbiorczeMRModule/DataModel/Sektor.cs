using System;
using System.Collections.Generic;
using System.Text;

namespace WydanieZbiorczeMRModule.DataModel
{
	public class Sektor
	{
		#region Private Fields

		private string m_Kod;
		private string m_Nazwa;

		#endregion
		#region Properties

		public string Nazwa
		{
			get { return m_Nazwa; }
			set { m_Nazwa = value; }
		}

		public string Kod
		{
			get { return m_Kod; }
			set { m_Kod = value; }
		}

		#endregion
		#region Constructors

		public Sektor()
		{
			m_Kod = string.Empty;
			m_Nazwa = string.Empty;
		}

		public Sektor(WydanieMRProxy.Sektor sektor)
			: this()
		{
			if (sektor != null)
			{
				m_Kod = sektor.kod;
				m_Nazwa = sektor.nazwa;
			}
		}

		#endregion
	}
}
