using System;
using System.Collections.Generic;
using System.Text;
using Common.DataModel;
using Common;

namespace WydanieZbiorczeMRModule.DataModel
{
	public class ListaNaglowkow
	{
		#region Private Fields

		private List<Naglowek> m_Lista;
		private StatusOperacji m_StatusOperacji;
		private long? m_IloscNaglowkow = null;

		#endregion
		#region Properties

		public StatusOperacji StatusOperacji
		{
			get { return m_StatusOperacji; }
			set { m_StatusOperacji = value; }
		}

		public List<Naglowek> Lista
		{
			get { return m_Lista; }
		}

		public long? IloscNaglowkow
		{
			get { return m_IloscNaglowkow; }
			set { m_IloscNaglowkow = value; }
		}

		#endregion
		#region Constructors

		public ListaNaglowkow()
		{
			m_Lista = new List<Naglowek>();
			m_StatusOperacji = new StatusOperacji(StatusOperacji.SUCCESS);
		}

		public ListaNaglowkow(WydanieMRProxy.ListaNaglowkow listaNaglowkow)
			: this()
		{
			if (listaNaglowkow != null)
			{
				if (listaNaglowkow.lista != null)
				{
					foreach (WydanieMRProxy.Naglowek naglowek in listaNaglowkow.lista)
					{
						m_Lista.Add(new Naglowek(naglowek));
					}
				}
				m_StatusOperacji = new StatusOperacji(listaNaglowkow.statusOperacji.status, listaNaglowkow.statusOperacji.tekst);
			}
			else
			{
				m_StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, DataModelConstants.NO_DATA);
			}
		}

		#endregion
	}
}
