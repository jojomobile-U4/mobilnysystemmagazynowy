﻿using System.Collections.Generic;

namespace WydanieZbiorczeMRModule.DataModel
{
    public class Pozycje
    {
        public List<Pozycja> Lista
        {
            get;
            set;
        }

        public long? LiczbaPozycji
        {
            get;
            set;
        }

        public long? LiczbaPozycjiNiezakonczonych
        {
            get;
            set;
        }

    }
}
