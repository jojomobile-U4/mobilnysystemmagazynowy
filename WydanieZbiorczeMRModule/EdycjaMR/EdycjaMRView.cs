using System;
using System.Windows.Forms;
using Common;
using Common.WagiProxy;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using WydanieZbiorczeMRModule.DataModel;


namespace WydanieZbiorczeMRModule.EdycjaMR
{
	[SmartPart]
	public partial class EdycjaMRView : ViewBase, IEdycjaMRView
	{
		#region Private Fields

		private readonly Pozycja m_Empty = new Pozycja();
        private Masa m_Waga;

		#endregion
		#region Constructor

		public EdycjaMRView()
		{
			InitializeComponent();
			InitializeFocusedControl();
		}

		#endregion
		#region Protected Methods

		private void OnPoprzedni(object sender, EventArgs e)
		{
			if (Poprzedni != null)
			{
				Poprzedni(this, EventArgs.Empty);
			}
		}

		private void OnNastepny(object sender, EventArgs e)
		{
			if ( Nastepny != null)
			{
				Nastepny(this, EventArgs.Empty);
			}
		}

        private void OnUsun(object sender, EventArgs e)
        {
            if (Usun != null)
            {
                Usun(this, EventArgs.Empty);
            }
        }

		private void OnZakoncz(object sender, EventArgs e)
		{
			if ( Zakoncz != null)
			{
				Zakoncz(this, EventArgs.Empty);
			}
		}

		private void OnSzukaj(object sender, EventArgs e)
		{
			if ( Szukaj != null)
			{
				Szukaj(this, EventArgs.Empty);
			}
		}

        
        private void tbWaga_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!FunctionsHelper.IsDigitFloat(e.KeyChar, tbWaga.Text))
            {
                e.Handled = true;
            }
        }

		#endregion
		#region IEdycjaMRView Members

		public event EventHandler Szukaj;

		public event EventHandler Zakoncz;

		public event EventHandler Nastepny;

		public event EventHandler Poprzedni;

        public event EventHandler Usun;

        public event EventHandler Test;


		public Pozycja DataSource
		{
			set { pozycjaBindingSource.DataSource = value ?? m_Empty; }
            get { return pozycjaBindingSource.DataSource as Pozycja; }
		}

		public string SymbolDokumentu
		{
			set { tbDokument.Text = value; }
		}

		public string IloscLp
		{
			set { tbIloscLP.Text = value; }
		}

		public string Pozostalo
		{
			set { tbPozostalo.Text = value; }
		}

		public bool ZakonczEnabled
		{
			set { btnZakoncz.Enabled = value; }
		}

        public bool UsunEnabled
        {
            set { btnUsun.Enabled = value; }
        }

		public string ZakonczTekst
		{
			set { btnZakoncz.Text = value; }
		}

        public string Zakonczona
        {
            set { tbZakonczona.Text = value; }
            get { return tbZakonczona.Text; }
        }

        public bool IloscReadOnly
        {
            set { tbIlosc.ReadOnly = value; }
        }

        public float? Ilosc
        {
            get { return FunctionsHelper.ConvertToFloatNullable(tbIlosc.Text); }
            set { tbIlosc.Text = value.ToString(); }
        }

        public Masa Waga
        {
            set
            {
                m_Waga = value;
                tbWaga.Text = m_Waga != null ? m_Waga.wartosc.ToString("0.000") : 0.ToString("0.000");
            }
        }

	    #endregion
		#region Override Members

		public override void SetNavigationState(bool navigationState)
		{
			base.SetNavigationState(navigationState);

			var potwierdzonaPozycja = false;
			if (pozycjaBindingSource != null &&
				//sprawdzenie czy obiekt pozycji jest juz podpiety
				(pozycjaBindingSource.DataSource as Pozycja) != null)
			{
				//pobranie informacji czy aktualnie wyswietlana pozycja jest juz potwierdzona
				potwierdzonaPozycja = (pozycjaBindingSource.DataSource as Pozycja).PotwierdzonaLokalizacja;
			}

		    //mozliwosc edycji tylko w stanie edycji i gdy pozycja jest juz potwierdzona
			tbIlosc.ReadOnly = navigationState | !potwierdzonaPozycja;
			if (!tbIlosc.ReadOnly)
			{
				//zaznaczenie calego tekstu
				tbIlosc.SelectAll();
			}
			else
			{
				//likwidacja zaznaczenia
				tbIlosc.SelectionLength = 0;
			}
		}

		#endregion
	}
}
