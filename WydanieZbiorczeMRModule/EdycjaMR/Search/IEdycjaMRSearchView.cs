#region

using Common.SearchForm;

#endregion

namespace WydanieZbiorczeMRModule.EdycjaMR.Search
{
    public interface IEdycjaMRSearchView : ISearchForm
    {
        string Kryteria { get; set; }
    }
}