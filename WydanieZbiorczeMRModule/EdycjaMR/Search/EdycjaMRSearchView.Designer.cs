using Common.Components;

namespace WydanieZbiorczeMRModule.EdycjaMR.Search
{
	partial class EdycjaMRSearchView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbLokalizacja = new System.Windows.Forms.TextBox();
            this.lbLokalizacja = new System.Windows.Forms.Label();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // btnOstatnieZapytanie
            // 
            this.btnOstatnieZapytanie.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Location = new System.Drawing.Point(0, 272);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 1;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "WYSZUKIWANIE POZYCJI MR";
            // 
            // tbLokalizacja
            // 
            this.tbLokalizacja.Location = new System.Drawing.Point(73, 20);
            this.tbLokalizacja.Name = "tbLokalizacja";
            this.tbLokalizacja.Size = new System.Drawing.Size(164, 21);
            this.tbLokalizacja.TabIndex = 35;
            // 
            // lbLokalizacja
            // 
            this.lbLokalizacja.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbLokalizacja.Location = new System.Drawing.Point(3, 21);
            this.lbLokalizacja.Name = "lbLokalizacja";
            this.lbLokalizacja.Size = new System.Drawing.Size(71, 21);
            this.lbLokalizacja.Text = "Lokalizacja:";
            // 
            // EdycjaMRSearchView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lbTytul);
            this.Controls.Add(this.tbLokalizacja);
            this.Controls.Add(this.lbLokalizacja);
            this.Name = "EdycjaMRSearchView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.lbLokalizacja, 0);
            this.Controls.SetChildIndex(this.tbLokalizacja, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private MSMLabel lbTytul;
		private System.Windows.Forms.TextBox tbLokalizacja;
		private System.Windows.Forms.Label lbLokalizacja;
	}
}
