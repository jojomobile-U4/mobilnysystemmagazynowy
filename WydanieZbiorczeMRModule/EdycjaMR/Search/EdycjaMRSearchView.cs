#region

using Common.SearchForm;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace WydanieZbiorczeMRModule.EdycjaMR.Search
{
    [SmartPart]
    public partial class EdycjaMRSearchView : SearchForm, IEdycjaMRSearchView
    {
        #region Constructors

        public EdycjaMRSearchView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #endregion

        #region IEdycjaMRSearchView Members

        public string Kryteria
        {
            get { return tbLokalizacja.Text; }
            set { tbLokalizacja.Text = value ?? string.Empty; }
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);

            tbLokalizacja.ReadOnly = navigationState;
        }

        #endregion
    }
}