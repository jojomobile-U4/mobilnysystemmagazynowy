using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using Common.SearchForm;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Common;

namespace WydanieZbiorczeMRModule.EdycjaMR.Search
{
	public class EdycjaMRSearchViewPresenter: SearchFormPresenter
	{
		#region Events

		[EventPublication(EventBrokerConstants.WYSZUKAJ_POZYCJE_DOKUMENTU_MR)]
		public event EventHandler WyszukajPozycjeDokumentuMP;

		#endregion
		#region Properties

		public IEdycjaMRSearchView View
		{
			get { return m_view as IEdycjaMRSearchView; }
		}

		#endregion
		#region Constructors

		public EdycjaMRSearchViewPresenter(IEdycjaMRSearchView view):base(view)
		{

		}

		#endregion
		#region Overrided Memebers

		protected override void OnViewSzukaj(object sender, EventArgs e)
		{
			base.OnViewSzukaj(sender, e);
			OnWyszukajPozycjeDokumentuMR();
		}

		protected override void OnViewUstawOstatnieZapytanie(object sender, EventArgs e)
		{
			base.OnViewUstawOstatnieZapytanie(sender, e);
			View.Kryteria = State[StateConstants.CRITERIAS] as string ?? string.Empty;
		}

		#endregion
		#region Raising Events

		protected void OnWyszukajPozycjeDokumentuMR()
		{
			State[StateConstants.CRITERIAS] = View.Kryteria;
			if (WyszukajPozycjeDokumentuMP != null)
			{
				WyszukajPozycjeDokumentuMP(this, EventArgs.Empty);
			}
		}

		#endregion
	}
}
