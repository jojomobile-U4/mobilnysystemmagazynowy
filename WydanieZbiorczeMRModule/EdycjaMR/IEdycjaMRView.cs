using Common.Base;
using System;
using Common.WagiProxy;
using WydanieZbiorczeMRModule.DataModel;

namespace WydanieZbiorczeMRModule.EdycjaMR
{
	public interface IEdycjaMRView: IViewBase
	{
		event EventHandler Szukaj;
		event EventHandler Zakoncz;
		event EventHandler Nastepny;
		event EventHandler Poprzedni;
        event EventHandler Usun;

        Pozycja DataSource { get; set; }
		string SymbolDokumentu { set; }
		string IloscLp { set; }
		string Pozostalo { set; }
		bool ZakonczEnabled { set; }
        string Zakonczona { set; get; }
        bool UsunEnabled { set; }
		string ZakonczTekst { set; }
        bool IloscReadOnly { set; }
        float? Ilosc { get;  set; }
        Masa Waga { set; }
        
	}
}
