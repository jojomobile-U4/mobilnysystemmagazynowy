#region

using System;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using WydanieZbiorczeMRModule.DataModel;
using WydanieZbiorczeMRModule.EdycjaMR.Search;
using WydanieZbiorczeMRModule.ListaMR;
using WydanieZbiorczeMRModule.Services;

#endregion

namespace WydanieZbiorczeMRModule.EdycjaMR
{
    public class EdycjaMRWorkItem : WorkItemBase
    {
        #region Events

        public event EventHandler<DataEventArgs<long>> ZatwierdzonoDokumentMr;

        #endregion

        #region Constants

        private const string INFO = "Informacja";
        private const string REZERWOWANY = "R";
        private const string ZATWIERDZONO_MR = "Wszystkie pozycje dokumentu zosta�y zako�czone.\nDokument MR zosta� zatwierdzony.";
        private const string MsgBrakPozycjiDlaDokumentu = "Brak pozycji w wybranych dokumentach dla kontrahenta.";
        private const string MsgCaption = "Edycja MR";

        #endregion

        #region Private Fields

        private Dokument m_EdytowanyDokument;
        private bool m_PokazujTylkoPozNiezakonczone;

        private String kontrahent;
        private long? magazyn;

        #endregion

        [EventSubscription(EventBrokerConstants.WYSZUKAJ_POZYCJE_DOKUMENTU_MR)]
        public void OnSzukajPozycjeMr(object sender, EventArgs e)
        {
            var edycjaMrViewPresenter = Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EdycjaZborczaMrPresenter);
            //pobieramy zawsze pierwsza strone

            edycjaMrViewPresenter.ResetujStronicowanie();
            edycjaMrViewPresenter.ZaladujDaneDoWidoku(PobierzDokumentyMr(null));
            MainWorkspace.Show(edycjaMrViewPresenter.View);
        }

        public Pozycje PobierzDokumentyMr(int? numerStrony)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                if (kontrahent != null)
                {
                    return PobierzDokumentyMrWgKontrahenta(kontrahent, numerStrony);
                }
                else
                {
                    return PobierzDokumentyMrWgMagazynu(magazyn, numerStrony);
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }


        public Pozycje PobierzDokumentyMrWgKontrahenta(string symbolKontrahenta, int? numerStrony)
        {
            Pozycje wynik;
            try
            {
                FunctionsHelper.SetCursorWait();

                string zakonczona = null;
                if (m_PokazujTylkoPozNiezakonczone)
                {
                    zakonczona = "N";
                }


                wynik = Services.Get<IWydanieMRService>(true).PobierzListePozycjiWgUzytkownika("R", "PP", zakonczona, symbolKontrahenta, numerStrony);

                if (!RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(StatusOperacji.SUCCESS))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), "Wydanie zbiorcze MR",
                                   RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE].ToString());
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
            return wynik;
        }

        public Pozycje PobierzDokumentyMrWgMagazynu(long? magaIdFk, int? numerStrony)
        {
            Pozycje wynik;
            try
            {
                FunctionsHelper.SetCursorWait();

                string zakonczona = null;
                if (m_PokazujTylkoPozNiezakonczone)
                {
                    zakonczona = "N";
                }


                wynik = Services.Get<IWydanieMRService>(true).PobierzListePozycjiWgMagazynu("R", "PP", zakonczona, magaIdFk, numerStrony);

                if (!RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(StatusOperacji.SUCCESS))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), "Wydanie zbiorcze MR",
                                   RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE].ToString());
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
            return wynik;
        }

        public void Szukaj()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var searchPresenter = Items.Get<EdycjaMRSearchViewPresenter>(ItemsConstants.EdycjaZborczaMrPresenter);
                //czyszczenie zapamietanego stanu
                searchPresenter.View.Kryteria = string.Empty;

                Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                MainWorkspace.Show(searchPresenter.View);
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public bool Zakoncz(Pozycja pozycja)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var wynikOperacji = Services.Get<IWydanieMRService>(true).ZakonczPozycje(pozycja.Id);
                if (wynikOperacji.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(wynikOperacji.Tekst, BLAD, wynikOperacji.StosWywolan);
                    return false;
                }
                else
                {
                    RootWorkItem.State[StateConstants.PrzejecieSkanRodzaj] = ModuleSkanRodzajConstants.IdLinii;
                    return true;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public bool Usun(long idPozycji)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var wynikOperacji = Services.Get<IWydanieMRService>(true).UsunPozycje(idPozycji);
                if (wynikOperacji.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(wynikOperacji.Tekst, BLAD, wynikOperacji.StosWywolan);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public bool Zatwierdz(long idDokumentu)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var wynikOperacji = Services.Get<IWydanieMRService>(true).ZatwierdzDokument(idDokumentu);
                if (wynikOperacji.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(wynikOperacji.Tekst, BLAD, wynikOperacji.StosWywolan);
                    return false;
                }
                else
                {
                    FunctionsHelper.SetCursorDefault();
                    OnZatwierdzonoDokumentMr(idDokumentu);
                    ShowMessageBox(ZATWIERDZONO_MR, INFO);
                    return true;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void OdswiezListeDokumentowMr()
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                if (RootWorkItem.WorkItems.Get<ListaMRWorkItem>(WorkItemsConstants.RealizacjaZbiorczaListaMrWorkItem).AktywnaListaMR)
                {
                    RootWorkItem.WorkItems.Get<ListaMRWorkItem>(WorkItemsConstants.RealizacjaZbiorczaListaMrWorkItem).OdswiezListeDokumentow();
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void PorzucDokument(Dokument dokument)
        {
            try
            {
                if (dokument == null ||
                    !dokument.Naglowek.Status.Equals(REZERWOWANY))
                {
                    return;
                }

                FunctionsHelper.SetCursorWait();
                var status = Services.Get<IWydanieMRService>(true).PorzucDokument(dokument.Naglowek.Id);
                if (status.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(status.Tekst, BLAD, status.StosWywolan);
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        [System.Obsolete("Use : AktaulizujPozycje(Pozycja pozycja)")]
        public bool AktaulizujPozycje(Pozycja pozycja, float? iloscOrg)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                var status = Services.Get<IWydanieMRService>(true).AktaulizujPozycje(pozycja, Common.FunctionsHelper.ConvertToFloat(iloscOrg.ToString()));

                if (status.Status.Equals(StatusOperacji.ERROR))
                {
                    FunctionsHelper.SetCursorDefault();
                    ShowMessageBox(status.Tekst, BLAD, status.StosWywolan);
                    return false;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }

            return true;
        }


        public bool AktaulizujPozycje(Pozycja pozycja)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                var status = Services.Get<IWydanieMRService>(true).AktaulizujPozycje(pozycja);

                if (status.Status.Equals(StatusOperacji.ERROR))
                {
                    FunctionsHelper.SetCursorDefault();
                    ShowMessageBox(status.Tekst, BLAD, status.StosWywolan);
                    return false;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }

            return true;
        }

        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ)]
        public void BarCodeRead(object sender, EventArgs e)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                if ((string)RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] == StateConstants.BoolValueYes)
                {
                    return;
                }

                var presenter = Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EdycjaZborczaMrPresenter);

                if (RootWorkItem.State[StateConstants.BAR_CODE] != null && MainWorkspace.ActiveSmartPart == presenter.View)
                {
                    Activate();
                    var odczyt = RootWorkItem.State[StateConstants.BAR_CODE] as string;

                    if (odczyt == null) return;

                    var lokalizacja = Localization.RemovePrefix(odczyt);
                    var edycjaPrezenter = Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EdycjaZborczaMrPresenter);
                    edycjaPrezenter.PotwierdzLokalizacjePozycji(lokalizacja);

                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        /// <summary>
        /// Wyswietla zaczytany wczesniej dokument wraz z jedna pozycja.
        /// </summary>
        /// <param name="parentWorkspace"></param>
        /// <param name="dokument">Dokument z zaczytana pojedyncza pozycja.</param>
        public void Show(IWorkspace parentWorkspace, Dokument dokument)
        {
            m_PokazujTylkoPozNiezakonczone = true;
            if (dokument == null ||
                dokument.Pozycje.Count == 0)
            {
                return;
            }
            State[StateConstants.DOCUMENT_MR_ID] = null;
            State[StateConstants.POSITION_MR_ID] = dokument.Pozycje[0].Id;

            RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
            RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
            RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
            RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
            RootWorkItem.State[StateConstants.BlokadaWywolaniaFormatkiK] = StateConstants.BoolValueYes;
            Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EdycjaZborczaMrPresenter).IsCloseView = false;


            //zapamietanie, jaki dokument zostal wybrany do edycji, na wypadek gdyby nie mial zadnych pozycji
            //- wtedy web service zwroci pusta liste lub po wyszukaniu zwrocona zostala pusta lista
            m_EdytowanyDokument = UtworzDokumentBezPozycji(dokument.Naglowek);

            PokazEdycjePozycji(parentWorkspace, dokument);

            SetStateValues();
        }

        public void Show(IWorkspace parentWorkspace, string symbolKontrahenta)
        {
            kontrahent = symbolKontrahenta;
            FunctionsHelper.SetCursorWait();

            m_PokazujTylkoPozNiezakonczone = true;
            RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
            RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
            RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
            RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
            RootWorkItem.State[StateConstants.BlokadaWywolaniaFormatkiK] = StateConstants.BoolValueYes;

            var edycjaMrViewPresenter = Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EdycjaZborczaMrPresenter);

            State[StateConstants.DOCUMENT_MR_ID] = null;
            State[StateConstants.POSITION_MR_ID] = null;
            State[StateConstants.CRITERIAS] = null;
            var wynik = PobierzDokumentyMrWgKontrahenta(symbolKontrahenta, 0);

            if (wynik == null)
            {
                MessageBox.Show(MsgBrakPozycjiDlaDokumentu, MsgCaption, MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                return;
            }
            edycjaMrViewPresenter.ZaladujDaneDoWidoku(wynik);

            edycjaMrViewPresenter.ResetujStronicowanie();
            edycjaMrViewPresenter.IsCloseView = false;
            MainWorkspace.Show(edycjaMrViewPresenter.View);
            Activate();

            FunctionsHelper.SetCursorDefault();

            SetStateValues();
        }

        public void Show(IWorkspace parentWorkspace, long? magaIdFk)
        {
            magazyn = magaIdFk;

            FunctionsHelper.SetCursorWait();

            m_PokazujTylkoPozNiezakonczone = true;
            RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
            RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
            RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
            RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
            RootWorkItem.State[StateConstants.BlokadaWywolaniaFormatkiK] = StateConstants.BoolValueYes;

            var edycjaMrViewPresenter = Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EdycjaZborczaMrPresenter);

            State[StateConstants.DOCUMENT_MR_ID] = null;
            State[StateConstants.POSITION_MR_ID] = null;
            State[StateConstants.CRITERIAS] = null;
            var wynik = PobierzDokumentyMrWgMagazynu(magaIdFk, 0);

            edycjaMrViewPresenter.ZaladujDaneDoWidoku(wynik);

            edycjaMrViewPresenter.ResetujStronicowanie();
            edycjaMrViewPresenter.IsCloseView = false;
            MainWorkspace.Show(edycjaMrViewPresenter.View);
            Activate();

            FunctionsHelper.SetCursorDefault();

            SetStateValues();
        }

        public void SetStateValues()
        {
            var edycjaMrViewPresenter = Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EdycjaZborczaMrPresenter);

            if (edycjaMrViewPresenter == null || edycjaMrViewPresenter.View.DataSource == null || edycjaMrViewPresenter.AktualnaPozycja == null) return;

            RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] = edycjaMrViewPresenter.View.ToString();
            RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
            RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = edycjaMrViewPresenter.AktualnaPozycja.SymbolTowaru;
            Activate();
        }

        private void PokazEdycjePozycji(IWorkspace parentWorkspace, Dokument dokument)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var edycjaMrViewPresenter = Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EdycjaZborczaMrPresenter);
                edycjaMrViewPresenter.ResetujStronicowanie();
                edycjaMrViewPresenter.ZaladujDaneDoWidoku(dokument);
                parentWorkspace.Show(edycjaMrViewPresenter.View);
                Activate();
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        protected void OnZatwierdzonoDokumentMr(long idDokumentu)
        {
            if (ZatwierdzonoDokumentMr != null)
            {
                ZatwierdzonoDokumentMr(this, new DataEventArgs<long>(idDokumentu));
            }
        }

        public void WyswietlOstrzezenie(string tekst)
        {
            ShowMessageBox(tekst, INFO);
        }

        public bool UruchomRaport(string nazwaFormularza, long idDokumentu, long idPozycji)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                Services.Get<IWydanieMRService>(true).UruchomRaport(nazwaFormularza, idDokumentu, idPozycji, RootWorkItem.State[StateConstants.ZONE] as string);
                if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, BLAD, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                    return false;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }

            return true;
        }

        private static Dokument UtworzDokumentBezPozycji(Naglowek naglowek)
        {
            var wynik = new Dokument { Naglowek = naglowek };
            if (!wynik.Naglowek.Status.Equals(REZERWOWANY))
            {
                wynik.Naglowek.Status = REZERWOWANY;
            }
            return wynik;
        }
    }
}