namespace KodyKreskoweModule.Search
{
    partial class KodyKreskoweSearchView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbNazwaTowaru = new System.Windows.Forms.TextBox();
            this.lbNazwa = new System.Windows.Forms.Label();
            this.tbSymbol = new System.Windows.Forms.TextBox();
            this.lbSymbol = new System.Windows.Forms.Label();
            this.tbKod = new System.Windows.Forms.TextBox();
            this.lbTytul = new Common.Components.MSMLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnAnuluj.Location = new System.Drawing.Point(182, 4);
            this.btnAnuluj.Size = new System.Drawing.Size(113, 22);
            // 
            // btnOstatnieZapytanie
            // 
            this.btnOstatnieZapytanie.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnOstatnieZapytanie.Location = new System.Drawing.Point(64, 4);
            this.btnOstatnieZapytanie.Size = new System.Drawing.Size(116, 23);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnOK.Location = new System.Drawing.Point(4, 4);
            this.btnOK.Size = new System.Drawing.Size(57, 22);
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Size = new System.Drawing.Size(269, 28);
            // 
            // tbNazwaTowaru
            // 
            this.tbNazwaTowaru.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNazwaTowaru.Location = new System.Drawing.Point(69, 54);
            this.tbNazwaTowaru.Name = "tbNazwaTowaru";
            this.tbNazwaTowaru.Size = new System.Drawing.Size(197, 21);
            this.tbNazwaTowaru.TabIndex = 2;
            // 
            // lbNazwa
            // 
            this.lbNazwa.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbNazwa.Location = new System.Drawing.Point(0, 57);
            this.lbNazwa.Name = "lbNazwa";
            this.lbNazwa.Size = new System.Drawing.Size(75, 21);
            this.lbNazwa.Text = "Nazwa:";
            // 
            // tbSymbol
            // 
            this.tbSymbol.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSymbol.Location = new System.Drawing.Point(69, 25);
            this.tbSymbol.Name = "tbSymbol";
            this.tbSymbol.Size = new System.Drawing.Size(197, 21);
            this.tbSymbol.TabIndex = 1;
            // 
            // lbSymbol
            // 
            this.lbSymbol.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbol.Location = new System.Drawing.Point(0, 28);
            this.lbSymbol.Name = "lbSymbol";
            this.lbSymbol.Size = new System.Drawing.Size(75, 21);
            this.lbSymbol.Text = "Indeks:";
            // 
            // tbKod
            // 
            this.tbKod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbKod.Location = new System.Drawing.Point(69, 83);
            this.tbKod.Name = "tbKod";
            this.tbKod.Size = new System.Drawing.Size(197, 21);
            this.tbKod.TabIndex = 3;
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(269, 20);
            this.lbTytul.TabIndex = 30;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "SZUKAJ INDEKSU";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(0, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 21);
            this.label1.Text = "Kod:";
            // 
            // KodyKreskoweSearchView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.Controls.Add(this.lbTytul);
            this.Controls.Add(this.tbNazwaTowaru);
            this.Controls.Add(this.lbNazwa);
            this.Controls.Add(this.tbSymbol);
            this.Controls.Add(this.lbSymbol);
            this.Controls.Add(this.tbKod);
            this.Controls.Add(this.label1);
            this.Name = "KodyKreskoweSearchView";
            this.Size = new System.Drawing.Size(269, 268);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.tbKod, 0);
            this.Controls.SetChildIndex(this.lbSymbol, 0);
            this.Controls.SetChildIndex(this.tbSymbol, 0);
            this.Controls.SetChildIndex(this.lbNazwa, 0);
            this.Controls.SetChildIndex(this.tbNazwaTowaru, 0);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbNazwaTowaru;
        private System.Windows.Forms.Label lbNazwa;
        private System.Windows.Forms.TextBox tbSymbol;
        private System.Windows.Forms.Label lbSymbol;
        private System.Windows.Forms.TextBox tbKod;
        private Common.Components.MSMLabel lbTytul;
        private System.Windows.Forms.Label label1;

    }
}