#region

using Common.SearchForm;

#endregion

namespace KodyKreskoweModule.Search
{
    public interface IKodyKreskoweSearchView : ISearchForm
    {
        KryteriaZapytaniaKodyKreskowe Kryteria { get; set; }
    }
}