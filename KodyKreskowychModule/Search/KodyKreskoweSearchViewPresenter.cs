using System;
using Common;
using Common.SearchForm;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;

namespace KodyKreskoweModule.Search
{
    public class KodyKreskoweSearchViewPresenter : SearchFormPresenter
    {
        public KodyKreskoweSearchViewPresenter(ISearchForm view)
            : base(view)
        {
        }

        public IKodyKreskoweSearchView View
        {
            get { return m_view as IKodyKreskoweSearchView; }
        }

        [EventPublication(EventBrokerConstants.WyszukajKodyKreskowe)]
        public event EventHandler WyszukajKodyKreskowe;

        protected override void OnViewSzukaj(object sender, EventArgs eventArgs)
        {
            base.OnViewSzukaj(sender, eventArgs);
            OnWyszukajKodyKreskowe(View.Kryteria);
        }

        protected void OnWyszukajKodyKreskowe(KryteriaZapytaniaKodyKreskowe kryteria)
        {
            State[StateConstants.CRITERIAS] = kryteria;
            if (WyszukajKodyKreskowe != null)
            {
                WyszukajKodyKreskowe(this, EventArgs.Empty);
            }
        }

        protected override void OnViewUstawOstatnieZapytanie(object sender, EventArgs eventArgs)
        {
            base.OnViewUstawOstatnieZapytanie(sender, eventArgs);
            View.Kryteria = State[StateConstants.CRITERIAS] as KryteriaZapytaniaKodyKreskowe ?? new KryteriaZapytaniaKodyKreskowe();
        }
    }
}
