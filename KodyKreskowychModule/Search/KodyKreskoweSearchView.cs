using Common.SearchForm;

namespace KodyKreskoweModule.Search
{
    public partial class KodyKreskoweSearchView : SearchForm, IKodyKreskoweSearchView
    {
        public KodyKreskoweSearchView()
        {
            InitializeComponent();
            InitializeFocusedControl();
            SelectNextControl(tbNazwaTowaru, false, true, true, true);
        }

        #region IKodyKreskoweSearchView Members

        public KryteriaZapytaniaKodyKreskowe Kryteria
        {
            get { return new KryteriaZapytaniaKodyKreskowe(tbKod.Text, tbSymbol.Text, tbNazwaTowaru.Text, null); }
            set
            {
                if (value != null)
                {
                    tbKod.Text = value.Kod;
                    tbNazwaTowaru.Text = value.NazwaTowaru;
                    tbSymbol.Text = value.Symbol;
                }
                {
                    tbKod.Text = tbNazwaTowaru.Text = tbSymbol.Text = string.Empty;
                }
            }
        }

        #endregion

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);

            tbKod.ReadOnly = navigationState;
            tbNazwaTowaru.ReadOnly = navigationState;
            tbSymbol.ReadOnly = navigationState;
        }
    }
}