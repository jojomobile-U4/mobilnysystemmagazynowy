
namespace KodyKreskoweModule.Search
{
    public class KryteriaZapytaniaKodyKreskowe
    {
        #region Constructors

		public KryteriaZapytaniaKodyKreskowe()
		{
				
		}

		public KryteriaZapytaniaKodyKreskowe(string kod):this()
		{
            Kod = kod;
            InmaId = null;
		}

        public KryteriaZapytaniaKodyKreskowe(string kod, string symbol)
            : this()
        {
            Kod = kod;
            Symbol = symbol;
            InmaId = null;
        }

        public KryteriaZapytaniaKodyKreskowe(string kod, string symbol, string nazwaTowaru, long? inmaId)
            : this()
        {
            Kod = kod;
            Symbol = symbol;
            NazwaTowaru = nazwaTowaru;
            InmaId = inmaId;
        }

		#endregion

        #region Properties

        public string Kod { get; set; }
        public string Symbol { get; set; }
        public string NazwaTowaru { get; set; }
        public long? InmaId { get; set; }


        #endregion
    }
}
