using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Common;
using Common.Base;
using KodyKreskoweModule.WsKartotekaKodowKreskowych;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;

namespace KodyKreskoweModule
{
    public class KodyKreskoweViewPresenter : PresenterBase
    {
        #region Private Fields

        private Indeks m_AktualnyIndeks;
        private List<Indeks> m_ListaIndeksow;
        private int m_NumerStrony;
        private int m_ZmianaStrony;
        private const int MNastepna = 1;
        private const int MPierwsza = 0;
        private const int MPorzednia = -1;
        private const int KEYS_ESCAPE = 27;


        #endregion

        #region Contructors

        public KodyKreskoweViewPresenter(IViewBase view)
            : base(view)
        {
        }

        #endregion

        #region Properties

        public IKodyKreskoweView View
        {
            get { return m_view as IKodyKreskoweView; }
        }

        protected KodyKreskoweWorkItem MyWorkItem
        {
            get { return WorkItem as KodyKreskoweWorkItem; }
        }

        #endregion

        #region Methods

        public void ZaladujDaneDoWidoku(List<Indeks> listaIndeksow)
        {
            if (listaIndeksow == null ||
                listaIndeksow.Count == 0)
            {
                m_ListaIndeksow = new List<Indeks>();
                m_AktualnyIndeks = null;
            }
            else
            {
                m_ListaIndeksow = listaIndeksow;
                if (m_ZmianaStrony == MPorzednia)
                {
                    m_AktualnyIndeks = m_ListaIndeksow[listaIndeksow.Count - 1];
                }
                else
                {
                    m_AktualnyIndeks = m_ListaIndeksow[0];
                }
            }

            AktualizujWidok(m_AktualnyIndeks);
        }

        #endregion

        #region Obsluga widoku

        private void ViewSzukaj(object sender, EventArgs e)
        {
            MyWorkItem.Szukaj();
        }

        private void ViewSzukajEan(object sender, EventArgs e)
        {
            MyWorkItem.SzukajEan();
        }


        [EventPublication(EventBrokerConstants.WYJSCIE_Z_FORMATKI_K)]
        public event EventHandler WyjscieZFormatkiK;

        #region Raising Events

        protected void OnWyjscieZFormatkiK()
        {
            if (WyjscieZFormatkiK != null)
            {
                WyjscieZFormatkiK(this, EventArgs.Empty);
            }
        }

        #endregion

        public void ViewPowrot(object sender, EventArgs e)
        {
            //var eventKeyArgs = e as KeyEventArgs;

            //if (eventKeyArgs != null && eventKeyArgs.KeyValue == KEYS_ESCAPE)
            //{
            //    MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
            //    MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
            //    MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikDodaniaEanDlaKartotekiKodowKreskowych] = StateConstants.BoolValueNo;
            //    MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueNo;
            //    MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowychSymbolIndeksu] = null;
            //    MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
            //    MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;

            //    View.KodEan = string.Empty;

            //    WorkItemBase.MainWorkspace.Close(View);
            //    return;
            //}

            if ((MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] != null)
                &&
                (MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows].ToString().Equals(StateConstants.BoolValueYes)))
            {
                MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
                View.KodEan = string.Empty;
                
                MyWorkItem.WlaczSubskrypcje();
            }
            else
            {
                MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
                MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
                MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikDodaniaEanDlaKartotekiKodowKreskowych] = StateConstants.BoolValueNo;
                //FunctionsHelper.ZnacznikZgodnosciKodowKreskowychStateHelper = StateConstants.BoolValueNo;
                //FunctionsHelper.ZnacznikZgodnosciKodowKreskowychSymbolIndeksu = null;
                MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueNo;
                MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowychSymbolIndeksu] = null;
                MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
                MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;

                View.KodEan = string.Empty;
            }

            WorkItemBase.MainWorkspace.Close(View);
        }

        private void ViewNastepny(object sender, EventArgs e)
        {
            if (m_AktualnyIndeks != null)
            {
                var indeksAktualnego = m_ListaIndeksow.IndexOf(m_AktualnyIndeks);
                if (indeksAktualnego < m_ListaIndeksow.Count - 1)
                {
                    m_AktualnyIndeks = m_ListaIndeksow[indeksAktualnego + 1];
                    AktualizujWidok(m_AktualnyIndeks);
                }
                else
                {
                    if (m_ListaIndeksow.Count == MyWorkItem.Configuration.KodyKreskoweWS.WielkoscStrony)
                    {
                        ZmienStrone(MNastepna);
                    }
                }
            }
        }

        private void ViewPoprzedni(object sender, EventArgs e)
        {
            if (m_AktualnyIndeks != null)
            {
                var indeksAktualnego = m_ListaIndeksow.IndexOf(m_AktualnyIndeks);
                if (indeksAktualnego > 0)
                {
                    m_AktualnyIndeks = m_ListaIndeksow[indeksAktualnego - 1];
                    AktualizujWidok(m_AktualnyIndeks);
                }
                else
                {
                    if (m_NumerStrony > 0)
                    {
                        ZmienStrone(MPorzednia);
                    }
                }
            }
        }

        private void ViewWybierz(object sender, EventArgs e)
        {
            if (!View.WybierzEnabled) return;
            MyWorkItem.DopiszEan();
            View.WybierzEnabled = false;
            MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikDodaniaEanDlaKartotekiKodowKreskowych] = StateConstants.BoolValueNo;
        }

        private void ZmienStrone(int kierunek)
        {
            m_NumerStrony += kierunek;
            m_ZmianaStrony = kierunek;
            ZaladujDaneDoWidoku(MyWorkItem.PobierzKodyKreskoweIndeksow(m_NumerStrony));
        }

        internal void ResetujStronicowanie()
        {
            m_NumerStrony = MPierwsza;
            m_ZmianaStrony = MNastepna;
        }

        #endregion

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.D1:
                    e.Handled = true;
                    ViewSzukaj(this, EventArgs.Empty);
                    break;
                case Keys.Return:
                    e.Handled = true;
                    ViewWybierz(this, EventArgs.Empty);
                    break;
                case Keys.Left:
                    e.Handled = true;
                    ViewPoprzedni(this, EventArgs.Empty);
                    break;
                case Keys.Right:
                    e.Handled = true;
                    ViewNastepny(this, EventArgs.Empty);
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    ViewPowrot(this, e);
                    OnWyjscieZFormatkiK();
                    break;
            }
        }

        protected override void AttachView()
        {
            View.Szukaj += ViewSzukaj;
            View.SzukajEan += ViewSzukajEan;
            View.Powrot += ViewPowrot;
            View.Wybierz += ViewWybierz;
            View.Poprzedni += ViewPoprzedni;
            View.Nastepny += ViewNastepny;
        }

        [EventSubscription(EventBrokerConstants.NAVIGATION_STATE_CHANGED)]
        public override void NavigationStateChanged(object sender, EventArgs e)
        {
            if (MyWorkItem.MainWorkspace.ActiveSmartPart.ToString() != View.ToString())
            {
                return;
            }

            base.NavigationStateChanged(sender, e);
        }

        #region Private Methods

        private void AktualizujWidok(Indeks indeks)
        {
            View.DataSource = indeks;

            if (MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikDodaniaEanDlaKartotekiKodowKreskowych] != null
                && (MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikDodaniaEanDlaKartotekiKodowKreskowych].ToString().Equals(StateConstants.BoolValueYes)))
            {
                View.WybierzEnabled = indeks != null;
            }
            else
            {
                View.WybierzEnabled = false;
            }
            View.UstawAktywnaListe();

            if (indeks != null)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = indeks.indeks;
                MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueYes;
            }
        }

        #endregion
    }
}
