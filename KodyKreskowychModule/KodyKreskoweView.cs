using System;

using System.Data;
using System.Windows.Forms;
using Common.Base;
using KodyKreskoweModule.WsKartotekaKodowKreskowych;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

namespace KodyKreskoweModule
{
    [SmartPart]
    public partial class KodyKreskoweView : ViewBase, IKodyKreskoweView
    {
        #region Private fields

        private readonly BindingSource m_KodyKreskoweBindingSource = new BindingSource();
        private readonly DataTable m_KodyKreskoweDataTable = new DataTable();
        private readonly DataTable m_IndeksyDataTable = new DataTable();
        private string m_KodEan;

        private const string MKodyKreskoweDataTableColumnNameKod = "Kod";
        private const string MKodyKreskoweDataTableColumnNameLp = "Lp";
        private const string MIndeksyDataTableColumnNameNazwaTowaru = "NazwaTowaru";
        private const string MIndeksyDataTableColumnNameSymbol = "Symbol";
        private const string MIndeksyDataTableColumnNameJednostkaMiary = "JednostkaMiary";

        #endregion 

        #region Constructors

        public KodyKreskoweView()
        {
            InitializeComponent();
            InitializeFocusedControl();
            InitializeBindingSources();
        }

        #endregion

        #region Protected Methods

        protected void OnSzukaj(object sender, EventArgs e)
        {
            if (Szukaj != null)
            {
                Szukaj(sender, e);
            }
        }

        protected void OnPowrot(object sender, EventArgs e)
        {
            if (Powrot != null)
            {
                Powrot(sender, e);
            }
        }

        protected void OnSzukajEanTest(object sender, EventArgs e)
        {
            if (SzukajEan != null)
            {
                SzukajEan(sender, e);
            }
        }

        #endregion

        #region IKodyKreskoweView Members

        public event EventHandler Szukaj;

        public event EventHandler Powrot;

        public event EventHandler Nastepny;

        public event EventHandler Poprzedni;

        public event EventHandler Wybierz;

        public event EventHandler SzukajEan;

        public string KodEan
        {
            get
            {
                return m_KodEan;
            }
            set
            {
                WybierzEnabled = !string.IsNullOrEmpty(value);
                m_KodEan = value;
            }
        }

        public Indeks DataSource
        {
            set
            {
                m_KodyKreskoweBindingSource.DataSource = value ?? new Indeks();
                SetDataSource();
            }
            get { return m_KodyKreskoweBindingSource.DataSource as Indeks; }
        }

        private void OnWybierz(object sender, EventArgs e)
        {
            if (Wybierz != null)
            {
                Wybierz(sender, e);
            }
        }

        protected void OnPoprzedni(object sender, EventArgs e)
        {
            if (Poprzedni != null)
            {
                Poprzedni(sender, e);
            }
        }

        protected void OnNastepny(object sender, EventArgs e)
        {
            if (Nastepny != null)
            {
                Nastepny(sender, e);
            }
        }

        public bool PoprzedniEnabled
        {
            get { return lrcNavigation.PreviousEnabled; }
            set { lrcNavigation.PreviousEnabled = value; }
        }

        public bool NastepnyEnabled
        {
            get { return lrcNavigation.NextEnabled; }
            set { lrcNavigation.NextEnabled = value; }
        }

        public bool WybierzEnabled
        {
            set { btnWybierz.Enabled = value; }
            get { return btnWybierz.Enabled; }
        }

        public bool WybierzVisible
        {
            set { btnWybierz.Visible = value; }
        }

        public void UstawAktywnaListe()
        {
            SelectNextControl(tbJm, true, true, true, true);
        }

        #endregion

        #region Private methods

        private void SetDataSource()
        {
            m_IndeksyDataTable.Clear();
            m_KodyKreskoweDataTable.Clear();
            lstKodyKreskowe.Items.Clear();

            var indeks = m_KodyKreskoweBindingSource.DataSource as Indeks;

            if (indeks == null) return;

            var dataRowIndeks = m_IndeksyDataTable.NewRow();
            dataRowIndeks[MIndeksyDataTableColumnNameSymbol] = indeks.indeks;
            dataRowIndeks[MIndeksyDataTableColumnNameNazwaTowaru] = indeks.nazwa;
            dataRowIndeks[MIndeksyDataTableColumnNameJednostkaMiary] = indeks.jednostkaMiary;

            m_IndeksyDataTable.Rows.Add(dataRowIndeks);

            SetDataSourceList();
        }

        private void SetDataSourceList()
        {
            var indeks = m_KodyKreskoweBindingSource.DataSource as Indeks;

            if (indeks == null || indeks.listaKodowKreskowych == null || indeks.listaKodowKreskowych.Length == 0) return;

            var counter = 0;
            foreach (var kodKreskowy in indeks.listaKodowKreskowych)
            {
                counter++;
                var dataRowKod = m_KodyKreskoweDataTable.NewRow();
                dataRowKod[MKodyKreskoweDataTableColumnNameLp] = counter.ToString();
                dataRowKod[MKodyKreskoweDataTableColumnNameKod] = kodKreskowy.kod;

                m_KodyKreskoweDataTable.Rows.Add(dataRowKod);
            }

            foreach (DataRow kodKreskowyRow in m_KodyKreskoweDataTable.Rows)
            {
                lstKodyKreskowe.Items.Add(
                    new ListViewItem(
                        new []
                            {
                                kodKreskowyRow[MKodyKreskoweDataTableColumnNameLp].ToString(),
                                kodKreskowyRow[MKodyKreskoweDataTableColumnNameKod].ToString()
                            }));
            }
        }

        private void InitializeBindingSources()
        {
            #region Create IndeksyDataTable

            if (m_IndeksyDataTable.Columns.Count == 0)
            {
                var column = new DataColumn { DataType = Type.GetType("System.String"), ColumnName = MIndeksyDataTableColumnNameSymbol, ReadOnly = true, Unique = false };
                m_IndeksyDataTable.Columns.Add(column);

                column = new DataColumn { DataType = Type.GetType("System.String"), ColumnName = MIndeksyDataTableColumnNameNazwaTowaru, ReadOnly = true, Unique = false };
                m_IndeksyDataTable.Columns.Add(column);

                column = new DataColumn { DataType = Type.GetType("System.String"), ColumnName = MIndeksyDataTableColumnNameJednostkaMiary, ReadOnly = true, Unique = false };
                m_IndeksyDataTable.Columns.Add(column);

                tbSymbol.DataBindings.Add(new Binding("Text", m_IndeksyDataTable, MIndeksyDataTableColumnNameSymbol));
                tbNazwaTowaru.DataBindings.Add(new Binding("Text", m_IndeksyDataTable, MIndeksyDataTableColumnNameNazwaTowaru));
                tbJm.DataBindings.Add(new Binding("Text", m_IndeksyDataTable, MIndeksyDataTableColumnNameJednostkaMiary));
            }

            #endregion

            #region Create KodyKreskoweDataTable

            if (m_KodyKreskoweDataTable.Columns.Count != 0) return;

            var columnKod = new DataColumn {DataType = Type.GetType("System.String"), ColumnName = MKodyKreskoweDataTableColumnNameLp, ReadOnly = true, Unique = false};
            m_KodyKreskoweDataTable.Columns.Add(columnKod);

            columnKod = new DataColumn {DataType = Type.GetType("System.String"), ColumnName = MKodyKreskoweDataTableColumnNameKod, ReadOnly = true, Unique = false};
            m_KodyKreskoweDataTable.Columns.Add(columnKod);

            var columnLp = new ColumnHeader { Text = MKodyKreskoweDataTableColumnNameLp, TextAlign = HorizontalAlignment.Left, Width = 50 };
            var columnKodKreskowy = new ColumnHeader { Text = MKodyKreskoweDataTableColumnNameKod, TextAlign = HorizontalAlignment.Left, Width = 180 };

            lstKodyKreskowe.Columns.Add(columnLp);
            lstKodyKreskowe.Columns.Add(columnKodKreskowy);

            #endregion
        }

        #endregion

    }
}