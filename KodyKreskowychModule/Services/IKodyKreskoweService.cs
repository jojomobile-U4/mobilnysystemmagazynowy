using KodyKreskoweModule.Search;
using KodyKreskoweModule.WsKartotekaKodowKreskowych;

namespace KodyKreskoweModule.Services
{
    public interface IKodyKreskoweService
    {
        Indeksy PobierzKodyKreskoweIndeksow(KryteriaZapytaniaKodyKreskowe kryteriaZapytania, long? numerStrony);
        bool DodajKodKreskowy(long inmaId,string kod);
        bool DopiszDoHistoriiOperacji(long? id, string opis);
    }
}
