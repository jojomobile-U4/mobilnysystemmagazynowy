#region

using System;
using System.Windows.Forms;
using Common;
using Common.Base;
using KodyKreskoweModule.ProxyWrappers;
using KodyKreskoweModule.Search;
using KodyKreskoweModule.WsKartotekaKodowKreskowych;
using Microsoft.Practices.Mobile.CompositeUI;

#endregion

namespace KodyKreskoweModule.Services
{
    [Service(typeof (IKodyKreskoweService))]
    public class KodyKreskoweService : ServiceBase, IKodyKreskoweService
    {
        #region Properties

        protected override WebServiceConfiguration Configuration
        {
            get { return MyWorkItem.Configuration.KodyKreskoweWS; }
        }

        #endregion

        #region Konstruktory

        public KodyKreskoweService([ServiceDependency] WorkItem workItem)
            : base(workItem)
        {
        }

        #endregion

        #region PobierzKodyKreskoweIndeksow

        public Indeksy PobierzKodyKreskoweIndeksow(KryteriaZapytaniaKodyKreskowe kryteriaZapytania, long? numerStrony)
        {
            Indeksy listaIndeksow = null;

            try
            {
                using (var serviceAgent = new KodyKreskoweWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);

                    var pobierzKodyKreskoweIndeksowParam = new pobierzKodyKreskoweIndeksow
                                                               {
                                                                   inmaId = kryteriaZapytania.InmaId,
                                                                   kodKreskowy = kryteriaZapytania.Kod,
                                                                   nazwaIndeksu = kryteriaZapytania.NazwaTowaru,
                                                                   numerStrony = numerStrony,
                                                                   symbolIndeksu = kryteriaZapytania.Symbol,
                                                                   wielkoscStrony = Configuration.WielkoscStrony,
                                                               };

                    var lista = serviceAgent.pobierzKodyKreskoweIndeksow(pobierzKodyKreskoweIndeksowParam).@return;

                    MyWorkItem.RootWorkItem.State["StatusOperacji"] = lista.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State["TekstOperacji"] = lista.statusOperacji.tekst;

                    if (lista.statusOperacji.status.Equals("S"))
                    {
                        listaIndeksow = lista;
                    }
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State["StatusOperacji"] = "E";
                MyWorkItem.RootWorkItem.State["TekstOperacji"] = e.ToString();
            }

            return listaIndeksow;
        }


        #endregion


        #region DodajKodKreskowy

        public bool DodajKodKreskowy(long inmaId, string kod)
        {
            try
            {
                using (var serviceAgent = new KodyKreskoweWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);

                    var kodKreskowyParam = new dodajKodKreskowy
                                               {
                                                   inmaId = inmaId,
                                                   kod = kod
                                               };

                    var statusOperacji = serviceAgent.dodajKodKreskowy(kodKreskowyParam).@return;

                    MyWorkItem.RootWorkItem.State["StatusOperacji"] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State["TekstOperacji"] = statusOperacji.tekst;

                    if (statusOperacji.status.Equals("S"))
                    {
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State["StatusOperacji"] = "F";
                MyWorkItem.RootWorkItem.State["TekstOperacji"] = e.ToString();
            }
            return false;
        }

        #endregion

        #region DopiszDoHistoriiOperacji

        public bool DopiszDoHistoriiOperacji(long? id, string opis)
        {
            try
            {
                using (var serviceAgent = new KodyKreskoweWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);

                    var dopiszDoHistoriiOperacjiParam = new dopiszDoHistoriiOperacji
                    {
                        id = id,
                        opis = opis,
                        typZrodla = ModuleSourceConstants.INMA.ToString() 
                    };

                    //MessageBox.Show(" ID = " + dopiszDoHistoriiOperacjiParam.id + " opis = " + dopiszDoHistoriiOperacjiParam.opis + " typZrodla = " + dopiszDoHistoriiOperacjiParam.typZrodla);


                    var statusOperacji = serviceAgent.dopiszDoHistoriiOperacji(dopiszDoHistoriiOperacjiParam).@return;

                    //MessageBox.Show(statusOperacji.ToString());

                    MyWorkItem.RootWorkItem.State["StatusOperacji"] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State["TekstOperacji"] = statusOperacji.tekst;

                    if (statusOperacji.status.Equals("S"))
                    {
                        //MessageBox.Show("statusOperacji.status.Equals(S)");
                        return true;
                    }
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State["StatusOperacji"] = "F";
                MyWorkItem.RootWorkItem.State["TekstOperacji"] = e.ToString();
            }
            //MessageBox.Show("statusOperacji.status.Equals(E)");

            return false;
        }

        #endregion
    }
}