﻿#region

using System;
using Common.Base;
using KodyKreskoweModule.WsKartotekaKodowKreskowych;

#endregion

namespace KodyKreskoweModule
{
    public interface IKodyKreskoweView : IViewBase
    {
        Indeks DataSource { set; get; }

        event EventHandler Szukaj;
        event EventHandler Powrot;
        event EventHandler Nastepny;
        event EventHandler Poprzedni;
        event EventHandler Wybierz;
        event EventHandler SzukajEan;
        

        bool PoprzedniEnabled { get; set; }
        bool NastepnyEnabled { get; set; }
        bool WybierzVisible { set; }
        bool WybierzEnabled { set; get; }

        string KodEan { get; set; }
        void UstawAktywnaListe();
        
    }
}