#region

using System;
using System.Net;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using Common;
using Common.Base;
using KodyKreskoweModule.WsKartotekaKodowKreskowych;
using OpenNETCF.Web.Services2;

#endregion

namespace KodyKreskoweModule.ProxyWrappers
{
    public class KodyKreskoweWrap : WsKartotekaKodowKreskowych.WsKartotekaKodowKreskowych, IWSSecurity
    {
        #region Private Fields

        #endregion

        #region Properties

        public SecurityHeader SecurityHeader { get; set; }

        #endregion

        #region Constructors

        public KodyKreskoweWrap(WebServiceConfiguration configuration)
        {
            Url = configuration.Url;
        }

        #endregion

        #region Protected Methods

        protected override WebRequest GetWebRequest(Uri uri)
        {
            var request = (HttpWebRequest) base.GetWebRequest(uri);

            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;

            return request;
        }

        #endregion

        #region Methods

        #region pobierzKodyKreskoweIndeksow

        [SoapDocumentMethod("urn:pobierzKodyKreskoweIndeksow", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzKodyKreskoweIndeksowResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzKodyKreskoweIndeksowResponse pobierzKodyKreskoweIndeksow([XmlElement("pobierzKodyKreskoweIndeksow", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzKodyKreskoweIndeksow pobierzKodyKreskoweIndeksow1)
        {
            var results = Invoke("pobierzKodyKreskoweIndeksow", new object[]
                                                                    {
                                                                        pobierzKodyKreskoweIndeksow1
                                                                    });
            return ((pobierzKodyKreskoweIndeksowResponse) (results[0]));
        }

        #endregion

        #region dodajKodKreskowy

        [SoapDocumentMethod("urn:dodajKodKreskowy", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("dodajKodKreskowyResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public dodajKodKreskowyResponse dodajKodKreskowy([XmlElement("dodajKodKreskowy", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] dodajKodKreskowy dodajKodKreskowy1)
        {
            var results = Invoke("dodajKodKreskowy", new object[]
                                                         {
                                                             dodajKodKreskowy1
                                                         });
            return ((dodajKodKreskowyResponse) (results[0]));
        }

        #endregion

        #region dopiszDoHistoriiOperacji

        [SoapDocumentMethod("urn:dopiszDoHistoriiOperacji", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("dopiszDoHistoriiOperacjiResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public dopiszDoHistoriiOperacjiResponse dopiszDoHistoriiOperacji([XmlElement("dopiszDoHistoriiOperacji", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] dopiszDoHistoriiOperacji dopiszDoHistoriiOperacji1)
        {
            var results = Invoke("dopiszDoHistoriiOperacji", new object[]{dopiszDoHistoriiOperacji1});
            return ((dopiszDoHistoriiOperacjiResponse) (results[0]));
        }

        #endregion

        #endregion
    }
}