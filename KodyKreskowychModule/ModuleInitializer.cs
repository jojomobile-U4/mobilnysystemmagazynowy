﻿using System;
using Common;
using KodyKreskoweModule.Search;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.Commands;

namespace KodyKreskoweModule
{
    public class ModuleInitializer : ModuleInit
    {
        private readonly WorkItem m_WorkItem;

        public ModuleInitializer([ServiceDependency] WorkItem workItem)
        {
            m_WorkItem = workItem;
        }

        private void Initialize()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                if (!m_WorkItem.WorkItems.Contains(WorkItemsConstants.KodyKreskoweWorkItem))
                {
                    var kodyKreskoweWorkItem = m_WorkItem.WorkItems.AddNew<KodyKreskoweWorkItem>(WorkItemsConstants.KodyKreskoweWorkItem);
                    kodyKreskoweWorkItem.State[StateConstants.CRITERIAS] = new KryteriaZapytaniaKodyKreskowe();

                    var searchPresenter = new KodyKreskoweSearchViewPresenter(kodyKreskoweWorkItem.Items.AddNew<KodyKreskoweSearchView>());
                    kodyKreskoweWorkItem.Items.Add(searchPresenter, ItemsConstants.KodyKreskoweSearchPresenter);

                    var kodyKreskowePresenter = new KodyKreskoweViewPresenter(kodyKreskoweWorkItem.Items.AddNew<KodyKreskoweView>());
                    kodyKreskoweWorkItem.Items.Add(kodyKreskowePresenter, ItemsConstants.KodyKreskowePresenter);
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        [CommandHandler(CommandConstants.KodyKreskowe)]
        public void KartotekaKodowKreskowych(object sender, EventArgs e)
        {
            Initialize();
            var kodyKreskoweWorkItem = m_WorkItem.WorkItems.Get<KodyKreskoweWorkItem>(WorkItemsConstants.KodyKreskoweWorkItem);
            kodyKreskoweWorkItem.Show();
        }
    }
}
