﻿using System;

using System.Collections.Generic;
using System.Windows.Forms;
using Common;
using Common.Base;
using DostawaModule;
using KodyKreskoweModule.Search;
using KodyKreskoweModule.Services;
using KodyKreskoweModule.WsKartotekaKodowKreskowych;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Symbol.Barcode;
using WydanieMRModule.EdycjaMR;
using StatusOperacji = Common.DataModel.StatusOperacji;

namespace KodyKreskoweModule
{
    public class KodyKreskoweWorkItem : WorkItemBase
    {
        private const string MMessageBoxCaption = "Kody kreskowe";
        private const string MMessageBoxBrakBiezacegoIndeksu = "Brak bieżącego indeksu - wskaż właściwy rekord.";
        private const string MMessageBoxDopiszEanNieIstnieje = "Nie istenije w bazie EAN o kodzie : {1}. Czy chcesz dopisać kod ?";
        private const string MMessageBoxDopiszEanEdycjaMrNieIstnieje = "Nie istenije w bazie EAN o kodzie : {1}. Czy wydać towar?";
        private const string MMessageBoxDodajEanBlad = "Wystąpił błąd podczas dodawania kodu EAN do bazy danych";
        private const string MMessageBoxOdczytEabBlad = "Odczyt kodu Ean nieprawidłowy";
        private const string MMessageBoxBladKodPrzypisanyDoIndeksu = "Błąd - Kod EAN {1} jest już przypisany do indeksu o symbolu {2}";
        private const string MMessageBoxBladKodPrzypisanyDoIndeksuMeEdycja = "Błąd - Kod EAN {1} jest już przypisany do indeksu o symbolu {2}.Nie wydawać towaru.";
        private const string MMessageBoxKodPrzypisanyDoIndeksuZgodny = "Kod EAN {1} przypisany do indeksu o symbolu {2} - zgodny";
        private const string MMessageBoxDopisaKod = "Czy dopisać kod EAN {1} do wybranego indeksu ({2}) ?";
        private const string MMessageBoxDodajWpisDoTabeliBledow = "Wystąpił błąd podczas dodawania wpisu do tabeli historia operacji";
        private const string MMessageBoxKodEanRoznyOdEan13 = "Odczytany kod nie jest kodem EAN 13.Czy chcesz kontynuować ?";
        private const string MMessageBoxEanRealizacjaMrNieIstnieje = "Nie istenije w bazie EAN o kodzie : {1}.Czy wydać towar ?";

        #region Event subscription

        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ)]
        public void BarCodeRead(object sender, EventArgs e)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                var kodyKreskowePresenter = Items.Get<KodyKreskoweViewPresenter>(ItemsConstants.KodyKreskowePresenter);
                if (RootWorkItem.State[StateConstants.BAR_CODE] != null
                    && RootWorkItem.State[StateConstants.BAR_CODE_TYPE] != null && MainWorkspace.ActiveSmartPart == kodyKreskowePresenter.View)
                {
                    var odczyt = RootWorkItem.State[StateConstants.BAR_CODE].ToString();

                    if (RootWorkItem.State[StateConstants.BAR_CODE_TYPE].ToString() != DecoderTypes.EAN13.ToString())
                    {
                        if (MessageBox.Show(MMessageBoxKodEanRoznyOdEan13, MMessageBoxCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                        {
                            return;
                        }
                    }

                    if (!string.IsNullOrEmpty(odczyt))
                    {
                        kodyKreskowePresenter.View.KodEan = odczyt;
                        SzukajEan();
                    }
                    else
                    {
                        MessageBox.Show(MMessageBoxOdczytEabBlad, MMessageBoxCaption, MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                    }
                }
            }
            finally
            {
                Activate();
                FunctionsHelper.SetCursorDefault();
            }
        }

        [EventSubscription(EventBrokerConstants.WyszukajKodyKreskowe)]
        public void OnSzukaj(object sender, EventArgs e)
        {
            var kodKreskowyPresenter = Items.Get<KodyKreskoweViewPresenter>(ItemsConstants.KodyKreskowePresenter);
            kodKreskowyPresenter.ResetujStronicowanie();
            kodKreskowyPresenter.ZaladujDaneDoWidoku(PobierzKodyKreskoweIndeksow(0));
            MainWorkspace.Show(kodKreskowyPresenter.View);
        }

        public List<Indeks> PobierzKodyKreskoweIndeksow(long? numerStrony)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var lista = Services.Get<IKodyKreskoweService>(true).PobierzKodyKreskoweIndeksow(State[StateConstants.CRITERIAS] as KryteriaZapytaniaKodyKreskowe, numerStrony);

                if (lista.statusOperacji.status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(lista.statusOperacji.tekst, BLAD, lista.statusOperacji.stosWywolan);
                    return new List<Indeks>();
                }

                var wynik = new List<Indeks>();
                if (lista.lista != null)
                    wynik.AddRange(lista.lista);

                return wynik;
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public bool DodajKodKreskowy(long inmaId, string kod)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var kodKreskowy = Services.Get<IKodyKreskoweService>(true).DodajKodKreskowy(inmaId, kod);

                if (!kodKreskowy)
                {
                    ShowMessageBox(MMessageBoxDodajEanBlad, BLAD);
                }

                return kodKreskowy;
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public bool DopiszDoHistoriiOperacji(long? id, string opis)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var statusOper = Services.Get<IKodyKreskoweService>(true).DopiszDoHistoriiOperacji(id, opis);

                //MessageBox.Show("statusOper = " + statusOper);


                if (!statusOper)
                {
                    //MessageBox.Show("MMessageBoxDodajWpisDoTabeliBledow");

                    ShowMessageBox(MMessageBoxDodajWpisDoTabeliBledow, BLAD);
                }

                return statusOper;
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        #endregion

        #region Show methods

        public void Show()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var indeksViewPresenter = Items.Get<KodyKreskoweViewPresenter>(ItemsConstants.KodyKreskowePresenter);

                if ((RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] != null)
                    && (RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows].ToString().Equals(StateConstants.BoolValueYes))
                    && RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] != null)
                {
                    var kryteriaZapytania = new KryteriaZapytaniaKodyKreskowe { Symbol = RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych].ToString() };
                    State[StateConstants.CRITERIAS] = kryteriaZapytania;
                    OnSzukaj(indeksViewPresenter.View, EventArgs.Empty);
                    WylaczSubskrypcje();
                }
                else
                {
                    indeksViewPresenter.View.DataSource = null;
                    MainWorkspace.Show(indeksViewPresenter.View);
                }

                RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = null;
                RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowychSymbolIndeksu] = null;

                Activate();
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        #endregion

        #region Methods

        public void Szukaj()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var searchPresenter = Items.Get<KodyKreskoweSearchViewPresenter>(ItemsConstants.KodyKreskoweSearchPresenter);
                searchPresenter.View.Kryteria = null;

                Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                MainWorkspace.Show(searchPresenter.View);
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void SzukajEan()
        {
            try
            {
                var indeksViewPresenter = Items.Get<KodyKreskoweViewPresenter>(ItemsConstants.KodyKreskowePresenter);
                var kodEan = indeksViewPresenter.View.KodEan;
                State[StateConstants.CRITERIAS] = new KryteriaZapytaniaKodyKreskowe(kodEan);
                var rezult = PobierzKodyKreskoweIndeksow(0);

                EdycjaMRViewPresenter edycjaMrViewPresenter = null;
                WydanieZbiorczeMRModule.EdycjaMR.EdycjaMRViewPresenter edycjaMrZbiorczaViewPresenter = null;
                KontrolaMPViewPresenter kontrolaMpViewPresenter = null;
                RealizacjaMPViewPresenter realizacjaMpViewPresenter = null;

                var edycjaMrWorkItem = Parent.WorkItems.Get<EdycjaMRWorkItem>(WorkItemsConstants.EDYCJA_MR_WORKITEM);
                if (edycjaMrWorkItem != null)
                    edycjaMrViewPresenter = edycjaMrWorkItem.Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EDYCJA_MR_PRESENTER);

                var edycjaMrZbiorczaWorkItem = Parent.WorkItems.Get<WydanieZbiorczeMRModule.EdycjaMR.EdycjaMRWorkItem>(WorkItemsConstants.EdycjaZbiorczaMrWorkItem);
                if (edycjaMrZbiorczaWorkItem != null)
                    edycjaMrZbiorczaViewPresenter = edycjaMrZbiorczaWorkItem.Items.Get<WydanieZbiorczeMRModule.EdycjaMR.EdycjaMRViewPresenter>(ItemsConstants.EdycjaZborczaMrPresenter);

                var kontrolaMpWorkItem = Parent.WorkItems.Get<KontrolaMPWorkItem>(WorkItemsConstants.KONTROLA_MP_WORKITEM);
                if (kontrolaMpWorkItem != null)
                    kontrolaMpViewPresenter = kontrolaMpWorkItem.Items.Get<KontrolaMPViewPresenter>(ItemsConstants.KONTROLA_MP_PRESENTER);

                var kodyKreskowePresenter = Items.Get<KodyKreskoweViewPresenter>(ItemsConstants.KodyKreskowePresenter);

                if (rezult.Count > 0)
                {
                    var indeks = rezult[0];

                    if (indeks != null)
                    {
                        RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowychSymbolIndeksu] = indeks.indeks;

                        if (indeksViewPresenter.View.DataSource != null && !string.IsNullOrEmpty(indeksViewPresenter.View.DataSource.indeks))
                        {
                            if (indeksViewPresenter.View.DataSource.id != indeks.id)
                            {
                                if (edycjaMrViewPresenter != null && (string)RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] == edycjaMrViewPresenter.View.ToString())
                                {
                                    KodEanNieZgodnyMrEdycja(kodEan, indeks);
                                    indeksViewPresenter.ViewPowrot(null, null);
                                    RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueNo;
                                    edycjaMrViewPresenter.SprawdzZgodnoscKodEan();
                                    return;
                                }
                                if (edycjaMrZbiorczaViewPresenter != null && (string)RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] == edycjaMrZbiorczaViewPresenter.View.ToString())
                                {
                                    KodEanNieZgodnyMrEdycja(kodEan, indeks);
                                    indeksViewPresenter.ViewPowrot(null, null);
                                    RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueNo;
                                    edycjaMrZbiorczaViewPresenter.SprawdzZgodnoscKodEan();
                                    return;
                                }
                                if (kontrolaMpViewPresenter != null && (string)RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] == kontrolaMpViewPresenter.View.ToString())
                                {
                                    KodEanNieZgodny(kodEan, indeks);
                                    RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueNo;
                                    indeksViewPresenter.ViewPowrot(null, null);
                                    kontrolaMpViewPresenter.PrzejdzDoNastepnejPozycji();
                                    return;
                                }
                                else if (RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] != null && RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK].ToString() == indeksViewPresenter.View.ToString())
                                {
                                    KodEanNieZgodny(kodEan, indeks);
                                    return;
                                }
                                else
                                {
                                    KodEanNieZgodny(kodEan, indeks);
                                    indeksViewPresenter.ViewPowrot(null, null);
                                    return;
                                }
                            }
                            else
                            {
                                if ((edycjaMrViewPresenter != null && (string)RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] == edycjaMrViewPresenter.View.ToString())
                                    || (edycjaMrZbiorczaViewPresenter != null && (string)RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] == edycjaMrZbiorczaViewPresenter.View.ToString())
                                    || (kontrolaMpViewPresenter != null && (string)RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] == kontrolaMpViewPresenter.View.ToString()))
                                {
                                    KodEanZgodnyMrEdycja(kodEan, indeks.indeks);
                                    indeksViewPresenter.ViewPowrot(null, null);
                                    return;
                                }
                                else if (RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] != null && RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK].ToString() == indeksViewPresenter.View.ToString())
                                {
                                    KodEanZgodny(kodEan, indeks.indeks);
                                    return;
                                }
                                else
                                {
                                    KodEanZgodny(kodEan, indeks.indeks);
                                    indeksViewPresenter.ViewPowrot(null, null);
                                    return;
                                }
                            }
                        }

                        indeksViewPresenter.ZaladujDaneDoWidoku(new List<Indeks> { indeks });

                        if (RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] != null && RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK].ToString() != indeksViewPresenter.View.ToString())                        
                        {
                            KodEanZgodny(kodEan, indeks.indeks);
                            indeksViewPresenter.ViewPowrot(null, null);
                            return ;                            
                        }
                        else if (RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] != null
                            && RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows].ToString().Equals(StateConstants.BoolValueYes))
                        {
                            KodEanZgodny(kodEan, indeks.indeks);
                        }
                    }
                }
                else
                {
                    if (indeksViewPresenter.View.DataSource == null || string.IsNullOrEmpty(indeksViewPresenter.View.DataSource.nazwa))
                    {
                        if (edycjaMrViewPresenter != null && (string) RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] == edycjaMrViewPresenter.View.ToString())
                        {
                            if (MessageBox.Show(MMessageBoxDopiszEanEdycjaMrNieIstnieje.Replace("{1}", kodEan), MMessageBoxCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                            {
                                RootWorkItem.State[StateConstants.ZnacznikDodaniaEanDlaKartotekiKodowKreskowych] = StateConstants.BoolValueYes;
                                RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowychSymbolIndeksu] = null;
                                indeksViewPresenter.ViewPowrot(null, null);
                                RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueNo;
                            }
                            else
                            {
                                RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueNo;
                                RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueNo;
                                indeksViewPresenter.ViewPowrot(null, null);
                                edycjaMrViewPresenter.SprawdzZgodnoscKodEan();
                            }
                        }
                        else if (edycjaMrZbiorczaViewPresenter != null && (string) RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] == edycjaMrZbiorczaViewPresenter.View.ToString())
                        {
                            if (MessageBox.Show(MMessageBoxDopiszEanEdycjaMrNieIstnieje.Replace("{1}", kodEan), MMessageBoxCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                            {
                                RootWorkItem.State[StateConstants.ZnacznikDodaniaEanDlaKartotekiKodowKreskowych] = StateConstants.BoolValueYes;
                                RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowychSymbolIndeksu] = null;
                                indeksViewPresenter.ViewPowrot(null, null);
                                RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueNo;
                            }
                            else
                            {
                                RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueNo;
                                RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueNo;
                                indeksViewPresenter.ViewPowrot(null, null);
                                edycjaMrZbiorczaViewPresenter.SprawdzZgodnoscKodEan();
                            }
                        }
                        else
                        {
                            if (MessageBox.Show(MMessageBoxDopiszEanNieIstnieje.Replace("{1}", kodEan), MMessageBoxCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                            {
                                RootWorkItem.State[StateConstants.ZnacznikDodaniaEanDlaKartotekiKodowKreskowych] = StateConstants.BoolValueYes;
                                RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowychSymbolIndeksu] = null;

                                Szukaj();
                            }
                        }
                    }
                    else
                    {
                        if ((string) RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] != indeksViewPresenter.View.ToString()
                            && (string) RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] == StateConstants.BoolValueYes)
                        {
                            if (edycjaMrViewPresenter != null && (string) RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] == edycjaMrViewPresenter.View.ToString())
                            {
                                if (MessageBox.Show(MMessageBoxDopiszEanEdycjaMrNieIstnieje.Replace("{1}", kodEan), MMessageBoxCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                {
                                    RootWorkItem.State[StateConstants.ZnacznikDodaniaEanDlaKartotekiKodowKreskowych] = StateConstants.BoolValueYes;
                                    RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowychSymbolIndeksu] = null;
                                    indeksViewPresenter.ViewPowrot(null, null);
                                    RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueNo;
                                }
                                else
                                {
                                    RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueNo;
                                    RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueNo;
                                    indeksViewPresenter.ViewPowrot(null, null);
                                    edycjaMrViewPresenter.SprawdzZgodnoscKodEan2();
                                }
                            }
                            else
                            {
                                DopiszEan();
                                indeksViewPresenter.ViewPowrot(null, null);
                            }
                        }
                        else
                        {
                            DopiszEan();
                        }
                    }
                }
            }
            finally
            {
                RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueNo;
                FunctionsHelper.SetCursorDefault();
            }
        }


        private void KodEanNieZgodny(string kodEan, Indeks indeks)
        {
            MessageBox.Show(MMessageBoxBladKodPrzypisanyDoIndeksu.Replace("{1}", kodEan).Replace("{2}", indeks.indeks), MMessageBoxCaption, MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueNo;
            DopiszDoHistoriiOperacji(indeks.id, "Niezgodny kod EAN : " + kodEan + " dla indeksu : " + indeks.indeks);
        }

        private void KodEanNieZgodnyMrEdycja(string kodEan, Indeks indeks)
        {
            MessageBox.Show(MMessageBoxBladKodPrzypisanyDoIndeksuMeEdycja.Replace("{1}", kodEan).Replace("{2}", indeks.indeks), MMessageBoxCaption, MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueNo;
            DopiszDoHistoriiOperacji(indeks.id, "Niezgodny kod EAN : " + kodEan + " dla indeksu : " + indeks.indeks);
        }

        private void KodEanZgodny(string kodEan, string indeks)
        {
            MessageBox.Show(MMessageBoxKodPrzypisanyDoIndeksuZgodny.Replace("{1}", kodEan).Replace("{2}", indeks), MMessageBoxCaption, MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueYes;
        }

        private void KodEanZgodnyMrEdycja(string kodEan, string indeks)
        {
            MessageBox.Show(MMessageBoxKodPrzypisanyDoIndeksuZgodny.Replace("{1}", kodEan).Replace("{2}", indeks), MMessageBoxCaption, MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueYes;
        }

        private void KodEanZgodnyMrRealizacja(string kodEan, string indeks)
        {
            MessageBox.Show(MMessageBoxKodPrzypisanyDoIndeksuZgodny.Replace("{1}", kodEan).Replace("{2}", indeks), MMessageBoxCaption, MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueYes;
        }
        
        public void DopiszEan()
        {
            var indeksViewPresenter = Items.Get<KodyKreskoweViewPresenter>(ItemsConstants.KodyKreskowePresenter);

            var aktualnyIndeks = indeksViewPresenter.View.DataSource;

            if (aktualnyIndeks == null)
            {
                MessageBox.Show(MMessageBoxBrakBiezacegoIndeksu, MMessageBoxCaption, MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                return;
            }

            if (MessageBox.Show(MMessageBoxDopisaKod.Replace("{1}", indeksViewPresenter.View.KodEan).Replace("{2}", aktualnyIndeks.indeks), MMessageBoxCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                DodajKodKreskowy(aktualnyIndeks.id, indeksViewPresenter.View.KodEan);

                var kryteriaZapytania = new KryteriaZapytaniaKodyKreskowe
                                            {
                                                InmaId = aktualnyIndeks.id,
                                                Kod = string.Empty,
                                                NazwaTowaru = string.Empty,
                                                Symbol = string.Empty
                                            };

                State[StateConstants.CRITERIAS] = kryteriaZapytania;

                indeksViewPresenter.ZaladujDaneDoWidoku(PobierzKodyKreskoweIndeksow(0));
                RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowychSymbolIndeksu] = aktualnyIndeks.indeks;
                RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueYes;
            }
            else
            {
                RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueNo;
                RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowychSymbolIndeksu] = null;
            }
        }

        public void WylaczSubskrypcje()
        {
            if (RootWorkItem.Items[WorkItemsConstants.EdycjaZbiorczaMrWorkItem] != null)
                RootWorkItem.EventTopics[EventBrokerConstants.BARCODE_HAS_BEEN_READ].RemoveSubscription(RootWorkItem.Items[WorkItemsConstants.EdycjaZbiorczaMrWorkItem], "BarCodeRead");
            if (RootWorkItem.Items[WorkItemsConstants.EDYCJA_MR_WORKITEM] != null)
                RootWorkItem.EventTopics[EventBrokerConstants.BARCODE_HAS_BEEN_READ].RemoveSubscription(RootWorkItem.Items[WorkItemsConstants.EDYCJA_MR_WORKITEM], "BarCodeRead");
        }

        public void WlaczSubskrypcje()
        {
            if (RootWorkItem.Items[WorkItemsConstants.EdycjaZbiorczaMrWorkItem] != null)
                EventTopics[EventBrokerConstants.BARCODE_HAS_BEEN_READ].AddSubscription(RootWorkItem.Items[WorkItemsConstants.EdycjaZbiorczaMrWorkItem],
                    "BarCodeRead",
                    RootWorkItem.Items[WorkItemsConstants.EdycjaZbiorczaMrWorkItem] as WorkItem,
                    ThreadOption.UserInterface);
            if (RootWorkItem.Items[WorkItemsConstants.EDYCJA_MR_WORKITEM] != null)
                EventTopics[EventBrokerConstants.BARCODE_HAS_BEEN_READ].AddSubscription(RootWorkItem.Items[WorkItemsConstants.EDYCJA_MR_WORKITEM],
                    "BarCodeRead",
                    RootWorkItem.Items[WorkItemsConstants.EDYCJA_MR_WORKITEM] as WorkItem,
                    ThreadOption.UserInterface);
        }

        #endregion
    }
}
