namespace KodyKreskoweModule
{
    partial class KodyKreskoweView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTytul = new Common.Components.MSMLabel();
            this.lstKodyKreskowe = new Common.Components.MSMListView();
            this.tbNazwaTowaru = new System.Windows.Forms.TextBox();
            this.lbNazwa = new System.Windows.Forms.Label();
            this.tbSymbol = new System.Windows.Forms.TextBox();
            this.lbSymbol = new System.Windows.Forms.Label();
            this.btnWybierz = new System.Windows.Forms.Button();
            this.btnSzukaj = new System.Windows.Forms.Button();
            this.lrcNavigation = new Common.Components.LeftRightControl();
            this.tbJm = new System.Windows.Forms.TextBox();
            this.lbJm = new System.Windows.Forms.Label();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnWybierz);
            this.pnlNavigation.Controls.Add(this.btnSzukaj);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 273);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 27);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 20);
            this.lbTytul.TabIndex = 18;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "KODY EAN";
            // 
            // lstKodyKreskowe
            // 
            this.lstKodyKreskowe.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstKodyKreskowe.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lstKodyKreskowe.FullRowSelect = true;
            this.lstKodyKreskowe.Location = new System.Drawing.Point(0, 111);
            this.lstKodyKreskowe.Name = "lstKodyKreskowe";
            this.lstKodyKreskowe.Size = new System.Drawing.Size(237, 160);
            this.lstKodyKreskowe.TabIndex = 4;
            this.lstKodyKreskowe.View = System.Windows.Forms.View.Details;
            // 
            // tbNazwaTowaru
            // 
            this.tbNazwaTowaru.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbNazwaTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.tbNazwaTowaru.Location = new System.Drawing.Point(69, 53);
            this.tbNazwaTowaru.Name = "tbNazwaTowaru";
            this.tbNazwaTowaru.ReadOnly = true;
            this.tbNazwaTowaru.Size = new System.Drawing.Size(168, 19);
            this.tbNazwaTowaru.TabIndex = 2;
            this.tbNazwaTowaru.TabStop = false;
            // 
            // lbNazwa
            // 
            this.lbNazwa.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbNazwa.Location = new System.Drawing.Point(0, 56);
            this.lbNazwa.Name = "lbNazwa";
            this.lbNazwa.Size = new System.Drawing.Size(75, 21);
            this.lbNazwa.Text = "Nazwa:";
            // 
            // tbSymbol
            // 
            this.tbSymbol.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSymbol.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.tbSymbol.Location = new System.Drawing.Point(69, 25);
            this.tbSymbol.Name = "tbSymbol";
            this.tbSymbol.ReadOnly = true;
            this.tbSymbol.Size = new System.Drawing.Size(168, 19);
            this.tbSymbol.TabIndex = 1;
            this.tbSymbol.TabStop = false;
            // 
            // lbSymbol
            // 
            this.lbSymbol.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbol.Location = new System.Drawing.Point(0, 28);
            this.lbSymbol.Name = "lbSymbol";
            this.lbSymbol.Size = new System.Drawing.Size(75, 21);
            this.lbSymbol.Text = "Indeks:";
            // 
            // btnWybierz
            // 
            this.btnWybierz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnWybierz.Enabled = false;
            this.btnWybierz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnWybierz.Location = new System.Drawing.Point(87, 4);
            this.btnWybierz.Name = "btnWybierz";
            this.btnWybierz.Size = new System.Drawing.Size(109, 22);
            this.btnWybierz.TabIndex = 5;
            this.btnWybierz.Text = "Ret Wybierz";
            this.btnWybierz.Click += new System.EventHandler(this.OnWybierz);
            // 
            // btnSzukaj
            // 
            this.btnSzukaj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSzukaj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnSzukaj.Location = new System.Drawing.Point(4, 4);
            this.btnSzukaj.Name = "btnSzukaj";
            this.btnSzukaj.Size = new System.Drawing.Size(80, 22);
            this.btnSzukaj.TabIndex = 6;
            this.btnSzukaj.Text = "&1 Szukaj";
            this.btnSzukaj.Click += new System.EventHandler(this.OnSzukaj);
            // 
            // lrcNavigation
            // 
            this.lrcNavigation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lrcNavigation.BackColor = System.Drawing.SystemColors.Desktop;
            this.lrcNavigation.Location = new System.Drawing.Point(208, 0);
            this.lrcNavigation.Name = "lrcNavigation";
            this.lrcNavigation.NextEnabled = true;
            this.lrcNavigation.PreviousEnabled = true;
            this.lrcNavigation.Size = new System.Drawing.Size(32, 16);
            this.lrcNavigation.TabIndex = 26;
            this.lrcNavigation.TabStop = false;
            this.lrcNavigation.Next += new System.EventHandler(this.OnNastepny);
            this.lrcNavigation.Previous += new System.EventHandler(this.OnPoprzedni);
            // 
            // tbJm
            // 
            this.tbJm.Location = new System.Drawing.Point(69, 80);
            this.tbJm.Name = "tbJm";
            this.tbJm.ReadOnly = true;
            this.tbJm.Size = new System.Drawing.Size(48, 21);
            this.tbJm.TabIndex = 3;
            this.tbJm.TabStop = false;
            // 
            // lbJm
            // 
            this.lbJm.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbJm.Location = new System.Drawing.Point(0, 84);
            this.lbJm.Name = "lbJm";
            this.lbJm.Size = new System.Drawing.Size(75, 21);
            this.lbJm.Text = "J.m.:";
            // 
            // KodyKreskoweView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.Controls.Add(this.tbJm);
            this.Controls.Add(this.lbJm);
            this.Controls.Add(this.lrcNavigation);
            this.Controls.Add(this.tbNazwaTowaru);
            this.Controls.Add(this.lbNazwa);
            this.Controls.Add(this.tbSymbol);
            this.Controls.Add(this.lbSymbol);
            this.Controls.Add(this.lstKodyKreskowe);
            this.Controls.Add(this.lbTytul);
            this.Name = "KodyKreskoweView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lstKodyKreskowe, 0);
            this.Controls.SetChildIndex(this.lbSymbol, 0);
            this.Controls.SetChildIndex(this.tbSymbol, 0);
            this.Controls.SetChildIndex(this.lbNazwa, 0);
            this.Controls.SetChildIndex(this.tbNazwaTowaru, 0);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lrcNavigation, 0);
            this.Controls.SetChildIndex(this.lbJm, 0);
            this.Controls.SetChildIndex(this.tbJm, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Common.Components.MSMLabel lbTytul;
        private Common.Components.MSMListView lstKodyKreskowe;
        private System.Windows.Forms.TextBox tbNazwaTowaru;
        private System.Windows.Forms.Label lbNazwa;
        private System.Windows.Forms.TextBox tbSymbol;
        private System.Windows.Forms.Label lbSymbol;
        private System.Windows.Forms.Button btnWybierz;
        private System.Windows.Forms.Button btnSzukaj;
        public Common.Components.LeftRightControl lrcNavigation;
        private System.Windows.Forms.TextBox tbJm;
        private System.Windows.Forms.Label lbJm;

    }
}
