using System;
using System.Collections.Generic;
using System.Text;

namespace DostawaModule
{
    public class KryteriaZapytaniaListyMP
    {
        private long? id;
        private long? idPozycji;
        private string symbol = "";
        private string symbolDostawcy = "";
        private string nazwaDostawcy = "";
        private string status = "";
        private DateTime? dataDokumentu;
        private string symbolDokumentuObrotowego = "";
        private string skontrolowany = "";
        private long? idMagazynu;
        private int? numerStrony;
		//private int? wielkoscStrony;

        public long? Id
        {
            get
            {
                return id;
            }
            set 
            {
                id = value;
            }
        }

        public long? IdPozycji
        {
            get
            {
                return idPozycji;
            }
            set 
            {
                idPozycji = value;
            }
        }

        public string Symbol 
        {
            get 
            {
                return symbol;
            }
            set
            {
                symbol = value;
            }
        }

        public string SymbolDostawcy
        {
            get
            {
                return symbolDostawcy;
            }
            set
            {
                symbolDostawcy = value;
            }         
        }

        public string NazwaDostawcy
        {
            get
            {
                return nazwaDostawcy;
            }
            set
            {
                nazwaDostawcy = value;
            }
        }

        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public DateTime? DataDokumentu
        {
            get 
            {
                return dataDokumentu;
            }
            set 
            {
                dataDokumentu = value;
            }
        }

        public string SymbolDokumentuObrotowego
        {
            get
            {
                return symbolDokumentuObrotowego;
            }
            set
            {
                symbolDokumentuObrotowego = value;
            }
        }

        public string Skontrolowany
        {
            get
            {
                return skontrolowany;
            }
            set
            {
                skontrolowany = value;
            }
        }

        public long? IdMagazynu
        {
            get
            {
                return idMagazynu;
            }
            set
            {
                idMagazynu = value;
            }
        }

        public int? NumerStrony
        {
            get
            {
                return numerStrony;
            }
            set
            {
                numerStrony = value;
            }
        }

		//public int? WielkoscStrony
		//{
		//    get
		//    {
		//        return wielkoscStrony;
		//    }
		//    set
		//    {
		//        wielkoscStrony = value;
		//    }
		//}
    }
}
