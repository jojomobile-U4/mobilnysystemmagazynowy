
using Common.Components;

namespace DostawaModule
{
    partial class ListaMPView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSzukaj = new System.Windows.Forms.Button();
            this.btnKontroluj = new System.Windows.Forms.Button();
            this.btnRealizuj = new System.Windows.Forms.Button();
            this.tbDostNazwa = new System.Windows.Forms.TextBox();
            this.lbDostNazwa = new System.Windows.Forms.Label();
            this.tbDostSymbol = new System.Windows.Forms.TextBox();
            this.lbDostSymbol = new System.Windows.Forms.Label();
            this.tbSkontrolowany = new System.Windows.Forms.TextBox();
            this.lbSkontrolowany = new System.Windows.Forms.Label();
            this.tbDokObrotowy = new System.Windows.Forms.TextBox();
            this.lbDokObrotowy = new System.Windows.Forms.Label();
            this.tbIloscLP = new System.Windows.Forms.TextBox();
            this.lbIloscLP = new System.Windows.Forms.Label();
            this.lbTytul = new Common.Components.MSMLabel();
            this.dataTable = new System.Data.DataTable();
            this.lstListaMP = new Common.Components.MSMListView();
            this.chId = new System.Windows.Forms.ColumnHeader();
            this.chLp = new System.Windows.Forms.ColumnHeader();
            this.chSymbolDokumentu = new System.Windows.Forms.ColumnHeader();
            this.chStatus = new System.Windows.Forms.ColumnHeader();
            this.chData = new System.Windows.Forms.ColumnHeader();
            this.btnPowrot = new System.Windows.Forms.Button();
            this.lrcNavigation = new Common.Components.LeftRightControl();
            this.pnlNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnPowrot);
            this.pnlNavigation.Controls.Add(this.btnSzukaj);
            this.pnlNavigation.Controls.Add(this.btnKontroluj);
            this.pnlNavigation.Controls.Add(this.btnRealizuj);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 52);
            // 
            // btnSzukaj
            // 
            this.btnSzukaj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnSzukaj.Location = new System.Drawing.Point(3, 7);
            this.btnSzukaj.Name = "btnSzukaj";
            this.btnSzukaj.Size = new System.Drawing.Size(75, 19);
            this.btnSzukaj.TabIndex = 6;
            this.btnSzukaj.TabStop = false;
            this.btnSzukaj.Text = "&1 Szukaj";
            this.btnSzukaj.Click += new System.EventHandler(this.btnSzukaj_Click);
            // 
            // btnKontroluj
            // 
            this.btnKontroluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnKontroluj.Location = new System.Drawing.Point(3, 29);
            this.btnKontroluj.Name = "btnKontroluj";
            this.btnKontroluj.Size = new System.Drawing.Size(75, 20);
            this.btnKontroluj.TabIndex = 7;
            this.btnKontroluj.TabStop = false;
            this.btnKontroluj.Text = "&2 Kontroluj";
            this.btnKontroluj.Click += new System.EventHandler(this.btnKontroluj_Click);
            // 
            // btnRealizuj
            // 
            this.btnRealizuj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnRealizuj.Location = new System.Drawing.Point(82, 29);
            this.btnRealizuj.Name = "btnRealizuj";
            this.btnRealizuj.Size = new System.Drawing.Size(75, 20);
            this.btnRealizuj.TabIndex = 8;
            this.btnRealizuj.TabStop = false;
            this.btnRealizuj.Text = "&3 Realizuj";
            this.btnRealizuj.Click += new System.EventHandler(this.btnRealizuj_Click);
            // 
            // tbDostNazwa
            // 
            this.tbDostNazwa.Location = new System.Drawing.Point(92, 217);
            this.tbDostNazwa.Name = "tbDostNazwa";
            this.tbDostNazwa.ReadOnly = true;
            this.tbDostNazwa.Size = new System.Drawing.Size(145, 21);
            this.tbDostNazwa.TabIndex = 5;
            this.tbDostNazwa.TabStop = false;
            // 
            // lbDostNazwa
            // 
            this.lbDostNazwa.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbDostNazwa.Location = new System.Drawing.Point(3, 217);
            this.lbDostNazwa.Name = "lbDostNazwa";
            this.lbDostNazwa.Size = new System.Drawing.Size(100, 21);
            this.lbDostNazwa.Text = "Dost. nazwa:";
            // 
            // tbDostSymbol
            // 
            this.tbDostSymbol.Location = new System.Drawing.Point(92, 193);
            this.tbDostSymbol.Name = "tbDostSymbol";
            this.tbDostSymbol.ReadOnly = true;
            this.tbDostSymbol.Size = new System.Drawing.Size(145, 21);
            this.tbDostSymbol.TabIndex = 4;
            this.tbDostSymbol.TabStop = false;
            // 
            // lbDostSymbol
            // 
            this.lbDostSymbol.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbDostSymbol.Location = new System.Drawing.Point(3, 193);
            this.lbDostSymbol.Name = "lbDostSymbol";
            this.lbDostSymbol.Size = new System.Drawing.Size(100, 21);
            this.lbDostSymbol.Text = "Dost. symbol:";
            // 
            // tbSkontrolowany
            // 
            this.tbSkontrolowany.Location = new System.Drawing.Point(92, 170);
            this.tbSkontrolowany.Name = "tbSkontrolowany";
            this.tbSkontrolowany.ReadOnly = true;
            this.tbSkontrolowany.Size = new System.Drawing.Size(60, 21);
            this.tbSkontrolowany.TabIndex = 3;
            this.tbSkontrolowany.TabStop = false;
            // 
            // lbSkontrolowany
            // 
            this.lbSkontrolowany.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSkontrolowany.Location = new System.Drawing.Point(3, 170);
            this.lbSkontrolowany.Name = "lbSkontrolowany";
            this.lbSkontrolowany.Size = new System.Drawing.Size(100, 21);
            this.lbSkontrolowany.Text = "Skontrolowany:";
            // 
            // tbDokObrotowy
            // 
            this.tbDokObrotowy.Location = new System.Drawing.Point(92, 147);
            this.tbDokObrotowy.Name = "tbDokObrotowy";
            this.tbDokObrotowy.ReadOnly = true;
            this.tbDokObrotowy.Size = new System.Drawing.Size(145, 21);
            this.tbDokObrotowy.TabIndex = 2;
            this.tbDokObrotowy.TabStop = false;
            // 
            // lbDokObrotowy
            // 
            this.lbDokObrotowy.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbDokObrotowy.Location = new System.Drawing.Point(3, 148);
            this.lbDokObrotowy.Name = "lbDokObrotowy";
            this.lbDokObrotowy.Size = new System.Drawing.Size(100, 21);
            this.lbDokObrotowy.Text = "Dok. obrotowy:";
            // 
            // tbIloscLP
            // 
            this.tbIloscLP.Location = new System.Drawing.Point(92, 124);
            this.tbIloscLP.Name = "tbIloscLP";
            this.tbIloscLP.ReadOnly = true;
            this.tbIloscLP.Size = new System.Drawing.Size(60, 21);
            this.tbIloscLP.TabIndex = 1;
            this.tbIloscLP.TabStop = false;
            // 
            // lbIloscLP
            // 
            this.lbIloscLP.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbIloscLP.Location = new System.Drawing.Point(3, 124);
            this.lbIloscLP.Name = "lbIloscLP";
            this.lbIloscLP.Size = new System.Drawing.Size(100, 21);
            this.lbIloscLP.Text = "Liczba LP:";
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 11;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "PRZYJ�CIE - LISTA MP";
            // 
            // dataTable
            // 
            this.dataTable.DisplayExpression = "";
            this.dataTable.Prefix = "";
            this.dataTable.TableName = "";
            // 
            // lstListaMP
            // 
            this.lstListaMP.Columns.Add(this.chId);
            this.lstListaMP.Columns.Add(this.chLp);
            this.lstListaMP.Columns.Add(this.chSymbolDokumentu);
            this.lstListaMP.Columns.Add(this.chStatus);
            this.lstListaMP.Columns.Add(this.chData);
            this.lstListaMP.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lstListaMP.FullRowSelect = true;
            this.lstListaMP.Location = new System.Drawing.Point(0, 16);
            this.lstListaMP.Name = "lstListaMP";
            this.lstListaMP.Size = new System.Drawing.Size(240, 103);
            this.lstListaMP.TabIndex = 0;
            this.lstListaMP.View = System.Windows.Forms.View.Details;
            this.lstListaMP.SelectedIndexChanged += new System.EventHandler(this.onSelectedIndexChanged);
            // 
            // chId
            // 
            this.chId.Text = "";
            this.chId.Width = 0;
            // 
            // chLp
            // 
            this.chLp.Text = "Lp";
            this.chLp.Width = 30;
            // 
            // chSymbolDokumentu
            // 
            this.chSymbolDokumentu.Text = "Dokument";
            this.chSymbolDokumentu.Width = 95;
            // 
            // chStatus
            // 
            this.chStatus.Text = "Status";
            this.chStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chStatus.Width = 50;
            // 
            // chData
            // 
            this.chData.Text = "Data";
            this.chData.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chData.Width = 62;
            // 
            // btnPowrot
            // 
            this.btnPowrot.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPowrot.Location = new System.Drawing.Point(161, 29);
            this.btnPowrot.Name = "btnPowrot";
            this.btnPowrot.Size = new System.Drawing.Size(75, 19);
            this.btnPowrot.TabIndex = 17;
            this.btnPowrot.TabStop = false;
            this.btnPowrot.Text = "&Esc Powr�t";
            this.btnPowrot.Click += new System.EventHandler(this.btnPowrot_Click);
            // 
            // lrcNavigation
            // 
            this.lrcNavigation.BackColor = System.Drawing.SystemColors.Desktop;
            this.lrcNavigation.Location = new System.Drawing.Point(205, 0);
            this.lrcNavigation.Name = "lrcNavigation";
            this.lrcNavigation.NextEnabled = true;
            this.lrcNavigation.PreviousEnabled = true;
            this.lrcNavigation.Size = new System.Drawing.Size(32, 16);
            this.lrcNavigation.TabIndex = 24;
            this.lrcNavigation.TabStop = false;
            this.lrcNavigation.Next += new System.EventHandler(this.btnRight_Click);
            this.lrcNavigation.Previous += new System.EventHandler(this.btnLeft_Click);
            // 
            // ListaMPView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lrcNavigation);
            this.Controls.Add(this.lstListaMP);
            this.Controls.Add(this.lbTytul);
            this.Controls.Add(this.tbDokObrotowy);
            this.Controls.Add(this.tbSkontrolowany);
            this.Controls.Add(this.tbDostSymbol);
            this.Controls.Add(this.tbDostNazwa);
            this.Controls.Add(this.tbIloscLP);
            this.Controls.Add(this.lbIloscLP);
            this.Controls.Add(this.lbDokObrotowy);
            this.Controls.Add(this.lbSkontrolowany);
            this.Controls.Add(this.lbDostSymbol);
            this.Controls.Add(this.lbDostNazwa);
            this.Name = "ListaMPView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbDostNazwa, 0);
            this.Controls.SetChildIndex(this.lbDostSymbol, 0);
            this.Controls.SetChildIndex(this.lbSkontrolowany, 0);
            this.Controls.SetChildIndex(this.lbDokObrotowy, 0);
            this.Controls.SetChildIndex(this.lbIloscLP, 0);
            this.Controls.SetChildIndex(this.tbIloscLP, 0);
            this.Controls.SetChildIndex(this.tbDostNazwa, 0);
            this.Controls.SetChildIndex(this.tbDostSymbol, 0);
            this.Controls.SetChildIndex(this.tbSkontrolowany, 0);
            this.Controls.SetChildIndex(this.tbDokObrotowy, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lstListaMP, 0);
            this.Controls.SetChildIndex(this.lrcNavigation, 0);
            this.pnlNavigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSzukaj;
        private System.Windows.Forms.Button btnKontroluj;
        private System.Windows.Forms.Button btnRealizuj;
        private System.Windows.Forms.TextBox tbDostNazwa;
        private System.Windows.Forms.Label lbDostNazwa;
        private System.Windows.Forms.TextBox tbDostSymbol;
        private System.Windows.Forms.Label lbDostSymbol;
        private System.Windows.Forms.TextBox tbSkontrolowany;
        private System.Windows.Forms.Label lbSkontrolowany;
        private System.Windows.Forms.TextBox tbDokObrotowy;
        private System.Windows.Forms.Label lbDokObrotowy;
        private System.Windows.Forms.TextBox tbIloscLP;
        private System.Windows.Forms.Label lbIloscLP;
        private MSMLabel lbTytul;
        private System.Data.DataTable dataTable;
        private MSMListView lstListaMP;
        private System.Windows.Forms.ColumnHeader chLp;
        private System.Windows.Forms.ColumnHeader chSymbolDokumentu;
        private System.Windows.Forms.ColumnHeader chStatus;
        private System.Windows.Forms.ColumnHeader chData;
        private System.Windows.Forms.ColumnHeader chId;
        private System.Windows.Forms.Button btnPowrot;
        private LeftRightControl lrcNavigation;

    }
}
