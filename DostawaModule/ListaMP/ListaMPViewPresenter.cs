#region

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Common;
using Common.Base;
using DostawaModule.DostawaProxy;
using DostawaModule.ListaSearch;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;

#endregion

namespace DostawaModule
{
    public class ListaMPViewPresenter : PresenterBase
    {
        private KryteriaZapytaniaListyMP m_KryteriaZapytania;
        private List<Naglowek> m_ListaNaglowkow;
        private int m_NumerStrony;

        public ListaMPViewPresenter(IListaMPView view) : base(view)
        {
            m_KryteriaZapytania = new KryteriaZapytaniaListyMP {NumerStrony = 0};
        }

        protected ListaMPWorkItem MyWorkItem
        {
            get { return WorkItem as ListaMPWorkItem; }
        }

        public IListaMPView View
        {
            get { return m_view as IListaMPView; }
        }

        public KryteriaZapytaniaListyMP KryteriaZapytania
        {
            get { return m_KryteriaZapytania; }
            set { m_KryteriaZapytania = value; }
        }

        private bool MozliwaKontrola
        {
            get
            {
                var mozliwaKontrola = MyWorkItem.RootWorkItem.State[StateConstants.DELIVERY_CONTROL];
                if (mozliwaKontrola != null)
                {
                    return (bool) mozliwaKontrola;
                }
                return false;
            }
        }

        private bool MozliwaRealizacja
        {
            get
            {
                var mozliwaRealizacja = MyWorkItem.RootWorkItem.State[StateConstants.DELIVERY_REALIZATION];
                if (mozliwaRealizacja != null)
                {
                    return (bool) mozliwaRealizacja;
                }
                return false;
            }
        }

        protected override void AttachView()
        {
            View.PrzejscieDoNowegoRekordu -= ViewPrzejdzDoNastepnegoRekordu;
            View.PrzejscieDoNowegoRekordu += ViewPrzejdzDoNastepnegoRekordu;
            View.Kontroluj -= ViewKontrolujDokument;
            View.Kontroluj += ViewKontrolujDokument;
            View.ZaczytajNastepnaStrone -= ViewZaczytajNastepnaStrone;
            View.ZaczytajNastepnaStrone += ViewZaczytajNastepnaStrone;
            View.ZaczytajPoprzedniaStrone -= ViewZaczytajPoprzedniaStrone;
            View.ZaczytajPoprzedniaStrone += ViewZaczytajPoprzedniaStrone;
            View.Szukaj -= ViewSzukaj;
            View.Szukaj += ViewSzukaj;
            View.Realizuj -= ViewRealizuj;
            View.Realizuj += ViewRealizuj;
            View.Powrot += ViewPowrot;
        }

        public void ZaladujDaneDoWidoku(List<Naglowek> listaNaglowkow, long iloscNaglowkow)
        {
            View.ListaMPDataSource.Clear();

            m_ListaNaglowkow = listaNaglowkow;

            View.IloscNaglowkow = iloscNaglowkow;

            if (listaNaglowkow != null)
            {
                for (var i = 0; i < listaNaglowkow.Count; i++)
                {
                    View.ListaMPDataSource.Add(UtworzWpis(listaNaglowkow[i]));
                }
                View.ListaMPDataSource[0].Selected = true;

                if (listaNaglowkow.Count > 0)
                {
                    View.Naglowek = listaNaglowkow[0];
                }
                else
                {
                    View.Naglowek = new Naglowek();
                }
            }
        }

        private void ViewPrzejdzDoNastepnegoRekordu(Object sender, EventArgs e)
        {
            var indeks = View.IndeksAkutalnejPozycji;

            if ((indeks.HasValue) && (m_ListaNaglowkow.Count > indeks))
            {
                View.Naglowek = m_ListaNaglowkow[(int) indeks];
            }
        }

        private void ViewKontrolujDokument(Object sender, EventArgs e)
        {
            var indeks = View.IndeksAkutalnejPozycji;

            if ((indeks.HasValue) && (m_ListaNaglowkow.Count > indeks))
            {
                //MyWorkItem.Kontroluj(IdDlaIndeksuPozycji(indeks));
                MyWorkItem.Parent.State["IdDokumentu"] = IdDlaIndeksuPozycji(indeks);
                MyWorkItem.Commands[CommandConstants.KONTROLA_MP].Execute();
            }
        }

        private void ViewZaczytajNastepnaStrone(Object sender, EventArgs e)
        {
            if (m_ListaNaglowkow != null)
            {
                if (m_ListaNaglowkow.Count == MyWorkItem.Configuration.DostawaWS.WielkoscStrony)
                {
                    m_NumerStrony++;
                    m_KryteriaZapytania.NumerStrony = m_NumerStrony;
                    MyWorkItem.Szukaj(m_KryteriaZapytania);
                }
            }
        }

        private void ViewZaczytajPoprzedniaStrone(Object sender, EventArgs e)
        {
            if (m_NumerStrony > 0)
            {
                m_NumerStrony--;
                m_KryteriaZapytania.NumerStrony = m_NumerStrony;
                MyWorkItem.Szukaj(m_KryteriaZapytania);
            }
        }

        private void ViewSzukaj(Object sender, EventArgs e)
        {
            WorkItem.State["m_KryteriaZapytania"] = m_KryteriaZapytania;
            var listaMpSearchViewPresenter = WorkItem.Items.Get<ListaMPSearchViewPresenter>("ListaMPSearchViewPresenter");

            listaMpSearchViewPresenter.View.KryteriaZapytania = new KryteriaZapytaniaListyMP();
            WorkItem.Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
            MyWorkItem.MainWorkspace.Show(listaMpSearchViewPresenter.View);
        }

        private void ViewRealizuj(Object sender, EventArgs e)
        {
            var indeks = View.IndeksAkutalnejPozycji;

            if ((indeks.HasValue) && (m_ListaNaglowkow.Count > indeks))
            {
                MyWorkItem.Parent.State["IdDokumentu"] = IdDlaIndeksuPozycji(View.IndeksAkutalnejPozycji);
                MyWorkItem.Commands[CommandConstants.REALIZACJA_MP].Execute();
            }
        }

        private void ViewPowrot(Object sender, EventArgs e)
        {
            //WorkItemBase.MainWorkspace.Close(View);
            CloseView();
        }


        //private string SymbolDlaIndeksuPozycji(int? indeks)
        //{
        //    if ((indeks.HasValue) && (m_ListaNaglowkow.Count > indeks))
        //    {
        //        return m_ListaNaglowkow[(int) indeks].symbol;
        //    }
        //    return null;
        //}

        private long? IdDlaIndeksuPozycji(int? indeks)
        {
            if ((indeks.HasValue) && (m_ListaNaglowkow.Count > indeks))
            {
                return m_ListaNaglowkow[(int) indeks].id;
            }
            return null;
        }


        private static ListViewItem UtworzWpis(Naglowek naglowek)
        {
            var elementy = new[]
                               {
                                   naglowek.id.ToString(),
                                   naglowek.numerRekordu.ToString(),
                                   naglowek.symbol,
                                   naglowek.stan,
                                   naglowek.dataWystawienia.ToString(Constants.DATE_FORMAT)
                               };
            return new ListViewItem(elementy);
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    e.Handled = true;
                    ViewZaczytajPoprzedniaStrone(this, EventArgs.Empty);
                    break;
                case Keys.Right:
                    e.Handled = true;
                    ViewZaczytajNastepnaStrone(this, EventArgs.Empty);
                    break;
                case Keys.D1:
                    e.Handled = true;
                    ViewSzukaj(this, EventArgs.Empty);
                    break;
                case Keys.D2:
                    if (MozliwaKontrola)
                    {
                        e.Handled = true;
                        ViewKontrolujDokument(this, EventArgs.Empty);
                    }
                    break;
                case Keys.D3:
                    if (MozliwaRealizacja)
                    {
                        e.Handled = true;
                        ViewRealizuj(this, EventArgs.Empty);
                    }
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
                    MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
                    MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
                    MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;

                    //WorkItemBase.MainWorkspace.Close(View);
                    CloseView();
                    break;
            }
        }

        #region Event Subscription

        [EventSubscription(EventBrokerConstants.USER_HAS_LOGGED_ON)]
        public void OnUserHasLoggedOn(object sender, EventArgs e)
        {
            //uzytkownik nie jest zalogowany - reczne wywolanie metody
            if (MyWorkItem.RootWorkItem.State[StateConstants.USER] == null)
            {
                return;
            }

            View.KontrolujVisible = MozliwaKontrola;
            View.RealizujVisible = MozliwaRealizacja;
        }

        #endregion
    }
}