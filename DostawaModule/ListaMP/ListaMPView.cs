#region

using System;
using System.Data;
using System.Windows.Forms;
using Common.Base;
using DostawaModule.DostawaProxy;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace DostawaModule
{
    [SmartPart]
    public partial class ListaMPView : ViewBase, IListaMPView
    {
        private readonly BindingSource m_NaglowekBindingSource = new BindingSource();
        private readonly DataTable m_NaglowekDataTable = new DataTable();


        public ListaMPView()
        {
            InitializeComponent();
            InitializeFocusedControl();
            InitializeBindingSources();
        }

        #region IListaMPView Members

        public event EventHandler PrzejscieDoNowegoRekordu;
        public event EventHandler Kontroluj;
        public event EventHandler Szukaj;
        public event EventHandler ZaczytajNastepnaStrone;
        public event EventHandler ZaczytajPoprzedniaStrone;
        public event EventHandler Realizuj;
        public event EventHandler Powrot;

        public Naglowek Naglowek
        {
            set
            {
                m_NaglowekBindingSource.DataSource = value;
                SetDataSource();
            }
        }

        public int? IndeksAkutalnejPozycji
        {
            get
            {
                if (lstListaMP.SelectedIndices.Count == 0)
                {
                    return null;
                }

                return lstListaMP.SelectedIndices[0];
            }
        }

        public ListView.ListViewItemCollection ListaMPDataSource
        {
            get { return lstListaMP.Items; }
        }

        public bool KontrolujVisible
        {
            set { btnKontroluj.Visible = value; }
        }

        public bool RealizujVisible
        {
            set { btnRealizuj.Visible = value; }
        }

        public long IloscNaglowkow
        {
            set { tbIloscLP.Text = value.ToString(); }
        }

        #endregion

        private void SetDataSource()
        {
            m_NaglowekDataTable.Clear();

            var dataRowIndeks = m_NaglowekDataTable.NewRow();

            var naglowek = m_NaglowekBindingSource.DataSource as Naglowek;
            if (naglowek == null) return;

            dataRowIndeks["symbolDokumentuObrotowego"] = naglowek.symbolDokumentuObrotowego;
            dataRowIndeks["skontrolowany"] = naglowek.skontrolowany;
            dataRowIndeks["symbolDostawcy"] = naglowek.symbolDostawcy;
            dataRowIndeks["nazwaDostawcy"] = naglowek.nazwaDostawcy;

            m_NaglowekDataTable.Rows.Add(dataRowIndeks);
        }

        private void onSelectedIndexChanged(object sender, EventArgs e)
        {
            if (PrzejscieDoNowegoRekordu != null)
            {
                PrzejscieDoNowegoRekordu(this, null);
            }
        }

        private void btnKontroluj_Click(object sender, EventArgs e)
        {
            if (Kontroluj != null)
            {
                Kontroluj(this, null);
            }
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            if (ZaczytajNastepnaStrone != null)
            {
                ZaczytajNastepnaStrone(this, null);
            }
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            if (ZaczytajPoprzedniaStrone != null)
            {
                ZaczytajPoprzedniaStrone(this, null);
            }
        }

        private void btnSzukaj_Click(object sender, EventArgs e)
        {
            if (Szukaj != null)
            {
                Szukaj(this, null);
            }
        }

        private void btnRealizuj_Click(object sender, EventArgs e)
        {
            if (Realizuj != null)
            {
                Realizuj(this, null);
            }
        }

        private void btnPowrot_Click(object sender, EventArgs e)
        {
            if (Powrot != null)
            {
                Powrot(this, null);
            }
        }

        #region Nested type: ListaNaglowkowDelegate

        //private delegate void ListaNaglowkowDelegate(DataTable dt);

        #endregion

        #region Nested type: NaglowekDelegate

        //private delegate void NaglowekDelegate(Naglowek n);

        #endregion


        public void InitializeBindingSources()
        {
            var column = new DataColumn { DataType = Type.GetType("System.String"), ColumnName = "symbolDokumentuObrotowego", ReadOnly = true, Unique = false };
            m_NaglowekDataTable.Columns.Add(column);

            column = new DataColumn { DataType = Type.GetType("System.String"), ColumnName = "skontrolowany", ReadOnly = true, Unique = false };
            m_NaglowekDataTable.Columns.Add(column);

            column = new DataColumn { DataType = Type.GetType("System.String"), ColumnName = "symbolDostawcy", ReadOnly = true, Unique = false };
            m_NaglowekDataTable.Columns.Add(column);

            column = new DataColumn { DataType = Type.GetType("System.String"), ColumnName = "nazwaDostawcy", ReadOnly = true, Unique = false };
            m_NaglowekDataTable.Columns.Add(column);


            m_NaglowekDataTable.Columns.Add(new DataColumn());

            tbDokObrotowy.DataBindings.Add(new Binding("Text", m_NaglowekDataTable, "symbolDokumentuObrotowego"));
            tbSkontrolowany.DataBindings.Add(new Binding("Text", m_NaglowekDataTable, "skontrolowany"));
            tbDostSymbol.DataBindings.Add(new Binding("Text", m_NaglowekDataTable, "symbolDostawcy"));
            tbDostNazwa.DataBindings.Add(new Binding("Text", m_NaglowekDataTable, "nazwaDostawcy"));

        }
    }
}