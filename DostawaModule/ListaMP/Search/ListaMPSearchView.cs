#region

using Common.SearchForm;

#endregion

namespace DostawaModule.ListaSearch
{
    public partial class ListaMPSearchView : SearchForm, IListaMPSearchView
    {
        private int? m_NumerStrony;
        //private int? wielkoscStrony;

        public ListaMPSearchView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #region IListaMPSearchView Members

        public KryteriaZapytaniaListyMP KryteriaZapytania
        {
            get
            {
                var kryteriaZapytania = new KryteriaZapytaniaListyMP
                                            {
                                                Symbol = tbDokument.Text,
                                                SymbolDostawcy = tbSymbolDostawcy.Text,
                                                Status = tbStatus.Text,
                                                SymbolDokumentuObrotowego = tbSymbolDokumentu.Text,
                                                Skontrolowany = tbSokntrolowany.Text,
                                                NumerStrony = m_NumerStrony,
                                                DataDokumentu = tbData.Value
                                            };
                //kryteriaZapytania.WielkoscStrony = wielkoscStrony;

                return kryteriaZapytania;
            }
            set { DoUpdate(value); }
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);

            tbDokument.ReadOnly = navigationState;
            tbSokntrolowany.ReadOnly = navigationState;
            tbStatus.ReadOnly = navigationState;
            tbSymbolDokumentu.ReadOnly = navigationState;
            tbSymbolDostawcy.ReadOnly = navigationState;
            tbData.ReadOnly = navigationState;
        }

        #endregion

        private void DoUpdate(KryteriaZapytaniaListyMP kryteriaZapytania)
        {
            if (InvokeRequired)
            {
                Invoke(new KryteriaZapytaniaDelegate(DoUpdate),
                       new object[] {kryteriaZapytania});
                return;
            }

            tbDokument.Text = kryteriaZapytania.Symbol;
            tbSymbolDostawcy.Text = kryteriaZapytania.SymbolDostawcy;
            tbStatus.Text = kryteriaZapytania.Status;
            tbSymbolDokumentu.Text = kryteriaZapytania.SymbolDokumentuObrotowego;
            tbSokntrolowany.Text = kryteriaZapytania.Skontrolowany;
            m_NumerStrony = kryteriaZapytania.NumerStrony;
            //wielkoscStrony = kryteriaZapytania.WielkoscStrony;
            tbData.Value = kryteriaZapytania.DataDokumentu;
        }

        #region Nested type: KryteriaZapytaniaDelegate

        private delegate void KryteriaZapytaniaDelegate(KryteriaZapytaniaListyMP kryteriaZapytania);

        #endregion
    }
}