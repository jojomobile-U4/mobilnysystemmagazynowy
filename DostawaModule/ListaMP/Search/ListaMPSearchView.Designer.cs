using Common.Components;

namespace DostawaModule.ListaSearch
{
    partial class ListaMPSearchView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTytul = new Common.Components.MSMLabel();
            this.lbDokument = new System.Windows.Forms.Label();
            this.tbDokument = new System.Windows.Forms.TextBox();
            this.lbSymbolDostawcy = new System.Windows.Forms.Label();
            this.tbSymbolDostawcy = new System.Windows.Forms.TextBox();
            this.lbStatus = new System.Windows.Forms.Label();
            this.tbStatus = new System.Windows.Forms.TextBox();
            this.tbSymbolDokumentu = new System.Windows.Forms.TextBox();
            this.lbSymbolDokumentu = new System.Windows.Forms.Label();
            this.tbSokntrolowany = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbData = new Common.Components.MSMDate();
            this.lbData = new System.Windows.Forms.Label();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // btnOstatnieZapytanie
            // 
            this.btnOstatnieZapytanie.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Location = new System.Drawing.Point(0, 252);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 23;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "LISTA MP - SZUKAJ";
            // 
            // lbDokument
            // 
            this.lbDokument.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbDokument.Location = new System.Drawing.Point(3, 44);
            this.lbDokument.Name = "lbDokument";
            this.lbDokument.Size = new System.Drawing.Size(90, 20);
            this.lbDokument.Text = "Dokument:";
            // 
            // tbDokument
            // 
            this.tbDokument.Location = new System.Drawing.Point(95, 44);
            this.tbDokument.Name = "tbDokument";
            this.tbDokument.Size = new System.Drawing.Size(142, 21);
            this.tbDokument.TabIndex = 0;
            // 
            // lbSymbolDostawcy
            // 
            this.lbSymbolDostawcy.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbolDostawcy.Location = new System.Drawing.Point(3, 70);
            this.lbSymbolDostawcy.Name = "lbSymbolDostawcy";
            this.lbSymbolDostawcy.Size = new System.Drawing.Size(90, 20);
            this.lbSymbolDostawcy.Text = "Symbol dost.:";
            // 
            // tbSymbolDostawcy
            // 
            this.tbSymbolDostawcy.Location = new System.Drawing.Point(95, 70);
            this.tbSymbolDostawcy.Name = "tbSymbolDostawcy";
            this.tbSymbolDostawcy.Size = new System.Drawing.Size(142, 21);
            this.tbSymbolDostawcy.TabIndex = 1;
            // 
            // lbStatus
            // 
            this.lbStatus.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbStatus.Location = new System.Drawing.Point(3, 96);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(90, 20);
            this.lbStatus.Text = "Status:";
            // 
            // tbStatus
            // 
            this.tbStatus.Location = new System.Drawing.Point(95, 96);
            this.tbStatus.Name = "tbStatus";
            this.tbStatus.Size = new System.Drawing.Size(142, 21);
            this.tbStatus.TabIndex = 2;
            // 
            // tbSymbolDokumentu
            // 
            this.tbSymbolDokumentu.Location = new System.Drawing.Point(95, 122);
            this.tbSymbolDokumentu.Name = "tbSymbolDokumentu";
            this.tbSymbolDokumentu.Size = new System.Drawing.Size(142, 21);
            this.tbSymbolDokumentu.TabIndex = 3;
            // 
            // lbSymbolDokumentu
            // 
            this.lbSymbolDokumentu.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbolDokumentu.Location = new System.Drawing.Point(3, 122);
            this.lbSymbolDokumentu.Name = "lbSymbolDokumentu";
            this.lbSymbolDokumentu.Size = new System.Drawing.Size(90, 20);
            this.lbSymbolDokumentu.Text = "Dok. obrotowy:";
            // 
            // tbSokntrolowany
            // 
            this.tbSokntrolowany.Location = new System.Drawing.Point(95, 147);
            this.tbSokntrolowany.Name = "tbSokntrolowany";
            this.tbSokntrolowany.Size = new System.Drawing.Size(142, 21);
            this.tbSokntrolowany.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(3, 147);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 20);
            this.label1.Text = "Skontrolowany:";
            // 
            // tbData
            // 
            this.tbData.Location = new System.Drawing.Point(95, 172);
            this.tbData.Name = "tbData";
            this.tbData.Size = new System.Drawing.Size(142, 21);
            this.tbData.TabIndex = 5;
            this.tbData.Value = null;
            // 
            // lbData
            // 
            this.lbData.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbData.Location = new System.Drawing.Point(3, 172);
            this.lbData.Name = "lbData";
            this.lbData.Size = new System.Drawing.Size(90, 28);
            this.lbData.Text = "Data dok.: (dd.mm.rrrr)";
            // 
            // ListaMPSearchView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.Controls.Add(this.tbData);
            this.Controls.Add(this.lbData);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbSokntrolowany);
            this.Controls.Add(this.lbSymbolDokumentu);
            this.Controls.Add(this.tbSymbolDokumentu);
            this.Controls.Add(this.tbStatus);
            this.Controls.Add(this.lbStatus);
            this.Controls.Add(this.tbSymbolDostawcy);
            this.Controls.Add(this.lbSymbolDostawcy);
            this.Controls.Add(this.tbDokument);
            this.Controls.Add(this.lbDokument);
            this.Controls.Add(this.lbTytul);
            this.Name = "ListaMPSearchView";
            this.Size = new System.Drawing.Size(240, 280);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lbDokument, 0);
            this.Controls.SetChildIndex(this.tbDokument, 0);
            this.Controls.SetChildIndex(this.lbSymbolDostawcy, 0);
            this.Controls.SetChildIndex(this.tbSymbolDostawcy, 0);
            this.Controls.SetChildIndex(this.lbStatus, 0);
            this.Controls.SetChildIndex(this.tbStatus, 0);
            this.Controls.SetChildIndex(this.tbSymbolDokumentu, 0);
            this.Controls.SetChildIndex(this.lbSymbolDokumentu, 0);
            this.Controls.SetChildIndex(this.tbSokntrolowany, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.lbData, 0);
            this.Controls.SetChildIndex(this.tbData, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MSMLabel lbTytul;
        private System.Windows.Forms.Label lbDokument;
        private System.Windows.Forms.TextBox tbDokument;
        private System.Windows.Forms.Label lbSymbolDostawcy;
        private System.Windows.Forms.TextBox tbSymbolDostawcy;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.TextBox tbStatus;
        private System.Windows.Forms.TextBox tbSymbolDokumentu;
        private System.Windows.Forms.Label lbSymbolDokumentu;
        private System.Windows.Forms.TextBox tbSokntrolowany;
        private System.Windows.Forms.Label label1;
        private MSMDate tbData;
        private System.Windows.Forms.Label lbData;    
    }
}
