using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using Microsoft.Practices.Mobile.CompositeUI;
using Common.SearchForm;
using System.Windows.Forms;
using Common;

namespace DostawaModule.ListaSearch
{
    public class ListaMPSearchViewPresenter: SearchFormPresenter
    {
        KryteriaZapytaniaListyMP kryteriaZapytania;
        KryteriaZapytaniaListyMP ostatnieZapytanie = new KryteriaZapytaniaListyMP();

		public IListaMPSearchView View
		{
			get { return m_view as IListaMPSearchView; }
		}

        protected ListaMPWorkItem MyWorkItem
        {
            get { return WorkItem as ListaMPWorkItem; }
        }

        public ListaMPSearchViewPresenter(IListaMPSearchView view)
            : base(view)
        {            
        }

		protected override void OnViewUstawOstatnieZapytanie(object sender, EventArgs eventArgs)
		{
			base.OnViewUstawOstatnieZapytanie(sender, eventArgs);
            View.KryteriaZapytania = ostatnieZapytanie;
		}

        public void ResetujKryteriaZapytania()
        {
            View.KryteriaZapytania = new KryteriaZapytaniaListyMP();
        }

		protected override void OnViewSzukaj(object sender, EventArgs eventArgs)
		{
            
			this.kryteriaZapytania = View.KryteriaZapytania;
            this.kryteriaZapytania.NumerStrony = ((KryteriaZapytaniaListyMP)MyWorkItem.State["m_KryteriaZapytania"]).NumerStrony;
			//this.kryteriaZapytania.WielkoscStrony = ((KryteriaZapytaniaListyMP)MyWorkItem.State["kryteriaZapytania"]).WielkoscStrony;

            ostatnieZapytanie = kryteriaZapytania;
			MyWorkItem.Szukaj(kryteriaZapytania);

            base.OnViewSzukaj(sender, eventArgs);
            MyWorkItem.MainWorkspace.Show(WorkItem.Items.Get<ListaMPViewPresenter>(ItemsConstants.LISTA_MP_PRESENTER).View);
            WorkItem.Activate();            
        }

    }
}
