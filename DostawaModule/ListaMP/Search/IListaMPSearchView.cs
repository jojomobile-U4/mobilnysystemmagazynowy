#region

using Common.SearchForm;

#endregion

namespace DostawaModule.ListaSearch
{
    public interface IListaMPSearchView : ISearchForm
    {
        KryteriaZapytaniaListyMP KryteriaZapytania { get; set; }
    }
}