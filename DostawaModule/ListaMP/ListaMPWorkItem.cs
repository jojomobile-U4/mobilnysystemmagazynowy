#region

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Common;
using Common.Base;
using DostawaModule.DostawaProxy;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using StatusOperacji=Common.DataModel.StatusOperacji;

#endregion

namespace DostawaModule
{
    public class ListaMPWorkItem : WorkItemBase
    {
        #region Constants

        private const string LISTA_MP = "Lista MP";

        #endregion

        #region Private Fields

        private KryteriaZapytaniaListyMP m_OstatieZapytanie = new KryteriaZapytaniaListyMP();

        #endregion

        public void Show(IWorkspace parentWorkspace)
        {
            FunctionsHelper.SetCursorWait();

            var listaMPViewPresenter = Items.Get<ListaMPViewPresenter>(ItemsConstants.LISTA_MP_PRESENTER);
            parentWorkspace.Show(listaMPViewPresenter.View);
            this.Activate();

            FunctionsHelper.SetCursorDefault();
        }


        public void Kontroluj(long? idDokumentuMP)
        {
            var kontrolaMPWorkItem = Parent.WorkItems.Get<KontrolaMPWorkItem>(WorkItemsConstants.KONTROLA_MP_WORKITEM);
            // KontrolaMPViewPresenter kontrolaMPViewPresenter = kontrolaMPWorkItem.Items.Get<KontrolaMPViewPresenter>(ItemsConstants.KONTROLA_MP_PRESENTER);
            //kontrolaMPWorkItem.Show(MainWorkspace, idDokumentuMP);            
        }


        public void Szukaj()
        {
            Szukaj(Items.Get<ListaMPViewPresenter>(ItemsConstants.LISTA_MP_PRESENTER).KryteriaZapytania);
        }


        public void Szukaj(KryteriaZapytaniaListyMP kryteriaZapytania)
        {
            FunctionsHelper.SetCursorWait();

            try
            {
                long iloscNaglowkow = 0;
                m_OstatieZapytanie = kryteriaZapytania;

                var dokumentyMagazynoweService = this.Services.Get<IDokumentyMagazynoweService>(true);
                var listaNaglowkow =
                    dokumentyMagazynoweService.PobierzListeNaglowkow(kryteriaZapytania, out iloscNaglowkow);
                if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, LISTA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                }

                //�atka
                if (listaNaglowkow == null)
                    listaNaglowkow = new List<Naglowek>();
                if (listaNaglowkow.Count == 0)
                    listaNaglowkow.Add(new Naglowek());

                Items.Get<ListaMPViewPresenter>(ItemsConstants.LISTA_MP_PRESENTER).ZaladujDaneDoWidoku(listaNaglowkow, iloscNaglowkow);
            }
            catch (Exception e)
            {
                ShowMessageBox(e.Message, LISTA_MP);
                Activate();
            }

            FunctionsHelper.SetCursorDefault();
        }


        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ, Thread = ThreadOption.UserInterface)]
        public void SzukajWgKoduKreskowego(object sender, EventArgs args)
        {
            var listaMPViewPresenter = Items.Get<ListaMPViewPresenter>(ItemsConstants.LISTA_MP_PRESENTER);

            if (MainWorkspace.ActiveSmartPart.ToString() == listaMPViewPresenter.View.ToString())
            {
                var kryteriaZapytania = new KryteriaZapytaniaListyMP();
                kryteriaZapytania.NumerStrony = 0;
                kryteriaZapytania.IdPozycji = -1;

                try
                {
                    kryteriaZapytania.IdPozycji = long.Parse(RootWorkItem.State[StateConstants.BAR_CODE].ToString());
                    Activate();
                }
                catch (FormatException)
                {
                    ShowMessageBox("Wczytany kod " + RootWorkItem.State[StateConstants.BAR_CODE] + " nie jest liczb�", "Szukaj");
                    Activate();
                }
                if (kryteriaZapytania.IdPozycji != -1)
                    Szukaj(kryteriaZapytania);

                Activate();
            }
        }

        public void OdswiezListeDokumentow()
        {
            Szukaj(m_OstatieZapytanie);
        }
    }
}