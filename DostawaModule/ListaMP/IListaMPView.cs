#region

using System;
using System.Windows.Forms;
using Common.Base;
using DostawaModule.DostawaProxy;

#endregion

namespace DostawaModule
{
    public interface IListaMPView : IViewBase
    {
        Naglowek Naglowek { set; }
        int? IndeksAkutalnejPozycji { get; }
        ListView.ListViewItemCollection ListaMPDataSource { get; }
        bool KontrolujVisible { set; }
        bool RealizujVisible { set; }

        long IloscNaglowkow { set; }
        event EventHandler PrzejscieDoNowegoRekordu;
        event EventHandler Kontroluj;
        event EventHandler Szukaj;
        event EventHandler Realizuj;
        event EventHandler Powrot;
        event EventHandler ZaczytajNastepnaStrone;
        event EventHandler ZaczytajPoprzedniaStrone;
    }
}