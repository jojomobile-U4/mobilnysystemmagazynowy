using System;
using System.Collections.Generic;
using System.Text;

namespace DostawaModule
{
    public class KryteriaZapytaniaPozycjiMP
    {
        private long? id;
        private long? idDokumentu;
        private string symbolTowaru;
        private string nazwaTowaru;
        private string skontrolowana;
        private string zakonczona;
        private int? numerStrony;
        private int? wielkoscStrony;

        public long? Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public long? IdDokumentu
        {
            get 
            {
                return idDokumentu;
            }
            set 
            {
                idDokumentu = value;
            }
        }

        public string SymbolTowaru
        {
            get
            {
                return symbolTowaru;
            }
            set
            {
                symbolTowaru = value;
            } 
        }

        public string NazwaTowaru
        {
            get
            {
                return nazwaTowaru;
            }
            set
            {
                nazwaTowaru = value;
            }
        }

        public string Zakonczona
        {
            get
            {
                return zakonczona;
            }
            set
            {
                zakonczona = value;
            }
        }

        public string Skontrolowana
        {
            get
            {
                return skontrolowana;
            }
            set
            {
                skontrolowana = value;
            }
        }

        public int? NumerStrony
        {
            get
            {
                return numerStrony;
            }
            set
            {
                numerStrony = value;
            }
        }

        public int? WielkoscStrony
        {
            get
            {
                return wielkoscStrony;
            }
            set
            {
                wielkoscStrony = value;
            }
        }

    }
}
