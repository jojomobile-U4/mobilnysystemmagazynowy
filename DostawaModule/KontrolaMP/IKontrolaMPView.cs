using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Common.Base;

namespace DostawaModule
{
    public interface IKontrolaMPView: IViewBase
    {
        // TODO: Expose events for the 
        // presenter to attach to.
        // Expose methods for the presenter 
        // to update the view.

        event EventHandler PrzejdzDoNastepnejPozycji;
        event EventHandler PrzejdzDoPoprzedniejPozycji;
        event EventHandler Szukaj;
        event EventHandler Zakoncz;
        event EventHandler Realizuj;
        PozycjaMP Pozycja { set; get; }
        string SymbolDokumentu { get; set; }
		bool IloscReadOnly { get; set; }
		bool WagaReadOnly { get; set; }
        void IloscSetFocus();
		void WagaSetFocus();
        float? Ilosc { get; set; }
		double? Waga { get; set; }
        string Skontrolowana { set; }
		void UstawWage(string waga);

		long Pozostalo { set; }

	 }
}
