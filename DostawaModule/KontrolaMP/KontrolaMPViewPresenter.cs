#region

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.DataModel;
using Common.WagiProxy;
using DostawaModule.KontrolaSearch;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;

#endregion

namespace DostawaModule
{
    public class KontrolaMPViewPresenter : PresenterBase
    {
        /**** [0001] - poprawka powstala podczas oddania wersji 02.07.2009 zgloszona przez K.Strawski
         ****          zakoncz pozycje jesli ilosc po kontroli jest rowna 0 ****/

        private const string ChangeWeightControlledItem = "Nie mo�na zmienia� wagi na skontrolowaniej pozycji.";
        private const long MIloscZerowana = 0;
        public const string NazwaFormularza = "PRZYJ�CIE - KONTROLA MP";
        private const string PomyslneWywolanieRaportu = "Wydruk zosta� wys�any na drukark�.";
        private long? m_IdAktualnegoDokumentu;
        private long m_IloscPozycjiNieskontrolowanych;
        private KryteriaZapytaniaPozycjiMP m_KryteriaZapytania;

        private List<PozycjaMP> m_ListaPozycji = new List<PozycjaMP>();
        private int m_NumerPozycji;
        private string m_SymbolDokumentu;

        private float? m_IloscOrg = -1;
        public float? IloscOrg
        {
            get
            { return m_IloscOrg; }
        }

        public KontrolaMPViewPresenter(IKontrolaMPView view) : base(view)
        {
            m_KryteriaZapytania = new KryteriaZapytaniaPozycjiMP();
            m_KryteriaZapytania.NumerStrony = 0;
            m_IloscPozycjiNieskontrolowanych = 0;
        }

        protected KontrolaMPWorkItem MyWorkItem
        {
            get { return WorkItem as KontrolaMPWorkItem; }
        }

        public IKontrolaMPView View
        {
            get { return m_view as IKontrolaMPView; }
        }

        public KryteriaZapytaniaPozycjiMP KryteriaZapytania
        {
            get { return m_KryteriaZapytania; }
            set { m_KryteriaZapytania = value; }
        }

        protected override void AttachView()
        {
            View.PrzejdzDoNastepnejPozycji += ViewPrzejdzDoNastepnejPozycji;
            View.PrzejdzDoPoprzedniejPozycji += ViewPrzejdzDoPoprzedniejPozycji;
            View.Szukaj += ViewSzukaj;
            View.Zakoncz += ViewZakoncz;
            View.Realizuj += ViewRealizuj;
        }

        public void ZaladujDaneDoWidoku(List<PozycjaMP> listaPozycji)
        {
            m_NumerPozycji = 0;

            if ((listaPozycji != null) && (listaPozycji.Count > 0) && (listaPozycji[m_NumerPozycji].Id.HasValue))
            {
                m_ListaPozycji = listaPozycji;
                m_SymbolDokumentu = m_ListaPozycji[m_NumerPozycji].SymbolDokumentu;
                View.SymbolDokumentu = m_ListaPozycji[m_NumerPozycji].SymbolDokumentu;
            }
            else
            {
                m_ListaPozycji.Clear();
                m_ListaPozycji.Add(new PozycjaMP());
                m_SymbolDokumentu = "";
                View.SymbolDokumentu = "";
            }
            m_NumerPozycji = 0;
            
            Aktualizuj();

            m_IloscOrg = View.Ilosc;
        }

        private void ViewPrzejdzDoNastepnejPozycji(Object sender, EventArgs e)
        {
            if (m_NumerPozycji < (m_ListaPozycji.Count - 1))
            {
                View.ForceDataBinding();
                m_NumerPozycji++;
                Aktualizuj();
                m_IloscOrg = View.Ilosc;
                View.IloscSetFocus();
            }
        }

        private void ViewPrzejdzDoPoprzedniejPozycji(Object sender, EventArgs e)
        {
            if (m_NumerPozycji > 0)
            {
                View.ForceDataBinding();
                m_NumerPozycji--;
                Aktualizuj();
                m_IloscOrg = View.Ilosc;
                View.IloscSetFocus();
            }
        }

        private void ViewSzukaj(Object sender, EventArgs e)
        {
            WorkItem.State["m_KryteriaZapytania"] = m_KryteriaZapytania;
            var kontrolaMpSearchViewPresenter = WorkItem.Items.Get<KontrolaMPSearchViewPresenter>(ItemsConstants.KONTROLA_MP_SEARCH_PRESENTER);
            kontrolaMpSearchViewPresenter.View.KryteriaZapytania = new KryteriaZapytaniaPozycjiMP();

            WorkItem.Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
            MyWorkItem.MainWorkspace.Show(kontrolaMpSearchViewPresenter.View);
        }

        private void UstawSkontrolowanaPozycje()
        {
            if ((m_ListaPozycji.Count > 0) && (m_ListaPozycji[m_NumerPozycji].Id != null) && (m_NumerPozycji >= 0) && (m_NumerPozycji < m_ListaPozycji.Count) && (m_ListaPozycji[m_NumerPozycji].Skontrolowana.Equals(DBBool.FALSE)))
            {
                MyWorkItem.Activate();
                if (MyWorkItem.UstawSkontrolowanaPozycje((long) m_ListaPozycji[m_NumerPozycji].Id, View.Ilosc, View.Waga, IloscOrg))
                {
                    m_ListaPozycji[m_NumerPozycji].Skontrolowana = DBBool.TRUE;
                    View.Skontrolowana = m_ListaPozycji[m_NumerPozycji].Skontrolowana;
                    m_IloscPozycjiNieskontrolowanych--;
                    Aktualizuj();
                }
            }
        }

        private void ViewZakoncz(Object sender, EventArgs e)
        {
            ZakonczPozycje();
        }

        private void ViewRealizuj(Object sender, EventArgs e)
        {
            if (!SkontrolowanoWszystkiePozycje())
            {
                var result = MessageBox.Show("Nie wszystkie pozycje zosta�y skontrolowane, czy na pewno chcesz rozp�cz�� cz�ciow� realizacj� dostawy?", "Realizacja", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                MyWorkItem.Activate();
                if (result == DialogResult.Yes)
                {
                    if (m_ListaPozycji.Count > 0)
                    {
                        MyWorkItem.RootWorkItem.State["IdDokumentu"] = m_ListaPozycji[0].IdDokumentu;
                    }
                    else
                    {
                        MyWorkItem.RootWorkItem.State["IdDokumentu"] = -1;
                    }

                    //WorkItemBase.MainWorkspace.Close(View);
                    CloseView();
                    MyWorkItem.RootWorkItem.Commands[CommandConstants.REALIZACJA_MP].Execute();
                }
            }
            else
            {
                if (m_ListaPozycji.Count > 0)
                {
                    MyWorkItem.RootWorkItem.State["IdDokumentu"] = m_ListaPozycji[0].IdDokumentu;
                }
                else
                {
                    MyWorkItem.RootWorkItem.State["IdDokumentu"] = -1;
                }

                //WorkItemBase.MainWorkspace.Close(View);
                CloseView();
                MyWorkItem.RootWorkItem.Commands[CommandConstants.REALIZACJA_MP].Execute();
            }
        }

        private bool SkontrolowanoWszystkiePozycje()
        {
            //Mozna oprzec sie na wartosci iloscNieskontrolowanychPozycji
            if (m_ListaPozycji.Count > 0)
            {
                var i = m_ListaPozycji.Count;

                while (i > 0)
                {
                    i--;
                    if (m_ListaPozycji[i].Skontrolowana.Equals(DBBool.FALSE))
                    {
                        return false;
                    }
                }
            }
            return true;
        }


        private void Aktualizuj()
        {
            if ((m_ListaPozycji[m_NumerPozycji].Skontrolowana == null) || (m_ListaPozycji[m_NumerPozycji].Skontrolowana.Equals(DBBool.TRUE)))
            {
                View.IloscReadOnly = true;
                View.WagaReadOnly = true;
            }
            else
            {
                View.IloscReadOnly = NavigationState;
                View.WagaReadOnly = NavigationState;
            }

            if ((m_IdAktualnegoDokumentu != m_ListaPozycji[m_NumerPozycji].IdDokumentu) && (m_ListaPozycji[m_NumerPozycji].IdDokumentu.HasValue))
            {
                m_IloscPozycjiNieskontrolowanych = MyWorkItem.IloscPozycjiNaDokumencie((long) m_ListaPozycji[m_NumerPozycji].IdDokumentu,
                                                                                       "N", m_KryteriaZapytania.Zakonczona);
                m_IdAktualnegoDokumentu = m_ListaPozycji[m_NumerPozycji].IdDokumentu;
            }

            MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = m_ListaPozycji[m_NumerPozycji].SymbolTowaru;
            MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueYes;
            MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = m_ListaPozycji[m_NumerPozycji].SymbolTowaru;
            MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;


            View.Pozostalo = m_IloscPozycjiNieskontrolowanych;
            View.Pozycja = m_ListaPozycji[m_NumerPozycji];
        }

        private void UruchomRaport()
        {
            if ((m_ListaPozycji != null) && (m_ListaPozycji.Count > 0) && (m_ListaPozycji[m_NumerPozycji] != null)
                && (m_ListaPozycji[m_NumerPozycji].IdDokumentu != null) && (m_ListaPozycji[m_NumerPozycji].Id != null))
            {
                if (MyWorkItem.UruchomRaport(NazwaFormularza, (long) m_ListaPozycji[m_NumerPozycji].IdDokumentu,
                                             (long) m_ListaPozycji[m_NumerPozycji].Id))
                {
                    MessageBox.Show(PomyslneWywolanieRaportu);
                }
            }
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    e.Handled = true;
                    View.FocusNextControl(false);
                    break;
                case Keys.Down:
                    e.Handled = true;
                    View.FocusNextControl(true);
                    break;
                case Keys.Left:
                    e.Handled = true;
                    ViewPrzejdzDoPoprzedniejPozycji(this, EventArgs.Empty);
                    break;
                case Keys.Right:
                    e.Handled = true;
                    ViewPrzejdzDoNastepnejPozycji(this, EventArgs.Empty);
                    break;
                case Keys.Enter:
                    e.Handled = true;
                    View.ForceDataBinding();
                    //zapewnia kontrole pozycji jezeli jest tylko 1 na dokumencie
                    UstawSkontrolowanaPozycje();
                    //jezeli jest wiecej pozycji przechodzi dalej
                    ViewPrzejdzDoNastepnejPozycji(this, EventArgs.Empty);
                    break;
                case Keys.D1:
                    e.Handled = true;
                    ViewSzukaj(this, EventArgs.Empty);
                    break;
                case Keys.D2:
                    e.Handled = true;
                    ViewRealizuj(this, EventArgs.Empty);
                    break;
                case Keys.D3:
                    e.Handled = true;
                    ViewZakoncz(this, EventArgs.Empty);
                    break;
                case Keys.D:
                    e.Handled = true;
                    UruchomRaport();
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    CloseView();
                    MyWorkItem.OdswiezListeMp();
                    MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
                    MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
                    MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
                    MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
                    MyWorkItem.RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] = null;
                    MyWorkItem.RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueYes;
                    break;
            }
        }

        public void ZakonczPozycje()
        {
            if ((m_ListaPozycji != null) && (m_ListaPozycji.Count > 0) &&
                (m_ListaPozycji[m_NumerPozycji].Id != null) &&
                (!SkontrolowanoWszystkiePozycje()))
            {
                var result = MessageBox.Show("Nie wszystkie pozycje zosta�y skontrolowane, czy na pewno chcesz wyj�� z dokumentu?", "Zako�cz", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                MyWorkItem.Activate();
                if (result == DialogResult.Yes)
                {
                    var listaMpWorkItem = MyWorkItem.Parent.WorkItems.Get<ListaMPWorkItem>(WorkItemsConstants.LISTA_MP_WORKITEM);
                    var listaMpViewPresenter = listaMpWorkItem.Items.Get<ListaMPViewPresenter>(ItemsConstants.LISTA_MP_PRESENTER);
                    listaMpWorkItem.Szukaj(listaMpViewPresenter.KryteriaZapytania);
                    MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
                    MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
                    MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
                    MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
                    MyWorkItem.RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueNo;
                    //WorkItemBase.MainWorkspace.Close(View);
                    MyWorkItem.RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] = null;
                    CloseView();
                }
            }
            else
            {
                var listaMpWorkItem = MyWorkItem.Parent.WorkItems.Get<ListaMPWorkItem>(WorkItemsConstants.LISTA_MP_WORKITEM);
                var listaMpViewPresenter = listaMpWorkItem.Items.Get<ListaMPViewPresenter>(ItemsConstants.LISTA_MP_PRESENTER);
                listaMpWorkItem.Szukaj(listaMpViewPresenter.KryteriaZapytania);
                MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
                MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
                MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
                MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
                MyWorkItem.RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueNo;
                WorkItemBase.MainWorkspace.Close(View);
                CloseView();
            }
        }

        public void PrzejdzDoNastepnejPozycji()
        {
            FunctionsHelper.ZnacznikZgodnosciKodowKreskowychSymbolIndeksu = null;

            if (MyWorkItem.ZmienIlosc(View.Pozycja.Id, MIloscZerowana, IloscOrg))
            {
                if (MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] != null)
                {
                    MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueYes;
                }

                View.Ilosc = MIloscZerowana;

                #region  /**** [0001] ****/

                UstawSkontrolowanaPozycje();
                MyWorkItem.RootWorkItem.WorkItems.Get<RealizacjaMPWorkItem>(WorkItemsConstants.REALIZACJA_MP_WORKITEM).UstawZakonczonaPozycje(FunctionsHelper.ConvertToLong(View.Pozycja.Id.ToString()));
                ViewPrzejdzDoNastepnejPozycji(null, null);
                #endregion
            }

            MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
            MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueYes;
        }

        [EventSubscription(EventBrokerConstants.WYJSCIE_Z_FORMATKI_K)]
        [EventSubscription(EventBrokerConstants.NAVIGATION_STATE_CHANGED)]
        public override void NavigationStateChanged(object sender, EventArgs e)
        {
            if (MyWorkItem.MainWorkspace.ActiveSmartPart.ToString() != this.View.ToString())
            {
                return;
            }

            base.NavigationStateChanged(sender, e);

            if (m_ListaPozycji.Count > 0)
            {
                Aktualizuj();
            }
            else
            {
                View.IloscReadOnly = NavigationState;
                View.WagaReadOnly = NavigationState;
            }
            //jezeli enter (edycja => nawigacja) to ustawSkontrolowana
            if (NavigationState &&
                (NavigationKeyArgs == null || NavigationKeyArgs.KeyCode != Keys.Escape))
            {
                UstawSkontrolowanaPozycje();
            }
        }

        public void WczytajMase(Masa masa)
        {
            if ((m_ListaPozycji != null) && (m_ListaPozycji.Count > 0) && (m_ListaPozycji[m_NumerPozycji].Id.HasValue))
            {
                if (m_ListaPozycji[m_NumerPozycji].Skontrolowana.Equals("T"))
                {
                    MessageBox.Show(ChangeWeightControlledItem);
                    MyWorkItem.Activate();
                    return;
                }

                if ((masa != null) && (masa.wartosc > 0) &&
                    ((m_ListaPozycji[m_NumerPozycji].Skontrolowana == null) || (m_ListaPozycji[m_NumerPozycji].Skontrolowana.Equals(DBBool.FALSE))))
                {
                    if (NavigationState)
                    {
                        View.SetNavigationState(false);
                        MyWorkItem.Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                        MyWorkItem.Activate();
                    }

                    View.WagaSetFocus();
                    View.UstawWage(masa.wartosc.ToString("0.000"));
                }
            }
        }
    }
}