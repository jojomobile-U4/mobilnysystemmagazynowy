#region

using Common.SearchForm;

#endregion

namespace DostawaModule.KontrolaSearch
{
    public partial class KontrolaMPSearchView : SearchForm, IKontrolaMPSearchView
    {
        public KontrolaMPSearchView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #region IKontrolaMPSearchView Members

        public KryteriaZapytaniaPozycjiMP KryteriaZapytania
        {
            get
            {
                var kryteriaZapytania = new KryteriaZapytaniaPozycjiMP();
                kryteriaZapytania.Skontrolowana = tbSkontrolowany.Text;
                return kryteriaZapytania;
            }
            set { DoUpdate(value); }
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);

            tbSkontrolowany.ReadOnly = navigationState;
        }

        #endregion

        private void DoUpdate(KryteriaZapytaniaPozycjiMP kryteriaZapytania)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new KryteriaZapytaniaDelegate(DoUpdate),
                            new object[] {kryteriaZapytania});
                return;
            }

            tbSkontrolowany.Text = kryteriaZapytania.Skontrolowana;
        }

        #region Nested type: KryteriaZapytaniaDelegate

        private delegate void KryteriaZapytaniaDelegate(KryteriaZapytaniaPozycjiMP kryteriaZapytania);

        #endregion
    }
}