using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using Microsoft.Practices.Mobile.CompositeUI;
using Common.SearchForm;


namespace DostawaModule.KontrolaSearch
{
    public class KontrolaMPSearchViewPresenter: SearchFormPresenter
    {
        KryteriaZapytaniaPozycjiMP ostatnieZapytanie = new KryteriaZapytaniaPozycjiMP();

        protected KontrolaMPWorkItem MyWorkItem
        {
            get { return WorkItem as KontrolaMPWorkItem; }
        }

		public IKontrolaMPSearchView View
		{
			get { return m_view as IKontrolaMPSearchView; }
		}

        public KontrolaMPSearchViewPresenter(IKontrolaMPSearchView view): base(view)
        {
        }

		protected override void OnViewUstawOstatnieZapytanie(object sender, EventArgs eventArgs)
		{
			base.OnViewUstawOstatnieZapytanie(sender, eventArgs);
			View.KryteriaZapytania = ostatnieZapytanie;
		}

        public void UstawKryteriaZapytania(KryteriaZapytaniaPozycjiMP kryteriaZapytania)
        {
            View.KryteriaZapytania = kryteriaZapytania;
        }

		protected override void OnViewSzukaj(object sender, EventArgs eventArgs)
		{
            ostatnieZapytanie = View.KryteriaZapytania;
			MyWorkItem.Szukaj(View.KryteriaZapytania);
            base.OnViewSzukaj(sender, eventArgs);
        }
    }
}
