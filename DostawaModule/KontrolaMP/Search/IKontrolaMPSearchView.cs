#region

using Common.SearchForm;

#endregion

namespace DostawaModule.KontrolaSearch
{
    public interface IKontrolaMPSearchView : ISearchForm
    {
        KryteriaZapytaniaPozycjiMP KryteriaZapytania { get; set; }
    }
}