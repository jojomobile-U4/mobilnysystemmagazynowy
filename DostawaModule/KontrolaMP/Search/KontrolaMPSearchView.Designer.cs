using Common.Components;

namespace DostawaModule.KontrolaSearch
{
    partial class KontrolaMPSearchView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbSkontrolowany = new System.Windows.Forms.TextBox();
            this.lbSkontrolowany = new System.Windows.Forms.Label();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // btnOstatnieZapytanie
            // 
            this.btnOstatnieZapytanie.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Location = new System.Drawing.Point(0, 252);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 5;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "KONTROLA MP - SZUKAJ";
            // 
            // tbSkontrolowany
            // 
            this.tbSkontrolowany.Location = new System.Drawing.Point(95, 36);
            this.tbSkontrolowany.Name = "tbSkontrolowany";
            this.tbSkontrolowany.Size = new System.Drawing.Size(142, 21);
            this.tbSkontrolowany.TabIndex = 4;
            // 
            // lbSkontrolowany
            // 
            this.lbSkontrolowany.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSkontrolowany.Location = new System.Drawing.Point(3, 37);
            this.lbSkontrolowany.Name = "lbSkontrolowany";
            this.lbSkontrolowany.Size = new System.Drawing.Size(90, 20);
            this.lbSkontrolowany.Text = "Skontrolowany:";
            // 
            // KontrolaMPSearchView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lbSkontrolowany);
            this.Controls.Add(this.tbSkontrolowany);
            this.Controls.Add(this.lbTytul);
            this.Name = "KontrolaMPSearchView";
            this.Size = new System.Drawing.Size(240, 280);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.tbSkontrolowany, 0);
            this.Controls.SetChildIndex(this.lbSkontrolowany, 0);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MSMLabel lbTytul;
        private System.Windows.Forms.TextBox tbSkontrolowany;
        private System.Windows.Forms.Label lbSkontrolowany;
    }
}
