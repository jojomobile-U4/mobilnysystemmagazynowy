#region

using System;
using Common;
using Common.Base;

#endregion

namespace DostawaModule
{
    public partial class KontrolaMPView : ViewBase, IKontrolaMPView
    {
        public KontrolaMPView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #region IKontrolaMPView Members

        public event EventHandler PrzejdzDoNastepnejPozycji;
        public event EventHandler PrzejdzDoPoprzedniejPozycji;
        public event EventHandler Szukaj;
        public event EventHandler Zakoncz;
        public event EventHandler Realizuj;

        public PozycjaMP Pozycja
        {
            set { pozycjaBindingSource.DataSource = value; }
            get { return pozycjaBindingSource.DataSource as PozycjaMP; }
        }

        public float? Ilosc
        {
            get
            {
                try
                {
                    return FunctionsHelper.ConvertToFloat(tbIlosc.Text);
                }
                catch (FormatException)
                {
                    return null;
                }
            }
            set { tbIlosc.Text = value == null ? 0.ToString() : value.ToString(); }
        }

        public string Skontrolowana
        {
            set { tbSkontrolowany.Text = value; }
        }

        public long Pozostalo
        {
            set { tbPozostalo.Text = value.ToString(); }
        }

        public bool IloscReadOnly
        {
            get { return tbIlosc.ReadOnly; }

            set { tbIlosc.ReadOnly = value; }
        }

        public bool WagaReadOnly
        {
            get { return tbWaga.ReadOnly; }

            set { tbWaga.ReadOnly = value; }
        }

        public void IloscSetFocus()
        {
            tbIlosc.Focus();
            tbIlosc.SelectAll();
        }

        public void WagaSetFocus()
        {
            tbWaga.Focus();
            tbWaga.SelectAll();
        }

        public string SymbolDokumentu
        {
            set { DoUpdate(value); }
            get { return tbDokument.Text; }
        }

        public double? Waga
        {
            get
            {
                try
                {
                    return double.Parse(tbWaga.Text);
                }
                catch (FormatException)
                {
                    return null;
                }
            }

            set { tbWaga.Text = value.ToString(); }
        }

        public void UstawWage(string waga)
        {
            tbWaga.Text = waga;
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);

            tbIlosc.ReadOnly = navigationState;
            tbWaga.ReadOnly = navigationState;
        }

        #endregion

        private void DoUpdate(string symbol)
        {
            if (InvokeRequired)
            {
                Invoke(new SymbolDokumentuDelegate(DoUpdate),
                            new object[] {symbol});
                return;
            }
            tbDokument.Text = symbol;
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            if (PrzejdzDoNastepnejPozycji != null)
            {
                PrzejdzDoNastepnejPozycji(this, null);
            }
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            if (PrzejdzDoPoprzedniejPozycji != null)
            {
                PrzejdzDoPoprzedniejPozycji(this, null);
            }
        }

        private void btnSzukaj_Click(object sender, EventArgs e)
        {
            if (Szukaj != null)
            {
                Szukaj(this, null);
            }
        }

        private void btnZakoncz_Click(object sender, EventArgs e)
        {
            if (Zakoncz != null)
            {
                Zakoncz(this, null);
            }
        }

        private void btnRealizuj_Click(object sender, EventArgs e)
        {
            if (Realizuj != null)
            {
                Realizuj(this, null);
            }
        }

        #region Nested type: SymbolDokumentuDelegate

        private delegate void SymbolDokumentuDelegate(string symbol);

        #endregion
    }
}