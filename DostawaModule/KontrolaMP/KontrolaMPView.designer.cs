
using Common.Components;

namespace DostawaModule
{
    partial class KontrolaMPView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbTytul = new Common.Components.MSMLabel();
            this.lbDokument = new System.Windows.Forms.Label();
            this.pozycjaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbDokument = new System.Windows.Forms.TextBox();
            this.lbPozycje = new System.Windows.Forms.Label();
            this.lbLP = new System.Windows.Forms.Label();
            this.tbLP = new System.Windows.Forms.TextBox();
            this.lbSlash = new System.Windows.Forms.Label();
            this.tbIloscLP = new System.Windows.Forms.TextBox();
            this.lbSymbolTowaru = new System.Windows.Forms.Label();
            this.tbSymbolTowaru = new System.Windows.Forms.TextBox();
            this.tbNazwaTowaru = new System.Windows.Forms.TextBox();
            this.lbNazwaTowaru = new System.Windows.Forms.Label();
            this.lbJM = new System.Windows.Forms.Label();
            this.tbJM = new System.Windows.Forms.TextBox();
            this.lbIlosc = new System.Windows.Forms.Label();
            this.tbIlosc = new System.Windows.Forms.TextBox();
            this.tbSkontrolowany = new System.Windows.Forms.TextBox();
            this.lbSkontrolowany = new System.Windows.Forms.Label();
            this.btnSzukaj = new System.Windows.Forms.Button();
            this.btnRealizuj = new System.Windows.Forms.Button();
            this.btnZakoncz = new System.Windows.Forms.Button();
            this.lrcNavigation = new Common.Components.LeftRightControl();
            this.lbPozostalo = new System.Windows.Forms.Label();
            this.tbPozostalo = new System.Windows.Forms.TextBox();
            this.tbWaga = new System.Windows.Forms.TextBox();
            this.lblWaga = new System.Windows.Forms.Label();
            this.lKg = new System.Windows.Forms.Label();
            this.pnlNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pozycjaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnRealizuj);
            this.pnlNavigation.Controls.Add(this.btnSzukaj);
            this.pnlNavigation.Controls.Add(this.btnZakoncz);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 272);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 28);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 33;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "PRZYJ�CIE - KONTROLA MP";
            // 
            // lbDokument
            // 
            this.lbDokument.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbDokument.Location = new System.Drawing.Point(3, 20);
            this.lbDokument.Name = "lbDokument";
            this.lbDokument.Size = new System.Drawing.Size(85, 20);
            this.lbDokument.Text = "Dokument:";
            // 
            // pozycjaBindingSource
            // 
            this.pozycjaBindingSource.DataSource = typeof(DostawaModule.PozycjaMP);
            // 
            // tbDokument
            // 
            this.tbDokument.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "SymbolDokumentu", true));
            this.tbDokument.Location = new System.Drawing.Point(88, 19);
            this.tbDokument.Name = "tbDokument";
            this.tbDokument.ReadOnly = true;
            this.tbDokument.Size = new System.Drawing.Size(149, 21);
            this.tbDokument.TabIndex = 0;
            this.tbDokument.TabStop = false;
            // 
            // lbPozycje
            // 
            this.lbPozycje.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbPozycje.Location = new System.Drawing.Point(0, 43);
            this.lbPozycje.Name = "lbPozycje";
            this.lbPozycje.Size = new System.Drawing.Size(240, 16);
            this.lbPozycje.Text = "POZYCJE";
            this.lbPozycje.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbLP
            // 
            this.lbLP.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbLP.Location = new System.Drawing.Point(3, 63);
            this.lbLP.Name = "lbLP";
            this.lbLP.Size = new System.Drawing.Size(85, 20);
            this.lbLP.Text = "LP/Liczba LP:";
            // 
            // tbLP
            // 
            this.tbLP.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "Lp", true));
            this.tbLP.Location = new System.Drawing.Point(88, 63);
            this.tbLP.Name = "tbLP";
            this.tbLP.ReadOnly = true;
            this.tbLP.Size = new System.Drawing.Size(53, 21);
            this.tbLP.TabIndex = 1;
            this.tbLP.TabStop = false;
            // 
            // lbSlash
            // 
            this.lbSlash.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSlash.Location = new System.Drawing.Point(144, 65);
            this.lbSlash.Name = "lbSlash";
            this.lbSlash.Size = new System.Drawing.Size(10, 18);
            this.lbSlash.Text = "/";
            // 
            // tbIloscLP
            // 
            this.tbIloscLP.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "IloscPozycjiNaDokumencie", true));
            this.tbIloscLP.Location = new System.Drawing.Point(154, 63);
            this.tbIloscLP.Name = "tbIloscLP";
            this.tbIloscLP.ReadOnly = true;
            this.tbIloscLP.Size = new System.Drawing.Size(53, 21);
            this.tbIloscLP.TabIndex = 2;
            this.tbIloscLP.TabStop = false;
            // 
            // lbSymbolTowaru
            // 
            this.lbSymbolTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbolTowaru.Location = new System.Drawing.Point(3, 90);
            this.lbSymbolTowaru.Name = "lbSymbolTowaru";
            this.lbSymbolTowaru.Size = new System.Drawing.Size(85, 20);
            this.lbSymbolTowaru.Text = "Indeks:";
            // 
            // tbSymbolTowaru
            // 
            this.tbSymbolTowaru.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "SymbolTowaru", true));
            this.tbSymbolTowaru.Location = new System.Drawing.Point(88, 89);
            this.tbSymbolTowaru.Name = "tbSymbolTowaru";
            this.tbSymbolTowaru.ReadOnly = true;
            this.tbSymbolTowaru.Size = new System.Drawing.Size(149, 21);
            this.tbSymbolTowaru.TabIndex = 3;
            this.tbSymbolTowaru.TabStop = false;
            // 
            // tbNazwaTowaru
            // 
            this.tbNazwaTowaru.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "NazwaTowaru", true));
            this.tbNazwaTowaru.Location = new System.Drawing.Point(88, 116);
            this.tbNazwaTowaru.Name = "tbNazwaTowaru";
            this.tbNazwaTowaru.ReadOnly = true;
            this.tbNazwaTowaru.Size = new System.Drawing.Size(149, 21);
            this.tbNazwaTowaru.TabIndex = 4;
            this.tbNazwaTowaru.TabStop = false;
            // 
            // lbNazwaTowaru
            // 
            this.lbNazwaTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbNazwaTowaru.Location = new System.Drawing.Point(3, 117);
            this.lbNazwaTowaru.Name = "lbNazwaTowaru";
            this.lbNazwaTowaru.Size = new System.Drawing.Size(85, 20);
            this.lbNazwaTowaru.Text = "Nazwa towaru:";
            // 
            // lbJM
            // 
            this.lbJM.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbJM.Location = new System.Drawing.Point(3, 144);
            this.lbJM.Name = "lbJM";
            this.lbJM.Size = new System.Drawing.Size(85, 20);
            this.lbJM.Text = "J.m.:";
            // 
            // tbJM
            // 
            this.tbJM.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "JednostkaMiary", true));
            this.tbJM.Location = new System.Drawing.Point(88, 143);
            this.tbJM.Name = "tbJM";
            this.tbJM.ReadOnly = true;
            this.tbJM.Size = new System.Drawing.Size(79, 21);
            this.tbJM.TabIndex = 5;
            this.tbJM.TabStop = false;
            // 
            // lbIlosc
            // 
            this.lbIlosc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbIlosc.Location = new System.Drawing.Point(3, 170);
            this.lbIlosc.Name = "lbIlosc";
            this.lbIlosc.Size = new System.Drawing.Size(85, 20);
            this.lbIlosc.Text = "Ilo��:";
            // 
            // tbIlosc
            // 
            this.tbIlosc.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "Ilosc", true));
            this.tbIlosc.Location = new System.Drawing.Point(88, 170);
            this.tbIlosc.Name = "tbIlosc";
            this.tbIlosc.Size = new System.Drawing.Size(79, 21);
            this.tbIlosc.TabIndex = 6;
            // 
            // tbSkontrolowany
            // 
            this.tbSkontrolowany.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "Skontrolowana", true));
            this.tbSkontrolowany.Location = new System.Drawing.Point(88, 221);
            this.tbSkontrolowany.Name = "tbSkontrolowany";
            this.tbSkontrolowany.ReadOnly = true;
            this.tbSkontrolowany.Size = new System.Drawing.Size(79, 21);
            this.tbSkontrolowany.TabIndex = 7;
            this.tbSkontrolowany.TabStop = false;
            // 
            // lbSkontrolowany
            // 
            this.lbSkontrolowany.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSkontrolowany.Location = new System.Drawing.Point(3, 222);
            this.lbSkontrolowany.Name = "lbSkontrolowany";
            this.lbSkontrolowany.Size = new System.Drawing.Size(85, 20);
            this.lbSkontrolowany.Text = "Skontrolowany:";
            // 
            // btnSzukaj
            // 
            this.btnSzukaj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnSzukaj.Location = new System.Drawing.Point(3, 4);
            this.btnSzukaj.Name = "btnSzukaj";
            this.btnSzukaj.Size = new System.Drawing.Size(72, 20);
            this.btnSzukaj.TabIndex = 8;
            this.btnSzukaj.TabStop = false;
            this.btnSzukaj.Text = "&1 Szukaj";
            this.btnSzukaj.Click += new System.EventHandler(this.btnSzukaj_Click);
            // 
            // btnRealizuj
            // 
            this.btnRealizuj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnRealizuj.Location = new System.Drawing.Point(84, 4);
            this.btnRealizuj.Name = "btnRealizuj";
            this.btnRealizuj.Size = new System.Drawing.Size(72, 20);
            this.btnRealizuj.TabIndex = 9;
            this.btnRealizuj.TabStop = false;
            this.btnRealizuj.Text = "&2 Realizuj";
            this.btnRealizuj.Click += new System.EventHandler(this.btnRealizuj_Click);
            // 
            // btnZakoncz
            // 
            this.btnZakoncz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnZakoncz.Location = new System.Drawing.Point(165, 4);
            this.btnZakoncz.Name = "btnZakoncz";
            this.btnZakoncz.Size = new System.Drawing.Size(72, 20);
            this.btnZakoncz.TabIndex = 10;
            this.btnZakoncz.TabStop = false;
            this.btnZakoncz.Text = "&3 Zako�cz";
            this.btnZakoncz.Click += new System.EventHandler(this.btnZakoncz_Click);
            // 
            // lrcNavigation
            // 
            this.lrcNavigation.BackColor = System.Drawing.SystemColors.Desktop;
            this.lrcNavigation.Location = new System.Drawing.Point(205, 0);
            this.lrcNavigation.Name = "lrcNavigation";
            this.lrcNavigation.NextEnabled = true;
            this.lrcNavigation.PreviousEnabled = true;
            this.lrcNavigation.Size = new System.Drawing.Size(32, 16);
            this.lrcNavigation.TabIndex = 23;
            this.lrcNavigation.TabStop = false;
            this.lrcNavigation.Next += new System.EventHandler(this.btnRight_Click);
            this.lrcNavigation.Previous += new System.EventHandler(this.btnLeft_Click);
            // 
            // lbPozostalo
            // 
            this.lbPozostalo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbPozostalo.Location = new System.Drawing.Point(3, 250);
            this.lbPozostalo.Name = "lbPozostalo";
            this.lbPozostalo.Size = new System.Drawing.Size(85, 20);
            this.lbPozostalo.Text = "Pozosta�o:";
            // 
            // tbPozostalo
            // 
            this.tbPozostalo.Location = new System.Drawing.Point(88, 248);
            this.tbPozostalo.Name = "tbPozostalo";
            this.tbPozostalo.ReadOnly = true;
            this.tbPozostalo.Size = new System.Drawing.Size(79, 21);
            this.tbPozostalo.TabIndex = 38;
            this.tbPozostalo.TabStop = false;
            // 
            // tbWaga
            // 
            this.tbWaga.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "Waga", true));
            this.tbWaga.Location = new System.Drawing.Point(88, 196);
            this.tbWaga.Name = "tbWaga";
            this.tbWaga.Size = new System.Drawing.Size(79, 21);
            this.tbWaga.TabIndex = 48;
            // 
            // lblWaga
            // 
            this.lblWaga.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWaga.Location = new System.Drawing.Point(2, 196);
            this.lblWaga.Name = "lblWaga";
            this.lblWaga.Size = new System.Drawing.Size(84, 21);
            this.lblWaga.Text = "Waga:";
            // 
            // lKg
            // 
            this.lKg.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lKg.Location = new System.Drawing.Point(170, 200);
            this.lKg.Name = "lKg";
            this.lKg.Size = new System.Drawing.Size(20, 16);
            this.lKg.Text = "Kg";
            // 
            // KontrolaMPView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lKg);
            this.Controls.Add(this.lblWaga);
            this.Controls.Add(this.tbWaga);
            this.Controls.Add(this.lbPozostalo);
            this.Controls.Add(this.tbPozostalo);
            this.Controls.Add(this.lrcNavigation);
            this.Controls.Add(this.lbSkontrolowany);
            this.Controls.Add(this.tbSkontrolowany);
            this.Controls.Add(this.tbIlosc);
            this.Controls.Add(this.lbIlosc);
            this.Controls.Add(this.tbJM);
            this.Controls.Add(this.lbJM);
            this.Controls.Add(this.lbNazwaTowaru);
            this.Controls.Add(this.tbNazwaTowaru);
            this.Controls.Add(this.tbSymbolTowaru);
            this.Controls.Add(this.lbSymbolTowaru);
            this.Controls.Add(this.tbIloscLP);
            this.Controls.Add(this.lbSlash);
            this.Controls.Add(this.tbLP);
            this.Controls.Add(this.lbLP);
            this.Controls.Add(this.lbPozycje);
            this.Controls.Add(this.tbDokument);
            this.Controls.Add(this.lbDokument);
            this.Controls.Add(this.lbTytul);
            this.Name = "KontrolaMPView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lbDokument, 0);
            this.Controls.SetChildIndex(this.tbDokument, 0);
            this.Controls.SetChildIndex(this.lbPozycje, 0);
            this.Controls.SetChildIndex(this.lbLP, 0);
            this.Controls.SetChildIndex(this.tbLP, 0);
            this.Controls.SetChildIndex(this.lbSlash, 0);
            this.Controls.SetChildIndex(this.tbIloscLP, 0);
            this.Controls.SetChildIndex(this.lbSymbolTowaru, 0);
            this.Controls.SetChildIndex(this.tbSymbolTowaru, 0);
            this.Controls.SetChildIndex(this.tbNazwaTowaru, 0);
            this.Controls.SetChildIndex(this.lbNazwaTowaru, 0);
            this.Controls.SetChildIndex(this.lbJM, 0);
            this.Controls.SetChildIndex(this.tbJM, 0);
            this.Controls.SetChildIndex(this.lbIlosc, 0);
            this.Controls.SetChildIndex(this.tbIlosc, 0);
            this.Controls.SetChildIndex(this.tbSkontrolowany, 0);
            this.Controls.SetChildIndex(this.lbSkontrolowany, 0);
            this.Controls.SetChildIndex(this.lrcNavigation, 0);
            this.Controls.SetChildIndex(this.tbPozostalo, 0);
            this.Controls.SetChildIndex(this.lbPozostalo, 0);
            this.Controls.SetChildIndex(this.tbWaga, 0);
            this.Controls.SetChildIndex(this.lblWaga, 0);
            this.Controls.SetChildIndex(this.lKg, 0);
            this.pnlNavigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pozycjaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MSMLabel lbTytul;
        private System.Windows.Forms.Label lbDokument;
        private System.Windows.Forms.TextBox tbDokument;
        private System.Windows.Forms.Label lbPozycje;
        private System.Windows.Forms.Label lbLP;
        private System.Windows.Forms.TextBox tbLP;
        private System.Windows.Forms.Label lbSlash;
        private System.Windows.Forms.TextBox tbIloscLP;
        private System.Windows.Forms.Label lbSymbolTowaru;
        private System.Windows.Forms.TextBox tbSymbolTowaru;
        private System.Windows.Forms.TextBox tbNazwaTowaru;
        private System.Windows.Forms.Label lbNazwaTowaru;
        private System.Windows.Forms.Label lbJM;
        private System.Windows.Forms.TextBox tbJM;
        private System.Windows.Forms.Label lbIlosc;
        private System.Windows.Forms.TextBox tbIlosc;
        private System.Windows.Forms.TextBox tbSkontrolowany;
        private System.Windows.Forms.Label lbSkontrolowany;
        private System.Windows.Forms.Button btnSzukaj;
        private System.Windows.Forms.Button btnRealizuj;
        private System.Windows.Forms.Button btnZakoncz;
        private System.Windows.Forms.BindingSource pozycjaBindingSource;
        private Common.Components.LeftRightControl lrcNavigation;
		private System.Windows.Forms.Label lbPozostalo;
		private System.Windows.Forms.TextBox tbPozostalo;
		private System.Windows.Forms.TextBox tbWaga;
		private System.Windows.Forms.Label lblWaga;
		private System.Windows.Forms.Label lKg;
    }
}
