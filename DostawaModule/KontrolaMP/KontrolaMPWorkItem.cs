#region

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace DostawaModule
{
    public class KontrolaMPWorkItem : WorkItemBase
    {
        

        #region Constants

        private const string KONTROLA_MP = "Kontrola MP";

        #endregion

        public void Show(IWorkspace parentWorkspace)
        {
            long? idDokumentuMp = long.Parse(Parent.State["IdDokumentu"].ToString());

            FunctionsHelper.SetCursorWait();

            RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
            RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
            RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
            RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
            RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueYes;

            var kontrolaMpViewPresenter = Items.Get<KontrolaMPViewPresenter>(ItemsConstants.KONTROLA_MP_PRESENTER);

            kontrolaMpViewPresenter.KryteriaZapytania.IdDokumentu = idDokumentuMp;
            RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] = kontrolaMpViewPresenter.View.ToString();
            Szukaj(kontrolaMpViewPresenter.KryteriaZapytania);

            parentWorkspace.Show(kontrolaMpViewPresenter.View);

            // Gdyby sie jednak zdecydowali tego uzywac to zostawiam - pod odbiorze mozna skasowac
            //if(MessageBox.Show("Czy chcesz skanowa� kod EAN ?", "Kody EAN", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            //{
            //    Activate();
            //    if (kontrolaMpViewPresenter.View.Pozycja != null)
            //    {
            //        RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueYes;
            //        RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = kontrolaMpViewPresenter.View.Pozycja.SymbolTowaru;
            //        Commands[CommandConstants.KodyKreskowe].Execute();
            //    }
            //}

            Activate();

            FunctionsHelper.SetCursorDefault();
        }

        public void Szukaj(KryteriaZapytaniaPozycjiMP kryteriaZapytania)
        {
            FunctionsHelper.SetCursorWait();

            try
            {
                var dokumentyMagazynoweService = Services.Get<IDokumentyMagazynoweService>(true);
                var listaPozycji =
                    dokumentyMagazynoweService.PobierzListePozycji(kryteriaZapytania);

                if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, KONTROLA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                }

                //�atka
                if (listaPozycji == null)
                    listaPozycji = new List<PozycjaMP>();
                if (listaPozycji.Count == 0)
                    listaPozycji.Add(new PozycjaMP());

                Items.Get<KontrolaMPViewPresenter>(ItemsConstants.KONTROLA_MP_PRESENTER).ZaladujDaneDoWidoku(listaPozycji);
            }
            catch (Exception e)
            {
                ShowMessageBox(e.Message, KONTROLA_MP);
            }
            FunctionsHelper.SetCursorDefault();
        }

        [System.Obsolete("Use : UstawSkontrolowanaPozycje(long id, float? ilosc, double? waga)")]
        public bool UstawSkontrolowanaPozycje(long id, float? ilosc, double? waga, float? iloscOrg )
        {
            FunctionsHelper.SetCursorWait();

            var dokumentyMagazynoweService = Services.Get<IDokumentyMagazynoweService>(true);
            dokumentyMagazynoweService.UstawSkontrolowanaPozycje(id, ilosc, waga, Common.FunctionsHelper.ConvertToFloat(iloscOrg.ToString()));
            if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, KONTROLA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                return false;
            }

            FunctionsHelper.SetCursorDefault();
            return true;
        }

        
        public bool UstawSkontrolowanaPozycje(long id, float? ilosc, double? waga)
        {
            FunctionsHelper.SetCursorWait();

            var dokumentyMagazynoweService = Services.Get<IDokumentyMagazynoweService>(true);
            dokumentyMagazynoweService.UstawSkontrolowanaPozycje(id, ilosc, waga);
            if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, KONTROLA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                return false;
            }

            FunctionsHelper.SetCursorDefault();
            return true;
        }
        public long IloscPozycjiNaDokumencie(long idDokumentu, String skontrolowana, String zakonczona)
        {
            FunctionsHelper.SetCursorWait();

            var dokumentyMagazynoweService = Services.Get<IDokumentyMagazynoweService>(true);
            var iloscPozycji = dokumentyMagazynoweService.IloscPozycjiNaDokumencie(idDokumentu, skontrolowana, zakonczona);
            if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, KONTROLA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                return -1;
            }

            FunctionsHelper.SetCursorDefault();
            return iloscPozycji;
        }

        public bool UruchomRaport(string nazwaFormularza, long idDokumentu, long idPozycji)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                Services.Get<IDokumentyMagazynoweService>(true).UruchomRaport(nazwaFormularza, idDokumentu, idPozycji);
                if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, KONTROLA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                    return false;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }

            return true;
        }

        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ, Thread = ThreadOption.UserInterface)]
        public void UstawNaPodstawieKoduKreskowego(object sender, EventArgs args)
        {
            var presenter = Items.Get<KontrolaMPViewPresenter>(ItemsConstants.KONTROLA_MP_PRESENTER);

            if (MainWorkspace.ActiveSmartPart.ToString() == presenter.View.ToString())

                try
                {
                    if (RootWorkItem.State[StateConstants.BAR_CODE] != null && MainWorkspace.ActiveSmartPart == presenter.View
                        //&& (string)RootWorkItem.State[StateConstants.AktywneCzytanieKodu] == StateConstants.BoolValueYes)
                        )
                    {
                        Activate();
                        var odczyt = RootWorkItem.State[StateConstants.BAR_CODE].ToString();
                        if ((odczyt != null) && (odczyt.IndexOf("WAG", 0) == 0))
                        {
                            FunctionsHelper.SetCursorWait();

                            var masa = Services.Get<IWagiService>(true).pobierzOdczytWagi(odczyt);

                            if (masa.statusOperacji.status.Equals(StatusOperacji.ERROR))
                            {
                                ShowMessageBox(masa.statusOperacji.tekst, BLAD, masa.statusOperacji.stosWywolan);
                                return;
                            }
                            else
                            {
                                Services.Get<IWagiService>(true).przeliczNaKg(ref masa);
                                presenter.WczytajMase(masa);
                            }
                        }
                        else
                        {
                            var kryteriaZapytania = new KryteriaZapytaniaPozycjiMP {NumerStrony = 0, Id = -1};

                            try
                            {
                                if (odczyt != null)
                                {
                                    kryteriaZapytania.Id = long.Parse(odczyt);
                                }
                                else
                                {
                                    kryteriaZapytania.Id = null;
                                }
                            }
                            catch (FormatException)
                            {
                                ShowMessageBox("Wczytana pozycja " + RootWorkItem.State[StateConstants.BAR_CODE] + " nie jest liczb�", "Szukaj");
                                Activate();
                            }
                            if (kryteriaZapytania.Id != -1)
                                Szukaj(kryteriaZapytania);
                        }
                    }
                }
                finally
                {
                    FunctionsHelper.SetCursorDefault();
                    Activate();
                }
        }

        public void OdswiezListeMp()
        {
            var listaWorkItem = RootWorkItem.WorkItems.Get<ListaMPWorkItem>(WorkItemsConstants.LISTA_MP_WORKITEM);
            listaWorkItem.OdswiezListeDokumentow();
        }

        public bool ZmienIlosc(long? id, float? ilosc)
        {
            FunctionsHelper.SetCursorWait();

            var rezult = Services.Get<IDokumentyMagazynoweService>(true).ZmienIlosc(id, ilosc);

            if (!rezult)
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, KONTROLA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
            }

            FunctionsHelper.SetCursorDefault();

            return rezult;
        }

        [System.Obsolete("Use : ZmienIlosc(long? id, float? ilosc)")]
        public bool ZmienIlosc(long? id, float? ilosc, float? iloscOrg)
        {
            FunctionsHelper.SetCursorWait();

            var rezult = Services.Get<IDokumentyMagazynoweService>(true).ZmienIlosc(id, ilosc, Common.FunctionsHelper.ConvertToFloat(iloscOrg.ToString()));

            if (!rezult)
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, KONTROLA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
            }

            FunctionsHelper.SetCursorDefault();

            return rezult;
        }
    }
}