#region

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.DataModel;
using Common.DbErrorHandling;
using DostawaModule.DostawaProxy;
using DostawaModule.ProxyWrappers;
using Microsoft.Practices.Mobile.CompositeUI;
using ListaDokumentow = DostawaModule.DostawaProxy.ListaDokumentow;
using Naglowek = DostawaModule.DostawaProxy.Naglowek;
using Nosnik = DostawaModule.DataModel.Nosnik;
using StatusOperacji = Common.DataModel.StatusOperacji;

#endregion

namespace DostawaModule
{
    [Service(typeof(IDokumentyMagazynoweService))]
    internal class DokumentyMagazynoweService : ServiceBase, IDokumentyMagazynoweService
    {
        #region Properties

        protected override WebServiceConfiguration Configuration
        {
            get { return MyWorkItem.Configuration.DostawaWS; }
        }

        #endregion

        #region Constructors

        public DokumentyMagazynoweService([ServiceDependency] WorkItem MyWorkItem)
            : base(MyWorkItem)
        {
        }

        #endregion

        #region Methods

        public List<Naglowek> PobierzListeNaglowkow(KryteriaZapytaniaListyMP kryteriaZapytania, out long iloscNaglowkow)
        {
            var lista = new List<Naglowek>();
            iloscNaglowkow = 0;

            try
            {
                using (var serviceAgent = new WsDostawaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzListeNaglowkowWrap
                                    {
                                        id = kryteriaZapytania.Id,
                                        idPozycji = kryteriaZapytania.IdPozycji,
                                        symbol = kryteriaZapytania.Symbol != null ? kryteriaZapytania.Symbol.ToUpper() : null,
                                        dataWystawienia = kryteriaZapytania.DataDokumentu,
                                        stan = kryteriaZapytania.Status != null ? kryteriaZapytania.Status.ToUpper() : null,
                                        skontrolowany = kryteriaZapytania.Skontrolowany != null ? kryteriaZapytania.Skontrolowany.ToUpper() : null,
                                        symbolDokumentuObrotowego = kryteriaZapytania.SymbolDokumentuObrotowego != null ? kryteriaZapytania.SymbolDokumentuObrotowego.ToUpper() : null,
                                        symbolDostawcy = kryteriaZapytania.SymbolDostawcy,
                                        idMagazynu = kryteriaZapytania.IdMagazynu,
                                        numerStrony = kryteriaZapytania.NumerStrony,
                                        wielkoscStrony = Configuration.WielkoscStrony
                                    };

                    var naglowki = serviceAgent.pobierzListeNaglowkow(param).@return;

                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = naglowki.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = naglowki.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = naglowki.statusOperacji.stosWywolan;

                    if (naglowki.statusOperacji.status.Equals(StatusOperacji.SUCCESS))
                    {
                        iloscNaglowkow = naglowki.iloscNaglowkow;

                        if ((naglowki.lista != null) && (naglowki.lista.Length > 0))
                        {
                            for (var i = 0; i < naglowki.lista.Length; i++)
                            {
                                lista.Add(naglowki.lista[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }

            return lista;
        }

        public List<PozycjaMP> PobierzListePozycji(KryteriaZapytaniaPozycjiMP kryteriaZapytania)
        {
            var lista = new List<PozycjaMP>();
            var listaDokumentow = new ListaDokumentow();

            try
            {
                using (var serviceAgent = new WsDostawaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzListeDokumentowWrap
                                    {
                                        id = kryteriaZapytania.Id,
                                        idDokumentu = kryteriaZapytania.IdDokumentu,
                                        numerStrony = kryteriaZapytania.NumerStrony,
                                        skontrolowana = kryteriaZapytania.Skontrolowana != null ? kryteriaZapytania.Skontrolowana.ToUpper() : null,
                                        zakonczona = kryteriaZapytania.Zakonczona != null ? kryteriaZapytania.Zakonczona.ToUpper() : null,
                                        wielkoscStrony = Configuration.WielkoscStrony
                                    };

                    listaDokumentow = serviceAgent.pobierzListeDokumentow(param).@return;

                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = listaDokumentow.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = listaDokumentow.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = listaDokumentow.statusOperacji.stosWywolan;

                    if ((listaDokumentow.lista != null) && (listaDokumentow.lista.Length > 0))
                    {
                        for (var i = 0; i < listaDokumentow.lista.Length; i++)
                        {
                            if ((listaDokumentow.lista[i].pozycje != null) && (listaDokumentow.lista[i].pozycje.Length > 0))
                            {
                                for (var j = 0; j < listaDokumentow.lista[i].pozycje.Length; j++)
                                {
                                    var pozycja = new PozycjaMP();
                                    pozycja.Id = listaDokumentow.lista[i].pozycje[j].id;
                                    pozycja.IdDokumentu = listaDokumentow.lista[i].pozycje[j].idDokumentu;
                                    pozycja.Ilosc = listaDokumentow.lista[i].pozycje[j].ilosc;
                                    pozycja.JednostkaMiary = listaDokumentow.lista[i].pozycje[j].jednostkaMiary;
                                    pozycja.KodLokalizacji = Localization.RemovePrefix(listaDokumentow.lista[i].pozycje[j].kodLokalizacji);
                                    pozycja.Lp = listaDokumentow.lista[i].pozycje[j].lp;
                                    pozycja.NazwaTowaru = listaDokumentow.lista[i].pozycje[j].nazwaTowaru;
                                    pozycja.Skontrolowana = listaDokumentow.lista[i].pozycje[j].skontrolowana;
                                    pozycja.SymbolTowaru = listaDokumentow.lista[i].pozycje[j].symbolTowaru;
                                    pozycja.Zakonczona = listaDokumentow.lista[i].pozycje[j].zakonczona;
                                    pozycja.SymbolDokumentu = listaDokumentow.lista[i].naglowek.symbol;
                                    pozycja.IloscPozycjiNaDokumencie = listaDokumentow.lista[i].iloscPozycji;
                                    pozycja.Waga = listaDokumentow.lista[i].pozycje[j].waga;
                                    pozycja.PainId = listaDokumentow.lista[i].pozycje[j].painId;
                                    lista.Add(pozycja);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
            return lista;
        }

        [System.Obsolete("Use : UstawSkontrolowanaPozycje(long id, float? ilosc, double? waga)")]
        public void UstawSkontrolowanaPozycje(long id, float? ilosc, double? waga, float iloscOrg)
        {
            var status = new DostawaProxy.StatusOperacji();
            try
            {
                if (iloscOrg != ilosc)
                {
                    var error = new DbError();
                    var dbeh = new DbErrorHandle();

                    MessageBox.Show("Ilo�� wprowadzona przez u�ytkownika [" + ilosc + "] r�ni si� od warto�c oryginalnej [" + iloscOrg + "].", "AUDYT", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                    {
                        error.Tekst = "DOKUMENT MP idDokumentu = " + id + " : Ilosc zosta�a zmieniona z [" + iloscOrg + "] na [" + ilosc + "]. Uzytkownik : " + MyWorkItem.State[StateConstants.USER];
                        error.StosWywolan = string.Empty;
                        dbeh.WriteToFile(error);
                    }
                }

                using (var serviceAgent = new WsDostawaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new skontrolujPozycjeWrap { id = id, ilosc = ilosc, waga = waga };
                    status = serviceAgent.skontrolujPozycje(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = status.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = status.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = status.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        public void UstawSkontrolowanaPozycje(long id, float? ilosc, double? waga)
        {
            var status = new DostawaProxy.StatusOperacji();
            try
            {
                MessageBox.Show("Wykonano metode UstawSkontrolowanaPozycje(long id, float? ilosc, double? waga)", "AUDYT", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                {
                    var error = new DbError();
                    var dbeh = new DbErrorHandle();

                    error.Tekst = "Wykonano metode UstawSkontrolowanaPozycje(long id, float? ilosc, double? waga). To nie ta metoda!";
                    dbeh.WriteToFile(error);
                }

                using (var serviceAgent = new WsDostawaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new skontrolujPozycjeWrap { id = id, ilosc = ilosc, waga = waga };
                    status = serviceAgent.skontrolujPozycje(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = status.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = status.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = status.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        public void RezerwujDokument(long id)
        {
            var status = new DostawaProxy.StatusOperacji();
            try
            {
                using (var serviceAgent = new WsDostawaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new rezerwujDokument { id = id };
                    status = serviceAgent.rezerwujDokument(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = status.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = status.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = status.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        public void UstawZakonczonaPozycje(long id)
        {
            var status = new DostawaProxy.StatusOperacji();
            try
            {
                using (var serviceAgent = new WsDostawaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new zakonczPozycje { id = id };
                    status = serviceAgent.zakonczPozycje(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = status.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = status.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = status.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        public List<PozycjaMP> DodajPozycje(long id, string lokalizacja, long? nosnId, float ilosc)
        {
            var listaDokumentow = new ListaDokumentow();

            var lista = new List<PozycjaMP>();
            try
            {
                using (var serviceAgent = new WsDostawaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new utworzNowaPozycjeWrap { id_zrd = id, kodLokalizacji = Localization.AddPrefix(lokalizacja), nosnId = nosnId, ilosc = ilosc };
                    listaDokumentow = serviceAgent.utworzNowaPozycje(param).@return;

                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = listaDokumentow.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = listaDokumentow.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = listaDokumentow.statusOperacji.stosWywolan;

                    if ((listaDokumentow.lista != null) && (listaDokumentow.lista.Length > 0))
                    {
                        for (var i = 0; i < listaDokumentow.lista.Length; i++)
                        {
                            if ((listaDokumentow.lista[i].pozycje != null) && (listaDokumentow.lista[i].pozycje.Length > 0))
                            {
                                for (var j = 0; j < listaDokumentow.lista[i].pozycje.Length; j++)
                                {
                                    var pozycja = new PozycjaMP();
                                    pozycja.Id = listaDokumentow.lista[i].pozycje[j].id;
                                    pozycja.IdDokumentu = listaDokumentow.lista[i].pozycje[j].idDokumentu;
                                    pozycja.Ilosc = listaDokumentow.lista[i].pozycje[j].ilosc;
                                    pozycja.JednostkaMiary = listaDokumentow.lista[i].pozycje[j].jednostkaMiary;
                                    pozycja.KodLokalizacji = Localization.RemovePrefix(listaDokumentow.lista[i].pozycje[j].kodLokalizacji);
                                    pozycja.Lp = listaDokumentow.lista[i].pozycje[j].lp;
                                    pozycja.NazwaTowaru = listaDokumentow.lista[i].pozycje[j].nazwaTowaru;
                                    pozycja.Skontrolowana = listaDokumentow.lista[i].pozycje[j].skontrolowana;
                                    pozycja.SymbolTowaru = listaDokumentow.lista[i].pozycje[j].symbolTowaru;
                                    pozycja.Zakonczona = listaDokumentow.lista[i].pozycje[j].zakonczona;
                                    pozycja.SymbolDokumentu = listaDokumentow.lista[i].naglowek.symbol;
                                    lista.Add(pozycja);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }

            return lista;
        }

        public void ZmienLokalizacje(long id, string lokalizacja, long? nosnId)
        {
            DostawaProxy.StatusOperacji status;
            try
            {
                using (var serviceAgent = new WsDostawaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new zmienLokalizacjeWrap { id = id, kodLokalizacji = Localization.AddPrefix(lokalizacja), nosnId =  nosnId};
                    status = serviceAgent.zmienLokalizacje(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = status.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = status.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = status.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        public float PobierzIloscSkontrolowana(long id)
        {
            try
            {
                using (var serviceAgent = new WsDostawaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new iloscSkontrolowana { idPozycji = id };
                    var iloscSkontrolowana = serviceAgent.iloscSkontrolowana(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = iloscSkontrolowana.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = iloscSkontrolowana.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = iloscSkontrolowana.statusOperacji.stosWywolan;
                    return iloscSkontrolowana.ilosc;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
                return -1;
            }
        }

        public long IloscPozycjiNaDokumencie(long idDokumentu, String skontrolowana, String zakonczona)
        {
            try
            {
                using (var serviceAgent = new WsDostawaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new liczbaPozycjiNaDokumencieWrap { idDokumentu = idDokumentu, skontrolowana = skontrolowana, zakonczona = zakonczona };
                    var liczbaPozycji = serviceAgent.liczbaPozycjiNaDokumencie(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = liczbaPozycji.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = liczbaPozycji.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = liczbaPozycji.statusOperacji.stosWywolan;
                    return liczbaPozycji.liczba;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
                return -1;
            }
        }

        public void UruchomRaport(string nazwaFormularza, long idDokumentu, long idPozycji)
        {
            try
            {
                using (var serviceAgent = new WsDostawaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new uruchomRaportWrap { nazwaFormularza = nazwaFormularza, idDokumentuMR = idDokumentu, idPozycji = idPozycji };

                    var statusOperacji = serviceAgent.uruchomRaport(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = statusOperacji.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        public bool MoznaJednoznacznieOkreslicNosnik(long? painId, string lokalizacja, float? ilosc)
        {
            try
            {
                using (var serviceAgent = new WsDostawaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new moznaJednoznacznieOkreslicNosnik { painId = painId, lokalizacja = lokalizacja, ilosc = ilosc };

                    var moznaOkreslicNsnik = serviceAgent.moznaJednoznacznieOkreslicNosnik(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = moznaOkreslicNsnik.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = moznaOkreslicNsnik.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = moznaOkreslicNsnik.statusOperacji.stosWywolan;

                    if ((moznaOkreslicNsnik.wartosc != null) && (moznaOkreslicNsnik.wartosc.Equals(BoolConstants.TAK)))
                        return true;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }

            return false;
        }

        public List<Nosnik> PobierzListeDostepnychNosnikow(long? painId, string kodLokalizacji, float? ilosc)
        {
            try
            {
                using (var serviceAgent = new WsDostawaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzListeDostepnychNosnDlaPainWMijs { painId = painId, ilosc = ilosc, lokalizacja = kodLokalizacji };

                    var listaNosnikowWs = serviceAgent.pobierzListeDostepnychNosnDlaPainWMijs(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = listaNosnikowWs.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = listaNosnikowWs.statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = listaNosnikowWs.statusOperacji.stosWywolan;

                    var listaNosnikow = new List<Nosnik>();

                    if (listaNosnikowWs.lista != null)
                    {
                        foreach (var nosnik in listaNosnikowWs.lista)
                        {
                            listaNosnikow.Add(new Nosnik(nosnik.id, nosnik.symbol));
                        }
                    }

                    return listaNosnikow;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }

            return null;
        }

        public bool ZmienIlosc(long? id, float? ilosc)
        {
            DostawaProxy.StatusOperacji status;
            try
            {
                using (var serviceAgent = new WsDostawaWrap(Configuration))
                {
                    var error = new DbError();
                    var dbeh = new DbErrorHandle();

                    MessageBox.Show("Wykonano metode ZmienIlosc(long? id, float? ilosc).", "AUDYT", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                    {
                        error.Tekst = "Wykonano metode ZmienIlosc(long? id, float? ilosc). To nie ta metoda!";
                        dbeh.WriteToFile(error);
                    }

                    

                    SecureSoapMessage(serviceAgent);
                    var param = new zmienIlosc
                                    {
                                        id = id,
                                        ilosc = ilosc
                                    };

                    status = serviceAgent.zmienIlosc(param).@return;

                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = status.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = status.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = status.stosWywolan;
                    return true;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
            return false;
        }


        public bool ZmienIlosc(long? id, float? ilosc, float iloscOrg)
        {
            DostawaProxy.StatusOperacji status;
            try
            {
                using (var serviceAgent = new WsDostawaWrap(Configuration))
                {
                    if (iloscOrg != ilosc)
                    {
                        var error = new DbError();
                        var dbeh = new DbErrorHandle();

                        MessageBox.Show("Ilo�� wprowadzona przez u�ytkownika [" + ilosc + "] r�ni si� od warto�c oryginalnej [" + iloscOrg + "].", "AUDYT", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                        {
                            error.Tekst = "DOKUMENT MP /zmiana ilosci/ idDokumentu = " + id + " : Ilosc zosta�a zmieniona z [" + iloscOrg + "] na [" + ilosc + "]. Uzytkownik : " + MyWorkItem.State[StateConstants.USER];
                            error.StosWywolan = string.Empty;
                            dbeh.WriteToFile(error);
                        }
                    }

                    SecureSoapMessage(serviceAgent);
                    var param = new zmienIlosc
                    {
                        id = id,
                        ilosc = ilosc
                    };

                    status = serviceAgent.zmienIlosc(param).@return;

                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = status.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = status.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = status.stosWywolan;
                    return true;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
            return false;
        }


        #endregion
    }
}