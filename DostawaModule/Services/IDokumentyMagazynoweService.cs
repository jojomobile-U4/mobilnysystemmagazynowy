using System;
using System.Collections.Generic;
using DostawaModule.DostawaProxy;

namespace DostawaModule
{
    public interface IDokumentyMagazynoweService
    {
        List<Naglowek> PobierzListeNaglowkow(KryteriaZapytaniaListyMP kryteriaZapytania, out long iloscNaglowkow);
		List<PozycjaMP> PobierzListePozycji(KryteriaZapytaniaPozycjiMP kryteriaZapytania);
        void UstawSkontrolowanaPozycje(long id, float? ilosc, double? waga);
        void UstawSkontrolowanaPozycje(long id, float? ilosc, double? waga, float iloscOrg);
        void RezerwujDokument(long id);
        List<PozycjaMP> DodajPozycje(long id, string lokalizacja, long? nosnId, float ilosc);
        void UstawZakonczonaPozycje(long id);
        float PobierzIloscSkontrolowana(long id);
        void ZmienLokalizacje(long id, string lokalizacja, long? nosnId);
        long IloscPozycjiNaDokumencie(long idDokumentu, String skontrolowana, String zakonczona);
        void UruchomRaport(string nazwaFormularza, long idDokumentu, long idPozycji);
        bool MoznaJednoznacznieOkreslicNosnik(long? painId, string lokalizacja, float? ilosc);
        List<DataModel.Nosnik> PobierzListeDostepnychNosnikow(long? painId, string kodLokalizacji, float? ilosc);
        bool ZmienIlosc(long? id, float? ilosc);
        bool ZmienIlosc(long? id, float? ilosc, float iloscOrg);

    }
}
