namespace DostawaModule
{
    public class PozycjaMP
    {
        private long? id;
        private long? idDokumentu;
        private float? ilosc;
        private string jednostkaMiary;
        private string kodLokalizacji;
        private int? lp;
        private string nazwaTowaru;
        private string skontrolowana;
        private string symbolTowaru;
        private string zakonczona;
        private string symbolDokumentu;
        private long iloscPozycjiNaDokumencie;
		private double? waga;
        private long? painId;

        public long? Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public long? IdDokumentu
        {
            get
            {
                return idDokumentu;
            }
            set
            {
                idDokumentu = value;
            }
        }

        public float? Ilosc
        {
            get
            {
                return ilosc;
            }
            set
            {
                ilosc = value;
            }
        }

        public string JednostkaMiary
        {
            get
            {
                return jednostkaMiary;
            }
            set
            {
                jednostkaMiary = value;
            }
        }

        public string KodLokalizacji
        {
            get
            {
                return kodLokalizacji;
            }
            set
            {
                kodLokalizacji = value;
            }
        }

        public int? Lp
        {
            get
            {
                return lp;
            }
            set
            {
                lp = value;
            }
        }

        public string NazwaTowaru
        {
            get
            {
                return nazwaTowaru;
            }
            set
            {
                nazwaTowaru = value;
            }
        }

        public string Skontrolowana
        {
            get
            {
                return skontrolowana;
            }
            set
            {
                skontrolowana = value;
            }
        }

        public string SymbolTowaru
        {
            get
            {
                return symbolTowaru;
            }
            set
            {
                symbolTowaru = value;
            }
        }

        public string Zakonczona
        {
            get
            {
                return zakonczona;
            }
            set
            {
                zakonczona = value;
            }
        }

        public string SymbolDokumentu
        {
            get
            {
                return symbolDokumentu;
            }
            set
            {
                symbolDokumentu = value;
            }
        }

        public long IloscPozycjiNaDokumencie
        {
            get
            {
                return iloscPozycjiNaDokumencie;
            }
            set
            {
                iloscPozycjiNaDokumencie = value;
            }
        }

		public double? Waga
		{
			get
			{
				return waga;
			}
			set
			{
				waga = value;
			}
		}

        public long? PainId
        {
            get
            {
                return painId;
            }
            set
            {
                painId = value;
            }
        }

    }
}
