#region

using System;
using System.Net;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using Common;
using Common.Base;
using DostawaModule.DostawaProxy;
using OpenNETCF.Web.Services2;

#endregion

namespace DostawaModule.ProxyWrappers
{
    public class WsDostawaWrap : WsDostawa, IWSSecurity
    {
        #region Private s

        #endregion

        #region Properties

        public SecurityHeader SecurityHeader { get; set; }

        #endregion

        #region Constructors

        public WsDostawaWrap(WebServiceConfiguration configuration)
        {
            Url = configuration.Url;
        }

        #endregion

        #region Protected Methods

        protected override WebRequest GetWebRequest(Uri uri)
        {
            var request = (HttpWebRequest) base.GetWebRequest(uri);

            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;

            return request;
        }

        #endregion

        #region Methods

        [SoapDocumentMethod("urn:zatwierdzDokumentObrotowy", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("zatwierdzDokumentObrotowyResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public zatwierdzDokumentObrotowyResponse zatwierdzDokumentObrotowy([XmlElement("zatwierdzDokumentObrotowy", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] zatwierdzDokumentObrotowy zatwierdzDokumentObrotowy1)
        {
            var results = Invoke("zatwierdzDokumentObrotowy", new object[]{zatwierdzDokumentObrotowy1});
            return ((zatwierdzDokumentObrotowyResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:pobierzListeNaglowkow", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeNaglowkowResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeNaglowkowResponse pobierzListeNaglowkow([XmlElement("pobierzListeNaglowkow", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeNaglowkowWrap pobierzListeNaglowkow1)
        {
            var results = Invoke("pobierzListeNaglowkow", new object[]{pobierzListeNaglowkow1});
            return ((pobierzListeNaglowkowResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:utworzNowaPozycje", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("utworzNowaPozycjeResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public utworzNowaPozycjeResponse utworzNowaPozycje([XmlElement("utworzNowaPozycje", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] utworzNowaPozycjeWrap utworzNowaPozycje1)
        {
            var results = Invoke("utworzNowaPozycje", new object[] {utworzNowaPozycje1});
            return ((utworzNowaPozycjeResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:skontrolujPozycje", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("skontrolujPozycjeResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public skontrolujPozycjeResponse skontrolujPozycje([XmlElement("skontrolujPozycje", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] skontrolujPozycjeWrap skontrolujPozycje1)
        {
            var results = Invoke("skontrolujPozycje", new object[] {skontrolujPozycje1});
            return ((skontrolujPozycjeResponse) (results[0]));
        }


        [SoapDocumentMethod("urn:rezerwujDokument", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("rezerwujDokumentResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public rezerwujDokumentResponse rezerwujDokument([XmlElement("rezerwujDokument", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] rezerwujDokument rezerwujDokument1)
        {
            var results = Invoke("rezerwujDokument", new object[]{rezerwujDokument1});
            return ((rezerwujDokumentResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:pobierzListeDokumentow", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeDokumentowResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeDokumentowResponse pobierzListeDokumentow([XmlElement("pobierzListeDokumentow", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeDokumentowWrap pobierzListeDokumentow1)
        {
            var results = Invoke("pobierzListeDokumentow", new object[] {pobierzListeDokumentow1});
            return ((pobierzListeDokumentowResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:zmienLokalizacje", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("zmienLokalizacjeResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public zmienLokalizacjeResponse zmienLokalizacje([XmlElement("zmienLokalizacje", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] zmienLokalizacjeWrap zmienLokalizacje1)
        {
            var results = Invoke("zmienLokalizacje", new object[] {zmienLokalizacje1});
            return ((zmienLokalizacjeResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:zakonczPozycje", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("zakonczPozycjeResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public zakonczPozycjeResponse zakonczPozycje([XmlElement("zakonczPozycje", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] zakonczPozycje zakonczPozycje1)
        {
            var results = Invoke("zakonczPozycje", new object[] {zakonczPozycje1});
            return ((zakonczPozycjeResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:iloscSkontrolowana", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("iloscSkontrolowanaResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public iloscSkontrolowanaResponse iloscSkontrolowana([XmlElement("iloscSkontrolowana", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] iloscSkontrolowana iloscSkontrolowana1)
        {
            var results = Invoke("iloscSkontrolowana", new object[] {iloscSkontrolowana1});
            return ((iloscSkontrolowanaResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:liczbaPozycjiNaDokumencie", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("liczbaPozycjiNaDokumencieResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public liczbaPozycjiNaDokumencieResponse liczbaPozycjiNaDokumencie([XmlElement("liczbaPozycjiNaDokumencie", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] liczbaPozycjiNaDokumencie liczbaPozycjiNaDokumencie1)
        {
            var results = Invoke("liczbaPozycjiNaDokumencie", new object[] {liczbaPozycjiNaDokumencie1});
            return ((liczbaPozycjiNaDokumencieResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:uruchomRaport", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("uruchomRaportResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public uruchomRaportResponse uruchomRaport([XmlElement("uruchomRaport", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] uruchomRaportWrap uruchomRaport1)
        {
            var results = Invoke("uruchomRaport", new object[]{uruchomRaport1});
            return ((uruchomRaportResponse) (results[0]));
        }

        [SoapDocumentMethodAttribute("urn:moznaJednoznacznieOkreslicNosnik", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElementAttribute("moznaJednoznacznieOkreslicNosnikResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public moznaJednoznacznieOkreslicNosnikResponse moznaJednoznacznieOkreslicNosnik([XmlElementAttribute("moznaJednoznacznieOkreslicNosnik", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] moznaJednoznacznieOkreslicNosnik moznaJednoznacznieOkreslicNosnik1)
        {
            var results = Invoke("moznaJednoznacznieOkreslicNosnik", new object[] {
                        moznaJednoznacznieOkreslicNosnik1});
            return ((moznaJednoznacznieOkreslicNosnikResponse)(results[0]));
        }


        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("urn:pobierzListeDostepnychNosnDlaPainWMijs", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("pobierzListeDostepnychNosnDlaPainWMijsResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeDostepnychNosnDlaPainWMijsResponse pobierzListeDostepnychNosnDlaPainWMijs([System.Xml.Serialization.XmlElementAttribute("pobierzListeDostepnychNosnDlaPainWMijs", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeDostepnychNosnDlaPainWMijs pobierzListeDostepnychNosnDlaPainWMijs1)
        {
            object[] results = this.Invoke("pobierzListeDostepnychNosnDlaPainWMijs", new object[] {
                        pobierzListeDostepnychNosnDlaPainWMijs1});
            return ((pobierzListeDostepnychNosnDlaPainWMijsResponse)(results[0]));
        }

        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("urn:zmienIlosc", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("zmienIloscResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public zmienIloscResponse zmienIlosc([System.Xml.Serialization.XmlElementAttribute("zmienIlosc", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] zmienIlosc zmienIlosc1)
        {
            object[] results = Invoke("zmienIlosc", new object[] {zmienIlosc1});
            return ((zmienIloscResponse)(results[0]));
        }

        #endregion
    }

    public class pobierzListeDokumentowWrap : pobierzListeDokumentow
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? id
        {
            get { return base.id; }
            set { base.id = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idDokumentu
        {
            get { return base.idDokumentu; }
            set { base.idDokumentu = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string skontrolowana
        {
            get { return base.skontrolowana; }
            set { base.skontrolowana = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string zakonczona
        {
            get { return base.zakonczona; }
            set { base.zakonczona = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? numerStrony
        {
            get { return base.numerStrony; }
            set { base.numerStrony = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? wielkoscStrony
        {
            get { return base.wielkoscStrony; }
            set { base.wielkoscStrony = value; }
        }
    }

    public class pobierzListeNaglowkowWrap : pobierzListeNaglowkow
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? id
        {
            get { return base.id; }
            set { base.id = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idPozycji
        {
            get { return base.idPozycji; }
            set { base.idPozycji = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string symbol
        {
            get { return base.symbol; }
            set { base.symbol = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public DateTime? dataWystawienia
        {
            get { return base.dataWystawienia; }
            set { base.dataWystawienia = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string stan
        {
            get { return base.stan; }
            set { base.stan = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string skontrolowany
        {
            get { return base.skontrolowany; }
            set { base.skontrolowany = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string symbolDokumentuObrotowego
        {
            get { return base.symbolDokumentuObrotowego; }
            set { base.symbolDokumentuObrotowego = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string symbolDostawcy
        {
            get { return base.symbolDostawcy; }
            set { base.symbolDostawcy = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string nazwaDostawcy
        {
            get { return base.nazwaDostawcy; }
            set { base.nazwaDostawcy = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idMagazynu
        {
            get { return base.idMagazynu; }
            set { base.idMagazynu = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? numerStrony
        {
            get { return base.numerStrony; }
            set { base.numerStrony = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? wielkoscStrony
        {
            get { return base.wielkoscStrony; }
            set { base.wielkoscStrony = value; }
        }
    }

    public class skontrolujPozycjeWrap : skontrolujPozycje
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? id
        {
            get { return base.id; }
            set { base.id = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public float? ilosc
        {
            get { return base.ilosc; }
            set { base.ilosc = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public double? waga
        {
            get { return base.waga; }
            set { base.waga = value; }
        }
    }

    public class utworzNowaPozycjeWrap : utworzNowaPozycje
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? id_zrd
        {
            get { return base.id_zrd; }
            set { base.id_zrd = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string kodLokalizacji
        {
            get { return base.kodLokalizacji; }
            set { base.kodLokalizacji = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? nosnId
        {
            get { return base.nosnId; }
            set { base.nosnId = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public float? ilosc
        {
            get { return base.ilosc; }
            set { base.ilosc = value; }
        }
    }

    public class zmienLokalizacjeWrap : zmienLokalizacje
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? id
        {
            get { return base.id; }
            set { base.id = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string kodLokalizacji
        {
            get { return base.kodLokalizacji; }
            set { base.kodLokalizacji = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? nosnId
        {
            get { return base.nosnId; }
            set { base.nosnId = value; }
        }
    }

    public class liczbaPozycjiNaDokumencieWrap : liczbaPozycjiNaDokumencie
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idDokumentu
        {
            get { return base.idDokumentu; }
            set { base.idDokumentu = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string skontrolowana
        {
            get { return base.skontrolowana; }
            set { base.skontrolowana = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string zakonczona
        {
            get { return base.zakonczona; }
            set { base.zakonczona = value; }
        }
    }

    public class uruchomRaportWrap : uruchomRaport
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string nazwaFormularza
        {
            get { return base.nazwaFormularza; }
            set { base.nazwaFormularza = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idDokumentuMR
        {
            get { return base.idDokumentuMR; }
            set { base.idDokumentuMR = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idPozycji
        {
            get { return base.idPozycji; }
            set { base.idPozycji = value; }
        }
    }

}