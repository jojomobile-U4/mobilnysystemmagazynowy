#region

using System;
using Common;
using Common.DataModel;
using DostawaModule.KontrolaSearch;
using DostawaModule.ListaSearch;
using DostawaModule.RealizacjaSearch;
using DostawaModule.RealizacjaMP.ListaNosnikow;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.Commands;

#endregion

namespace DostawaModule
{
    public class DostawaModuleInit : ModuleInit
    {
        private readonly WorkItem m_WorkItem;

        public DostawaModuleInit([ServiceDependency] WorkItem workItem)
        {
            m_WorkItem = workItem;
        }

        public override void Load()
        {
            base.Load();
            m_WorkItem.State["IdDokumentu"] = -1;
            m_WorkItem.State["odczytKoduKreskowego"] = DBBool.FALSE;
            m_WorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.SUCCESS;
            m_WorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = null;
        }

        private void Initialize()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                if (!m_WorkItem.WorkItems.Contains(WorkItemsConstants.LISTA_MP_WORKITEM))
                {
                    //Lista
                    var listaMpWorkItem = m_WorkItem.WorkItems.AddNew<ListaMPWorkItem>(WorkItemsConstants.LISTA_MP_WORKITEM);
                    var listaMpViewPresenter = new ListaMPViewPresenter(listaMpWorkItem.Items.AddNew<ListaMPView>());

                    listaMpWorkItem.Items.Add(listaMpViewPresenter, ItemsConstants.LISTA_MP_PRESENTER);

                    var listaMpSearchViewPresenter =
                        new ListaMPSearchViewPresenter(listaMpWorkItem.Items.AddNew<ListaMPSearchView>());
                    listaMpWorkItem.Items.Add(listaMpSearchViewPresenter, ItemsConstants.LISTA_MP_SEARCH_PRESENTER);

                    //Kontrola
                    var kontrolaMpWorkItem = m_WorkItem.WorkItems.AddNew<KontrolaMPWorkItem>(WorkItemsConstants.KONTROLA_MP_WORKITEM);

                    var kontrolaMpViewPresenter = new KontrolaMPViewPresenter(kontrolaMpWorkItem.Items.AddNew<KontrolaMPView>());
                    kontrolaMpWorkItem.Items.Add(kontrolaMpViewPresenter, ItemsConstants.KONTROLA_MP_PRESENTER);

                    var kontrolaMpSearchViewPresenter =
                        new KontrolaMPSearchViewPresenter(kontrolaMpWorkItem.Items.AddNew<KontrolaMPSearchView>());
                    kontrolaMpWorkItem.Items.Add(kontrolaMpSearchViewPresenter, ItemsConstants.KONTROLA_MP_SEARCH_PRESENTER);

                    //Realizacja
                    var realizacjaMpWorkItem = m_WorkItem.WorkItems.AddNew<RealizacjaMPWorkItem>(WorkItemsConstants.REALIZACJA_MP_WORKITEM);

                    var realizacjaMpViewPresenter = new RealizacjaMPViewPresenter(realizacjaMpWorkItem.Items.AddNew<RealizacjaMPView>());
                    realizacjaMpWorkItem.Items.Add(realizacjaMpViewPresenter, ItemsConstants.REALIZACJA_MP_PRESENTER);

                    var realizacjaMpSearchViewPresenter =
                        new RealizacjaMPSearchViewPresenter(realizacjaMpWorkItem.Items.AddNew<RealizacjaMPSearchView>());
                    realizacjaMpWorkItem.Items.Add(realizacjaMpSearchViewPresenter, ItemsConstants.REALIZACJA_MP_SEARCH_PRESENTER);

                    var nowaPozycjaMpViewPresenter = new NowaPozycjaMPViewPresenter(realizacjaMpWorkItem.Items.AddNew<NowaPozycjaMPView>());
                    realizacjaMpWorkItem.Items.Add(nowaPozycjaMpViewPresenter, ItemsConstants.NOWA_POZYCJA_MP_PRESENTER);

                    var listaNosnikowPresener = new ListaNosnikowPresenter(realizacjaMpWorkItem.Items.AddNew<ListaNosnikowView>());
                    realizacjaMpWorkItem.Items.Add(listaNosnikowPresener, ItemsConstants.REALIZACJA_MR_LISTA_NOSNIKOW_PRESENTER);
                }

                //Service
                if (!m_WorkItem.Services.Contains(typeof (IDokumentyMagazynoweService)))
                {
                    m_WorkItem.Services.AddNew(typeof (DokumentyMagazynoweService), typeof (IDokumentyMagazynoweService));
                }
                if (!m_WorkItem.Services.Contains(typeof (IWagiService)))
                {
                    m_WorkItem.Services.AddNew(typeof (WagiService), typeof (IWagiService));
                }

                m_WorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        [CommandHandler(CommandConstants.LISTA_MP)]
        public void ListaMp(object sender, EventArgs e)
        {
            Initialize();
            var mainWorkspace = m_WorkItem.Workspaces[WorkspacesConstants.MAIN_WORKSPACE];
            var listaMpWorkItem = m_WorkItem.WorkItems.Get<ListaMPWorkItem>(WorkItemsConstants.LISTA_MP_WORKITEM);
            listaMpWorkItem.Show(mainWorkspace);
        }

        [CommandHandler(CommandConstants.REALIZACJA_MP)]
        public void RealizacjaMp(object sender, EventArgs e)
        {
            Initialize();
            var mainWorkspace = m_WorkItem.Workspaces[WorkspacesConstants.MAIN_WORKSPACE];
            var realizacjaMpWorkItem = m_WorkItem.WorkItems.Get<RealizacjaMPWorkItem>(WorkItemsConstants.REALIZACJA_MP_WORKITEM);
            realizacjaMpWorkItem.Show(mainWorkspace);
        }

        [CommandHandler(CommandConstants.KONTROLA_MP)]
        public void KontrolaMp(object sender, EventArgs e)
        {
            Initialize();
            var mainWorkspace = m_WorkItem.Workspaces[WorkspacesConstants.MAIN_WORKSPACE];
            var kontrolaMpWorkItem = m_WorkItem.WorkItems.Get<KontrolaMPWorkItem>(WorkItemsConstants.KONTROLA_MP_WORKITEM);
            kontrolaMpWorkItem.Show(mainWorkspace);
        }
    }
}