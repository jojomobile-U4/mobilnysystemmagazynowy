﻿using System;
using System.Windows.Forms;
using Common.Base;
using DostawaModule.DataModel;

namespace DostawaModule.RealizacjaMP.ListaNosnikow
{
    public partial class ListaNosnikowView : ViewBase, IListaNosnikowView
    {
        public event EventHandler Wybierz;
        public event EventHandler Anuluj;

        public ListaNosnikowView()
        {
            InitializeComponent();
        }

        private void OnBtnAnuluj(object sender, EventArgs e)
        {
            if (Anuluj != null)
                Anuluj(sender, e);
        }

        private void OnBtnWybierz(object sender, EventArgs e)
        {
            if (Wybierz != null)
                Wybierz(sender, e);
        }

        public ListView.ListViewItemCollection ListaNosnikowDataSource
        {
            get
            {
                return lstNosniki.Items;
            }
        }

        public Nosnik ZaznaczonyNosnik
        {
            get
            {
                if (lstNosniki.SelectedIndices.Count == 0)
                {
                    return null;
                }

                return lstNosniki.Items[lstNosniki.SelectedIndices[0]].Tag as Nosnik;
            }
        }

        public string Lokalizacja
        {
            set { tbLokalizacja.Text = value; }
        }
    }
}
