﻿using System;
using System.Windows.Forms;
using Common.Base;
using DostawaModule.DataModel;

namespace DostawaModule.RealizacjaMP.ListaNosnikow
{
    interface IListaNosnikowView : IViewBase
    {
        event EventHandler Wybierz;
        event EventHandler Anuluj;
        ListView.ListViewItemCollection ListaNosnikowDataSource { get; }
        Nosnik ZaznaczonyNosnik { get; }
        string Lokalizacja { set; }
    }
}
