﻿namespace DostawaModule.RealizacjaMP.ListaNosnikow
{
    partial class ListaNosnikowView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnWybierz = new System.Windows.Forms.Button();
            this.btnAnuluj = new System.Windows.Forms.Button();
            this.lbTytul = new Common.Components.MSMLabel();
            this.lstNosniki = new Common.Components.MSMListView();
            this.chLp = new System.Windows.Forms.ColumnHeader();
            this.chNosnik = new System.Windows.Forms.ColumnHeader();
            this.tbLokalizacja = new System.Windows.Forms.TextBox();
            this.lbLokalizacja = new System.Windows.Forms.Label();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnWybierz);
            this.pnlNavigation.Controls.Add(this.btnAnuluj);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 255);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 25);
            // 
            // btnWybierz
            // 
            this.btnWybierz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnWybierz.Location = new System.Drawing.Point(73, 2);
            this.btnWybierz.Name = "btnWybierz";
            this.btnWybierz.Size = new System.Drawing.Size(80, 20);
            this.btnWybierz.TabIndex = 0;
            this.btnWybierz.TabStop = false;
            this.btnWybierz.Text = "&Ret Wybierz";
            this.btnWybierz.Click += new System.EventHandler(this.OnBtnWybierz);
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnAnuluj.Location = new System.Drawing.Point(157, 2);
            this.btnAnuluj.Name = "btnAnuluj";
            this.btnAnuluj.Size = new System.Drawing.Size(80, 20);
            this.btnAnuluj.TabIndex = 1;
            this.btnAnuluj.TabStop = false;
            this.btnAnuluj.Text = "&Esc Anuluj";
            this.btnAnuluj.Click += new System.EventHandler(this.OnBtnAnuluj);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 1;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "REALIZACJA MP - LISTA NOŚNIKÓW";
            // 
            // lstNosniki
            // 
            this.lstNosniki.Columns.Add(this.chLp);
            this.lstNosniki.Columns.Add(this.chNosnik);
            this.lstNosniki.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lstNosniki.FullRowSelect = true;
            this.lstNosniki.Location = new System.Drawing.Point(3, 55);
            this.lstNosniki.Name = "lstNosniki";
            this.lstNosniki.Size = new System.Drawing.Size(234, 194);
            this.lstNosniki.TabIndex = 2;
            this.lstNosniki.View = System.Windows.Forms.View.Details;
            // 
            // chLp
            // 
            this.chLp.Text = "Lp";
            this.chLp.Width = 40;
            // 
            // chNosnik
            // 
            this.chNosnik.Text = "Nośnik";
            this.chNosnik.Width = 191;
            // 
            // tbLokalizacja
            // 
            this.tbLokalizacja.Location = new System.Drawing.Point(74, 25);
            this.tbLokalizacja.Name = "tbLokalizacja";
            this.tbLokalizacja.ReadOnly = true;
            this.tbLokalizacja.Size = new System.Drawing.Size(163, 21);
            this.tbLokalizacja.TabIndex = 2;
            this.tbLokalizacja.TabStop = false;
            // 
            // lbLokalizacja
            // 
            this.lbLokalizacja.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbLokalizacja.Location = new System.Drawing.Point(3, 26);
            this.lbLokalizacja.Name = "lbLokalizacja";
            this.lbLokalizacja.Size = new System.Drawing.Size(65, 20);
            this.lbLokalizacja.Text = "Lokalizacja:";
            // 
            // ListaNosnikowView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lbTytul);
            this.Controls.Add(this.tbLokalizacja);
            this.Controls.Add(this.lbLokalizacja);
            this.Controls.Add(this.lstNosniki);
            this.Name = "ListaNosnikowView";
            this.Size = new System.Drawing.Size(240, 280);
            this.Controls.SetChildIndex(this.lstNosniki, 0);
            this.Controls.SetChildIndex(this.lbLokalizacja, 0);
            this.Controls.SetChildIndex(this.tbLokalizacja, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnWybierz;
        private System.Windows.Forms.Button btnAnuluj;
        private Common.Components.MSMLabel lbTytul;
        private Common.Components.MSMListView lstNosniki;
        private System.Windows.Forms.ColumnHeader chNosnik;
        private System.Windows.Forms.ColumnHeader chLp;
        private System.Windows.Forms.TextBox tbLokalizacja;
        private System.Windows.Forms.Label lbLokalizacja;
    }
}
