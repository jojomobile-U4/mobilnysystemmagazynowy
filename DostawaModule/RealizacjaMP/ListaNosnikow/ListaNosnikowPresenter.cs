﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Common;
using Common.Base;
using DostawaModule.DataModel;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;

namespace DostawaModule.RealizacjaMP.ListaNosnikow
{
    class ListaNosnikowPresenter : PresenterBase
    {
        [EventPublication(EventBrokerConstants.WYBRANO_NOSNIK, PublicationScope.WorkItem)]
        public event EventHandler WybranoNosnik;

        List<Nosnik> ListaNosnikow;
        private string Lokalizacja;

        public ListaNosnikowPresenter(IViewBase view)
            : base(view)
        {
        }

        protected RealizacjaMPWorkItem MyWorkItem
        {
            get { return WorkItem as RealizacjaMPWorkItem; }
        }

        public IListaNosnikowView View
        {
            get { return m_view as IListaNosnikowView; }
        }

        public void WyswietlListe(long? painId, string kodLokalizacji, float? ilosc)
        {
            ListaNosnikow = MyWorkItem.PobierzListeDostepnychNosnikow(painId, kodLokalizacji, ilosc);
            Lokalizacja = kodLokalizacji;
            AktualizujWidok();
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Return:
                    e.Handled = true;
                    Wybierz(this, EventArgs.Empty);
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    Anuluj(this, EventArgs.Empty);
                    break;
            }
        }

        protected override void AttachView()
        {
            View.Wybierz += Wybierz;
            View.Anuluj += Anuluj;

        }

        private void Wybierz(object sender, EventArgs e)
        {
            if (View.ZaznaczonyNosnik == null)
                return;

            WorkItem.State[StateConstants.IDENTYFIKATOR_NOSNIKA] = View.ZaznaczonyNosnik.Id;
            WorkItem.State[StateConstants.LOCALIZATION] = Lokalizacja;
            WorkItemBase.MainWorkspace.Close(View);
            if (WybranoNosnik != null)
                WybranoNosnik(this, EventArgs.Empty);
        }

        private void Anuluj(object sender, EventArgs e)
        {
            WorkItemBase.MainWorkspace.Close(View);
        }

        private void AktualizujWidok()
        {
            View.Lokalizacja = Lokalizacja;
            View.ListaNosnikowDataSource.Clear();
            if (ListaNosnikow != null)
            {
                var i = 0;
                foreach (var nosnik in ListaNosnikow)
                {
                    i++;
                    var item = new ListViewItem(new[] { i.ToString(), nosnik.Symbol }) { Tag = nosnik };

                    View.ListaNosnikowDataSource.Add(item);
                }
            }
        }
    }
}
