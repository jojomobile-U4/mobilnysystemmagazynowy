#region

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace DostawaModule
{
    public class RealizacjaMPWorkItem : WorkItemBase
    {
        #region Constants

        private const string MNIEJSZA = "mniejsza";
        private const string NIEZGODNA_ILOSC_SKONTROLOWANA_I_UMIESZCZANA = "Suma ilo�ci skontrolowanego indeksu jest {0} od sumy ilo�ci umieszczanych o {1}.\nCzy chcesz umie�ci� tak� ilo��?";
        private const string REALIZACJA_MP = "Realizacja MP";
        private const string WIEKSZA = "wieksza";
        private const string WZORZEC_LICZBY_RZECZYWISTEJ = "0.###";

        #endregion

        public void Show(IWorkspace parentWorkspace)
        {
            FunctionsHelper.SetCursorWait();
            RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
            RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
            RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
            RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;


            var realizacjaMpViewPresenter = Items.Get<RealizacjaMPViewPresenter>(ItemsConstants.REALIZACJA_MP_PRESENTER);
            State["odczytKoduKreskowego"] = DBBool.FALSE;
            State["kryteriaZapytania"] = new KryteriaZapytaniaPozycjiMP();

            if (long.Parse(Parent.State["IdDokumentu"].ToString()) != -1)
            {
                realizacjaMpViewPresenter.KryteriaZapytania.IdDokumentu = long.Parse(Parent.State["IdDokumentu"].ToString());
                Szukaj(realizacjaMpViewPresenter.KryteriaZapytania);
            }
            else
            {
                realizacjaMpViewPresenter.ZaladujDaneDoWidoku(new List<PozycjaMP>());
            }

            Parent.State["IdDokumentu"] = -1;
            parentWorkspace.Show(realizacjaMpViewPresenter.View);
            Activate();

            FunctionsHelper.SetCursorDefault();
        }

        public bool Szukaj(KryteriaZapytaniaPozycjiMP kryteriaZapytania)
        {
            FunctionsHelper.SetCursorWait();

            bool wynikSzukania;
            var dokumentyMagazynoweService = Services.Get<IDokumentyMagazynoweService>(true);
            Items.Get<RealizacjaMPViewPresenter>(ItemsConstants.REALIZACJA_MP_PRESENTER);
            var lista = dokumentyMagazynoweService.PobierzListePozycji(kryteriaZapytania);
            if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, REALIZACJA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                wynikSzukania = false;
            }
            else if (lista.Count > 0)
            {
                Items.Get<RealizacjaMPViewPresenter>(ItemsConstants.REALIZACJA_MP_PRESENTER).ZaladujDaneDoWidoku(lista);
                wynikSzukania = true;
            }
            else
            {
                wynikSzukania = false;
            }

            FunctionsHelper.SetCursorDefault();
            return wynikSzukania;
        }

        public long IloscPozycjiNaDokumencie(long idDokumentu, String skontrolowana, String zakonczona)
        {
            FunctionsHelper.SetCursorWait();

            var dokumentyMagazynoweService = Services.Get<IDokumentyMagazynoweService>(true);
            var iloscPozycji = dokumentyMagazynoweService.IloscPozycjiNaDokumencie(idDokumentu, skontrolowana, zakonczona);
            if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, REALIZACJA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                return -1;
            }

            FunctionsHelper.SetCursorDefault();
            return iloscPozycji;
        }

        public void RezerwujDokument(long id)
        {
            FunctionsHelper.SetCursorWait();
            Services.Get<IDokumentyMagazynoweService>(true).RezerwujDokument(id);
            if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, REALIZACJA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
            }
            FunctionsHelper.SetCursorDefault();
        }

        public bool PoprawnaIloscUmieszczanaISkontrolowana(long id, float iloscUmieszczana)
        {
            FunctionsHelper.SetCursorWait();
            var iloscSkontrolowana = Services.Get<IDokumentyMagazynoweService>(true).PobierzIloscSkontrolowana(id);
            FunctionsHelper.SetCursorDefault();
            if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, REALIZACJA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                return false;
            }
            if (iloscSkontrolowana != iloscUmieszczana)
            {
                var porownanie = iloscSkontrolowana > iloscUmieszczana ? WIEKSZA : MNIEJSZA;
                var roznica = iloscSkontrolowana - iloscUmieszczana;

                var result = MessageBox.Show(
                    String.Format(NIEZGODNA_ILOSC_SKONTROLOWANA_I_UMIESZCZANA, porownanie, roznica.ToString(WZORZEC_LICZBY_RZECZYWISTEJ)),
                    "Zako�cz",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button2);
                Activate();

                return result == DialogResult.Yes;
            }

            return true;
        }

        public void UstawZakonczonaPozycje(long id)
        {
            FunctionsHelper.SetCursorWait();
            Services.Get<IDokumentyMagazynoweService>(true).UstawZakonczonaPozycje(id);
            if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, REALIZACJA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
            }
            FunctionsHelper.SetCursorDefault();
        }

        public void DodajPozycje(long id, string lokalizacja, long? nosnId, float ilosc)
        {
            FunctionsHelper.SetCursorWait();
            var listaPozycji = Services.Get<IDokumentyMagazynoweService>(true).DodajPozycje(id, lokalizacja, nosnId, ilosc);
            if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, REALIZACJA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
            }
            else
            {
                Items.Get<RealizacjaMPViewPresenter>(ItemsConstants.REALIZACJA_MP_PRESENTER).DodajPozycjeDoWidoku(listaPozycji);
            }
            FunctionsHelper.SetCursorDefault();
        }

        public void ZmienLokalizacje(long id, string lokalizacja, long? nosnId)
        {
            FunctionsHelper.SetCursorWait();
            Services.Get<IDokumentyMagazynoweService>(true).ZmienLokalizacje(id, lokalizacja, nosnId);
            if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, REALIZACJA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
            }
            FunctionsHelper.SetCursorDefault();
        }

        public bool UruchomRaport(string nazwaFormularza, long idDokumentu, long idPozycji)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                Services.Get<IDokumentyMagazynoweService>(true).UruchomRaport(nazwaFormularza, idDokumentu, idPozycji);
                if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, REALIZACJA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                    return false;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }

            return true;
        }

        public bool MoznaJednoznacznieOkreslicNosnik(long? painId, string kodLokalizacji, float? ilosc)
        {
            return Services.Get<IDokumentyMagazynoweService>(true).MoznaJednoznacznieOkreslicNosnik(painId,
                                                                                                    kodLokalizacji,
                                                                                                    ilosc);
        }

        public new void ShowMessageBox(string text, string caption, string stackTrace)
        {
            base.ShowMessageBox(text, caption, stackTrace);
        }

        public List<DataModel.Nosnik> PobierzListeDostepnychNosnikow(long? painId, string kodLokalizacji, float? ilosc)
        {
            FunctionsHelper.SetCursorWait();
            var listaNosnikow = Services.Get<IDokumentyMagazynoweService>(true).PobierzListeDostepnychNosnikow(painId, kodLokalizacji, ilosc);

            if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, REALIZACJA_MP, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
            }
            FunctionsHelper.SetCursorDefault();

            return listaNosnikow;
        }

        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ, Thread = ThreadOption.UserInterface)]
        public void SzukajWgKoduKreskowego(object sender, EventArgs args)
        {
            if (MainWorkspace.ActiveSmartPart == Items.Get<RealizacjaMPViewPresenter>(ItemsConstants.REALIZACJA_MP_PRESENTER).View)
            {
                try
                {
                    FunctionsHelper.SetCursorWait();


                    var barCode = RootWorkItem.State[StateConstants.BAR_CODE] as string;
                    var lokalizacja = Localization.RemovePrefix(barCode);

                    var nowaPozycjaPrezenter = Items.Get<NowaPozycjaMPViewPresenter>(ItemsConstants.NOWA_POZYCJA_MP_PRESENTER);
                    // jezeli dodajemy nowa pozycje to zaczytanie powoduje wpisanie lokalizacji dla nowej pozycji
                    if (MainWorkspace.ActiveSmartPart == nowaPozycjaPrezenter.View)
                    {
                        nowaPozycjaPrezenter.View.Lokalizacja = barCode;
                    }
                    else
                    {
                        var id = KonwertujBezOstrzezenia(barCode);
                        var realizacjaMpViewPresenter = Items.Get<RealizacjaMPViewPresenter>(ItemsConstants.REALIZACJA_MP_PRESENTER);

                        if (id.HasValue)
                        {
                            if ((realizacjaMpViewPresenter.IdAktualnejPozycji() != id) && (!realizacjaMpViewPresenter.ZmienPozycje()))
                            {
                                FunctionsHelper.SetCursorDefault();
                                return;
                            }

                            var kryteriaZapytania = new KryteriaZapytaniaPozycjiMP { NumerStrony = 0, Id = id.Value };
                            State["kryteriaZapytania"] = kryteriaZapytania;
                            State["odczytKoduKreskowego"] = "T";
                            Szukaj(kryteriaZapytania);
                        }
                        else
                        {
                            Items.Get<RealizacjaMPViewPresenter>(ItemsConstants.REALIZACJA_MP_PRESENTER).
                                UmiescTowarWLokalizacji(lokalizacja);
                        }
                    }
                }
                finally
                {
                    Activate();
                    FunctionsHelper.SetCursorDefault();
                }
            }
        }

        public void Przesun()
        {
            RootWorkItem.Commands[CommandConstants.NOWE_PRZESUNIECIE].Execute();
        }

        private long? KonwertujBezOstrzezenia(string barCode)
        {
            try
            {
                return long.Parse(barCode);
            }
            catch (FormatException)
            {
                return null;
            }
        }


    }
}