using System;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using Common.Base;

namespace DostawaModule
{
    [SmartPart]
    public partial class RealizacjaMPView : ViewBase, IRealizacjaMPView
    {
        private delegate void SymbolDokumentuDelegate(string symbol);
        public event EventHandler Szukaj;
        public event EventHandler PrzejdzDoNastepnejPozycji;
        public event EventHandler PrzejdzDoPoprzedniejPozycji;
        public event EventHandler Dodaj;
        public event EventHandler Zakoncz;
        public event EventHandler Powrot;
		public event EventHandler Przesun;

        public RealizacjaMPView()
        {
            InitializeComponent();
			InitializeFocusedControl();
        }

        private void DoUpdate(string symbol)
        {
            if (InvokeRequired)
            {
                Invoke(new SymbolDokumentuDelegate(DoUpdate),
                            new object[] { symbol });
                return;
            }
            tbDokument.Text = symbol;
        } 

        public string SymbolDokumentu
        {
            set 
            {
                DoUpdate(value);
            }
            get 
            {
                return tbDokument.Text;
            }
        }

        public PozycjaMP Pozycja
        {
            set
            {
                pozycjaMPBindingSource.DataSource = value;
            }
        }

        public bool KodLokalizacjiReadOnly
        {
            set 
            {
                tbLokalizacja.ReadOnly = value;
            }
        }

        public void LokalizacjaSetFocus()
        {
            tbLokalizacja.Focus();
        }

        public string KodLokalizacji
        {
            get
            {
                return tbLokalizacja.Text;
            }           
        }

        private void btnSzukaj_Click(object sender, EventArgs e)
        {
            if (Szukaj != null)
            {
                Szukaj(this, null);
            }
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            if (PrzejdzDoNastepnejPozycji != null)
            {
                PrzejdzDoNastepnejPozycji(this, null);
            }
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            if (PrzejdzDoPoprzedniejPozycji != null)
            {
                PrzejdzDoPoprzedniejPozycji(this, null);
            }
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (Dodaj != null)
            {
                Dodaj(this, null);
            }
        }

        private void btnZakoncz_Click(object sender, EventArgs e)
        {
            if (Zakoncz != null)
            {
                Zakoncz(this, null);
            }
        }

        private void btnPowrot_Click(object sender, EventArgs e)
        {
            if (Powrot != null)
            {
                Powrot(this, null);
            }
		}

		private void btnPrzesun_Click(object sender, EventArgs e)
		{
			if (Przesun != null)
			{
				Przesun(this, EventArgs.Empty);
			}
		}

		public bool PrzesunVisible
		{
			set { btnPrzesun.Visible = value; }
		}

        public long IloscPozycji
        {
            set
            {
                tbIloscLP.Text = value.ToString();
            }
            get
            {
                try
                {
                    return long.Parse(tbIloscLP.Text);
                }
                catch (FormatException)
                {
                    return 0;
                }
            }
        }

		public long Pozostalo
		{
			set
			{
				tbPozostalo.Text = value.ToString();
			}
		}

        public bool IloscReadOnly
        {
            set
            {
                tbIlosc.ReadOnly = value;
            }
        }
    }
}
