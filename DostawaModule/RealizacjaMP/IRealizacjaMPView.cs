using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Common.Base;

namespace DostawaModule
{
    public interface IRealizacjaMPView: IViewBase
    {
        event EventHandler Szukaj;
        event EventHandler PrzejdzDoNastepnejPozycji;
        event EventHandler PrzejdzDoPoprzedniejPozycji;
        event EventHandler Dodaj;
        event EventHandler Zakoncz;
        event EventHandler Powrot;
		event EventHandler Przesun;

        bool KodLokalizacjiReadOnly { set; }
        void LokalizacjaSetFocus();
        PozycjaMP Pozycja { set; }
        string SymbolDokumentu { get; set; }
        string KodLokalizacji { get; }
		bool PrzesunVisible { set; }

        long IloscPozycji { set; get; }
		long Pozostalo { set; }
        bool IloscReadOnly { set; }
    }
}
