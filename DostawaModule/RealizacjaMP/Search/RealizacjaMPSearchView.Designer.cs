using Common.Components;

namespace DostawaModule.RealizacjaSearch
{
    partial class RealizacjaMPSearchView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.lbTytul = new Common.Components.MSMLabel();
			this.tbSymbolTowaru = new System.Windows.Forms.TextBox();
			this.lbSymbolTowaru = new System.Windows.Forms.Label();
			this.tbNazwaTowaru = new System.Windows.Forms.TextBox();
			this.lbNazwaTowaru = new System.Windows.Forms.Label();
			this.tbZakonczona = new System.Windows.Forms.TextBox();
			this.lbZakonczona = new System.Windows.Forms.Label();
			this.pnlNavigation.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnAnuluj
			// 
			this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			// 
			// btnOstatnieZapytanie
			// 
			this.btnOstatnieZapytanie.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			// 
			// btnOK
			// 
			this.btnOK.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			// 
			// lbTytul
			// 
			this.lbTytul.BackColor = System.Drawing.Color.Empty;
			this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
			this.lbTytul.CenterAlignX = false;
			this.lbTytul.CenterAlignY = true;
			this.lbTytul.ColorText = System.Drawing.Color.White;
			this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
			this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.lbTytul.ForeColor = System.Drawing.Color.Empty;
			this.lbTytul.Location = new System.Drawing.Point(0, 0);
			this.lbTytul.Name = "lbTytul";
			this.lbTytul.RectangleColor = System.Drawing.Color.Black;
			this.lbTytul.Size = new System.Drawing.Size(240, 16);
			this.lbTytul.TabIndex = 5;
			this.lbTytul.TabStop = false;
			this.lbTytul.TextDisplayed = "REALIZACJA MP - SZUKAJ";
			// 
			// tbSymbolTowaru
			// 
			this.tbSymbolTowaru.Location = new System.Drawing.Point(80, 42);
			this.tbSymbolTowaru.Name = "tbSymbolTowaru";
			this.tbSymbolTowaru.Size = new System.Drawing.Size(157, 21);
			this.tbSymbolTowaru.TabIndex = 0;
			// 
			// lbSymbolTowaru
			// 
			this.lbSymbolTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
			this.lbSymbolTowaru.Location = new System.Drawing.Point(3, 42);
			this.lbSymbolTowaru.Name = "lbSymbolTowaru";
			this.lbSymbolTowaru.Size = new System.Drawing.Size(77, 20);
			this.lbSymbolTowaru.Text = "Indeks:";
			// 
			// tbNazwaTowaru
			// 
			this.tbNazwaTowaru.Location = new System.Drawing.Point(80, 67);
			this.tbNazwaTowaru.Name = "tbNazwaTowaru";
			this.tbNazwaTowaru.Size = new System.Drawing.Size(157, 21);
			this.tbNazwaTowaru.TabIndex = 1;
			// 
			// lbNazwaTowaru
			// 
			this.lbNazwaTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
			this.lbNazwaTowaru.Location = new System.Drawing.Point(3, 67);
			this.lbNazwaTowaru.Name = "lbNazwaTowaru";
			this.lbNazwaTowaru.Size = new System.Drawing.Size(77, 20);
			this.lbNazwaTowaru.Text = "Nazwa towaru:";
			// 
			// tbZakonczona
			// 
			this.tbZakonczona.Location = new System.Drawing.Point(80, 92);
			this.tbZakonczona.Name = "tbZakonczona";
			this.tbZakonczona.Size = new System.Drawing.Size(157, 21);
			this.tbZakonczona.TabIndex = 2;
			// 
			// lbZakonczona
			// 
			this.lbZakonczona.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
			this.lbZakonczona.Location = new System.Drawing.Point(3, 92);
			this.lbZakonczona.Name = "lbZakonczona";
			this.lbZakonczona.Size = new System.Drawing.Size(77, 20);
			this.lbZakonczona.Text = "Zakończona:";
			// 
			// RealizacjaMPSearchView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.Controls.Add(this.lbZakonczona);
			this.Controls.Add(this.tbZakonczona);
			this.Controls.Add(this.lbNazwaTowaru);
			this.Controls.Add(this.tbNazwaTowaru);
			this.Controls.Add(this.lbSymbolTowaru);
			this.Controls.Add(this.tbSymbolTowaru);
			this.Controls.Add(this.lbTytul);
			this.Name = "RealizacjaMPSearchView";
			this.Controls.SetChildIndex(this.pnlNavigation, 0);
			this.Controls.SetChildIndex(this.lbTytul, 0);
			this.Controls.SetChildIndex(this.tbSymbolTowaru, 0);
			this.Controls.SetChildIndex(this.lbSymbolTowaru, 0);
			this.Controls.SetChildIndex(this.tbNazwaTowaru, 0);
			this.Controls.SetChildIndex(this.lbNazwaTowaru, 0);
			this.Controls.SetChildIndex(this.tbZakonczona, 0);
			this.Controls.SetChildIndex(this.lbZakonczona, 0);
			this.pnlNavigation.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private MSMLabel lbTytul;
        private System.Windows.Forms.TextBox tbSymbolTowaru;
        private System.Windows.Forms.Label lbSymbolTowaru;
        private System.Windows.Forms.TextBox tbNazwaTowaru;
        private System.Windows.Forms.Label lbNazwaTowaru;
        private System.Windows.Forms.TextBox tbZakonczona;
        private System.Windows.Forms.Label lbZakonczona;
    }
}
