#region

using Common.SearchForm;

#endregion

namespace DostawaModule.RealizacjaSearch
{
    public partial class RealizacjaMPSearchView : SearchForm, IRealizacjaMPSearchView
    {
        public RealizacjaMPSearchView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #region IRealizacjaMPSearchView Members

        public KryteriaZapytaniaPozycjiMP KryteriaZapytania
        {
            get
            {
                var kryteriaZapytania = new KryteriaZapytaniaPozycjiMP();
                kryteriaZapytania.SymbolTowaru = tbSymbolTowaru.Text;
                kryteriaZapytania.NazwaTowaru = tbNazwaTowaru.Text;
                kryteriaZapytania.Zakonczona = tbZakonczona.Text;
                return kryteriaZapytania;
            }
            set { DoUpdate(value); }
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);

            tbNazwaTowaru.ReadOnly = navigationState;
            tbSymbolTowaru.ReadOnly = navigationState;
            tbZakonczona.ReadOnly = navigationState;
        }

        #endregion

        private void DoUpdate(KryteriaZapytaniaPozycjiMP kryteriaZapytania)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new KryteriaZapytaniaDelegate(DoUpdate),
                            new object[] {kryteriaZapytania});
                return;
            }
            this.tbSymbolTowaru.Text = kryteriaZapytania.SymbolTowaru;
            this.tbNazwaTowaru.Text = kryteriaZapytania.NazwaTowaru;
            this.tbZakonczona.Text = kryteriaZapytania.Zakonczona;
        }

        #region Nested type: KryteriaZapytaniaDelegate

        private delegate void KryteriaZapytaniaDelegate(KryteriaZapytaniaPozycjiMP kryteriaZapytania);

        #endregion
    }
}