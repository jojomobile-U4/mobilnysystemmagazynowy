using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using Microsoft.Practices.Mobile.CompositeUI;
using Common.SearchForm;

namespace DostawaModule.RealizacjaSearch
{
    public class RealizacjaMPSearchViewPresenter: SearchFormPresenter
    {
        KryteriaZapytaniaPozycjiMP ostatnieZapytanie = new KryteriaZapytaniaPozycjiMP();

        protected RealizacjaMPWorkItem MyWorkItem
        {
            get { return WorkItem as RealizacjaMPWorkItem; }
        }

		public IRealizacjaMPSearchView View
		{
			get { return m_view as IRealizacjaMPSearchView; }
		}

        public RealizacjaMPSearchViewPresenter(IRealizacjaMPSearchView view): base(view)
        {
        }

		protected override void OnViewSzukaj(object sender, EventArgs e)
		{
            KryteriaZapytaniaPozycjiMP kryteriaZapytania = new KryteriaZapytaniaPozycjiMP();
			kryteriaZapytania = View.KryteriaZapytania;
			kryteriaZapytania.NumerStrony = ((KryteriaZapytaniaPozycjiMP)State["kryteriaZapytania"]).NumerStrony;
			kryteriaZapytania.WielkoscStrony = ((KryteriaZapytaniaPozycjiMP)State["kryteriaZapytania"]).WielkoscStrony;
			kryteriaZapytania.Skontrolowana = ((KryteriaZapytaniaPozycjiMP)State["kryteriaZapytania"]).Skontrolowana;
            ostatnieZapytanie = kryteriaZapytania;
			MyWorkItem.Szukaj(kryteriaZapytania);
            base.OnViewSzukaj(sender, e);
        }

		protected override void OnViewUstawOstatnieZapytanie(object sender, EventArgs e)
		{
            View.KryteriaZapytania = ostatnieZapytanie;
		}
    }
}
