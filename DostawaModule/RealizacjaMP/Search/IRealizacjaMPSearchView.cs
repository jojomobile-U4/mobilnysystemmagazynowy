#region

using Common.SearchForm;

#endregion

namespace DostawaModule.RealizacjaSearch
{
    public interface IRealizacjaMPSearchView : ISearchForm
    {
        KryteriaZapytaniaPozycjiMP KryteriaZapytania { get; set; }
    }
}