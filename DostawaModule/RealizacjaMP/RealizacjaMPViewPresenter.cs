using System;
using System.Collections.Generic;
using DostawaModule.RealizacjaMP.ListaNosnikow;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using System.Windows.Forms;
using Common.Base;
using Common;
using Common.DataModel;

namespace DostawaModule
{
    public class RealizacjaMPViewPresenter : PresenterBase
    {
        public const string NAZWA_FORMULARZA = "PRZYJ�CIE - REALIZACJA MP";
        private const string POMYSLNE_WYWOLANIE_RAPORTU = "Wydruk zosta� wys�any na drukark�.";
        private const string REALIZACJA_MP = "Realizacja MP";

        private KryteriaZapytaniaPozycjiMP kryteriaZapytania;
        private List<PozycjaMP> listaPozycji = new List<PozycjaMP>();
        private int numerPozycji = 0;
        private int numerStrony = 0;
        private long iloscPozycjiNiezakonczonych;
        private long iloscPozycji;
        private long idDokumentu = -1;
        private long? idAktualnegoDokumentu;
        private bool NalezyZmienicLokalizacje;


        protected RealizacjaMPWorkItem MyWorkItem
        {
            get { return WorkItem as RealizacjaMPWorkItem; }
        }

        public IRealizacjaMPView View
        {
            get { return m_view as IRealizacjaMPView; }
        }

        public RealizacjaMPViewPresenter(IRealizacjaMPView view)
            : base(view)
        {
            kryteriaZapytania = new KryteriaZapytaniaPozycjiMP();
            kryteriaZapytania.NumerStrony = numerStrony;
            kryteriaZapytania.Skontrolowana = DBBool.TRUE;
            iloscPozycjiNiezakonczonych = 0;
        }

        protected override void AttachView()
        {
            View.Szukaj += ViewSzukaj;
            View.PrzejdzDoNastepnejPozycji += ViewPrzejdzDoNastepnejPozycji;
            View.PrzejdzDoPoprzedniejPozycji += ViewPrzejdzDoPoprzedniejPozycji;
            View.Dodaj -= ViewDodaj;
            View.Dodaj += ViewDodaj;
            View.Zakoncz += view_Zakoncz;
            View.Powrot += ViewPowrot;
            View.Przesun += ViewPrzesun;
        }

        void ViewPrzesun(object sender, EventArgs e)
        {
            MyWorkItem.Przesun();
        }

        void ViewDodaj(object sender, EventArgs e)
        {
            if (listaPozycji.Count > 0)
            {
                WorkItem.State["kryteriaZapytania"] = kryteriaZapytania;
                var nowaPozycjaMPViewPresenter
                    = WorkItem.Items.Get<NowaPozycjaMPViewPresenter>(ItemsConstants.NOWA_POZYCJA_MP_PRESENTER);

                nowaPozycjaMPViewPresenter.PozycjaMp = listaPozycji[numerPozycji];

                WorkItem.Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                MyWorkItem.MainWorkspace.Show(nowaPozycjaMPViewPresenter.View);
            }
        }

        public KryteriaZapytaniaPozycjiMP KryteriaZapytania
        {
            get
            {
                return kryteriaZapytania;
            }
            set
            {
                kryteriaZapytania = value;
            }
        }

        public void ZaladujDaneDoWidoku(List<PozycjaMP> listaPozycji)
        {
            if (State["odczytKoduKreskowego"].Equals(DBBool.TRUE))
            {
                if (listaPozycji.Count != 0)
                {
                    if (listaPozycji[0].Skontrolowana.Equals(DBBool.FALSE))
                    {
                        MessageBox.Show("Ta pozycja dokumentu nie jest jeszcze skontrolowana");
                        MyWorkItem.Activate();
                    }
                    else
                    {
                        this.listaPozycji = listaPozycji;
                        numerPozycji = 0;
                        Rezerwuj();
                        View.LokalizacjaSetFocus();
                        AktualizujWidok();
                    }
                }
            }
            else
            {
                this.listaPozycji = listaPozycji;
                if (listaPozycji.Count != 0)
                {
                    numerPozycji = 0;
                    Rezerwuj();
                    View.LokalizacjaSetFocus();
                    AktualizujWidok();
                }
                else
                {
                    View.KodLokalizacjiReadOnly = NavigationState;
                    View.LokalizacjaSetFocus();
                    View.Pozycja = new PozycjaMP();
                    View.Pozostalo = 0;
                }
            }
        }

        private void ViewSzukaj(Object sender, EventArgs e)
        {
            WorkItem.State["kryteriaZapytania"] = kryteriaZapytania;
            RealizacjaSearch.RealizacjaMPSearchViewPresenter realizacjaMPSearchViewPresenter
                = WorkItem.Items.Get<RealizacjaSearch.RealizacjaMPSearchViewPresenter>(ItemsConstants.REALIZACJA_MP_SEARCH_PRESENTER);

            realizacjaMPSearchViewPresenter.View.KryteriaZapytania = new KryteriaZapytaniaPozycjiMP();
            WorkItem.Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
            MyWorkItem.MainWorkspace.Show(realizacjaMPSearchViewPresenter.View);
        }

        private void ViewPrzejdzDoNastepnejPozycji(Object sender, EventArgs e)
        {
            View.ForceDataBinding();
            if (numerPozycji < listaPozycji.Count - 1)
            {
                if (!ZmienPozycje())
                {
                    return;
                }

                numerPozycji++;
                Rezerwuj();
                View.LokalizacjaSetFocus();
                AktualizujWidok();
            }
            else
            {
                if (numerPozycji == MyWorkItem.Configuration.DostawaWS.WielkoscStrony - 1)
                {
                    numerStrony++;
                    kryteriaZapytania.NumerStrony = numerStrony;
                    View.LokalizacjaSetFocus();
                    MyWorkItem.Szukaj(kryteriaZapytania);
                }
            }
        }

        private void ViewPrzejdzDoPoprzedniejPozycji(Object sender, EventArgs e)
        {
            View.ForceDataBinding();
            if (numerPozycji > 0)
            {
                if (!ZmienPozycje())
                {
                    return;
                }

                numerPozycji--;
                Rezerwuj();
                View.LokalizacjaSetFocus();
                AktualizujWidok();
            }
        }

        private void view_Zakoncz(Object sender, EventArgs e)
        {
            View.ForceDataBinding();
            Zakoncz();
        }


        public void Zakoncz()
        {
            if (listaPozycji.Count > 0)
            {
                if (listaPozycji[numerPozycji].Zakonczona.Equals(DBBool.FALSE))
                {
                    ZakonczPozycje();
                }
            }
        }

        private void ViewPowrot(Object sender, EventArgs e)
        {
            //MyWorkItem.MainWorkspace.Close(View);
            CloseView();
        }

        private void Rezerwuj()
        {
            View.ForceDataBinding();
            if (idDokumentu != listaPozycji[numerPozycji].IdDokumentu)
            {
                idDokumentu = (long)listaPozycji[numerPozycji].IdDokumentu;
                MyWorkItem.RezerwujDokument(idDokumentu);
            }
        }

        public bool ZmienPozycje()
        {
            if ((listaPozycji != null) && (numerPozycji < listaPozycji.Count)
                && (listaPozycji[numerPozycji].Zakonczona.Equals(DBBool.FALSE)))
            {
                DialogResult result = MessageBox.Show("Bie��ca pozycja nie zosta�a zako�czona. Czy kontynuowa�?", "Zmiana niezako�czonej pozycji", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                MyWorkItem.Activate();
                if (result == DialogResult.No)
                {
                    return false;
                }
            }
            return true;
        }

        public long? IdAktualnejPozycji()
        {
            if ((listaPozycji != null) && (numerPozycji < listaPozycji.Count))
            {
                return listaPozycji[numerPozycji].Id;
            }
            return null;
        }

        public PozycjaMP AktualnaPozycja()
        {
            if ((listaPozycji != null) && (numerPozycji < listaPozycji.Count))
            {
                return listaPozycji[numerPozycji];
            }
            return null;
        }

        public void UmiescTowarWLokalizacji(string lokalizacja)
        {
            if ((listaPozycji.Count > 0) && (listaPozycji[numerPozycji].Zakonczona.Equals(DBBool.FALSE)))
            {
                if (listaPozycji[numerPozycji].KodLokalizacji != lokalizacja)
                {
                    DialogResult result = MessageBox.Show("Czy na pewno chcesz umie�ci� towar w tej lokalizacji?", "Zmiana lokalizacji", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                    MyWorkItem.Activate();
                    if (result == DialogResult.Yes)
                    {
                        if (!MyWorkItem.MoznaJednoznacznieOkreslicNosnik(AktualnaPozycja().PainId, lokalizacja, AktualnaPozycja().Ilosc))
                        {
                            if (StatusOperacji.ERROR.Equals(MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                            {
                                MyWorkItem.ShowMessageBox(MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, REALIZACJA_MP, MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                                return;
                            }

                            var listaNosnikowPresenter
                                = WorkItem.Items.Get<ListaNosnikowPresenter>(ItemsConstants.REALIZACJA_MR_LISTA_NOSNIKOW_PRESENTER);
                            listaNosnikowPresenter.WyswietlListe(AktualnaPozycja().PainId, lokalizacja, AktualnaPozycja().Ilosc);
                            MyWorkItem.MainWorkspace.Show(listaNosnikowPresenter.View);
                            NalezyZmienicLokalizacje = true;
                            return;
                        }

                        ZmienLokalizacje(lokalizacja, null);
                    }
                }
                else
                {
                    ZakonczPozycje();
                }
            }
        }

        [EventSubscription(EventBrokerConstants.WYBRANO_NOSNIK)]
        public void WybranoNosnik(Object sender, EventArgs e)
        {
            if (NalezyZmienicLokalizacje)
            {
                NalezyZmienicLokalizacje = false;
                ZmienLokalizacje(WorkItem.State[StateConstants.LOCALIZATION] as string, WorkItem.State[StateConstants.IDENTYFIKATOR_NOSNIKA] as long?);
            }
        }

        public void ZmienLokalizacje(string lokalizacja, long? nosnik)
        {
            if (AktualnaPozycja() != null)
            {
                View.ForceDataBinding();
                MyWorkItem.ZmienLokalizacje((long)listaPozycji[numerPozycji].Id, lokalizacja, nosnik);
                if (StatusOperacji.SUCCESS.Equals(WorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                {
                    listaPozycji[numerPozycji].KodLokalizacji = lokalizacja;
                    View.Pozycja = new PozycjaMP();
                    AktualizujWidok();

                    ZakonczPozycje();
                }
            }
        }

        private void ZakonczPozycje()
        {
            if (!listaPozycji[numerPozycji].Ilosc.HasValue)
            {
                MessageBox.Show("Pole ilo�� musi by� wype�nione", "Niepoprawne dane");
                MyWorkItem.Activate();
                return;
            }

            if (MyWorkItem.PoprawnaIloscUmieszczanaISkontrolowana(listaPozycji[numerPozycji].Id.Value, listaPozycji[numerPozycji].Ilosc.Value))
            {
                MyWorkItem.UstawZakonczonaPozycje((long)listaPozycji[numerPozycji].Id);

                if (StatusOperacji.SUCCESS.Equals(WorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                {
                    listaPozycji[numerPozycji].Zakonczona = DBBool.TRUE;
                    View.Pozycja = new PozycjaMP();
                    iloscPozycjiNiezakonczonych--;
                    AktualizujWidok();
                }
            }
        }

        private void AktualizujWidok()
        {
            if (listaPozycji[numerPozycji].Zakonczona.Equals(DBBool.TRUE))
            {
                View.KodLokalizacjiReadOnly = true;
                View.IloscReadOnly = true;
            }
            else
            {
                View.KodLokalizacjiReadOnly = NavigationState;
                View.IloscReadOnly = NavigationState;
            }

            if ((idAktualnegoDokumentu != listaPozycji[numerPozycji].IdDokumentu) && (listaPozycji[numerPozycji].IdDokumentu.HasValue))
            {
                iloscPozycjiNiezakonczonych = MyWorkItem.IloscPozycjiNaDokumencie((long)listaPozycji[numerPozycji].IdDokumentu,
                    kryteriaZapytania.Skontrolowana, "N");

                iloscPozycji = MyWorkItem.IloscPozycjiNaDokumencie((long)listaPozycji[numerPozycji].IdDokumentu,
                    kryteriaZapytania.Skontrolowana, kryteriaZapytania.Zakonczona);

                idAktualnegoDokumentu = listaPozycji[numerPozycji].IdDokumentu;

            }

            MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = listaPozycji[numerPozycji].SymbolTowaru;
            MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueYes;
            MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = listaPozycji[numerPozycji].SymbolTowaru;
            MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueYes;

            View.Pozycja = listaPozycji[numerPozycji];
            View.Pozostalo = iloscPozycjiNiezakonczonych;
            View.IloscPozycji = iloscPozycji;
        }

        public void DodajPozycjeDoWidoku(List<PozycjaMP> lista)
        {
            if (StatusOperacji.SUCCESS.Equals(WorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
            {
                iloscPozycji++;
                View.IloscPozycji = iloscPozycji;
                iloscPozycjiNiezakonczonych++;
                View.Pozostalo = iloscPozycjiNiezakonczonych;
                listaPozycji.Insert(numerPozycji, lista[0]);
                View.Pozycja = listaPozycji[numerPozycji];
            }
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.D1:
                    e.Handled = true;
                    ViewSzukaj(this, EventArgs.Empty);
                    break;
                case Keys.D2:
                    e.Handled = true;
                    ViewDodaj(this, EventArgs.Empty);
                    break;
                case Keys.D3:
                    if (MozliwePrzesuniecie)
                    {
                        e.Handled = true;
                        ViewPrzesun(this, EventArgs.Empty);
                    }
                    break;
                /*				case Keys.D4:
                                    e.Handled = true;
                                    view_Zakoncz(this, EventArgs.Empty);
                                    break;*/
                case Keys.Left:
                    e.Handled = true;
                    ViewPrzejdzDoPoprzedniejPozycji(this, EventArgs.Empty);
                    break;
                case Keys.Right:
                    e.Handled = true;
                    ViewPrzejdzDoNastepnejPozycji(this, EventArgs.Empty);
                    break;
                case Keys.Up:
                    e.Handled = true;
                    View.FocusNextControl(false);
                    break;
                case Keys.Down:
                    e.Handled = true;
                    View.FocusNextControl(true);
                    break;
                case Keys.D:
                    e.Handled = true;
                    uruchomRaport();
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
                    MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
                    MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;

                    //MyWorkItem.MainWorkspace.Close(View);
                    CloseView();
                    break;
            }
        }

        [EventSubscription(EventBrokerConstants.NAVIGATION_STATE_CHANGED)]
        public override void NavigationStateChanged(object sender, EventArgs e)
        {

            if (!MyWorkItem.MainWorkspace.Equals(this))
            {
                return;
            }

            base.NavigationStateChanged(sender, e);

            if (listaPozycji.Count > 0)
            {
                AktualizujWidok();
            }
            else
            {
                View.KodLokalizacjiReadOnly = NavigationState;
                View.IloscReadOnly = NavigationState;
            }
        }

        public void OnUserHasLoggedOn(object sender, EventArgs e)
        {
            if (MyWorkItem.RootWorkItem.State[StateConstants.USER] == null)
            {
                return;
            }

            View.PrzesunVisible = MozliwePrzesuniecie;
        }

        private bool MozliwePrzesuniecie
        {
            get
            {
                object mozliwePrzesuniecie = MyWorkItem.RootWorkItem.State[StateConstants.ASSORTIMENT_MOVE_RIGHTS];
                if (mozliwePrzesuniecie != null)
                {
                    return (bool)mozliwePrzesuniecie;
                }
                return false;
            }
        }

        private void uruchomRaport()
        {
            if ((listaPozycji != null) && (listaPozycji.Count > 0) && (listaPozycji[numerPozycji] != null)
                && (listaPozycji[numerPozycji].IdDokumentu != null) && (listaPozycji[numerPozycji].Id != null))
            {
                if (MyWorkItem.UruchomRaport(NAZWA_FORMULARZA, (long)listaPozycji[numerPozycji].IdDokumentu,
                    (long)listaPozycji[numerPozycji].Id))
                {
                    MessageBox.Show(POMYSLNE_WYWOLANIE_RAPORTU);
                }
            }
        }
    }
}
