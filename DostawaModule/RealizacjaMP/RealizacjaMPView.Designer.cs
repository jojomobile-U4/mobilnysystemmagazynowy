
using Common.Components;

namespace DostawaModule
{
    partial class RealizacjaMPView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbTytul = new Common.Components.MSMLabel();
            this.lbDokument = new System.Windows.Forms.Label();
            this.lbLokalizacje = new System.Windows.Forms.Label();
            this.pozycjaMPBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbIloscLP = new System.Windows.Forms.TextBox();
            this.lbSlash = new System.Windows.Forms.Label();
            this.tbLP = new System.Windows.Forms.TextBox();
            this.lbLP = new System.Windows.Forms.Label();
            this.tbLokalizacja = new System.Windows.Forms.TextBox();
            this.lbLokalizacja = new System.Windows.Forms.Label();
            this.tbSymbolTowaru = new System.Windows.Forms.TextBox();
            this.lbSymbolTowaru = new System.Windows.Forms.Label();
            this.tbNazwaTowaru = new System.Windows.Forms.TextBox();
            this.lbNazwaTowaru = new System.Windows.Forms.Label();
            this.btnSzukaj = new System.Windows.Forms.Button();
            this.tbIlosc = new System.Windows.Forms.TextBox();
            this.lbIlosc = new System.Windows.Forms.Label();
            this.tbZakonczona = new System.Windows.Forms.TextBox();
            this.lbZakonczona = new System.Windows.Forms.Label();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnPrzesun = new System.Windows.Forms.Button();
            this.btnZakoncz = new System.Windows.Forms.Button();
            this.tbDokument = new System.Windows.Forms.TextBox();
            this.btnPowrot = new System.Windows.Forms.Button();
            this.lrcNavigation = new Common.Components.LeftRightControl();
            this.lbPozostalo = new System.Windows.Forms.Label();
            this.tbPozostalo = new System.Windows.Forms.TextBox();
            this.pnlNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pozycjaMPBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnPowrot);
            this.pnlNavigation.Controls.Add(this.btnSzukaj);
            this.pnlNavigation.Controls.Add(this.btnZakoncz);
            this.pnlNavigation.Controls.Add(this.btnDodaj);
            this.pnlNavigation.Controls.Add(this.btnPrzesun);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 52);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 24;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "PRZYJ�CIE - REALIZACJA MP";
            // 
            // lbDokument
            // 
            this.lbDokument.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbDokument.Location = new System.Drawing.Point(4, 24);
            this.lbDokument.Name = "lbDokument";
            this.lbDokument.Size = new System.Drawing.Size(76, 20);
            this.lbDokument.Text = "Dokument:";
            // 
            // lbLokalizacje
            // 
            this.lbLokalizacje.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbLokalizacje.Location = new System.Drawing.Point(0, 47);
            this.lbLokalizacje.Name = "lbLokalizacje";
            this.lbLokalizacje.Size = new System.Drawing.Size(240, 16);
            this.lbLokalizacje.Text = "LOKALIZACJE";
            this.lbLokalizacje.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pozycjaMPBindingSource
            // 
            this.pozycjaMPBindingSource.DataSource = typeof(DostawaModule.PozycjaMP);
            // 
            // tbIloscLP
            // 
            this.tbIloscLP.Location = new System.Drawing.Point(152, 72);
            this.tbIloscLP.Name = "tbIloscLP";
            this.tbIloscLP.ReadOnly = true;
            this.tbIloscLP.Size = new System.Drawing.Size(53, 21);
            this.tbIloscLP.TabIndex = 2;
            this.tbIloscLP.TabStop = false;
            // 
            // lbSlash
            // 
            this.lbSlash.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSlash.Location = new System.Drawing.Point(137, 74);
            this.lbSlash.Name = "lbSlash";
            this.lbSlash.Size = new System.Drawing.Size(10, 18);
            this.lbSlash.Text = "/";
            // 
            // tbLP
            // 
            this.tbLP.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaMPBindingSource, "Lp", true));
            this.tbLP.Location = new System.Drawing.Point(80, 72);
            this.tbLP.Name = "tbLP";
            this.tbLP.ReadOnly = true;
            this.tbLP.Size = new System.Drawing.Size(53, 21);
            this.tbLP.TabIndex = 1;
            this.tbLP.TabStop = false;
            // 
            // lbLP
            // 
            this.lbLP.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbLP.Location = new System.Drawing.Point(3, 72);
            this.lbLP.Name = "lbLP";
            this.lbLP.Size = new System.Drawing.Size(77, 20);
            this.lbLP.Text = "LP/Ilo�� LP:";
            // 
            // tbLokalizacja
            // 
            this.tbLokalizacja.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaMPBindingSource, "KodLokalizacji", true));
            this.tbLokalizacja.Location = new System.Drawing.Point(80, 97);
            this.tbLokalizacja.Name = "tbLokalizacja";
            this.tbLokalizacja.Size = new System.Drawing.Size(157, 21);
            this.tbLokalizacja.TabIndex = 3;
            // 
            // lbLokalizacja
            // 
            this.lbLokalizacja.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbLokalizacja.Location = new System.Drawing.Point(3, 97);
            this.lbLokalizacja.Name = "lbLokalizacja";
            this.lbLokalizacja.Size = new System.Drawing.Size(77, 20);
            this.lbLokalizacja.Text = "Lokalizacja:";
            // 
            // tbSymbolTowaru
            // 
            this.tbSymbolTowaru.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaMPBindingSource, "SymbolTowaru", true));
            this.tbSymbolTowaru.Location = new System.Drawing.Point(80, 122);
            this.tbSymbolTowaru.Name = "tbSymbolTowaru";
            this.tbSymbolTowaru.ReadOnly = true;
            this.tbSymbolTowaru.Size = new System.Drawing.Size(157, 21);
            this.tbSymbolTowaru.TabIndex = 4;
            this.tbSymbolTowaru.TabStop = false;
            // 
            // lbSymbolTowaru
            // 
            this.lbSymbolTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbolTowaru.Location = new System.Drawing.Point(3, 122);
            this.lbSymbolTowaru.Name = "lbSymbolTowaru";
            this.lbSymbolTowaru.Size = new System.Drawing.Size(77, 20);
            this.lbSymbolTowaru.Text = "Indeks:";
            // 
            // tbNazwaTowaru
            // 
            this.tbNazwaTowaru.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaMPBindingSource, "NazwaTowaru", true));
            this.tbNazwaTowaru.Location = new System.Drawing.Point(80, 147);
            this.tbNazwaTowaru.Name = "tbNazwaTowaru";
            this.tbNazwaTowaru.ReadOnly = true;
            this.tbNazwaTowaru.Size = new System.Drawing.Size(157, 21);
            this.tbNazwaTowaru.TabIndex = 5;
            this.tbNazwaTowaru.TabStop = false;
            // 
            // lbNazwaTowaru
            // 
            this.lbNazwaTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbNazwaTowaru.Location = new System.Drawing.Point(3, 147);
            this.lbNazwaTowaru.Name = "lbNazwaTowaru";
            this.lbNazwaTowaru.Size = new System.Drawing.Size(77, 20);
            this.lbNazwaTowaru.Text = "Nazwa towaru:";
            // 
            // btnSzukaj
            // 
            this.btnSzukaj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnSzukaj.Location = new System.Drawing.Point(4, 5);
            this.btnSzukaj.Name = "btnSzukaj";
            this.btnSzukaj.Size = new System.Drawing.Size(75, 20);
            this.btnSzukaj.TabIndex = 8;
            this.btnSzukaj.TabStop = false;
            this.btnSzukaj.Text = "&1 Szukaj";
            this.btnSzukaj.Click += new System.EventHandler(this.btnSzukaj_Click);
            // 
            // tbIlosc
            // 
            this.tbIlosc.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaMPBindingSource, "Ilosc", true));
            this.tbIlosc.Location = new System.Drawing.Point(80, 172);
            this.tbIlosc.Name = "tbIlosc";
            this.tbIlosc.Size = new System.Drawing.Size(67, 21);
            this.tbIlosc.TabIndex = 6;
            // 
            // lbIlosc
            // 
            this.lbIlosc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbIlosc.Location = new System.Drawing.Point(4, 172);
            this.lbIlosc.Name = "lbIlosc";
            this.lbIlosc.Size = new System.Drawing.Size(76, 20);
            this.lbIlosc.Text = "Ilo��:";
            // 
            // tbZakonczona
            // 
            this.tbZakonczona.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaMPBindingSource, "Zakonczona", true));
            this.tbZakonczona.Location = new System.Drawing.Point(80, 197);
            this.tbZakonczona.Name = "tbZakonczona";
            this.tbZakonczona.ReadOnly = true;
            this.tbZakonczona.Size = new System.Drawing.Size(67, 21);
            this.tbZakonczona.TabIndex = 7;
            this.tbZakonczona.TabStop = false;
            // 
            // lbZakonczona
            // 
            this.lbZakonczona.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbZakonczona.Location = new System.Drawing.Point(3, 197);
            this.lbZakonczona.Name = "lbZakonczona";
            this.lbZakonczona.Size = new System.Drawing.Size(77, 20);
            this.lbZakonczona.Text = "Zako�czona:";
            // 
            // btnDodaj
            // 
            this.btnDodaj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnDodaj.Location = new System.Drawing.Point(82, 5);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(75, 20);
            this.btnDodaj.TabIndex = 9;
            this.btnDodaj.TabStop = false;
            this.btnDodaj.Text = "&2 Dodaj";
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnPrzesun
            // 
            this.btnPrzesun.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPrzesun.Location = new System.Drawing.Point(4, 28);
            this.btnPrzesun.Name = "btnPrzesun";
            this.btnPrzesun.Size = new System.Drawing.Size(75, 20);
            this.btnPrzesun.TabIndex = 10;
            this.btnPrzesun.TabStop = false;
            this.btnPrzesun.Text = "&3 Przesu�";
            this.btnPrzesun.Click += new System.EventHandler(this.btnPrzesun_Click);
            // 
            // btnZakoncz
            // 
            this.btnZakoncz.Enabled = false;
            this.btnZakoncz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnZakoncz.Location = new System.Drawing.Point(82, 28);
            this.btnZakoncz.Name = "btnZakoncz";
            this.btnZakoncz.Size = new System.Drawing.Size(75, 20);
            this.btnZakoncz.TabIndex = 11;
            this.btnZakoncz.TabStop = false;
            this.btnZakoncz.Text = "&4 Zako�cz";
            this.btnZakoncz.Click += new System.EventHandler(this.btnZakoncz_Click);
            // 
            // tbDokument
            // 
            this.tbDokument.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaMPBindingSource, "SymbolDokumentu", true));
            this.tbDokument.Location = new System.Drawing.Point(80, 24);
            this.tbDokument.Name = "tbDokument";
            this.tbDokument.ReadOnly = true;
            this.tbDokument.Size = new System.Drawing.Size(157, 21);
            this.tbDokument.TabIndex = 0;
            this.tbDokument.TabStop = false;
            // 
            // btnPowrot
            // 
            this.btnPowrot.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPowrot.Location = new System.Drawing.Point(160, 28);
            this.btnPowrot.Name = "btnPowrot";
            this.btnPowrot.Size = new System.Drawing.Size(77, 20);
            this.btnPowrot.TabIndex = 25;
            this.btnPowrot.TabStop = false;
            this.btnPowrot.Text = "&Esc Powr�t";
            this.btnPowrot.Click += new System.EventHandler(this.btnPowrot_Click);
            // 
            // lrcNavigation
            // 
            this.lrcNavigation.BackColor = System.Drawing.SystemColors.Desktop;
            this.lrcNavigation.Location = new System.Drawing.Point(205, -1);
            this.lrcNavigation.Name = "lrcNavigation";
            this.lrcNavigation.NextEnabled = true;
            this.lrcNavigation.PreviousEnabled = true;
            this.lrcNavigation.Size = new System.Drawing.Size(32, 16);
            this.lrcNavigation.TabIndex = 25;
            this.lrcNavigation.TabStop = false;
            this.lrcNavigation.Next += new System.EventHandler(this.btnRight_Click);
            this.lrcNavigation.Previous += new System.EventHandler(this.btnLeft_Click);
            // 
            // lbPozostalo
            // 
            this.lbPozostalo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbPozostalo.Location = new System.Drawing.Point(3, 224);
            this.lbPozostalo.Name = "lbPozostalo";
            this.lbPozostalo.Size = new System.Drawing.Size(77, 20);
            this.lbPozostalo.Text = "Pozosta�o:";
            // 
            // tbPozostalo
            // 
            this.tbPozostalo.Location = new System.Drawing.Point(80, 222);
            this.tbPozostalo.Name = "tbPozostalo";
            this.tbPozostalo.ReadOnly = true;
            this.tbPozostalo.Size = new System.Drawing.Size(67, 21);
            this.tbPozostalo.TabIndex = 36;
            this.tbPozostalo.TabStop = false;
            // 
            // RealizacjaMPView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lbPozostalo);
            this.Controls.Add(this.tbPozostalo);
            this.Controls.Add(this.lrcNavigation);
            this.Controls.Add(this.tbDokument);
            this.Controls.Add(this.lbZakonczona);
            this.Controls.Add(this.tbZakonczona);
            this.Controls.Add(this.lbIlosc);
            this.Controls.Add(this.tbIlosc);
            this.Controls.Add(this.lbNazwaTowaru);
            this.Controls.Add(this.tbNazwaTowaru);
            this.Controls.Add(this.lbSymbolTowaru);
            this.Controls.Add(this.tbSymbolTowaru);
            this.Controls.Add(this.lbLokalizacja);
            this.Controls.Add(this.tbLokalizacja);
            this.Controls.Add(this.lbLP);
            this.Controls.Add(this.tbLP);
            this.Controls.Add(this.lbSlash);
            this.Controls.Add(this.tbIloscLP);
            this.Controls.Add(this.lbLokalizacje);
            this.Controls.Add(this.lbDokument);
            this.Controls.Add(this.lbTytul);
            this.Name = "RealizacjaMPView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lbDokument, 0);
            this.Controls.SetChildIndex(this.lbLokalizacje, 0);
            this.Controls.SetChildIndex(this.tbIloscLP, 0);
            this.Controls.SetChildIndex(this.lbSlash, 0);
            this.Controls.SetChildIndex(this.tbLP, 0);
            this.Controls.SetChildIndex(this.lbLP, 0);
            this.Controls.SetChildIndex(this.tbLokalizacja, 0);
            this.Controls.SetChildIndex(this.lbLokalizacja, 0);
            this.Controls.SetChildIndex(this.tbSymbolTowaru, 0);
            this.Controls.SetChildIndex(this.lbSymbolTowaru, 0);
            this.Controls.SetChildIndex(this.tbNazwaTowaru, 0);
            this.Controls.SetChildIndex(this.lbNazwaTowaru, 0);
            this.Controls.SetChildIndex(this.tbIlosc, 0);
            this.Controls.SetChildIndex(this.lbIlosc, 0);
            this.Controls.SetChildIndex(this.tbZakonczona, 0);
            this.Controls.SetChildIndex(this.lbZakonczona, 0);
            this.Controls.SetChildIndex(this.tbDokument, 0);
            this.Controls.SetChildIndex(this.lrcNavigation, 0);
            this.Controls.SetChildIndex(this.tbPozostalo, 0);
            this.Controls.SetChildIndex(this.lbPozostalo, 0);
            this.pnlNavigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pozycjaMPBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MSMLabel lbTytul;
        private System.Windows.Forms.Label lbDokument;
        private System.Windows.Forms.Label lbLokalizacje;
        private System.Windows.Forms.TextBox tbIloscLP;
        private System.Windows.Forms.Label lbSlash;
        private System.Windows.Forms.TextBox tbLP;
        private System.Windows.Forms.Label lbLP;
        private System.Windows.Forms.TextBox tbLokalizacja;
        private System.Windows.Forms.Label lbLokalizacja;
        private System.Windows.Forms.TextBox tbSymbolTowaru;
        private System.Windows.Forms.Label lbSymbolTowaru;
        private System.Windows.Forms.TextBox tbNazwaTowaru;
        private System.Windows.Forms.Label lbNazwaTowaru;
        private System.Windows.Forms.Button btnSzukaj;
        private System.Windows.Forms.TextBox tbIlosc;
        private System.Windows.Forms.Label lbIlosc;
        private System.Windows.Forms.TextBox tbZakonczona;
        private System.Windows.Forms.Label lbZakonczona;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnPrzesun;
        private System.Windows.Forms.Button btnZakoncz;
        private System.Windows.Forms.BindingSource pozycjaMPBindingSource;
        private System.Windows.Forms.TextBox tbDokument;
        private System.Windows.Forms.Button btnPowrot;
        private LeftRightControl lrcNavigation;
		private System.Windows.Forms.Label lbPozostalo;
		private System.Windows.Forms.TextBox tbPozostalo;
    }
}
