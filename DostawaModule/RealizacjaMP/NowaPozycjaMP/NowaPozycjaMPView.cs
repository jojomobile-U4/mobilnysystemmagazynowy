using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using Common.Base;

namespace DostawaModule
{
    public partial class NowaPozycjaMPView : ViewBase, INowaPozycjaMPView
    {
        public event EventHandler Dodaj;
        public event EventHandler Anuluj;

        private long? idPozycji;

        public NowaPozycjaMPView()
        {
            InitializeComponent();
			InitializeFocusedControl();
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            if (Dodaj != null)
            {
                Dodaj(this, null);
            }
        }

        private void btnAnuluj_Click(object sender, EventArgs e)
        {
            if (Anuluj != null)
            {
                Anuluj(this, null);
            }
        }

        public PozycjaMP Pozycja
        {
            get
            {
                return pozycjaMPBindingSource.DataSource as PozycjaMP;
            }
            set {
                pozycjaMPBindingSource.DataSource = value;
                idPozycji = value.Id;
            }
        }

        public string Lokalizacja
        {
            get 
            {
                return this.tbLokalizacja.Text;
            }
			set
			{
				tbLokalizacja.Text = value;
			}
        }

        public float? Ilosc
        {
            get
            {
                try
                {
                    return float.Parse(tbIlosc.Text);
                }
                catch (FormatException)
                {
                    return null;
                }
            }

        }

        public long? IdPozycji
        {
            get
            {
                return idPozycji;
            }
		}

		#region Override Members

		public override void SetNavigationState(bool navigationState)
		{
			base.SetNavigationState(navigationState);

			tbIlosc.ReadOnly = navigationState;
			tbLokalizacja.ReadOnly = navigationState;
		}

		#endregion
	}
}
