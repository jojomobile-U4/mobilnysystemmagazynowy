namespace DostawaModule
{
    partial class NowaPozycjaMPView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnAnuluj = new System.Windows.Forms.Button();
            this.lbTytul = new Common.Components.MSMLabel();
            this.pozycjaMPBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbDokument = new System.Windows.Forms.TextBox();
            this.lbDokument = new System.Windows.Forms.Label();
            this.tbSymbolTowaru = new System.Windows.Forms.TextBox();
            this.lbSymbolTowaru = new System.Windows.Forms.Label();
            this.tbNazwaTowaru = new System.Windows.Forms.TextBox();
            this.lbNazwaTowaru = new System.Windows.Forms.Label();
            this.tbLokalizacja = new System.Windows.Forms.TextBox();
            this.lbLokalizacja = new System.Windows.Forms.Label();
            this.tbIlosc = new System.Windows.Forms.TextBox();
            this.lbIlosc = new System.Windows.Forms.Label();
            this.pnlNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pozycjaMPBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnDodaj);
            this.pnlNavigation.Controls.Add(this.btnAnuluj);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 228);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 52);
            // 
            // btnDodaj
            // 
            this.btnDodaj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnDodaj.Location = new System.Drawing.Point(74, 29);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(80, 20);
            this.btnDodaj.TabIndex = 5;
            this.btnDodaj.TabStop = false;
            this.btnDodaj.Text = "&Ret Dodaj";
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnAnuluj.Location = new System.Drawing.Point(157, 29);
            this.btnAnuluj.Name = "btnAnuluj";
            this.btnAnuluj.Size = new System.Drawing.Size(80, 20);
            this.btnAnuluj.TabIndex = 6;
            this.btnAnuluj.TabStop = false;
            this.btnAnuluj.Text = "&Esc Anuluj";
            this.btnAnuluj.Click += new System.EventHandler(this.btnAnuluj_Click);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 9;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "REALIZACJA MP - NOWA POZYCJA";
            // 
            // pozycjaMPBindingSource
            // 
            this.pozycjaMPBindingSource.DataSource = typeof(DostawaModule.PozycjaMP);
            // 
            // tbDokument
            // 
            this.tbDokument.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaMPBindingSource, "SymbolDokumentu", true));
            this.tbDokument.Location = new System.Drawing.Point(88, 34);
            this.tbDokument.Name = "tbDokument";
            this.tbDokument.ReadOnly = true;
            this.tbDokument.Size = new System.Drawing.Size(149, 21);
            this.tbDokument.TabIndex = 0;
            this.tbDokument.TabStop = false;
            // 
            // lbDokument
            // 
            this.lbDokument.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbDokument.Location = new System.Drawing.Point(3, 34);
            this.lbDokument.Name = "lbDokument";
            this.lbDokument.Size = new System.Drawing.Size(85, 20);
            this.lbDokument.Text = "Dokument:";
            // 
            // tbSymbolTowaru
            // 
            this.tbSymbolTowaru.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaMPBindingSource, "SymbolTowaru", true));
            this.tbSymbolTowaru.Location = new System.Drawing.Point(88, 59);
            this.tbSymbolTowaru.Name = "tbSymbolTowaru";
            this.tbSymbolTowaru.ReadOnly = true;
            this.tbSymbolTowaru.Size = new System.Drawing.Size(149, 21);
            this.tbSymbolTowaru.TabIndex = 1;
            this.tbSymbolTowaru.TabStop = false;
            // 
            // lbSymbolTowaru
            // 
            this.lbSymbolTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbolTowaru.Location = new System.Drawing.Point(3, 59);
            this.lbSymbolTowaru.Name = "lbSymbolTowaru";
            this.lbSymbolTowaru.Size = new System.Drawing.Size(85, 20);
            this.lbSymbolTowaru.Text = "Indeks:";
            // 
            // tbNazwaTowaru
            // 
            this.tbNazwaTowaru.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaMPBindingSource, "NazwaTowaru", true));
            this.tbNazwaTowaru.Location = new System.Drawing.Point(88, 84);
            this.tbNazwaTowaru.Name = "tbNazwaTowaru";
            this.tbNazwaTowaru.ReadOnly = true;
            this.tbNazwaTowaru.Size = new System.Drawing.Size(149, 21);
            this.tbNazwaTowaru.TabIndex = 2;
            this.tbNazwaTowaru.TabStop = false;
            // 
            // lbNazwaTowaru
            // 
            this.lbNazwaTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbNazwaTowaru.Location = new System.Drawing.Point(3, 84);
            this.lbNazwaTowaru.Name = "lbNazwaTowaru";
            this.lbNazwaTowaru.Size = new System.Drawing.Size(85, 20);
            this.lbNazwaTowaru.Text = "Nazwa towaru:";
            // 
            // tbLokalizacja
            // 
            this.tbLokalizacja.Location = new System.Drawing.Point(88, 109);
            this.tbLokalizacja.Name = "tbLokalizacja";
            this.tbLokalizacja.Size = new System.Drawing.Size(149, 21);
            this.tbLokalizacja.TabIndex = 3;
            // 
            // lbLokalizacja
            // 
            this.lbLokalizacja.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbLokalizacja.Location = new System.Drawing.Point(3, 109);
            this.lbLokalizacja.Name = "lbLokalizacja";
            this.lbLokalizacja.Size = new System.Drawing.Size(85, 20);
            this.lbLokalizacja.Text = "Lokalizacja:";
            // 
            // tbIlosc
            // 
            this.tbIlosc.Location = new System.Drawing.Point(88, 134);
            this.tbIlosc.Name = "tbIlosc";
            this.tbIlosc.Size = new System.Drawing.Size(71, 21);
            this.tbIlosc.TabIndex = 4;
            this.tbIlosc.Text = "0";
            // 
            // lbIlosc
            // 
            this.lbIlosc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbIlosc.Location = new System.Drawing.Point(3, 135);
            this.lbIlosc.Name = "lbIlosc";
            this.lbIlosc.Size = new System.Drawing.Size(85, 20);
            this.lbIlosc.Text = "Ilo��:";
            // 
            // NowaPozycjaMPView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lbIlosc);
            this.Controls.Add(this.tbIlosc);
            this.Controls.Add(this.lbLokalizacja);
            this.Controls.Add(this.tbLokalizacja);
            this.Controls.Add(this.lbNazwaTowaru);
            this.Controls.Add(this.tbNazwaTowaru);
            this.Controls.Add(this.lbSymbolTowaru);
            this.Controls.Add(this.tbSymbolTowaru);
            this.Controls.Add(this.lbDokument);
            this.Controls.Add(this.tbDokument);
            this.Controls.Add(this.lbTytul);
            this.Name = "NowaPozycjaMPView";
            this.Size = new System.Drawing.Size(240, 280);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.tbDokument, 0);
            this.Controls.SetChildIndex(this.lbDokument, 0);
            this.Controls.SetChildIndex(this.tbSymbolTowaru, 0);
            this.Controls.SetChildIndex(this.lbSymbolTowaru, 0);
            this.Controls.SetChildIndex(this.tbNazwaTowaru, 0);
            this.Controls.SetChildIndex(this.lbNazwaTowaru, 0);
            this.Controls.SetChildIndex(this.tbLokalizacja, 0);
            this.Controls.SetChildIndex(this.lbLokalizacja, 0);
            this.Controls.SetChildIndex(this.tbIlosc, 0);
            this.Controls.SetChildIndex(this.lbIlosc, 0);
            this.pnlNavigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pozycjaMPBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnAnuluj;
        private Common.Components.MSMLabel lbTytul;
        private System.Windows.Forms.TextBox tbDokument;
        private System.Windows.Forms.Label lbDokument;
        private System.Windows.Forms.TextBox tbSymbolTowaru;
        private System.Windows.Forms.Label lbSymbolTowaru;
        private System.Windows.Forms.TextBox tbNazwaTowaru;
        private System.Windows.Forms.Label lbNazwaTowaru;
        private System.Windows.Forms.TextBox tbLokalizacja;
        private System.Windows.Forms.Label lbLokalizacja;
        private System.Windows.Forms.TextBox tbIlosc;
        private System.Windows.Forms.Label lbIlosc;
        private System.Windows.Forms.BindingSource pozycjaMPBindingSource;
    }
}
