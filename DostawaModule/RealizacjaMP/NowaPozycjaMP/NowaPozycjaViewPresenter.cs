using System;
using Common.DataModel;
using DostawaModule.RealizacjaMP.ListaNosnikow;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using System.Windows.Forms;
using Common.Base;
using Common;

namespace DostawaModule
{
    public class NowaPozycjaMPViewPresenter : PresenterBase
    {
        private const string RealizacjaMp = "Realizacja MP";
        private bool NalezyDodacPozycje;
        private PozycjaMP Pozycja;

        protected RealizacjaMPWorkItem MyWorkItem
        {
            get { return WorkItem as RealizacjaMPWorkItem; }
        }

        public PozycjaMP PozycjaMp
        {
            get { return Pozycja; }
            set
            {
                Pozycja = value;
                View.Pozycja = value;
            }
        }

        public INowaPozycjaMPView View
        {
            get { return m_view as INowaPozycjaMPView; }
        }

        public NowaPozycjaMPViewPresenter(INowaPozycjaMPView view)
            : base(view)
        {
        }

        protected override void AttachView()
        {
            View.Dodaj += ViewDodaj;
            View.Anuluj += ViewAnuluj;
        }


        private void ViewDodaj(Object sender, EventArgs e)
        {
            Pozycja = View.Pozycja;
            Pozycja.KodLokalizacji = View.Lokalizacja;
            Pozycja.Ilosc = View.Ilosc;
            if (!MyWorkItem.MoznaJednoznacznieOkreslicNosnik(Pozycja.PainId, Pozycja.KodLokalizacji, Pozycja.Ilosc))
            {
                if (StatusOperacji.ERROR.Equals(MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                {
                    MyWorkItem.ShowMessageBox(MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, RealizacjaMp, MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                    return;
                }
                var listaNosnikowPresenter
                    = WorkItem.Items.Get<ListaNosnikowPresenter>(ItemsConstants.REALIZACJA_MR_LISTA_NOSNIKOW_PRESENTER);
                listaNosnikowPresenter.WyswietlListe(Pozycja.PainId, Pozycja.KodLokalizacji, Pozycja.Ilosc);
                MyWorkItem.MainWorkspace.Show(listaNosnikowPresenter.View);
                NalezyDodacPozycje = true;
                return;

            }
            DodajPozycje((long)View.IdPozycji, View.Lokalizacja, null, (float)View.Ilosc);
            MyWorkItem.MainWorkspace.Close(View);
            MyWorkItem.MainWorkspace.Show(
                WorkItem.Items.Get<RealizacjaMPViewPresenter>(ItemsConstants.REALIZACJA_MP_PRESENTER).View);
        }

        [EventSubscription(EventBrokerConstants.WYBRANO_NOSNIK)]
        public void WybranoNosnik(Object sender, EventArgs e)
        {
            if (NalezyDodacPozycje)
            {
                NalezyDodacPozycje = false;
                DodajPozycje((long)Pozycja.Id, Pozycja.KodLokalizacji, WorkItem.State[StateConstants.IDENTYFIKATOR_NOSNIKA] as long?, (float)Pozycja.Ilosc);
                MyWorkItem.MainWorkspace.Close(View);
                MyWorkItem.MainWorkspace.Show(
                    WorkItem.Items.Get<RealizacjaMPViewPresenter>(ItemsConstants.REALIZACJA_MP_PRESENTER).View);
            }
        }
        public void DodajPozycje(long idPozycji, string lokalizacja, long? nosnId, float ilosc)
        {
            MyWorkItem.DodajPozycje(idPozycji, lokalizacja, nosnId, ilosc);
        }

        private void ViewAnuluj(Object sender, EventArgs e)
        {
            WorkItemBase.MainWorkspace.Close(View);
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.D:
                    e.Handled = true;
                    ViewDodaj(this, EventArgs.Empty);
                    break;
                case Keys.A:
                    e.Handled = true;
                    ViewAnuluj(this, EventArgs.Empty);
                    break;
                case Keys.Enter:
                    e.Handled = true;
                    ViewDodaj(this, EventArgs.Empty);
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    ViewAnuluj(this, EventArgs.Empty);
                    break;
                case Keys.Up:
                    e.Handled = true;
                    View.FocusNextControl(false);
                    break;
                case Keys.Down:
                    e.Handled = true;
                    View.FocusNextControl(true);
                    break;
            }
        }
    }
}
