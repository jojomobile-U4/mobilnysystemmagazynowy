using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Common.Base;

namespace DostawaModule
{
    public interface INowaPozycjaMPView : IViewBase
    {
        event EventHandler Dodaj;
        event EventHandler Anuluj;
        PozycjaMP Pozycja { get; set; }
        string Lokalizacja { get; set; }
        float? Ilosc { get; }
        long? IdPozycji { get; }
    }
}
