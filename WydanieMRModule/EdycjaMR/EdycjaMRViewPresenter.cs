#region

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.DataModel;
using Common.WagiProxy;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using WydanieMRModule.DataModel;
using WydanieMRModule.Services;

#endregion

namespace WydanieMRModule.EdycjaMR
{
    public class EdycjaMRViewPresenter : PresenterBase
    {
        #region Constants

        private const int Aktualna = 0;
        /*
                private const int BRAK = 0;
        */
        private const string DocumentHasBeenApproved = "Dokument zosta� ju� zatwierdzony.";
        private const string ItemHasBeenFinished = "Pozycja zosta�a ju� zako�czona.";
        private const string KonfliktLokalizacjiWzorzec = "Wyst�puje konflikt lokalizacji.\nW celu potwierdzenia pobrania zaczytaj kod lokalizacji: {0}.\nKod aktualnie zaczytanej lokalizacji: {1}";
        private const int Nastepna = 1;
        public const string NazwaFormularza = "WYDANIE - EDYCJA MR";
        private const int Pierwsza = 0;
        private const string PomyslneWywolanieRaportu = "Wydruk zosta� wys�any na drukark�.";
        private const int Poprzednia = -1;
        private const string Rezerwowany = "R";
        private const string ZakonczPozycje = "&Ret Zako�cz";
        private const string ZatwierdzDokument = "&Ret Zatwierd�";
        private const long MIloscZerowana = 0;
        private const string MMessageBoxKodPrzypisany = "Kod przypisany do indeksu {1} czy wyda� towar ?";
        private const string MMessageBoxkodPrzypisanyCaption = "Kody EAN";
        private const string MMessageBoxKodNiePrzypisany = "Kod kreskowy nie zosta� przypisany do �adnego indeksu. Czy wyda� towar?";
        private const string Info = "Informacja";
        private const string MessageProszeZaczytacLokalizacje = "Prosz� zaczyta� kod lokalizacji indeksu";

        #endregion

        #region Private Fields

        private Dokument m_DokumentMr;
        private int m_IloscUsunietychPozycji;
        private List<Pozycja> m_ListaPozycji;
        private int m_NumerStrony;
        private int m_ZmianaStrony;

        #endregion

        #region Properties

        public IEdycjaMRView View
        {
            get { return m_view as IEdycjaMRView; }
        }

        protected EdycjaMRWorkItem MyWorkItem
        {
            get { return WorkItem as EdycjaMRWorkItem; }
        }

        public Pozycja AktualnaPozycja { get; set; }

        public Dokument AktualnyDokument { get; private set; }

        protected bool MozliweZakonczeniePozycji
        {
            get { return AktualnaPozycja != null && AktualnaPozycja.Zakonczona.Equals(DBBool.FALSE) && AktualnaPozycja.PotwierdzonaLokalizacja; }
        }

        protected bool MozliweZatwierdzenieDokumentu
        {
            get
            {
                return m_DokumentMr.Naglowek.IloscNiezakonczonychPozycji == 0 && m_DokumentMr.Naglowek.Status.Equals(Rezerwowany)
                       && (!m_DokumentMr.Naglowek.Zatwierdzony.Equals("T"));
            }
        }

        protected bool MozliweUsuwaniePozycji
        {
            get
            {
                return false;
                /*return m_DokumentMr.Naglowek.Status.Equals(Rezerwowany)
                       && (!m_DokumentMr.Naglowek.Zatwierdzony.Equals("T"));*/
            }
        }

        public bool IsCloseView { get; set; }

        public void AktualizujIlosc(float ilosc)
        {
            if (AktualnaPozycja != null && View.Ilosc != null)
            {
                AktualnaPozycja.Ilosc = ilosc;
                View.Ilosc = ilosc;
            }
        }

        #endregion

        #region Constructor

        public EdycjaMRViewPresenter(IEdycjaMRView view)
            : base(view)
        {
            ResetujStronicowanie();
        }

        #endregion

        #region Events

        [EventPublication(EventBrokerConstants.ZATWIERDZONO_DOKUMENT_MR)]
        public event EventHandler ZatwierdzonoDokumentMR;

        #endregion

        #region Methods

        public void ZaladujDaneDoWidoku(List<Dokument> listaDokumentow)
        {
            Dokument dokumentMR = null;
            if ((listaDokumentow != null) && (listaDokumentow.Count > 0))
                dokumentMR = listaDokumentow[0];

            var numerAktualnejPozycji = 0;
            if (m_IloscUsunietychPozycji > 0)
            {
                numerAktualnejPozycji = MyWorkItem.Configuration.WydanieMRWS.WielkoscStrony - m_IloscUsunietychPozycji;
            }

            m_IloscUsunietychPozycji = 0;

            if (dokumentMR == null ||
                dokumentMR.Pozycje.Count == 0)
            {
                m_DokumentMr = dokumentMR ?? new Dokument();
                m_ListaPozycji = m_DokumentMr.Pozycje;
                AktualnaPozycja = null;
            }
            else
            {
                m_DokumentMr = dokumentMR;
                m_ListaPozycji = m_DokumentMr.Pozycje;
                if (m_ZmianaStrony == Poprzednia)
                {
                    AktualnaPozycja = m_ListaPozycji[m_ListaPozycji.Count - 1];
                }
                else if (m_ZmianaStrony == Aktualna)
                {
                    if (numerAktualnejPozycji >= m_ListaPozycji.Count)
                        numerAktualnejPozycji = m_ListaPozycji.Count - 1;

                    AktualnaPozycja = m_ListaPozycji[numerAktualnejPozycji];
                }
                else
                {
                    AktualnaPozycja = m_ListaPozycji[0];
                }
            }

            if (AktualnaPozycja != null)
                WczytajMase(new Masa { wartosc = AktualnaPozycja.Waga, jednostka = AktualnaPozycja.Jm });

            View.ForceDataBinding();
            AktualizujWidok();

            MyWorkItem.SetStateValues();
        }

        internal void ResetujStronicowanie()
        {
            m_NumerStrony = Pierwsza;
            m_ZmianaStrony = Nastepna;
        }

        public void PotwierdzLokalizacjePozycji(string lokalizacja)
        {
            if (AktualnaPozycja == null)
            {
                return;
            }

            if (AktualnaPozycja.Zakonczona.Equals("T"))
            {
                MyWorkItem.WyswietlOstrzezenie(ItemHasBeenFinished);
                return;
            }

            if (m_DokumentMr != null && m_DokumentMr.Naglowek.Zatwierdzony.Equals("T"))
            {
                MyWorkItem.WyswietlOstrzezenie(DocumentHasBeenApproved);
                return;
            }

            if (AktualnaPozycja.Lokalizacja.Equals(lokalizacja))
            {
                AktualnaPozycja.PotwierdzonaLokalizacja = true;
                View.ZakonczEnabled = true;
                View.IloscReadOnly = false;
                WorkItem.Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                View.ForceDataBinding();
            }
            else
            {
                View.IloscReadOnly = true;
                MyWorkItem.WyswietlOstrzezenie(string.Format(KonfliktLokalizacjiWzorzec, AktualnaPozycja.Lokalizacja, lokalizacja));
            }
        }

        public void WczytajMase(Masa masa)
        {
            if (masa != null)
            {
                View.Waga = masa;
            }
            else
            {
                View.Waga = null;
            }
        }

        #endregion

        #region Obsluga widoku

        private void ViewNastepny(object sender, EventArgs e)
        {
            NastepnyRekord();
        }

        public void NastepnyRekord()
        {
            //sprawdzenie czy nie jestesmy na ostatniej stronie
            if (AktualnaPozycja != null)
            {
                View.ForceDataBinding();

                var indeksAktualnej = m_ListaPozycji.IndexOf(AktualnaPozycja);
                if (indeksAktualnej < m_ListaPozycji.Count - 1)
                {
                    AktualnaPozycja = m_ListaPozycji[indeksAktualnej + 1];
                    AktualizujWidok();

                }
                else
                {
                    //Sprawdzic czy jest nastepna paczka (strona)

                    //jezeli nie ma indeksow, znaczy ze nie ma dalszych stron
                    if (m_ListaPozycji.Count + m_IloscUsunietychPozycji == MyWorkItem.Configuration.WydanieMRWS.WielkoscStrony)
                    {
                        if (m_IloscUsunietychPozycji > 0)
                        {
                            ZmienStrone(Aktualna);
                        }
                        else
                        {
                            ZmienStrone(Nastepna);
                        }
                    }
                }
                MyWorkItem.SetStateValues();
            }
        }

        private void ViewPoprzedni(object sender, EventArgs e)
        {
            if (AktualnaPozycja != null)
            {
                View.ForceDataBinding();

                var indeksAktualnego = m_ListaPozycji.IndexOf(AktualnaPozycja);
                if (indeksAktualnego > 0)
                {
                    AktualnaPozycja = m_ListaPozycji[indeksAktualnego - 1];
                    AktualizujWidok();
                }
                else
                {
                    //Sprawdzic czy jest poprzednia paczka (strona)

                    //jezeli numer strony = 0, znaczy ze nie ma wczesniejszych stron
                    if (m_NumerStrony > 0)
                    {
                        ZmienStrone(Poprzednia);
                    }
                }
            }
            else
            {
                if (m_NumerStrony > 0)
                {
                    ZmienStrone(Poprzednia);
                }
            }
            MyWorkItem.SetStateValues();
        }

        private void ViewZakoncz(object sender, EventArgs e)
        {
            if (AktualnaPozycja != null &&
                AktualnaPozycja.PotwierdzonaLokalizacja)
            {
                ZakonczPozycje2();

            }
            else
            {
                //if (m_DokumentMr != null && m_DokumentMr.Naglowek.IloscNiezakonczonychPozycji == 0)

                var pozycjaMRstatus = MyWorkItem.Services.Get<IWydanieMRService>(true).IloscNiezakonczonychPozycji(m_DokumentMr.Naglowek.Id);

                if (m_DokumentMr != null && pozycjaMRstatus <= 0)
                {
                    Zatwierdz();
                }
            }
            MyWorkItem.RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueYes;
            MyWorkItem.RootWorkItem.State[StateConstants.BlokadaWywolaniaFormatkiK] = StateConstants.BoolValueNo;

        }

        private void ZakonczPozycje2()
        {
                View.ForceDataBinding();

                if ((AktualnaPozycja.Zakonczona != DBBool.TRUE) && (MyWorkItem.Zakoncz(AktualnaPozycja)))
                {
                    AktualnaPozycja.Zakonczona = DBBool.TRUE;
                    View.DataSource = null;

                    if (MyWorkItem.RootWorkItem.State[StateConstants.WydanieMRModuleKontekstView] != null && MyWorkItem.RootWorkItem.State[StateConstants.WydanieMRModuleKontekstView].ToString() != ContextConstants.WydanieMRPrzejecie.ToString())
                    {
                        m_DokumentMr.Naglowek.IloscNiezakonczonychPozycji -= 1;
                        UsunPozycjeZListy(AktualnaPozycja);
                        AktualizujWidok();
                    }
                }

                var pozycjaMRstatus = MyWorkItem.Services.Get<IWydanieMRService>(true).IloscNiezakonczonychPozycji(AktualnaPozycja.IdDokumentu);
                if (pozycjaMRstatus <= 0)
                {
                    Zatwierdz();
                }
                else
                {
                    if (MyWorkItem.RootWorkItem.State[StateConstants.WydanieMRModuleKontekstView].ToString() != ContextConstants.WydanieMRPrzejecie.ToString())
                    {
                        ViewNastepny(this, EventArgs.Empty);
                        AktualizujWidok();
                    }
                }
        }

        private void ViewSzukaj(object sender, EventArgs e)
        {
            if (View.SzukajEnabled)
            {
                MyWorkItem.Szukaj();
            }
        }

        private void ViewUsun(object sender, EventArgs e)
        {
            if (AktualnaPozycja != null)
            {
                View.ForceDataBinding();

                if (MyWorkItem.Usun(AktualnaPozycja.Id))
                {
                    UsunPozycjeZListy(AktualnaPozycja);
                    View.DataSource = null;
                    ViewNastepny(this, EventArgs.Empty);
                    AktualizujWidok();
                }
            }
        }

        #endregion

        protected override void AttachView()
        {
            View.Szukaj += ViewSzukaj;
            View.Zakoncz += ViewZakoncz;
            View.Poprzedni += ViewPoprzedni;
            View.Nastepny += ViewNastepny;
            View.Usun += ViewUsun;
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    e.Handled = true;
                    ViewPoprzedni(this, EventArgs.Empty);
                    break;
                case Keys.Right:
                    e.Handled = true;
                    ViewNastepny(this, EventArgs.Empty);
                    break;
                case Keys.D1:
                    e.Handled = true;
                    ViewSzukaj(this, EventArgs.Empty);
                    break;
                case Keys.D:
                    e.Handled = true;
                    UruchomRaport();
                    break;
                case Keys.K:
                    if (AktualnaPozycja.PotwierdzonaLokalizacja)
                    {
                        MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueYes;
                        e.Handled = true;
                        MyWorkItem.RootWorkItem.Commands[CommandConstants.KodyKreskowe].Execute();
                    }
                    else
                    {
                        MessageBox.Show(MessageProszeZaczytacLokalizacje, Info, MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                    }
                    break;
                case Keys.Enter:
                    e.Handled = true;
                    ViewZakoncz(this, EventArgs.Empty);
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    CloseView();
                    IsCloseView = true;
                    if (MyWorkItem.RootWorkItem.State[StateConstants.WydanieMRModuleKontekstView] != null
                        && (ContextConstants)MyWorkItem.RootWorkItem.State[StateConstants.WydanieMRModuleKontekstView] == ContextConstants.WydanieMRRealizacja)
                    {
                        MyWorkItem.PorzucDokument(m_DokumentMr);
                        MyWorkItem.OdswiezListeDokumentowMr();
                    }

                    MyWorkItem.RootWorkItem.State[StateConstants.BlokadaWywolaniaFormatkiK] = StateConstants.BoolValueNo;
                    MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
                    MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
                    MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
                    MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
                    MyWorkItem.RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] = null;
                    MyWorkItem.RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueYes;
                    break;
            }
        }

        [EventSubscription(EventBrokerConstants.NAVIGATION_STATE_CHANGED)]
        public override void NavigationStateChanged(object sender, EventArgs e)
        {
            if( IsCloseView)
                return;

            if (MyWorkItem.MainWorkspace.ActiveSmartPart.ToString() != View.ToString()
            && (MyWorkItem.MainWorkspace.ActiveSmartPart.ToString().Equals(SmartParts.KodyKreskoweView))                        
            && (string)MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] == StateConstants.BoolValueYes
            && (string)MyWorkItem.RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] == View.ToString())
            {
                SprawdzZgodnoscKodEan();
                return;
            }

            if (MyWorkItem.MainWorkspace.ActiveSmartPart.ToString() == View.ToString())
            {
                base.NavigationStateChanged(sender, e);

                //if (!NavigationState || (NavigationKeyArgs != null && NavigationKeyArgs.KeyCode == Keys.Escape)) return;

                if ((AktualnaPozycja == null) || (AktualnaPozycja.Ilosc == AktualnaPozycja.IloscOryginalna && AktualnaPozycja.Waga == AktualnaPozycja.WagaOryginalna)) return;

                if (MyWorkItem.AktaulizujPozycje(AktualnaPozycja, AktualnaPozycja.IloscOryginalna, true))
                {
                    AktualnaPozycja.WagaOryginalna = AktualnaPozycja.Waga;
                    AktualnaPozycja.IloscOryginalna = AktualnaPozycja.Ilosc;
                }
                else
                {
                    AktualnaPozycja.Waga = AktualnaPozycja.WagaOryginalna;
                    AktualnaPozycja.Ilosc = AktualnaPozycja.IloscOryginalna;
                    View.Ilosc = AktualnaPozycja.Ilosc;
                    View.ForceDataBinding();
                }

                WczytajMase(new Masa { wartosc = AktualnaPozycja.Waga, jednostka = AktualnaPozycja.Jm });
            }
        }

        #region SprawdzZgodnoscKodEan

        public void SprawdzZgodnoscKodEan()
        {
            if (MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] != null
                && MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych].ToString() == StateConstants.BoolValueNo)
            {
                DialogResult dialogResult;

                if (MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowychSymbolIndeksu] != null)
                {
                    dialogResult = DialogResult.No;
                }
                else
                {
                    dialogResult = MessageBox.Show(MMessageBoxKodNiePrzypisany, MMessageBoxkodPrzypisanyCaption,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                }

                if (dialogResult == DialogResult.No)
                {
                    AktualnaPozycja.Ilosc = MIloscZerowana;
                    if (MyWorkItem.AktaulizujPozycje(AktualnaPozycja, AktualnaPozycja.IloscOryginalna, true))
                    {
                        MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueYes;
                        MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowychSymbolIndeksu] = AktualnaPozycja.SymbolTowaru;
                        View.Ilosc = MIloscZerowana;
                    }
                    else
                    {
                        MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueYes;
                    }

                    ZakonczPozycje2();
                }
            }
        }

        public void SprawdzZgodnoscKodEan2()
        {
            AktualnaPozycja.Ilosc = MIloscZerowana;
            if (MyWorkItem.AktaulizujPozycje(AktualnaPozycja, AktualnaPozycja.IloscOryginalna, true))
            {
                MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueYes;
                MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowychSymbolIndeksu] = AktualnaPozycja.SymbolTowaru;
                View.Ilosc = MIloscZerowana;
            }
            else
            {
                MyWorkItem.RootWorkItem.State[StateConstants.ZnacznikZgodnosciKodowKreskowych] = StateConstants.BoolValueYes;
            }

            ZakonczPozycje2();
        }

        #endregion

        #region Private Methods

        private void AktualizujWidok()
        {
            View.DataSource = AktualnaPozycja;
            View.IloscLp = m_DokumentMr.Naglowek.IloscPozycji.ToString();
            View.Pozostalo = m_DokumentMr.Naglowek.IloscNiezakonczonychPozycji.ToString();
            View.SymbolDokumentu = m_DokumentMr.Naglowek.SymbolDokumentu;
            View.ZakonczEnabled = MozliweZakonczeniePozycji || MozliweZatwierdzenieDokumentu;
            View.UsunEnabled = MozliweUsuwaniePozycji;
            View.ZakonczTekst = m_DokumentMr.Naglowek.IloscNiezakonczonychPozycji == 0 ? ZatwierdzDokument : ZakonczPozycje;

            if (AktualnaPozycja != null)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = AktualnaPozycja.SymbolTowaru;
                MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueYes;
                MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = AktualnaPozycja.SymbolTowaru;
                MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;

                View.IloscReadOnly = NavigationState | !AktualnaPozycja.PotwierdzonaLokalizacja;
                WczytajMase(new Masa { wartosc = AktualnaPozycja.Waga, jednostka = AktualnaPozycja.Jm });

                AktaulizujStatus(AktualnaPozycja.Id);
            }
            else
            {
                View.IloscReadOnly = true;
                MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
                MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
                MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
                MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;

                WczytajMase(new Masa { wartosc = 0 });
            }
            if (m_DokumentMr.Naglowek.Zatwierdzony.Equals("T"))
            {
                View.ZakonczEnabled = false;
                View.UsunEnabled = false;
            }
            MyWorkItem.Activate();
        }

        private void AktaulizujStatus(long id)
        {
            var pozycjaMRstatus = MyWorkItem.Services.Get<IWydanieMRService>(true).PobierzPozycjeMR(id);

            if (!MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(Common.DataModel.StatusOperacji.SUCCESS))
            {
                MessageBox.Show(MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), "Aktulizacja MR");
            }

            if (pozycjaMRstatus != null)
            {
                AktualnaPozycja.Zakonczona = pozycjaMRstatus.Zakonczona;
                View.Zakonczona = pozycjaMRstatus.Zakonczona;
            }
        }

        private void ZmienStrone(int kierunek)
        {
            m_NumerStrony += kierunek;
            m_ZmianaStrony = kierunek;
            ZaladujDaneDoWidoku(MyWorkItem.PobierzDokumentyMr(m_NumerStrony));
        }

        private void Zatwierdz()
        {
            if ((m_DokumentMr != null) || (AktualnaPozycja != null))
            {
                View.ForceDataBinding();

                long idDokumentu;
                if (m_DokumentMr != null && m_DokumentMr.Naglowek != null && m_DokumentMr.Naglowek.Id != 0)
                {
                    idDokumentu = m_DokumentMr.Naglowek.Id;
                }
                else
                {
                    idDokumentu = AktualnaPozycja.IdDokumentu;
                }


                if (MyWorkItem.Zatwierdz(idDokumentu))
                {
                    MyWorkItem.MainWorkspace.Close(View);
                    MyWorkItem.OdswiezListeDokumentowMr();

                    MyWorkItem.RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueNo;

                        if (ZatwierdzonoDokumentMR != null)
                        {
                            ZatwierdzonoDokumentMR(this, EventArgs.Empty);
                        }
                }
                else
                {
                    AktualizujWidok();
                }
            }
        }

        private void UruchomRaport()
        {
            if ((AktualnaPozycja != null) && (MyWorkItem.UruchomRaport(NazwaFormularza,
                                                                         AktualnaPozycja.IdDokumentu, AktualnaPozycja.Id)))
            {
                MessageBox.Show(PomyslneWywolanieRaportu);
                MyWorkItem.Activate();
            }
        }

        private void UsunPozycjeZListy(Pozycja aktualnaPozycja)
        {
            m_ListaPozycji.Remove(aktualnaPozycja);
            m_IloscUsunietychPozycji++;
        }

        #endregion
    }
}