using Common.Base;
using System;
using Common.WagiProxy;
using WydanieMRModule.DataModel;

namespace WydanieMRModule.EdycjaMR
{
	public interface IEdycjaMRView: IViewBase
	{
		event EventHandler Szukaj;
		event EventHandler Zakoncz;
		event EventHandler Nastepny;
		event EventHandler Poprzedni;
        event EventHandler Usun;

        Pozycja DataSource { set; get; }
		string SymbolDokumentu { set; }
		string IloscLp { set; }
		string Pozostalo { set; }
		bool ZakonczEnabled { set; }
        bool UsunEnabled { set; }
        bool SzukajEnabled { get;  set; }
        string ZakonczTekst { set; }
        string Zakonczona { set; get; }
        bool IloscReadOnly { set; }
        float? Ilosc { set; get; }
        Masa Waga { set; }
	}
}
