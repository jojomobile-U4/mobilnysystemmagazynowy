#region

using Common.SearchForm;

#endregion

namespace WydanieMRModule.EdycjaMR.Search
{
    public interface IEdycjaMRSearchView : ISearchForm
    {
        string Kryteria { get; set; }
    }
}