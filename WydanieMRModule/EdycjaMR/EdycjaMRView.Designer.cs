
using Common.Components;
namespace WydanieMRModule.EdycjaMR
{
	partial class EdycjaMRView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.lrcNavigation = new Common.Components.LeftRightControl();
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbDokument = new System.Windows.Forms.TextBox();
            this.btnZakoncz = new System.Windows.Forms.Button();
            this.lbZakonczona = new System.Windows.Forms.Label();
            this.pozycjaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbZakonczona = new System.Windows.Forms.TextBox();
            this.lbIlosc = new System.Windows.Forms.Label();
            this.tbIlosc = new System.Windows.Forms.TextBox();
            this.btnSzukaj = new System.Windows.Forms.Button();
            this.lbNazwaTowaru = new System.Windows.Forms.Label();
            this.tbNazwaTowaru = new System.Windows.Forms.TextBox();
            this.lbSymbolTowaru = new System.Windows.Forms.Label();
            this.tbSymbolTowaru = new System.Windows.Forms.TextBox();
            this.lbLokalizacja = new System.Windows.Forms.Label();
            this.tbLokalizacja = new System.Windows.Forms.TextBox();
            this.lbLP = new System.Windows.Forms.Label();
            this.tbLP = new System.Windows.Forms.TextBox();
            this.lbSlash = new System.Windows.Forms.Label();
            this.tbIloscLP = new System.Windows.Forms.TextBox();
            this.lbPozycje = new System.Windows.Forms.Label();
            this.lbDokument = new System.Windows.Forms.Label();
            this.lbJm = new System.Windows.Forms.Label();
            this.tbJm = new System.Windows.Forms.TextBox();
            this.lbPozostalo = new System.Windows.Forms.Label();
            this.tbPozostalo = new System.Windows.Forms.TextBox();
            this.btnUsun = new System.Windows.Forms.Button();
            this.lKg = new System.Windows.Forms.Label();
            this.lblWaga = new System.Windows.Forms.Label();
            this.tbWaga = new System.Windows.Forms.TextBox();
            this.pnlNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pozycjaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnUsun);
            this.pnlNavigation.Controls.Add(this.btnSzukaj);
            this.pnlNavigation.Controls.Add(this.btnZakoncz);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 256);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 44);
            // 
            // lrcNavigation
            // 
            this.lrcNavigation.BackColor = System.Drawing.SystemColors.Desktop;
            this.lrcNavigation.Location = new System.Drawing.Point(208, 0);
            this.lrcNavigation.Name = "lrcNavigation";
            this.lrcNavigation.NextEnabled = true;
            this.lrcNavigation.PreviousEnabled = true;
            this.lrcNavigation.Size = new System.Drawing.Size(32, 16);
            this.lrcNavigation.TabIndex = 7;
            this.lrcNavigation.TabStop = false;
            this.lrcNavigation.Next += new System.EventHandler(this.OnNastepny);
            this.lrcNavigation.Previous += new System.EventHandler(this.OnPoprzedni);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 21;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "WYDANIE - EDYCJA MR";
            // 
            // tbDokument
            // 
            this.tbDokument.Location = new System.Drawing.Point(85, 20);
            this.tbDokument.Name = "tbDokument";
            this.tbDokument.ReadOnly = true;
            this.tbDokument.Size = new System.Drawing.Size(152, 21);
            this.tbDokument.TabIndex = 9;
            this.tbDokument.TabStop = false;
            // 
            // btnZakoncz
            // 
            this.btnZakoncz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnZakoncz.Location = new System.Drawing.Point(157, 21);
            this.btnZakoncz.Name = "btnZakoncz";
            this.btnZakoncz.Size = new System.Drawing.Size(80, 20);
            this.btnZakoncz.TabIndex = 5;
            this.btnZakoncz.TabStop = false;
            this.btnZakoncz.Text = "&Ret Zako�cz";
            this.btnZakoncz.Click += new System.EventHandler(this.OnZakoncz);
            // 
            // lbZakonczona
            // 
            this.lbZakonczona.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbZakonczona.Location = new System.Drawing.Point(3, 225);
            this.lbZakonczona.Name = "lbZakonczona";
            this.lbZakonczona.Size = new System.Drawing.Size(82, 20);
            this.lbZakonczona.Text = "Zako�czona:";
            // 
            // pozycjaBindingSource
            // 
            this.pozycjaBindingSource.DataSource = typeof(WydanieMRModule.DataModel.Pozycja);
            // 
            // tbZakonczona
            // 
            this.tbZakonczona.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "Zakonczona", true));
            this.tbZakonczona.Location = new System.Drawing.Point(85, 223);
            this.tbZakonczona.Name = "tbZakonczona";
            this.tbZakonczona.ReadOnly = true;
            this.tbZakonczona.Size = new System.Drawing.Size(68, 21);
            this.tbZakonczona.TabIndex = 2;
            this.tbZakonczona.TabStop = false;
            // 
            // lbIlosc
            // 
            this.lbIlosc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbIlosc.Location = new System.Drawing.Point(3, 156);
            this.lbIlosc.Name = "lbIlosc";
            this.lbIlosc.Size = new System.Drawing.Size(82, 20);
            this.lbIlosc.Text = "Ilo��:";
            // 
            // tbIlosc
            // 
            this.tbIlosc.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "Ilosc", true));
            this.tbIlosc.Location = new System.Drawing.Point(85, 154);
            this.tbIlosc.Name = "tbIlosc";
            this.tbIlosc.Size = new System.Drawing.Size(68, 21);
            this.tbIlosc.TabIndex = 0;
            // 
            // btnSzukaj
            // 
            this.btnSzukaj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnSzukaj.Location = new System.Drawing.Point(3, 21);
            this.btnSzukaj.Name = "btnSzukaj";
            this.btnSzukaj.Size = new System.Drawing.Size(80, 20);
            this.btnSzukaj.TabIndex = 4;
            this.btnSzukaj.TabStop = false;
            this.btnSzukaj.Text = "&1 Szukaj";
            this.btnSzukaj.Click += new System.EventHandler(this.OnSzukaj);
            // 
            // lbNazwaTowaru
            // 
            this.lbNazwaTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbNazwaTowaru.Location = new System.Drawing.Point(3, 133);
            this.lbNazwaTowaru.Name = "lbNazwaTowaru";
            this.lbNazwaTowaru.Size = new System.Drawing.Size(78, 20);
            this.lbNazwaTowaru.Text = "Nazwa towaru:";
            // 
            // tbNazwaTowaru
            // 
            this.tbNazwaTowaru.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "NazwaTowaru", true));
            this.tbNazwaTowaru.Location = new System.Drawing.Point(85, 131);
            this.tbNazwaTowaru.Name = "tbNazwaTowaru";
            this.tbNazwaTowaru.ReadOnly = true;
            this.tbNazwaTowaru.Size = new System.Drawing.Size(152, 21);
            this.tbNazwaTowaru.TabIndex = 14;
            this.tbNazwaTowaru.TabStop = false;
            // 
            // lbSymbolTowaru
            // 
            this.lbSymbolTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbolTowaru.Location = new System.Drawing.Point(3, 109);
            this.lbSymbolTowaru.Name = "lbSymbolTowaru";
            this.lbSymbolTowaru.Size = new System.Drawing.Size(82, 20);
            this.lbSymbolTowaru.Text = "Indeks:";
            // 
            // tbSymbolTowaru
            // 
            this.tbSymbolTowaru.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "SymbolTowaru", true));
            this.tbSymbolTowaru.Location = new System.Drawing.Point(85, 108);
            this.tbSymbolTowaru.Name = "tbSymbolTowaru";
            this.tbSymbolTowaru.ReadOnly = true;
            this.tbSymbolTowaru.Size = new System.Drawing.Size(152, 21);
            this.tbSymbolTowaru.TabIndex = 13;
            this.tbSymbolTowaru.TabStop = false;
            // 
            // lbLokalizacja
            // 
            this.lbLokalizacja.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbLokalizacja.Location = new System.Drawing.Point(3, 86);
            this.lbLokalizacja.Name = "lbLokalizacja";
            this.lbLokalizacja.Size = new System.Drawing.Size(82, 20);
            this.lbLokalizacja.Text = "Lokalizacja:";
            // 
            // tbLokalizacja
            // 
            this.tbLokalizacja.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "Lokalizacja", true));
            this.tbLokalizacja.Location = new System.Drawing.Point(85, 85);
            this.tbLokalizacja.Name = "tbLokalizacja";
            this.tbLokalizacja.ReadOnly = true;
            this.tbLokalizacja.Size = new System.Drawing.Size(152, 21);
            this.tbLokalizacja.TabIndex = 12;
            this.tbLokalizacja.TabStop = false;
            // 
            // lbLP
            // 
            this.lbLP.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbLP.Location = new System.Drawing.Point(3, 63);
            this.lbLP.Name = "lbLP";
            this.lbLP.Size = new System.Drawing.Size(82, 20);
            this.lbLP.Text = "LP/Ilo�� LP:";
            // 
            // tbLP
            // 
            this.tbLP.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "Lp", true));
            this.tbLP.Location = new System.Drawing.Point(85, 62);
            this.tbLP.Name = "tbLP";
            this.tbLP.ReadOnly = true;
            this.tbLP.Size = new System.Drawing.Size(68, 21);
            this.tbLP.TabIndex = 10;
            this.tbLP.TabStop = false;
            // 
            // lbSlash
            // 
            this.lbSlash.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSlash.Location = new System.Drawing.Point(157, 64);
            this.lbSlash.Name = "lbSlash";
            this.lbSlash.Size = new System.Drawing.Size(10, 18);
            this.lbSlash.Text = "/";
            // 
            // tbIloscLP
            // 
            this.tbIloscLP.Location = new System.Drawing.Point(169, 62);
            this.tbIloscLP.Name = "tbIloscLP";
            this.tbIloscLP.ReadOnly = true;
            this.tbIloscLP.Size = new System.Drawing.Size(68, 21);
            this.tbIloscLP.TabIndex = 11;
            this.tbIloscLP.TabStop = false;
            // 
            // lbPozycje
            // 
            this.lbPozycje.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbPozycje.Location = new System.Drawing.Point(0, 43);
            this.lbPozycje.Name = "lbPozycje";
            this.lbPozycje.Size = new System.Drawing.Size(240, 16);
            this.lbPozycje.Text = "POZYCJE";
            this.lbPozycje.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lbDokument
            // 
            this.lbDokument.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbDokument.Location = new System.Drawing.Point(3, 20);
            this.lbDokument.Name = "lbDokument";
            this.lbDokument.Size = new System.Drawing.Size(91, 20);
            this.lbDokument.Text = "Dokument:";
            // 
            // lbJm
            // 
            this.lbJm.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbJm.Location = new System.Drawing.Point(3, 202);
            this.lbJm.Name = "lbJm";
            this.lbJm.Size = new System.Drawing.Size(82, 20);
            this.lbJm.Text = "J.m.:";
            // 
            // tbJm
            // 
            this.tbJm.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "Jm", true));
            this.tbJm.Location = new System.Drawing.Point(85, 200);
            this.tbJm.Name = "tbJm";
            this.tbJm.ReadOnly = true;
            this.tbJm.Size = new System.Drawing.Size(68, 21);
            this.tbJm.TabIndex = 1;
            this.tbJm.TabStop = false;
            // 
            // lbPozostalo
            // 
            this.lbPozostalo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbPozostalo.Location = new System.Drawing.Point(3, 248);
            this.lbPozostalo.Name = "lbPozostalo";
            this.lbPozostalo.Size = new System.Drawing.Size(82, 20);
            this.lbPozostalo.Text = "Pozosta�o:";
            // 
            // tbPozostalo
            // 
            this.tbPozostalo.Location = new System.Drawing.Point(85, 246);
            this.tbPozostalo.Name = "tbPozostalo";
            this.tbPozostalo.ReadOnly = true;
            this.tbPozostalo.Size = new System.Drawing.Size(68, 21);
            this.tbPozostalo.TabIndex = 3;
            this.tbPozostalo.TabStop = false;
            // 
            // btnUsun
            // 
            this.btnUsun.Enabled = false;
            this.btnUsun.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnUsun.Location = new System.Drawing.Point(85, 21);
            this.btnUsun.Name = "btnUsun";
            this.btnUsun.Size = new System.Drawing.Size(70, 20);
            this.btnUsun.TabIndex = 6;
            this.btnUsun.TabStop = false;
            this.btnUsun.Text = "&2 Usu�";
            this.btnUsun.Click += new System.EventHandler(this.OnUsun);
            // 
            // lKg
            // 
            this.lKg.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lKg.Location = new System.Drawing.Point(156, 181);
            this.lKg.Name = "lKg";
            this.lKg.Size = new System.Drawing.Size(20, 16);
            this.lKg.Text = "Kg";
            // 
            // lblWaga
            // 
            this.lblWaga.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lblWaga.Location = new System.Drawing.Point(3, 179);
            this.lblWaga.Name = "lblWaga";
            this.lblWaga.Size = new System.Drawing.Size(80, 21);
            this.lblWaga.Text = "Waga:";
            // 
            // tbWaga
            // 
            this.tbWaga.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.pozycjaBindingSource, "Waga", true));
            this.tbWaga.Location = new System.Drawing.Point(85, 177);
            this.tbWaga.Name = "tbWaga";
            this.tbWaga.ReadOnly = true;
            this.tbWaga.Size = new System.Drawing.Size(68, 21);
            this.tbWaga.TabIndex = 51;
            this.tbWaga.TabStop = false;
            this.tbWaga.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbWaga_KeyPress);
            // 
            // EdycjaMRView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.tbPozostalo);
            this.Controls.Add(this.tbZakonczona);
            this.Controls.Add(this.tbJm);
            this.Controls.Add(this.tbWaga);
            this.Controls.Add(this.tbIlosc);
            this.Controls.Add(this.lKg);
            this.Controls.Add(this.lblWaga);
            this.Controls.Add(this.lbPozostalo);
            this.Controls.Add(this.lbJm);
            this.Controls.Add(this.tbDokument);
            this.Controls.Add(this.lbZakonczona);
            this.Controls.Add(this.lbIlosc);
            this.Controls.Add(this.lbNazwaTowaru);
            this.Controls.Add(this.tbNazwaTowaru);
            this.Controls.Add(this.lbSymbolTowaru);
            this.Controls.Add(this.tbSymbolTowaru);
            this.Controls.Add(this.lbLokalizacja);
            this.Controls.Add(this.tbLokalizacja);
            this.Controls.Add(this.lbLP);
            this.Controls.Add(this.tbLP);
            this.Controls.Add(this.lbSlash);
            this.Controls.Add(this.tbIloscLP);
            this.Controls.Add(this.lbPozycje);
            this.Controls.Add(this.lbDokument);
            this.Controls.Add(this.lrcNavigation);
            this.Controls.Add(this.lbTytul);
            this.Name = "EdycjaMRView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lrcNavigation, 0);
            this.Controls.SetChildIndex(this.lbDokument, 0);
            this.Controls.SetChildIndex(this.lbPozycje, 0);
            this.Controls.SetChildIndex(this.tbIloscLP, 0);
            this.Controls.SetChildIndex(this.lbSlash, 0);
            this.Controls.SetChildIndex(this.tbLP, 0);
            this.Controls.SetChildIndex(this.lbLP, 0);
            this.Controls.SetChildIndex(this.tbLokalizacja, 0);
            this.Controls.SetChildIndex(this.lbLokalizacja, 0);
            this.Controls.SetChildIndex(this.tbSymbolTowaru, 0);
            this.Controls.SetChildIndex(this.lbSymbolTowaru, 0);
            this.Controls.SetChildIndex(this.tbNazwaTowaru, 0);
            this.Controls.SetChildIndex(this.lbNazwaTowaru, 0);
            this.Controls.SetChildIndex(this.lbIlosc, 0);
            this.Controls.SetChildIndex(this.lbZakonczona, 0);
            this.Controls.SetChildIndex(this.tbDokument, 0);
            this.Controls.SetChildIndex(this.lbJm, 0);
            this.Controls.SetChildIndex(this.lbPozostalo, 0);
            this.Controls.SetChildIndex(this.lblWaga, 0);
            this.Controls.SetChildIndex(this.lKg, 0);
            this.Controls.SetChildIndex(this.tbIlosc, 0);
            this.Controls.SetChildIndex(this.tbWaga, 0);
            this.Controls.SetChildIndex(this.tbJm, 0);
            this.Controls.SetChildIndex(this.tbZakonczona, 0);
            this.Controls.SetChildIndex(this.tbPozostalo, 0);
            this.pnlNavigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pozycjaBindingSource)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private LeftRightControl lrcNavigation;
		private MSMLabel lbTytul;
		private System.Windows.Forms.TextBox tbDokument;
		private System.Windows.Forms.Button btnZakoncz;
		private System.Windows.Forms.Label lbZakonczona;
		private System.Windows.Forms.TextBox tbZakonczona;
		private System.Windows.Forms.Label lbIlosc;
		private System.Windows.Forms.TextBox tbIlosc;
		private System.Windows.Forms.Button btnSzukaj;
		private System.Windows.Forms.Label lbNazwaTowaru;
		private System.Windows.Forms.TextBox tbNazwaTowaru;
		private System.Windows.Forms.Label lbSymbolTowaru;
		private System.Windows.Forms.TextBox tbSymbolTowaru;
		private System.Windows.Forms.Label lbLokalizacja;
		private System.Windows.Forms.TextBox tbLokalizacja;
		private System.Windows.Forms.Label lbLP;
		private System.Windows.Forms.TextBox tbLP;
		private System.Windows.Forms.Label lbSlash;
		private System.Windows.Forms.TextBox tbIloscLP;
		private System.Windows.Forms.Label lbPozycje;
		private System.Windows.Forms.Label lbDokument;
		private System.Windows.Forms.Label lbJm;
		private System.Windows.Forms.TextBox tbJm;
		private System.Windows.Forms.Label lbPozostalo;
		private System.Windows.Forms.TextBox tbPozostalo;
		private System.Windows.Forms.BindingSource pozycjaBindingSource;
        private System.Windows.Forms.Button btnUsun;
        private System.Windows.Forms.Label lKg;
        private System.Windows.Forms.Label lblWaga;
        private System.Windows.Forms.TextBox tbWaga;
	}
}
