#region

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.DataModel;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using WydanieMRModule.DataModel;
using WydanieMRModule.EdycjaMR.Search;
using WydanieMRModule.ListaMR;
using WydanieMRModule.Services;

#endregion

namespace WydanieMRModule.EdycjaMR
{
    public class EdycjaMRWorkItem : WorkItemBase
    {
        #region Events

        public event EventHandler<DataEventArgs<long>> ZatwierdzonoDokumentMr;

        #endregion

        #region Constants

        private const string Info = "Informacja";
        private const string Rezerwowany = "R";
        private const string ZatwierdzonoMR = "Wszystkie pozycje dokumentu zosta�y zako�czone.\nDokument MR zosta� zatwierdzony.";
        private const string NazwaModulu = "Wydanie MR";
        private const string MessageNiepoprawnyFormatIdLini = "Niepoprawny format identyfikatora linii.";
        private const string MessageProszeZaczytacLokalizacje = "Prosz� zaczyta� kod lokalizacji indeksu";

        
        #endregion

        #region Private Fields

        private Dokument m_EdytowanyDokument;
        private bool m_PokazujTylkoPozNiezakonczone;

        #endregion

        [EventSubscription(EventBrokerConstants.WYSZUKAJ_POZYCJE_DOKUMENTU_MR)]
        public void OnSzukajPozycjeMr(object sender, EventArgs e)
        {
            var edycjaMrViewPresenter = Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EDYCJA_MR_PRESENTER);
            //pobieramy zawsze pierwsza strone

            edycjaMrViewPresenter.ResetujStronicowanie();
            edycjaMrViewPresenter.ZaladujDaneDoWidoku(PobierzDokumentyMr(null));
            MainWorkspace.Show(edycjaMrViewPresenter.View);
        }

        public List<Dokument> PobierzDokumentyMr(int? numerStrony)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                string zakonczona = null;
                if (m_PokazujTylkoPozNiezakonczone)
                {
                    zakonczona = "N";
                }

                var wynik = Services.Get<IWydanieMRService>(true).PobierzListeDokumentowMR(
                    (long?)State[StateConstants.DOCUMENT_MR_ID],
                    (long?)State[StateConstants.POSITION_MR_ID],
                    State[StateConstants.CRITERIAS] as string,
                    "R",
                    "PP",
                    zakonczona,
                    numerStrony);
                if (wynik.StatusOperacji.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(wynik.StatusOperacji.Tekst, BLAD, wynik.StatusOperacji.StosWywolan);
                }
                //jezeli zostala zwrocona pusta lista, to zwracamy pusty dokument, ktory byl wybrany do edycji
                return wynik.Lista;
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void Szukaj()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var searchPresenter = Items.Get<EdycjaMRSearchViewPresenter>(ItemsConstants.EDYCJA_MR_SEARCH_PRESENTER);
                //czyszczenie zapamietanego stanu
                searchPresenter.View.Kryteria = string.Empty;

                Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                MainWorkspace.Show(searchPresenter.View);
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public bool Zakoncz(Pozycja pozycja)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var wynikOperacji = Services.Get<IWydanieMRService>(true).ZakonczPozycje(pozycja.Id);
                if (wynikOperacji.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(wynikOperacji.Tekst, BLAD, wynikOperacji.StosWywolan);
                    return false;
                }
                else
                {
                    RootWorkItem.State[StateConstants.PrzejecieSkanRodzaj] = ModuleSkanRodzajConstants.IdLinii;
                    return true;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public bool Usun(long idPozycji)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var wynikOperacji = Services.Get<IWydanieMRService>(true).UsunPozycje(idPozycji);
                if (wynikOperacji.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(wynikOperacji.Tekst, BLAD, wynikOperacji.StosWywolan);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public bool Zatwierdz(Dokument dokumentMr)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                if (dokumentMr == null ||
                    dokumentMr.Naglowek == null)
                {
                    return false;
                }

                var wynikOperacji = Services.Get<IWydanieMRService>(true).ZatwierdzDokument(dokumentMr.Naglowek.Id);
                if (wynikOperacji.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(wynikOperacji.Tekst, BLAD, wynikOperacji.StosWywolan);
                    return false;
                }
                else
                {
                    ShowMessageBox(ZatwierdzonoMR, Info);

                    if (RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiEducjaMr] != null && (string)RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiEducjaMr] == ContextViewConstants.RealizacjaSWA.ToString())
                    {
                        FunctionsHelper.SetCursorDefault();
                        OnZatwierdzonoDokumentMr(dokumentMr.Naglowek.Id);
                    }
                    return true;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public bool Zatwierdz(long id)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var wynikOperacji = Services.Get<IWydanieMRService>(true).ZatwierdzDokument(id);
                if (wynikOperacji.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(wynikOperacji.Tekst, BLAD, wynikOperacji.StosWywolan);
                    return false;
                }
                else
                {
                    ShowMessageBox(ZatwierdzonoMR, Info);

                    if (RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiEducjaMr] != null && (string)RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiEducjaMr] == ContextViewConstants.RealizacjaSWA.ToString())
                    {
                        FunctionsHelper.SetCursorDefault();
                        OnZatwierdzonoDokumentMr(id);
                    }
                    return true;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void OdswiezListeDokumentowMr()
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                if (RootWorkItem.WorkItems.Get<ListaMRWorkItem>(WorkItemsConstants.LISTA_MR_WORKITEM).AktywnaListaMR)
                {
                    RootWorkItem.WorkItems.Get<ListaMRWorkItem>(WorkItemsConstants.LISTA_MR_WORKITEM).OdswiezListeDokumentow();
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void PorzucDokument(Dokument dokument)
        {
            try
            {
                if (dokument == null ||
                    !dokument.Naglowek.Status.Equals(Rezerwowany))
                {
                    return;
                }

                FunctionsHelper.SetCursorWait();
                var status = Services.Get<IWydanieMRService>(true).PorzucDokument(dokument.Naglowek.Id);
                if (status.Status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(status.Tekst, BLAD, status.StosWywolan);
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public bool AktaulizujPozycje(Pozycja pozycja)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                var status = Services.Get<IWydanieMRService>(true).AktaulizujPozycje(pozycja);

                if (status.Status.Equals(StatusOperacji.ERROR))
                {
                    FunctionsHelper.SetCursorDefault();
                    ShowMessageBox(status.Tekst, BLAD, status.StosWywolan);
                    return false;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }

            return true;
        }

        [System.Obsolete("Use : AktaulizujPozycje(Pozycja pozycja)")]
        public bool AktaulizujPozycje(Pozycja pozycja, float? iloscOrg, bool komunikat)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var status = Services.Get<IWydanieMRService>(true).AktaulizujPozycje(pozycja, Common.FunctionsHelper.ConvertToFloat(iloscOrg.ToString()), komunikat);

                if (status.Status.Equals(StatusOperacji.ERROR))
                {
                    FunctionsHelper.SetCursorDefault();
                    ShowMessageBox(status.Tekst, BLAD, status.StosWywolan);
                    return false;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }

            return true;
        }

        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ)]
        public void BarCodeRead(object sender, EventArgs e)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var presenter = Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EDYCJA_MR_PRESENTER);

                if (RootWorkItem.State[StateConstants.BAR_CODE] != null && MainWorkspace.ActiveSmartPart == presenter.View)
                {
                    if ((string)RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] == StateConstants.BoolValueYes)
                    {
                        return;
                    }

                    Activate();

                    var odczyt = RootWorkItem.State[StateConstants.BAR_CODE] as string;
                    if (odczyt == null) return;

                    if (odczyt.IndexOf("WAG", 0) == 0)
                    {
                        if (!presenter.AktualnaPozycja.PotwierdzonaLokalizacja)
                        {
                            MessageBox.Show(MessageProszeZaczytacLokalizacje, Info, MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                            return;
                        }

                        FunctionsHelper.SetCursorWait();

                        var masa = Services.Get<IWagiService>(true).pobierzOdczytWagi(odczyt);

                        if (masa.statusOperacji.status.Equals(StatusOperacji.ERROR))
                        {
                            ShowMessageBox(masa.statusOperacji.tekst, BLAD, masa.statusOperacji.stosWywolan);
                            return;
                        }
                        else
                        {
                            var jm = Services.Get<IWydanieMRService>(true).PodstawowaJednostkaMiaryDlaIndeksu(presenter.AktualnaPozycja.SymbolTowaru);

                            if (!Equals(jm.wartosc, WagiService.JednostkaMiaryKg))
                            {
                                ShowMessageBox("Zwa�enie ilo�ci wydanej partii towaru jest mo�liwe tylko dla indeks�w, kt�rych jednostk� bazow� jest kilogram [KG]", "Wa�enie ilo�ci wydawanego towaru");
                                return;
                            }

                            Services.Get<IWagiService>(true).przeliczNaKg(ref masa);

                            presenter.View.Ilosc = masa.wartosc;
                            presenter.AktualnaPozycja.Ilosc = masa.wartosc;

                            if (AktaulizujPozycje(presenter.AktualnaPozycja, presenter.AktualnaPozycja.IloscOryginalna, false))
                            {
                                presenter.AktualnaPozycja.IloscOryginalna = masa.wartosc;
                            }
                        }
                    }
                    else if ((RootWorkItem.State[StateConstants.PrzejecieSkanIdLinii].ToString() == ModuleBooleanConstants.Yes.ToString()
                        && RootWorkItem.State[StateConstants.PrzejecieSkanRodzaj].ToString() == ModuleSkanRodzajConstants.IdLinii.ToString())
                        )
                    {
                        try
                        {
                            var returnValue = long.Parse(odczyt.Replace(FunctionsHelper.MComma, FunctionsHelper.MFullStop), NumberStyles.Number, CultureInfo.InvariantCulture);

                            presenter.AktualnaPozycja = PobierzPozycjeDokumentuMr(returnValue);

                            if ((presenter.AktualnaPozycja != null) && (presenter.AktualnaPozycja.Id > 0))
                            {
                                RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = presenter.AktualnaPozycja.SymbolTowaru;
                                RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueYes;

                                presenter.View.DataSource = presenter.AktualnaPozycja;
                                presenter.View.SymbolDokumentu = presenter.AktualnaPozycja.SymbolDokumentu;
                                presenter.View.Pozostalo = "1";
                                RootWorkItem.State[StateConstants.PrzejecieSkanRodzaj] = ModuleSkanRodzajConstants.Lokalizacja;
                            } else
                            {
                                RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
                                RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
                            }
                        }
                        catch (Exception)
                        {

                            MessageBox.Show(MessageNiepoprawnyFormatIdLini, Info);
                        }
                    }
                    else
                    {
                        var lokalizacja = Localization.RemovePrefix(odczyt);
                        presenter.PotwierdzLokalizacjePozycji(lokalizacja);
                    }
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        //Refaktoring - utworzyc Show(parentWorkspace, dokument, bool edycjaPojedynczejPozycji)
        public void Show(IWorkspace parentWorkspace, Naglowek naglowekMr, bool tylkoPozycjeNiezakonczone)
        {
            m_PokazujTylkoPozNiezakonczone = tylkoPozycjeNiezakonczone;
            State[StateConstants.DOCUMENT_MR_ID] = naglowekMr.Id;
            State[StateConstants.POSITION_MR_ID] = null;
            RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
            RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;

            //zapamietanie, jaki dokument zostal wybrany do edycji, na wypadek gdyby nie mial zadnych pozycji 
            //- wtedy web service zwroci pusta liste lub po wyszukaniu zwrocona zostala pusta lista
            m_EdytowanyDokument = UtworzDokumentBezPozycji(naglowekMr);

            PokazEdycjePozycji(parentWorkspace, PobierzDokumentyMr(null));

            SetStateValues();

            var edycjaMrViewPresenter = Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EDYCJA_MR_PRESENTER);
            edycjaMrViewPresenter.IsCloseView = false;
            edycjaMrViewPresenter.View.SzukajEnabled = true;
            RootWorkItem.State[StateConstants.PrzejecieSkanIdLinii] = ModuleBooleanConstants.No;
            RootWorkItem.State[StateConstants.PrzejecieSkanRodzaj] = ModuleSkanRodzajConstants.Lokalizacja;
        }

        /// <summary>
        /// Wyswietla zaczytany wczesniej dokument wraz z jedna pozycja.
        /// </summary>
        /// <param name="parentWorkspace"></param>
        /// <param name="dokument">Dokument z zaczytana pojedyncza pozycja.</param>
        public void Show(IWorkspace parentWorkspace, Dokument dokument)
        {
            m_PokazujTylkoPozNiezakonczone = true;
            if (dokument == null ||
                dokument.Pozycje.Count == 0)
            {
                return;
            }
            State[StateConstants.DOCUMENT_MR_ID] = null;
            State[StateConstants.POSITION_MR_ID] = dokument.Pozycje[0].Id;

            RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
            RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
            RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
            RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
            RootWorkItem.State[StateConstants.BlokadaWywolaniaFormatkiK] = StateConstants.BoolValueYes;

            //zapamietanie, jaki dokument zostal wybrany do edycji, na wypadek gdyby nie mial zadnych pozycji
            //- wtedy web service zwroci pusta liste lub po wyszukaniu zwrocona zostala pusta lista
            m_EdytowanyDokument = UtworzDokumentBezPozycji(dokument.Naglowek);
            var listaDokumentow = new List<Dokument> { dokument };

            Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EDYCJA_MR_PRESENTER).IsCloseView = false;

            PokazEdycjePozycji(parentWorkspace, listaDokumentow);

            SetStateValues();
        }

        public void Show(IWorkspace parentWorkspace)
        {
            FunctionsHelper.SetCursorWait();

            m_PokazujTylkoPozNiezakonczone = true;

            RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
            RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
            RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
            RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
            RootWorkItem.State[StateConstants.BlokadaWywolaniaFormatkiK] = StateConstants.BoolValueYes;

            var edycjaMrViewPresenter = Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EDYCJA_MR_PRESENTER);
            edycjaMrViewPresenter.ResetujStronicowanie();
            edycjaMrViewPresenter.View.SzukajEnabled = true;
            edycjaMrViewPresenter.IsCloseView = false;
            parentWorkspace.Show(edycjaMrViewPresenter.View);
            Activate();

            SetStateValues();

            FunctionsHelper.SetCursorDefault();
        }

        public void Show(IWorkspace parentWorkspace, bool clearView)
        {
            FunctionsHelper.SetCursorWait();

            var edycjaPrezenter = Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EDYCJA_MR_PRESENTER);

            if (clearView)
            {
                edycjaPrezenter.View.DataSource = PobierzPozycjeDokumentuMr(0);
                edycjaPrezenter.View.SymbolDokumentu = null;
                edycjaPrezenter.View.Pozostalo = null;
                edycjaPrezenter.View.IloscLp = null;
                edycjaPrezenter.View.SzukajEnabled = false;
                edycjaPrezenter.View.SetNavigationState(true);
                RootWorkItem.State[StateConstants.PrzejecieSkanIdLinii] = ModuleBooleanConstants.Yes;
                RootWorkItem.State[StateConstants.PrzejecieSkanRodzaj] = ModuleSkanRodzajConstants.IdLinii;
            }
            else
            {
                edycjaPrezenter.View.SzukajEnabled = true;
                RootWorkItem.State[StateConstants.PrzejecieSkanIdLinii] = ModuleBooleanConstants.No;
                RootWorkItem.State[StateConstants.PrzejecieSkanRodzaj] = ModuleSkanRodzajConstants.Lokalizacja;
            }


            m_PokazujTylkoPozNiezakonczone = true;

            RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = null;
            RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
            RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
            RootWorkItem.State[StateConstants.BlokadaWywolaniaFormatkiK] = StateConstants.BoolValueYes;

            var edycjaMrViewPresenter = Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EDYCJA_MR_PRESENTER);
            edycjaMrViewPresenter.ResetujStronicowanie();
            edycjaMrViewPresenter.IsCloseView = false;
            parentWorkspace.Show(edycjaMrViewPresenter.View);
            Activate();

            SetStateValues();

            FunctionsHelper.SetCursorDefault();
        }


        public void SetStateValues()
        {
            var edycjaMrViewPresenter = Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EDYCJA_MR_PRESENTER);

            if (edycjaMrViewPresenter == null || edycjaMrViewPresenter.View.DataSource == null || edycjaMrViewPresenter.AktualnaPozycja == null) return;

            RootWorkItem.State[StateConstants.ZrodloWywolaniaFormatkiK] = edycjaMrViewPresenter.View.ToString();
            RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
            RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = edycjaMrViewPresenter.AktualnaPozycja.SymbolTowaru;
            RootWorkItem.State[StateConstants.BlokadaWywolaniaFormatkiK] = StateConstants.BoolValueYes;

            Activate();
        }

        private void PokazEdycjePozycji(IWorkspace parentWorkspace, List<Dokument> dokumenty)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var edycjaMrViewPresenter = Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EDYCJA_MR_PRESENTER);
                edycjaMrViewPresenter.ResetujStronicowanie();
                edycjaMrViewPresenter.ZaladujDaneDoWidoku(dokumenty);
                parentWorkspace.Show(edycjaMrViewPresenter.View);
                Activate();
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        protected void OnZatwierdzonoDokumentMr(long idDokumentu)
        {
            if (ZatwierdzonoDokumentMr != null)
            {
                ZatwierdzonoDokumentMr(this, new DataEventArgs<long>(idDokumentu));
            }
        }

        public void WyswietlOstrzezenie(string tekst)
        {
            ShowMessageBox(tekst, Info);
        }

        public bool UruchomRaport(string nazwaFormularza, long idDokumentu, long idPozycji)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                Services.Get<IWydanieMRService>(true).UruchomRaport(nazwaFormularza, idDokumentu, idPozycji, RootWorkItem.State[StateConstants.ZONE] as string);
                if (StatusOperacji.ERROR.Equals(RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS]))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] as string, BLAD, RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                    return false;
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }

            return true;
        }

        private static Dokument UtworzDokumentBezPozycji(Naglowek naglowek)
        {
            var wynik = new Dokument { Naglowek = naglowek };
            if (!wynik.Naglowek.Status.Equals(Rezerwowany))
            {
                wynik.Naglowek.Status = Rezerwowany;
            }
            return wynik;
        }

        public Pozycja PobierzPozycjeDokumentuMr(long id)
        {
            var pozycjaMR = Services.Get<IWydanieMRService>(true).PobierzPozycjeMR(id);

            if (!RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(StatusOperacji.SUCCESS))
            {
                ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), NazwaModulu,
                               RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE].ToString());
            }
            return pozycjaMR;
        }

        //[EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ, Thread = ThreadOption.UserInterface)]
        //public void UstawNaPodstawieKoduKreskowego(object sender, EventArgs args)
        //{
        //    ShowMessageBox("Status : ", "Status");
        //    if (Status == WorkItemStatus.Active)
        //    {
        //        try
        //        {
        //            ShowMessageBox("Status : OK" , "Status");

        //            if (RootWorkItem.State[StateConstants.BAR_CODE] != null)
        //            {
        //                ShowMessageBox("RootWorkItem.State[StateConstants.BAR_CODE] != null ", "Status");

        //                Activate();
        //                var odczyt = RootWorkItem.State[StateConstants.BAR_CODE].ToString();

        //                ShowMessageBox("odczyt " + odczyt, "Status");

        //                if ((odczyt != null) && (odczyt.IndexOf("WAG", 0) == 0))
        //                {
        //                    var presenter = Items.Get<EdycjaMRViewPresenter>(ItemsConstants.EDYCJA_MR_PRESENTER);

        //                    FunctionsHelper.SetCursorWait();

        //                    var masa = Services.Get<IWagiService>(true).pobierzOdczytWagi(odczyt);

        //                    ShowMessageBox("Pobralem masa z pobierzOdczytWagi o wartosci : " + masa.wartosc,"debug");

        //                    if (masa.statusOperacji.status.Equals(StatusOperacji.ERROR))
        //                    {
        //                        ShowMessageBox(masa.statusOperacji.tekst, BLAD, masa.statusOperacji.stosWywolan);
        //                        return;
        //                    }
        //                    else
        //                    {
        //                        var jm = Services.Get<IWydanieMRService>(true).PodstawowaJednostkaMiaryDlaIndeksu(presenter.AktualnaPozycja.SymbolTowaru);

        //                        ShowMessageBox("Pobralem jm z PodstawowaJednostkaMiaryDlaIndeksu o wartosci : " + jm.wartosc, "debug");


        //                        if (!Equals(jm, WagiService.JednostkaMiaryKg))
        //                        {
        //                            ShowMessageBox("DODAC", "CAP");
        //                            return;
        //                        }

        //                        Services.Get<IWagiService>(true).przeliczNaKg(ref masa);

        //                        ShowMessageBox("Wykoano przeliczNaKg " + masa.wartosc, "debug");

        //                        presenter.WczytajMase(masa);

        //                        ShowMessageBox("Wykoano WczytajMase " + masa.wartosc, "debug");

        //                    }
        //                }
        //            }
        //        }
        //        finally
        //        {
        //            FunctionsHelper.SetCursorDefault();
        //            Activate();
        //        }
        //    }
        //}
    }
}
