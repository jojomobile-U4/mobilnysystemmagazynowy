#region

using System;
using Common;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.Commands;
using WydanieMRModule.EdycjaMR;
using WydanieMRModule.EdycjaMR.Search;
using WydanieMRModule.ListaMR;
using WydanieMRModule.ListaMR.Search;
using WydanieMRModule.WyborStrefy;

#endregion

namespace WydanieMRModule
{
    public class ModuleInitializer : ModuleInit
    {
        private readonly WorkItem m_WorkItem;

        public ModuleInitializer([ServiceDependency] WorkItem workItem)
        {
            m_WorkItem = workItem;
        }

        public void Initialize()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                InitializeListaMR();
                InitializeEdycjaMR();
                InitializeWagi();
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        private void InitializeEdycjaMR()
        {
            if (!m_WorkItem.WorkItems.Contains(WorkItemsConstants.EDYCJA_MR_WORKITEM))
            {
                var edycjaMRWorkItem = m_WorkItem.WorkItems.AddNew<EdycjaMRWorkItem>(WorkItemsConstants.EDYCJA_MR_WORKITEM);
                edycjaMRWorkItem.State[StateConstants.CRITERIAS] = string.Empty;
                edycjaMRWorkItem.RootWorkItem.State[StateConstants.ZONE] = string.Empty;
                edycjaMRWorkItem.RootWorkItem.State[StateConstants.ZONE] = string.Empty;


                var edycjaMRSearchViewPresenter = new EdycjaMRSearchViewPresenter(edycjaMRWorkItem.Items.AddNew<EdycjaMRSearchView>());
                edycjaMRWorkItem.Items.Add(edycjaMRSearchViewPresenter, ItemsConstants.EDYCJA_MR_SEARCH_PRESENTER);

                var edycjaMRViewPresenter = new EdycjaMRViewPresenter(edycjaMRWorkItem.Items.AddNew<EdycjaMRView>());
                edycjaMRWorkItem.Items.Add(edycjaMRViewPresenter, ItemsConstants.EDYCJA_MR_PRESENTER);
            }
        }

        private void InitializeListaMR()
        {
            if (!m_WorkItem.WorkItems.Contains(WorkItemsConstants.LISTA_MR_WORKITEM))
            {
                var listaMRWorkItem = m_WorkItem.WorkItems.AddNew<ListaMRWorkItem>(WorkItemsConstants.LISTA_MR_WORKITEM);
                listaMRWorkItem.State[StateConstants.CRITERIAS] = new KryteriaZapytaniaListyMR();
                listaMRWorkItem.RootWorkItem.State[StateConstants.ZONE] = string.Empty;

                var searchPresenter = new ListaMRSearchViewPresenter(listaMRWorkItem.Items.AddNew<ListaMRSearchView>());
                listaMRWorkItem.Items.Add(searchPresenter, ItemsConstants.LISTA_MR_SEARCH_PRESENTER);

                var listaMRViewPresenter = new ListaMRViewPresenter(listaMRWorkItem.Items.AddNew<ListaMRView>());
                listaMRWorkItem.Items.Add(listaMRViewPresenter, ItemsConstants.LISTA_MR_PRESENTER);

                var wyborStrefyViewPresenter = new WyborStrefyViewPresenter(listaMRWorkItem.Items.AddNew<WyborStrefyView>());
                listaMRWorkItem.Items.Add(wyborStrefyViewPresenter, ItemsConstants.LISTA_MR_WYBOR_STREFY_PRESENTER);

                m_WorkItem.RootWorkItem.State[StateConstants.AktywneCzytanieKodu] = StateConstants.BoolValueYes;
            }
        }

        private void InitializeWagi()
        {
            if (!m_WorkItem.Services.Contains(typeof(IWagiService)))
            {
                m_WorkItem.Services.AddNew(typeof(WagiService), typeof(IWagiService));
            }
        }

        [CommandHandler(CommandConstants.WydanieMRRealizacja)]
        public void WydanieMR(object sender, EventArgs e)
        {
            m_WorkItem.RootWorkItem.State[StateConstants.WydanieMRModuleKontekstView] = ContextConstants.WydanieMRRealizacja;

            Initialize();

            var mainWorkspace = m_WorkItem.Workspaces[WorkspacesConstants.MAIN_WORKSPACE];
            var listaMRWorkItem = m_WorkItem.WorkItems.Get<ListaMRWorkItem>(WorkItemsConstants.LISTA_MR_WORKITEM);
            listaMRWorkItem.RootWorkItem.State[StateConstants.PrzejecieSkanIdLinii] = ModuleBooleanConstants.No;
            listaMRWorkItem.RootWorkItem.State[StateConstants.PrzejecieSkanRodzaj] = ModuleSkanRodzajConstants.Lokalizacja;
            listaMRWorkItem.Show(mainWorkspace);
        }


        [CommandHandler(CommandConstants.WydanieMRPrzejecie)]
        public void WydanieMRPrzejecie(object sender, EventArgs e)
        {
            m_WorkItem.RootWorkItem.State[StateConstants.WydanieMRModuleKontekstView] = ContextConstants.WydanieMRPrzejecie;
            
            InitializeEdycjaMR();

            var mainWorkspace = m_WorkItem.Workspaces[WorkspacesConstants.MAIN_WORKSPACE];

            m_WorkItem.RootWorkItem.State[StateConstants.PrzejecieSkanIdLinii] = ModuleBooleanConstants.Yes;
            m_WorkItem.RootWorkItem.State[StateConstants.PrzejecieSkanRodzaj] = ModuleSkanRodzajConstants.IdLinii;

            m_WorkItem.WorkItems.Get<EdycjaMRWorkItem>(WorkItemsConstants.EDYCJA_MR_WORKITEM).Show(mainWorkspace, true);
        }
    }
}
