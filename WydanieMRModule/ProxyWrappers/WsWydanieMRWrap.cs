#region

using System;
using System.Net;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using Common;
using Common.Base;
using OpenNETCF.Web.Services2;
using WydanieMRModule.WydanieMRProxy;

#endregion

namespace WydanieMRModule.ProxyWrappers
{
    public class WsWydanieMRWrap : WsWydanieMR, IWSSecurity
    {
        #region Private s

        #endregion

        #region Properties

        public SecurityHeader SecurityHeader { get; set; }

        #endregion

        #region Constructors

        public WsWydanieMRWrap(WebServiceConfiguration configuration)
        {
            Url = configuration.Url;
        }

        #endregion

        #region Protected Methods

        protected override WebRequest GetWebRequest(Uri uri)
        {
            var request = (HttpWebRequest) base.GetWebRequest(uri);

            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;

            return request;
        }

        #endregion

        #region Methods

        [SoapDocumentMethod("urn:pobierzListeNaglowkow", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeNaglowkowResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeNaglowkowResponse pobierzListeNaglowkow([XmlElement("pobierzListeNaglowkow", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeNaglowkowWrap pobierzListeNaglowkow1)
        {
            var results = Invoke("pobierzListeNaglowkow", new object[] {pobierzListeNaglowkow1});
            return ((pobierzListeNaglowkowResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:pobierzListeSektorow", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeSektorowResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeSektorowResponse pobierzListeSektorow()
        {
            var results = Invoke("pobierzListeSektorow", new object[0]);
            return ((pobierzListeSektorowResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:zatwierdzDokument", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("zatwierdzDokumentResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public zatwierdzDokumentResponse zatwierdzDokument([XmlElement("zatwierdzDokument", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] zatwierdzDokument zatwierdzDokument1)
        {
            var results = Invoke("zatwierdzDokument", new object[] {zatwierdzDokument1});
            return ((zatwierdzDokumentResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:rezerwujDokument", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("rezerwujDokumentResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public rezerwujDokumentResponse rezerwujDokument([XmlElement("rezerwujDokument", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] rezerwujDokument rezerwujDokument1)
        {
            var results = Invoke("rezerwujDokument", new object[] {rezerwujDokument1});
            return ((rezerwujDokumentResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:pobierzListeDokumentow", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeDokumentowResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeDokumentowResponse pobierzListeDokumentow([XmlElement("pobierzListeDokumentow", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeDokumentowWrap pobierzListeDokumentow1)
        {
            var results = Invoke("pobierzListeDokumentow", new object[] {pobierzListeDokumentow1});
            return ((pobierzListeDokumentowResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:aktualizujPozycje", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("aktualizujPozycjeResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public aktualizujPozycjeResponse aktualizujPozycje([XmlElement("aktualizujPozycje", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] aktualizujPozycje aktualizujPozycje1)
        {
            var results = Invoke("aktualizujPozycje", new object[] {aktualizujPozycje1});
            return ((aktualizujPozycjeResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:porzucDokument", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("porzucDokumentResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public porzucDokumentResponse porzucDokument([XmlElement("porzucDokument", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] porzucDokument porzucDokument1)
        {
            var results = Invoke("porzucDokument", new object[] {porzucDokument1});
            return ((porzucDokumentResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:zakonczPozycje", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("zakonczPozycjeResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public zakonczPozycjeResponse zakonczPozycje([XmlElement("zakonczPozycje", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] zakonczPozycje zakonczPozycje1)
        {
            var results = Invoke("zakonczPozycje", new object[] {zakonczPozycje1});
            return ((zakonczPozycjeResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:usunPozycje", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("usunPozycjeResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public usunPozycjeResponse usunPozycje([XmlElement("usunPozycje", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] usunPozycje usunPozycje1)
        {
            var results = Invoke("usunPozycje", new object[] {usunPozycje1});
            return ((usunPozycjeResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:uruchomRaport", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("uruchomRaportResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public uruchomRaportResponse uruchomRaport([XmlElement("uruchomRaport", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] uruchomRaport uruchomRaport1)
        {
            var results = Invoke("uruchomRaport", new object[] {uruchomRaport1});
            return ((uruchomRaportResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:iloscDokumentow", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("iloscDokumentowResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public iloscDokumentowResponse iloscDokumentow([XmlElement("iloscDokumentow", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] iloscDokumentowWrap iloscDokumentow1)
        {
            var results = Invoke("iloscDokumentow", new object[] {iloscDokumentow1});
            return ((iloscDokumentowResponse) (results[0]));
        }

        [SoapDocumentMethod("urn:podstawowaJednostkaMiaryDlaIndeksu", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("podstawowaJednostkaMiaryDlaIndeksuResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public podstawowaJednostkaMiaryDlaIndeksuResponse podstawowaJednostkaMiaryDlaIndeksu([XmlElement("podstawowaJednostkaMiaryDlaIndeksu", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] podstawowaJednostkaMiaryDlaIndeksu podstawowaJednostkaMiaryDlaIndeksu1)
        {
            var results = Invoke("podstawowaJednostkaMiaryDlaIndeksu", new object[]{podstawowaJednostkaMiaryDlaIndeksu1});
            return ((podstawowaJednostkaMiaryDlaIndeksuResponse) (results[0]));
        }

        [SoapDocumentMethodAttribute("urn:zmienStanZarezerwowania", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElementAttribute("zmienStanZarezerwowaniaResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public zmienStanZarezerwowaniaResponse zmienStanZarezerwowania([XmlElementAttribute("zmienStanZarezerwowania", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] zmienStanZarezerwowania zmienStanZarezerwowania1)
        {
            object[] results = Invoke("zmienStanZarezerwowania", new object[] {
                        zmienStanZarezerwowania1});
            return ((zmienStanZarezerwowaniaResponse)(results[0]));
        }

        /// <remarks/>
        [SoapDocumentMethodAttribute("urn:pobierzPozycjeDokumentuMr", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElementAttribute("pobierzPozycjeDokumentuMrResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzPozycjeDokumentuMrResponse pobierzPozycjeDokumentuMr([XmlElementAttribute("pobierzPozycjeDokumentuMr", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzPozycjeDokumentuMr pobierzPozycjeDokumentuMr1)
        {
            var results = Invoke("pobierzPozycjeDokumentuMr", new object[] {
                        pobierzPozycjeDokumentuMr1});
            return ((pobierzPozycjeDokumentuMrResponse)(results[0]));
        }

        [SoapDocumentMethodAttribute("urn:iloscNiezakonczonychPozycjiPp", Use = System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle = System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: XmlElementAttribute("iloscNiezakonczonychPozycjiPpResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public iloscNiezakonczonychPozycjiPpResponse iloscNiezakonczonychPozycjiPp([System.Xml.Serialization.XmlElementAttribute("iloscNiezakonczonychPozycjiPp", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] iloscNiezakonczonychPozycjiPp iloscNiezakonczonychPozycjiPp1)
        {
            var results = Invoke("iloscNiezakonczonychPozycjiPp", new object[] {iloscNiezakonczonychPozycjiPp1});
            return ((iloscNiezakonczonychPozycjiPpResponse)(results[0]));
        }

        #endregion
    }

    public class pobierzListeDokumentowWrap : pobierzListeDokumentow
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idDokumentu
        {
            get { return base.idDokumentu; }
            set { base.idDokumentu = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idPozycji
        {
            get { return base.idPozycji; }
            set { base.idPozycji = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string lokalizacja
        {
            get { return base.lokalizacja; }
            set { base.lokalizacja = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string rodzajDokumentu
        {
            get { return base.rodzajDokumentu; }
            set { base.rodzajDokumentu = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string rodzajPozycji
        {
            get { return base.rodzajPozycji; }
            set { base.rodzajPozycji = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string zakonczona
        {
            get { return base.zakonczona; }
            set { base.zakonczona = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string uzytkownik
        {
            get { return base.uzytkownik; }
            set { base.uzytkownik = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? numerStrony
        {
            get { return base.numerStrony; }
            set { base.numerStrony = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? wielkoscStrony
        {
            get { return base.wielkoscStrony; }
            set { base.wielkoscStrony = value; }
        }
    }

    public class pobierzListeNaglowkowWrap : pobierzListeNaglowkow
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string symbolDokumentu
        {
            get { return base.symbolDokumentu; }
            set { base.symbolDokumentu = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string status
        {
            get { return base.status; }
            set { base.status = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string zatwierdzony
        {
            get { return base.zatwierdzony; }
            set { base.zatwierdzony = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string dostepnosc
        {
            get { return base.dostepnosc; }
            set { base.dostepnosc = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string priorytet
        {
            get { return base.priorytet; }
            set { base.priorytet = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public DateTime? dataDokumentu
        {
            get { return base.dataDokumentu; }
            set { base.dataDokumentu = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string symbolOdbiorcy
        {
            get { return base.symbolOdbiorcy; }
            set { base.symbolOdbiorcy = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string strefa
        {
            get { return base.strefa; }
            set { base.strefa = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string zarezerwowany
        {
            get { return base.zarezerwowany; }
            set { base.zarezerwowany = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string uzytkownik
        {
            get { return base.uzytkownik; }
            set { base.uzytkownik = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string sortujWgDostepnosci
        {
            get { return base.sortujWgDostepnosci; }
            set { base.sortujWgDostepnosci = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? magaIdFk
        {
            get { return base.magaIdFk; }
            set { base.magaIdFk = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? numerStrony
        {
            get { return base.numerStrony; }
            set { base.numerStrony = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? wielkoscStrony
        {
            get { return base.wielkoscStrony; }
            set { base.wielkoscStrony = value; }
        }
    }

    public class UruchomRaportWrap : uruchomRaport
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string nazwaFormularza
        {
            get { return base.nazwaFormularza; }
            set { base.nazwaFormularza = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idDokumentuMR
        {
            get { return base.idDokumentuMR; }
            set { base.idDokumentuMR = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? idPozycji
        {
            get { return base.idPozycji; }
            set { base.idPozycji = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string symbolStrefy
        {
            get { return base.symbolStrefy; }
            set { base.symbolStrefy = value; }
        }

    }


    [XmlType(AnonymousType = true)]
    public class iloscDokumentowWrap : iloscDokumentow
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string symbolDokumentu
        {
            get { return base.symbolDokumentu; }
            set { base.symbolDokumentu = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string status
        {
            get { return base.status; }
            set { base.status = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string zatwierdzony
        {
            get { return base.zatwierdzony; }
            set { base.zatwierdzony = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string dostepnosc
        {
            get { return base.dostepnosc; }
            set { base.dostepnosc = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string priorytet
        {
            get { return base.priorytet; }
            set { base.priorytet = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public DateTime? dataDokumentu
        {
            get { return base.dataDokumentu; }
            set { base.dataDokumentu = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string symbolOdbiorcy
        {
            get { return base.symbolOdbiorcy; }
            set { base.symbolOdbiorcy = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string strefa
        {
            get { return base.strefa; }
            set { base.strefa = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string zarezerwowany
        {
            get { return base.zarezerwowany; }
            set { base.zarezerwowany = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string uzytkownik
        {
            get { return base.uzytkownik; }
            set { base.uzytkownik = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? magaIdFk
        {
            get { return base.magaIdFk; }
            set { base.magaIdFk = value; }
        }

    }
}