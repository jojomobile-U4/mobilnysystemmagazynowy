using System;
using System.Collections.Generic;
using System.Text;
using WydanieMRModule.ListaMR.Search;
using WydanieMRModule.WydanieMRProxy;
using Dokument=WydanieMRModule.DataModel.Dokument;
using ListaDokumentow=WydanieMRModule.DataModel.ListaDokumentow;
using ListaNaglowkow=WydanieMRModule.DataModel.ListaNaglowkow;
using ListaSektorow=WydanieMRModule.DataModel.ListaSektorow;
using Pozycja=WydanieMRModule.DataModel.Pozycja;
using StatusOperacji=Common.DataModel.StatusOperacji;

namespace WydanieMRModule.Services
{
	public interface IWydanieMRService
	{
		ListaSektorow PobierzListeStref();

		ListaNaglowkow PobierzListeNaglowkowMR(
			KryteriaZapytaniaListyMR kryteria,
			int? numerStrony);

		ListaDokumentow PobierzListeDokumentowMR(
			//TODO: zmienic na kryteria
			//KryteriaZapytaniaEdycjaMR kryteria,
            long? idDokumentuMR, long? pozycjaDokumentu_ID, string lokalizacja, string rodzajDokumentu, string rodzajPozycji, string zakonczona, int? numerStrony);

		StatusOperacji ZakonczPozycje(long id);

        StatusOperacji UsunPozycje(long id);

		StatusOperacji RezerwujDokument(long idDokumentuMR);

        StatusOperacji ZmienStanZarezerwowania(long idDokumentuMR);

		StatusOperacji ZatwierdzDokument(long idDokumentuMR);

        Dokument PobierzDokumentMR(long idPozycji);

        Pozycja PobierzPozycjeMR(long id);

		StatusOperacji PorzucDokument(long idDokumentu);

        void PobierzAsynchronicznieIlosc(KryteriaZapytaniaListyMR kryteria);
        void AnulujOczekujaceOperacje();

        void UruchomRaport(string nazwaFormularza, long idDokumentu, long idPozycji, string symbolStrefy);

        StatusOperacji AktaulizujPozycje(Pozycja pozycja);

        StatusOperacji AktaulizujPozycje(Pozycja pozycja, float iloscOrg, bool komunikat);

	    JednostkaMiary PodstawowaJednostkaMiaryDlaIndeksu(string indeks);

	    long IloscNiezakonczonychPozycji(long idDokumentu);
	}
}
