#region

using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.DataModel;
using Common.DbErrorHandling;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using WydanieMRModule.ListaMR.Search;
using WydanieMRModule.ProxyWrappers;
using WydanieMRModule.WydanieMRProxy;
using Dokument = WydanieMRModule.DataModel.Dokument;
using ListaDokumentow = WydanieMRModule.DataModel.ListaDokumentow;
using ListaNaglowkow = WydanieMRModule.DataModel.ListaNaglowkow;
using ListaSektorow = WydanieMRModule.DataModel.ListaSektorow;
using Pozycja = WydanieMRModule.DataModel.Pozycja;
using StatusOperacji = Common.DataModel.StatusOperacji;

#endregion

namespace WydanieMRModule.Services
{
    [Service(typeof(IWydanieMRService))]
    public class WydanieMRService : ServiceBase, IWydanieMRService
    {
        #region Private Fields

        private WsWydanieMRWrap m_AsyncServiceAgent;

        #endregion

        #region Public Fields

        [EventPublication(EventBrokerConstants.ODCZYTANO_ILOSC)]
        public event EventHandler OdczytanoIlosc;

        #endregion

        #region Properties

        protected override WebServiceConfiguration Configuration
        {
            get { return MyWorkItem.Configuration.WydanieMRWS; }
        }

        #endregion

        #region Constructors

        public WydanieMRService([ServiceDependency] WorkItem workItem)
            : base(workItem)
        {
        }

        #endregion

        #region IWydanieMRService Members

        public ListaSektorow PobierzListeStref()
        {
            var listaSektorow = new ListaSektorow();
            try
            {
                using (var serviceAgent = new WsWydanieMRWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    listaSektorow = new ListaSektorow(serviceAgent.pobierzListeSektorow().@return);
                }
            }
            catch (Exception ex)
            {
                //TODO: dodac zapisywanie do logow
                listaSektorow.StatusOperacji = new StatusOperacji { Status = StatusOperacji.ERROR, Tekst = ex.Message };
            }
            return listaSektorow;
        }

        public ListaNaglowkow PobierzListeNaglowkowMR(KryteriaZapytaniaListyMR kryteria, int? numerStrony)
        {
            var listaNaglowkow = new ListaNaglowkow();
            try
            {
                using (var serviceAgent = new WsWydanieMRWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzListeNaglowkowWrap
                                    {
                                        symbolDokumentu = kryteria.Dokument != null ? kryteria.Dokument.ToUpper() : null,
                                        status = kryteria.Status != null ? kryteria.Status.ToUpper() : null,
                                        dostepnosc = kryteria.Dostepnosc,
                                        priorytet = kryteria.Priorytet,
                                        dataDokumentu = kryteria.Data,
                                        symbolOdbiorcy = kryteria.SymbolOdbiorcy,
                                        strefa = kryteria.Strefa,
                                        numerStrony = numerStrony ?? 0,
                                        zatwierdzony = DBBool.FALSE,
                                        magaIdFk = kryteria.MagaIdFk,
                                        wielkoscStrony = Configuration.WielkoscStrony
                                    };

                    listaNaglowkow = new ListaNaglowkow(serviceAgent.pobierzListeNaglowkow(param).@return);
                }
            }
            catch (Exception ex)
            {
                //TODO: dodac zapisywanie do logow
                listaNaglowkow.StatusOperacji = new StatusOperacji { Status = StatusOperacji.ERROR, Tekst = ex.Message };
            }
            return listaNaglowkow;
        }

        public ListaDokumentow PobierzListeDokumentowMR(long? idDokumentuMr, long? idPozycjiDokumentu, string lokalizacja, string rodzajDokumentu, string rodzajPozycji, string zakonczona, int? numerStrony)
        {
            var listaDokumentow = new ListaDokumentow();
            var listaPozycji = new List<Pozycja>();
            try
            {
                using (var serviceAgent = new WsWydanieMRWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzListeDokumentowWrap
                                    {
                                        idDokumentu = idDokumentuMr,
                                        idPozycji = idPozycjiDokumentu,
                                        lokalizacja = Localization.AddPrefix(lokalizacja),
                                        rodzajDokumentu = rodzajDokumentu,
                                        rodzajPozycji = rodzajPozycji,
                                        numerStrony = numerStrony ?? 0,
                                        wielkoscStrony = Configuration.WielkoscStrony,
                                        zakonczona = zakonczona
                                    };

                    listaDokumentow = new ListaDokumentow(serviceAgent.pobierzListeDokumentow(param).@return);
                }
            }
            catch (Exception ex)
            {
                //TODO: dodac zapisywanie do logow
                listaDokumentow.StatusOperacji = new StatusOperacji { Status = StatusOperacji.ERROR, Tekst = ex.Message };
            }

            if ((listaDokumentow.Lista != null) && (listaDokumentow.Lista.Count > 0))
            {
                for (var nrDokumentu = 0; nrDokumentu < listaDokumentow.Lista.Count; nrDokumentu++)
                {
                    if ((listaDokumentow.Lista[nrDokumentu].Pozycje != null) && (listaDokumentow.Lista[nrDokumentu].Pozycje.Count > 0))
                    {
                        for (var i = 0; i < listaDokumentow.Lista[nrDokumentu].Pozycje.Count; i++)
                        {
                            listaPozycji.Add(listaDokumentow.Lista[nrDokumentu].Pozycje[i]);
                        }
                        listaDokumentow.Lista[nrDokumentu].Pozycje.RemoveRange(0, listaDokumentow.Lista[nrDokumentu].Pozycje.Count);

                        for (var i = 0; i < listaPozycji.Count; i++)
                        {
                            listaDokumentow.Lista[nrDokumentu].Pozycje.Add(listaPozycji[i]);
                        }
                    }
                }
            }

            return listaDokumentow;
        }

        public StatusOperacji ZakonczPozycje(long id)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieMRWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new zakonczPozycje { id = id };

                    var statusOperacjiWs = serviceAgent.zakonczPozycje(param).@return;
                    statusOperacji.Status = statusOperacjiWs.status;
                    statusOperacji.Tekst = statusOperacjiWs.tekst;
                }
            }
            catch (Exception ex)
            {
                //TODO: dodac zapisywanie do logow
                statusOperacji.Status = StatusOperacji.ERROR;
                statusOperacji.Tekst = ex.Message;
            }
            return statusOperacji;
        }

        public StatusOperacji UsunPozycje(long id)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieMRWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new usunPozycje { id = id };

                    var statusOperacjiWs = serviceAgent.usunPozycje(param).@return;
                    statusOperacji.Status = statusOperacjiWs.status;
                    statusOperacji.Tekst = statusOperacjiWs.tekst;
                }
            }
            catch (Exception ex)
            {
                //TODO: dodac zapisywanie do logow
                statusOperacji.Status = StatusOperacji.ERROR;
                statusOperacji.Tekst = ex.Message;
            }
            return statusOperacji;
        }

        public StatusOperacji RezerwujDokument(long idDokumentuMr)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieMRWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new rezerwujDokument { id = idDokumentuMr, uzytkownik = MyWorkItem.State[StateConstants.USER] as string };

                    var statusOperacjiWs = serviceAgent.rezerwujDokument(param).@return;
                    statusOperacji.Status = statusOperacjiWs.status;
                    statusOperacji.Tekst = statusOperacjiWs.tekst;
                }
            }
            catch (Exception ex)
            {
                //TODO: dodac zapisywanie do logow
                statusOperacji.Status = StatusOperacji.ERROR;
                statusOperacji.Tekst = ex.Message;
            }
            return statusOperacji;
        }

        public StatusOperacji ZmienStanZarezerwowania(long idDokumentuMr)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieMRWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new zmienStanZarezerwowania { id = idDokumentuMr };

                    var statusOperacjiWs = serviceAgent.zmienStanZarezerwowania(param).@return;
                    statusOperacji.Status = statusOperacjiWs.status;
                    statusOperacji.Tekst = statusOperacjiWs.tekst;

                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = statusOperacji.Status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = statusOperacji.Tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = statusOperacji.StosWywolan;
                }
            }
            catch (Exception ex)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = ex.Message;
                statusOperacji.Status = StatusOperacji.ERROR;
                statusOperacji.Tekst = ex.Message;
            }
            return statusOperacji;
        }

        public StatusOperacji ZatwierdzDokument(long idDokumentuMr)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieMRWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new zatwierdzDokument { id = idDokumentuMr };

                    var statusOperacjiWs = serviceAgent.zatwierdzDokument(param).@return;
                    statusOperacji.Status = statusOperacjiWs.status;
                    statusOperacji.Tekst = statusOperacjiWs.tekst;
                }
            }
            catch (Exception ex)
            {
                //TODO: dodac zapisywanie do logow
                statusOperacji.Status = StatusOperacji.ERROR;
                statusOperacji.Tekst = ex.Message;
            }
            return statusOperacji;
        }

        public Dokument PobierzDokumentMR(long idPozycji)
        {
            var lista = PobierzListeDokumentowMR(null, idPozycji, string.Empty, null, "PP", "N", null);
            if (lista == null ||
                lista.Lista == null ||
                lista.Lista.Count == 0 ||
                lista.StatusOperacji.Status.Equals(StatusOperacji.ERROR))
            {
                //TODO: dodac zapisywanie do logow
                return null;
            }
            return lista.Lista[0];
        }

        public Pozycja PobierzPozycjeMR(long id)
        {
            Pozycja pozycja = null;
            if (id > 0)
            {
                try
                {
                    using (var serviceAgent = new WsWydanieMRWrap(Configuration))
                    {
                        SecureSoapMessage(serviceAgent);
                        var param = new pobierzPozycjeDokumentuMr
                                        {
                                            idPozycji = id
                                        };

                        pozycja = new Pozycja(serviceAgent.pobierzPozycjeDokumentuMr(param).@return);
                    }
                }
                catch (Exception ex)
                {
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = ex.Message;
                }
            }

            return pozycja;
        }

        public long IloscNiezakonczonychPozycji(long idDokumentu)
        {
            Ilosc ilosc = null;
            if (idDokumentu > 0)
            {
                try
                {
                    using (var serviceAgent = new WsWydanieMRWrap(Configuration))
                    {
                        SecureSoapMessage(serviceAgent);
                        var param = new iloscNiezakonczonychPozycjiPp
                                        {
                                            idDokumentu = idDokumentu
                                        };

                        ilosc = serviceAgent.iloscNiezakonczonychPozycjiPp(param).@return;
                    }
                }
                catch (Exception ex)
                {
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = ex.Message;
                }
            }

            return ilosc != null ? ilosc.ilosc : 0;
        }
        

        public StatusOperacji PorzucDokument(long idDokumentu)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieMRWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new porzucDokument { id = idDokumentu };
                    var statusOperacjiWs = serviceAgent.porzucDokument(param).@return;

                    statusOperacji = new StatusOperacji(statusOperacjiWs.status, statusOperacjiWs.tekst);
                }
            }
            catch (Exception ex)
            {
                statusOperacji.Status = StatusOperacji.ERROR;
                statusOperacji.Tekst = ex.Message;
            }
            return statusOperacji;
        }

        public StatusOperacji AktaulizujPozycje(Pozycja pozycja)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieMRWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);

                   var param = new aktualizujPozycje
                                    {
                                        pozycja = new WydanieMRProxy.Pozycja
                                                      {
                                                          ilosc = pozycja.Ilosc,
                                                          id = pozycja.Id,
                                                          waga = pozycja.Waga,
                                                          idDokumentu = pozycja.IdDokumentu
                                                      }
                                    };

                    var statusOperacjiWs = serviceAgent.aktualizujPozycje(param).@return;

                    statusOperacji = new StatusOperacji(statusOperacjiWs.status, statusOperacjiWs.tekst);
                }
            }
            catch (Exception ex)
            {
                statusOperacji.Status = StatusOperacji.ERROR;
                statusOperacji.Tekst = ex.Message;
            }

            return statusOperacji;
        }

        [System.Obsolete("Use : AktaulizujPozycje(Pozycja pozycja)")]
        public StatusOperacji AktaulizujPozycje(Pozycja pozycja, float iloscOrg, bool komunikat)
        {
            var statusOperacji = new StatusOperacji();
            try
            {
                using (var serviceAgent = new WsWydanieMRWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);

                    if (iloscOrg != pozycja.Ilosc)
                    {
                        var error = new DbError();
                        var dbeh = new DbErrorHandle();

                        if (komunikat)
                        {
                            MessageBox.Show("Ilo�� wprowadzona przez u�ytkownika [" + pozycja.Ilosc + "] r�ni si� od warto�c oryginalnej [" + iloscOrg + "].", "AUDYT", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                        }

                        error.Tekst = "DOKUMENT MR idDokumentu = " + pozycja.IdDokumentu + " : Ilosc zosta�a zmieniona z [" + iloscOrg + "] na [" + pozycja.Ilosc + "]. Uzytkownik : " + MyWorkItem.State[StateConstants.USER];
                        error.StosWywolan = string.Empty;
                        dbeh.WriteToFile(error);
                    }

                    var param = new aktualizujPozycje
                    {
                        pozycja = new WydanieMRProxy.Pozycja
                        {
                            ilosc = pozycja.Ilosc,
                            id = pozycja.Id,
                            waga = pozycja.Waga,
                            idDokumentu = pozycja.IdDokumentu
                        }
                    };

                    var statusOperacjiWs = serviceAgent.aktualizujPozycje(param).@return;

                    statusOperacji = new StatusOperacji(statusOperacjiWs.status, statusOperacjiWs.tekst);
                }
            }
            catch (Exception ex)
            {
                statusOperacji.Status = StatusOperacji.ERROR;
                statusOperacji.Tekst = ex.Message;
            }

            return statusOperacji;
        }



        public JednostkaMiary PodstawowaJednostkaMiaryDlaIndeksu(string indeks)
        {
            try
            {
                using (var serviceAgent = new WsWydanieMRWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);

                    var podstawowaJednostkaMiaryDlaIndeksu = new podstawowaJednostkaMiaryDlaIndeksu { indeks = indeks };

                    var podstawowaJednostkaMiary = serviceAgent.podstawowaJednostkaMiaryDlaIndeksu(podstawowaJednostkaMiaryDlaIndeksu).@return;

                    return podstawowaJednostkaMiary;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void UruchomRaport(string nazwaFormularza, long idDokumentu, long idPozycji, string symbolStrefy)
        {
            try
            {
                using (var serviceAgent = new WsWydanieMRWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new UruchomRaportWrap { nazwaFormularza = nazwaFormularza, idDokumentuMR = idDokumentu, idPozycji = idPozycji, symbolStrefy = symbolStrefy };

                    var statusOperacji = serviceAgent.uruchomRaport(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = statusOperacji.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        #endregion

        #region Operacje asynchroniczne

        public void PobierzAsynchronicznieIlosc(KryteriaZapytaniaListyMR kryteria)
        {
            try
            {
                var serviceAgent = GetAsyncServiceAgentInstance();
                var param = new iloscDokumentowWrap
                                {
                                    symbolDokumentu = kryteria.Dokument != null ? kryteria.Dokument.ToUpper() : null,
                                    status = kryteria.Status != null ? kryteria.Status.ToUpper() : null,
                                    dostepnosc = kryteria.Dostepnosc,
                                    priorytet = kryteria.Priorytet,
                                    dataDokumentu = kryteria.Data,
                                    symbolOdbiorcy = kryteria.SymbolOdbiorcy,
                                    strefa = kryteria.Strefa,
                                    magaIdFk = kryteria.MagaIdFk,
                                    zatwierdzony = DBBool.FALSE
                                };

                PobranoIlosc(serviceAgent.iloscDokumentow(param).@return);
            }
            catch (ThreadAbortException)
            {
                OnOdczytanoIlosc();
            }
            catch (Exception)
            {
                //TODO: log wyjatkow
                MyWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
            }
        }

        public void AnulujOczekujaceOperacje()
        {
            if (m_AsyncServiceAgent != null)
            {
                DisposeAsyncServiceAgent();
            }
        }


        private void PobranoIlosc(Ilosc ilosc)
        {
            if (ilosc != null)
            {
                try
                {
                    MyWorkItem.RootWorkItem.State[StateConstants.ASYNC_OP_ILOSC_RESULT] = ilosc.ilosc;
                    OnOdczytanoIlosc();
                    DisposeAsyncServiceAgent();
                }
                catch (Exception)
                {
                    //TODO: log wyjatkow
                    MyWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = StatusOperacji.ERROR;
                }
            }
        }

        protected void OnOdczytanoIlosc()
        {
            if (OdczytanoIlosc != null)
            {
                OdczytanoIlosc(this, EventArgs.Empty);
            }
        }

        private WsWydanieMRWrap GetAsyncServiceAgentInstance()
        {
            if (m_AsyncServiceAgent == null)
            {
                m_AsyncServiceAgent = new WsWydanieMRWrap(Configuration);
                SecureSoapMessage(m_AsyncServiceAgent);
            }
            return m_AsyncServiceAgent;
        }

        private void DisposeAsyncServiceAgent()
        {
            m_AsyncServiceAgent.Abort();
            m_AsyncServiceAgent.Dispose();
            m_AsyncServiceAgent = null;
        }

        #endregion
    }
}