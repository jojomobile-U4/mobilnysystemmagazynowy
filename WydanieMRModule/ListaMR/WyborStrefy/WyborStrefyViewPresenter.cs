using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.Mobile.CompositeUI.Utility;
using Common.Base;
using Common;
using WydanieMRModule.ListaMR;
using System.Windows.Forms;
using WydanieMRModule.DataModel;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;

namespace WydanieMRModule.WyborStrefy
{
	public class WyborStrefyViewPresenter: PresenterBase
	{
		#region Private Fields

		private ListaSektorow m_ListaSektorow;

		#endregion
		#region Properties

		public IWyborStrefyView View
		{
			get { return m_view as IWyborStrefyView; }
		}

		protected ListaMRWorkItem MyWorkItem
		{
			get { return WorkItem as ListaMRWorkItem; }
		}

		public ListaSektorow ListaSektorow
		{
			get { return m_ListaSektorow; }
			set { m_ListaSektorow = value; }
		}

		#endregion
		#region Constructor

		public WyborStrefyViewPresenter(IWyborStrefyView view):base(view)
		{

		}

		#endregion
		#region Event Subscription

		[EventSubscription(EventBrokerConstants.ZONE_HAS_BEEN_READ)]
		public void OnZoneHasBeenRead(object sender, EventArgs e)
		{
            View.WybranaStrefa = MyWorkItem.RootWorkItem.State[StateConstants.ZONE] as string;
			ViewOK(sender, e);
		}

		#endregion
		#region Methods

		public void ZaladujDaneDoWidoku(ListaSektorow listaStref)
		{
			m_ListaSektorow = listaStref;
			View.DataSource = m_ListaSektorow;
            View.WybranaStrefa = MyWorkItem.RootWorkItem.State[StateConstants.ZONE] as string;
		}

		#endregion
		#region Overide Members

		protected override void AttachView()
		{
			View.OK += new EventHandler(ViewOK);
			View.Anuluj += new EventHandler(ViewAnuluj);
		}

		protected override void HandleNavigationKey(System.Windows.Forms.KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Enter:
					e.Handled = true;
					ViewOK(this, EventArgs.Empty);
					break;
				case Keys.Escape:
					e.Handled = true;
					ViewAnuluj(this, EventArgs.Empty);
					break;
			}
		}

		#endregion
		#region Obsluga widoku

		private void ViewOK(object sender, EventArgs e)
		{
            MyWorkItem.RootWorkItem.State[StateConstants.ZONE] = View.WybranaStrefa;
			CloseView();
			MyWorkItem.OnSzukajDokumentyMR(this, EventArgs.Empty);
		}

		private void ViewAnuluj(object sender, EventArgs e)
		{
			CloseView();
		}

		#endregion
	}
}
