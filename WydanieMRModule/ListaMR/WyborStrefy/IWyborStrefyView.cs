using Common.Base;
using System;
using WydanieMRModule.DataModel;

namespace WydanieMRModule.WyborStrefy
{
	public interface IWyborStrefyView: IViewBase
	{
		event EventHandler OK;
		event EventHandler Anuluj;

		ListaSektorow DataSource { set; }

		string WybranaStrefa { set; get; }
	}
}
