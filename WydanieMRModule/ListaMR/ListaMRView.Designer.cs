
using Common.Components;
namespace WydanieMRModule.ListaMR
{
	partial class ListaMRView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.lbTytul = new Common.Components.MSMLabel();
            this.lstDokumentyMR = new Common.Components.MSMListView();
            this.chLp = new System.Windows.Forms.ColumnHeader();
            this.chDokument = new System.Windows.Forms.ColumnHeader();
            this.chStatus = new System.Windows.Forms.ColumnHeader();
            this.chDostepnosc = new System.Windows.Forms.ColumnHeader();
            this.chPriorytet = new System.Windows.Forms.ColumnHeader();
            this.naglowekBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbData = new System.Windows.Forms.TextBox();
            this.lbData = new System.Windows.Forms.Label();
            this.tbSymbolOdbiorcy = new System.Windows.Forms.TextBox();
            this.lbSymbolOdbiorcy = new System.Windows.Forms.Label();
            this.tbNazwaOdbiorcy = new System.Windows.Forms.TextBox();
            this.lbNazwaOdbiorcy = new System.Windows.Forms.Label();
            this.tbIloscLP = new System.Windows.Forms.TextBox();
            this.lbIloscLP = new System.Windows.Forms.Label();
            this.btnPobierz = new System.Windows.Forms.Button();
            this.btnSzukaj = new System.Windows.Forms.Button();
            this.lrcNavigation = new Common.Components.LeftRightControl();
            this.tbPoleWydania = new System.Windows.Forms.TextBox();
            this.lbPoleWydania = new System.Windows.Forms.Label();
            this.pnlNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.naglowekBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnPobierz);
            this.pnlNavigation.Controls.Add(this.btnSzukaj);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 272);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 28);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 11;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "WYDANIE - LISTA MR";
            // 
            // lstDokumentyMR
            // 
            this.lstDokumentyMR.Columns.Add(this.chLp);
            this.lstDokumentyMR.Columns.Add(this.chDokument);
            this.lstDokumentyMR.Columns.Add(this.chStatus);
            this.lstDokumentyMR.Columns.Add(this.chDostepnosc);
            this.lstDokumentyMR.Columns.Add(this.chPriorytet);
            this.lstDokumentyMR.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lstDokumentyMR.FullRowSelect = true;
            this.lstDokumentyMR.Location = new System.Drawing.Point(3, 20);
            this.lstDokumentyMR.Name = "lstDokumentyMR";
            this.lstDokumentyMR.Size = new System.Drawing.Size(234, 115);
            this.lstDokumentyMR.TabIndex = 1;
            this.lstDokumentyMR.View = System.Windows.Forms.View.Details;
            this.lstDokumentyMR.SelectedIndexChanged += new System.EventHandler(this.OnZaznaczonyDokumentMRChanged);
            // 
            // chLp
            // 
            this.chLp.Text = "Lp";
            this.chLp.Width = 25;
            // 
            // chDokument
            // 
            this.chDokument.Text = "Dokument";
            this.chDokument.Width = 95;
            // 
            // chStatus
            // 
            this.chStatus.Text = "S";
            this.chStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.chStatus.Width = 30;
            // 
            // chDostepnosc
            // 
            this.chDostepnosc.Text = "Dost.";
            this.chDostepnosc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.chDostepnosc.Width = 50;
            // 
            // chPriorytet
            // 
            this.chPriorytet.Text = "P";
            this.chPriorytet.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.chPriorytet.Width = 30;
            // 
            // naglowekBindingSource
            // 
            this.naglowekBindingSource.DataSource = typeof(WydanieMRModule.DataModel.Naglowek);
            // 
            // tbData
            // 
            this.tbData.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.naglowekBindingSource, "DataDokumentu", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "D"));
            this.tbData.Location = new System.Drawing.Point(54, 139);
            this.tbData.Name = "tbData";
            this.tbData.ReadOnly = true;
            this.tbData.Size = new System.Drawing.Size(183, 21);
            this.tbData.TabIndex = 2;
            this.tbData.TabStop = false;
            // 
            // lbData
            // 
            this.lbData.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbData.Location = new System.Drawing.Point(3, 142);
            this.lbData.Name = "lbData";
            this.lbData.Size = new System.Drawing.Size(49, 21);
            this.lbData.Text = "Data:";
            // 
            // tbSymbolOdbiorcy
            // 
            this.tbSymbolOdbiorcy.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.naglowekBindingSource, "SymbolOdbiorcy", true));
            this.tbSymbolOdbiorcy.Location = new System.Drawing.Point(54, 166);
            this.tbSymbolOdbiorcy.Name = "tbSymbolOdbiorcy";
            this.tbSymbolOdbiorcy.ReadOnly = true;
            this.tbSymbolOdbiorcy.Size = new System.Drawing.Size(183, 21);
            this.tbSymbolOdbiorcy.TabIndex = 3;
            this.tbSymbolOdbiorcy.TabStop = false;
            // 
            // lbSymbolOdbiorcy
            // 
            this.lbSymbolOdbiorcy.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbolOdbiorcy.Location = new System.Drawing.Point(2, 158);
            this.lbSymbolOdbiorcy.Name = "lbSymbolOdbiorcy";
            this.lbSymbolOdbiorcy.Size = new System.Drawing.Size(50, 32);
            this.lbSymbolOdbiorcy.Text = "Symbol odbiorcy:";
            // 
            // tbNazwaOdbiorcy
            // 
            this.tbNazwaOdbiorcy.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.naglowekBindingSource, "NazwaOdbiorcy", true));
            this.tbNazwaOdbiorcy.Location = new System.Drawing.Point(53, 193);
            this.tbNazwaOdbiorcy.Name = "tbNazwaOdbiorcy";
            this.tbNazwaOdbiorcy.ReadOnly = true;
            this.tbNazwaOdbiorcy.Size = new System.Drawing.Size(183, 21);
            this.tbNazwaOdbiorcy.TabIndex = 4;
            this.tbNazwaOdbiorcy.TabStop = false;
            // 
            // lbNazwaOdbiorcy
            // 
            this.lbNazwaOdbiorcy.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbNazwaOdbiorcy.Location = new System.Drawing.Point(1, 185);
            this.lbNazwaOdbiorcy.Name = "lbNazwaOdbiorcy";
            this.lbNazwaOdbiorcy.Size = new System.Drawing.Size(51, 32);
            this.lbNazwaOdbiorcy.Text = "Nazwa odbiorcy:";
            // 
            // tbIloscLP
            // 
            this.tbIloscLP.Location = new System.Drawing.Point(54, 248);
            this.tbIloscLP.Name = "tbIloscLP";
            this.tbIloscLP.ReadOnly = true;
            this.tbIloscLP.Size = new System.Drawing.Size(183, 21);
            this.tbIloscLP.TabIndex = 6;
            this.tbIloscLP.TabStop = false;
            // 
            // lbIloscLP
            // 
            this.lbIloscLP.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbIloscLP.Location = new System.Drawing.Point(2, 250);
            this.lbIloscLP.Name = "lbIloscLP";
            this.lbIloscLP.Size = new System.Drawing.Size(55, 21);
            this.lbIloscLP.Text = "Ilo�� LP:";
            // 
            // btnPobierz
            // 
            this.btnPobierz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPobierz.Location = new System.Drawing.Point(151, 4);
            this.btnPobierz.Name = "btnPobierz";
            this.btnPobierz.Size = new System.Drawing.Size(85, 20);
            this.btnPobierz.TabIndex = 200;
            this.btnPobierz.Text = "&Ret Pobierz";
            this.btnPobierz.Click += new System.EventHandler(this.OnPobierz);
            // 
            // btnSzukaj
            // 
            this.btnSzukaj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnSzukaj.Location = new System.Drawing.Point(62, 4);
            this.btnSzukaj.Name = "btnSzukaj";
            this.btnSzukaj.Size = new System.Drawing.Size(85, 20);
            this.btnSzukaj.TabIndex = 100;
            this.btnSzukaj.Text = "&1 Szukaj";
            this.btnSzukaj.Click += new System.EventHandler(this.OnSzukaj);
            // 
            // lrcNavigation
            // 
            this.lrcNavigation.BackColor = System.Drawing.SystemColors.Desktop;
            this.lrcNavigation.Location = new System.Drawing.Point(208, 0);
            this.lrcNavigation.Name = "lrcNavigation";
            this.lrcNavigation.NextEnabled = true;
            this.lrcNavigation.PreviousEnabled = true;
            this.lrcNavigation.Size = new System.Drawing.Size(32, 16);
            this.lrcNavigation.TabIndex = 5;
            this.lrcNavigation.TabStop = false;
            this.lrcNavigation.Next += new System.EventHandler(this.OnNastepny);
            this.lrcNavigation.Previous += new System.EventHandler(this.OnPoprzedni);
            // 
            // tbPoleWydania
            // 
            this.tbPoleWydania.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.naglowekBindingSource, "PoleWydania", true));
            this.tbPoleWydania.Location = new System.Drawing.Point(53, 220);
            this.tbPoleWydania.Name = "tbPoleWydania";
            this.tbPoleWydania.ReadOnly = true;
            this.tbPoleWydania.Size = new System.Drawing.Size(183, 21);
            this.tbPoleWydania.TabIndex = 5;
            this.tbPoleWydania.TabStop = false;
            this.tbPoleWydania.Text = " ";
            // 
            // lbPoleWydania
            // 
            this.lbPoleWydania.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbPoleWydania.Location = new System.Drawing.Point(2, 217);
            this.lbPoleWydania.Name = "lbPoleWydania";
            this.lbPoleWydania.Size = new System.Drawing.Size(51, 32);
            this.lbPoleWydania.Text = "Pole wydania:";
            // 
            // ListaMRView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lbPoleWydania);
            this.Controls.Add(this.tbPoleWydania);
            this.Controls.Add(this.lrcNavigation);
            this.Controls.Add(this.tbIloscLP);
            this.Controls.Add(this.lbIloscLP);
            this.Controls.Add(this.tbNazwaOdbiorcy);
            this.Controls.Add(this.lbNazwaOdbiorcy);
            this.Controls.Add(this.tbSymbolOdbiorcy);
            this.Controls.Add(this.lbSymbolOdbiorcy);
            this.Controls.Add(this.tbData);
            this.Controls.Add(this.lbData);
            this.Controls.Add(this.lstDokumentyMR);
            this.Controls.Add(this.lbTytul);
            this.Name = "ListaMRView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lstDokumentyMR, 0);
            this.Controls.SetChildIndex(this.lbData, 0);
            this.Controls.SetChildIndex(this.tbData, 0);
            this.Controls.SetChildIndex(this.lbSymbolOdbiorcy, 0);
            this.Controls.SetChildIndex(this.tbSymbolOdbiorcy, 0);
            this.Controls.SetChildIndex(this.lbNazwaOdbiorcy, 0);
            this.Controls.SetChildIndex(this.tbNazwaOdbiorcy, 0);
            this.Controls.SetChildIndex(this.lbIloscLP, 0);
            this.Controls.SetChildIndex(this.tbIloscLP, 0);
            this.Controls.SetChildIndex(this.lrcNavigation, 0);
            this.Controls.SetChildIndex(this.tbPoleWydania, 0);
            this.Controls.SetChildIndex(this.lbPoleWydania, 0);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.pnlNavigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.naglowekBindingSource)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private MSMLabel lbTytul;
		private MSMListView lstDokumentyMR;
		private System.Windows.Forms.ColumnHeader chLp;
		private System.Windows.Forms.ColumnHeader chDokument;
		private System.Windows.Forms.ColumnHeader chStatus;
		private System.Windows.Forms.ColumnHeader chDostepnosc;
		private System.Windows.Forms.TextBox tbData;
		private System.Windows.Forms.Label lbData;
		private System.Windows.Forms.TextBox tbSymbolOdbiorcy;
		private System.Windows.Forms.Label lbSymbolOdbiorcy;
		private System.Windows.Forms.TextBox tbNazwaOdbiorcy;
		private System.Windows.Forms.Label lbNazwaOdbiorcy;
		private System.Windows.Forms.TextBox tbIloscLP;
		private System.Windows.Forms.Label lbIloscLP;
		private System.Windows.Forms.Button btnPobierz;
		private System.Windows.Forms.Button btnSzukaj;
		private System.Windows.Forms.ColumnHeader chPriorytet;
		private LeftRightControl lrcNavigation;
		private System.Windows.Forms.BindingSource naglowekBindingSource;
        private System.Windows.Forms.TextBox tbPoleWydania;
        private System.Windows.Forms.Label lbPoleWydania;
	}
}
