#region

using System;
using System.Windows.Forms;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;
using WydanieMRModule.DataModel;

#endregion

namespace WydanieMRModule.ListaMR
{
    [SmartPart]
    public partial class ListaMRView : ViewBase, IListaMRView
    {
        #region Private Fields

        private readonly Naglowek m_Empty = new Naglowek();

        #endregion

        #region IListaMRView Members

        public event EventHandler Szukaj;

        public event EventHandler Pobierz;

        public event EventHandler Nastepny;

        public event EventHandler Poprzedni;

        public event EventHandler ZaznaczonyDokumentMRChanged;

        public Naglowek DataSource
        {
            set { naglowekBindingSource.DataSource = value ?? m_Empty; }
        }

        public long? IloscLp
        {
            set
            {
                if (value != null)
                {
                    tbIloscLP.Text = value.ToString();
                }
                else
                {
                    tbIloscLP.Text = "?";
                }
            }
        }

        public bool PoprzedniEnabled
        {
            get { return lrcNavigation.PreviousEnabled; }
            set { lrcNavigation.PreviousEnabled = value; }
        }

        public bool NastepnyEnabled
        {
            get { return lrcNavigation.NextEnabled; }
            set { lrcNavigation.NextEnabled = value; }
        }

        public ListView.ListViewItemCollection DokumentyMRDataSource
        {
            get { return lstDokumentyMR.Items; }
        }

        public Naglowek ZaznaczonyDokumentMR
        {
            get
            {
                if (lstDokumentyMR.SelectedIndices.Count == 0)
                {
                    return null;
                }

                return lstDokumentyMR.Items[lstDokumentyMR.SelectedIndices[0]].Tag as Naglowek;
            }
        }

        public void AsynchUstawIlosc(long? ilosc)
        {
            if (InvokeRequired)
            {
                Invoke(new InvokeAsynchUstawWartosc(AsynchUstawIlosc), ilosc);
                return;
            }
            tbIloscLP.Text = ilosc.ToString();
        }

        public void UstawAktywnaListe()
        {
            SelectNextControl(tbData, false, true, true, true);
        }

        #endregion

        #region Nested type: InvokeAsynchUstawWartosc

        private delegate void InvokeAsynchUstawWartosc(long? ilosc);

        #endregion

        #region Constructors

        public ListaMRView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #endregion

        #region Protected Methods

        protected void OnSzukaj(object sender, EventArgs e)
        {
            if (Szukaj != null)
            {
                Szukaj(sender, e);
            }
        }

        protected void OnPobierz(object sender, EventArgs e)
        {
            if (Pobierz != null)
            {
                Pobierz(sender, e);
            }
        }

        protected void OnPoprzedni(object sender, EventArgs e)
        {
            if (Poprzedni != null)
            {
                Poprzedni(sender, e);
            }
        }

        protected void OnNastepny(object sender, EventArgs e)
        {
            if (Nastepny != null)
            {
                Nastepny(sender, e);
            }
        }

        protected void OnZaznaczonyDokumentMRChanged(object sender, EventArgs e)
        {
            if (ZaznaczonyDokumentMRChanged != null)
            {
                ZaznaczonyDokumentMRChanged(sender, e);
            }
        }

        #endregion
    }
}