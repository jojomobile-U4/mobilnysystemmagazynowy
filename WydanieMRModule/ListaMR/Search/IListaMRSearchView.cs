#region

using Common.SearchForm;

#endregion

namespace WydanieMRModule.ListaMR.Search
{
    public interface IListaMRSearchView : ISearchForm
    {
        KryteriaZapytaniaListyMR Kryteria { get; set; }
    }
}