#region

using Common.SearchForm;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace WydanieMRModule.ListaMR.Search
{
    [SmartPart]
    public partial class ListaMRSearchView : SearchForm, IListaMRSearchView
    {
        #region Constructors

        public ListaMRSearchView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #endregion

        #region IListaMRSearchView Members

        public KryteriaZapytaniaListyMR Kryteria
        {
            get
            {
                return new KryteriaZapytaniaListyMR(
                    string.Empty,
                    tbDokument.Text,
                    tbStatus.Text,
                    tbDostepnosc.Text,
                    tbPriorytet.Text,
                    tbData.Value,
                    tbSymbolOdbiorcy.Text,
                    null);
            }
            set
            {
                if (value != null)
                {
                    tbDokument.Text = value.Dokument;
                    tbStatus.Text = value.Status;
                    tbDostepnosc.Text = value.Dostepnosc;
                    tbPriorytet.Text = value.Priorytet;
                    tbData.Value = value.Data;
                    tbSymbolOdbiorcy.Text = value.SymbolOdbiorcy;
                }
                else
                {
                    tbDokument.Text =
                        tbStatus.Text =
                        tbDostepnosc.Text =
                        tbPriorytet.Text =
                        tbData.Text =
                        tbSymbolOdbiorcy.Text = string.Empty;
                }
            }
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);

            tbData.ReadOnly = navigationState;
            tbDokument.ReadOnly = navigationState;
            tbDostepnosc.ReadOnly = navigationState;
            tbPriorytet.ReadOnly = navigationState;
            tbStatus.ReadOnly = navigationState;
            tbSymbolOdbiorcy.ReadOnly = navigationState;
        }

        #endregion
    }
}