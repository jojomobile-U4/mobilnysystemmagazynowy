using System;
using System.Collections.Generic;
using System.Text;
using Common.DataModel;
using Common;

namespace WydanieMRModule.DataModel
{
	public class ListaDokumentow
	{
		#region Private Fields

		private List<Dokument> m_Lista;
		private StatusOperacji m_StatusOperacji;

		#endregion
		#region Properties

		public StatusOperacji StatusOperacji
		{
			get { return m_StatusOperacji; }
			set { m_StatusOperacji = value; }
		}

		public List<Dokument> Lista
		{
			get { return m_Lista; }
		}

		#endregion
		#region Constructors

		public ListaDokumentow()
		{
			m_Lista = new List<Dokument>();
			m_StatusOperacji = new StatusOperacji(StatusOperacji.SUCCESS);
		}

		public ListaDokumentow(WydanieMRProxy.ListaDokumentow listaDokumentow)
			: this()
		{
			if (listaDokumentow != null)
			{
				if (listaDokumentow.lista != null)
				{
					foreach (WydanieMRProxy.Dokument dokument in listaDokumentow.lista)
					{
						m_Lista.Add(new Dokument(dokument));
					}
				}
				m_StatusOperacji = new StatusOperacji(listaDokumentow.statusOperacji.status, listaDokumentow.statusOperacji.tekst);
			}
			else
			{
				m_StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, DataModelConstants.NO_DATA);
			}
		}

		#endregion
	}
}
