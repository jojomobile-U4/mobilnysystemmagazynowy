using Common.DataModel;

namespace WydanieMRModule.DataModel
{
	public class Pozycja
	{
		#region Private Fields

	    #endregion
		#region Properties

	    public bool PotwierdzonaLokalizacja { get; set; }

	    public string Zakonczona { get; set; }

	    public string SymbolTowaru { get; set; }

	    public string Sektor { get; set; }

	    public string NazwaTowaru { get; set; }

	    public int Lp { get; set; }

	    public string Lokalizacja { get; set; }

	    public string Jm { get; set; }

	    public float Ilosc { get; set; }

	    public long IdDokumentu { get; set; }

	    public long Id { get; set; }

	    public float IloscOryginalna { get; set; }

	    public float Waga { get; set; }
        public float WagaOryginalna { get; set; }
        public string SymbolDokumentu { get; set; }


		#endregion
		#region Constructors

		public Pozycja()
		{
			PotwierdzonaLokalizacja = false;
			Jm = string.Empty;
			Lokalizacja = string.Empty;
			Sektor = string.Empty;
			SymbolTowaru = string.Empty;
			Zakonczona = string.Empty;
		}

		public Pozycja(WydanieMRProxy.Pozycja pozycja)
			: this()
		{
		    if (pozycja == null) return;

		    Id = pozycja.id;
		    IdDokumentu = pozycja.idDokumentu;
		    Ilosc = pozycja.ilosc;
		    Jm = pozycja.jm;
		    Lokalizacja = Localization.RemovePrefix(pozycja.lokalizacja);
		    Lp = pozycja.lp;
		    NazwaTowaru = pozycja.nazwaTowaru;
		    Sektor = pozycja.sektor;
		    SymbolTowaru = pozycja.symbolTowaru;
		    Zakonczona = pozycja.zakonczona;
		    IloscOryginalna = pozycja.ilosc;
		    Waga = pozycja.waga;
		    SymbolDokumentu = pozycja.symbolDokumentu;
		}

		#endregion
	}
}
