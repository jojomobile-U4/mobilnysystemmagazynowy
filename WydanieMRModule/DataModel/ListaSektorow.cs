using System;
using System.Collections.Generic;
using System.Text;
using Common.DataModel;
using Common;

namespace WydanieMRModule.DataModel
{
	public class ListaSektorow
	{
		#region Private Fields

		private List<Sektor> m_Lista;
		private StatusOperacji m_StatusOperacji;

		#endregion
		#region Properties

		public StatusOperacji StatusOperacji
		{
			get { return m_StatusOperacji; }
			set { m_StatusOperacji = value; }
		}

		public List<Sektor> Lista
		{
			get { return m_Lista; }
		}

		#endregion
		#region Constructors

		public ListaSektorow()
		{
			m_Lista = new List<Sektor>();
			m_StatusOperacji = new StatusOperacji(StatusOperacji.SUCCESS);
		}

		public ListaSektorow(WydanieMRProxy.ListaSektorow listaSektorow)
			: this()
		{
			if (listaSektorow != null)
			{
				if (listaSektorow.lista != null)
				{
					foreach (WydanieMRProxy.Sektor sektor in listaSektorow.lista)
					{
						m_Lista.Add(new Sektor(sektor));
					}
				}
				m_StatusOperacji = new StatusOperacji(listaSektorow.statusOperacji.status, listaSektorow.statusOperacji.tekst);
			}
			else
			{
				m_StatusOperacji = new StatusOperacji(StatusOperacji.ERROR, DataModelConstants.NO_DATA);
			}
		}

		#endregion
		#region Methods

		public bool Contains(string kodSektoru)
		{
			foreach (Sektor sektor in m_Lista)
			{
				if (string.Equals(sektor.Kod,kodSektoru))
				{
					return true;
				}
			}

			return false;
		}

		#endregion
	}
}
