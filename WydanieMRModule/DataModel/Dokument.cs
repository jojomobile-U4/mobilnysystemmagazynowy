using System;
using System.Collections.Generic;
using System.Text;

namespace WydanieMRModule.DataModel
{
	public class Dokument
	{
		#region Private Fields

		private Naglowek m_Naglowek;
		private List<Pozycja> m_Pozycje;

		#endregion
		#region Properties

		public List<Pozycja> Pozycje
		{
			get { return m_Pozycje; }
		}

		public Naglowek Naglowek
		{
			get { return m_Naglowek; }
			set { m_Naglowek = value; }
		}

		#endregion
		#region Constructors

		public Dokument()
		{
			m_Naglowek = new Naglowek();
			m_Pozycje = new List<Pozycja>();
		}

		public Dokument(WydanieMRProxy.Dokument dokument)
			: this()
		{
			if (dokument != null)
			{
				m_Naglowek = new Naglowek(dokument.naglowek);
				if (dokument.pozycje != null)
				{
					for (int i = 0; i < dokument.pozycje.Length; i++)
					{
						m_Pozycje.Add(new Pozycja(dokument.pozycje[i]));
					}
				}
			}
		}

		#endregion
	}
}
