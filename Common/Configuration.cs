namespace Common
{
    public class Configuration
    {
        public WebServiceConfiguration AdministracjaWS;
        public WebServiceConfiguration DostawaWS;
        public WebServiceConfiguration InwentaryzacjaWS;
        public WebServiceConfiguration KartotekaIndeksuWS;
        public WebServiceConfiguration KartotekaMiejscaWS;
        public WebServiceConfiguration KodyKreskoweWS;
        public string PrefiksLokalizacji;
        public WebServiceConfiguration PrzesuniecieWS;
        public WebServiceConfiguration WagiWS;
        public WebServiceConfiguration WydanieMRWS;
        public WebServiceConfiguration WydanieSWAWS;

        public static Configuration StandardConfiguration
        {
            get
            {
                var cfg = new Configuration
                              {
                                  PrefiksLokalizacji = null,
                                  InwentaryzacjaWS = new WebServiceConfiguration {Url = "10.7.6.197:8080/axis2/services/WsInwentaryzacja?wsdl", WielkoscStrony = 10},
                                  DostawaWS = new WebServiceConfiguration {Url = "10.7.6.197:8080/axis2/services/WsDostawa?wsdl", WielkoscStrony = 10},
                                  KartotekaIndeksuWS = new WebServiceConfiguration { Url = "10.7.6.197:8080/axis2/services/WsKartotekaIndeksu?wsdl", WielkoscStrony = 10 },
                                  KodyKreskoweWS = new WebServiceConfiguration { Url = "10.7.6.197:8080/axis2/services/WsKartotekaKodowKreskowych?wsdl", WielkoscStrony = 10 },
                                  KartotekaMiejscaWS = new WebServiceConfiguration {Url = "10.7.6.197:8080/axis2/services/WsKartotekaMiejsca?wsdl", WielkoscStrony = 10},
                                  PrzesuniecieWS = new WebServiceConfiguration {Url = "10.7.6.197:8080/axis2/services/WsPrzesuniecie?wsdl", WielkoscStrony = 10},
                                  WydanieMRWS = new WebServiceConfiguration {Url = "10.7.6.197:8080/axis2/services/WsWydanieMR?wsdl", WielkoscStrony = 10},
                                  WydanieSWAWS = new WebServiceConfiguration {Url = "10.7.6.197:8080/axis2/services/WsWydanieSWA?wsdl", WielkoscStrony = 10},
                                  AdministracjaWS = new WebServiceConfiguration {Url = "10.7.6.197:8080/axis2/services/WsAdministracja?wsdl"},
                                  WagiWS = new WebServiceConfiguration {Url = "10.7.6.197:8080/axis2/services/WsWagi?wsdl"}
                              };

                return cfg;
            }
        }
    }

    public class WebServiceConfiguration
    {
        public string Url;
        public int WielkoscStrony;
    }
}