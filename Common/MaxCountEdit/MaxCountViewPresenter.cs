#region

using System;
using System.Windows.Forms;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.Utility;

#endregion

namespace Common.MaxCountEdit
{
    public class MaxCountViewPresenter : PresenterBase
    {
        #region Events

        public event EventHandler<DataEventArgs<float>> MaxCountValueChanged;
        public event EventHandler Cancel;

        #endregion

        #region Constants

        private const string ERROR = "B��d";
//		private const string BLAD_ILOSC_MAX_WIEKSZA_OD_ILOSC = "Ilo�� maksymalna nie mo�e by� mniejsza ni� ilo�� aktualnie znajduj�ca si� w lokalizacij.";
        private const string ERROR_FORMAT_EXCEPTION = "Nieprawid�owy format liczby.";
        private const string ERROR_MAX_COUNT_LOWER_ZERO = "Ilo�� maksymalna nie mo�e by� mniejsza od zera.";

        #endregion

        #region Constructors

        public MaxCountViewPresenter(IMaxCountView view) : base(view)
        {
        }

        #endregion

        #region Properties

        public IMaxCountView View
        {
            get { return m_view as IMaxCountView; }
        }

        #endregion

        #region Protected Methods

        protected void OnMaxCountValueChanged(object sender, DataEventArgs<float> e)
        {
            if (MaxCountValueChanged != null)
            {
                MaxCountValueChanged(sender, e);
            }
        }

        protected void OnCancel(object sender, EventArgs e)
        {
            if (Cancel != null)
            {
                Cancel(sender, e);
            }
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    e.Handled = true;
                    view_Ok(View, EventArgs.Empty);
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    view_Anuluj(View, EventArgs.Empty);
                    break;
                case Keys.Up:
                    e.Handled = true;
                    View.FocusNextControl(false);
                    break;
                case Keys.Down:
                    e.Handled = true;
                    View.FocusNextControl(true);
                    break;
            }
        }

        #endregion

        #region Private Methods

        protected override void AttachView()
        {
            View.Anuluj += view_Anuluj;
            View.IloscMaxChanged += view_IloscMaxChanged;
            View.Ok += view_Ok;
        }

        private void view_Ok(object sender, EventArgs e)
        {
            var nowaIloscMax = NowaIloscMax();
            if (nowaIloscMax != null &&
                PoprawneDane((float) nowaIloscMax))
            {
                //TODO: oglosic zdarzenie wymagajace zmiany lub zmienic na bazie...
                //TODO: zostawic lub pobierac nowe wartosci po zapisie do bazy
                OnMaxCountValueChanged(sender, new DataEventArgs<float>((float) nowaIloscMax));
                CloseView();
            }
        }

        private void view_IloscMaxChanged(object sender, EventArgs e)
        {
            View.OkEnabled = !View.MaxCountValue.Equals(string.Empty);
        }

        private void view_Anuluj(object sender, EventArgs e)
        {
            OnCancel(sender, e);
            CloseView();
        }

        private bool PoprawneDane(float nowaIloscMax)
        {
            //if (nowaIloscMax < miejsce.Ilosc)
            //{
            //    MessageBox.Show(BLAD_ILOSC_MAX_WIEKSZA_OD_ILOSC, ERROR);
            //    WorkItem.Activate();
            //    return false;
            //}
            if (nowaIloscMax < 0)
            {
                MessageBox.Show(ERROR_MAX_COUNT_LOWER_ZERO, ERROR);
                WorkItem.Activate();
                return false;
            }
            return true;
        }

        private float? NowaIloscMax()
        {
            try
            {
                return float.Parse(View.MaxCountValue);
            }
            catch (FormatException)
            {
                MessageBox.Show(ERROR_FORMAT_EXCEPTION, ERROR);
                WorkItem.Activate();
                return null;
            }
        }

        #endregion
    }
}