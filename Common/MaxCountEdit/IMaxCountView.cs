#region

using System;
using Common.Base;

#endregion

namespace Common.MaxCountEdit
{
    public interface IMaxCountView : IViewBase
    {
        string MaxCountValue { get; set; }

        string IndexSymbol { set; }

        string Localization { set; }

        bool OkEnabled { set; }
        event EventHandler Ok;
        event EventHandler Anuluj;
        event EventHandler IloscMaxChanged;
    }
}