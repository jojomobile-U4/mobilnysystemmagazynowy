
using Common.Components;

namespace Common.MaxCountEdit
{
	partial class MaxCountView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lbTytul = new Common.Components.MSMLabel();
			this.tbSymbol = new System.Windows.Forms.TextBox();
			this.lbSymbol = new System.Windows.Forms.Label();
			this.tbLokalizacja = new System.Windows.Forms.TextBox();
			this.lbLokalizacja = new System.Windows.Forms.Label();
			this.btnAnuluj = new System.Windows.Forms.Button();
			this.btnOk = new System.Windows.Forms.Button();
			this.tbIloscMaksymalna = new System.Windows.Forms.TextBox();
			this.lbIloscMaksymalna = new System.Windows.Forms.Label();
			this.pnlNavigation.SuspendLayout();
			this.SuspendLayout();
			// 
			// pnlNavigation
			// 
			this.pnlNavigation.Controls.Add(this.btnAnuluj);
			this.pnlNavigation.Controls.Add(this.btnOk);
			this.pnlNavigation.Location = new System.Drawing.Point(0, 272);
			this.pnlNavigation.Size = new System.Drawing.Size(240, 28);
			// 
			// lbTytul
			// 
			this.lbTytul.BackColor = System.Drawing.Color.Empty;
			this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
			this.lbTytul.CenterAlignX = false;
			this.lbTytul.CenterAlignY = true;
			this.lbTytul.ColorText = System.Drawing.Color.White;
			this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
			this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.lbTytul.ForeColor = System.Drawing.Color.Empty;
			this.lbTytul.Location = new System.Drawing.Point(0, 0);
			this.lbTytul.Name = "lbTytul";
			this.lbTytul.RectangleColor = System.Drawing.Color.Black;
			this.lbTytul.Size = new System.Drawing.Size(240, 20);
			this.lbTytul.TabIndex = 7;
			this.lbTytul.TabStop = false;
			this.lbTytul.TextDisplayed = "EDYCJA ILO�CI MAKSYMALNEJ";
			// 
			// tbSymbol
			// 
			this.tbSymbol.Location = new System.Drawing.Point(76, 25);
			this.tbSymbol.Name = "tbSymbol";
			this.tbSymbol.ReadOnly = true;
			this.tbSymbol.Size = new System.Drawing.Size(160, 21);
			this.tbSymbol.TabIndex = 3;
			this.tbSymbol.TabStop = false;
			// 
			// lbSymbol
			// 
			this.lbSymbol.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
			this.lbSymbol.Location = new System.Drawing.Point(2, 26);
			this.lbSymbol.Name = "lbSymbol";
			this.lbSymbol.Size = new System.Drawing.Size(70, 21);
			this.lbSymbol.Text = "Indeks:";
			// 
			// tbLokalizacja
			// 
			this.tbLokalizacja.Location = new System.Drawing.Point(76, 49);
			this.tbLokalizacja.Name = "tbLokalizacja";
			this.tbLokalizacja.ReadOnly = true;
			this.tbLokalizacja.Size = new System.Drawing.Size(160, 21);
			this.tbLokalizacja.TabIndex = 4;
			this.tbLokalizacja.TabStop = false;
			// 
			// lbLokalizacja
			// 
			this.lbLokalizacja.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
			this.lbLokalizacja.Location = new System.Drawing.Point(1, 49);
			this.lbLokalizacja.Name = "lbLokalizacja";
			this.lbLokalizacja.Size = new System.Drawing.Size(68, 21);
			this.lbLokalizacja.Text = "Lokalizacja:";
			// 
			// btnAnuluj
			// 
			this.btnAnuluj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.btnAnuluj.Location = new System.Drawing.Point(160, 5);
			this.btnAnuluj.Name = "btnAnuluj";
			this.btnAnuluj.Size = new System.Drawing.Size(77, 20);
			this.btnAnuluj.TabIndex = 2;
			this.btnAnuluj.Text = "&Esc Anuluj";
			this.btnAnuluj.Click += new System.EventHandler(this.OnAnuluj);
			// 
			// btnOk
			// 
			this.btnOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnOk.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.btnOk.Location = new System.Drawing.Point(88, 5);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(69, 20);
			this.btnOk.TabIndex = 1;
			this.btnOk.Text = "&Ret OK";
			this.btnOk.Click += new System.EventHandler(this.OnOk);
			// 
			// tbIloscMaksymalna
			// 
			this.tbIloscMaksymalna.Location = new System.Drawing.Point(76, 79);
			this.tbIloscMaksymalna.Name = "tbIloscMaksymalna";
			this.tbIloscMaksymalna.Size = new System.Drawing.Size(160, 21);
			this.tbIloscMaksymalna.TabIndex = 0;
			this.tbIloscMaksymalna.TextChanged += new System.EventHandler(this.OnIloscMaxChaneg);
			// 
			// lbIloscMaksymalna
			// 
			this.lbIloscMaksymalna.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
			this.lbIloscMaksymalna.Location = new System.Drawing.Point(2, 70);
			this.lbIloscMaksymalna.Name = "lbIloscMaksymalna";
			this.lbIloscMaksymalna.Size = new System.Drawing.Size(73, 32);
			this.lbIloscMaksymalna.Text = "Ilo�� maksymalna:";
			// 
			// MaxCountView
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
			this.Controls.Add(this.tbIloscMaksymalna);
			this.Controls.Add(this.lbIloscMaksymalna);
			this.Controls.Add(this.tbLokalizacja);
			this.Controls.Add(this.lbLokalizacja);
			this.Controls.Add(this.tbSymbol);
			this.Controls.Add(this.lbSymbol);
			this.Controls.Add(this.lbTytul);
			this.Name = "MaxCountView";
			this.Size = new System.Drawing.Size(240, 300);
			this.Controls.SetChildIndex(this.pnlNavigation, 0);
			this.Controls.SetChildIndex(this.lbTytul, 0);
			this.Controls.SetChildIndex(this.lbSymbol, 0);
			this.Controls.SetChildIndex(this.tbSymbol, 0);
			this.Controls.SetChildIndex(this.lbLokalizacja, 0);
			this.Controls.SetChildIndex(this.tbLokalizacja, 0);
			this.Controls.SetChildIndex(this.lbIloscMaksymalna, 0);
			this.Controls.SetChildIndex(this.tbIloscMaksymalna, 0);
			this.pnlNavigation.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private MSMLabel lbTytul;
		private System.Windows.Forms.TextBox tbSymbol;
		private System.Windows.Forms.Label lbSymbol;
		private System.Windows.Forms.TextBox tbLokalizacja;
		private System.Windows.Forms.Label lbLokalizacja;
		private System.Windows.Forms.Button btnAnuluj;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.TextBox tbIloscMaksymalna;
		private System.Windows.Forms.Label lbIloscMaksymalna;
	}
}
