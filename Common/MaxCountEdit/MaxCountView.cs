#region

using System;
using Common.Base;

#endregion

namespace Common.MaxCountEdit
{
    public partial class MaxCountView : ViewBase, IMaxCountView
    {
        #region Constructors

        public MaxCountView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #endregion

        #region Obsluga zdarzen

        protected void OnOk(object sender, EventArgs e)
        {
            if (Ok != null)
            {
                Ok(sender, e);
            }
        }

        protected void OnAnuluj(object sender, EventArgs e)
        {
            if (Anuluj != null)
            {
                Anuluj(sender, e);
            }
        }

        protected void OnIloscMaxChaneg(object sender, EventArgs e)
        {
            if (IloscMaxChanged != null)
            {
                IloscMaxChanged(sender, e);
            }
        }

        #endregion

        #region IMaxCountView Members

        public event EventHandler Ok;

        public event EventHandler Anuluj;

        public event EventHandler IloscMaxChanged;

        public string IndexSymbol
        {
            set { tbSymbol.Text = value; }
        }

        public string Localization
        {
            set { tbLokalizacja.Text = value; }
        }

        public string MaxCountValue
        {
            get { return tbIloscMaksymalna.Text; }
            set { tbIloscMaksymalna.Text = value; }
        }

        public bool OkEnabled
        {
            set { btnOk.Enabled = value; }
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);

            tbIloscMaksymalna.ReadOnly = navigationState;
            tbLokalizacja.ReadOnly = navigationState;
            tbSymbol.ReadOnly = navigationState;
        }

        #endregion
    }
}