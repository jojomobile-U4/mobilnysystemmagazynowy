#region

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using Common.Base;
using Common.WagiProxy;

using OpenNETCF.Web.Services2;

#endregion

namespace Common.WebServiceWrap
{
    public class WsWagiWrap : WsWagi, IWSSecurity
    {
        #region Private Fields

        #endregion

        #region Properties

        public SecurityHeader SecurityHeader { get; set; }

        #endregion

        #region Constructors

        public WsWagiWrap(WebServiceConfiguration configuration)
        {
            Url = configuration.Url;
        }

        #endregion

        #region Protected Methods

        protected override WebRequest GetWebRequest(Uri uri)
        {
            var request = (HttpWebRequest) base.GetWebRequest(uri);

            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;

            return request;
        }

        #endregion

        #region Methods

        [SoapDocumentMethod("urn:pobierzOdczytWagi", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzOdczytWagiResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzOdczytWagiResponse pobierzOdczytWagi([XmlElement("pobierzOdczytWagi", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzOdczytWagiWrap pobierzOdczytWagi1)
        {
            var results = Invoke("pobierzOdczytWagi", new object[] {pobierzOdczytWagi1});
            return ((pobierzOdczytWagiResponse) (results[0]));
        }

        #endregion
    }

    /// <remarks/>
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class pobierzOdczytWagiWrap : pobierzOdczytWagi
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]

        public string pNazwa
        {
            get { return base.pNazwa; }
            set { base.pNazwa = value; }
        }

    }
}