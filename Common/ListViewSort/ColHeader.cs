#region

using System.Windows.Forms;

#endregion

namespace Common.ListViewSort
{
    public class ColHeader : ColumnHeader
    {
        public bool ascending;

        public ColHeader(string text, int width, HorizontalAlignment align, bool asc)
        {
            this.Text = text;
            this.Width = width;
            this.TextAlign = align;
            this.ascending = asc;
        }

        public ColHeader(string text, bool asc)
        {
            this.Text = text;
            this.ascending = asc;
        }

        public ColHeader()
        {
            ascending = true;
        }
    }
}