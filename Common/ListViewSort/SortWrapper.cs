#region

using System.Collections;
using System.Windows.Forms;

#endregion

namespace Common.ListViewSort
{
    public class SortWrapper
    {
        public int sortColumn;
        public ListViewItem sortItem;


        // A SortWrapper requires the item and the index of the clicked column.
        public SortWrapper(ListViewItem Item, int iColumn)
        {
            sortItem = Item;
            sortColumn = iColumn;
        }

        // Text property for getting the text of an item.
        public string Text
        {
            get { return sortItem.SubItems[sortColumn].Text; }
        }

        // Implementation of the IComparer 
        // interface for sorting ArrayList items.

        #region Nested type: SortComparer

        public class SortComparer : IComparer
        {
            private readonly bool ascending;

            // Constructor requires the sort order;
            // true if ascending, otherwise descending.
            public SortComparer(bool asc)
            {
                this.ascending = asc;
            }

            // Implemnentation of the IComparer:Compare 
            // method for comparing two objects.

            #region IComparer Members

            public int Compare(object x, object y)
            {
                var xItem = (SortWrapper) x;
                var yItem = (SortWrapper) y;

                var xText = xItem.sortItem.SubItems[xItem.sortColumn].Text;
                var yText = yItem.sortItem.SubItems[yItem.sortColumn].Text;
                return xText.CompareTo(yText)*(this.ascending ? 1 : -1);
            }

            #endregion
        }

        #endregion
    }
}