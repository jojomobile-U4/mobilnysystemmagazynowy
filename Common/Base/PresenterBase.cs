#region

using System;
using System.Windows.Forms;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;

#endregion

namespace Common.Base
{
    public abstract class PresenterBase : Controller
    {
        #region Private Fields

        protected IViewBase m_view;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the current work item where the controller lives.
        /// </summary>
        public WorkItemBase WorkItemBase
        {
            get { return WorkItem as WorkItemBase; }
        }

        /// <summary>
        /// Gets the navigation key args assosiated with NavigationKeyPressed event.
        /// </summary>
        protected KeyEventArgs NavigationKeyArgs
        {
            get { return WorkItem.RootWorkItem.State[StateConstants.NAVIGATION_KEY] as KeyEventArgs; }
        }

        /// <summary>
        /// Gets the application's navigation state.
        /// </summary>
        protected bool NavigationState
        {
            get { return (bool) WorkItem.RootWorkItem.State[StateConstants.NAVIGATION_STATE]; }
        }

        #endregion

        #region Constructors

        public PresenterBase(IViewBase view)
        {
            m_view = view;
            AttachView();
        }

        protected void CloseView()
        {
            WorkItemBase.CloseView(m_view);
        }

        #endregion

        #region NavigationHandle

        /// <summary>
        /// Handles NavigationKeyPressed event if the current smart part is the view assosiated with the presenter.
        /// </summary>
        /// <param name="sender">Event sender object.</param>
        /// <param name="e">Event params.</param>
        [EventSubscription(EventBrokerConstants.NAVIGATION_KEY_PRESSED)]
        public virtual void NavigationKeyPressed(object sender, EventArgs e)
        {
            if (WorkItemBase.MainWorkspace.ActiveSmartPart == m_view &&
                !NavigationKeyArgs.Handled)
            {
                HandleNavigationKey(NavigationKeyArgs);
            }
        }

        /// <summary>
        /// Handles NavigationStatechanged event.
        /// </summary>
        /// <param name="sender">Event sender object.</param>
        /// <param name="e">Event params.</param>
        [EventSubscription(EventBrokerConstants.NAVIGATION_STATE_CHANGED)]
        public virtual void NavigationStateChanged(object sender, EventArgs e)
        {
            m_view.SetNavigationState(NavigationState);
        }

        protected abstract void HandleNavigationKey(KeyEventArgs e);

        protected abstract void AttachView();

        #endregion
    }
}