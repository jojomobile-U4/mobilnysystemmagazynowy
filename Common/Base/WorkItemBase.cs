#region

using System;
using System.Windows.Forms;
using Common.DbErrorHandling;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace Common.Base
{
    public class WorkItemBase : WorkItem
    {
        #region Constants

        protected const string BLAD = "B��d";
        protected const string NIEPRAWIDLOWY_FORMAT_ID_POZYCJI = "Odczytana warto�� identyfikatora pozycji nie jest poprawn� liczb�.";
        protected const string NIEPRAWIDLOWY_FORMAT_LICZBY = "Wprowadzona warto�� nie jest poprawn� liczb�.";

        #endregion

        public IWorkspace MainWorkspace
        {
            get
            {
                //TODO: Check if RootWorkItem is not null, and WorkSpaces contains work space with id 'mainWorkSpace'
                return RootWorkItem.Workspaces.Get("mainWorkSpace");
            }
        }

        public Configuration Configuration
        {
            get { return RootWorkItem.State[StateConstants.CONFIGURATION] as Configuration ?? Configuration.StandardConfiguration; }
        }

        public void CloseView(IViewBase view)
        {
            if (MainWorkspace.ActiveSmartPart == view)
            {
                MainWorkspace.Close(view);
            }
        }

        protected long? Konwertuj(string idPozycji)
        {
            try
            {
                return long.Parse(idPozycji);
            }
            catch (FormatException)
            {
                FunctionsHelper.SetCursorDefault();
                ShowMessageBox(NIEPRAWIDLOWY_FORMAT_ID_POZYCJI, BLAD);
                return null;
            }
        }

        protected long? KonwertujTextNaLongBezOstrzezenia(string tekst)
        {
            try
            {
                return long.Parse(tekst);
            }
            catch (FormatException)
            {
                return null;
            }
        }

        protected void ShowMessageBox(string text, string caption)
        {
            ShowMessageBox(text, caption, null);
        }

        protected void ShowMessageBox(string text, string caption, string stackTrace)
        {
            FunctionsHelper.SetCursorDefault();
            MessageBox.Show(text, caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);

            var error = new DbError();
            error.Tekst = text;
            error.StosWywolan = stackTrace;
            var dbeh = new DbErrorHandle();
            dbeh.WriteToFile(error);

            this.Activate();
        }
    }
}