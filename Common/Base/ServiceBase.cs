#region

using Microsoft.Practices.Mobile.CompositeUI;
using OpenNETCF.Web.Services2;

#endregion

namespace Common.Base
{
    public abstract class ServiceBase
    {
        #region Private Fields

        private readonly WorkItem m_WorkItem;

        #endregion

        #region Properties

        protected WorkItemBase MyWorkItem
        {
            get { return m_WorkItem as WorkItemBase; }
        }

        protected abstract WebServiceConfiguration Configuration { get; }

        #endregion

        #region Constructors

        public ServiceBase(WorkItem workItem)
        {
            m_WorkItem = workItem;
        }

        #endregion

        #region Methods

        protected void SecureSoapMessage(IWSSecurity serviceAgent)
        {
            serviceAgent.SecurityHeader = new SecurityHeader();
            var userName = MyWorkItem.State[StateConstants.USER] as string;
            var password = MyWorkItem.State[StateConstants.PASSWORD] as string;
            serviceAgent.SecurityHeader.UsernameToken =
                new UsernameToken(userName, password, PasswordOption.SendPlainText, EncodingType.Base64Binary);
        }

        #endregion
    }
}