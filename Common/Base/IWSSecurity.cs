#region

using OpenNETCF.Web.Services2;

#endregion

namespace Common.Base
{
    public interface IWSSecurity
    {
        SecurityHeader SecurityHeader { get; set; }
    }
}