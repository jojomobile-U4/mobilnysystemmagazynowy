#region

using System;
using System.Windows.Forms;

#endregion

namespace Common.Base
{
    public partial class ViewBase : UserControl, IViewBase
    {
        #region Private Fields

        private bool m_AllowTabStop;

        #endregion

        #region Constructors

        public ViewBase()
        {
            InitializeComponent();
            m_AllowTabStop = false;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the control with the focus.
        /// </summary>
        public Control FocusedControl { get; private set; }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Subscribes to the controls' GotFocus events from Controls collection.
        /// </summary>
        protected void InitializeFocusedControl()
        {
            foreach (Control control in Controls)
            {
                control.GotFocus -= ControlGotFocus;
                control.GotFocus += ControlGotFocus;
            }
        }

        #endregion

        #region Private Methods

        private void ControlGotFocus(object sender, EventArgs e)
        {
            var control = sender as Control;
            if (control != null)
                if (control.TabStop ||
                    m_AllowTabStop)
                {
                    FocusedControl = control;

                    if (control is TextBox)
                    {
                        (control as TextBox).SelectAll();
                    }
                }
        }

        #endregion

        #region IViewBase Members

        /// <summary>
        /// Sets the focus on the next control according to forward parameter.
        /// </summary>
        /// <param name="forward">Determines if select next or previous control.</param>
        /// <returns>true if succeded</returns>
        public bool FocusNextControl(bool forward)
        {
            return SelectNextControl(FocusedControl, forward, true, false, true);
        }

        /// <summary>
        /// Sets the controls enable and read only state.
        /// </summary>
        /// <param name="navigationState">true - if control shoud by in navigation state, no data edition available</param>
        public virtual void SetNavigationState(bool navigationState)
        {
            pnlNavigation.Enabled = navigationState;
            SelectNextControl(pnlNavigation, true, true, false, true);
        }

        /// <summary>
        /// Forces the databinding to refresh binded objects value.
        /// </summary>
        public void ForceDataBinding()
        {
            m_AllowTabStop = true;
            SelectNextControl(FocusedControl, true, false, false, true);
            if (FocusedControl is TextBox)
            {
                (FocusedControl as TextBox).SelectionLength = 0;
            }
            SelectNextControl(FocusedControl, false, false, false, true);
            m_AllowTabStop = false;
        }

        #endregion
    }
}