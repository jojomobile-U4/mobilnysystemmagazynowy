namespace Common.Base
{
    public interface IViewBase
    {
        bool FocusNextControl(bool forward);

        void SetNavigationState(bool navigationState);
        void ForceDataBinding();
    }
}