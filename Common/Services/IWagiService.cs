#region

using System;
using Common.WagiProxy;

#endregion

namespace Common
{
    public interface IWagiService
    {
        Masa pobierzOdczytWagi(String nazwa);
        void przeliczNaKg(ref Masa masa);
    }
}