#region

using System;
using Common.Base;
using Common.WagiProxy;
using Common.WebServiceWrap;
using Microsoft.Practices.Mobile.CompositeUI;

#endregion

namespace Common
{
    [Service(typeof (IWagiService))]
    public class WagiService : ServiceBase, IWagiService
    {
        public const string JednostkaMiaryKg = "KG";
        public const string JednostkaMiaryGramy = "g";

        public WagiService([ServiceDependency] WorkItem workItem)
            : base(workItem)
        {
        }

        protected override WebServiceConfiguration Configuration
        {
            get { return MyWorkItem.Configuration.WagiWS; }
        }

        #region IWagiService Members

        public Masa pobierzOdczytWagi(String nazwa)
        {
            var masa = new Masa();

            try
            {
                using (var serviceAgent = new WsWagiWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new pobierzOdczytWagiWrap {pNazwa = nazwa};
                    masa.wartosc = 10;
                    masa = serviceAgent.pobierzOdczytWagi(param).@return;
                }
            }
            catch (Exception ex)
            {
                masa.statusOperacji = new StatusOperacji {status = DataModel.StatusOperacji.ERROR, tekst = ex.Message};
                return masa;
            }

            return masa;
        }


        public void przeliczNaKg(ref Masa masa)
        {
            if (masa != null)
            {
                switch (masa.jednostka.ToLower())
                {
                    case JednostkaMiaryGramy:
                        masa.wartosc *= 1000;
                        break;
                }

                masa.jednostka = JednostkaMiaryKg;
            }
        }

        #endregion
    }
}