#region

using System;
using Symbol;
using Symbol.Barcode;

#endregion

namespace Common.BarCodeReader
{
    public class BarCodeReader
    {
        private string barCode;
        private DecoderTypes barCodeType;
        private EventHandler MyEventHandler;
        private Reader MyReader;
        private ReaderData MyReaderData;


        public string BarCode
        {
            get { return barCode; }
        }

        public DecoderTypes BarCodeType
        {
            get { return barCodeType; }
        }

        public event EventHandler OdczytanoDane;

        /// <summary>
        /// Initialize the reader.      
        /// </summary>
        private bool InitReader()
        {
            string a;
            try
            {
                // If reader is already present then fail initialize
                if (this.MyReader != null)
                {
                    return false;
                }

                this.MyReader = new Reader();
                
                // Create new reader, first available reader will be used.


                // Create reader data
                this.MyReaderData = new ReaderData(
                    ReaderDataTypes.Text,
                    ReaderDataLengths.MaximumLabel);

                // Create event handler delegate
                this.MyEventHandler = new EventHandler(MyReader_ReadNotify);

                // Enable reader, with wait cursor
                this.MyReader.Actions.Enable();

#if DEBUG
                // Disable beeper (for TESTS)
                this.MyReader.Parameters.Feedback.Success.BeepTime = 0;
#endif
            }
            catch (Exception e)
            {
                a = e.Message;
            }

            return true;
        }

        /// <summary>
        /// Stop reading and disable/close reader
        /// </summary>
        private void TermReader()
        {
            // If we have a reader
            if (this.MyReader != null)
            {
                // Disable the reader
                this.MyReader.Actions.Disable();

                // Free it up
                this.MyReader.Dispose();

                // Indicate we no longer have one
                this.MyReader = null;
            }

            // If we have a reader data
            if (this.MyReaderData != null)
            {
                // Free it up
                this.MyReaderData.Dispose();

                // Indicate we no longer have one
                this.MyReaderData = null;
            }
        }

        /// <summary>
        /// Start a read on the reader
        /// </summary>
        public void StartRead()
        {
            // If we have both a reader and a reader data
            if ((this.MyReader != null) &&
                (this.MyReaderData != null))
            {
                // Submit a read
                this.MyReader.ReadNotify += this.MyEventHandler;
                this.MyReader.Actions.Read(this.MyReaderData);
            }
        }

        /// <summary>
        /// Stop all reads on the reader
        /// </summary>
        public void StopRead()
        {
            // If we have a reader
            if (this.MyReader != null)
            {
                // Flush (Cancel all pending reads)
                this.MyReader.ReadNotify -= this.MyEventHandler;
                this.MyReader.Actions.Flush();
            }
        }

        /// <summary>
        /// Read complete or failure notification
        /// </summary>
        private void MyReader_ReadNotify(object sender, EventArgs e)
        {
            var TheReaderData = this.MyReader.GetNextReaderData();

            // If it is a successful read (as opposed to a failed one)
            if (TheReaderData.Result == Results.SUCCESS)
            {
                // Handle the data from this read
                this.HandleData(TheReaderData);

                // Start the next read
                this.StartRead();
            }
        }

        /// <summary>
        /// Handle data from the reader
        /// </summary>
        private void HandleData(ReaderData TheReaderData)
        {
            barCode = TheReaderData.Text;
            barCodeType = TheReaderData.Type;

            if (OdczytanoDane != null)
            {
                OdczytanoDane(this, null);
            }
        }

        public void Initialize()
        {
            // Initialize the Reader
            if (this.InitReader())
            {
                // Start a read on the reader
                this.StartRead();
            }
        }

        public void Terminate()
        {
            this.TermReader();
        }
    }
}