﻿#region

using System;
using System.Globalization;
using System.Windows.Forms;

#endregion

namespace Common
{
    public static class FunctionsHelper
    {
        public const string MFullStop = ".";
        public const string MComma = ",";

        #region Cursors function

        public static void SetCursorWait()
        {
            Cursor.Current = Cursors.WaitCursor;
            Cursor.Show();
            Application.DoEvents();
        }

        public static void SetCursorDefault()
        {
            Cursor.Current = Cursors.Default;
            Cursor.Show();
            Application.DoEvents();
        }

        #endregion

        #region Key function

        public static bool IsDigitFloat(char sing, string value)
        {
            if (value.IndexOf(".", StringComparison.Ordinal) > 0 && sing == 46)
                return false;

            return (char.IsDigit(sing) || sing == (char)Keys.Back || sing == 46);
        }

        #endregion

        #region Convert functions

        #region ConvertToDouble

        public static double ConvertToDouble(string value)
        {
            double returnValue = StateConstants.ConvertFailure;

            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    returnValue = double.Parse(value.Replace(MComma, MFullStop), NumberStyles.Float, CultureInfo.InvariantCulture);
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                MessageBox.Show(argumentNullException.Message, "ConvertToDouble", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            catch (ArgumentException argumentException)
            {
                MessageBox.Show(argumentException.Message, "ConvertToDouble", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            catch (FormatException formatException)
            {
                MessageBox.Show(formatException.Message, "ConvertToDouble", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            catch (OverflowException overflowException)
            {
                MessageBox.Show(overflowException.Message, "ConvertToDouble", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }

            return returnValue;
        }

        #endregion

        #region ConvertToFloat

        public static float ConvertToFloat(string value)
        {
            float returnValue = StateConstants.ConvertFailure;

            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    returnValue = float.Parse(value.Replace(MComma, MFullStop), NumberStyles.Float, CultureInfo.InvariantCulture);
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                MessageBox.Show(argumentNullException.Message, "ConvertToFloat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            catch (ArgumentException argumentException)
            {
                MessageBox.Show(argumentException.Message, "ConvertToFloat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            catch (FormatException formatException)
            {
                MessageBox.Show(formatException.Message, "ConvertToFloat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            catch (OverflowException overflowException)
            {
                MessageBox.Show(overflowException.Message, "ConvertToFloat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }

            return returnValue;
        }

        #endregion

        #region ConvertToFloatNullable

        public static float? ConvertToFloatNullable(string value)
        {
            float? returnValue = StateConstants.ConvertFailure;

            try
            {
                if (string.IsNullOrEmpty(value))
                {
                    returnValue = null;
                }
                else
                {
                    returnValue = float.Parse(value.Replace(MComma, MFullStop), NumberStyles.Float, CultureInfo.InvariantCulture);
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                MessageBox.Show(argumentNullException.Message, "ConvertToFloatNullable", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            catch (ArgumentException argumentException)
            {
                MessageBox.Show(argumentException.Message, "ConvertToFloatNullable", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            catch (FormatException formatException)
            {
                MessageBox.Show(formatException.Message, "ConvertToFloatNullable", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            catch (OverflowException overflowException)
            {
                MessageBox.Show(overflowException.Message, "ConvertToFloatNullable", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }

            return returnValue;
        }

        #endregion

        #region ConvertToLong

        public static long ConvertToLong(string value)
        {
            long returnValue = StateConstants.ConvertFailure;

            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    returnValue = long.Parse(value.Replace(MComma, MFullStop), NumberStyles.Number, CultureInfo.InvariantCulture);
                }
            }
            catch (ArgumentNullException argumentNullException)
            {
                MessageBox.Show(argumentNullException.Message, "ConvertToLong", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            catch (ArgumentException argumentException)
            {
                MessageBox.Show(argumentException.Message, "ConvertToLong", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            catch (FormatException formatException)
            {
                MessageBox.Show(formatException.Message, "ConvertToLong", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            catch (OverflowException overflowException)
            {
                MessageBox.Show(overflowException.Message, "ConvertToLong", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }

            return returnValue;
        }

        #endregion

        #endregion

        public static object ZnacznikZgodnosciKodowKreskowychStateHelper { get; set; }
        public static object ZnacznikZgodnosciKodowKreskowychSymbolIndeksu { get; set; }

    }
}