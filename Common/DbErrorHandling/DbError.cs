#region

using System;

#endregion

namespace Common.DbErrorHandling  
{
    public class DbError
    {
        private DateTime dataBledu = DateTime.Now;

        public DateTime DataBledu
        {
            get { return dataBledu; }
            set { dataBledu = value; }
        }

        public String Tekst { get; set; }

        public String StosWywolan { get; set; }
    }
}