#region

using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;

#endregion

namespace Common.DbErrorHandling
{
    public class DbErrorHandle
    {
        private readonly MemoryStream memStream = new MemoryStream();
        private String ERROR_LOG_FILE = "\\Program Files\\Mobilny System Magazynowania\\DbMessages.xml";
        private XmlTextReader reader;


        private void writeToXmlTextStream(XmlTextWriter writer, DbError dbError)
        {
            writer.WriteStartElement("blad");
            writer.WriteStartElement("data");
            if (dbError.DataBledu != null)
                writer.WriteValue(dbError.DataBledu);
            writer.WriteEndElement();
            writer.WriteStartElement("tekst");
            if (dbError.Tekst != null)
                writer.WriteValue(dbError.Tekst);
            writer.WriteEndElement();
            writer.WriteStartElement("stosWywolan");
            if (dbError.StosWywolan != null)
                writer.WriteValue(dbError.StosWywolan);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        public void WriteToFile(DbError dbError)
        {
            try
            {
                var fsRead = File.Open(ERROR_LOG_FILE, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                reader = new XmlTextReader(fsRead);
                reader.WhitespaceHandling = WhitespaceHandling.All;
                var memWriter = new XmlTextWriter(memStream, null);
                memWriter.Formatting = Formatting.Indented;
                memWriter.WriteStartDocument();

                String aktualnyElement = null;
                var tmpDbError = new DbError();

                if (fsRead.Length > 0)
                {
                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element:
                                switch (reader.Name)
                                {
                                    case "Bledy":
                                        memWriter.WriteStartElement("Bledy");
                                        break;
                                }
                                aktualnyElement = reader.Name;
                                break;
                            case XmlNodeType.EndElement:
                                switch (reader.Name)
                                {
                                    case "Bledy":
                                        writeToXmlTextStream(memWriter, dbError);
                                        break;
                                    case "blad":
                                        if ((dbError.DataBledu != null) && (dbError.DataBledu > (DateTime.Now.AddDays(-2))))
                                        {
                                            writeToXmlTextStream(memWriter, tmpDbError);
                                        }
                                        break;
                                }
                                break;
                            case XmlNodeType.Text:
                                switch (aktualnyElement)
                                {
                                    case "data":
                                        tmpDbError.DataBledu = reader.ReadContentAsDateTime();
                                        break;
                                    case "tekst":
                                        tmpDbError.Tekst = reader.ReadContentAsString();
                                        break;
                                    case "stosWywolan":
                                        tmpDbError.StosWywolan = reader.ReadContentAsString();
                                        break;
                                }
                                break;
                        }
                    }
                }
                else
                {
                    memWriter.WriteStartElement("Bledy");
                    writeToXmlTextStream(memWriter, dbError);
                    memWriter.WriteEndElement();
                }
                memWriter.WriteEndDocument();


                reader.Close();
                fsRead.Close();
                memWriter.Flush();
                memStream.Position = 0;

                File.Delete(ERROR_LOG_FILE);
                var fs = File.Open(ERROR_LOG_FILE, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                memStream.WriteTo(fs);
                memWriter.Close();
                memStream.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Zapis b��du do pliku");
            }
        }
    }
}