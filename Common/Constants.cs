namespace Common
{
    public static class Constants
    {
        //TODO: zamienic na "MC-"
        public const string DATE_FORMAT = "dd.MM.yyyy";
        public static string LOCALIZATION_PREFIX;

        public const string YES = "T";
        public const string NO = "N";
    }

    public static class UIExtensionConstants
    {
        public const string MAIN_MENU = "MainMenu";
    }

    /// <summary>
    /// Used for creating and handling commands.
    /// These are constants because they are used in attributes.
    /// </summary>
    public static class CommandConstants
    {
        public const string KodyKreskowe = "MainMenu/KodyKreskowe";
        public const string DOSTAWA = "MainMenu/Dostawa";
        public const string INWENTARYZACJA = "MainMenu/Inwentaryzacja";
        public const string KARTOTEKA_INDEKSU = "MainMenu/KartotekaIndeksu";
        public const string KARTOTEKA_MIEJSCA = "MainMenu/KartotekaMiejsca";
        public const string KONTROLA_MP = "MainMenu/Dostawa/KontrolaMP";
        public const string LISTA_MP = "MainMenu/Dostawa/ListaMP";
        public const string NOWE_PRZESUNIECIE = "MainMenu/Przesuniecia/NowePrzesuniecie";
        public const string PRZESUNIECIA = "MainMenu/Przesuniecia";
        public const string PRZESUNIECIA_LISTA_TOWAROW = "MainMenu/Przesuniecia/ListaTowarow";
        public const string PRZESUNIECIA_REALIZACJA = "MainMenu/Przesuniecia/RealizacjaMR";
        public const string REALIZACJA_MP = "MainMenu/Dostawa/RealizacjaMP";
        public const string REQUEST_EDIT_STATE_ACTIVATION = "ShowSearchForm";
        public const string WYDANIE_SWA = "MainMenu/WydanieSWA";
        public const string WYJDZ = "MainMenu/Wyjdz";
        public const string WYLOGUJ = "MainMenu/Wyloguj";
        public const string ZMIEN_PROFIL = "MainMenu/ZmienProfil";

        public const string WydanieMR = "MainMenu/WydanieMR";
        public const string WydanieMRRealizacja = "MainMenu/WydanieMR/WydanieMRRealizacja";
        public const string WydanieMRRealizacjaZbiorcza = "MainMenu/WydanieMR/WydanieMRRealizacjaZbiorcza";
        public const string WydanieMRPrzejecie = "MainMenu/WydanieMR/WydanieMRPrzejecie";
    }

    /// <summary>
    /// Used for handling State.
    /// These are constants because they are used in attributes.
    /// </summary>
    public static class StateConstants
    {
        public const string ASSORTIMENT_MOVE_RIGHTS = "UprawnienieDoPrzesuniecia";
        public const string ASYNC_IDENTYFIKATOR_ZADANIA_RESULT = "OperacjeAsynchroniczneIdentyfikatorZadania";
        public const string ASYNC_OP_ILOSC_REMAINING = "IloscPozostaleOperacjeAsynchroniczne";
        public const string ASYNC_OP_ILOSC_RESULT = "OperacjeAsynchroniczneIlosc";
        public const string ASYNC_OP_ILOSC_RESULTS = "IloscWykonaneOperacjeAsynchroniczne";
        public const string ASYNC_OP_STANY_REMAINING = "StanyPozostaleOperacjeAsynchroniczne";
        public const string ASYNC_OP_STANY_RESULTS = "StanyWykonaneOperacjeAsynchroniczne";
        public const string ASYNC_STAN_GORACY_RESULT = "OperacjeAsynchroniczneStanGoracy";
        public const string ASYNC_STAN_ILOSC_W_LOKALIZACJI_RESULT = "OperacjeAsynchroniczneIloscWLokalizacji";
        public const string BAR_CODE = "BarCode";
        public const string BAR_CODE_TYPE = "BarCodeType";

        public const int ConvertFailure = -1;// Int32.MinValue;
        public const string BoolValueNo = "N";
        public const string BoolValueYes = "T";
        public const string CONFIGURATION = "Konfiguracja";
        public const string CRITERIAS = "Kryteria";
        public const string DELIVERY_CONTROL = "KontrolaDostawy";
        public const string DELIVERY_REALIZATION = "RealizacjaDostawy";
        public const string DOCUMENT_MR_ID = "IdDokumentuMR";
        public const string KARTOTEKA_INDEKSU_JAKO_BROWS = "KartotekaIndeksuJakoBrows";
        public const string LAST_PAGE = "OstatnioLadowanaStrona";
        public const string LOCALIZATION = "Lokalizacja";
        public const string LOKALIZACJA_Z_KARTOTEKI_INDEKSU = "LokalizacjaZKartotekiIndeksu";
        public const string NAVIGATION_KEY = "NavigationKey";
        public const string NAVIGATION_STATE = "NavigationState";
        public const string NETWORK_STATUS = "NetworkStatus";
        public const string OPERATION_RESULT_STACKTRACE = "StosWywolan";
        public const string OPERATION_RESULT_STATUS = "StatusOperacji";
        public const string OPERATION_RESULT_TEXT = "TekstOperacji";
        public const string PASSWORD = "Has�o";
        public const string POSITION_MR_ID = "IdPozycjiDokumentuMR";
        public const string RANGE = "Zakres";
        public const string ROLE = "Rola";
        public const string SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU = "SymbolTowaruDlaKartotekiIndeksu";
        public const string USER = "LoggedOnUser";
        public const string USER_ROLES = "RoleUzytkownika";
        public const string ZONE = "Strefa";
        public const string SYMBOL_MIEJSCA = "SymbolMiejsca";

        public const string PrzejecieSkanIdLinii = "PrzejecieSkanIdLinii";
        public const string PrzejecieSkanRodzaj = "PrzejecieSkanRodzaj";

        public const string IDENTYFIKATOR_NOSNIKA = "IdentyfikatorNosnika";

        public const string KartotekaKodowKreskowychJakoBrows = "KartotekaKodowKreskowychJakoBrows";
        public const string KartotekaKodowKreskowychKontekstView = "KartotekaKodowKreskowychKontekstView";
        public const string SymbolIndeksuDlaKartotekiKodowKreskowych = "SymbolIndeksuDlaKartotekiKodowKreskowych";
        public const string ZnacznikDodaniaEanDlaKartotekiKodowKreskowych = "ZnacznikDodaniaEanDlaKartotekiKodowKreskowych";
        public const string ZnacznikZgodnosciKodowKreskowych = "ZnacznikZgodnosciKodowKreskowych";
        public const string ZnacznikZgodnosciKodowKreskowychSymbolIndeksu = "ZnacznikZgodnosciKodowKreskowychSymbolIndeksu";
        public const string WydanieMRModuleKontekstView = "WydanieMRModuleKontekstView";
        public const string ZrodloWywolaniaFormatkiK = "ZrodloWywolaniaFormatkiK";
        public const string ZrodloWywolaniaFormatkiEducjaMr = "ZrodloWywolaniaFormatkiEducjaMr";
        public const string AktywneCzytanieKodu = "AktywneCzytanieKodu";
        public const string BlokadaWywolaniaFormatkiK = "BlokadaWywolaniaFormatkiK";
    }

    public enum ContextViewConstants
    {
        EdycjaMrView,
        RealizacjaSWA,
        None
    }

    public enum ContextConstants
    {
        WydanieMRPrzejecie ,
        WydanieMRRealizacja,
        WydanieMRRealizacjaZbiorcza,
        KodyKreskoweModule,
        KartotekaIndeksuModule,
        DostawaModule,
        WydanieSWA,
        None
    }

    public enum ModuleSourceConstants
    {
        DOBI,
        INMA
    }

    public enum ModuleBooleanConstants
    {
        Yes,
        No
    }

    public enum ModuleSkanRodzajConstants
    {
        Lokalizacja,
        IdLinii
    }

    public static class WorkspacesConstants
    {
        public const string MAIN_WORKSPACE = "mainWorkSpace";
    }

    public static class EventBrokerConstants
    {
        public const string ASYNC_OPERATIONS_STARTED = "topic://Przesuniecie/RozpoczecieOperacjiAsynchronicznych";
        public const string ASYNC_OPERATOIN_FINISHED = "topic://Przesuniecie/ZakonczenieOperacjiAsynchronicznej";
        public const string BARCODE_HAS_BEEN_READ = "topic://MSMShell/BarCodeHasBeenRead";
        public const string DODANO_LUB_ZMIENIONO_POZYCJE_UMIESZCZENIA = "topic://Przesuniecie/RealizacjaMR/DodanoLubZmienionoPozycjeUmieszczenia";
        public const string NAVIGATION_KEY_PRESSED = "topic://MSMShell/NavigationKeyPressed";
        public const string NAVIGATION_STATE_CHANGED = "topic://MSMShell/NavigationStateChanged";
        public const string ODCZYTANO_ILOSC = "topic://WydanieMR/ListaMR/OdczytanoIlosc";
        public const string ODCZYTANO_ILOSC_W_LOK = "topic://Przesuniecie/OdczytanoIloscWLok";
        public const string ODCZYTANO_STAN_GORACY = "topic://Przesuniecie/OdczytanoStanGoracy";
        public const string USER_HAS_LOGGED_ON = "topic://MSMShell/UserHasLoggedOn";
        public const string WYBRANO_LOKALIZACJE = "topic://KartotekaIndeksu/WybranoLokalizacje";
        public const string WYSZUKAJ_DOKUMENTY_MR = "topic://WydanieMR/WyszukajDokumentyMR";
        public const string RealizacjaZbiorczaWyszukajMr = "topic://WydanieZbiorczeMR/WyszukajDokumentyMR";
        public const string WYSZUKAJ_DOKUMENTY_SWA = "topic://WydanieMR/WyszukajDokumentySWA";
        public const string WYSZUKAJ_INDEKSY = "topic://KartotekaIndeksu/WyszukajIndeksy";
        public const string WyszukajKodyKreskowe = "topic://KartotekaKodyKreskowe/WyszukajKodyKreskowe";
        public const string WYSZUKAJ_LISTE_DOKUMENTOW_MR = "topic://Przesuniecie/RealizacjaMR/WyszukajListeDokumentowMR";
        public const string WYSZUKAJ_LISTE_TOWAROW = "topic://Przesuniecie/ListaTowarow/WyszukajListeTowarow";
        public const string WYSZUKAJ_POZYCJE_DOKUMENTU_MR = "topic://WydanieMR/WyszukajPozycjeDokumentuMR";
        public const string ZATWIERDZONO_DOKUMENT_MR = "topic://WydanieMR/EdycjaMR/ZatwierdzonoDokument";
        public const string ZONE_HAS_BEEN_READ = "topic://WydanieMR/ZoneHasBennRead";
        public const string RealizacjaZbiorczaOdczytanoStrefe = "topic://WydanieZbiorczeMR/OdczytanoStrefe";
        public const string WYBRANO_NOSNIK = "topic://RealizacjaMP/WybranoNosnik";
        public const string WYJSCIE_Z_FORMATKI_K = "topic://KodyKreskowe/WyjscieZFormatki";

    }

    public static class WorkItemsConstants
    {
        public const string ADMINISTRATION_WORKITEM = "AdministrationWorkItem";
        public const string EDYCJA_MR_WORKITEM = "EdycjaMRWorkItem";
        public const string EdycjaZbiorczaMrWorkItem = "EdycjaZbiorczaMRWorkItem";
        public const string INDEKS_WORKITEM = "IndeksWorkItem";
        public const string KodyKreskoweWorkItem = "KodyKreskoweWorkItem";
        public const string INWENTARYZACJA_WORKITEM = "InwentaryzacjaWorkItem";
        public const string KONTROLA_MP_WORKITEM = "KontrolaMPWorkItem";
        public const string LISTA_MP_WORKITEM = "ListaMPWorkItem";
        public const string LISTA_MR_WORKITEM = "ListaMRWorkItem";
        public const string RealizacjaZbiorczaListaMrWorkItem = "RealizacjaZbiorczaListaMRWorkItem";
        public const string LISTA_SWA_WORKITEM = "ListaSWAWorkItem";
        public const string LISTA_TOWAROW_WORKITEM = "ListaTowarowWorkItem";
        public const string MIEJSCE_WORKITEM = "MiejsceWorkItem";
        public const string NOWE_PRZESUNIECIE_WORKITEM = "NowePrzesuniecieWorkItem";
        public const string REALIZACJA_MP_WORKITEM = "RealizacjaMPWorkItem";
        public const string REALIZACJA_MR_WORKITEM = "RealizacjaMRWorkItem";
        public const string REALIZACJA_SWA_WORKITEM = "RealizacjaSWAWorkItem";
        public const string UMIESZCZENIE_MR_WORKITEM = "UmieszczenieWorkItem";
        public const string WYDANIE_SWA_PRZEGLAD_MR_WORKITEM = "PrzegladMRWorkItem";
    }

    public static class ItemsConstants
    {
        public const string EDYCJA_MR_PRESENTER = "EdycjaMRViewPresenter";
        public const string EdycjaZborczaMrPresenter = "EdycjaZbiorczaMRViewPresenter";
        public const string EDYCJA_MR_SEARCH_PRESENTER = "EdycjaMRSearchViewPresenter";
        public const string EdycjaZbiorczaMrSearchPresenter = "EdycjaZbiorczaMRSearchViewPresenter";
        public const string INDEKS_MAX_COUNT_PRESENTER = "MaxCountViewPresenter";
        public const string KodyKreskowePresenter = "KodyKreskoweViewPresenter";
        public const string INDEKS_PRESENTER = "IndeksViewPresenter";
        public const string KodyKreskoweSearchPresenter = "KodyKreskoweSearchPresenter";
        public const string INDEKS_SEARCH_PRESENTER = "SearchIndeksViewPresenter";
        public const string INWENTARYAZCJA_PRESENTER = "InwentaryzacjaViewPresenter";
        public const string KONTROLA_MP_PRESENTER = "KontrolaMPViewPresenter";
        public const string KONTROLA_MP_SEARCH_PRESENTER = "KontrolaMPSearchViewPresenter";
        public const string LISTA_INWENTARYZACJI_PRESENTER = "ListaInwentaryzacjiViewPresenter";
        public const string LISTA_MP_PRESENTER = "ListaMPViewPresenter";
        public const string LISTA_MP_SEARCH_PRESENTER = "ListaMPSearchViewPresenter";
        public const string LISTA_MR_PRESENTER = "ListaMRViewPresenter";
        public const string RealizacjaZbiorczaListaMrPresenter = "RealizacjaZbiorczaListaMRViewPresenter";
        public const string LISTA_MR_SEARCH_PRESENTER = "SearchListaMRPresenter";
        public const string RealizacjaZbiorczaListaMrSearchPresenter = "RealizacjaZbiorczaSearchListaMRPresenter";
        public const string LISTA_MR_WYBOR_STREFY_PRESENTER = "WyborStrefyViewPresenter";
        public const string RealizacjaZbiorczaWyborStrefyPresenter = "RealizacjaZbiorczaWyborStrefyViewPresenter";
        public const string LISTA_SWA_KOD_KRESKOWY_MR_PRESENTER = "ListaSWAKodKreskowyMRViewPresenter";
        public const string LISTA_SWA_PRESENTER = "ListaSWAViewPresenter";
        public const string LISTA_SWA_SEARCH_PRESENTER = "SearchListaSWAPresenter";
        public const string LISTA_TOWAROW_PRESENTER = "ListaTowarowViewPresenter";
        public const string LISTA_TOWAROW_SEARCH_PRESENTER = "ListaTowarowSearchViewPresenter";
        public const string LOGIN_PRESENTER = "LoginViewPresenter";
        public const string MIEJSCE_PRESENTER = "MiejscePresenter";
        public const string NOWA_POZYCJA_INWENTARYZACJI_PRESENTER = "NowaPozycjaInwentaryzacjiViewPresenter";
        public const string NOWA_POZYCJA_MP_PRESENTER = "NowaPozycjaMPViewPresenter";
        public const string NOWE_PRZESUNIECIE_PRESENTER = "NowePrzesunieciePresenter";
        public const string PRZEPISZ_OPAKOWANIE_PRESENTER = "PrzepiszOpakowaniePresenter";
        public const string REALIZACJA_MP_PRESENTER = "RealizacjaMPViewPresenter";
        public const string REALIZACJA_MP_SEARCH_PRESENTER = "RealizacjaMPSearchViewPresenter";
        public const string REALIZACJA_MR_PRESENTER = "RealizacjaMRPresenter";
        public const string REALIZACJA_MR_SEARCH_PRESENTER = "RealizacjaMRSearchPresenter";
        public const string REALIZACJA_MR_LISTA_NOSNIKOW_PRESENTER = "RealizacjaMRListaNosnikowPresenter";
        public const string ROLE_SELECTION_PRESENTER = "RoleSelectionViewPresenter";
        public const string UMIESZCZENIE_MR_PRESENTER = "UmieszczenieSearchPresenter";
        public const string PRZESUNIECIE_LISTA_NOSNIKOW_PRESENTER = "PrzesuniecieListaNosnikowPresenter";
        public const string RealizacjaZbiorczaMrKontrahentaPresenter = "RealizacjaZbiorczaMRListaMRKontrahentaPresenter";
    }

    /// <summary>
    /// Tu powinny sie rowniez znalezc obslugiwane przez konkretna formatke skroty (KOD skrotu) powiazany z kastomizacja
    /// </summary>
    public static class NavigationConstants
    {
        //TODO: przy kastomizacji skrotow klawiszowych mnemoniki nie maja sensu => wyrzucic je po zaimplementowaniu
        //		klasy obslugujacej kastomizowane skroty
        public const string KodyKreskowe = "&0 Kody kreskowe";
        public const string DOSTAWA = "&1 Dostawa";
        public const string INWENTARYZACJA = "&7 Inwentaryzacja";
        public const string KARTOTEKA_INDEKSU = "&5 Indeksy";
        public const string KARTOTEKA_MIEJSCA = "&6 Miejsca";
        public const string LISTA_MP = "&1 Lista MP";
        public const string NOWE_PRZESUNIECIE = "&3 Nowe przesuni�cie";
        public const string PRZESUNIECIA = "&2 Przesuni�cia";
        public const string PRZESUNIECIA_LISTA_TOWAROW = "&1 Lista towar�w";
        public const string PRZESUNIECIA_REALIZACJA_MP = "&2 Realizacja MR";
        public const string REALIZACJA_MP = "&2 Realizacja MP";
        public const string WYDANIE_MR = "&3 Wydanie MR";
        public const string WydanieMRRealizacja = "&1 Realizacja MR";
        public const string WydanieMRRealizacjaZbiorcza = "&2 Realizacja zbiorcza";
        public const string WydanieMRPrzejecie = "&3 Przej�cie MR";
        public const string WYDANIE_SWA = "&4 Wydanie SWA";
        public const string WYLOGUJ = "&9 Wyloguj";
        public const string ZMIEN_PROFIL = "&8 Zmie� profil";
    }

    public static class DataModelConstants
    {
        public const string NO_DATA = "Brak danych.";
    }

    public static class FunctionalityConstants
    {
        //public const string MAGAZYNIER = "Magazynier";
        //public const string KONTROLER_DOSTAWY = "Kontroler dostawy";
        //public const string MAGAZYNIER_PRZYJMUJACY = "Magazynier przyjmujacy";
        //public const string MAGAZYNIER_WYDAJACY = "Magazynier wydajacy";
        //public const string KONTROLER_WYDANIA = "Kontroler wydania";
        //public const string INWENTARYZATOR = "Inwentaryzator";
        public const string DOSTAWA_KONTROLA_MP = "Kontrola MP";
        public const string DOSTAWA_LISTA_DOKUMENTOW_MP = "Lista dokumentow MP";
        public const string DOSTAWA_REALIZACJA_MP = "Realizacja MP";
        public const string INDEKSY = "Kartoteka indeksow";
        public const string INWENTARYZACJA_EDYCJA = "Edycja inwentaryzacji";
        public const string INWENTARYZACJA_LISTA = "Lista inwentaryzacji";
        public const string MIEJSCA = "Kartoteka miejsc";
        public const string OPAKOWANIA = "Opakowania";
        public const string PRZESUNIECIE_LISTA = "Lista towarow do przesuniecia";
        public const string PRZESUNIECIE_REALIZACJA_MR = "Realizacja MR";
        public const string WSZYSTKO = "Wszystko";
        public const string WYDANIE_MR_EDYCJA = "Edycja dokumentu MR pod wydanie";
        public const string WYDANIE_MR_LISTA = "Lista dokumentow MR pod wydanie";
        public const string WYDANIE_SWA_LISTA = "Lista dokumentow SWA";
        public const string WYDANIE_SWA_REALIZACJA = "Realizacja SWA";
        public const string KodKreskowe = "KodKreskowe";

    }

    public static class BoolConstants
    {
        public const string TAK = "T";
        public const string NIE = "N";
    }

    public static class SmartParts
    {
        public const string KodyKreskoweView = "KodyKreskoweModule.KodyKreskoweView";
    }
}
