#region

using System.Windows.Forms;

#endregion

namespace Common.Components
{
    public class MSMListView : ListView
    {
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            var key = e.KeyChar.ToString();
            if ((key.Equals("0"))
                || (key.Equals("1"))
                || (key.Equals("2"))
                || (key.Equals("3"))
                || (key.Equals("4"))
                || (key.Equals("5"))
                || (key.Equals("6"))
                || (key.Equals("7"))
                || (key.Equals("8"))
                || (key.Equals("9"))
                || (key.ToUpper().Equals("K"))
                || (key.ToUpper().Equals("I"))
                || (key.ToUpper().Equals("M"))
                )
            {
                e.Handled = true;
            }
            else
                base.OnKeyPress(e);
        }
    }
}