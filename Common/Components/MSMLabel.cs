#region

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

#endregion

namespace Common.Components
{
    public class MSMLabel : UserControl
    {
        #region Pola prywatne

        private Color m_BackgroundColor = Color.Gainsboro;
        private bool m_CenterAlignX;
        private bool m_CenterAlignY;
        private Color m_ColorText = Color.Black;
        private Color m_RectangleColor = Color.Gainsboro;
        private string m_Text;

        #endregion

        #region Konstruktor

        public MSMLabel()
        {
            TabStop = false;
            m_CenterAlignX = false;
            m_CenterAlignY = true;
        }

        #endregion

        #region Nowe w�asno�ci

        public bool CenterAlignX
        {
            get { return m_CenterAlignX; }
            set
            {
                m_CenterAlignX = value;
                Invalidate();
            }
        }

        public bool CenterAlignY
        {
            get { return m_CenterAlignY; }
            set
            {
                m_CenterAlignY = value;
                Invalidate();
            }
        }

        public override string Text
        {
            get { return TextDisplayed; }
            set
            {
                TextDisplayed = value;
                Invalidate();
            }
        }

        public string TextDisplayed
        {
            get { return m_Text; }
            set
            {
                m_Text = value;
                Invalidate();
            }
        }

        public Color RectangleColor
        {
            set
            {
                m_RectangleColor = value;
                Invalidate();
            }
            get { return m_RectangleColor; }
        }

        public Color ColorText
        {
            set
            {
                m_ColorText = value;
                Invalidate();
            }
            get { return m_ColorText; }
        }

        public Color BackGroundColor
        {
            set
            {
                m_BackgroundColor = value;
                Invalidate();
            }
            get { return m_BackgroundColor; }
        }

        #endregion

        #region Metody override

        protected override void OnMouseDown(MouseEventArgs e)
        {
            Invalidate();
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            Invalidate();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            var graphics = e.Graphics;

            using (var pen = new Pen(m_RectangleColor))
            {
                using (var brush = new SolidBrush(m_BackgroundColor))
                {
                    using (var textBrush = new SolidBrush(m_ColorText))
                    {
                        //Draw a rectangle and fill the inside
                        graphics.FillRectangle(brush, 0, 0, Width + 1, Height + 1);
                        graphics.DrawRectangle(pen, 0, 0, Width - 1, Height - 1);

                        //Create a font based on the default font
                        //            int fontHeight = 10;
                        //            Font font = new Font(FontFamily.GenericSerif,
                        //                                fontHeight, FontStyle.Bold);            
                        //Find out the size of the text
                        SizeF textSize;
                        textSize = e.Graphics.MeasureString(m_Text, Font);

                        //Work out how to position the text centrally
                        float x = 0, y = 0;
                        if (m_CenterAlignX && textSize.Width < Width)
                            x = (Width - textSize.Width)/2;

                        if (m_CenterAlignY && textSize.Height < Height)
                            y = (Height - textSize.Height)/2;

                        //Draw the text in the centre of the button using
                        // the default font
                        graphics.DrawString(m_Text, Font, textBrush, x, y);
                    }
                }
            }
        }

        #endregion

        #region W�asno�ci override

        [EditorBrowsable(EditorBrowsableState.Never)]
        public override Color BackColor
        {
            set { ; }
            get { return new Color(); }
        }


        [EditorBrowsable(EditorBrowsableState.Never)]
        public override Color ForeColor
        {
            set { ; }
            get { return new Color(); }
        }

        #endregion
    }
}