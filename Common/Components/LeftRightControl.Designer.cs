namespace Common.Components
{
	partial class LeftRightControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LeftRightControl));
			this.pbNext = new System.Windows.Forms.PictureBox();
			this.pbPrevious = new System.Windows.Forms.PictureBox();
			this.SuspendLayout();
			// 
			// pbNext
			// 
			this.pbNext.BackColor = System.Drawing.SystemColors.Desktop;
			this.pbNext.Image = ((System.Drawing.Image)(resources.GetObject("pbNext.Image")));
			this.pbNext.Location = new System.Drawing.Point(16, 0);
			this.pbNext.Name = "pbNext";
			this.pbNext.Size = new System.Drawing.Size(16, 16);
			// 
			// pbPrevious
			// 
			this.pbPrevious.BackColor = System.Drawing.SystemColors.Desktop;
			this.pbPrevious.Image = ((System.Drawing.Image)(resources.GetObject("pbPrevious.Image")));
			this.pbPrevious.Location = new System.Drawing.Point(0, 0);
			this.pbPrevious.Name = "pbPrevious";
			this.pbPrevious.Size = new System.Drawing.Size(16, 16);
			// 
			// LeftRightControl
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
			this.BackColor = System.Drawing.SystemColors.Desktop;
			this.Controls.Add(this.pbNext);
			this.Controls.Add(this.pbPrevious);
			this.Name = "LeftRightControl";
			this.Size = new System.Drawing.Size(32, 16);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.PictureBox pbNext;
		private System.Windows.Forms.PictureBox pbPrevious;
	}
}
