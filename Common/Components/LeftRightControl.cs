#region

using System;
using System.Windows.Forms;

#endregion

namespace Common.Components
{
    public partial class LeftRightControl : UserControl
    {
        #region Events

        public event EventHandler Next
        {
            add { pbNext.Click += value; }
            remove { pbNext.Click -= value; }
        }

        public event EventHandler Previous
        {
            add { pbPrevious.Click += value; }
            remove { pbPrevious.Click -= value; }
        }

        #endregion

        #region Properties

        public bool NextEnabled
        {
            get { return pbNext.Enabled; }
            set { pbNext.Enabled = value; }
        }

        public bool PreviousEnabled
        {
            get { return pbPrevious.Enabled; }
            set { pbPrevious.Enabled = value; }
        }

        #endregion

        #region Constructors

        public LeftRightControl()
        {
            InitializeComponent();
            this.TabStop = false;
        }

        #endregion

        //Blocks resizing
        protected override void OnResize(EventArgs e)
        {
            Width = 32;
            Height = 16;
            base.OnResize(e);
        }
    }
}