#region

using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

#endregion

namespace Common.Components
{
    public class MSMDate : TextBox
    {
        #region Constants

        private const int DAY_PART = 0;
        private const string DEFAULT_EXPRESSION_PATTERN = @"^\d{2}.\d{2}.\d{4}$";
        private const string DEFAULT_FORMAT = "dd.MM.yyyy";
        private const char DOT = '.';
        private const int MONTH_PART = 1;
        private const int YEAR_PART = 2;

        #endregion

        #region Private Fields

        private readonly Color m_Color;
        private readonly Regex m_ExpressionValidator;
        private readonly string m_Format;
        private DateTime? m_Value;

        #endregion

        #region Properties

        public DateTime? Value
        {
            get { return m_Value; }
            set
            {
                m_Value = value;
                if (m_Value.HasValue)
                {
                    base.Text = m_Value.Value.ToString(m_Format);
                }
                else
                {
                    base.Text = string.Empty;
                }
            }
        }

        //public string Format
        //{
        //    get { return m_Format; }
        //    set { m_Format = value; }
        //}

        #endregion

        #region Constructors

        public MSMDate()
        {
            m_Format = DEFAULT_FORMAT;
            m_ExpressionValidator = new Regex(DEFAULT_EXPRESSION_PATTERN);
            m_Color = ForeColor;
        }

        #endregion

        public override string Text
        {
            get { return base.Text; }
            set { SetValue(value); }
        }

        protected override void OnGotFocus(EventArgs e)
        {
            base.OnGotFocus(e);
            SelectAll();
        }

        protected override void OnTextChanged(EventArgs e)
        {
            base.OnTextChanged(e);
            SetValue(Text);
        }

        #region Private Methods

        private void SetValue(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                m_Value = null;
                base.Text = value;
                ForeColor = m_Color;
            }
            else if (m_ExpressionValidator.IsMatch(value))
            {
                try
                {
                    var parts = value.Split(DOT);
                    m_Value = new DateTime(int.Parse(parts[YEAR_PART]), int.Parse(parts[MONTH_PART]), int.Parse(parts[DAY_PART]));
                    ForeColor = m_Color;
                    base.Text = m_Value.Value.ToString(m_Format);
                }
                catch
                {
                    m_Value = null;
                    ForeColor = Color.Red;
                }
            }
            else
            {
                ForeColor = Color.Red;
            }
        }

        #endregion
    }
}