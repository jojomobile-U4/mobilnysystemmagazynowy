namespace Common.DataModel
{
    public static class DBBool
    {
        public const string FALSE = "N";
        public const string TRUE = "T";

        public static bool IsDBBool(string value)
        {
            return TRUE.Equals(value) || FALSE.Equals(value);
        }

        public static bool ToBool(string value)
        {
            return TRUE.Equals(value);
        }

        public static string ToDBBool(bool value)
        {
            return value ? TRUE : FALSE;
        }
    }
}