namespace Common.DataModel
{
    public class Localization
    {
        #region Static Methods

        public static string AddPrefix(string localization)
        {
            if (!string.IsNullOrEmpty(Constants.LOCALIZATION_PREFIX) &&
                !string.IsNullOrEmpty(localization) &&
                !localization.StartsWith(Constants.LOCALIZATION_PREFIX))
            {
                return Constants.LOCALIZATION_PREFIX + localization;
            }

            return localization;
        }

        public static string RemovePrefix(string localization)
        {
            if (!string.IsNullOrEmpty(Constants.LOCALIZATION_PREFIX) &&
                localization != null &&
                localization.StartsWith(Constants.LOCALIZATION_PREFIX))
            {
                return localization.Remove(0, Constants.LOCALIZATION_PREFIX.Length);
            }

            return localization;
        }

        #endregion
    }
}