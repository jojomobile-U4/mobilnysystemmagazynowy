namespace Common.DataModel
{
    public class StatusOperacji
    {
        #region Constants

        public const string ERROR = "E";
        public const string SUCCESS = "S";

        #endregion

        #region Private Fields

        #endregion

        #region Properties

        public string Tekst { get; set; }

        public string Status { get; set; }

        public string StosWywolan { get; set; }

        #endregion

        #region Constructors

        public StatusOperacji()
        {
        }

        public StatusOperacji(string status)
            : this()
        {
            Status = status ?? ERROR;
        }

        public StatusOperacji(string status, string tekst) : this(status)
        {
            Tekst = tekst ?? string.Empty;
        }

        public StatusOperacji(string status, string tekst, string stosWywolan)
            : this(status, tekst)
        {
            StosWywolan = stosWywolan ?? string.Empty;
        }

        #endregion
    }
}