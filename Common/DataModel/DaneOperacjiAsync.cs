#region

using System;

#endregion

namespace Common.DataModel
{
    public class DaneOperacjiAsync
    {
        #region Private Fields

        #endregion

        #region Properties

        public IAsyncResult AsyncResult { get; set; }

        public float ZwroconaWartosc { get; set; }

        public string Lokalizacja { get; set; }

        public long? NosnId { get; set; }

        public long? PainId { get; set; }

        #endregion

        #region Constructors

        public DaneOperacjiAsync(long? painId, string lokalizacja, long? nosnId)
        {
            PainId = painId;
            Lokalizacja = lokalizacja;
            NosnId = nosnId;
        }

        #endregion
    }
}