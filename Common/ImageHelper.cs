#region

using System.Drawing;

#endregion

namespace Common.Images
{
    public static class ImageHelper
    {
        public static Image MenuKodyKreskowe
        {
            get { return Menu.MenuKodyKreskowe; }
        }

        public static Image MenuDostawa
        {
            get { return Menu.MenuDostawa; }
        }

        public static Image MenuPrzesuniecie
        {
            get { return Menu.MenuPrzesuniecie; }
        }

        public static Image MenuWydanieMr
        {
            get { return Menu.MenuWydanieMr; }
        }

        public static Image MenuWydanieSwa
        {
            get { return Menu.MenuWydanieSwa; }
        }

        public static Image MenuIndeksy
        {
            get { return Menu.MenuIndeksy; }
        }

        public static Image MenuMiejsca
        {
            get { return Menu.MenuMiejsca; }
        }

        public static Image MenuWyloguj
        {
            get { return Menu.MenuWyloguj; }
        }

        public static Image MenuZmienProfil
        {
            get { return Menu.MenuZmienProfil; }
        }

        public static Image MenuInwentaryzacja
        {
            get { return Menu.MenuInwentaryzacja; }
        }

        public static Image Polaczenie
        {
            get { return Menu.Po��czenie; }
        }

        public static Image BrakPolaczenia
        {
            get { return Menu.BrakPo��czenia; }
        }

        public static Image TrybNawigacji
        {
            get { return Menu.TrybNawigacji; }
        }

        public static Image TrybEdycji
        {
            get { return Menu.TrybEdycji; }
        }

        public static Image PobieranieDanych
        {
            get { return Menu.PobieranieDanych; }
        }
    }
}