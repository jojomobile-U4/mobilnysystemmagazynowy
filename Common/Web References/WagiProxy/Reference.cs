﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3074
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.CompactFramework.Design.Data, Version 2.0.50727.3074.
// 
namespace Common.WagiProxy {
    using System.Diagnostics;
    using System.Web.Services;
    using System.ComponentModel;
    using System.Web.Services.Protocols;
    using System;
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="WsWagiSOAP11Binding", Namespace="http://mobilnySystemMagazynowy.teta.com")]
    public partial class WsWagi : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        /// <remarks/>
        public WsWagi() {
            this.Url = "http://SERVICES:8080/axis2/services/WsWagi";
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("urn:pobierzOdczytWagi", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("pobierzOdczytWagiResponse", Namespace="http://mobilnySystemMagazynowy.teta.com/xsd")]
        public pobierzOdczytWagiResponse pobierzOdczytWagi([System.Xml.Serialization.XmlElementAttribute("pobierzOdczytWagi", Namespace="http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzOdczytWagi pobierzOdczytWagi1) {
            object[] results = this.Invoke("pobierzOdczytWagi", new object[] {
                        pobierzOdczytWagi1});
            return ((pobierzOdczytWagiResponse)(results[0]));
        }
        
        /// <remarks/>
        public System.IAsyncResult BeginpobierzOdczytWagi(pobierzOdczytWagi pobierzOdczytWagi1, System.AsyncCallback callback, object asyncState) {
            return this.BeginInvoke("pobierzOdczytWagi", new object[] {
                        pobierzOdczytWagi1}, callback, asyncState);
        }
        
        /// <remarks/>
        public pobierzOdczytWagiResponse EndpobierzOdczytWagi(System.IAsyncResult asyncResult) {
            object[] results = this.EndInvoke(asyncResult);
            return ((pobierzOdczytWagiResponse)(results[0]));
        }
    }
    
    /// <remarks/>
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://mobilnySystemMagazynowy.teta.com/xsd")]
    public partial class pobierzOdczytWagi {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string pNazwa;
    }
    
    /// <remarks/>
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://dto.wagi.mobilnySystemMagazynowy.teta.com/xsd")]
    public partial class StatusOperacji {
        
        /// <remarks/>
        public int numer;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string status;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string stosWywolan;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string tekst;
    }
    
    /// <remarks/>
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://dto.wagi.mobilnySystemMagazynowy.teta.com/xsd")]
    public partial class Masa {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public string jednostka;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public StatusOperacji statusOperacji;
        
        /// <remarks/>
        public float wartosc;
    }
    
    /// <remarks/>
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://mobilnySystemMagazynowy.teta.com/xsd")]
    public partial class pobierzOdczytWagiResponse {
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable=true)]
        public Masa @return;
    }
}
