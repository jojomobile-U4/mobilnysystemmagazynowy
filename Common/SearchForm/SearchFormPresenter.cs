#region

using System;
using System.Windows.Forms;
using Common.Base;

#endregion

namespace Common.SearchForm
{
    public class SearchFormPresenter : PresenterBase
    {
        public SearchFormPresenter(ISearchForm view) : base(view)
        {
        }

        private ISearchForm View
        {
            get { return m_view as ISearchForm; }
        }

        protected override void AttachView()
        {
            View.Anuluj += OnViewAnuluj;
            View.UstawOstatnieZapytanie += OnViewUstawOstatnieZapytanie;
            View.Szukaj += OnViewSzukaj;
        }

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    e.Handled = true;
                    OnViewSzukaj(this, EventArgs.Empty);
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    OnViewAnuluj(this, EventArgs.Empty);
                    break;
                case Keys.D1:
                    e.Handled = true;
                    OnViewUstawOstatnieZapytanie(this, EventArgs.Empty);
                    break;
                case Keys.Up:
                    e.Handled = true;
                    View.FocusNextControl(false);
                    break;
                case Keys.Down:
                    e.Handled = true;
                    View.FocusNextControl(true);
                    break;
            }
        }

        protected virtual void OnViewSzukaj(object sender, EventArgs e)
        {
            CloseView();
        }

        protected virtual void OnViewAnuluj(object sender, EventArgs e)
        {
            CloseView();
        }

        protected virtual void OnViewUstawOstatnieZapytanie(object sender, EventArgs e)
        {
        }
    }
}