#region

using System;
using Common.Base;

#endregion

namespace Common.SearchForm
{
    public partial class SearchForm : ViewBase
    {
        public SearchForm()
        {
            InitializeComponent();
        }

        public event EventHandler Anuluj;
        public event EventHandler UstawOstatnieZapytanie;
        public event EventHandler Szukaj;

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (Szukaj != null)
            {
                Szukaj(this, EventArgs.Empty);
            }
        }

        private void btnOstatnieZapytanie_Click(object sender, EventArgs e)
        {
            if (UstawOstatnieZapytanie != null)
            {
                UstawOstatnieZapytanie(this, EventArgs.Empty);
            }
        }

        private void btnAnuluj_Click(object sender, EventArgs e)
        {
            if (Anuluj != null)
            {
                Anuluj(this, EventArgs.Empty);
            }
        }
    }
}