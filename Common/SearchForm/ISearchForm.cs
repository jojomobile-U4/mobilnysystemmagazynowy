#region

using System;
using Common.Base;

#endregion

namespace Common.SearchForm
{
    public interface ISearchForm : IViewBase
    {
        event EventHandler Anuluj;
        event EventHandler UstawOstatnieZapytanie;
        event EventHandler Szukaj;
    }
}