namespace Common.SearchForm
{
    partial class SearchForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAnuluj = new System.Windows.Forms.Button();
            this.btnOstatnieZapytanie = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnOK);
            this.pnlNavigation.Controls.Add(this.btnAnuluj);
            this.pnlNavigation.Controls.Add(this.btnOstatnieZapytanie);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 240);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 28);
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnAnuluj.Location = new System.Drawing.Point(165, 5);
            this.btnAnuluj.Name = "btnAnuluj";
            this.btnAnuluj.Size = new System.Drawing.Size(72, 20);
            this.btnAnuluj.TabIndex = 32;
            this.btnAnuluj.TabStop = false;
            this.btnAnuluj.Text = "&Esc Anuluj";
            this.btnAnuluj.Click += new System.EventHandler(this.btnAnuluj_Click);
            // 
            // btnOstatnieZapytanie
            // 
            this.btnOstatnieZapytanie.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOstatnieZapytanie.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnOstatnieZapytanie.Location = new System.Drawing.Point(63, 5);
            this.btnOstatnieZapytanie.Name = "btnOstatnieZapytanie";
            this.btnOstatnieZapytanie.Size = new System.Drawing.Size(99, 20);
            this.btnOstatnieZapytanie.TabIndex = 31;
            this.btnOstatnieZapytanie.TabStop = false;
            this.btnOstatnieZapytanie.Text = "&1 Ostatnie zap";
            this.btnOstatnieZapytanie.Click += new System.EventHandler(this.btnOstatnieZapytanie_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnOK.Location = new System.Drawing.Point(3, 5);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(57, 20);
            this.btnOK.TabIndex = 30;
            this.btnOK.TabStop = false;
            this.btnOK.Text = "&Ret OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // SearchForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Name = "SearchForm";
            this.Size = new System.Drawing.Size(240, 268);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

		protected System.Windows.Forms.Button btnAnuluj;
		protected System.Windows.Forms.Button btnOstatnieZapytanie;
		protected System.Windows.Forms.Button btnOK;

	}
}
