using System;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.DataModel;
using KartotekaMiejscaModule.KartotekaMiejscaProxy;
using KartotekaMiejscaModule.ProxyWrappers;
using Microsoft.Practices.Mobile.CompositeUI;

namespace KartotekaMiejscaModule.Services
{
    [Service(typeof(IKartotekaMiejscaService))]
    public class KartotekaMiejscaService : ServiceBase, IKartotekaMiejscaService
    {
        #region Properties

        protected override WebServiceConfiguration Configuration
        {
            get { return MyWorkItem.Configuration.KartotekaMiejscaWS; }
        }

        #endregion
        
        #region Konstruktory

        public KartotekaMiejscaService([ServiceDependency]WorkItem workItem):base(workItem)
        {            
        }

        #endregion
        #region Interfejs IKartotekaMiejscaService
     
        /// <summary>
        /// Pobiera miejsca z kartoteki miejsc
        /// </summary>
        /// <param name="lokalizacja">Filtr po lokalizacji</param>
        /// <param name="numerStrony">Numer wy�wietlanej strony</param>
        /// <param name="wielkoscStrony">Wielko�� strony</param>
        /// <returns>Miejsca spe�niaj�ce kryteria</returns>
        public Miejsce[] PobierzMiejsca(string lokalizacja, int numerStrony, int? wielkoscStrony)
        {
            Miejsce[] lista = null;
           
            try
            {
                using (WsKartotekaMiejscaWrap serviceAgent = new WsKartotekaMiejscaWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);

                    WsKartotekaMiejscaWrap.pobierzListeMiejscWrap param =
                        new WsKartotekaMiejscaWrap.pobierzListeMiejscWrap();
                    param.lokalizacja = Localization.AddPrefix(lokalizacja);
                    param.numerStrony = numerStrony;
                    param.wielkoscStrony = wielkoscStrony;                 
                    ListaMiejsc listaMiejsc = serviceAgent.pobierzListeMiejsc(param).@return;


                    MyWorkItem.RootWorkItem.State["StatusOperacji"] = listaMiejsc.statusOperacji.status;
                    MyWorkItem.RootWorkItem.State["TekstOperacji"] = listaMiejsc.statusOperacji.tekst;
                
                    //je�li zako�czy�o si� sukcesem pobieram dane
                    if (listaMiejsc.statusOperacji.status.Equals("S"))
                    {
                        lista = listaMiejsc.listaMiejsc;
                        if (lista!=null)
                        {
                            foreach (Miejsce miejce in lista)
                            {
                                miejce.lokalizacja = Localization.RemovePrefix(miejce.lokalizacja);                        
                            }
                        }
                    }   
                }                                           
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State["StatusOperacji"] = "F";
                MyWorkItem.RootWorkItem.State["TekstOperacji"] = e.ToString();                               
            }
            return lista; 
        }
        
        #endregion
    }
}
