using KartotekaMiejscaModule.KartotekaMiejscaProxy;

namespace KartotekaMiejscaModule.Services
{
    interface IKartotekaMiejscaService
    {       
        Miejsce[] PobierzMiejsca(string lokalizacja, int numerStrony, int? wielkoscStrony); 
    }
}
