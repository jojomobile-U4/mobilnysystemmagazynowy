#region

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using Common;
using Common.Base;
using KartotekaMiejscaModule.KartotekaMiejscaProxy;
using OpenNETCF.Web.Services2;

#endregion

namespace KartotekaMiejscaModule.ProxyWrappers
{
    public class WsKartotekaMiejscaWrap : WsKartotekaMiejsca, IWSSecurity
    {
        #region Private Fields

        #endregion

        #region Properties

        public SecurityHeader SecurityHeader { get; set; }

        #endregion

        #region Constructors

        public WsKartotekaMiejscaWrap(WebServiceConfiguration configuration)
        {
            Url = configuration.Url;
        }

        #endregion

        #region Protected Methods

        protected override WebRequest GetWebRequest(Uri uri)
        {
            var request = (HttpWebRequest) base.GetWebRequest(uri);

            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;

            return request;
        }

        #endregion

        #region Methods

        [SoapDocumentMethod("urn:pobierzListeMiejsc", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeMiejscResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeMiejscResponse pobierzListeMiejsc([XmlElement("pobierzListeMiejsc", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeMiejscWrap pobierzListeMiejsc1)
        {
            var results = Invoke("pobierzListeMiejsc", new object[]{pobierzListeMiejsc1});
            return ((pobierzListeMiejscResponse) (results[0]));
        }

        #endregion

        #region Nested type: pobierzListeMiejscWrap

        [DebuggerStepThrough]
        [DesignerCategory("code")]
        [XmlType(AnonymousType = true)]
        public class pobierzListeMiejscWrap : pobierzListeMiejsc
        {
            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public new string lokalizacja
            {
                get { return base.lokalizacja; }
                set { base.lokalizacja = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public new int? numerStrony
            {
                get { return base.numerStrony; }
                set { base.numerStrony = value; }
            }

            /// <remarks/>
            [XmlElement(IsNullable = true)]
            public new int? wielkoscStrony
            {
                get { return base.wielkoscStrony; }
                set { base.wielkoscStrony = value; }
            }
        }

        #endregion
    }
}