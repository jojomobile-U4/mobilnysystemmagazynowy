using System;
using Common;
using Common.SearchForm;
using KartotekaMiejscaModule.Browse;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;

namespace KartotekaMiejscaModule.Search
{
    public class MiejsceSearchViewPresenter : SearchFormPresenter
    {
        #region Zmienne prywatne
        
        private string OstatnioSzukane = String.Empty;

        #endregion
        #region G��wne w�asno�ci + konstruktor

        public MiejsceSearchWorkItem MyWorkItem
        {
            get
            {
                return (WorkItemBase as MiejsceSearchWorkItem);
            }            
        }

        public MiejsceSearchViewPresenter(IMiejsceSearchView miejsceView)
            : base(miejsceView)
        {            
        }

        public IMiejsceSearchView View
        {
            get
            {
                return (m_view as IMiejsceSearchView);
            }           
        }

        #endregion
        #region Obs�uga zdarze�

        protected override void OnViewSzukaj(object sender, EventArgs eventArgs)
        {
            //zamkniecie formatki wyszukiwania realizowane przez base.OnViewSzukaj
            base.OnViewSzukaj(sender, eventArgs);

            //zapami�tuje ostatni� wyszukiwan� lokalizacj�
            OstatnioSzukane = View.WartoscLokalizacji;//e.LocationString;

            (MyWorkItem.Parent as MiejsceWorkItem).Show(WorkItemBase.MainWorkspace,
                                                        View.WartoscLokalizacji, 0, ModuleInitializer.WIELKOSC_STRONY);				
        }

        protected override void OnViewUstawOstatnieZapytanie(object sender, EventArgs eventArgs)
        {
            base.OnViewUstawOstatnieZapytanie(sender, eventArgs);
            View.WartoscLokalizacji = OstatnioSzukane;
        }

        #endregion
        #region Obs�uga kod�w kreskowych

        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ)]
        public void WczytajKodKreskowy(object sender, EventArgs e)
        {
            if (MyWorkItem.Status == WorkItemStatus.Active)
            {
                OstatnioSzukane = MyWorkItem.RootWorkItem.State["BarCode"] as string;
                View.WartoscLokalizacji = OstatnioSzukane;
            }
        }

        #endregion

    }
}
