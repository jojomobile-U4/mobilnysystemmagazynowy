#region

using System;
using Common;
using Common.Base;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace KartotekaMiejscaModule.Search
{
    public class MiejsceSearchWorkItem : WorkItemBase
    {
        #region Ustalenie p�l prywatnych

        private MiejsceSearchView miejsceSearchView;
        private MiejsceSearchViewPresenter miejsceSearchViewPresenter;

        #endregion

        #region Metody publiczne

        public void Show(IWorkspace mainWorkspace)
        {
            FunctionsHelper.SetCursorWait();

            #region Inicjacja zmiennych

            miejsceSearchView = miejsceSearchView ?? Items.AddNew<MiejsceSearchView>();
            miejsceSearchViewPresenter = miejsceSearchViewPresenter ?? new MiejsceSearchViewPresenter(miejsceSearchView);
            Items.Add(miejsceSearchViewPresenter);

            miejsceSearchView.WartoscLokalizacji = String.Empty;

            #endregion

            #region Wy�wietlenie fomatki

            Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
            mainWorkspace.Show(miejsceSearchView);
            Activate();

            #endregion

            FunctionsHelper.SetCursorDefault();
        }

        #endregion
    }
}