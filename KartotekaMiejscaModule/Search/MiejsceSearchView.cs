#region

using Common.SearchForm;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace KartotekaMiejscaModule.Search
{
    [SmartPart]
    public partial class MiejsceSearchView : SearchForm, IMiejsceSearchView
    {
        #region Konstruktor

        public MiejsceSearchView()
        {
            InitializeComponent();
            InitializeFocusedControl();

            #region Podpi�cie si� pod zdarzenia na formatce

            //Szukaj -= new EventHandler(PrzesunieciaSearchView_Szukaj);
            //Szukaj += new EventHandler(PrzesunieciaSearchView_Szukaj);

            #endregion
        }

        #region  Interakcja u�ytkownika

        //private void PrzesunieciaSearchView_Szukaj(object sender, EventArgs e)
        //{
        //    if (WyswietlFormularzGlowny != null)
        //    {
        //        WyswietlFormularzGlowny(this, new SearchLocationEventArgs(tbLokalizacja.Text));
        //    }
        //}

        #endregion

        #endregion

        #region Implementacja interfejsu IMiejsceSearchView

        // Raised events must be 'published' to the presenter 
        // through the IMiejsceView interface.
        //public event EventHandler<SearchLocationEventArgs> WyswietlFormularzGlowny;

        public string WartoscLokalizacji
        {
            set { tbLokalizacja.Text = value; }
            get { return tbLokalizacja.Text; }
        }

        #endregion

        #region Override Methods

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);

            tbLokalizacja.ReadOnly = navigationState;
        }

        #endregion
    }
}