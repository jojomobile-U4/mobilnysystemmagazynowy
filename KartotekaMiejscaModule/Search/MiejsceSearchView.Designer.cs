
using Common.Components;

namespace KartotekaMiejscaModule.Search
{
    partial class MiejsceSearchView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu viewMenu;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.viewMenu = new System.Windows.Forms.MainMenu();
            this.lbTytul = new Common.Components.MSMLabel();
            this.lbLokalizacja = new Common.Components.MSMLabel();
            this.tbLokalizacja = new System.Windows.Forms.TextBox();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAnuluj
            // 
            this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // btnOstatnieZapytanie
            // 
            this.btnOstatnieZapytanie.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // btnOK
            // 
            this.btnOK.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 2;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "SZUKAJ MIEJSCA";
            // 
            // lbLokalizacja
            // 
            this.lbLokalizacja.BackColor = System.Drawing.Color.Empty;
            this.lbLokalizacja.BackGroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.lbLokalizacja.CenterAlignX = false;
            this.lbLokalizacja.CenterAlignY = true;
            this.lbLokalizacja.ColorText = System.Drawing.Color.Black;
            this.lbLokalizacja.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbLokalizacja.ForeColor = System.Drawing.Color.Empty;
            this.lbLokalizacja.Location = new System.Drawing.Point(3, 27);
            this.lbLokalizacja.Name = "lbLokalizacja";
            this.lbLokalizacja.RectangleColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.lbLokalizacja.Size = new System.Drawing.Size(100, 21);
            this.lbLokalizacja.TabIndex = 1;
            this.lbLokalizacja.TabStop = false;
            this.lbLokalizacja.TextDisplayed = "Lokalizacja:";
            // 
            // tbLokalizacja
            // 
            this.tbLokalizacja.Location = new System.Drawing.Point(102, 27);
            this.tbLokalizacja.MaxLength = 30;
            this.tbLokalizacja.Name = "tbLokalizacja";
            this.tbLokalizacja.Size = new System.Drawing.Size(135, 21);
            this.tbLokalizacja.TabIndex = 0;
            // 
            // MiejsceSearchView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.Controls.Add(this.tbLokalizacja);
            this.Controls.Add(this.lbLokalizacja);
            this.Controls.Add(this.lbTytul);
            this.Name = "MiejsceSearchView";
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lbLokalizacja, 0);
            this.Controls.SetChildIndex(this.tbLokalizacja, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MSMLabel lbTytul;
        private MSMLabel lbLokalizacja;
        private System.Windows.Forms.TextBox tbLokalizacja;
    }
}
