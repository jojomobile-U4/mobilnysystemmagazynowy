#region

using Common.SearchForm;

#endregion

namespace KartotekaMiejscaModule.Search
{
    public interface IMiejsceSearchView : ISearchForm
    {
        // TODO: Expose events for the 
        // presenter to attach to.
        // Expose methods for the presenter 
        // to update the view.	
//        event EventHandler<SearchLocationEventArgs> WyswietlFormularzGlowny;
        string WartoscLokalizacji { get; set; }
    }
}