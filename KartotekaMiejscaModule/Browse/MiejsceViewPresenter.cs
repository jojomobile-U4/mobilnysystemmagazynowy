#region

using System;
using System.Windows.Forms;
using Common;
using Common.Base;
using KartotekaMiejscaModule.KartotekaMiejscaProxy;

#endregion

namespace KartotekaMiejscaModule.Browse
{
    public class MiejsceViewPresenter : PresenterBase
    {
        #region Zmienne prywatne

        private string m_Lokalizacja;
        private Miejsce[] m_Miejsca;
        private int m_NumerWyswietlanegoRekordu;
        private int m_NumerWyswietlanejStrony;

        #endregion

        #region Pola i zmienne publiczne

        public MiejsceViewPresenter(IMiejsceView miejsceView)
            : base(miejsceView)
        {
        }

        public MiejsceWorkItem MyWorkItem
        {
            get { return (WorkItemBase as MiejsceWorkItem); }
        }

        public IMiejsceView View
        {
            get { return m_view as IMiejsceView; }
        }

        public void WyswietlWyszukane(Miejsce[] pMiejsca)
        {
            m_Miejsca = pMiejsca;

            //je�li w odpowiedzi nic nie dosta�em to tworz� zwracam pusty
            if (pMiejsca == null || pMiejsca.Length == 0)
            {
                m_Miejsca = new[] {new Miejsce()};
                m_Miejsca[0].listaIndeksow = new Indeks[0];
            }

            m_NumerWyswietlanegoRekordu = 0;
            //zawsze po wczytaniu danych ustalam warto�� numeru wyswietlanego na 0 chyba �e nie ma nic w wyniku                        
            UstalWyswietlaneIndeksy();
        }

        #endregion

        #region Metody prywatne pomocnicze

        private void UstalWyswietlaneIndeksy()
        {
            View.PrzyciskPoprzedniRekord = m_NumerWyswietlanegoRekordu != 0 || m_NumerWyswietlanejStrony != 0;
            View.WyswietlaneMiejsce = m_Miejsca[m_NumerWyswietlanegoRekordu];
            View.WywietlaneIndeksy = m_Miejsca[m_NumerWyswietlanegoRekordu].listaIndeksow;

            UstawKontekstKartotekiIndeksu(View.PobierzSymbolBiezacegoIndeksu);
        }

        protected override void AttachView()
        {
            #region Podpi�cie zdarze� z interfejsu IMiejsceView

            View.WyswietlFormularzSzukania -= ViewWyswietlFormularzSzukania;
            View.WyswietlFormularzSzukania += ViewWyswietlFormularzSzukania;
            View.NastepnyRekord -= ViewNastepnyRekord;
            View.NastepnyRekord += ViewNastepnyRekord;
            View.PoprzedniRekord -= ViewPoprzedniRekord;
            View.PoprzedniRekord += ViewPoprzedniRekord;
            View.Powrot -= ViewPowrot;
            View.Powrot += ViewPowrot;
            View.PobierzIndeksRekord -= ViewPobierzIndeks;
            View.PobierzIndeksRekord += ViewPobierzIndeks;

            #endregion
        }

        #endregion

        #region Obs�uga zdarze�

        private void ViewPobierzIndeks(object sender, EventArgs e)
        {
            UstawKontekstKartotekiIndeksu(View.PobierzSymbolBiezacegoIndeksu);
        }

        private void ViewPoprzedniRekord(object sender, EventArgs e)
        {
            if (m_NumerWyswietlanegoRekordu > 0)
            {
                m_NumerWyswietlanegoRekordu--;
                UstalWyswietlaneIndeksy();
            }
            else if (m_NumerWyswietlanegoRekordu == 0 && m_NumerWyswietlanejStrony > 0)
            {
                m_NumerWyswietlanejStrony--;
                MyWorkItem.Show(MyWorkItem.MainWorkspace, m_Lokalizacja, m_NumerWyswietlanejStrony, ModuleInitializer.WIELKOSC_STRONY);
                //id� na ostatni element w paczce
                if (m_Miejsca.Length > 0)
                {
                    m_NumerWyswietlanegoRekordu = m_Miejsca.Length - 1;
                    UstalWyswietlaneIndeksy();
                }
            }

            View.PrzyciskPoprzedniRekord = m_NumerWyswietlanegoRekordu != 0 || m_NumerWyswietlanejStrony != 0;
        }

        private void ViewNastepnyRekord(object sender, EventArgs e)
        {
            if (m_Miejsca != null)
            {
                if (m_NumerWyswietlanegoRekordu < m_Miejsca.Length - 1)
                {
                    m_NumerWyswietlanegoRekordu++;
                    UstalWyswietlaneIndeksy();
                }
                else if (m_NumerWyswietlanegoRekordu == m_Miejsca.Length - 1)
                {
                    m_NumerWyswietlanejStrony++;
                    MyWorkItem.Show(MyWorkItem.MainWorkspace, m_Lokalizacja, m_NumerWyswietlanejStrony, ModuleInitializer.WIELKOSC_STRONY);
                }
            }
        }

        private void ViewWyswietlFormularzSzukania(object sender, EventArgs e)
        {
            //wyswietlenie drugiego formularza
            MyWorkItem.ShowSearch();
        }

        private void ViewPowrot(object sender, EventArgs e)
        {
            UstawKontekstKartotekiIndeksu(null);

            WorkItemBase.MainWorkspace.Close(View);
        }

        private void UstawKontekstKartotekiIndeksu(string symbol)
        {
            MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU] = symbol;
            MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = string.IsNullOrEmpty(symbol) ? StateConstants.BoolValueNo : StateConstants.BoolValueYes;
            MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = symbol;
            MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = string.IsNullOrEmpty(symbol) ? StateConstants.BoolValueNo : StateConstants.BoolValueYes;

        }

        #endregion

        #region Zapami�tanie kryterium wyszukiwania

        public void UstawWarunek(string location, int numerStrony)
        {
            m_Lokalizacja = location;
            m_NumerWyswietlanejStrony = numerStrony;
        }

        #endregion

        #region Obs�uga klawiatury

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    e.Handled = true;
                    ViewPoprzedniRekord(this, EventArgs.Empty);
                    break;
                case Keys.Right:
                    e.Handled = true;
                    ViewNastepnyRekord(this, EventArgs.Empty);
                    break;
                case Keys.D1:
                    e.Handled = true;
                    ViewWyswietlFormularzSzukania(this, EventArgs.Empty);
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    ViewPowrot(this, EventArgs.Empty);
                    MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_MIEJSCA] = string.Empty;
                    break;
            }
        }

        #endregion
    }
}