
using Common.Components;
using Common.ListViewSort;

namespace KartotekaMiejscaModule.Browse
{
    partial class MiejsceView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbTytul = new Common.Components.MSMLabel();
            this.lbLokalizacja = new Common.Components.MSMLabel();
            this.lbABC = new Common.Components.MSMLabel();
            this.miejsceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tbLokalizacja = new System.Windows.Forms.TextBox();
            this.tbABC = new System.Windows.Forms.TextBox();
            this.lbBlokadaPrzyjecia = new Common.Components.MSMLabel();
            this.tbBlokadaPrzyjecia = new System.Windows.Forms.TextBox();
            this.tbBlokadaWydania = new System.Windows.Forms.TextBox();
            this.lbBlokadaWydania = new Common.Components.MSMLabel();
            this.lbNazwaTowaru = new Common.Components.MSMLabel();
            this.tbNazwaTowaru = new System.Windows.Forms.TextBox();
            this.btnSzukaj = new System.Windows.Forms.Button();
            this.btnPowrot = new System.Windows.Forms.Button();
            this.lsIndeksy = new Common.Components.MSMListView();
            this.chSymbol = new System.Windows.Forms.ColumnHeader();
            this.chABC = new System.Windows.Forms.ColumnHeader();
            this.chIlosc = new System.Windows.Forms.ColumnHeader();
            this.chJM = new System.Windows.Forms.ColumnHeader();
            this.lrcNavigation = new Common.Components.LeftRightControl();
            this.pnlNavigation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.miejsceBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnSzukaj);
            this.pnlNavigation.Controls.Add(this.btnPowrot);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 273);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 27);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 13;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "MIEJSCA";
            // 
            // lbLokalizacja
            // 
            this.lbLokalizacja.BackColor = System.Drawing.Color.Empty;
            this.lbLokalizacja.BackGroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.lbLokalizacja.CenterAlignX = false;
            this.lbLokalizacja.CenterAlignY = true;
            this.lbLokalizacja.ColorText = System.Drawing.Color.Black;
            this.lbLokalizacja.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbLokalizacja.ForeColor = System.Drawing.Color.Empty;
            this.lbLokalizacja.Location = new System.Drawing.Point(4, 26);
            this.lbLokalizacja.Name = "lbLokalizacja";
            this.lbLokalizacja.RectangleColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.lbLokalizacja.Size = new System.Drawing.Size(100, 21);
            this.lbLokalizacja.TabIndex = 12;
            this.lbLokalizacja.TabStop = false;
            this.lbLokalizacja.TextDisplayed = "Lokalizacja:";
            // 
            // lbABC
            // 
            this.lbABC.BackColor = System.Drawing.Color.Empty;
            this.lbABC.BackGroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.lbABC.CenterAlignX = false;
            this.lbABC.CenterAlignY = true;
            this.lbABC.ColorText = System.Drawing.Color.Black;
            this.lbABC.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbABC.ForeColor = System.Drawing.Color.Empty;
            this.lbABC.Location = new System.Drawing.Point(4, 49);
            this.lbABC.Name = "lbABC";
            this.lbABC.RectangleColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.lbABC.Size = new System.Drawing.Size(100, 21);
            this.lbABC.TabIndex = 11;
            this.lbABC.TabStop = false;
            this.lbABC.TextDisplayed = "ABC:";
            // 
            // miejsceBindingSource
            // 
            this.miejsceBindingSource.AllowNew = false;
            this.miejsceBindingSource.DataSource = typeof(KartotekaMiejscaModule.KartotekaMiejscaProxy.Miejsce);
            // 
            // tbLokalizacja
            // 
            this.tbLokalizacja.Location = new System.Drawing.Point(87, 26);
            this.tbLokalizacja.Name = "tbLokalizacja";
            this.tbLokalizacja.ReadOnly = true;
            this.tbLokalizacja.Size = new System.Drawing.Size(150, 21);
            this.tbLokalizacja.TabIndex = 2;
            this.tbLokalizacja.TabStop = false;
            // 
            // tbABC
            // 
            this.tbABC.Location = new System.Drawing.Point(87, 49);
            this.tbABC.Name = "tbABC";
            this.tbABC.ReadOnly = true;
            this.tbABC.Size = new System.Drawing.Size(28, 21);
            this.tbABC.TabIndex = 3;
            this.tbABC.TabStop = false;
            // 
            // lbBlokadaPrzyjecia
            // 
            this.lbBlokadaPrzyjecia.BackColor = System.Drawing.Color.Empty;
            this.lbBlokadaPrzyjecia.BackGroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.lbBlokadaPrzyjecia.CenterAlignX = false;
            this.lbBlokadaPrzyjecia.CenterAlignY = true;
            this.lbBlokadaPrzyjecia.ColorText = System.Drawing.Color.Black;
            this.lbBlokadaPrzyjecia.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbBlokadaPrzyjecia.ForeColor = System.Drawing.Color.Empty;
            this.lbBlokadaPrzyjecia.Location = new System.Drawing.Point(4, 72);
            this.lbBlokadaPrzyjecia.Name = "lbBlokadaPrzyjecia";
            this.lbBlokadaPrzyjecia.RectangleColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.lbBlokadaPrzyjecia.Size = new System.Drawing.Size(100, 21);
            this.lbBlokadaPrzyjecia.TabIndex = 10;
            this.lbBlokadaPrzyjecia.TabStop = false;
            this.lbBlokadaPrzyjecia.TextDisplayed = "Blokada przyj.:";
            // 
            // tbBlokadaPrzyjecia
            // 
            this.tbBlokadaPrzyjecia.Location = new System.Drawing.Point(87, 72);
            this.tbBlokadaPrzyjecia.Name = "tbBlokadaPrzyjecia";
            this.tbBlokadaPrzyjecia.ReadOnly = true;
            this.tbBlokadaPrzyjecia.Size = new System.Drawing.Size(28, 21);
            this.tbBlokadaPrzyjecia.TabIndex = 4;
            this.tbBlokadaPrzyjecia.TabStop = false;
            // 
            // tbBlokadaWydania
            // 
            this.tbBlokadaWydania.Location = new System.Drawing.Point(209, 72);
            this.tbBlokadaWydania.Name = "tbBlokadaWydania";
            this.tbBlokadaWydania.ReadOnly = true;
            this.tbBlokadaWydania.Size = new System.Drawing.Size(28, 21);
            this.tbBlokadaWydania.TabIndex = 5;
            this.tbBlokadaWydania.TabStop = false;
            // 
            // lbBlokadaWydania
            // 
            this.lbBlokadaWydania.BackColor = System.Drawing.Color.Empty;
            this.lbBlokadaWydania.BackGroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.lbBlokadaWydania.CenterAlignX = false;
            this.lbBlokadaWydania.CenterAlignY = true;
            this.lbBlokadaWydania.ColorText = System.Drawing.Color.Black;
            this.lbBlokadaWydania.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbBlokadaWydania.ForeColor = System.Drawing.Color.Empty;
            this.lbBlokadaWydania.Location = new System.Drawing.Point(121, 73);
            this.lbBlokadaWydania.Name = "lbBlokadaWydania";
            this.lbBlokadaWydania.RectangleColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.lbBlokadaWydania.Size = new System.Drawing.Size(100, 21);
            this.lbBlokadaWydania.TabIndex = 9;
            this.lbBlokadaWydania.TabStop = false;
            this.lbBlokadaWydania.TextDisplayed = "Blokada wyd.:";
            // 
            // lbNazwaTowaru
            // 
            this.lbNazwaTowaru.BackColor = System.Drawing.Color.Empty;
            this.lbNazwaTowaru.BackGroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.lbNazwaTowaru.CenterAlignX = false;
            this.lbNazwaTowaru.CenterAlignY = true;
            this.lbNazwaTowaru.ColorText = System.Drawing.Color.Black;
            this.lbNazwaTowaru.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbNazwaTowaru.ForeColor = System.Drawing.Color.Empty;
            this.lbNazwaTowaru.Location = new System.Drawing.Point(3, 229);
            this.lbNazwaTowaru.Name = "lbNazwaTowaru";
            this.lbNazwaTowaru.RectangleColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.lbNazwaTowaru.Size = new System.Drawing.Size(100, 21);
            this.lbNazwaTowaru.TabIndex = 8;
            this.lbNazwaTowaru.TabStop = false;
            this.lbNazwaTowaru.TextDisplayed = "Nazwa towaru:";
            // 
            // tbNazwaTowaru
            // 
            this.tbNazwaTowaru.Location = new System.Drawing.Point(3, 247);
            this.tbNazwaTowaru.Multiline = true;
            this.tbNazwaTowaru.Name = "tbNazwaTowaru";
            this.tbNazwaTowaru.ReadOnly = true;
            this.tbNazwaTowaru.Size = new System.Drawing.Size(234, 21);
            this.tbNazwaTowaru.TabIndex = 7;
            this.tbNazwaTowaru.TabStop = false;
            // 
            // btnSzukaj
            // 
            this.btnSzukaj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnSzukaj.Location = new System.Drawing.Point(79, 4);
            this.btnSzukaj.Name = "btnSzukaj";
            this.btnSzukaj.Size = new System.Drawing.Size(72, 20);
            this.btnSzukaj.TabIndex = 8;
            this.btnSzukaj.TabStop = false;
            this.btnSzukaj.Text = "&1 Szukaj";
            this.btnSzukaj.Click += new System.EventHandler(this.btnSzukaj_Click);
            // 
            // btnPowrot
            // 
            this.btnPowrot.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPowrot.Location = new System.Drawing.Point(156, 4);
            this.btnPowrot.Name = "btnPowrot";
            this.btnPowrot.Size = new System.Drawing.Size(81, 20);
            this.btnPowrot.TabIndex = 9;
            this.btnPowrot.TabStop = false;
            this.btnPowrot.Text = "&Esc Powr�t";
            this.btnPowrot.Click += new System.EventHandler(this.btnPowrot_Click);
            // 
            // lsIndeksy
            // 
            this.lsIndeksy.Columns.Add(this.chSymbol);
            this.lsIndeksy.Columns.Add(this.chABC);
            this.lsIndeksy.Columns.Add(this.chIlosc);
            this.lsIndeksy.Columns.Add(this.chJM);
            this.lsIndeksy.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lsIndeksy.FullRowSelect = true;
            this.lsIndeksy.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lsIndeksy.Location = new System.Drawing.Point(3, 95);
            this.lsIndeksy.Name = "lsIndeksy";
            this.lsIndeksy.Size = new System.Drawing.Size(234, 135);
            this.lsIndeksy.TabIndex = 6;
            this.lsIndeksy.View = System.Windows.Forms.View.Details;
            this.lsIndeksy.SelectedIndexChanged += new System.EventHandler(this.lsIndeksy_SelectedIndexChanged);
            // 
            // chSymbol
            // 
            this.chSymbol.Text = "Indeks";
            this.chSymbol.Width = 105;
            // 
            // chABC
            // 
            this.chABC.Text = "ABC";
            this.chABC.Width = 30;
            // 
            // chIlosc
            // 
            this.chIlosc.Text = "Ilo��";
            this.chIlosc.Width = 50;
            // 
            // chJM
            // 
            this.chJM.Text = "J.m.";
            this.chJM.Width = 30;
            // 
            // lrcNavigation
            // 
            this.lrcNavigation.BackColor = System.Drawing.SystemColors.Desktop;
            this.lrcNavigation.Location = new System.Drawing.Point(208, 0);
            this.lrcNavigation.Name = "lrcNavigation";
            this.lrcNavigation.NextEnabled = true;
            this.lrcNavigation.PreviousEnabled = true;
            this.lrcNavigation.Size = new System.Drawing.Size(32, 16);
            this.lrcNavigation.TabIndex = 1;
            this.lrcNavigation.TabStop = false;
            this.lrcNavigation.Next += new System.EventHandler(this.btnRight_Click);
            this.lrcNavigation.Previous += new System.EventHandler(this.btnLeft_Click);
            // 
            // MiejsceView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.lrcNavigation);
            this.Controls.Add(this.lsIndeksy);
            this.Controls.Add(this.tbNazwaTowaru);
            this.Controls.Add(this.lbNazwaTowaru);
            this.Controls.Add(this.tbBlokadaWydania);
            this.Controls.Add(this.lbBlokadaWydania);
            this.Controls.Add(this.tbBlokadaPrzyjecia);
            this.Controls.Add(this.lbBlokadaPrzyjecia);
            this.Controls.Add(this.tbABC);
            this.Controls.Add(this.tbLokalizacja);
            this.Controls.Add(this.lbABC);
            this.Controls.Add(this.lbLokalizacja);
            this.Controls.Add(this.lbTytul);
            this.Name = "MiejsceView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lbLokalizacja, 0);
            this.Controls.SetChildIndex(this.lbABC, 0);
            this.Controls.SetChildIndex(this.tbLokalizacja, 0);
            this.Controls.SetChildIndex(this.tbABC, 0);
            this.Controls.SetChildIndex(this.lbBlokadaPrzyjecia, 0);
            this.Controls.SetChildIndex(this.tbBlokadaPrzyjecia, 0);
            this.Controls.SetChildIndex(this.lbBlokadaWydania, 0);
            this.Controls.SetChildIndex(this.tbBlokadaWydania, 0);
            this.Controls.SetChildIndex(this.lbNazwaTowaru, 0);
            this.Controls.SetChildIndex(this.tbNazwaTowaru, 0);
            this.Controls.SetChildIndex(this.lsIndeksy, 0);
            this.Controls.SetChildIndex(this.lrcNavigation, 0);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.pnlNavigation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.miejsceBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MSMLabel lbTytul;
        private MSMLabel lbLokalizacja;
        private MSMLabel lbABC;
        private System.Windows.Forms.TextBox tbLokalizacja;
        private System.Windows.Forms.TextBox tbABC;
        private MSMLabel lbBlokadaPrzyjecia;
        private System.Windows.Forms.TextBox tbBlokadaPrzyjecia;
        private System.Windows.Forms.TextBox tbBlokadaWydania;
        private MSMLabel lbBlokadaWydania;
        private MSMLabel lbNazwaTowaru;
        private System.Windows.Forms.TextBox tbNazwaTowaru;
        private System.Windows.Forms.Button btnSzukaj;
        private System.Windows.Forms.Button btnPowrot;
        private System.Windows.Forms.BindingSource miejsceBindingSource;
        private MSMListView lsIndeksy;
        private System.Windows.Forms.ColumnHeader chSymbol;
        private System.Windows.Forms.ColumnHeader chABC;
        private System.Windows.Forms.ColumnHeader chIlosc;
		private System.Windows.Forms.ColumnHeader chJM;
		private LeftRightControl lrcNavigation;
//        private ColHeader chSymbol;
//        private ColHeader chABC;
//        private ColHeader chIlosc;
//        private ColHeader chJM;
    }
}
