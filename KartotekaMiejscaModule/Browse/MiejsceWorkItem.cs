#region

using System;
using Common;
using Common.Base;
using KartotekaMiejscaModule.Search;
using KartotekaMiejscaModule.Services;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace KartotekaMiejscaModule.Browse
{
    public class MiejsceWorkItem : WorkItemBase
    {
        #region Pola prywatne

        private MiejsceSearchWorkItem miejsceSearchWorkItem;

        #endregion

        #region Metody publiczne

        /// <summary>
        /// Pokazanie g��wnego formularza
        /// </summary>
        /// <param name="mainWorkspace">Workspace gdzie ma zosta� wy�wietlony</param>        
        public void Show(IWorkspace mainWorkspace)
        {
            try
            {
                FunctionsHelper.SetCursorWait();
                var miejsceViewPresenter = Items.Get<MiejsceViewPresenter>(ItemsConstants.MIEJSCE_PRESENTER);
                if (RootWorkItem.State[StateConstants.SYMBOL_MIEJSCA] != null && RootWorkItem.State[StateConstants.SYMBOL_MIEJSCA].ToString() != string.Empty)
                {
                    Show(mainWorkspace, RootWorkItem.State[StateConstants.SYMBOL_MIEJSCA].ToString(), 0, null);
                }
                else
                {
                    mainWorkspace.Show(miejsceViewPresenter.View);
                }
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
                Activate();
            }
        }

        /// <summary>
        /// Pokazanie g��wnego formularza
        /// </summary>
        /// <param name="mainWorkspace">Workspace gdzie ma zosta� wy�wietlony</param>
        /// <param name="lokalizacja">Lokalizacja, kt�r� nale�y wyszuka�</param>        
        public void Show(IWorkspace mainWorkspace, string lokalizacja, int numerStrony, int? wielkoscStrony)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var kartotekaMiejscaService = Services.Get<IKartotekaMiejscaService>(true);
                var miejsca = kartotekaMiejscaService.PobierzMiejsca(lokalizacja, numerStrony, wielkoscStrony);
                if (!RootWorkItem.State["StatusOperacji"].Equals("S"))
                {
                    ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), "Pobranie miejsc", RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] as string);
                    Activate();
                }

                var miejsceViewPresenter = Items.Get<MiejsceViewPresenter>(ItemsConstants.MIEJSCE_PRESENTER);
                miejsceViewPresenter.WyswietlWyszukane(miejsca);
                miejsceViewPresenter.UstawWarunek(lokalizacja, numerStrony);


                mainWorkspace.Show(miejsceViewPresenter.View);
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
                Activate();
            }
        }

        /// <summary>
        /// Pokazanie formularza do wyszukania
        /// </summary>
        public void ShowSearch()
        {
            var mainWorkspace = Parent.Workspaces["mainWorkSpace"];
            miejsceSearchWorkItem = miejsceSearchWorkItem ?? WorkItems.AddNew<MiejsceSearchWorkItem>();
            miejsceSearchWorkItem.Show(mainWorkspace);
        }

        #region Kod kreskowy

        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ, Thread = ThreadOption.UserInterface)]
        public void WczytajKodKreskowy(object sender, EventArgs e)
        {

            if (MainWorkspace.ActiveSmartPart.ToString() == Items.Get<MiejsceViewPresenter>(ItemsConstants.MIEJSCE_PRESENTER).View.ToString())
            {
                Show(MainWorkspace, RootWorkItem.State["BarCode"] as string, 0, ModuleInitializer.WIELKOSC_STRONY);
            }
        }

        #endregion

        #endregion
    }
}