#region

using System;
using System.Windows.Forms;
using Common.Base;
using KartotekaMiejscaModule.KartotekaMiejscaProxy;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace KartotekaMiejscaModule.Browse
{
    [SmartPart]
    public partial class MiejsceView : ViewBase, IMiejsceView
    {
        #region Pola prywatne

        #endregion

        #region Metody pomocnicze

        private void UstawWartosciKontrolek(Miejsce wyswietlaneMiejsce)
        {
            tbABC.Text = wyswietlaneMiejsce.aBC;
            tbBlokadaWydania.Text = wyswietlaneMiejsce.blokadaWydania;
            tbBlokadaPrzyjecia.Text = wyswietlaneMiejsce.blokadaPrzyjec;
            tbLokalizacja.Text = wyswietlaneMiejsce.lokalizacja;
            tbNazwaTowaru.Text = wyswietlaneMiejsce.nazwaTowaru;
        }

        #endregion

        #region Konstruktor

        public MiejsceView()
        {
            InitializeComponent();
            //Nadpisuj� kolumny aby umo�liwi� sortowanie
            // StworzKolumnySortowalne();
            InitializeFocusedControl();
        }

        #endregion

        #region StworzKolumnySortowalne

        /*
        private void StworzKolumnySortowalne()
        {
            chSymbol = new ColHeader();
            chABC = new ColHeader();
            chIlosc = new ColHeader();
            chJM = new ColHeader();
            // 
            // chSymbol
            // 
            chSymbol.Text = "Symbol";
            chSymbol.Width = 105;
            // 
            // chABC
            // 
            chABC.Text = "ABC";
            chABC.Width = 30;
            // 
            // chIlosc
            // 
            chIlosc.Text = "Ilo��";
            chIlosc.Width = 50;
            // 
            // chJM
            // 
            chJM.Text = "J.M.";
            chJM.Width = 30;

            lsIndeksy.Columns.Clear();
            lsIndeksy.Columns.Add(chSymbol);
            lsIndeksy.Columns.Add(chABC);
            lsIndeksy.Columns.Add(chIlosc);
            lsIndeksy.Columns.Add(chJM);
        }
        */

        #endregion

        #region Interfejs IMiejsceView - ustawianie warto�ci wywietlanych kontrolek

        public event EventHandler WyswietlFormularzSzukania;
        public event EventHandler PoprzedniRekord;
        public event EventHandler NastepnyRekord;
        public event EventHandler PobierzIndeksRekord;
        public event EventHandler Powrot;

        public Miejsce WyswietlaneMiejsce
        {
            set { UstawWartosciKontrolek(value); }
        }

        public Indeks[] WywietlaneIndeksy
        {
            set
            {
                lsIndeksy.Items.Clear();
                ListViewItem listViewItem;
                if (value == null) return;

                foreach (var indeks in value)
                {
                    listViewItem = lsIndeksy.Items.Add(new ListViewItem(indeks.symbol));
                    listViewItem.SubItems.Add(indeks.abc);
                    listViewItem.SubItems.Add(indeks.ilosc.ToString("0.000"));
                    listViewItem.SubItems.Add(indeks.jm);
                    listViewItem.Tag = indeks.nazwaTowaru;
                }
            }
        }

        public string PobierzSymbolBiezacegoIndeksu { get; private set; }

        public bool PrzyciskNastepnyRekord
        {
            set { lrcNavigation.NextEnabled = value; }
        }

        public bool PrzyciskPoprzedniRekord
        {
            set { lrcNavigation.PreviousEnabled = value; }
        }

        #endregion

        #region Interakcja

        private void btnSzukaj_Click(object sender, EventArgs e)
        {
            if (WyswietlFormularzSzukania != null)
            {
                WyswietlFormularzSzukania(this, null);
            }
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            if (PoprzedniRekord != null)
            {
                PoprzedniRekord(this, null);
            }
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            if (NastepnyRekord != null)
            {
                NastepnyRekord(this, null);
            }
        }

        private void btnPowrot_Click(object sender, EventArgs e)
        {
            if (Powrot != null)
            {
                Powrot(this, EventArgs.Empty);
            }
        }

        private void lsIndeksy_SelectedIndexChanged(object sender, EventArgs e)
        {
            UstawNazweTowaru();
            if (PobierzIndeksRekord != null)
                PobierzIndeksRekord(sender, e);
        }


        private void UstawNazweTowaru()
        {
            if (lsIndeksy.Items.Count > 0 && lsIndeksy.SelectedIndices.Count > 0 && lsIndeksy.Items[lsIndeksy.SelectedIndices[0]].Tag != null)
            {
                tbNazwaTowaru.Text = lsIndeksy.Items[lsIndeksy.SelectedIndices[0]].Tag.ToString();
                PobierzSymbolBiezacegoIndeksu = lsIndeksy.Items[lsIndeksy.SelectedIndices[0]].Text;
            }
            else
                tbNazwaTowaru.Text = String.Empty;
        }

        #region LsIndeksyColumnClick

        /*
        private void LsIndeksyColumnClick(object sender, ColumnClickEventArgs e)        
        {
            if (lsIndeksy.Columns[e.Column].Text!="Symbol")
                return;
            // Create an instance of the ColHeader class. 
            ColHeader clickedCol = (ColHeader)this.lsIndeksy.Columns[e.Column];

            // Set the ascending property to sort in the opposite order.
            clickedCol.ascending = !clickedCol.ascending;

            // Get the number of items in the list.
            int numItems = lsIndeksy.Items.Count;

            // Turn off display while data is repoplulated.
            lsIndeksy.BeginUpdate();

            // Populate an ArrayList with a SortWrapper of each list item.
            ArrayList SortArray = new ArrayList();
            for (int i = 0; i < numItems; i++)
            {
                SortArray.Add(new SortWrapper(lsIndeksy.Items[i], e.Column));
            }

            // Sort the elements in the ArrayList using a new instance of the SortComparer
            // class. The parameters are the starting index, the length of the range to sort,
            // and the IComparer implementation to use for comparing elements.
            // the IComparer implementation (SortComparer) requires the sort  
            // direction for its constructor; true if ascending, othwise false.
            SortArray.Sort(0, SortArray.Count, new SortWrapper.SortComparer(clickedCol.ascending));

            // Clear the list, and repopulate with the sorted items.
            lsIndeksy.Items.Clear();
            for (int i = 0; i < numItems; i++)
                lsIndeksy.Items.Add(((SortWrapper)SortArray[i]).sortItem);

            // Turn display back on.
            lsIndeksy.EndUpdate();

            UstawNazweTowaru();
        }                      

        */

        #endregion

        #endregion

        public void UstawFocus()
        {
            lsIndeksy.Focus();
        }
    }
}