using System;
using Common.Base;
using KartotekaMiejscaModule.KartotekaMiejscaProxy;

namespace KartotekaMiejscaModule.Browse
{
    public interface IMiejsceView : IViewBase
    {
        #region Zdarzenia

        event EventHandler WyswietlFormularzSzukania;
        event EventHandler PoprzedniRekord;
        event EventHandler NastepnyRekord;
        event EventHandler PobierzIndeksRekord;
        event EventHandler Powrot;

        #endregion
        #region W�asno�ci

        Miejsce WyswietlaneMiejsce { set; }
        Indeks[] WywietlaneIndeksy { set; }
        bool PrzyciskNastepnyRekord { set; }
        bool PrzyciskPoprzedniRekord { set; }
        string PobierzSymbolBiezacegoIndeksu { get; }

        #endregion

    }
}
