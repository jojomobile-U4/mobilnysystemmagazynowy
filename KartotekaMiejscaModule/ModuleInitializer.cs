#region

using System;
using Common;
using KartotekaMiejscaModule.Browse;
using KartotekaMiejscaModule.Services;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.Commands;

#endregion

namespace KartotekaMiejscaModule
{
    public class ModuleInitializer : ModuleInit
    {
        #region Zmienne prywatne

        public static readonly int WIELKOSC_STRONY = 10;
        private readonly WorkItem workItem;

        #endregion

        #region Inicjalizacja modu�u

        public ModuleInitializer([ServiceDependency] WorkItem workItem)
        {
            this.workItem = workItem;
        }

        public override void Load()
        {
            base.Load();
        }

        private void Initialize()
        {
            try
            {
                if (!workItem.WorkItems.Contains(WorkItemsConstants.MIEJSCE_WORKITEM))
                {
                    var miejsceWorkItem = workItem.WorkItems.AddNew<MiejsceWorkItem>(WorkItemsConstants.MIEJSCE_WORKITEM);
                    var miejsceViewPresenter = new MiejsceViewPresenter(miejsceWorkItem.Items.AddNew<MiejsceView>());

                    miejsceWorkItem.Items.Add(miejsceViewPresenter, ItemsConstants.MIEJSCE_PRESENTER);
                }

                if (!workItem.Services.Contains(typeof (IKartotekaMiejscaService)))
                {
                    workItem.Services.AddNew(typeof (KartotekaMiejscaService), typeof (IKartotekaMiejscaService));
                }

                FunctionsHelper.SetCursorWait();

                workItem.RootWorkItem.State["StatusOperacji"] = "S";
                workItem.RootWorkItem.State["TekstOperacji"] = null;
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        #endregion

        #region Obs�uga g��wnego menu

        [CommandHandler(CommandConstants.KARTOTEKA_MIEJSCA)]
        public void KartotekaMiejsca(object sender, EventArgs e)
        {
            Initialize();
            var mainWorkspace = workItem.Workspaces[WorkspacesConstants.MAIN_WORKSPACE];
            var miejsceWorkItem = workItem.WorkItems.Get<MiejsceWorkItem>(WorkItemsConstants.MIEJSCE_WORKITEM);
            miejsceWorkItem.Show(mainWorkspace);
        }

        #endregion
    }
}