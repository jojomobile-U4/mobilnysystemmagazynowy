
using Common.Components;

namespace KartotekaIndeksuModule
{
	partial class IndeksView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lbTytul = new Common.Components.MSMLabel();
            this.tbSymbol = new System.Windows.Forms.TextBox();
            this.lbSymbol = new System.Windows.Forms.Label();
            this.tbVat = new System.Windows.Forms.TextBox();
            this.lbVat = new System.Windows.Forms.Label();
            this.tbBranzysta = new System.Windows.Forms.TextBox();
            this.lbBranza = new System.Windows.Forms.Label();
            this.tbNazwaTowaru = new System.Windows.Forms.TextBox();
            this.lbNazwa = new System.Windows.Forms.Label();
            this.tbJm = new System.Windows.Forms.TextBox();
            this.lbJm = new System.Windows.Forms.Label();
            this.tbAbc = new System.Windows.Forms.TextBox();
            this.lbAbc = new System.Windows.Forms.Label();
            this.btnPowrot = new System.Windows.Forms.Button();
            this.btnWprowadzMax = new System.Windows.Forms.Button();
            this.btnSzukaj = new System.Windows.Forms.Button();
            this.lstLokalizacje = new Common.Components.MSMListView();
            this.chLp = new System.Windows.Forms.ColumnHeader();
            this.chLokalizacja = new System.Windows.Forms.ColumnHeader();
            this.chIlosc = new System.Windows.Forms.ColumnHeader();
            this.chPojemnosc = new System.Windows.Forms.ColumnHeader();
            this.lstPodsumowanie = new Common.Components.MSMListView();
            this.chAkapit = new System.Windows.Forms.ColumnHeader();
            this.chSuma = new System.Windows.Forms.ColumnHeader();
            this.chSumaIlosci = new System.Windows.Forms.ColumnHeader();
            this.chSumaPojemnosci = new System.Windows.Forms.ColumnHeader();
            this.lrcNavigation = new Common.Components.LeftRightControl();
            this.btnWybierz = new System.Windows.Forms.Button();
            this.lbWaga = new System.Windows.Forms.Label();
            this.tbWaga = new System.Windows.Forms.TextBox();
            this.pnlNavigation.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlNavigation
            // 
            this.pnlNavigation.Controls.Add(this.btnWybierz);
            this.pnlNavigation.Controls.Add(this.btnPowrot);
            this.pnlNavigation.Controls.Add(this.btnSzukaj);
            this.pnlNavigation.Controls.Add(this.btnWprowadzMax);
            this.pnlNavigation.Location = new System.Drawing.Point(0, 273);
            this.pnlNavigation.Size = new System.Drawing.Size(240, 27);
            // 
            // lbTytul
            // 
            this.lbTytul.BackColor = System.Drawing.Color.Empty;
            this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
            this.lbTytul.CenterAlignX = false;
            this.lbTytul.CenterAlignY = true;
            this.lbTytul.ColorText = System.Drawing.Color.White;
            this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lbTytul.ForeColor = System.Drawing.Color.Empty;
            this.lbTytul.Location = new System.Drawing.Point(0, 0);
            this.lbTytul.Name = "lbTytul";
            this.lbTytul.RectangleColor = System.Drawing.Color.Black;
            this.lbTytul.Size = new System.Drawing.Size(240, 16);
            this.lbTytul.TabIndex = 17;
            this.lbTytul.TabStop = false;
            this.lbTytul.TextDisplayed = "INDEKSY";
            // 
            // tbSymbol
            // 
            this.tbSymbol.Location = new System.Drawing.Point(42, 20);
            this.tbSymbol.Name = "tbSymbol";
            this.tbSymbol.ReadOnly = true;
            this.tbSymbol.Size = new System.Drawing.Size(194, 21);
            this.tbSymbol.TabIndex = 7;
            this.tbSymbol.TabStop = false;
            // 
            // lbSymbol
            // 
            this.lbSymbol.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbSymbol.Location = new System.Drawing.Point(0, 23);
            this.lbSymbol.Name = "lbSymbol";
            this.lbSymbol.Size = new System.Drawing.Size(45, 21);
            this.lbSymbol.Text = "Indeks:";
            // 
            // tbVat
            // 
            this.tbVat.Location = new System.Drawing.Point(202, 92);
            this.tbVat.Name = "tbVat";
            this.tbVat.ReadOnly = true;
            this.tbVat.Size = new System.Drawing.Size(34, 21);
            this.tbVat.TabIndex = 12;
            this.tbVat.TabStop = false;
            // 
            // lbVat
            // 
            this.lbVat.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbVat.Location = new System.Drawing.Point(176, 95);
            this.lbVat.Name = "lbVat";
            this.lbVat.Size = new System.Drawing.Size(37, 21);
            this.lbVat.Text = "VAT:";
            // 
            // tbBranzysta
            // 
            this.tbBranzysta.Location = new System.Drawing.Point(42, 68);
            this.tbBranzysta.Name = "tbBranzysta";
            this.tbBranzysta.ReadOnly = true;
            this.tbBranzysta.Size = new System.Drawing.Size(134, 21);
            this.tbBranzysta.TabIndex = 9;
            this.tbBranzysta.TabStop = false;
            // 
            // lbBranza
            // 
            this.lbBranza.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbBranza.Location = new System.Drawing.Point(0, 71);
            this.lbBranza.Name = "lbBranza";
            this.lbBranza.Size = new System.Drawing.Size(45, 21);
            this.lbBranza.Text = "Bran�a:";
            // 
            // tbNazwaTowaru
            // 
            this.tbNazwaTowaru.Location = new System.Drawing.Point(42, 44);
            this.tbNazwaTowaru.Name = "tbNazwaTowaru";
            this.tbNazwaTowaru.ReadOnly = true;
            this.tbNazwaTowaru.Size = new System.Drawing.Size(194, 21);
            this.tbNazwaTowaru.TabIndex = 8;
            this.tbNazwaTowaru.TabStop = false;
            // 
            // lbNazwa
            // 
            this.lbNazwa.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbNazwa.Location = new System.Drawing.Point(0, 46);
            this.lbNazwa.Name = "lbNazwa";
            this.lbNazwa.Size = new System.Drawing.Size(45, 21);
            this.lbNazwa.Text = "Nazwa:";
            // 
            // tbJm
            // 
            this.tbJm.Location = new System.Drawing.Point(42, 92);
            this.tbJm.Name = "tbJm";
            this.tbJm.ReadOnly = true;
            this.tbJm.Size = new System.Drawing.Size(36, 21);
            this.tbJm.TabIndex = 11;
            this.tbJm.TabStop = false;
            // 
            // lbJm
            // 
            this.lbJm.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbJm.Location = new System.Drawing.Point(0, 96);
            this.lbJm.Name = "lbJm";
            this.lbJm.Size = new System.Drawing.Size(30, 21);
            this.lbJm.Text = "J.m.:";
            // 
            // tbAbc
            // 
            this.tbAbc.Location = new System.Drawing.Point(202, 68);
            this.tbAbc.Name = "tbAbc";
            this.tbAbc.ReadOnly = true;
            this.tbAbc.Size = new System.Drawing.Size(34, 21);
            this.tbAbc.TabIndex = 10;
            this.tbAbc.TabStop = false;
            // 
            // lbAbc
            // 
            this.lbAbc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbAbc.Location = new System.Drawing.Point(176, 71);
            this.lbAbc.Name = "lbAbc";
            this.lbAbc.Size = new System.Drawing.Size(36, 21);
            this.lbAbc.Text = "ABC:";
            // 
            // btnPowrot
            // 
            this.btnPowrot.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnPowrot.Location = new System.Drawing.Point(157, 4);
            this.btnPowrot.Name = "btnPowrot";
            this.btnPowrot.Size = new System.Drawing.Size(80, 20);
            this.btnPowrot.TabIndex = 4;
            this.btnPowrot.Text = "&Esc Powr�t";
            this.btnPowrot.Click += new System.EventHandler(this.OnPowrot);
            // 
            // btnWprowadzMax
            // 
            this.btnWprowadzMax.Enabled = false;
            this.btnWprowadzMax.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnWprowadzMax.Location = new System.Drawing.Point(3, 4);
            this.btnWprowadzMax.Name = "btnWprowadzMax";
            this.btnWprowadzMax.Size = new System.Drawing.Size(108, 20);
            this.btnWprowadzMax.TabIndex = 3;
            this.btnWprowadzMax.Text = "Wprowad� Ma&x";
            this.btnWprowadzMax.Visible = false;
            this.btnWprowadzMax.Click += new System.EventHandler(this.OnWprowadzMax);
            // 
            // btnSzukaj
            // 
            this.btnSzukaj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnSzukaj.Location = new System.Drawing.Point(83, 4);
            this.btnSzukaj.Name = "btnSzukaj";
            this.btnSzukaj.Size = new System.Drawing.Size(71, 20);
            this.btnSzukaj.TabIndex = 2;
            this.btnSzukaj.Text = "&1 Szukaj";
            this.btnSzukaj.Click += new System.EventHandler(this.OnSzukaj);
            // 
            // lstLokalizacje
            // 
            this.lstLokalizacje.Columns.Add(this.chLp);
            this.lstLokalizacje.Columns.Add(this.chLokalizacja);
            this.lstLokalizacje.Columns.Add(this.chIlosc);
            this.lstLokalizacje.Columns.Add(this.chPojemnosc);
            this.lstLokalizacje.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lstLokalizacje.FullRowSelect = true;
            this.lstLokalizacje.Location = new System.Drawing.Point(3, 116);
            this.lstLokalizacje.Name = "lstLokalizacje";
            this.lstLokalizacje.Size = new System.Drawing.Size(234, 134);
            this.lstLokalizacje.TabIndex = 0;
            this.lstLokalizacje.View = System.Windows.Forms.View.Details;
            this.lstLokalizacje.SelectedIndexChanged += new System.EventHandler(this.OnZaznaczonaLokalizacjaChanged);
            // 
            // chLp
            // 
            this.chLp.Text = "Lp";
            this.chLp.Width = 25;
            // 
            // chLokalizacja
            // 
            this.chLokalizacja.Text = "Lokalizacja";
            this.chLokalizacja.Width = 70;
            // 
            // chIlosc
            // 
            this.chIlosc.Text = "Ilo��";
            this.chIlosc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chIlosc.Width = 50;
            // 
            // chPojemnosc
            // 
            this.chPojemnosc.Text = "Pojemno��";
            this.chPojemnosc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chPojemnosc.Width = 71;
            // 
            // lstPodsumowanie
            // 
            this.lstPodsumowanie.Columns.Add(this.chAkapit);
            this.lstPodsumowanie.Columns.Add(this.chSuma);
            this.lstPodsumowanie.Columns.Add(this.chSumaIlosci);
            this.lstPodsumowanie.Columns.Add(this.chSumaPojemnosci);
            this.lstPodsumowanie.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lstPodsumowanie.Location = new System.Drawing.Point(3, 249);
            this.lstPodsumowanie.Name = "lstPodsumowanie";
            this.lstPodsumowanie.Size = new System.Drawing.Size(234, 26);
            this.lstPodsumowanie.TabIndex = 1;
            this.lstPodsumowanie.TabStop = false;
            this.lstPodsumowanie.View = System.Windows.Forms.View.Details;
            // 
            // chAkapit
            // 
            this.chAkapit.Text = "";
            this.chAkapit.Width = 32;
            // 
            // chSuma
            // 
            this.chSuma.Text = "Suma";
            this.chSuma.Width = 60;
            // 
            // chSumaIlosci
            // 
            this.chSumaIlosci.Text = "";
            this.chSumaIlosci.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chSumaIlosci.Width = 55;
            // 
            // chSumaPojemnosci
            // 
            this.chSumaPojemnosci.Text = "";
            this.chSumaPojemnosci.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.chSumaPojemnosci.Width = 71;
            // 
            // lrcNavigation
            // 
            this.lrcNavigation.BackColor = System.Drawing.SystemColors.Desktop;
            this.lrcNavigation.Location = new System.Drawing.Point(208, 0);
            this.lrcNavigation.Name = "lrcNavigation";
            this.lrcNavigation.NextEnabled = true;
            this.lrcNavigation.PreviousEnabled = true;
            this.lrcNavigation.Size = new System.Drawing.Size(32, 16);
            this.lrcNavigation.TabIndex = 1;
            this.lrcNavigation.TabStop = false;
            this.lrcNavigation.Next += new System.EventHandler(this.OnNastepny);
            this.lrcNavigation.Previous += new System.EventHandler(this.OnPoprzedni);
            // 
            // btnWybierz
            // 
            this.btnWybierz.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.btnWybierz.Location = new System.Drawing.Point(157, 4);
            this.btnWybierz.Name = "btnWybierz";
            this.btnWybierz.Size = new System.Drawing.Size(80, 20);
            this.btnWybierz.TabIndex = 5;
            this.btnWybierz.Text = "Ret Wybierz";
            this.btnWybierz.Click += new System.EventHandler(this.OnWybierz);
            // 
            // lbWaga
            // 
            this.lbWaga.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbWaga.Location = new System.Drawing.Point(80, 96);
            this.lbWaga.Name = "lbWaga";
            this.lbWaga.Size = new System.Drawing.Size(62, 16);
            this.lbWaga.Text = "Waga(kg):";
            // 
            // tbWaga
            // 
            this.tbWaga.Location = new System.Drawing.Point(134, 92);
            this.tbWaga.Name = "tbWaga";
            this.tbWaga.ReadOnly = true;
            this.tbWaga.Size = new System.Drawing.Size(42, 21);
            this.tbWaga.TabIndex = 20;
            this.tbWaga.TabStop = false;
            // 
            // IndeksView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.Controls.Add(this.tbWaga);
            this.Controls.Add(this.lbWaga);
            this.Controls.Add(this.lrcNavigation);
            this.Controls.Add(this.lstPodsumowanie);
            this.Controls.Add(this.lstLokalizacje);
            this.Controls.Add(this.tbAbc);
            this.Controls.Add(this.lbAbc);
            this.Controls.Add(this.tbJm);
            this.Controls.Add(this.lbJm);
            this.Controls.Add(this.tbNazwaTowaru);
            this.Controls.Add(this.lbNazwa);
            this.Controls.Add(this.tbBranzysta);
            this.Controls.Add(this.lbBranza);
            this.Controls.Add(this.tbVat);
            this.Controls.Add(this.lbVat);
            this.Controls.Add(this.tbSymbol);
            this.Controls.Add(this.lbSymbol);
            this.Controls.Add(this.lbTytul);
            this.Name = "IndeksView";
            this.Size = new System.Drawing.Size(240, 300);
            this.Controls.SetChildIndex(this.pnlNavigation, 0);
            this.Controls.SetChildIndex(this.lbTytul, 0);
            this.Controls.SetChildIndex(this.lbSymbol, 0);
            this.Controls.SetChildIndex(this.tbSymbol, 0);
            this.Controls.SetChildIndex(this.lbVat, 0);
            this.Controls.SetChildIndex(this.tbVat, 0);
            this.Controls.SetChildIndex(this.lbBranza, 0);
            this.Controls.SetChildIndex(this.tbBranzysta, 0);
            this.Controls.SetChildIndex(this.lbNazwa, 0);
            this.Controls.SetChildIndex(this.tbNazwaTowaru, 0);
            this.Controls.SetChildIndex(this.lbJm, 0);
            this.Controls.SetChildIndex(this.tbJm, 0);
            this.Controls.SetChildIndex(this.lbAbc, 0);
            this.Controls.SetChildIndex(this.tbAbc, 0);
            this.Controls.SetChildIndex(this.lstLokalizacje, 0);
            this.Controls.SetChildIndex(this.lstPodsumowanie, 0);
            this.Controls.SetChildIndex(this.lrcNavigation, 0);
            this.Controls.SetChildIndex(this.lbWaga, 0);
            this.Controls.SetChildIndex(this.tbWaga, 0);
            this.pnlNavigation.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private MSMLabel lbTytul;
		private System.Windows.Forms.TextBox tbSymbol;
		private System.Windows.Forms.Label lbSymbol;
		private System.Windows.Forms.TextBox tbVat;
		private System.Windows.Forms.Label lbVat;
		private System.Windows.Forms.TextBox tbBranzysta;
		private System.Windows.Forms.Label lbBranza;
		private System.Windows.Forms.TextBox tbNazwaTowaru;
		private System.Windows.Forms.Label lbNazwa;
		private System.Windows.Forms.TextBox tbJm;
		private System.Windows.Forms.Label lbJm;
		private System.Windows.Forms.TextBox tbAbc;
		private System.Windows.Forms.Label lbAbc;
		private System.Windows.Forms.Button btnPowrot;
		private System.Windows.Forms.Button btnWprowadzMax;
        private System.Windows.Forms.Button btnSzukaj;
		private MSMListView lstLokalizacje;
		private System.Windows.Forms.ColumnHeader chLp;
		private System.Windows.Forms.ColumnHeader chLokalizacja;
		private System.Windows.Forms.ColumnHeader chIlosc;
		private System.Windows.Forms.ColumnHeader chPojemnosc;
		private MSMListView lstPodsumowanie;
		private System.Windows.Forms.ColumnHeader chSuma;
		private System.Windows.Forms.ColumnHeader chSumaIlosci;
		private System.Windows.Forms.ColumnHeader chSumaPojemnosci;
		private System.Windows.Forms.ColumnHeader chAkapit;
		private LeftRightControl lrcNavigation;
        private System.Windows.Forms.Button btnWybierz;
		private System.Windows.Forms.Label lbWaga;
        private System.Windows.Forms.TextBox tbWaga;
	}
}
