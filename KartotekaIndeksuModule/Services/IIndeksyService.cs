using System;
using System.Collections.Generic;
using System.Text;
using KartotekaIndeksuModule.KartotekaIndeksuProxy;

namespace KartotekaIndeksuModule
{
    public interface IIndeksyService
    {
        ListaIndeksow PobierzListeIndeksow(KartotekaIndeksuModule.Search.KryteriaZapytaniaIndeksu kryteriaZapytania, int? numerStrony);
		ListaIndeksow PobierzIndeks(long idPozycjiDokumentuMagazynowego);
		void ZmienWageIndeksu(long id, double waga);
    }
}
