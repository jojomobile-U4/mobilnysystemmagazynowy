#region

using System;
using Common;
using Common.Base;
using Common.DataModel;
using KartotekaIndeksuModule.KartotekaIndeksuProxy;
using KartotekaIndeksuModule.Search;
using KartotekaIndeksuModule.WebServiceWrap;
using Microsoft.Practices.Mobile.CompositeUI;
using StatusOperacji=KartotekaIndeksuModule.KartotekaIndeksuProxy.StatusOperacji;

#endregion

namespace KartotekaIndeksuModule
{
    [Service(typeof (IIndeksyService))]
    public class IndeksyService : ServiceBase, IIndeksyService
    {
        #region Properties

        protected override WebServiceConfiguration Configuration
        {
            get { return MyWorkItem.Configuration.KartotekaIndeksuWS; }
        }

        #endregion

        #region Constructors

        public IndeksyService([ServiceDependency] WorkItem workItem) : base(workItem)
        {
        }

        #endregion

        #region Methods

        public ListaIndeksow PobierzListeIndeksow(KryteriaZapytaniaIndeksu kryteriaZapytania, int? numerStrony)
        {
            var lista = new ListaIndeksow();
            try
            {
                using (var serviceAgent = new WsKartotekaIndeksuWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);

                    var param = new pobierzListeIndeksowWrap {branza = kryteriaZapytania.Branzysta, nazwaTowaru = kryteriaZapytania.Nazwa, numerStrony = numerStrony ?? 0, symbol = kryteriaZapytania.Symbol, wielkoscStrony = Configuration.WielkoscStrony};

                    //TODO: dodac DataModel
                    lista = serviceAgent.pobierzListeIndeksow(param).@return;

                    // wyciecie prefiksu dla lokalizacji
                    if (!string.IsNullOrEmpty(Constants.LOCALIZATION_PREFIX) &&
                        lista.listaIndeksow != null)
                    {
                        //var dlugosc = Constants.LOCALIZATION_PREFIX.Length;
                        foreach (var indeks in lista.listaIndeksow)
                        {
                            if (indeks.lokalizacje != null)
                            {
                                foreach (var miejsce in indeks.lokalizacje)
                                {
                                    miejsce.lokalizacja = Localization.RemovePrefix(miejsce.lokalizacja);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //TODO: dodac zapisywanie do logow
                lista.statusOperacji = new StatusOperacji {status = Common.DataModel.StatusOperacji.ERROR, tekst = ex.Message};
            }
            return lista;
        }

        public ListaIndeksow PobierzIndeks(long idPozycjiDokumentuMagazynowego)
        {
            var listaIndeksow = new ListaIndeksow();
            try
            {
                using (var serviceAgent = new WsKartotekaIndeksuWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);

                    var param = new pobierzIndeksDlaPozycjiDokumentu {IdPozycjiDokumentuMagazynowego = idPozycjiDokumentuMagazynowego};

                    listaIndeksow = serviceAgent.pobierzIndeksDlaPozycjiDokumentu(param).@return;
                }
            }
            catch (Exception ex)
            {
                //TODO: dodac zapisywanie do logow
                listaIndeksow.statusOperacji = new StatusOperacji {status = Common.DataModel.StatusOperacji.ERROR, tekst = ex.Message};
            }
            return listaIndeksow;
        }

        public void ZmienWageIndeksu(long id, double waga)
        {
            try
            {
                using (var serviceAgent = new WsKartotekaIndeksuWrap(Configuration))
                {
                    SecureSoapMessage(serviceAgent);
                    var param = new zmienWageIndeksuWrap {id = id, waga = waga};

                    var statusOperacji = serviceAgent.zmienWageIndeksu(param).@return;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = statusOperacji.status;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = statusOperacji.tekst;
                    MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = statusOperacji.stosWywolan;
                }
            }
            catch (Exception e)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS] = Common.DataModel.StatusOperacji.ERROR;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] = e.StackTrace;
                MyWorkItem.RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] = e.Message;
            }
        }

        #endregion
    }
}