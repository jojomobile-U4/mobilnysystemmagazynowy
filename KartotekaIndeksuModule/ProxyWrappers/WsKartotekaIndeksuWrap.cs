#region

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using Common;
using Common.Base;
using KartotekaIndeksuModule.KartotekaIndeksuProxy;
using OpenNETCF.Web.Services2;

#endregion

namespace KartotekaIndeksuModule.WebServiceWrap
{
    public class WsKartotekaIndeksuWrap : WsKartotekaIndeksu, IWSSecurity
    {
        #region Private Fields

        #endregion

        #region Properties

        public SecurityHeader SecurityHeader { get; set; }

        #endregion

        #region Constructors

        public WsKartotekaIndeksuWrap(WebServiceConfiguration configuration)
        {
            Url = configuration.Url;
        }

        #endregion

        #region Protected Methods

        protected override WebRequest GetWebRequest(Uri uri)
        {
            var request = (HttpWebRequest)base.GetWebRequest(uri);

            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;

            return request;
        }

        #endregion

        #region Methods

        [SoapDocumentMethod("urn:pobierzListeIndeksow", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzListeIndeksowResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzListeIndeksowResponse pobierzListeIndeksow([XmlElement("pobierzListeIndeksow", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzListeIndeksowWrap pobierzListeIndeksow1)
        {
            var results = Invoke("pobierzListeIndeksow", new object[] { pobierzListeIndeksow1 });
            return ((pobierzListeIndeksowResponse)(results[0]));
        }

        [SoapDocumentMethod("urn:pobierzIndeksDlaPozycjiDokumentu", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElement("pobierzIndeksDlaPozycjiDokumentuResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public pobierzIndeksDlaPozycjiDokumentuResponse pobierzIndeksDlaPozycjiDokumentu([XmlElement("pobierzIndeksDlaPozycjiDokumentu", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] pobierzIndeksDlaPozycjiDokumentu pobierzIndeksDlaPozycjiDokumentu1)
        {
            var results = Invoke("pobierzIndeksDlaPozycjiDokumentu", new object[] { pobierzIndeksDlaPozycjiDokumentu1 });
            return ((pobierzIndeksDlaPozycjiDokumentuResponse)(results[0]));
        }

        [SoapDocumentMethodAttribute("urn:zmienWageIndeksu", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlElementAttribute("zmienWageIndeksuResponse", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")]
        [SoapHeader("SecurityHeader")]
        public zmienWageIndeksuResponse zmienWageIndeksu([XmlElementAttribute("zmienWageIndeksu", Namespace = "http://mobilnySystemMagazynowy.teta.com/xsd")] zmienWageIndeksuWrap zmienWageIndeksu1)
        {
            object[] results = Invoke("zmienWageIndeksu", new object[] {
                        zmienWageIndeksu1});
            return ((zmienWageIndeksuResponse)(results[0]));
        }

        #endregion
    }

    /// <remarks/>
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    public class pobierzListeIndeksowWrap : pobierzListeIndeksow
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string symbol
        {
            get { return base.symbol; }
            set { base.symbol = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string nazwaTowaru
        {
            get { return base.nazwaTowaru; }
            set { base.nazwaTowaru = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public string branza
        {
            get { return base.branza; }
            set { base.branza = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? numerStrony
        {
            get { return base.numerStrony; }
            set { base.numerStrony = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public int? wielkoscStrony
        {
            get { return base.wielkoscStrony; }
            set { base.wielkoscStrony = value; }
        }
    }

    /// <remarks/>
    /// <remarks/>
    [DebuggerStepThrough]
    [DesignerCategory("code")]
    public class zmienWageIndeksuWrap : zmienWageIndeksu
    {
        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public long? id
        {
            get { return base.id; }
            set { base.id = value; }
        }

        /// <remarks/>
        [XmlElement(IsNullable = true)]
        public double? waga
        {
            get { return base.waga; }
            set { base.waga = value; }
        }
    }
}