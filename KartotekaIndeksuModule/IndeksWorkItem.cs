#region

using System;
using System.Collections.Generic;
using Common;
using Common.Base;
using Common.MaxCountEdit;
using KartotekaIndeksuModule.KartotekaIndeksuProxy;
using KartotekaIndeksuModule.Search;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using StatusOperacji=Common.DataModel.StatusOperacji;

#endregion

namespace KartotekaIndeksuModule
{
    public class IndeksWorkItem : WorkItemBase
    {
        #region Event subscription

        [EventSubscription(EventBrokerConstants.BARCODE_HAS_BEEN_READ)]
        public void BarCodeRead(object sender, EventArgs e)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var presenter = Items.Get<IndeksViewPresenter>(ItemsConstants.INDEKS_PRESENTER);
                if (Status == WorkItemStatus.Active &&
                    RootWorkItem.State[StateConstants.BAR_CODE] != null)
                {
                    var odczyt = RootWorkItem.State[StateConstants.BAR_CODE].ToString();

                    if ((odczyt != null) && (odczyt.IndexOf("WAG", 0) == 0))
                    {
                        FunctionsHelper.SetCursorWait();

                        var masa = Services.Get<IWagiService>(true).pobierzOdczytWagi(odczyt);

                        if (masa.statusOperacji.status.Equals(StatusOperacji.ERROR))
                        {
                            ShowMessageBox(masa.statusOperacji.tekst, BLAD, masa.statusOperacji.stosWywolan);
                            return;
                        }
                        else
                        {
                            Services.Get<IWagiService>(true).przeliczNaKg(ref masa);
                            presenter.WczytajMase(masa);
                        }
                    }
                    else
                    {
                        var idPozycji = Konwertuj(odczyt);
                        if (idPozycji != null)
                        {
                            FunctionsHelper.SetCursorWait();
                            var lista = Services.Get<IIndeksyService>(true).PobierzIndeks((long) idPozycji);
                            if (lista.statusOperacji.status.Equals(StatusOperacji.ERROR))
                            {
                                ShowMessageBox(lista.statusOperacji.tekst, BLAD, lista.statusOperacji.stosWywolan);
                                return;
                            }

                            if (lista.listaIndeksow != null && lista.listaIndeksow.Length > 0)
                            {
                                presenter.ZaladujDaneDoWidoku(lista.listaIndeksow[0]);

                                MainWorkspace.Show(presenter.View);
                            }
                        }
                    }
                }
            }
            finally
            {
                Activate();
                FunctionsHelper.SetCursorDefault();
            }
        }

        [EventSubscription(EventBrokerConstants.WYSZUKAJ_INDEKSY)]
        public void OnSzukaj(object sender, EventArgs e)
        {
            // poniewaz rzutowania stosowanie DataEventArgs<Kryteria..>> rzuca wyjatek
            // zastosowano eventHandler'a, a argument przekazywany przez State
            //State["Kryteria"] = e;

            //pobieramy zawsze pierwsza strone
            var indeksPresenter = Items.Get<IndeksViewPresenter>(ItemsConstants.INDEKS_PRESENTER);
            indeksPresenter.ResetujStronicowanie();
            indeksPresenter.ZaladujDaneDoWidoku(PobierzListeIndeksow(null));
            indeksPresenter.View.SetNavigationState(true);
            MainWorkspace.Show(indeksPresenter.View);
        }

        #endregion

        #region Show methods

        public void Show()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var indeksViewPresenter = Items.Get<IndeksViewPresenter>(ItemsConstants.INDEKS_PRESENTER);

                if ((RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] != null)
                    && (RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS].ToString().Equals("T")))
                {
                    var kryteriaZapytania = new KryteriaZapytaniaIndeksu();
                    kryteriaZapytania.Symbol = RootWorkItem.State[StateConstants.SYMBOL_TOWARU_DLA_KARTOTEKI_INDEKSU].ToString();
                    State[StateConstants.CRITERIAS] = kryteriaZapytania;
                    OnSzukaj(indeksViewPresenter.View, EventArgs.Empty);
                }
                else
                {
                    MainWorkspace.Show(indeksViewPresenter.View);
                }
                indeksViewPresenter.UstawWlasciwosciPrzyciskow();
                this.Activate();
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        #endregion

        #region Methods

        public List<Indeks> PobierzListeIndeksow(int? numerStrony)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var lista = Services.Get<IIndeksyService>(true).PobierzListeIndeksow(
                    State[StateConstants.CRITERIAS] as KryteriaZapytaniaIndeksu, numerStrony);

                if (lista.statusOperacji.status.Equals(StatusOperacji.ERROR))
                {
                    ShowMessageBox(lista.statusOperacji.tekst, BLAD, lista.statusOperacji.stosWywolan);
                    return new List<Indeks>();
                }

                var wynik = new List<Indeks>();
                if (lista.listaIndeksow != null)
                    wynik.AddRange(lista.listaIndeksow);

                return wynik;
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void Szukaj()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var searchPresenter = Items.Get<IndeksSearchViewPresenter>(ItemsConstants.INDEKS_SEARCH_PRESENTER);
                //czyszczenie zapamietanego stanu
                searchPresenter.View.Kryteria = null;

                Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                MainWorkspace.Show(searchPresenter.View);
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public void WprowadzMax(string symbol, string lokalizacja)
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                var maxCountViewPresenter = Items.Get<MaxCountViewPresenter>(ItemsConstants.INDEKS_MAX_COUNT_PRESENTER);
                maxCountViewPresenter.View.IndexSymbol = symbol;
                maxCountViewPresenter.View.Localization = lokalizacja;

                //TODO: pobrac aktualny max
                maxCountViewPresenter.View.MaxCountValue = string.Empty;

                Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                MainWorkspace.Show(maxCountViewPresenter.View);
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        public bool ZapiszZmianeWagi(Indeks indeks)
        {
            if (indeks != null)
            {
                try
                {
                    FunctionsHelper.SetCursorWait();

                    Services.Get<IIndeksyService>(true).ZmienWageIndeksu(indeks.id, indeks.waga);

                    FunctionsHelper.SetCursorDefault();
                    if (!RootWorkItem.State[StateConstants.OPERATION_RESULT_STATUS].Equals(StatusOperacji.SUCCESS))
                    {
                        if (RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE] != null
                            && RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT] != null)
                        {
                            ShowMessageBox(RootWorkItem.State[StateConstants.OPERATION_RESULT_TEXT].ToString(), "Indeksy",
                                           RootWorkItem.State[StateConstants.OPERATION_RESULT_STACKTRACE].ToString());
                        }
                        return false;
                    }
                }
                finally
                {
                    FunctionsHelper.SetCursorDefault();
                }
            }
            return true;
        }

        #endregion
    }
}