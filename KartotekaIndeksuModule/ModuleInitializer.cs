#region

using System;
using Common;
using Common.MaxCountEdit;
using KartotekaIndeksuModule.Search;
using Microsoft.Practices.Mobile.CompositeUI;
using Microsoft.Practices.Mobile.CompositeUI.Commands;

#endregion

namespace KartotekaIndeksuModule
{
    public class ModuleInitializer : ModuleInit
    {
        private readonly WorkItem m_WorkItem;

        public ModuleInitializer([ServiceDependency] WorkItem workItem)
        {
            m_WorkItem = workItem;
        }

        private void Initialize()
        {
            try
            {
                FunctionsHelper.SetCursorWait();

                if (!m_WorkItem.WorkItems.Contains(WorkItemsConstants.INDEKS_WORKITEM))
                {
                    var indeksWorkItem = m_WorkItem.WorkItems.AddNew<IndeksWorkItem>(WorkItemsConstants.INDEKS_WORKITEM);
                    indeksWorkItem.State[StateConstants.CRITERIAS] = new KryteriaZapytaniaIndeksu();

                    var searchPresenter = new IndeksSearchViewPresenter(indeksWorkItem.Items.AddNew<IndeksSearchView>());
                    indeksWorkItem.Items.Add(searchPresenter, ItemsConstants.INDEKS_SEARCH_PRESENTER);

                    var indeksViewPresenter = new IndeksViewPresenter(indeksWorkItem.Items.AddNew<IndeksView>());
                    indeksWorkItem.Items.Add(indeksViewPresenter, ItemsConstants.INDEKS_PRESENTER);

                    var maxCountViewPresenter = new MaxCountViewPresenter(indeksWorkItem.Items.AddNew<MaxCountView>());
                    indeksWorkItem.Items.Add(maxCountViewPresenter, ItemsConstants.INDEKS_MAX_COUNT_PRESENTER);

                    maxCountViewPresenter.MaxCountValueChanged += indeksViewPresenter.ZmienIloscMaksymalna;

                    if (!m_WorkItem.Services.Contains(typeof (IWagiService)))
                    {
                        m_WorkItem.Services.AddNew(typeof (WagiService), typeof (IWagiService));
                    }
                }

                m_WorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
            }
            finally
            {
                FunctionsHelper.SetCursorDefault();
            }
        }

        [CommandHandler(CommandConstants.KARTOTEKA_INDEKSU)]
        public void KartotekaIndeksu(object sender, EventArgs e)
        {
            Initialize();
            var indeksWorkItem = m_WorkItem.WorkItems.Get<IndeksWorkItem>(WorkItemsConstants.INDEKS_WORKITEM);
            indeksWorkItem.Show();
        }
    }
}