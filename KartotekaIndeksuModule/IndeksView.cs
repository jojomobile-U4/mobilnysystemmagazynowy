#region

using System;
using System.Data;
using System.Windows.Forms;
using Common.Base;
using KartotekaIndeksuModule.KartotekaIndeksuProxy;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace KartotekaIndeksuModule
{
    [SmartPart]
    public partial class IndeksView : ViewBase, IIndeksView
    {
        #region Constructors

        private readonly BindingSource m_IndeksBindingSource = new BindingSource();
        private readonly DataTable m_IndeksDataTable = new DataTable();

        public IndeksView()
        {
            InitializeComponent();
            InitializeFocusedControl();
            InitializeBindingSources();
        }

        // GUI agnostic events listened by the presenter.
        // These can use EventHandler<TEventArgs> in combination 
        // with DataEventArgs<TData> to avoid proliferation of 
        // delegate types.
        // Raised events must be 'published' to the presenter 
        // through the IIndeksView interface.

        #endregion

        #region Private Fields

        private readonly Indeks m_Empty = new Indeks();

        #endregion

        #region Protected Methods

        protected void OnSzukaj(object sender, EventArgs e)
        {
            if (Szukaj != null)
            {
                Szukaj(sender, e);
            }
        }

        protected void OnWprowadzMax(object sender, EventArgs e)
        {
            if (WprowadzMax != null)
            {
                WprowadzMax(sender, e);
            }
        }

        protected void OnPowrot(object sender, EventArgs e)
        {
            if (Powrot != null)
            {
                Powrot(sender, e);
            }
        }

        protected void OnPoprzedni(object sender, EventArgs e)
        {
            if (Poprzedni != null)
            {
                Poprzedni(sender, e);
            }
        }

        protected void OnNastepny(object sender, EventArgs e)
        {
            if (Nastepny != null)
            {
                Nastepny(sender, e);
            }
        }

        protected void OnZaznaczonaLokalizacjaChanged(object sender, EventArgs e)
        {
            if (ZaznaczonaLokalizacjaChanged != null)
            {
                ZaznaczonaLokalizacjaChanged(sender, e);
            }
        }

        private void OnWybierz(object sender, EventArgs e)
        {
            if (Wybierz != null)
            {
                Wybierz(sender, e);
            }
        }

        private void SetDataSource()
        {
            m_IndeksDataTable.Clear();

            var dataRowIndeks = m_IndeksDataTable.NewRow();

            var indeks = m_IndeksBindingSource.DataSource as Indeks;
            if (indeks == null) return;

            dataRowIndeks["Symbol"] = indeks.symbol;
            dataRowIndeks["NazwaTowaru"] = indeks.nazwaTowaru;
            dataRowIndeks["Branzysta"] = indeks.branzysta;
            dataRowIndeks["Abc"] = indeks.abc;
            dataRowIndeks["Jm"] = indeks.jm;
            dataRowIndeks["Waga"] = indeks.waga;
            dataRowIndeks["Vat"] = indeks.vat;

            m_IndeksDataTable.Rows.Add(dataRowIndeks);
        }

        #endregion

        #region IIndeksView Members

        public event EventHandler Szukaj;

        public event EventHandler WprowadzMax;

        public event EventHandler Powrot;

        public event EventHandler Nastepny;

        public event EventHandler Poprzedni;

        public event EventHandler ZaznaczonaLokalizacjaChanged;

        public event EventHandler Wybierz;

        public Indeks DataSource
        {
            set
            {
                m_IndeksBindingSource.DataSource = value ?? m_Empty;
                SetDataSource();
            }
        }

        public bool PowrotEnabled
        {
            set { btnPowrot.Enabled = value; }
        }

        public bool PowrotVisible
        {
            set { btnPowrot.Visible = value; }
        }

        public bool WprowadzMaxEnabled
        {
            set { btnWprowadzMax.Enabled = value; }
        }

        public bool WybierzEnabled
        {
            set { btnWybierz.Enabled = value; }
        }

        public bool WybierzVisible
        {
            set { btnWybierz.Visible = value; }
        }

        public bool PoprzedniEnabled
        {
            get { return lrcNavigation.PreviousEnabled; }
            set { lrcNavigation.PreviousEnabled = value; }
        }

        public bool NastepnyEnabled
        {
            get { return lrcNavigation.NextEnabled; }
            set { lrcNavigation.NextEnabled = value; }
        }

        public ListView.ListViewItemCollection LokalizacjeDataSource
        {
            get { return lstLokalizacje.Items; }
        }

        public ListView.ListViewItemCollection PodsumowanieDataSource
        {
            get { return lstPodsumowanie.Items; }
        }

        public Miejsce ZaznaczonaLokalizacja
        {
            get
            {
                if (lstLokalizacje.SelectedIndices.Count == 0)
                {
                    return null;
                }

                return lstLokalizacje.Items[lstLokalizacje.SelectedIndices[0]].Tag as Miejsce;
            }
        }

        public void UstawWage(string waga)
        {
            tbWaga.Text = waga;
        }

        public bool WagaReadOnly
        {
            get { return tbWaga.ReadOnly; }

            set { tbWaga.ReadOnly = value; }
        }

        public void WagaSetFocus()
        {
            tbWaga.Focus();
            tbWaga.SelectAll();
        }

        public double Waga
        {
            get
            {
                return Common.FunctionsHelper.ConvertToDouble(tbWaga.Text);
            }
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);

            tbWaga.ReadOnly = navigationState;
        }

        #endregion

        public void InitializeBindingSources()
        {
            var column = new DataColumn { DataType = Type.GetType("System.String"), ColumnName = "Symbol", ReadOnly = true, Unique = false };
            m_IndeksDataTable.Columns.Add(column);

            column = new DataColumn { DataType = Type.GetType("System.String"), ColumnName = "NazwaTowaru", ReadOnly = true, Unique = false };
            m_IndeksDataTable.Columns.Add(column);

            column = new DataColumn { DataType = Type.GetType("System.String"), ColumnName = "Branzysta", ReadOnly = true, Unique = false };
            m_IndeksDataTable.Columns.Add(column);

            column = new DataColumn { DataType = Type.GetType("System.String"), ColumnName = "Abc", ReadOnly = true, Unique = false };
            m_IndeksDataTable.Columns.Add(column);

            column = new DataColumn { DataType = Type.GetType("System.String"), ColumnName = "Jm", ReadOnly = true, Unique = false };
            m_IndeksDataTable.Columns.Add(column);

            column = new DataColumn { DataType = Type.GetType("System.String"), ColumnName = "Waga", ReadOnly = true, Unique = false };
            m_IndeksDataTable.Columns.Add(column);

            column = new DataColumn { DataType = Type.GetType("System.String"), ColumnName = "Vat", ReadOnly = true, Unique = false };
            m_IndeksDataTable.Columns.Add(column);

            m_IndeksDataTable.Columns.Add(new DataColumn());
            
            tbSymbol.DataBindings.Add(new Binding("Text", m_IndeksDataTable, "Symbol"));
            tbNazwaTowaru.DataBindings.Add(new Binding("Text", m_IndeksDataTable, "NazwaTowaru"));
            tbBranzysta.DataBindings.Add(new Binding("Text", m_IndeksDataTable, "Branzysta"));
            tbAbc.DataBindings.Add(new Binding("Text", m_IndeksDataTable, "Abc"));
            tbJm.DataBindings.Add(new Binding("Text", m_IndeksDataTable, "Jm"));
            tbWaga.DataBindings.Add(new Binding("Text", m_IndeksDataTable, "Waga"));
            tbVat.DataBindings.Add(new Binding("Text", m_IndeksDataTable, "Vat"));

        }
    }
}