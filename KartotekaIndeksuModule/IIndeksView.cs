#region

using System;
using System.Windows.Forms;
using Common.Base;
using KartotekaIndeksuModule.KartotekaIndeksuProxy;

#endregion

namespace KartotekaIndeksuModule
{
    public interface IIndeksView : IViewBase
    {
        Indeks DataSource { set; }

        ListView.ListViewItemCollection LokalizacjeDataSource { get; }

        ListView.ListViewItemCollection PodsumowanieDataSource { get; }

        bool PoprzedniEnabled { get; set; }

        bool NastepnyEnabled { get; set; }

        bool PowrotEnabled { set; }

        bool PowrotVisible { set; }

        bool WprowadzMaxEnabled { set; }

        bool WybierzEnabled { set; }

        bool WybierzVisible { set; }

        Miejsce ZaznaczonaLokalizacja { get; }

        bool WagaReadOnly { get; set; }
        event EventHandler Szukaj;
        event EventHandler WprowadzMax;
        event EventHandler Powrot;
        event EventHandler Nastepny;
        event EventHandler Poprzedni;
        event EventHandler ZaznaczonaLokalizacjaChanged;
        event EventHandler Wybierz;
        void UstawWage(string waga);
        void WagaSetFocus();
        double Waga { get; }
    }
}