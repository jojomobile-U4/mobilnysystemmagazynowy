#region

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Common;
using Common.Base;
using Common.WagiProxy;
using KartotekaIndeksuModule.KartotekaIndeksuProxy;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;
using Microsoft.Practices.Mobile.CompositeUI.Utility;

#endregion

namespace KartotekaIndeksuModule
{
    public class IndeksViewPresenter : PresenterBase
    {
        #region Constants

        private const int NASTEPNA = 1;
        private const int PIERWSZA = 0;
        private const int POPRZEDNIA = -1;
        private const string SUM = "Suma:";
        private const string WZORZEC_LICZBY_RZECZYWISTEJ = "#0.##";

        #endregion

        #region Public Fields

        [EventPublication(EventBrokerConstants.WYBRANO_LOKALIZACJE, PublicationScope.Global)]
        public event EventHandler WybranoLokalizacje;

        #endregion

        #region Private Fields

        private Indeks aktualnyIndeks;
        private List<Indeks> listaIndeksow;
        private Miejsce modyfikowanaLokalizacja;
        private int numerStrony;
        private int zmianaStrony;

        #endregion

        #region Contructors

        public IndeksViewPresenter(IIndeksView view) : base(view)
        {
            listaIndeksow = new List<Indeks>();
            numerStrony = PIERWSZA;
            zmianaStrony = NASTEPNA;
        }

        #endregion

        #region Properties

        public IIndeksView View
        {
            get { return m_view as IIndeksView; }
        }

        protected IndeksWorkItem MyWorkItem
        {
            get { return WorkItem as IndeksWorkItem; }
        }

        #endregion

        #region Methods

        public void ZaladujDaneDoWidoku(Indeks indeks)
        {
            this.listaIndeksow = new List<Indeks>();
            aktualnyIndeks = indeks;
            AktualizujWidok();
        }

        public void ZaladujDaneDoWidoku(List<Indeks> listaIndeksow)
        {
            if (listaIndeksow == null ||
                listaIndeksow.Count == 0)
            {
                this.listaIndeksow = new List<Indeks>();
                aktualnyIndeks = null;
            }
            else
            {
                this.listaIndeksow = listaIndeksow;
                if (zmianaStrony == POPRZEDNIA)
                {
                    aktualnyIndeks = this.listaIndeksow[listaIndeksow.Count - 1];
                }
                else
                {
                    aktualnyIndeks = this.listaIndeksow[0];
                }
            }
            AktualizujWidok();
        }

        public void ZmienIloscMaksymalna(object sender, DataEventArgs<float> e)
        {
            if (modyfikowanaLokalizacja != null)
            {
                var aktualizowanyIndeks = aktualnyIndeks.symbol;
                ZaladujDaneDoWidoku(MyWorkItem.PobierzListeIndeksow(numerStrony));
                aktualnyIndeks = GetIndeks(aktualizowanyIndeks);
                AktualizujWidok();

                //Wyczyszczenie referenci, poniewaz zamknieto okno edycji
                modyfikowanaLokalizacja = null;

                //HACK: poniewaz zdarzenie zmiany zaznaczonej lokalizacji nie odpalane przy stracie focusu
                View.WprowadzMaxEnabled = false;
            }
        }

        internal void ResetujStronicowanie()
        {
            numerStrony = PIERWSZA;
            zmianaStrony = NASTEPNA;
        }

        #endregion

        #region Obsluga widoku

        private void ViewNastepny(object sender, EventArgs e)
        {
            //sprawdzenie czy nie jestesmy na ostatniej stronie
            if (aktualnyIndeks != null)
            {
                var indeksAktualnego = listaIndeksow.IndexOf(aktualnyIndeks);
                if (indeksAktualnego < listaIndeksow.Count - 1)
                {
                    aktualnyIndeks = listaIndeksow[indeksAktualnego + 1];
                    AktualizujWidok();
                }
                else
                {
                    //TODO: sprawdzic czy jest nastepna paczka (strona)

                    //jezeli nie ma indeksow, znaczy ze nie ma dalszych stron
                    if (listaIndeksow.Count == MyWorkItem.Configuration.KartotekaIndeksuWS.WielkoscStrony)
                    {
                        ZmienStrone(NASTEPNA);
                    }
                }
            }
        }

        private void ViewPoprzedni(object sender, EventArgs e)
        {
            if (aktualnyIndeks != null)
            {
                var indeksAktualnego = listaIndeksow.IndexOf(aktualnyIndeks);
                if (indeksAktualnego > 0)
                {
                    aktualnyIndeks = listaIndeksow[indeksAktualnego - 1];
                    AktualizujWidok();
                }
                else
                {
                    //TODO: sprawdzic czy jest poprzednia paczka (strona)

                    //jezeli numer strony = 0, znaczy ze nie ma wczesniejszych stron
                    if (numerStrony > 0)
                    {
                        ZmienStrone(POPRZEDNIA);
                    }
                }
            }
        }

        private void ViewPowrot(object sender, EventArgs e)
        {
            if ((MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] != null)
                && (MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS].ToString().Equals("T"))
                && (View.ZaznaczonaLokalizacja != null))
            {
                WorkItemBase.RootWorkItem.State[StateConstants.LOKALIZACJA_Z_KARTOTEKI_INDEKSU] = View.ZaznaczonaLokalizacja.lokalizacja;
                if (WybranoLokalizacje != null)
                {
                    WybranoLokalizacje(this, null);
                }
                MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] = StateConstants.BoolValueNo;
                MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = null;
                MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;

            }

            MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = StateConstants.BoolValueNo;
            WorkItemBase.MainWorkspace.Close(View);
        }

        private void ViewWprowadzMax(object sender, EventArgs e)
        {
            if (aktualnyIndeks != null &&
                aktualnyIndeks.lokalizacje != null &&
                View.ZaznaczonaLokalizacja != null)
            {
                modyfikowanaLokalizacja = View.ZaznaczonaLokalizacja;
                MyWorkItem.WprowadzMax(aktualnyIndeks.symbol, modyfikowanaLokalizacja.lokalizacja);
            }
        }

        private void ViewSzukaj(object sender, EventArgs e)
        {
            MyWorkItem.Szukaj();
        }

        private void ViewWybierz(object sender, EventArgs e)
        {
            ViewPowrot(sender, e);
        }

        public void UstawWlasciwosciPrzyciskow()
        {
            if ((MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] != null)
                && (MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS].ToString().Equals("T")))
            {
                View.WybierzEnabled = true;
                View.WybierzVisible = true;
                View.PowrotEnabled = false;
                View.PowrotVisible = false;
            }
            else
            {
                View.WybierzEnabled = false;
                View.WybierzVisible = false;
                View.PowrotEnabled = true;
                View.PowrotVisible = true;
            }
        }

        #endregion

        protected override void HandleNavigationKey(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    e.Handled = true;
                    ViewPoprzedni(this, EventArgs.Empty);
                    break;
                case Keys.Right:
                    e.Handled = true;
                    ViewNastepny(this, EventArgs.Empty);
                    break;
                case Keys.D1:
                    e.Handled = true;
                    ViewSzukaj(this, EventArgs.Empty);
                    break;
                    //case Keys.D2:
                    //    e.Handled = true;
                    //    ViewWprowadzMax(this, EventArgs.Empty);
                    //    break;
                case Keys.Return:
                    e.Handled = true;
                    if (MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS] != null
                     && MyWorkItem.RootWorkItem.State[StateConstants.KARTOTEKA_INDEKSU_JAKO_BROWS].Equals("T"))
                    {
                        ViewPowrot(this, EventArgs.Empty);
                    }
                    break;
                case Keys.Escape:
                    e.Handled = true;
                    ViewPowrot(this, EventArgs.Empty);
                    break;
            }
        }

        protected override void AttachView()
        {
            View.Szukaj += ViewSzukaj;
            View.WprowadzMax += ViewWprowadzMax;
            View.Powrot += ViewPowrot;
            View.Wybierz += ViewWybierz;
            View.Poprzedni += ViewPoprzedni;
            View.Nastepny += ViewNastepny;
            View.ZaznaczonaLokalizacjaChanged += view_ZaznaczonaLokalizacjaChanged;
        }

        [EventSubscription(EventBrokerConstants.NAVIGATION_STATE_CHANGED)]
        public override void NavigationStateChanged(object sender, EventArgs e)
        {

            if (MyWorkItem.MainWorkspace.ActiveSmartPart.ToString() != View.ToString())
            {
                return;
            }

            base.NavigationStateChanged(sender, e);

            if (aktualnyIndeks != null)
            {
                View.WagaReadOnly = NavigationState;
            }


            //jezeli enter (edycja => nawigacja) to ustawSkontrolowana
            if (NavigationState && (aktualnyIndeks != null) &&
                (NavigationKeyArgs == null || NavigationKeyArgs.KeyCode != Keys.Escape))
            {
                aktualnyIndeks.waga =  View.Waga == -1 ? 0 : View.Waga;
                MyWorkItem.ZapiszZmianeWagi(aktualnyIndeks);
            }
        }

        #region Private Methods

        private Indeks GetIndeks(string aktualizowanyIndeks)
        {
            Indeks rezultat = null;
            foreach (var indeks in listaIndeksow)
            {
                if (indeks.symbol.Equals(aktualizowanyIndeks))
                {
                    rezultat = indeks;
                    break;
                }
            }
            return rezultat;
        }

        private void view_ZaznaczonaLokalizacjaChanged(object sender, EventArgs e)
        {
            View.WprowadzMaxEnabled = View.ZaznaczonaLokalizacja != null;

            if (View.ZaznaczonaLokalizacja != null)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.SYMBOL_MIEJSCA] = View.ZaznaczonaLokalizacja.lokalizacja;
            }
        }

        private void AktualizujWidok()
        {
            View.DataSource = aktualnyIndeks;
            AktualizujLokalizacje();

            if (aktualnyIndeks != null)
            {
                MyWorkItem.RootWorkItem.State[StateConstants.SymbolIndeksuDlaKartotekiKodowKreskowych] = aktualnyIndeks.symbol;
                MyWorkItem.RootWorkItem.State[StateConstants.KartotekaKodowKreskowychJakoBrows] = string.IsNullOrEmpty(aktualnyIndeks.symbol) ? StateConstants.BoolValueNo : StateConstants.BoolValueYes;
            }
        }

        private void AktualizujLokalizacje()
        {
            View.LokalizacjeDataSource.Clear();
            float sumaIlosci = 0;
            float sumaPojemnosci = 0;

            if (aktualnyIndeks != null &&
                aktualnyIndeks.lokalizacje != null)
            {
                Miejsce miejsce;
                for (var i = 0; i < aktualnyIndeks.lokalizacje.Length; i++)
                {
                    miejsce = aktualnyIndeks.lokalizacje[i];
                    sumaIlosci += miejsce.ilosc;
                    try
                    {
                        sumaPojemnosci += miejsce.pojemnosc;
                    }
                    catch
                    {
                        // stary kod: sumaPojemnosci += miejsce.pojemnosc ? 0;
                        // nagle zaczal sypac wyjatkiem Operator '??' cannot be applied to operands of type 'float' and 'float'
                    }

                    View.LokalizacjeDataSource.Add(UtworzWpis(i + 1, miejsce));
                }
            }
            View.PodsumowanieDataSource.Clear();
            View.PodsumowanieDataSource.Add(
                new ListViewItem(
                    new[]
                        {
                            string.Empty,
                            SUM,
                            sumaIlosci.ToString(WZORZEC_LICZBY_RZECZYWISTEJ),
                            sumaPojemnosci.ToString(WZORZEC_LICZBY_RZECZYWISTEJ)
                        }));
        }

        private ListViewItem UtworzWpis(int lp, Miejsce miejsce)
        {
            var elementy = new[]
                               {
                                   lp.ToString(),
                                   miejsce.lokalizacja,
                                   miejsce.ilosc.ToString(WZORZEC_LICZBY_RZECZYWISTEJ),
                                   miejsce.pojemnosc.ToString()
                               };
            var wpis = new ListViewItem(elementy);
            wpis.Tag = miejsce;

            return wpis;
        }

        private void ZmienStrone(int kierunek)
        {
            numerStrony += kierunek;
            zmianaStrony = kierunek;
            ZaladujDaneDoWidoku(MyWorkItem.PobierzListeIndeksow(numerStrony));
        }

        public void WczytajMase(Masa masa)
        {
            if ((aktualnyIndeks != null) && (masa.wartosc > 0))
            {
                if (NavigationState)
                {
                    View.SetNavigationState(false);
                    MyWorkItem.Commands[CommandConstants.REQUEST_EDIT_STATE_ACTIVATION].Execute();
                }

                View.WagaReadOnly = false;
                View.WagaSetFocus();

                View.UstawWage(masa.wartosc.ToString("0.000"));
                aktualnyIndeks.waga = masa.wartosc;
            }
        }

        #endregion
    }
}