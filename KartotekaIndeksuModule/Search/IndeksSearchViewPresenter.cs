#region

using System;
using Common;
using Common.SearchForm;
using Microsoft.Practices.Mobile.CompositeUI.EventBroker;

#endregion

namespace KartotekaIndeksuModule.Search
{
    public class IndeksSearchViewPresenter : SearchFormPresenter
    {
        // poniewaz rzutowania stosowanie DataEventArgs<Kryteria..>> rzuca wyjatek
        // zastosowano eventHandler'a, a argument przekazywany przez State

        public IndeksSearchViewPresenter(IIndeksSearchView view) : base(view)
        {
        }

        public IIndeksSearchView View
        {
            get { return m_view as IIndeksSearchView; }
        }

        [EventPublication(EventBrokerConstants.WYSZUKAJ_INDEKSY)]
        public event EventHandler WyszukajIndeksy;

        protected override void OnViewSzukaj(object sender, EventArgs eventArgs)
        {
            base.OnViewSzukaj(sender, eventArgs);
            OnWyszukajIndeksy(View.Kryteria);
        }

        protected override void OnViewUstawOstatnieZapytanie(object sender, EventArgs eventArgs)
        {
            base.OnViewUstawOstatnieZapytanie(sender, eventArgs);
            View.Kryteria = State[StateConstants.CRITERIAS] as KryteriaZapytaniaIndeksu ?? new KryteriaZapytaniaIndeksu();
        }

        protected void OnWyszukajIndeksy(KryteriaZapytaniaIndeksu kryteria)
        {
            // poniewaz rzutowania stosowanie DataEventArgs<Kryteria..>> rzuca wyjatek
            // zastosowano eventHandler'a, a argument przekazywany przez State
            State[StateConstants.CRITERIAS] = kryteria;
            if (WyszukajIndeksy != null)
            {
                WyszukajIndeksy(this, EventArgs.Empty);
            }
        }
    }
}