using System;
using System.Collections.Generic;
using System.Text;

namespace KartotekaIndeksuModule.Search
{
	public class KryteriaZapytaniaIndeksu
	{
		#region Private Fields

		private string m_symbol;
		private string m_abc;
		private string m_jm;
		private string m_nazwa;
		private string m_branzysta;
		private string m_vat;

		#endregion
		#region Constructors

		public KryteriaZapytaniaIndeksu()
		{
				
		}

		public KryteriaZapytaniaIndeksu(string symbol):this()
		{
			m_symbol = symbol;
		}

		public KryteriaZapytaniaIndeksu(string symbol, string nazwa, string branzysta, string abc, string jm, string vat):this(symbol,nazwa,branzysta)
		{
			m_abc = abc;
			m_jm = jm;
			m_vat = vat;
		}

		public KryteriaZapytaniaIndeksu(string symbol, string nazwa, string branzysta):this(symbol)
		{
			m_nazwa = nazwa;
			m_branzysta = branzysta;
		}

		#endregion
		#region Properties

		public string Abc
		{
			get { return m_abc; }
			set { m_abc = value; }
		}

		public string Branzysta
		{
			get { return m_branzysta; }
			set { m_branzysta = value; }
		}

		public string Jm
		{
			get { return m_jm; }
			set { m_jm = value; }
		}

		public string Nazwa
		{
			get { return m_nazwa; }
			set { m_nazwa = value; }
		}

		public string Symbol
		{
			get { return m_symbol; }
			set { m_symbol = value; }
		}
		
		public string Vat
		{
			get { return m_vat; }
			set { m_vat = value; }
		}

		#endregion
	}
}
