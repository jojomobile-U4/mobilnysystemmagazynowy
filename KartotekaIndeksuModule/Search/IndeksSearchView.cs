#region

using Common.SearchForm;
using Microsoft.Practices.Mobile.CompositeUI.SmartParts;

#endregion

namespace KartotekaIndeksuModule.Search
{
    [SmartPart]
    public partial class IndeksSearchView : SearchForm, IIndeksSearchView
    {
        public IndeksSearchView()
        {
            InitializeComponent();
            InitializeFocusedControl();
        }

        #region IIndeksSearchView Members

        public KryteriaZapytaniaIndeksu Kryteria
        {
            get { return new KryteriaZapytaniaIndeksu(tbSymbol.Text, tbNazwaTowaru.Text, tbBranzysta.Text); }
            set
            {
                if (value != null)
                {
                    tbSymbol.Text = value.Symbol;
                    tbNazwaTowaru.Text = value.Nazwa;
                    tbBranzysta.Text = value.Branzysta;
                }
                else
                {
                    tbSymbol.Text = tbNazwaTowaru.Text = tbBranzysta.Text = string.Empty;
                }
            }
        }

        public override void SetNavigationState(bool navigationState)
        {
            base.SetNavigationState(navigationState);

            tbBranzysta.ReadOnly = navigationState;
            tbNazwaTowaru.ReadOnly = navigationState;
            tbSymbol.ReadOnly = navigationState;
        }

        #endregion
    }
}