#region

using Common.SearchForm;

#endregion

namespace KartotekaIndeksuModule.Search
{
    public interface IIndeksSearchView : ISearchForm
    {
        KryteriaZapytaniaIndeksu Kryteria { get; set; }
    }
}