
using Common.Components;

namespace KartotekaIndeksuModule.Search
{
	partial class IndeksSearchView
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tbNazwaTowaru = new System.Windows.Forms.TextBox();
			this.lbNazwa = new System.Windows.Forms.Label();
			this.tbBranzysta = new System.Windows.Forms.TextBox();
			this.lbBranza = new System.Windows.Forms.Label();
			this.tbSymbol = new System.Windows.Forms.TextBox();
			this.lbSymbol = new System.Windows.Forms.Label();
			this.lbTytul = new Common.Components.MSMLabel();
			this.pnlNavigation.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnAnuluj
			// 
			this.btnAnuluj.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.btnAnuluj.Location = new System.Drawing.Point(165, 8);
			// 
			// btnOstatnieZapytanie
			// 
			this.btnOstatnieZapytanie.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.btnOstatnieZapytanie.Location = new System.Drawing.Point(63, 8);
			// 
			// btnOK
			// 
			this.btnOK.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.btnOK.Location = new System.Drawing.Point(3, 8);
			// 
			// pnlNavigation
			// 
			this.pnlNavigation.Location = new System.Drawing.Point(0, 237);
			this.pnlNavigation.Size = new System.Drawing.Size(240, 31);
			// 
			// tbNazwaTowaru
			// 
			this.tbNazwaTowaru.Location = new System.Drawing.Point(48, 54);
			this.tbNazwaTowaru.Name = "tbNazwaTowaru";
			this.tbNazwaTowaru.Size = new System.Drawing.Size(189, 21);
			this.tbNazwaTowaru.TabIndex = 1;
			// 
			// lbNazwa
			// 
			this.lbNazwa.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
			this.lbNazwa.Location = new System.Drawing.Point(3, 55);
			this.lbNazwa.Name = "lbNazwa";
			this.lbNazwa.Size = new System.Drawing.Size(45, 21);
			this.lbNazwa.Text = "Nazwa:";
			// 
			// tbBranzysta
			// 
			this.tbBranzysta.Location = new System.Drawing.Point(48, 78);
			this.tbBranzysta.Name = "tbBranzysta";
			this.tbBranzysta.Size = new System.Drawing.Size(189, 21);
			this.tbBranzysta.TabIndex = 2;
			// 
			// lbBranza
			// 
			this.lbBranza.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
			this.lbBranza.Location = new System.Drawing.Point(3, 79);
			this.lbBranza.Name = "lbBranza";
			this.lbBranza.Size = new System.Drawing.Size(45, 21);
			this.lbBranza.Text = "Bran�a:";
			// 
			// tbSymbol
			// 
			this.tbSymbol.Location = new System.Drawing.Point(48, 30);
			this.tbSymbol.Name = "tbSymbol";
			this.tbSymbol.Size = new System.Drawing.Size(189, 21);
			this.tbSymbol.TabIndex = 0;
			// 
			// lbSymbol
			// 
			this.lbSymbol.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
			this.lbSymbol.Location = new System.Drawing.Point(3, 31);
			this.lbSymbol.Name = "lbSymbol";
			this.lbSymbol.Size = new System.Drawing.Size(45, 21);
			this.lbSymbol.Text = "Indeks:";
			// 
			// lbTytul
			// 
			this.lbTytul.BackColor = System.Drawing.Color.Empty;
			this.lbTytul.BackGroundColor = System.Drawing.Color.Black;
			this.lbTytul.CenterAlignX = false;
			this.lbTytul.CenterAlignY = true;
			this.lbTytul.ColorText = System.Drawing.Color.White;
			this.lbTytul.Dock = System.Windows.Forms.DockStyle.Top;
			this.lbTytul.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
			this.lbTytul.ForeColor = System.Drawing.Color.Empty;
			this.lbTytul.Location = new System.Drawing.Point(0, 0);
			this.lbTytul.Name = "lbTytul";
			this.lbTytul.RectangleColor = System.Drawing.Color.Black;
			this.lbTytul.Size = new System.Drawing.Size(240, 16);
			this.lbTytul.TabIndex = 1;
			this.lbTytul.TabStop = false;
			this.lbTytul.TextDisplayed = "SZUKAJ INDEKSU";
			// 
			// IndeksSearchView
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
			this.AutoScroll = true;
			this.Controls.Add(this.lbTytul);
			this.Controls.Add(this.tbNazwaTowaru);
			this.Controls.Add(this.lbNazwa);
			this.Controls.Add(this.tbBranzysta);
			this.Controls.Add(this.lbBranza);
			this.Controls.Add(this.tbSymbol);
			this.Controls.Add(this.lbSymbol);
			this.Name = "IndeksSearchView";
			this.Controls.SetChildIndex(this.pnlNavigation, 0);
			this.Controls.SetChildIndex(this.lbSymbol, 0);
			this.Controls.SetChildIndex(this.tbSymbol, 0);
			this.Controls.SetChildIndex(this.lbBranza, 0);
			this.Controls.SetChildIndex(this.tbBranzysta, 0);
			this.Controls.SetChildIndex(this.lbNazwa, 0);
			this.Controls.SetChildIndex(this.tbNazwaTowaru, 0);
			this.Controls.SetChildIndex(this.lbTytul, 0);
			this.pnlNavigation.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TextBox tbNazwaTowaru;
		private System.Windows.Forms.Label lbNazwa;
		private System.Windows.Forms.TextBox tbBranzysta;
		private System.Windows.Forms.Label lbBranza;
		private System.Windows.Forms.TextBox tbSymbol;
		private System.Windows.Forms.Label lbSymbol;
		private MSMLabel lbTytul;
	}
}
